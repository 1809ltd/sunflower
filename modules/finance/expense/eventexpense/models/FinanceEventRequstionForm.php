<?php

namespace app\modules\finance\expense\eventexpense\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\finance\expense\eventexpense\eventexpenselines\models\FinanceEventRequstionFormList;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;
use app\modules\personnel\personneldetails\models\PersonnelDetails;
// use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionForm;
use app\modules\event\workplan\models\EventWorkplan;


use Yii;

/**
 * This is the model class for table "finance_event_requstion_form".
 *
 * @property int $id
 * @property string $requstionNumber
 * @property int $eventId
 * @property string $date
 * @property int $requstionStatus
 * @property string $requstionNote
 * @property double $amount
 * @property int $preparedBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $approvedBy
 *
 * @property EventDetails $event
 * @property UserDetails $user
 * @property UserDetails $preparedBy0
 * @property UserDetails $approvedBy0
 * @property FinanceEventRequstionFormList[] $financeEventRequstionFormLists
 * @property FinanceRequisitionPayments[] $financeRequisitionPayments
 */
class FinanceEventRequstionForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_event_requstion_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requstionNumber', 'eventId', 'date', 'requstionStatus', 'amount', 'preparedBy', 'userId'], 'required'],
            [['eventId', 'requstionStatus', 'preparedBy', 'userId', 'approvedBy'], 'integer'],
            [['date', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['requstionNote'], 'string'],
            [['amount'], 'number'],
            [['requstionNumber'], 'string', 'max' => 100],
            [['requstionNumber'], 'unique'],
            [['eventId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['eventId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['preparedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['preparedBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requstionNumber' => 'Requstion Number',
            'eventId' => 'Event',
            'date' => 'Date',
            'requstionStatus' => 'Requstion Status',
            'requstionNote' => 'Requstion Note',
            'amount' => 'Amount',
            'preparedBy' => 'Prepared By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'approvedBy' => 'Approved By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'eventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreparedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'preparedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionFormLists()
    {
        return $this->hasMany(FinanceEventRequstionFormList::className(), ['reqId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceRequisitionPayments()
    {
        return $this->hasMany(FinanceRequisitionPayments::className(), ['reqpaymentLinkedTxn' => 'requstionNumber']);
    }
    public function getRQnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(requstionNumber) AS number from finance_event_requstion_form where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "RQEV-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "RQEV-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "RQEV-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }

    public function geteventDetailss($id)
    {
      return EventDetails::find()
        ->where(['and', "id=$id"])
        ->all();
    }

    public function getFinanceEventRequstionFormList($id)
    {
      return FinanceEventRequstionFormList::find()
        ->where(['and', "reqId=$id"])
        ->all();
    }
    public function getActivity($id)
    {
      return FinanceEventActivity::find()
        ->where(['and', "id=$id"])
        ->all();
    }
}
