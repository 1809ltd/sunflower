CREATE OR REPLACE  VIEW `expense_summary` AS
-- The Expenses Part
-- A The Internal Expenses Part
-- 1) All Office requstion form
SELECT
	finance_office_requstion_form.id AS transactionId,
	finance_office_requstion_form.requstionNumber AS referenceCode,
	'' AS projectId,
	'' AS recipientId,
	'' AS recipientName,
	CONCAT( "Office Requstion: ", finance_office_requstion_form.requstionNumber ) AS tranctionDescription,
	CONCAT( sum( finance_office_requstion_form_list.amont ), 0 ) AS subAmount,
	'0' AS taxAmount,
	finance_office_requstion_form.date AS transactionDate,
	finance_office_requstion_form.requstionStatus AS `status`,
	'Expense' AS transactionCategory,
	'Office Expense' AS transactionClassification,
	'finance_office_requstion_form' AS referenceTable
FROM
	finance_office_requstion_form_list
	INNER JOIN finance_office_requstion_form ON finance_office_requstion_form_list.reqId = finance_office_requstion_form.id
WHERE
	finance_office_requstion_form.requstionStatus > 0
	AND finance_office_requstion_form_list.`status` > 0
GROUP BY
	finance_office_requstion_form.id
   UNION ALL
   -- 2 Lead Requistion
SELECT
	`finance_lead_requstion_form`.`id` AS `transactionId`,
	`finance_lead_requstion_form`.`requstionNumber` AS `referenceCode`,
	`finance_lead_requstion_form_list`.`activityId` AS `projectId`,
	'' AS recipientId,
	'' AS recipientName,
	CONCAT( "Marketing Expense for Lead: ", finance_lead_requstion_form.requstionNumber ) AS `tranctionDescription`,
	CONCAT( sum( `finance_lead_requstion_form_list`.`amont` ), 0 ) AS subAmount,
	'0' AS taxAmount,
	`finance_lead_requstion_form`.`date` AS `transactionDate`,
	finance_lead_requstion_form.requstionStatus AS `status`,
	'Expense' AS `transactionCategory`,
	'Lead expense' AS `transactionClassification`,
	'finance_lead_requstion_form' AS `referenceTable`
FROM
	`finance_lead_requstion_form_list`
	INNER JOIN `finance_lead_requstion_form` ON `finance_lead_requstion_form_list`.`reqId` = `finance_lead_requstion_form`.`id`
WHERE
	finance_lead_requstion_form.requstionStatus > 0
	AND finance_lead_requstion_form_list.STATUS > 0
GROUP BY
	finance_lead_requstion_form.id
   UNION ALL
   -- 3 Event Requstion
SELECT
	`finance_event_requstion_form`.`id` AS `transactionId`,
	`finance_event_requstion_form`.`requstionNumber` AS `referenceCode`,
	`finance_event_requstion_form_list`.`activityId` AS `projectId`,
	'' AS recipientId,
	'' AS recipientName,
	CONCAT( "Event Requstion: ", finance_event_requstion_form.requstionNumber ) AS `tranctionDescription`,
	CONCAT( sum( `finance_event_requstion_form_list`.`amont` ), 0 ) AS subAmount,
	'0' AS taxAmount,
	`finance_event_requstion_form`.`date` AS `transactionDate`,
	`finance_event_requstion_form`.requstionStatus AS `status`,
	'Expense' AS `transactionCategory`,
	'Event Expense' AS `transactionClassification`,
	'finance_event_requstion_form' AS `referenceTable`
FROM
	`finance_event_requstion_form_list`
	INNER JOIN `finance_event_requstion_form` ON `finance_event_requstion_form_list`.`reqId` = `finance_event_requstion_form`.`id`
WHERE
	`finance_event_requstion_form_list`.`status` > 0
	AND finance_event_requstion_form.requstionStatus > 0
GROUP BY
	finance_event_requstion_form.id
UNION ALL
-- The External Expenses Part
-- 4 Bill Lines
SELECT
	finance_bills.id AS transactionId,
	finance_bills.billRef AS referenceCode,
	finance_bills.projectId AS projectId,
	finance_bills.vendorId AS recipientId,
	vendor_company_details.vendorCompanyName AS recipientName,
	CONCAT( "Bill Expense: ", `finance_bills`.`billRef` ) AS tranctionDescription,
	CONCAT( sum( finance_bills_lines.biilsLinesAmount ), 0 ) AS subAmount,
	CONCAT( sum( finance_bills_lines.biilsLinesTaxAmount ), 0 ) AS taxAmount,
	finance_bills.billDueDate AS transactionDate,
	finance_bills.billStatus AS `status`,
	'Expense' AS transactionCategory,
	'Bill Expense' AS transactionClassification,
	'finance_bills' AS referenceTable
FROM
	finance_bills_lines
	INNER JOIN finance_bills ON finance_bills_lines.biilsId = finance_bills.id
	LEFT JOIN vendor_company_details ON finance_bills.vendorId = vendor_company_details.id
WHERE
	finance_bills.billStatus > 0
	AND finance_bills_lines.billsLinesBillableStatus > 0
GROUP BY
	finance_bills.id
