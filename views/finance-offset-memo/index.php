<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceOffsetMemoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Offset Memos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-offset-memo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Offset Memo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'offsetMemoAccount',
            'refernumber',
            'offsetMemoTxnDate',
            'offsetMemoNote:ntext',
            //'customerId',
            //'offsetAmount',
            //'offsetMemoStatus',
            //'offsetMemoCreatedAt',
            //'offsetMemoUpdatedAt',
            //'offsetMemoDeletedAt',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
