<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\receipt\invoicepayment\models\FinancePaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Finance Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Finance Payment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'invoicePaymentPayType',
            'invoicePaymentaccount',
            'recepitNumber',
            'invoicePaymentTotalAmt',
            //'unallocatedAmount',
            //'date',
            //'invoicePaymentinvoiceRef',
            //'customerId',
            //'invoicePaymentPrivateNote:ntext',
            //'invoicePaymentCreatedAt',
            //'invoicePaymentUpdatedAt',
            //'invoicePaymentDeletedAt',
            //'userId',
            //'createdBy',
            //'approvedBy',
            //'paymentstatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
