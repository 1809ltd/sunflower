<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\transfer\models\FinanceTransferSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-transfer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tranferRefernce') ?>

    <?= $form->field($model, 'tranferFromAccountRef') ?>

    <?= $form->field($model, 'tranferToAccountRef') ?>

    <?= $form->field($model, 'tranferAmount') ?>

    <?php // echo $form->field($model, 'transferCharges') ?>

    <?php // echo $form->field($model, 'tranferTxnDate') ?>

    <?php // echo $form->field($model, 'tranferCreatedAt') ?>

    <?php // echo $form->field($model, 'tranferUpdatedAt') ?>

    <?php // echo $form->field($model, 'tranferDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
