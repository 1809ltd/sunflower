<?php

namespace app\modules\finance\productsetup\financeitemscategory;

/**
 * financeitemscategory module definition class
 */
class financeitemscategory extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\productsetup\financeitemscategory\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
