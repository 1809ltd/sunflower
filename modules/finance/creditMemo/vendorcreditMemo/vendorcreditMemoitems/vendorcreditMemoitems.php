<?php

namespace app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems;

/**
 * vendorcreditMemoitems module definition class
 */
class vendorcreditMemoitems extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
