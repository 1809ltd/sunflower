<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Invoice details*/
use app\modules\finance\income\invoice\models\FinanceInvoice;
/* @var $this yii\web\View */
/* @var $model app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-payment-lines-form">

</div>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body finance-payment-lines-form">

  <?= $form->field($model, 'invoiceId')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(FinanceInvoice::find()
                              ->where('`invoiceTxnStatus` != 0')
                              ->Andwhere(['customerId' => $_SESSION['customerInvoiceId']])
                              ->all(),'id','invoiceNumber'),
      'language' => 'en',
      'options' => ['placeholder' => 'Select a customer Invoice ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);?>

  <?= $form->field($model, 'amount')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
  <?php
  // necessary for update action.
  if ($model->isNewRecord&&((!empty($_GET)))) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput(['value'=> $_GET["status"]])->label(false);
    echo  $form->field($model, 'invoicepaymentId')->hiddenInput(['value'=> $_GET["invoicepaymentId"]])->label(false);

  }elseif (!$model->isNewRecord) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput()->label(false);
    echo  $form->field($model, 'customerId')->hiddenInput()->label(false);

  }else {
    // code...
    echo  $form->field($model, 'customerId')->hiddenInput(['value'=>$_SESSION['customerInvoiceId']])->label(false);
    echo  $form->field($model, 'status')->hiddenInput(['value'=>3])->label(false);

  }
  ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
