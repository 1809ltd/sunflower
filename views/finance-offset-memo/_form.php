<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;

use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;

/*Getting the product and service offered, customer*/

use app\models\FinanceInvoice;
use app\models\CustomerDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceoffsetMemo */
/* @var $form yii\widgets\ActiveForm */

?>
<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="panel-body finance-offset-memo-form">

                  <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>


                  <div class="col-sm-3">
                    <div class="form-group">

                      <?php $customer = ArrayHelper::map(CustomerDetails::find()->all(),'id','customerDisplayName'); ?>
                      <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                          'data' => $customer,
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Customer ...',
                                        'onChange'=>'$.post("index.php?r=finance-invoice/lists&customerId='.'"+$(this).val(),function(data){
                                          $("select#invoiceNumber").html(data);
                                        });'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?php
                          // necessary for update action.
                          if (! $model->isNewRecord) {
                            // code...
                            $offsetMemoNum = $model->refernumber;

                          } else {
                            // code...
                            $offsetMemoNum=$model->getOffstnumber();
                          }

                      ?>
                      <?= $form->field($model, 'refernumber')->textInput(['readonly' => true, 'value' => $offsetMemoNum])?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'offsetMemoAccount')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(FinanceAccounts::find()->all(),'id','fullyQualifiedName'),
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Account ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'offsetMemoTxnDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

                    </div>
                  </div>



                  <div class="col-sm-3">
                    <div class="form-group">


                    </div>
                  </div>

                  <div class="panel-body"></div>

                  <div class="panel-body">

                    <div class="panel panel-pvr panel--style--1">
                      <div class="panel-heading">
                          <div class="panel-heading-btn">
                              <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                              <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                              <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                              <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                          </div>
                          <h4 class="panel-title">Add offset Memo Item</h4>
                      </div>
                      <div class="panel-body">
                           <?php DynamicFormWidget::begin([
                              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                              'widgetBody' => '.container-items', // required: css class selector
                              'widgetItem' => '.item', // required: css class
                              'limit' => 4, // the maximum times, an element can be cloned (default 999)
                              'min' => 1, // 0 or 1 (default 1)
                              'insertButton' => '.add-item', // css class
                              'deleteButton' => '.remove-item', // css class
                              'model' => $modelsFinanceOffsetMemoLines[0],
                              'formId' => 'dynamic-form',
                              'formFields' => [

                                'invoiceNumber',
                                'offsetMemoLineDescription',
                                'offsetMemoLineAmount',
                                //'offsetMemoLineStatus',

                              ],
                          ]); ?>

                          <div class="container-items"><!-- widgetContainer -->
                            <!-- Loopping Items in the Lists -->
                          <?php foreach ($modelsFinanceOffsetMemoLines as $i => $modelFinanceOffsetMemoLines): ?>
                              <div class="item panel-pvr panel--style--2 "><!-- widgetBody -->


                                  <div class="panel-heading">
                                      <h3 class="panel-title pull-left">offset Memo Item</h3>
                                      <div class="pull-right">
                                          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>


                                  <div class="panel-body">
                                      <?php
                                          // necessary for update action.
                                          if (! $modelFinanceOffsetMemoLines->isNewRecord) {
                                              echo Html::activeHiddenInput($modelFinanceOffsetMemoLines, "[{$i}]id");
                                          }
                                      ?>
                                      <div class="col-sm-4">
                                        <div class="form-group">

                                          <?= $form->field($modelFinanceOffsetMemoLines, "[{$i}]invoiceNumber")->widget(Select2::classname(), [
                                              'data' => ArrayHelper::map(FinanceInvoice::find()->all(),'invoiceNumber','invoiceNumber'),
                                              'language' => 'en',
                                              'class' => 'invoiceNumber form-control',
                                              'options' => ['placeholder' => 'Select a Invoice ...'],
                                              'pluginOptions' => [
                                                  'allowClear' => true
                                              ],
                                          ]); ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceOffsetMemoLines, "[{$i}]offsetMemoLineDescription")->textarea(['rows' => 6]) ?>
                                        </div>
                                      </div>

                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceOffsetMemoLines, "[{$i}]offsetMemoLineAmount")->textInput([
                                          'maxlength' => true,
                                          'class' => 'sumTaxPart form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>

                                  </div>
                              </div>
                          <?php endforeach; ?>
                          </div>
                          <?php DynamicFormWidget::end(); ?>
                      </div>
                  </div>

                  </div>

                    <div class="panel-body"></div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($model, 'offsetMemoNote')->textarea(['rows' => 6]) ?>

                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <?= $form->field($model, 'offsetAmount')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
                      </div>
                    </div>

                    <div class="col-sm-3">
                      <div class="form-group">
                        <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                        <?= $form->field($model, 'offsetMemoStatus')->dropDownList($status, ['prompt'=>'Select Status']);?>
                      </div>
                    </div>


                    <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>


                    <div class="panel-body"></div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= Html::submitButton($modelFinanceOffsetMemoLines->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                    </div>
                  </div>

                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->

<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var tax=0

        items.each(function (index, elem) {

            //getting specific elements

            //getting the tax amount figures
            var taxValue = $(elem).find(".sumTaxPart").val();

            //getting total tax amountValue
            tax = parseFloat(tax) + parseFloat(taxValue);

            //assing value to the total tax amount
            $(".tax").val(tax);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
