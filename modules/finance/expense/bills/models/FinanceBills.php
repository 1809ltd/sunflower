<?php

namespace app\modules\finance\expense\bills\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Payment Lines Items*/
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models\FinanceVendorCreditMemoLines;
use app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines;


use Yii;

/**
 * This is the model class for table "finance_bills".
 *
 * @property int $id
 * @property int $projectId
 * @property int $vendorId
 * @property string $billRef
 * @property string $billDueDate
 * @property double $billAmount
 * @property double $billsTaxAmount
 * @property string $billCreatetime
 * @property string $billUpdatedTime
 * @property string $billDeletedAt
 * @property int $billStatus
 * @property int $userId
 *
 * @property EventDetails $project
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 * @property FinanceBillsLines[] $financeBillsLines
 * @property FinanceBillsPaymentsLines[] $financeBillsPaymentsLines
 * @property FinanceVendorOffsetMemoLines[] $financeVendorOffsetMemoLines
 */
class FinanceBills extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
     public $billTotal;
    public static function tableName()
    {
        return 'finance_bills';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['projectId', 'vendorId', 'billStatus', 'userId'], 'integer'],
            [['vendorId', 'billRef', 'billDueDate', 'billAmount', 'billsTaxAmount', 'billStatus', 'userId'], 'required'],
            [['billDueDate', 'billCreatetime', 'billUpdatedTime', 'billDeletedAt'], 'safe'],
            [['billAmount', 'billsTaxAmount'], 'number'],
            [['billRef'], 'string', 'max' => 50],
            [['vendorId', 'billRef'], 'unique', 'targetAttribute' => ['vendorId', 'billRef']],
            [['projectId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['projectId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'projectId' => 'Project/ Event',
          'vendorId' => 'Vendor',
          'billRef' => 'Invoice No:',
          'billDueDate' => 'Due Date',
          'billAmount' => 'Sub Amount',
          'billsTaxAmount' => 'Total Tax Amount',
          'billCreatetime' => 'Bill Createtime',
          'billUpdatedTime' => 'Bill Updated Time',
          'billDeletedAt' => 'Bill Deleted At',
          'billStatus' => 'Status',
          'userId' => 'User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'projectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsLines()
    {
        return $this->hasMany(FinanceBillsLines::className(), ['biilsId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsPaymentsLines()
    {
        return $this->hasMany(FinanceBillsPaymentsLines::className(), ['billId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorOffsetMemoLines()
    {
        return $this->hasMany(FinanceVendorOffsetMemoLines::className(), ['billRef' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemoLines()
    {
        return $this->hasMany(FinanceVendorCreditMemoLines::className(), ['billRef' => 'id']);
    }
    // All Payments
    public function allPayments($id){
      // Get the actual Payments
      $inoicepayment= $this->find()->joinWith('financeBillsPaymentsLines')
                           ->where(['finance_bills_payments_lines.billId' => $id])
                           ->Andwhere('finance_bills_payments_lines.status >0')
                           ->sum('finance_bills_payments_lines.amount');

      // Get all the Offest cancel payments
      $offsetPayments= $this->find()->joinWith('financeVendorOffsetMemoLines')
                           ->where(['finance_vendor_offset_memo_lines.billRef' => $id])
                           ->Andwhere('finance_vendor_offset_memo_lines.vendorOffsetMemoLineStatus >0')
                           ->sum('finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount');

      // Get all the Credit Payments
      $creditPayments=$this->find()->joinWith('financeVendorCreditMemoLines')
                           ->where(['finance_vendor_credit_memo_lines.billRef' => $id])
                           ->Andwhere('finance_vendor_credit_memo_lines.vendorCreditMemoLineStatus >0')
                           ->sum('finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount');

     // This is the total payments of all the amount
     $totalpaymentmade=$inoicepayment+$offsetPayments+$creditPayments;

     return $totalpaymentmade;
    }
    // Getting the Bill Total
    public function Balance()
    {
      $data = FinanceBills::findOne($this->id);

      $total = $data->billsTaxAmount+$data->billAmount;
      return $total;
    }
}
