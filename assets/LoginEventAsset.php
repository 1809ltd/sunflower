<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginEventAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',

        /*Begin Base Css Style*/
        'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        'assets1/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css',
        'assets1/plugins/bootstrap/css/bootstrap.min.css',
        'assets1/css/style.css',
        'assets1/css/style-responsive.css',
        'assets1/css/default.css',
        /*End Base Css Style*/

        /* BEGIN PAGE LEVEL JS */
        'assets1/plugins/page_login_v2/modernizr.custom.js',
        /* END PAGE LEVEL JS */

        /* BEGIN PAGE LEVEL STYLE */
        'assets1/plugins/page_login_v2/style.css',
        /* END PAGE LEVEL STYLE */


    ];
    public $js = [
        /* BEGIN BASE JS */
        'assets/plugins/jquery/jquery-1.12.4.min.js',
        'assets/plugins/jquery/jquery-migrate-1.4.1.min.js',
        'assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js',
        'assets/plugins/bootstrap/js/bootstrap.min.js',

        'assets/plugins/slimscroll/jquery.slimscroll.min.js',
        'assets/plugins/jquery-cookie/jquery.cookie.js',
        'assets/plugins/jquery-cookie/js.cookie.js',
        /* END BASE JS */


        /* BEGIN Login Page LEVEL JS*/
        'assets1/plugins/moment/moment.min.js',
        'assets1/js/pvr_ess_login_v2.js',
        /* END login page PAGE LEVEL JS*/

    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//        'yii\bootstrap5\BootstrapAsset'
    ];
}
