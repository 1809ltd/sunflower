<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompanySiteDetails */

$this->title = 'Update Company Site Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Site Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-site-details-update">

      <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
