<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\journal\models\FinanceJournalEntryLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-journal-entry-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'journalId')->textInput() ?>

    <?= $form->field($model, 'accountId')->textInput() ?>

    <?= $form->field($model, 'transactionId')->textInput() ?>

    <?= $form->field($model, 'postingType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'drAmount')->textInput() ?>

    <?= $form->field($model, 'crAmount')->textInput() ?>

    <?= $form->field($model, 'createAt')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
