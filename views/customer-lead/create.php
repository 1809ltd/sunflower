<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerLead */

$this->title = 'Create Customer Lead';
$this->params['breadcrumbs'][] = ['label' => 'Customer Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-lead-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
