<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOffsetMemoLines */

$this->title = 'Create Finance Offset Memo Lines';
$this->params['breadcrumbs'][] = ['label' => 'Finance Offset Memo Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-offset-memo-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
