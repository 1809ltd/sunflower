<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AssetAmortization */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-amortization-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'assetId')->textInput() ?>

    <?= $form->field($model, 'repayment')->textInput() ?>

    <?= $form->field($model, 'interestAmount')->textInput() ?>

    <?= $form->field($model, 'principalAmount')->textInput() ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
