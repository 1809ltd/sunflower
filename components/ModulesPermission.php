<?php

/**
 * @author Prakash S
 * @copyright 2017
 */

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class ModulesPermission extends Component
{
    public function getMenus()
    {

        // Getting the Roles of the users based on the userId
        $userId = Yii::$app->user->identity->id;

        $modules = \app\modules\user\userrolemodulepermission\models\UserRoleModulePermission::find()
            ->select('ML.id,ML.moduleName, ML.controller,  ML.route, ML.icon')
            ->join('LEFT JOIN', 'user_modules_list AS ML', 'ML.id = user_role_module_permission.moduleId')
            ->join('LEFT JOIN', 'user_assignment', 'user_assignment.roleId = user_role_module_permission.roleId')
            ->where('user_assignment.userId = :userId', [':userId' => $userId])
            ->andwhere(['is', 'ML.parentId', new \yii\db\Expression('null')])
            ->andWhere('user_role_module_permission.view = :view', [':view' => 1])
            ->andWhere('user_role_module_permission.view = :view', [':view' => 1])
            ->andWhere('user_assignment.`status` = :is_active', [':is_active' => 1])
            ->orderBy('ML.`parentId`')
            ->asArray()->all();
        $items = array();
        $items[] = '<li class="header">Menu</li>';
        $items[] = '<li id="dashboard"><a href="' . \yii\helpers\Url::to(['/']) . '"><span class="fa fa-dashboard"></span> Dashboard</a></li>';

        // List of Menu Based on rights of the users.
        for ($i = 0; $i < count($modules); $i++) {
            // code...
            // Get the first children
            $Children = \app\modules\user\userrolemodulepermission\models\UserRoleModulePermission::find()
                ->select('ML.id,ML.moduleName, ML.controller,  ML.route, ML.icon')
                ->join('LEFT JOIN', 'user_modules_list AS ML', 'ML.id = user_role_module_permission.moduleId')
                ->join('LEFT JOIN', 'user_assignment', 'user_assignment.roleId = user_role_module_permission.roleId')
                ->where('user_assignment.userId = :userId', [':userId' => $userId])
                ->andwhere('ML.parentId = :parentId', [':parentId' => $modules[$i]['id']])
                ->andWhere('user_role_module_permission.view = :view', [':view' => 1])
                ->andWhere('user_role_module_permission.view = :view', [':view' => 1])
                ->andWhere('user_assignment.`status` = :is_active', [':is_active' => 1])
                ->orderBy('ML.`parentId`')
                ->asArray()->all();

            // Checking if the list is Empty

            if (!empty($Children)) {
                // code...
                $items[] = '<li class="treeview"><a href="' . \yii\helpers\Url::to(['/' . $modules[$i]['route'] . '/']) . '"> <i class="fa ' . $modules[$i]['icon'] . '"></i><span>' . $modules[$i]['moduleName'] . '</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';

                $items[] = '<ul class="treeview-menu">';

                for ($j = 0; $j < count($Children); $j++) {
                    // code...
                    // Getting Grandchild
                    // Get the first children

                    $grandchild = \app\modules\user\userrolemodulepermission\models\UserRoleModulePermission::find()
                        ->select('ML.id,ML.moduleName, ML.controller,  ML.route, ML.icon')
                        ->join('LEFT JOIN', 'user_modules_list AS ML', 'ML.id = user_role_module_permission.moduleId')
                        ->join('LEFT JOIN', 'user_assignment', 'user_assignment.roleId = user_role_module_permission.roleId')
                        ->where('user_assignment.userId = :userId', [':userId' => $userId])
                        ->andwhere('ML.parentId = :parentId', [':parentId' => $Children[$i]['id']])
                        ->andWhere('user_role_module_permission.view = :view', [':view' => 1])
                        ->andWhere('user_role_module_permission.view = :view', [':view' => 1])
                        ->andWhere('user_assignment.`status` = :is_active', [':is_active' => 1])
                        ->orderBy('ML.`parentId`')
                        ->asArray()->all();

                    // Checking if the list is Empty

                    if (!empty($grandchild)) {
                        // code...
                        $items[] = '<li class="treeview"><a href="' . \yii\helpers\Url::to(['/' . $Children[$j]['route'] . '/']) . '"> <i class="fa ' . $Children[$j]['icon'] . '"></i><span>' . $Children[$j]['moduleName'] . '</span>
                    <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>';

                        $items[] = '<ul class="treeview-menu">';

                        for ($k = 0; $k < count($grandchild); $k++) {
                            // code...
                            $items[] = '<li id="' . $grandchild[$k]['controller'] . '"><a href="' . \yii\helpers\Url::to(['/' . $grandchild[$k]['route'] . '/']) . '"><span class="fa ' . $grandchild[$k]['icon'] . '"></span>  ' . $grandchild[$k]['moduleName'] . '</a></li>';

                        }
                        $items[] = '</ul></li>';
                        // $items[] = '</li>';

                    } else {
                        // No GrandChild  Available
                        // code...
                        $items[] = '<li id="' . $Children[$j]['controller'] . '"><a href="' . \yii\helpers\Url::to(['/' . $Children[$j]['route'] . '/']) . '"><span class="fa ' . $Children[$j]['icon'] . '"></span>  ' . $Children[$j]['moduleName'] . '</a></li>';

                    }

                }
                $items[] = '</ul></li>';

            } else {
                // No Child  Available
                // code...
                $items[] = '<li id="' . $modules[$i]['controller'] . '"><a href="' . \yii\helpers\Url::to(['/' . $modules[$i]['route'] . '/']) . '"><span class="fa ' . $modules[$i]['icon'] . '"></span>  ' . $modules[$i]['moduleName'] . '</a></li>';

            }

        }

        return $items;
    }

    public function getPermission()
    {
        // Mapping actions to their corresponding permission keys
        $actions = [
            'index' => 'view',
            'view' => 'view',
            'create' => 'new',
            'update' => 'save',
            'delete' => 'remove',
            'other' => 'other',
        ];

        // Getting user information
        $userId = Yii::$app->user->identity->id;
        $action = $actions[Yii::$app->controller->action->id];
        $controller = Yii::$app->controller->id;

        // Querying the database for user permissions
        $permission = \app\modules\user\userrolemodulepermission\models\UserRoleModulePermission::find()
            ->select($action)
            ->join('LEFT JOIN', 'user_modules_list AS ML', 'ML.id = user_role_module_permission.moduleId')
            ->join('LEFT JOIN', 'user_assignment', 'user_assignment.roleId = user_role_module_permission.roleId')
            ->where('user_assignment.userId = :userId', [':userId' => $userId])
            ->andWhere('user_assignment.`status` = :is_active', [':is_active' => 1])
            ->andWhere($action . ' = :' . $action, [':' . $action => 1])
            ->andWhere('ML.controller = :controller', [':controller' => $controller])
            ->one();

        // Checking if $permission is not null and the action key exists
        return $permission && isset($permission[$action]) ? $permission[$action] : false;
    }

    public function getSpecificPermission($kazi)
    {
        // Getting user information
        $userId = Yii::$app->user->identity->id;
        $action = $kazi;
        $controller = Yii::$app->controller->id;

        // Querying the database for user-specific permissions
        $permission = \app\modules\user\userrolemodulepermission\models\UserRoleModulePermission::find()
            ->select($action)
            ->join('LEFT JOIN', 'user_modules_list AS ML', 'ML.id = user_role_module_permission.moduleId')
            ->join('LEFT JOIN', 'user_assignment', 'user_assignment.roleId = user_role_module_permission.roleId')
            ->where('user_assignment.userId = :userId', [':userId' => $userId])
            ->andWhere('user_assignment.`status` = :is_active', [':is_active' => 1])
            ->andWhere($action . ' = :' . $action, [':' . $action => 1])
            ->andWhere('ML.controller = :controller', [':controller' => $controller])
            ->one();

        // Checking if $permission is not null and the action key exists
        return $permission && isset($permission[$action]) ? $permission[$action] : false;
    }

}

?>
