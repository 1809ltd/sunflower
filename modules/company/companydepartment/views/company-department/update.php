<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companydepartment\models\CompanyDepartment */

$this->title = 'Update Company Department: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-department-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
