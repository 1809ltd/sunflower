<?php

namespace app\modules\finance\financereport\companyfinancials\balancesheet;

/**
 * balancesheet module definition class
 */
class balancesheet extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\companyfinancials\balancesheet\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
