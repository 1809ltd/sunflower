<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userassignment\models\UserAssignment */

$this->title = 'Create User Assignment';
$this->params['breadcrumbs'][] = ['label' => 'User Assignments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-assignment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
