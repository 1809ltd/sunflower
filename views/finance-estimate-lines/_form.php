<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimateLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-estimate-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'estimateId')->textInput() ?>

    <?= $form->field($model, 'itemRefId')->textInput() ?>

    <?= $form->field($model, 'itemRefName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'estimateLinesDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'estimateLinespack')->textInput() ?>

    <?= $form->field($model, 'estimateLinesQty')->textInput() ?>

    <?= $form->field($model, 'estimateLinesUnitPrice')->textInput() ?>

    <?= $form->field($model, 'estimateLinesAmount')->textInput() ?>

    <?= $form->field($model, 'estimateLinesTaxCodeRef')->textInput() ?>

    <?= $form->field($model, 'estimateLinesTaxamount')->textInput() ?>

    <?= $form->field($model, 'estimateLinesDiscount')->textInput() ?>

    <?= $form->field($model, 'estimateLinesCreatedAt')->textInput() ?>

    <?= $form->field($model, 'estimateLinesUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'estimateLinesDeletedAt')->textInput() ?>

    <?= $form->field($model, 'estimateLinesStatus')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
