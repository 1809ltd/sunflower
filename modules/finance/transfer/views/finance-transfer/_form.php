<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;

/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;

/*Dependant Drop*/
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\transfer\models\FinanceTransfer */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body finance-transfer-form">
  <div class="row">
    <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'tranferRefernce')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
      <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'tranferTxnDate')->widget(\yii\jui\DatePicker::class, [
                  //'language' => 'ru',
                  'options' => ['autocomplete'=>'off','class' => 'form-control'],
                  'inline' => false,
                  'dateFormat' => 'yyyy-MM-dd',
              ]); ?>
          </div>
      </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'tranferFromAccountRef')->widget(Select2::classname(), [
              'data' =>  ArrayHelper::map(FinanceAccounts::find()->where(['like', 'accountsPays', 'yes'])->all(),'id','fullyQualifiedName'),
              'language' => 'en',
              'options' => ['id'=>'tranferFromAccountRef','prompt'=>'Select From Account...'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ]);?>

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'transferCharges')->textInput(['value' => '0']) ?>

        </div>
    </div>
    <!-- <div class="col-sm-3">
        <div class="form-group">

        </div>
    </div> -->
  </div>
  <div class="row">
    <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'tranferToAccountRef')->widget(DepDrop::classname(), [
              'options'=>['id'=>'tranferToAccountRef'],
              'data' => [$model->tranferToAccountRef => $model->tranferToAccountRef],
              'type' => DepDrop::TYPE_SELECT2,
              'pluginOptions'=>[
                  'depends'=>['tranferFromAccountRef'],
                  'initialize' => true,
                  // 'initDepends'=>['customerId'],
                  'placeholder'=>'Select...',
                  'url'=>Url::to(['/finance/transfer/finance-transfer/transfereeaccount'])
              ]
          ]);?>

        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($modeltransferee, 'transferCharges')->textInput(['value' => '0']) ?>

        </div>
    </div>
    <!-- <div class="col-sm-3">
        <div class="form-group">

        </div>
    </div> -->
  </div>
  <div class="row">
    <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'tranferAmount')->textInput() ?>
        </div>
    </div>
      <div class="col-sm-6">
          <div class="form-group">
            <?php $status = ['1' => 'Cleared', '2' => 'Reconciled']; ?>
            <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']); ?>

          </div>
      </div>
  </div>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
