<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\DashboardEventAsset;

DashboardEventAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="multi_color">
<?php $this->beginBody() ?>

<!-- begin #pvr-container -->
<div id="pvr-container" class="pvr-container fade fixed_sidebar fixed_header">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-default navbar-fixed-top">
        <!-- begin container-fluid -->
        <div class="container-fluid">
            <!-- begin mobile sidebar expand / collapse button -->
            <div class="navbar-header">
                <a href="#" class="navbar-brand f-w-500">
                    <img src="http://sunflowertents.com/wp-content/uploads/2017/11/unnamed-1.png" alt="logo"> <span class="m-l-10">
                    <?= Html::encode(Yii::$app->name) ?></span></a>
                <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle p-0 m-r-5" data-toggle="collapse" data-target="#mega_menu">
                    <i class="ion ion-ios-keypad-outline l-h-34"></i>
                </button>
            </div>
            <!-- end mobile sidebar expand / collapse button -->

            <!-- begin navbar-collapse -->
            <div class="collapse navbar-collapse pull-left" id="top-navbar">
                <ul class="nav navbar-nav">
                    <li data-click="sidebar-minify">
                        <a href="javascript:void(0)">
                            <i class="ion ion-ios-keypad-outline"></i>
                        </a>
                    </li>
                    <li class="page_heading">
                        <h4>Dashboard v2</h4>
                    </li>
                </ul>
            </div>
            <!-- end navbar-collapse -->

            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right hidden-xs">
                <li id="toggleFullScreen" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"
                    title="Full Screen">
                    <a href="javascript:void(0)" class="">
                        <i class="ion ion-ios-monitor-outline"></i>
                    </a>
                </li>
                <li id="btn-search">
                    <a href="javascript:void(0)" class="">
                        <i class="ion ion-ios-search"></i>
                        <span class="label">5</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
                        <i class="ion ion-ios-bell-outline"></i>
                        <span class="label">3</span>
                    </a>
                    <ul class="dropdown-menu media-list pull-right animated fadeInDown">
                        <li class="dropdown-header">Notifications (3)</li>
                        <li class="media">
                            <a href="javascript:void(0)">
                                <div class="media-left">
                                    <img src="http://via.placeholder.com/128x128" class="media-object" alt=""/>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">Andrew</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <div class="text-muted f-s-11">25 minutes ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:void(0)">
                                <div class="media-left">
                                    <img src="http://via.placeholder.com/128x128" class="media-object" alt=""/>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">Need Action</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <div class="text-muted f-s-11">35 minutes ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:void(0)">
                                <div class="media-left"><i class="fa fa-plus media-object bg-green"></i></div>
                                <div class="media-body">
                                    <h6 class="media-heading"> New User Registered</h6>
                                    <div class="text-muted f-s-11">1 hour ago</div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown navbar-user">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="ion ion-ios-folder-outline m-r-5 v-a-m"></i>
                        <span class="hidden-xs">Andrew</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInDown">
                        <li class="arrow"></li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="badge badge-success pull-right m-t-3">2</span>
                                <i class="ion ion-ios-email-outline"></i> Message
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-ios-contact-outline"></i> Profile
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-ios-settings"></i> Setting
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-ios-locked-outline"></i> Lock Screen
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-log-out"></i> Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse pull-right" id="mega_menu">
                <ul class="nav navbar-nav b-r">
                    <li data-toggle="tooltip" data-placement="bottom" data-trigger="hover" class="hidden-xs hidden-sm hide"
                        title="Enter Peek Hours">
                        <div class="input-group input-group-sm width-300 m-t-13">
                            <span class="input-group-addon btn sm_bg_6">Peek Hours</span>
                            <input type="text" class="form-control" placeholder="Peek Hours" value="10:10 - 02:54">
                            <span class="input-group-addon btn sm_bg_6">Save</span>
                        </div>
                    </li>
                    <li class="dropdown dropdown-lg">
                        <a href="javascript:void(0)" class="dropdown-toggle"
                           data-toggle="dropdown">
                            <i class="ion ion-ios-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg">
                            <div class="row">
                                <div class="col-md-4 col-sm-4"><h4 class="dropdown-header">Lorem ipsum</h4>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6">
                                            <ul class="nav b-r">
                                                <li><a href="login.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Login v1</a>
                                                </li>
                                                <li><a href="login_v1.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Login v2</a>
                                                </li>
                                                <li><a href="registration.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    Registration</a></li>
                                                <li><a href="lock_screen.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Lock
                                                    Screen</a></li>
                                                <li><a href="coming_soon.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Coming
                                                    Soon</a></li>
                                                <li><a href="404.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> 404
                                                    v1</a></li>
                                                <li><a href="profile.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Profile</a>
                                                </li>
                                                <li><a href="cookie.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Cookie
                                                    v1</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <ul class="nav b-r">
                                                <li><a href="buttons.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Buttons</a>
                                                </li>
                                                <li><a href="spinner_buttons.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Spinner
                                                    Buttons</a></li>
                                                <li><a href="badges.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    Badges</a></li>
                                                <li><a href="typography.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    Typography</a></li>
                                                <li><a href="tabs.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Tabs &
                                                    Accordion</a></li>
                                                <li><a href="progress.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Progress
                                                    Bar</a></li>
                                                <li><a href="grid_system.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Grid
                                                    System</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4"><h4 class="dropdown-header">Lorem ipsum</h4>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-6">
                                            <ul class="nav b-r">
                                                <li><a href="date_time.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Date &
                                                    Time Pickers</a></li>
                                                <li><a href="color.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Color
                                                    Pickers</a></li>
                                                <li><a href="select2.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Select2
                                                    Dropdown</a></li>
                                                <li><a href="bootstrap_select.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    Bootstrap Select</a></li>
                                                <li><a href="list_box.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> List Box</a>
                                                </li>
                                                <li><a href="clipboard.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    Clipboard</a></li>
                                                <li><a href="tags.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    Bootstrap Tags</a></li>
                                                <li><a href="spin.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    Bootstrap Touchspin</a></li>
                                                <li><a href="session.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Session</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-xs-6">
                                            <ul class="nav b-r">
                                                <li><a href="javascript:void(0)"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i>
                                                    E-commerce</a></li>
                                                <li><a href="blog.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Blog</a>
                                                </li>
                                                <li><a href="file_manager.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> File
                                                    Manager</a></li>
                                                <li><a href="contact_app.html"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Contact
                                                    v1</a></li>
                                                <li><a href="javascript:void(0)"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Contact
                                                    v2</a></li>
                                                <li><a href="javascript:void(0)"><i
                                                        class="ion ion-ios-arrow-thin-right fa-fw fa-lg text-inverse"></i> Calendar</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4"><h4 class="dropdown-header">Lorem ipsum</h4>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul class="registered-users-list clearfix">
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Savory Posh
                                                        <small>Algerian</small>
                                                    </h4>
                                                </li>
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Ancient Caviar
                                                        <small>Korean</small>
                                                    </h4>
                                                </li>
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Marble Lungs
                                                        <small>Indian</small>
                                                    </h4>
                                                </li>
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Blank Bloke
                                                        <small>Japanese</small>
                                                    </h4>
                                                </li>
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Hip Sculling
                                                        <small>Cuban</small>
                                                    </h4>
                                                </li>
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Flat Moon
                                                        <small>Nepalese</small>
                                                    </h4>
                                                </li>
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Packed Puffs
                                                        <small>Malaysian&gt;</small>
                                                    </h4>
                                                </li>
                                                <li class="zoom_in"><a href="javascript:void(0)"> <img
                                                        src="http://via.placeholder.com/128x128" alt=""> </a>
                                                    <h4 class="username text-ellipsis"> Clay Hike
                                                        <small>Swedish</small>
                                                    </h4>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li id="map_screen" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"
                        title="Map Screen">
                        <a href="javascript:void(0)" class="">
                            <i class="ion ion-map"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- end header navigation right -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <div id="sidebar" class="sidebar">
        <!-- begin sidebar scrollbar -->
        <div data-scrollbar="true" data-height="100%">
            <!-- begin sidebar user -->
            <ul class="nav">
                <li class="nav-profile">
                    <div class="image">
                        <a href="javascript:void(0)">
                            <img src="http://via.placeholder.com/128x128" alt=""/>
                        </a>
                    </div>
                    <div class="info">
                        Andrew
                        <small class="m-b-15">Dispatcher - Sunflower Events</small>
                        <a href="booking_v1.html" class="user_icons" data-toggle="tooltip" title="Dashboard">
                            <i class="ion ion-ios-paper-outline"></i>
                        </a>
                        <a href="javascript:void(0)" class="user_icons" data-toggle="tooltip" title="Notification">
                            <i class="ion ion-ios-bell-outline"></i>
                        </a>
                        <a href="javascript:void(0)" class="user_icons" data-toggle="tooltip" title="Email">
                            <i class="ion ion-ios-email-outline"></i>
                        </a>
                        <a href="javascript:void(0)" class="user_icons" data-toggle="tooltip" title="Logout">
                            <i class="ion ion-log-out"></i>
                        </a>
                    </div>
                </li>
            </ul>
            <!-- end sidebar user -->
            <ul class="nav">
                <li class="nav-header">Set Up</li>
                <li class="has-sub">
                    <a href="javascript:void(0)">
                        <b class="caret pull-right"></b>
                        <i class="ion ion-cloud"></i>
                        <span>Masters</span>
                    </a>
                    <ul class="sub-menu">
                      <li><a href="index.php?r=company-details">Company Master</a></li>
                      <li><a href="index.php?r=company-department">Department Master</a></li>
                      <li class="has-sub">
                          <a href="javascript:void(0)">
                              <span>Company Storage Master</span>
                          </a>
                          <ul class="sub-menu">
                              <li><a href="index.php?r=company-site-details">Company Site</a></li>
                              <li><a href="index.php?r=company-site-location">Company Site Locations</a></li>
                          </ul>
                      </li>
                      <li class="has-sub">
                          <a href="javascript:void(0)">
                              <span>Inventory Master</span>
                          </a>
                          <ul class="sub-menu">
                              <li><a href="index.php?r=asset-registration">Asset Registration</a></li>
                              <li><a href="index.php?r=asset-preciation">Asset Valuation</a></li>
                          </ul>
                      </li>


                    </ul>
                </li>
                <li class="has-sub">
                    <a href="javascript:void(0)">
                        <b class="caret pull-right"></b>
                        <i class="ion ion-android-contacts"></i>
                        <span>Customer</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="index.php?r=customer-details">Customer Master</a></li>
                        <li><a href="vehicle.html">Vehicle</a></li>
                        <li><a href="driver.html">Driver</a></li>
                        <li><a href="traiff.html">Tariff</a></li>
                        <li><a href="dnd_list.html">DND List</a></li>
                    </ul>
                </li>
                <li class="has-sub">
              <a href="javascript:void(0)">
                  <b class="caret pull-right"></b>
                  <i class="ion ion-android-call"></i>
                  <span>Follow-up</span>
              </a>
              <ul class="sub-menu">
                  <li><a href="cancelled_trips.html">Cancelled Trips</a></li>
                  <li><a href="enquiries.html">Enquiries</a></li>
                  <li><a href="follow_up.html">Follow-up</a></li>
                  <li><a href="feedback_complaint.html">Feedback / Complaint</a></li>
              </ul>
          </li>
            </ul>
        </div>
        <!-- end sidebar scrollbar -->
    </div>
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin row -->
        <!-- begin row -->
        <div class="row">
            <div class="col-md-12">
              <?= Breadcrumbs::widget([
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
              ]) ?>
              <?= Alert::widget() ?>
              <?= $content ?>

            </div>
            <!-- end panel -->
        </div>
        <!-- end row -->

      <!--  <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
                <a class="sm-box sm_bodered_widget centered trend-in-corner xs" href="#">
                    <div class="label">FREE VEHICLE</div>
                    <div class="value">1,224</div>
                    <div class="trending trending-up"><span>11%</span></div>
                </a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 res-xs-m-t-20">
                <a class="sm-box sm_bodered_widget centered trend-in-corner xs" href="#">
                    <div class="label">RUNNING VEHICLE</div>
                    <div class="value">1,118</div>
                    <div class="trending trending-down"><span>14%</span></div>
                </a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 res-xs-m-t-20 res-sm-m-t-20">
                <a class="sm-box sm_bodered_widget centered trend-in-corner xs" href="#">
                    <div class="label">ALLOTTED VEHICLE</div>
                    <div class="value">1,384</div>
                    <div class="trending trending-up"><span>16%</span></div>
                </a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 res-xs-m-t-20 res-sm-m-t-20">
                <a class="sm-box sm_bodered_widget centered trend-in-corner xs" href="#">
                    <div class="label">BREAK VEHICLE</div>
                    <div class="value text-danger">1,634</div>
                    <div class="trending trending-up"><span>13%</span></div>
                </a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 res-xs-m-t-20 res-sm-m-t-20 res-md-m-t-20">
                <a class="sm-box sm_bodered_widget centered trend-in-corner xs" href="#">
                    <div class="label">ASSIGNED VEHICLE</div>
                    <div class="value">3,447</div>
                    <div class="trending trending-down"><span>02%</span></div>
                </a>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 res-xs-m-t-20 res-sm-m-t-20 res-md-m-t-20">
                <a class="sm-box sm_bodered_widget centered trend-in-corner xs" href="#">
                    <div class="label">DISABLED VEHICLE</div>
                    <div class="value">1,448</div>
                    <div class="trending trending-down"><span>24%</span></div>
                </a>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="ui-block">
                    <div class="widget sm_birthday_alert">
                        <div class="icons-block">
                            <svg class="olymp-cupcake-icon">
                                <use xlink:href="assets/img/svg/sprites/icons.svg#olymp-cupcake-icon"></use>
                            </svg>
                            <a href="javascript:void(0)" class="more">
                                <svg class="dots-icons">
                                    <use xlink:href="assets/img/svg/sprites/icons.svg#dots-icons"></use>
                                </svg>
                            </a>
                        </div>
                        <div class="con">
                            <div class="author-thumb">
                                <img src="http://via.placeholder.com/128x128" alt="author">
                            </div>
                            <p>Today is</p>
                            <a href="javascript:void(0)" class="h4 title">Andrew Heston’s
                                Birthday!</a>
                            <p>Leave her a message with your best wishes on her profile
                                page!</p>
                        </div>
                    </div>
                </div>

                <div class="ui-block">
                    <div class="widget sm_action">
                        <img src="http://sunflowertents.com/wp-content/uploads/2017/11/unnamed-1.png" alt="">
                        <div class="con">
                            <h4 class="title">PVR Tech Studio</h4>
                            <span>We Design Your Future!</span>
                            <a href="javascript:void(0)"
                               class="btn btn-outline-light btn-md">Register
                                Now!</a>
                        </div>
                    </div>
                </div>

                <div class="panel panel-pvr panel--style--1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                            <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                            <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                            <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                        </div>
                        <h4 class="panel-title">Latest Comments</h4>
                    </div>
                    <div class="panel-body">
                        <div class="sidebar-object m-b-0">
                            <div class="section-title section-title--style-1">
                                <h3 class="section-title-inner heading-sm strong-500">
                                    Latest comments
                                </h3>
                            </div>
                            <ul class="list-recent">
                                <li class="clearfix">
                                    <a href="javascript:void(0)" class="post-thumb w24">
                                        <img src="http://via.placeholder.com/128x128" alt="">
                                    </a>
                                    <span class="post-author">
                                                                    <a href="javascript:void(0)">Andrew Heston</a>
                                                                    <br>
                                                                    2 mins ago
                                                                </span>
                                    <span class="post-entry">
                                                                    Lorem Ipsum is simply dummy text of the printing.
                                                                </span>
                                </li>
                                <li class="clearfix">
                                    <a href="javascript:void(0)" class="post-thumb w24">
                                        <img src="http://via.placeholder.com/128x128" alt="">
                                    </a>
                                    <span class="post-author">
                                                                    <a href="javascript:void(0)">Michel Newton</a>
                                                                    <br>
                                                                    1 hr ago
                                                                </span>
                                    <span class="post-entry">
                                                                    Lorem Ipsum is simply dummy text of the printing.
                                                                </span>
                                </li>
                                <li class="clearfix">
                                    <a href="javascript:void(0)" class="post-thumb w24">
                                        <img src="http://via.placeholder.com/128x128" alt="">
                                    </a>
                                    <span class="post-author">
                                                                    <a href="javascript:void(0)">Mark Ruffalo</a>
                                                                    <br>
                                                                    4 hrs ago
                                                                </span>
                                    <span class="post-entry">
                                                                    Lorem Ipsum is simply dummy text of the printing.
                                                                </span>
                                </li>
                                <li class="clearfix">
                                    <a href="javascript:void(0)" class="post-thumb w24">
                                        <img src="http://via.placeholder.com/128x128" alt="">
                                    </a>
                                    <span class="post-author">
                                                                    <a href="javascript:void(0)">Catherine Weiss</a>
                                                                    <br>
                                                                    5 hrs ago
                                                                </span>
                                    <span class="post-entry">
                                                                    Lorem Ipsum is simply dummy text of the printing.
                                                                </span>
                                </li>
                                <li class="clearfix">
                                    <a href="javascript:void(0)" class="post-thumb w24">
                                        <img src="http://via.placeholder.com/128x128" alt="">
                                    </a>
                                    <span class="post-author">
                                                                    <a href="javascript:void(0)">Mark Ruffalo</a>
                                                                    <br>
                                                                    6 hrs ago
                                                                </span>
                                    <span class="post-entry">
                                                                    Lorem Ipsum is simply dummy text of the printing.
                                                                </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="d-md-flex justify-content-between p-25">
                        <div style="display: inline-block;" class="pull-left">
                            <h6 class="f-s-13 ttu f-w-600 tx-spacing-1">How GPS Engaged Our Users Daily</h6>
                            <p>Past <span data-count="true" data-number="30" id="dt_overall"></span> Days — Last
                                Updated May 10, 2018</p>
                        </div>
                        <div class="d-sm-flex pull-right">
                            <div>
                                <p class="m-b-5 ttu f-s-10 f-w-600">GPS Bandwidth</p>
                                <h4 class="f-w-700 m-b-0"><span data-count="true" data-number="1321"
                                                                id="br_1"></span>%</h4>
                                <span class="f-s-12 text-success">22.8% increased</span>
                            </div>
                            <div class="bd-sm-l pd-sm-l-20 mg-sm-l-20">
                                <p class="m-b-5 ttu f-s-10 f-w-600">GPS Data Transfer</p>
                                <h4 class="f-w-700 m-b-0"><span data-count="true" data-number="2038" id="pv"></span>%
                                </h4>
                                <span class="f-s-12 text-danger">54.65% decreased</span>
                            </div>
                            <div class="bd-sm-l pd-sm-l-20 mg-sm-l-20">
                                <p class="m-b-5 ttu f-s-10 f-w-600">GPS Traffic</p>
                                <h4 class="f-w-700 m-b-0">14:32</h4>
                                <span class="f-s-12 text-success">61.22% increased</span>
                            </div>
                        </div>
                    </div>
                    <div class="p-l-25 p-r-25 p-b-25">
                        <canvas id="sales_chart" height="300"
                                style="display: block; width: 100%; height: 300px;"></canvas>
                    </div>
                </div>

                <div class="row m-t-20">
                    <div class="col-lg-7">
                        <div class="bg-white">
                            <div class="widget-body no-padding todo-form">
                                <h5 class="todo-group-title">
                                    <i class="fa fa-warning"></i> High Priority Task
                                    <small class="num-of-tasks">2</small>
                                </h5>
                                <ul id="sortable1" class="todo">
                                    <li>
                                                                            <span class="handle">
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox"
                                                                                           name="checkbox-inline">
                                                                                    <i></i>
                                                                                </label>
                                                                            </span>
                                        <p>
                                            <strong>Ticket #34785</strong> -
                                            Dashboard Design issue
                                            <a href="javascript:void(0);"
                                               class="font-xs">More Details</a>
                                            <span class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span>
                                            <span class="date">Jun 15, 2018</span>
                                        </p>
                                    </li>
                                    <li>
                                                                            <span class="handle">
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox"
                                                                                           name="checkbox-inline">
                                                                                    <i></i>
                                                                                </label>
                                                                            </span>
                                        <p>
                                            <strong>Ticket #1347</strong> - 404 Page
                                            Design issue
                                            <small>(fix bug)</small>
                                            <a href="javascript:void(0);"
                                               class="font-xs">More Details</a>
                                            <span class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span>
                                            <span class="date">Jun 13, 2018</span>
                                        </p>
                                    </li>
                                </ul>
                                <h5 class="todo-group-title">
                                    <i class="fa fa-exclamation"></i> In Progress
                                    Tasks
                                    <small class="num-of-tasks">2</small>
                                </h5>
                                <ul id="sortable2" class="todo">
                                    <li>
                                                                            <span class="handle">
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox"
                                                                                           name="checkbox-inline">
                                                                                    <i></i>
                                                                                </label>
                                                                            </span>
                                        <p>
                                            <strong>Ticket #1347</strong> - Pricing
                                            Table
                                            <small>(fix bug)</small>
                                            <a href="javascript:void(0);"
                                               class="font-xs">More Details</a>
                                            <span class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span>
                                            <span class="date">Jun 13, 2018</span>
                                        </p>
                                    </li>
                                    <li>
                                                                            <span class="handle">
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox"
                                                                                           name="checkbox-inline">
                                                                                    <i></i>
                                                                                </label>
                                                                            </span>
                                        <p>
                                            <strong>Ticket #1347</strong> - 404 Page
                                            Design issue
                                            <small>(fix bug)</small>
                                            <a href="javascript:void(0);"
                                               class="font-xs">More Details</a>
                                            <span class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span>
                                            <span class="date">Jun 13, 2018</span>
                                        </p>
                                    </li>
                                </ul>

                                <h5 class="todo-group-title"><i
                                        class="fa fa-check"></i> Released Tasks
                                    <small class="num-of-tasks">2</small>
                                </h5>
                                <ul id="sortable3" class="todo">
                                    <li class="complete">
                                                                            <span class="handle hide">
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox"
                                                                                           name="checkbox-inline">
                                                                                    <i></i>
                                                                                </label>
                                                                            </span>
                                        <p>
                                            <strong>Ticket #1347</strong> - 404 Page
                                            Design issue
                                            <small>(fix bug)</small>
                                            <a href="javascript:void(0);"
                                               class="font-xs">More Details</a>
                                            <span class="text-muted">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span>
                                            <span class="date">Jun 13, 2018</span>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5 col-md-12 res-xs-m-t-20 res-md-m-t-20">
                        <div class="card text-white m-b-0 basic-gradient">
                            <div class="card-body p-20">
                                <div class="widget-active-user">
                                    <h5 class="p-b-10 b-b1 text-white">Highest Collection this month</h5>
                                    <div class="heading-1 f-s-45 text-white f-w-800">15,483</div>
                                    <h5 class="p-b-10 b-b1 text-white f-s-15">Request Per Minute</h5>
                                    <div id="users_online" class="text-center sparkline overflow--hidden">
                                        <canvas width="200" height="130"
                                                style="display: inline-block; width: 294px; height: 100px; vertical-align: top;"></canvas>
                                    </div>
                                    <h5 class="text-white m-t-10">Top active users</h5>
                                    <ul class="list-unstyled active-page-link">
                                        <li>
                                            <small>Andrew</small>
                                        </li>
                                        <li>
                                            <small>John Doe</small>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-pvr panel--style--1 m-t-20">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                            <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                            <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                            <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                        </div>
                        <h4 class="panel-title">Latest Comments</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Customer</th>
                                    <th>Products</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-right">Sub Total</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="nowrap">Andrew Heston</td>
                                    <td>
                                        <div class="cell-image-list">
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="status-pill green" data-title="Complete"
                                             data-toggle="tooltip" data-original-title=""
                                             title=""></div>
                                    </td>
                                    <td class="text-right">$478</td>
                                    <td class="text-right">$4,478</td>
                                </tr>
                                <tr>
                                    <td class="nowrap">Michel Newton</td>
                                    <td>
                                        <div class="cell-image-list">
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img-more">+ 5 more</div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="status-pill red" data-title="Cancelled"
                                             data-toggle="tooltip" data-original-title=""
                                             title=""></div>
                                    </td>
                                    <td class="text-right">$154</td>
                                    <td class="text-right">$1,154</td>
                                </tr>
                                <tr>
                                    <td class="nowrap">Mark Ruffalo</td>
                                    <td>
                                        <div class="cell-image-list">
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="status-pill green" data-title="Complete"
                                             data-toggle="tooltip" data-original-title=""
                                             title=""></div>
                                    </td>
                                    <td class="text-right">$478</td>
                                    <td class="text-right">$1,478</td>
                                </tr>
                                <tr>
                                    <td class="nowrap">John</td>
                                    <td>
                                        <div class="cell-image-list">
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="status-pill yellow" data-title="Pending"
                                             data-toggle="tooltip" data-original-title=""
                                             title=""></div>
                                    </td>
                                    <td class="text-right">$3,420</td>
                                    <td class="text-right">$5,420</td>
                                </tr>
                                <tr>
                                    <td class="nowrap">Catherine</td>
                                    <td>
                                        <div class="cell-image-list">
                                            <div class="cell-img"
                                                 style="background-image: url('http://via.placeholder.com/1980x1322')"></div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="status-pill green" data-title="Complete"
                                             data-toggle="tooltip" data-original-title=""
                                             title=""></div>
                                    </td>
                                    <td class="text-right">$856</td>
                                    <td class="text-right">$1,856</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        < end row -->
    </div>
    <!-- end #content -->

    <!-- begin search --
    <div class="search">
        <i id="btn-search-close" class="ion ion-ios-close-outline btn--search-close"></i>
        <form class="search__form" action="#">
            <input class="search__input" name="search" type="search" placeholder="Search.." autocomplete="off"
                   autocapitalize="off" spellcheck="false"/>
            <span class="search__info">Hit enter to search or ESC to close</span>
        </form>
        <div class="search__related">
            <div class="search__suggestion">
                <h3>May We Suggest?</h3>
                <p>#drone #funny #catgif #broken #lost #hilarious #good #red #blue #nono #why #yes #yesyes #aliens
                    #green</p>
            </div>
            <div class="search__suggestion">
                <h3>Is It This?</h3>
                <p>#good #red #hilarious #blue #nono #why #yes #yesyes #aliens #green #drone #funny #catgif #broken
                    #lost</p>
            </div>
            <div class="search__suggestion">
                <h3>Needle, Where Art Thou?</h3>
                <p>#broken #lost #good #red #funny #hilarious #catgif #blue #nono #why #yes #yesyes #aliens #green
                    #drone</p>
            </div>
        </div>
    </div>
    <-- end #search -->

    <!-- begin scroll to top btn -->
    <a href="javascript:void(0)" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade"
       data-click="scroll-top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
