<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AssetPreciation */

$this->title = 'Create Asset Preciation';
$this->params['breadcrumbs'][] = ['label' => 'Asset Preciations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-preciation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
