<!-- <div class="expense-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Expense Summary';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <div class="customerreport-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<!-- Main content -->
<!-- Search Date And the rest -->

<?php
if (!empty($allExpense)) {
  // code...
// print_r($customer);
// echo $customer[0]["customerCompanyName"];

// die();
  ?>

  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">

      <div class="col-sm-8 invoice-col">
        <h6>
          <img class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
        </h6>
      </div>
      <!-- <div class="col-sm-4 invoice-col">
        <p></p>
      </div> -->
      <div class="col-sm-4 invoice-col">

      </div>
      <!-- /.col -->
     </div>


     <!-- /.row -->
       <!-- info row -->
       <div class="row invoice-info">
         <!-- /.col -->
         <div class="col-sm-4 invoice-col">
           <p>
           <address>
             <strong>
               <?= $companydetails->companyName?>,</strong><br/>
               <?= $companydetails->companyAddress?><br/>
               <?= $companydetails->companyPostal?><br/>
                  Phone: +254 (0) 722 790632<br/>
                  Email: info@sunflowertents.com
           </address>
         </p>
         </div>
         <!-- /.col -->
       </div>
       <!-- /.row -->

         <!-- title row -->
         <div class="row">
           <div class="col-xs-12">
             <p class="page-header">
               <i class="fa fa-globe"></i> Expense Based on Invoces from Supppliers,Site Expense and Office Requstions:
               <small class="pull-right"></small>
             </p>
             <h5 class="page-header">
               <i class="fa fa-globe"></i>From <?= Yii::$app->formatter->asDate($startDate, 'long');?> to <?= Yii::$app->formatter->asDate($endDate, 'long');?>
             </h5>
           </div>
           <!-- /.col -->
         </div>
     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-bordered table-striped">
           <thead>
             <tr>
               <th>Invoice Date:</th>
               <th style="text-align: center;">Invoice No:</th>
               <th>Description:</th>
               <th style="text-align: right;">Sub Total</th>
               <th style="text-align: right;">V.A.T</th>
               <th style="text-align: right;">W.H.T</th>
               <th style="text-align: right;">Other Tax</th>
               <th style="text-align: right;">Payments</th>
               <th style="text-align: right;">Balance</th>
           </tr>
           </thead>
             <tbody>
               <?php
               // Get all the specific Invoice

               $Subtoalm=0;
               $VATtotalm=0;
               $WHTtotalm=0;
               $taxtotalm=0;
               $paymenttotalm=0;

               foreach ($allExpense as $allExpense) {
                 // code...
                 $Subtoal=0;
                 $VATtotal=0;
                 $WHTtotal=0;
                 $taxtotal=0;
                 // print_r($allExpense->getAttributes());
                 $expenseItems=$allExpense->getexpenseItems($allExpense->transactionId,$allExpense->status,$allExpense->referenceTable);

                 ?>
                 <tr>
                   <td><?= Yii::$app->formatter->asDate($allExpense["transactionDate"], 'long');?></td>

                   <td style="text-align: center;"><?= $allExpense->referenceCode;?></td>
                   <td><strong>Supplier : </strong><?= $allExpense->recipientName;?><br/><strong>Details: </strong>: <?= $allExpense->tranctionDescription;?></td>

                     <?php
                     foreach ($expenseItems as $expenseItems) {
                       // code...
                       if (($allExpense->referenceTable=="finance_office_requstion_form")) {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["amont"];

                       }elseif ($allExpense->referenceTable=="finance_lead_requstion_form") {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["amont"];

                       }elseif ($allExpense->referenceTable=="finance_event_requstion_form") {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["amont"];

                       }elseif ($allExpense->referenceTable=="finance_bills") {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["biilsLinesAmount"];

                         if ($expenseItems["billLinesTaxCodeRef"]== 0.16) {
                           // code...
                           $VATtotal=$VATtotal+$expenseItems["biilsLinesTaxAmount"];

                         }elseif ($expenseItems["billLinesTaxCodeRef"]== 0.05) {
                           // code...
                           $WHTtotal=$WHTtotal+$expenseItems["biilsLinesTaxAmount"];

                         } else {
                           // code...
                           $taxtotal=$taxtotal+$expenseItems["biilsLinesTaxAmount"];
                         }

                       }else {
                         // code...
                         $Subtoal=$Subtoal+0;
                         $VATtotal=$VATtotal+0;
                         $WHTtotal=$WHTtotal+0;
                         $taxtotal=$taxtotal+0;
                       }

                     }
                     // To get the overal sub total;
                     $Subtoalm+=$Subtoal;

                     // To get the overal VAT  total;
                     $VATtotalm+=$VATtotal;

                     // To get the overal VAT  total;
                     $WHTtotalm+=$WHTtotal;

                     // To get the overal Total Tax  total;
                     $taxtotalm+=$taxtotal;
                     ?>
                   <!-- </td> -->
                   <td style="text-align: right;"><?= number_format($Subtoal,2) ;?></td>
                   <td style="text-align: right;"><?= number_format($VATtotal,2) ;?></td>
                   <td style="text-align: right;"><?= number_format($WHTtotal,2) ;?></td>
                   <td style="text-align: right;"><?= number_format($taxtotal,2) ;?></td>
                   <td style="text-align: right;">

                     <?php
                     $malipo=$allExpense->allPayments($allExpense->transactionId,$allExpense->referenceTable);

                     $paymenttotalm+=$malipo;

                     $malipoamount = preg_replace(
                        '/(-)([\d\.\,]+)/ui',
                        '($2)',
                        number_format($malipo,2,'.',',')
                    );

                     echo $malipoamount;

                     ?>
                  </td>
                  <td style="text-align: right;">
                    <?php
                    $bal = ($Subtoal+$VATtotal+$WHTtotal+$taxtotal)-$malipo;
                    echo number_format($bal,2);
                    ?>
                  </td>
                </tr>
                <?php
              }
              // Bracjets if the figure is negatove
            $Subtoalmamount = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($Subtoalm,2,'.',',')
           );
           // Brackets if value is negative
           $VATtotalmamount = preg_replace(
              '/(-)([\d\.\,]+)/ui',
              '($2)',
              number_format($VATtotalm,2,'.',',')
          );
          // Brackets if value is negative
          $WHTtotalmamount = preg_replace(
             '/(-)([\d\.\,]+)/ui',
             '($2)',
             number_format($WHTtotalm,2,'.',',')
         );
         // Brackets if value is negative
         $taxtotalmamount = preg_replace(
            '/(-)([\d\.\,]+)/ui',
            '($2)',
            number_format($taxtotalm,2,'.',',')
        );
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <br>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
            <?php
            // $model->invoiceFooter; ?>
          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <div class="col-md-8">
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due  <strong>Ksh
            <?php

            // Brackets if value is negative
            $Due = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm-$paymenttotalm,2,'.',',')
           );

            echo $Due ;?>
          </strong></p>

        </br>
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Currency:</th>
              <th class="text-right">(Ksh)</th>
            </tr>
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td class="text-right"><?= $Subtoalmamount?></td>
            </tr>
            <tr>
              <th>VAT</th>
              <td class="text-right"><?= $VATtotalmamount ;?></td>
            </tr>
            <tr>
              <th>WHT</th>
              <td class="text-right"><?= $WHTtotalmamount?></td>
            </tr>
            <tr>
              <th>Other Tax</th>
              <td class="text-right"><?= $taxtotalmamount?></td>
            </tr>
            <tr>
              <th>Total:</th>
              <td class="text-right"><?php
              // Brackets if value is negative
                $totalmamount = preg_replace(
                   '/(-)([\d\.\,]+)/ui',
                   '($2)',
                   number_format($Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm,2,'.',',')
               );

                echo $totalmamount ;?>
              </td>
            </tr>
            <tr>
              <th>Total Payment:</th>
              <td class="text-right">
                  <?php

                  // Brackets if value is negative
                  $totalPaymentmamount = preg_replace(
                     '/(-)([\d\.\,]+)/ui',
                     '($2)',
                     number_format($paymenttotalm,2,'.',',')
                 );

                  echo $totalPaymentmamount ;?>
                </td>
              </tr>

            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  <?php
  } else {
    // code...

    echo "No Expense were found Here";
  }
  ?>
