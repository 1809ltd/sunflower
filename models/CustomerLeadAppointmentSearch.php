<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerLeadAppointment;

/**
 * CustomerLeadAppointmentSearch represents the model behind the search form of `app\models\CustomerLeadAppointment`.
 */
class CustomerLeadAppointmentSearch extends CustomerLeadAppointment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'userId'], 'integer'],
            [['leadAppointSubject', 'leadAppointStart', 'leadAppointEnd', 'leadAppointStatus', 'leadAppointNotes', 'leadAppointCreatedAt', 'leadAppointLastUpdatedAt', 'leadAppointDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerLeadAppointment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'leadId' => $this->leadId,
            'leadAppointStart' => $this->leadAppointStart,
            'leadAppointEnd' => $this->leadAppointEnd,
            'leadAppointStatus' => $this->leadAppointStatus,
            'leadAppointCreatedAt' => $this->leadAppointCreatedAt,
            'leadAppointLastUpdatedAt' => $this->leadAppointLastUpdatedAt,
            'leadAppointDeletedAt' => $this->leadAppointDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'leadAppointSubject', $this->leadAppointSubject])
            ->andFilterWhere(['like', 'leadAppointNotes', $this->leadAppointNotes]);

        return $dataProvider;
    }
}
