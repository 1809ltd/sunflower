<?php

namespace app\modules\inventory\assetmovement;

/**
 * assetmovement module definition class
 */
class assetmovement extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\assetmovement\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Asset Check out Sub Module*/
          'assetcheckout' => [
              'class' => 'app\modules\inventory\assetmovement\assetcheckout\assetcheckout',
          ],
          'delivery' => [
              'class' => 'app\modules\inventory\assetmovement\delivery\delivery',
          ],

        ];

        // custom initialization code goes here
    }
}
