<?php

namespace app\modules\customer\customerdetails\models;
/*Getting Event Details */
use app\modules\event\eventInfo\models\EventDetails;
/*User details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "customer_details".
 *
 * @property int $id
 * @property string $customerNumber
 * @property string $customerFullname
 * @property string $customerCompanyName
 * @property string $customerKraPin
 * @property string $customerDisplayName
 * @property string $customerPrimaryPhone
 * @property string $customerPrimaryEmail
 * @property int $customerstatus
 * @property string $customercity
 * @property string $customerPhyscialAdress
 * @property string $customerCounty
 * @property string $customerCountry
 * @property string $customerAddress
 * @property string $customerCreateTime
 * @property string $customerUpdatedAt
 * @property string $customerDeleteAt
 * @property int $createdBy
 * @property int $userId
 *
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property CustomerLead[] $customerLeads
 * @property EventDetails[] $eventDetails
 * @property EventTransport[] $eventTransports
 * @property EventWorkplan[] $eventWorkplans
 * @property FinanceCreditMemo[] $financeCreditMemos
 * @property FinanceEstimate[] $financeEstimates
 * @property FinanceInvoice[] $financeInvoices
 * @property FinanceOffsetMemo[] $financeOffsetMemos
 */
class CustomerDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerNumber', 'customerCompanyName', 'customerDisplayName', 'customerstatus'], 'required'],
            [['customerstatus', 'createdBy', 'userId'], 'integer'],
            [['customerCreateTime', 'customerUpdatedAt', 'customerDeleteAt', 'customerCreateTime', 'createdBy', 'userId'], 'safe'],
            [['customerNumber', 'customerCompanyName', 'customerKraPin', 'customerDisplayName'], 'string', 'max' => 50],
            [['customerFullname', 'customerPrimaryPhone'], 'string', 'max' => 25],
            [['customerPrimaryEmail', 'customercity', 'customerCounty', 'customerCountry', 'customerAddress'], 'string', 'max' => 100],
            [['customerPhyscialAdress'], 'string', 'max' => 250],
            [['customerNumber'], 'unique'],
            [['customerDisplayName'], 'unique'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'customerNumber' => 'Customer Number',
          'customerFullname' => 'Contact Fullname',
          'customerCompanyName' => 'Company Name',
          'customerKraPin' => 'Kra Pin',
          'customerDisplayName' => 'Display Name',
          'customerPrimaryPhone' => 'Primary Phone',
          'customerPrimaryEmail' => 'Primary Email',
          'customerstatus' => 'status',
          'customercity' => 'Town',
          'customerPhyscialAdress' => 'Physcial Adress',
          'customerCounty' => 'County',
          'customerCountry' => 'Country',
          'customerAddress' => 'Postal Address',
          'customerCreateTime' => 'Create Time',
          'customerUpdatedAt' => 'Updated At',
          'customerDeleteAt' => 'Delete At',
          'createdBy' => 'Created By',
          'userId' => 'Last Modify',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLeads()
    {
        return $this->hasMany(CustomerLead::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDetails()
    {
        return $this->hasMany(EventDetails::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventTransports()
    {
        return $this->hasMany(EventTransport::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventWorkplans()
    {
        return $this->hasMany(EventWorkplan::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemos()
    {
        return $this->hasMany(FinanceCreditMemo::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEstimates()
    {
        return $this->hasMany(FinanceEstimate::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoices()
    {
        return $this->hasMany(FinanceInvoice::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOffsetMemos()
    {
        return $this->hasMany(FinanceOffsetMemo::className(), ['customerId' => 'id']);
    }
    /*Generate Customer number */
    public function getCustomerNumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(customerNumber) AS number from customer_details where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number = "CUST-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "CUST-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "CUST-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }

    public function getCustomerListEvent($id) {
      // code...
      return EventDetails::find()
        ->where(['and', "customerId=$id"])
        ->all();
        // ->indexBy('id')->column();

    }
}
