<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Estimate Lines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-estimate-lines-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Estimate Lines', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'estimateId',
            'itemRefId',
            'itemRefName',
            'estimateLinesDescription:ntext',
            //'estimateLinespack',
            //'estimateLinesQty',
            //'estimateLinesUnitPrice',
            //'estimateLinesAmount',
            //'estimateLinesTaxCodeRef',
            //'estimateLinesTaxamount',
            //'estimateLinesDiscount',
            //'estimateLinesCreatedAt',
            //'estimateLinesUpdatedAt',
            //'estimateLinesDeletedAt',
            //'estimateLinesStatus',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
