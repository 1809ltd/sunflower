<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<div class="finance-invoice-lines-view">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'invoiceId',
            // 'itemRefId',
            'itemRefName',
            'invoiceLinesDescription:ntext',
            'invoiceLinespack',
            'invoiceLinesQty',
            'invoiceLinesUnitPrice',
            // 'invoiceLinesTaxCodeRef',
            'invoiceLinesTaxamount',
            'invoiceLinesDiscount',
            'invoiceLinesAmount',
            //'invoiceLinesCreatedAt',
            //'invoiceLinesUpdatedAt',
            //'invoiceLinesDeletedAt',
            //'invoiceLinesStatus',
            //'userId',

        ],
    ]); ?>

</div>
