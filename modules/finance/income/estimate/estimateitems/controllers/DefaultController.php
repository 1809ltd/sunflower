<?php

namespace app\modules\finance\income\estimate\estimateitems\controllers;

use yii\web\Controller;

/**
 * Default controller for the `estimateitems` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
