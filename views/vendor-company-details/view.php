<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VendorCompanyDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vendor Company Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="vendor-company-details-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vendorCompanyName',
            'vendorKraPin',
            'vendorContactPersonFullName',
            'contactMobilePhone',
            'vendorPhone',
            'contactEmail:email',
            'vendoremail:email',
            'vendorCompanyAddress',
            'vendorCompanySuite',
            'vendorCompanyCity',
            'vendorCompanyCounty',
            'vendorCompanyPostal',
            'vendorCompanyCountry',
            'vendorCompanyLogo:ntext',
            'vendorTwitter',
            'vendorFb',
            'vendorIg',
            'vendorType',
            'vendorCompanyStatus',
            'vendorCompanyTimestamp',
            'vendorCompanyupdatedAt',
            'vendorCompanyDeletedAt',
            'companyUserId',
        ],
    ]) ?>

</div>
