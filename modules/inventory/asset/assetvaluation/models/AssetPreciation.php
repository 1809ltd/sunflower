<?php

namespace app\modules\inventory\asset\assetvaluation\models;

use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
use app\modules\company\companydetails\models\CompanyDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "asset_preciation".
 *
 * @property int $id
 * @property int $assetId
 * @property int $companyId
 * @property double $preciableCost
 * @property double $preciationSalvageValue
 * @property double $assetLifeMonths
 * @property string $preciationDateAcquired
 * @property string $preciationDateAcquiredDepreciationMethod
 * @property string $preciationStatus
 * @property string $preciationTimestamp
 * @property string $preciationUpdatedAt
 * @property string $preciationDeletedAt
 * @property int $userId
 */
class AssetPreciation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_preciation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'companyId', 'preciableCost', 'preciationSalvageValue', 'assetLifeMonths', 'preciationDateAcquired', 'preciationDateAcquiredDepreciationMethod', 'preciationStatus', 'userId'], 'required'],
            [['assetId', 'companyId', 'userId'], 'integer'],
            [['preciableCost', 'preciationSalvageValue', 'assetLifeMonths'], 'number'],
            [['preciationDateAcquired', 'preciationTimestamp', 'preciationUpdatedAt', 'preciationDeletedAt'], 'safe'],
            [['preciationDateAcquiredDepreciationMethod', 'preciationStatus'], 'string', 'max' => 255],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['companyId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyDetails::className(), 'targetAttribute' => ['companyId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Preciation ',
            'assetId' => 'Asset Name*',
            'companyId' => 'Company*',
            'preciableCost' => 'Rate %*',
            'preciationSalvageValue' => 'Salvage Value*',
            'assetLifeMonths' => 'Life Months*',
            'preciationDateAcquired' => 'Date Acquired*',
            'preciationDateAcquiredDepreciationMethod' => 'Depreciation Method*',
            'preciationStatus' => 'Status',
            'preciationTimestamp' => 'Preciation Timestamp',
            'preciationUpdatedAt' => 'Preciation Updated At',
            'preciationDeletedAt' => 'Preciation Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(CompanyDetails::className(), ['id' => 'companyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
  }
