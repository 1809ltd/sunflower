<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_transport".
 *
 * @property int $id
 * @property int $eventId
 * @property int $outsourced
 * @property int $assetId
 * @property string $numberPlate
 * @property int $vendorId
 * @property int $delivery
 * @property int $collection
 * @property string $deliveryTime
 * @property string $collectionTime
 * @property int $status
 * @property int $userId
 * @property int $approvedBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property EventTransport $event
 * @property EventTransport[] $eventTransports
 * @property AssetRegistration $asset
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 * @property UserDetails $approvedBy0
 */
class EventTransport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_transport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eventId', 'outsourced', 'status', 'userId'], 'required'],
            [['eventId', 'outsourced', 'assetId', 'vendorId', 'delivery', 'collection', 'status', 'userId', 'approvedBy'], 'integer'],
            [['deliveryTime', 'collectionTime', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['numberPlate'], 'string', 'max' => 50],
            [['eventId'], 'exist', 'skipOnError' => true, 'targetClass' => EventTransport::className(), 'targetAttribute' => ['eventId' => 'id']],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eventId' => 'Event ID',
            'outsourced' => 'Outsourced',
            'assetId' => 'Asset ID',
            'numberPlate' => 'Number Plate',
            'vendorId' => 'Vendor ID',
            'delivery' => 'Delivery',
            'collection' => 'Collection',
            'deliveryTime' => 'Delivery Time',
            'collectionTime' => 'Collection Time',
            'status' => 'Status',
            'userId' => 'User ID',
            'approvedBy' => 'Approved By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventTransport::className(), ['id' => 'eventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventTransports()
    {
        return $this->hasMany(EventTransport::className(), ['eventId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }
}
