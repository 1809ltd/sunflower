<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\EventOrderfromListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Orderfrom Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-orderfrom-list-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Orderfrom List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'orderId',
            'itemid',
            'details:ntext',
            'outsourced',
            //'vendorId',
            //'quantity',
            //'status',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',
            //'userId',
            //'approvedBy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
