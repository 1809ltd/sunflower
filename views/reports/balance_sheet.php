<?php

use yii\helpers\Html;
use app\models\AllTransactions;
use app\models\AgedReceivables;
use app\models\AgePayables;
/* @var $this yii\web\View */

?>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?= date('Y-m-d'); ?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>
           <strong>SUNFLOWER EVENTS, Income Statement</strong><br/>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">


       </div>
       <!-- /.col -->
     </div>
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>
           <b>As of: <?= date('Y-m-d H:i:s'); ?></b><br>


         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

         <br>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-hover">
           <thead>
           </thead>
           <tbody>

             <!--  Revenue-->
             <?php

                $connection = Yii::$app->getDb();
                $command1 = $connection->createCommand("SELECT
															all_transactions.accountParentId,
															all_transactions.accountsclassfication,
															all_transactions.accountId,
															all_transactions.accountName,
															finance_accounts.accountsCurrentBalance,
															Sum(all_transactions.dr_amount),
															Sum(all_transactions.cr_amount), finance_accounts.accountsCurrentBalance+
															Sum( all_transactions.dr_amount ) -
																Sum( all_transactions.cr_amount ) AS accountBalance,
															all_transactions.transactionClassification,
															all_transactions.transactionCategory
															FROM
															all_transactions
															LEFT JOIN finance_accounts ON all_transactions.accountId = finance_accounts.id
															WHERE
															all_transactions.transactionClassification LIKE 'Invoice Payment' OR
															all_transactions.transactionClassification LIKE 'office expense Payment' OR
															all_transactions.transactionClassification LIKE 'Lead expense Payment' OR
															all_transactions.transactionClassification LIKE 'Bill Payment expense'
															GROUP BY
															all_transactions.accountId,finance_accounts.id
															ORDER BY
															all_transactions.accountParentId ASC
															");

                $bankbalances = $command1->query();

              ?>
              	 <table class="table table table-hover">
                  <tr>A. BANK ACCOUNTS</tr>
                  <tr>
                    <th style="width:75%">Accounts:</th>
                    <th>Balance</th>
                  </tr>
                  	<?php	
			             $balanceTT=0;
			             foreach ($bankbalances as $account) {
			               // code...
			             	 // print_r($account['accountBalance']);die();
			               $balanceTT+= $account['accountBalance'];
			               ?>
			               <tr>
		                      <td><?= strtoupper($account['accountName']);?></td>
		                      <td><?= number_format($account['accountBalance'],2);?></td>
		                    </tr>
			               <?php
			             }
			        ?>
                   <tr></tr>
                  <tr>
                    <th>Total Account Balances :</th>
                    <td><strong>Ksh <?= number_format($balanceTT,2);?></strong></td>
                  </tr>
                </table>
                <table class="table table table-hover">
                  <tr>B. ACCOUNT RECEIVABLES</tr>
                  <tr>
                    <th style="width:75%">Accounts:</th>
                    <th>Balance</th>
                  </tr>
                  	<?php
			             $agedreceiables= AgedReceivables::find()->all();	
			             $SumTT=0;
			             foreach ($agedreceiables as $agedreceiable) {
			               // code...
			               $agedreceiable->customerId;
			               $SumTT+= $agedreceiable->Total;
			               ?>
			               <tr>
		                      <td><?= strtoupper($agedreceiable->receivables);?></td>
		                      <td><?= number_format($agedreceiable->Total,2);?></td>
		                    </tr>
			               <?php
			             }
			        ?>
                   <tr></tr>
                  <tr>
                    <th>Total Account Receivables:</th>
                    <td><strong>Ksh <?= number_format($SumTT,2);?></strong></td>
                  </tr>
                </table>

                <table class="table table table-hover">
                  <tr>C. OTHER CURRENT ASSETS</tr>
                  <tr>
                    <th style="width:75%">Accounts:</th>
                    <th>Balance</th>
                  </tr>
                  	
                   <tr></tr>
                  <tr>
                    <th>Total Other Current Assets:</th>
                    <td><strong>Ksh <?= number_format(0,2);?></strong></td>
                  </tr>
                </table>

                <table class="table table table-hover">
                  <tr>D. FIXED ASSETS</tr>
                  <tr>
                    <th style="width:75%">Accounts:</th>
                    <th>Balance</th>
                  </tr>
                  	
                   <tr></tr>
                  <tr>
                    <th>Total Fixed Assets:</th>
                    <td><strong>Ksh <?= number_format(0,2);?></strong></td>
                  </tr>

                  <tr></tr>
                  <tr>
                    <th>Total Assets:</th>
                    <td><strong>Ksh <?= number_format($SumTT + $balanceTT,2);?></strong></td>
                  </tr>
                </table>

                <table class="table table table-hover">
                  <tr>E. CURRENT LIABILITIES</tr>
                  <tr>
                    <th style="width:75%">Accounts:</th>
                    <th>Balance</th>
                  </tr>
                  <tr><td colspan="2"><strong>ACCOUNT PAYABLES</strong></td></tr>
                  	<?php
			             $agedpayables= AgePayables::find()->all();	
			             $SumPT=0;
			             foreach ($agedpayables as $payables) {
			               // code...
			               $payables->vendorId;
			               $SumPT+= $payables->Total;
			               ?>
			               <tr>
		                      <td><?= strtoupper($payables->payables);?></td>
		                      <td><?= number_format($payables->Total,2);?></td>
		                    </tr>

			               <?php
			             }
			        ?>
			        <tr><td colspan="2"><strong>TAX PAYABLES</strong></td></tr>
			        <tr>
                      <td>VAT Payable</td>
                      <td><?= number_format(0,2);?></td>
                    </tr>
                    <tr>
                      <td>WHT Payable</td>
                      <td><?= number_format(0,2);?></td>
                    </tr>
                    
                   <tr></tr>
                  <tr>
                    <th>Total Current Liabilities:</th>
                    <td><strong>Ksh <?= number_format($SumPT,2);?></strong></td>
                  </tr>
                </table>


                <table class="table table table-hover">
                  <tr>CAPITAL / EQUITIES</tr>
                  <tr>
                    <th style="width:75%">Accounts:</th>
                    <th>Balance</th>
                  </tr>
                  	<tr>
                  	 	<td>OWNER INVESTMENTS/DRAWINGS</td>
                  	 	<td>Ksh. 0</td>
                  	 </tr>
                  	 <tr>
                  	 	<td>RETAINED EARNINGS B/F</td>
                  	 	<td>Ksh. 0</td>
                  	 </tr>
                  	 <tr>
                  	 	<td>CURRENT PROFIT</td>
                  	 	<td>Ksh. 0</td>
                  	 </tr>
                  	 <tr>
                  	 	<td>RETAINED EARNINGS C/F </td>
                  	 	<td>Ksh. 0</td>
                  	 </tr>

                   <tr></tr>
                
                </table>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>

            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
