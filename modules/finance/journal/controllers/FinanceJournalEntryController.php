<?php

namespace app\modules\finance\journal\controllers;

use Yii;
use app\modules\finance\journal\models\FinanceJournalEntry;
use app\modules\finance\journal\models\FinanceJournalEntrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\modules\finance\journal\models\FinanceJournalEntryLines;
use app\modules\finance\journal\models\FinanceJournalEntryLinesSearch;


/**
 * FinanceJournalEntryController implements the CRUD actions for FinanceJournalEntry model.
 */
class FinanceJournalEntryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function beforeAction($event){
        if(Yii::$app->Permission->getPermission())
            return parent::beforeAction($event);
        else
          // $this->redirect(\yii\helpers\Url::to(['/site/permission']));
            $this->redirect(['/site/permission']);
    }

    /**
     * Lists all FinanceJournalEntry models.
     * @return mixed
     */
    public function actionIndex()
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new FinanceJournalEntrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceJournalEntry model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
          // $this->layout='addformdatatablelayout';
        $this->layout = '@app/views/layouts/addformdatatablelayout';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceJournalEntry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //
    //     // $this->layout='addformdatatablelayout';
    //     $this->layout = '@app/views/layouts/addformdatatablelayout';
    //
    //     $model = new FinanceJournalEntry();
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }
    //
    //     return $this->render('create', [
    //         'model' => $model,
    //     ]);
    // }
    //
    // /**
    //  * Updates an existing FinanceJournalEntry model.
    //  * If update is successful, the browser will be redirected to the 'view' page.
    //  * @param integer $id
    //  * @return mixed
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    // public function actionUpdate($id)
    // {
    //
    //     // $this->layout='addformdatatablelayout';
    //     $this->layout = '@app/views/layouts/addformdatatablelayout';
    //
    //     $model = $this->findModel($id);
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }
    //
    //     return $this->render('update', [
    //         'model' => $model,
    //     ]);
    // }



    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new FinanceJournalEntry();

        $modelFinanceJournalEntryLiness = [new FinanceJournalEntryLines];

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Geting the time created Timestamp
          $model->createdate = date('Y-m-d H:i:s');

          //Getting user Log in details

          $model->preparedby = Yii::$app->user->identity->id;

          //Generating Estimate Number

          $JouEnty=$model->getJouEntynumber();
          $model->journalEntryNo= $JouEnty;


          $model->save();

          //after Saving the Modules roles Details

          $modelFinanceJournalEntryLiness = Model::createMultiple(FinanceJournalEntryLines::classname());
          Model::loadMultiple($modelFinanceJournalEntryLiness, Yii::$app->request->post());


          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelFinanceJournalEntryLiness) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelFinanceJournalEntryLiness as $modelFinanceJournalEntryLines) {

                        $modelFinanceJournalEntryLines->journalId = $model->id;
                        $modelFinanceJournalEntryLines->createAt = $model->createdate;
                        $modelFinanceJournalEntryLines->userId = $model->preparedby;
                        $modelFinanceJournalEntryLines->status = $model->status;


                          if (! ($flag = $modelFinanceJournalEntryLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }

          // return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('create', [
              'model' => $model,
              'modelFinanceJournalEntryLiness' => (empty($modelFinanceJournalEntryLiness)) ? [new FinanceJournalEntryLines] : $modelFinanceJournalEntryLiness
          ]);
        }

    }

    /**
     * Updates an existing FinanceJournalEntry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);


         $modelFinanceJournalEntryLiness = $this->getFinanceJournalEntryLines($model->id);

         if ($model->load(Yii::$app->request->post())) {
           // code...

           // //Getting user Log in details

           $model->save();

           //after Saving the customer Details

           $oldIDs = ArrayHelper::map($modelFinanceJournalEntryLiness, 'id', 'id');
           $modelFinanceJournalEntryLiness = Model::createMultiple(FinanceJournalEntryLines::classname(), $modelFinanceJournalEntryLiness);
           Model::loadMultiple($modelFinanceJournalEntryLiness, Yii::$app->request->post());
           $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelFinanceJournalEntryLiness, 'id', 'id')));

           // ajax validation
           if (Yii::$app->request->isAjax) {
               Yii::$app->response->format = Response::FORMAT_JSON;
               return ArrayHelper::merge(
                   ActiveForm::validateMultiple($modelFinanceJournalEntryLiness),
                   ActiveForm::validate($model)
               );
           }

           // validate all models
           $valid = $model->validate();
           $valid = Model::validateMultiple($modelFinanceJournalEntryLiness) && $valid;

           if ($valid) {
               $transaction = \Yii::$app->db->beginTransaction();
               try {
                   if ($flag = $model->save(false)) {
                       if (! empty($deletedIDs)) {
                           FinanceJournalEntryLines::deleteAll(['id' => $deletedIDs]);
                       }
                       foreach ($modelFinanceJournalEntryLiness as $modelFinanceJournalEntryLines) {

                           $modelFinanceJournalEntryLines->roleId = $model->id;
                           $modelFinanceJournalEntryLines->userId = Yii::$app->user->identity->id;
                           $modelFinanceJournalEntryLines->status = $model->status;


                           if (! ($flag = $modelFinanceJournalEntryLines->save(false))) {
                               $transaction->rollBack();
                               break;
                           }
                       }
                   }
                   if ($flag) {
                       $transaction->commit();
                       return $this->redirect(['view', 'id' => $model->id]);
                   }
               } catch (Exception $e) {
                   $transaction->rollBack();
               }
           }

           // return $this->redirect(['view', 'id' => $model->id]);

         } else {
           // code...

           //load the create form
           return $this->render('update', [
               'model' => $model,
               'modelFinanceJournalEntryLiness' => (empty($modelFinanceJournalEntryLiness)) ? [new FinanceJournalEntryLines] : $modelFinanceJournalEntryLiness
           ]);
         }

    }
    public function getFinanceJournalEntryLines($id)
    {
      $model = FinanceJournalEntryLines::find()->where(['journalId' => $id])->all();
      return $model;
    }



    /**
     * Deletes an existing FinanceJournalEntry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        // $this->layout='addformdatatablelayout';
        $this->layout = '@app/views/layouts/addformdatatablelayout';

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceJournalEntry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceJournalEntry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceJournalEntry::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
