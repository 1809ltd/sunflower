<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CompanyDetails;

/**
 * CompanyDetailsSearch represents the model behind the search form of `app\models\CompanyDetails`.
 */
class CompanyDetailsSearch extends CompanyDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'companyStatus', 'companyUserId'], 'integer'],
            [['companyName', 'companyAddress', 'companySuite', 'companyCity', 'companyCounty', 'companyPostal', 'companyCountry', 'companyLogo', 'companyTimestamp', 'companyupdatedAt', 'companyDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanyDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'companyStatus' => $this->companyStatus,
            'companyTimestamp' => $this->companyTimestamp,
            'companyupdatedAt' => $this->companyupdatedAt,
            'companyDeletedAt' => $this->companyDeletedAt,
            'companyUserId' => $this->companyUserId,
        ]);

        $query->andFilterWhere(['like', 'companyName', $this->companyName])
            ->andFilterWhere(['like', 'companyAddress', $this->companyAddress])
            ->andFilterWhere(['like', 'companySuite', $this->companySuite])
            ->andFilterWhere(['like', 'companyCity', $this->companyCity])
            ->andFilterWhere(['like', 'companyCounty', $this->companyCounty])
            ->andFilterWhere(['like', 'companyPostal', $this->companyPostal])
            ->andFilterWhere(['like', 'companyCountry', $this->companyCountry])
            ->andFilterWhere(['like', 'companyLogo', $this->companyLogo]);

        return $dataProvider;
    }
}
