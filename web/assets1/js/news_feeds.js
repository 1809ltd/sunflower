"use strict";
var ass = function () {
    "use strict";

    function ButtonStatus(){
        if ($('[data-checked=email-checkbox]:checked').length !== 0) {
            $('[data-email-action]').removeClass('hide');
        } else {
            $('[data-email-action]').addClass('hide');
        }
    }

    $(document).on('change', '[data-checked=email-checkbox]', function () {
        var targetLabel = $(this).closest('label');
        var targetEmailList = $(this).closest('li');
        if ($(this).prop('checked')) {
            $(targetLabel).addClass('active');
            $(targetEmailList).addClass('selected');
        } else {
            $(targetLabel).removeClass('active');
            $(targetEmailList).removeClass('selected');
        }
        ButtonStatus();
    });

    $(document).on('click', '[data-email-action]', function () {
        var targetEmailList = '[data-checked=email-checkbox]:checked';
        if ($(targetEmailList).length !== 0) {
            $(targetEmailList).closest('li').slideToggle(function () {
                $(this).remove();
                ButtonStatus();
                if ($('.list-email > li').length === 0) {
                    $('.list-email.list-group').html('<li class="p-15 text-center"><div class="p-20"><i class="fa fa-trash fa-5x text-silver"></i></div> This folder is empty</li>');
                }
            });
        }
    });

    $('[data-click=email-select-all]').on('click', function (e) {
        e.preventDefault();
        var targetIcon = $(this).find('i');
        if ($(targetIcon).hasClass('fa-check-square')) {
            $(targetIcon).removeClass('fa-check-square text-inverse').addClass('fa-square text-muted');
            $('.list-email .email-checkbox input[type="checkbox"]').prop('checked', false);
        } else {
            $(targetIcon).addClass('fa-check-square text-inverse').removeClass('fa-square text-muted');
            $('.list-email .email-checkbox input[type="checkbox"]').prop('checked', true);
        }
        $('.list-email .email-checkbox input[type="checkbox"]').trigger('change');
    });
	
	$($('[type="checkbox"]')[1]).trigger('click');

};
var Feeds = function () {
    "use strict";
    return {
        init: function () {
            ass();
        }
    }
}();
$(function () {
    Feeds.init();
});