<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

?>
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Add an Invoice</h3>
		<div class="box-tools pull-right">
			<?= Html::button('Add Invoice', ['value'=>Url::to((['/finance/receipt/invoicepayment/finance-payment-lines/create'])),'class' => 'btn btn-sm btn-success','id'=>'modalButton']) ?>
		</div>
	</div>
	<?php
	Modal::begin([
		// 'header'=>'<h3>Add Customer</h3>',
		'class'=>'modal-dialog',
		'id'=>'modal',
		'size'=>'modal-lg',
		'options' => [
				'tabindex' => false // important for Select2 to work properly
		],
	]);
	echo "<div class='modal-content' id='modalContent'></div>";
	Modal::end();
	?>
	<?php
	Modal::begin([
		'header'=>'<h4>Update Model</h4>',
		'id'=>'update-modal',
		'size'=>'modal-lg',
		'options' => [
				'tabindex' => false // important for Select2 to work properly
		],
	]);
	echo "<div id='updateModalContent'></div>";
	Modal::end();
	?>
	<div class="box-body">
		<?php Pjax::begin(['id' => 'holdbillpayment']); ?>
		<?php
		// Assign the output to dataProvider to be able to be view in grid
		$dataProvider = new \yii\data\ArrayDataProvider([
			'key'=>'id',
			'allModels' => $output,
			// 'sort' => [
			//     'attributes' => ['id', 'billRef', 'amount'],
			// ],
		]);

		echo GridView::widget([
			'dataProvider' => $dataProvider,
			// 'filterModel' => $searchModel,
			// 'dataProvider' => $output,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				// 'id',
				// 'invoicepaymentId',
				// 'invoiceId',
				[
					'label' => 'Invoice No:',
					'attribute'=>'invoiceId',
					'value'=>'invoice.invoiceNumber',
				],
				// 'customerId',
				'amount',
				//'status',
				//'deletedAt',
				//'created_at',
				//'updated_at',
				//'userId',

				// ['class' => 'yii\grid\ActionColumn'],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{view}{update}{delete}',
					'buttons' => ['view' => function($url, $model) {
						return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
					},
					'update' => function($url,$model,$key){
						$btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
							'value'=>Url::to((['/finance/receipt/invoicepayment/finance-payment-lines/update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
							'class'=>'update-modal-click grid-action',
							'data-toggle'=>'tooltip',
							'data-placement'=>'bottom',
							'title'=>'Update'
						]);
						return $btn;
					},
					'delete' => function($url, $model) {
						return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['/finance/receipt/invoicepayment/finance-payment-lines/delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
					}
				]
			],
		],
	]); ?>
	<?php Pjax::end(); ?>
	<br>
	<br>
	<div class="row">
		<div class="col-md-12">
			<?=	$this->render('_form', [
			'model' => $model
			]) ?>
		</div>
	</div>

</div>
</div>
