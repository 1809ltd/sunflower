<?php

namespace app\modules\inventory\assetsetup\assetcategory;

/**
 * assetcategory module definition class
 */
class assetcategory extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\assetsetup\assetcategory\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        
        // custom initialization code goes here
    }
}
