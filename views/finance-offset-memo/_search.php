<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOffsetMemoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-offset-memo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'offsetMemoAccount') ?>

    <?= $form->field($model, 'refernumber') ?>

    <?= $form->field($model, 'offsetMemoTxnDate') ?>

    <?= $form->field($model, 'offsetMemoNote') ?>

    <?php // echo $form->field($model, 'customerId') ?>

    <?php // echo $form->field($model, 'offsetAmount') ?>

    <?php // echo $form->field($model, 'offsetMemoStatus') ?>

    <?php // echo $form->field($model, 'offsetMemoCreatedAt') ?>

    <?php // echo $form->field($model, 'offsetMemoUpdatedAt') ?>

    <?php // echo $form->field($model, 'offsetMemoDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
