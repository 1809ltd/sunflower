<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut;

/**
 * AssetCheckOutSearch represents the model behind the search form of `app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut`.
 */
class AssetCheckOutSearch extends AssetCheckOut
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assetId', 'locationId', 'activity', 'deliveryId', 'activityId', 'orderId', 'orderlistId', 'vehicle', 'assignto', 'status', 'athurizedBy', 'userId', 'createdBy'], 'integer'],
            [['checkOutDate', 'dueDate', 'timestamp', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetCheckOut::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assetId' => $this->assetId,
            'locationId' => $this->locationId,
            'checkOutDate' => $this->checkOutDate,
            'dueDate' => $this->dueDate,
            'activity' => $this->activity,
            'deliveryId' => $this->deliveryId,
            'activityId' => $this->activityId,
            'orderId' => $this->orderId,
            'orderlistId' => $this->orderlistId,
            'vehicle' => $this->vehicle,
            'assignto' => $this->assignto,
            'timestamp' => $this->timestamp,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'status' => $this->status,
            'athurizedBy' => $this->athurizedBy,
            'userId' => $this->userId,
            'createdBy' => $this->createdBy,
        ]);

        return $dataProvider;
    }
}
