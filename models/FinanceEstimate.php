<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_estimate".
 *
 * @property int $id
 * @property string $estimateNumber
 * @property int $customerId
 * @property int $localpurchaseOrder
 * @property int $estimateProjectId
 * @property string $estimateTxnDate
 * @property string $estimateDeadlineDate
 * @property string $estimateNote
 * @property string $estimateFooter
 * @property string $estimateTxnStatus
 * @property int $estimateEmailStatus
 * @property string $estimateBillEmail
 * @property double $estimateTaxAmount
 * @property double $estimateDiscountAmount
 * @property double $estimateSubAmount
 * @property double $estimateAmount
 * @property int $estimatedCreatedby
 * @property string $estimatedCreatedAt
 * @property string $estimatedUpdatedAt
 * @property string $estimatedDeletedAt
 * @property int $approvedby
 *
 * @property UserDetails $estimatedCreatedby0
 * @property FinanceCustomerPurchaseOrder $localpurchaseOrder0
 * @property EventDetails $estimateProject
 */
class FinanceEstimate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_estimate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estimateNumber', 'customerId', 'estimateProjectId', 'estimateTxnDate', 'estimateDeadlineDate', 'estimateTxnStatus','estimateTaxAmount', 'estimateDiscountAmount', 'estimateSubAmount', 'estimateAmount', 'estimatedCreatedby'], 'required'],
            [['customerId', 'localpurchaseOrder', 'estimateProjectId', 'estimateEmailStatus', 'estimatedCreatedby', 'approvedby'], 'integer'],
            [['estimateTxnDate', 'estimateBillEmail', 'estimateDeadlineDate', 'estimatedCreatedAt', 'estimatedUpdatedAt', 'estimatedDeletedAt'], 'safe'],
            [['estimateNote', 'estimateFooter', 'estimateTxnStatus'], 'string'],
            [['estimateTaxAmount', 'estimateDiscountAmount', 'estimateSubAmount', 'estimateAmount'], 'number'],
            [['estimateNumber'], 'string', 'max' => 50],
            [['estimateBillEmail'], 'string', 'max' => 100],
            [['estimateNumber'], 'unique'],
            [['estimatedCreatedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['estimatedCreatedby' => 'id']],
            [['localpurchaseOrder'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceCustomerPurchaseOrder::className(), 'targetAttribute' => ['localpurchaseOrder' => 'id']],
            [['estimateProjectId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['estimateProjectId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estimateNumber' => 'Quotation Number',
            'customerId' => 'Customer',
            'localpurchaseOrder' => 'Localpurchase Order',
            'estimateProjectId' => 'Project',
            'estimateTxnDate' => 'Date',
            'estimateDeadlineDate' => 'Vadility Date',
            'estimateNote' => 'Quotation Note',
            'estimateFooter' => 'Quotation Footer',
            'estimateTxnStatus' => 'Status',
            'estimateEmailStatus' => 'Quotation Email Status',
            'estimateBillEmail' => 'Quotation Bill Email',
            'estimateTaxAmount' => 'Quotation Tax Amount',
            'estimateDiscountAmount' => 'Quotation Discount Amount',
            'estimateSubAmount' => 'Sub Amount',
            'estimateAmount' => 'Amount',
            'estimatedCreatedby' => 'Createdby',
            'estimatedCreatedAt' => 'Created At',
            'estimatedUpdatedAt' => 'Updated At',
            'estimatedDeletedAt' => 'Deleted At',
            'approvedby' => 'Approvedby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimatedCreatedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'estimatedCreatedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalpurchaseOrder0()
    {
        return $this->hasOne(FinanceCustomerPurchaseOrder::className(), ['id' => 'localpurchaseOrder']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimateProject()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'estimateProjectId']);
    }
    public function getestimatesItems($id)
    {
      return FinanceEstimateLines::find()
        ->where(['and', "estimateId=$id"])
        ->all();
    }
    public function getCustomerDetails($id)
    {
      return CustomerDetails::find()
        ->where(['and', "id=$id"])->all();
        // ->indexBy('id')->column();
    }
    public function getEstnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(estimateNumber) AS number from finance_estimate where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "Qoute-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "Qoute-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "Qoute-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
