<?php

namespace app\modules\customer\marketing\customerleadappointment;

/**
 * customerleadappointment module definition class
 */
class customerleadappointment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\customer\marketing\customerleadappointment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
