<?php
namespace app\modules\finance\expensepayment\billpayment\controllers;
use Yii;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLinesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceBillsPaymentsLinesController implements the CRUD actions for FinanceBillsPaymentsLines model.
 */
class FinanceBillsPaymentsLinesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinanceBillsPaymentsLines models.
     * @return mixed
     */
    public function actionIndex()
    {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new FinanceBillsPaymentsLinesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceBillsPaymentsLines model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceBillsPaymentsLines model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = new FinanceBillsPaymentsLines();

      //Geting the time created Timestamp
      $model->created_at= new \yii\db\Expression('NOW()');
      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
        // return $this->redirect(['view', 'id' => $model->id]);

        if (!empty($model->billpaymentId)) {
          // code...
          return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/update', 'id' => $model->billpaymentId]);

        } else {
          // code...
          return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/create']);
        }
      }
      return $this->renderAjax('create', [
        'model' => $model,
      ]);

    }

    /**
     * Updates an existing FinanceBillsPaymentsLines model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = $this->findModel($id);

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
        // return $this->redirect(['view', 'id' => $model->id]);
        if (!empty($model->billpaymentId)) {
          // code...
          return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/update', 'id' => $model->billpaymentId]);
        } else {
          // code...
          return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/create']);
        }
        // return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/create']);
      }
      return $this->renderAjax('update', [
              'model' => $model,
          ]);

    }

    /**
     * Deletes an existing FinanceBillsPaymentsLines model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      // return $this->redirect(['index']);
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // Update all the data based on the payment made and status
      $this->findModel($id)->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);
      // Get the parent Id to determin if its and update or delete
      $billpaymentId = FinanceBillsPaymentsLines::find()->where(['id' => $id])->one();

      if (!empty($billpaymentId["billpaymentId"])) {
          // code...
          return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/update', 'id' => $billpaymentId]);

        } else {
          // code...
          return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/create']);

        }
    }

    /**
     * Finds the FinanceBillsPaymentsLines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceBillsPaymentsLines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceBillsPaymentsLines::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
