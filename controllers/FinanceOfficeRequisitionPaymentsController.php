<?php

namespace app\controllers;

use Yii;
use app\models\FinanceOfficeRequisitionPayments;
use app\models\FinanceOfficeRequisitionPaymentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceOfficeRequisitionPaymentsController implements the CRUD actions for FinanceOfficeRequisitionPayments model.
 */
class FinanceOfficeRequisitionPaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->reqpaymentUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->reqpaymentUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceOfficeRequisitionPayments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinanceOfficeRequisitionPaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceOfficeRequisitionPayments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceOfficeRequisitionPayments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FinanceOfficeRequisitionPayments();

        $model->reqrecepitNumber = $model->getRcpnumber();

        //Getting user Log in details
        $model->userId= "1";


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FinanceOfficeRequisitionPayments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FinanceOfficeRequisitionPayments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceOfficeRequisitionPayments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceOfficeRequisitionPayments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceOfficeRequisitionPayments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
