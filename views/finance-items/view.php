<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItems */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-items-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'itemsName',
            'itemsCode',
            'itemsDescription:ntext',
            'itemsTaxable',
            'itemsCustomerUnitPrice',
            'itemsSupplierUnitPrice',
            'itemsClassification',
            'itemsIncomeAccountRef',
            'itemsPurchaseDesc:ntext',
            'itemsPurchaseCost',
            'itemsExpenseAccountRef',
            'itemsAssetAccountRef',
            'itemsCreatedAt',
            'itemsLastupdatedAt',
            'itemsDeletedAt',
            'itemsStatus',
            'userId',
        ],
    ]) ?>

</div>
