<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoice */

$this->title = 'Update Finance Invoice: ' . $model->invoiceNumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->invoiceNumber, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-invoice-update">
    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceInvoiceLines'=>$modelsFinanceInvoiceLines, //to enable Dynamic Form
    ]) ?>

</div>
