<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines */

$this->title = Yii::t('app', 'Create Finance Office Requisition Payments Lines');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Office Requisition Payments Lines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-office-requisition-payments-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
