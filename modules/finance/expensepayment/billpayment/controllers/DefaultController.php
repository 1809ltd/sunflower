<?php

namespace app\modules\finance\expensepayment\billpayment\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsSearch;

// use yii\web\Controller;
/*GEtting the Bills*/
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLinesSearch;
/**
 * Default controller for the `billpayment` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionSearch()
    {
      $model = new \yii\base\DynamicModel([
        'supplier'
      ]);
      $model->addRule(['supplier'], 'required')
          ->validate();

      if($model->load(Yii::$app->request->post())){
        // do somenthing with model
        \Yii::$app->session->set('vendorInvoiceId',$model->supplier);
        // return $this->redirect(['view']);
        return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/create']);
      }
      return $this->renderAjax('searchSupplier', ['model'=>$model]);
    }
    // Close session
    public function actionClosesearch(){
      unset($_SESSION['vendorInvoiceId']);
      return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/']);
    }
    
    // Gett all the payment details
    public function getpaymentline($id)
    {
      $model = FinanceBillsPaymentsLines::find()->where(['id' => $id])->all();
      return $model;
    }

// // Close session
//     public function actionClosesearch(){
//
//         unset($_SESSION['vendorId']);
//
//
//         return $this->redirect(['/finance/expensepayment/bill-payment/finance-bills-payments/']);
//     }

    protected function findModel($id)
    {
        if (($model = FinanceBillsPayments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
