<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItemsCategory */

$this->title = 'Create Service Category';
$this->params['breadcrumbs'][] = ['label' => 'Finance Items Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-items-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
