<?php

namespace app\modules\finance\expense\officeexpense\officeexpenselines;

/**
 * officeexpenselines module definition class
 */
class officeexpenselines extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\officeexpense\officeexpenselines\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
