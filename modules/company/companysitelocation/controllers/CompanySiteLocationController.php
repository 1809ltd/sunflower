<?php

namespace app\modules\company\companysitelocation\controllers;

use Yii;
use app\modules\company\companysitelocation\models\CompanySiteLocation;
use app\modules\company\companysitelocation\models\CompanySiteLocationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanySiteLocationController implements the CRUD actions for CompanySiteLocation model.
 */
class CompanySiteLocationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->locationUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->locationUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all CompanySiteLocation models.
     * @return mixed
     */
    public function actionIndex()
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $searchModel = new CompanySiteLocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompanySiteLocation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';


        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CompanySiteLocation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;

        $model = new CompanySiteLocation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CompanySiteLocation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

      $model = $this->findModel($id);

      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CompanySiteLocation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

      $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompanySiteLocation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CompanySiteLocation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CompanySiteLocation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
