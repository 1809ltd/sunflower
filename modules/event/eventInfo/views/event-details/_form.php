<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Event Categories*/
use app\modules\event\eventsetup\eventcaterogies\models\EventCaterogies;
// use kartik\time\TimePicker;

/*Date And Time*/
use yii\bootstrap4\Modal;
// use kartik\widgets\DateTimePicker;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\EventDetails */
/* @var $form yii\widgets\ActiveForm */

?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Event</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="event-details-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CustomerDetails::find()->all(),'id','customerDisplayName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Customer ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                  </div>
                </div>


                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                      <?= $form->field($model, 'eventCategory')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(EventCaterogies::find()->all(),'id','eventCatName'),
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Category Name ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventTheme')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventVistorsNumber')->textInput() ?>

                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventStartDate')->widget(DateTimePicker::classname(), [
                          	'options' => ['placeholder' => 'Enter event Start time ...'],
                          	'pluginOptions' => [
                          		'autoclose' => true
                          	]
                          ]); ?>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventEndDate')->widget(DateTimePicker::classname(), [
                          	'options' => ['placeholder' => 'Enter event  End time ...'],
                          	'pluginOptions' => [
                          		'autoclose' => true
                          	]
                          ]); ?>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventSetUpDateTime')->widget(DateTimePicker::classname(), [
                          	'options' => ['placeholder' => 'Enter event time ...'],
                          	'pluginOptions' => [
                          		'autoclose' => true
                          	]
                          ]); ?>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                  <?= $form->field($model, 'eventSetDownDateTime')->widget(DateTimePicker::classname(), [
                          'options' => ['placeholder' => 'Enter event time ...'],
                          'pluginOptions' => [
                            'autoclose' => true
                          ]
                        ]); ?>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventLocation')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                  <?= $form->field($model, 'eventDescription')->textarea(['rows' => 6]) ?>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'eventNotes')->textarea(['rows' => 6]) ?>

                  </div>
                </div>
                <?php
                    // necessary for update action.
                    if (! $model->isNewRecord) {
                      // code...
                      $status = ['0' => 'Cancel','1' => 'Ready', '2' => 'Hold', '3' => 'Approved'];
                        echo $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']);

                    } else {
                      // code...
                      echo $form->field($model, 'status')->hiddenInput(['value'=> "2"])->label(false);
                    }

                ?>

                <div class="col-sm-3">
                  <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>

                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
