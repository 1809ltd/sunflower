<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCreditMemo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Credit Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-credit-memo-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'creditMemoAccount',
            'refernumber',
            'creditMemoTxnDate',
            'creditMemoNote:ntext',
            'customerId',
            'creditAmount',
            'creditMemoCreatedAt',
            'creditMemoUpdatedAt',
            'creditMemoDeletedAt',
            'userId',
        ],
    ]) ?>

</div>
