<?php

namespace app\controllers;

use Yii;
use app\models\FinanceBills;
use app\models\FinanceBillsLines;
use app\models\FinanceBillsSearch;
use yii\web\Controller;
use app\models\Model;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * FinanceBillsController implements the CRUD actions for FinanceBills model.
 */
class FinanceBillsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->billUpdatedTime = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->billUpdatedTime = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }


    /**
     * Lists all FinanceBills models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new FinanceBillsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceBills model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceBills model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new FinanceBills();

        $modelsFinanceBillsLines = [new FinanceBillsLines];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsFinanceBillsLines = Model::createMultiple(FinanceBillsLines::classname());
          Model::loadMultiple($modelsFinanceBillsLines, Yii::$app->request->post());

          // ajax validation
          /*
          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFinanceBillsLines),
                    ActiveForm::validate($model)
                );
            }*/

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceBillsLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceBillsLines as $modelFinanceBillsLines) {

                          $modelFinanceBillsLines->biilsId = $model->id;
                          $modelFinanceBillsLines->userId = $model->userId;
                          $modelFinanceBillsLines->billsLinesBillableStatus = $model->billStatus;


                          if (! ($flag = $modelFinanceBillsLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }

        
        }

        return $this->render('create', [
            'model' => $model,
              'modelsFinanceBillsLines' => (empty($modelsFinanceBillsLines)) ? [new FinanceBillsLines] : $modelsFinanceBillsLines
        ]);
        /*

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);*/
    }

    /**
     * Updates an existing FinanceBills model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        $modelsFinanceBillsLines = $this->getFinanceBillsLines($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $oldIDs = ArrayHelper::map($modelsFinanceBillsLines, 'id', 'id');
          $modelsFinanceBillsLines = Model::createMultiple(FinanceBillsLines::classname(), $modelsFinanceBillsLines);
          Model::loadMultiple($modelsFinanceBillsLines, Yii::$app->request->post());
          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceBillsLines, 'id', 'id')));

          // ajax validation
          if (Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ArrayHelper::merge(
                  ActiveForm::validateMultiple($modelsFinanceBillsLines),
                  ActiveForm::validate($model)
              );
          }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceBillsLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      if (! empty($deletedIDs)) {
                          FinanceBillsLines::deleteAll(['id' => $deletedIDs]);
                      }
                      foreach ($modelsFinanceBillsLines as $modelFinanceBillsLines) {

                        $modelFinanceBillsLines->biilsId = $model->id;
                        $modelFinanceBillsLines->userId = $model->userId;
                        $modelFinanceBillsLines->billsLinesBillableStatus = $model->billStatus;

                          if (! ($flag = $modelFinanceBillsLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
              }
          }

            //return $this->redirect(['view', 'id' => $model->estimateId]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelsFinanceBillsLines' => (empty($modelsFinanceBillsLines)) ? [new FinanceBillsLines] : $modelsFinanceBillsLines
        ]);



      /*  if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);*/
    }

    /**
     * Deletes an existing FinanceBills model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceBills model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceBills the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceBills::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getFinanceBillsLines($id)
    {
      $model = FinanceBillsLines::find()->where(['biilsId' => $id])->all();
      return $model;
    }
}
