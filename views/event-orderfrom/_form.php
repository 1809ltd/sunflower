<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/*Getting the product and service offered, customer, Tax*/
use app\models\FinanceItems;
use app\models\EventDetails;

use app\models\VendorCompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfrom */
/* @var $form yii\widgets\ActiveForm */

?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="panel-body finance-estimate-form">

                  <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'eventId')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(EventDetails::find()->all(),'id','eventName'),
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Event ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]);
                      ?>
                      <!-- ->dropDownList(EventDetails::getEvents(), ['prompt' => 'Select Event...']) -->
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?php
                          // necessary for update action.
                          if (! $model->isNewRecord) {
                            // code...

                            $orderNum = $model->orderNumber;

                          } else {
                            // code...
                            $orderNum=$model->getOrdnumber();
                          }

                      ?>
                      <?= $form->field($model, 'orderNumber')->textInput(['readonly' => true, 'value' => $orderNum])?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                      <?= $form->field($model, 'orderStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">

                    </div>
                  </div>

                  <div class="panel-body"></div>

                  <div class="panel-body">

                    <div class="panel panel-pvr panel--style--1">
                      <div class="panel-heading">
                          <div class="panel-heading-btn">
                              <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                              <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                              <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                              <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                          </div>
                          <h4 class="panel-title">Add Order Item</h4>
                      </div>
                      <div class="panel-body">
                           <?php DynamicFormWidget::begin([
                              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                              'widgetBody' => '.container-items', // required: css class selector
                              'widgetItem' => '.item', // required: css class
                              'limit' => 80, // the maximum times, an element can be cloned (default 999)
                              'min' => 1, // 0 or 1 (default 1)
                              'insertButton' => '.add-item', // css class
                              'deleteButton' => '.remove-item', // css class
                              'model' => $modelsEventOrderfromList[0],
                              'formId' => 'dynamic-form',
                              'formFields' => [

                                // 'id',
                                // 'orderId',
                                'itemid',
                                'details',
                                'outsourced',
                                'vendorId',
                                'quantity',
                                //'status',
                                //'createdAt',
                                //'updatedAt',
                                //'deletedAt',
                                //'userId',
                                //'approvedBy',

                              ],
                          ]); ?>

                          <div class="container-items"><!-- widgetContainer -->
                            <!-- Loopping Items in the Lists -->
                          <?php foreach ($modelsEventOrderfromList as $i => $modelEventOrderfromList): ?>
                              <div class="item panel-pvr panel--style--2 "><!-- widgetBody -->


                                  <div class="panel-heading">
                                      <h3 class="panel-title pull-left">Estimate Item</h3>
                                      <div class="pull-right">
                                          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>


                                  <div class="panel-body">
                                      <?php
                                          // necessary for update action.
                                          if (! $modelEventOrderfromList->isNewRecord) {
                                              echo Html::activeHiddenInput($modelEventOrderfromList, "[{$i}]id");
                                          }
                                      ?>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelEventOrderfromList, "[{$i}]itemid")->dropDownList(FinanceItems::getFinanceITems(), ['prompt' => 'Select Serivce...']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelEventOrderfromList, "[{$i}]details")->textInput(['maxlength' => true]) ?>
                                        </div>
                                      </div>

                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelEventOrderfromList, "[{$i}]quantity")->textInput([
                                          'maxlength' => true,
                                          'class' => 'form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelEventOrderfromList, "[{$i}]outsourced")->textInput() ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelEventOrderfromList, "[{$i}]vendorId")->widget(Select2::classname(), [
                                              'data' => ArrayHelper::map(VendorCompanyDetails::find()->all(),'id','vendorCompanyName'),
                                              'language' => 'en',
                                              'options' => ['placeholder' => 'Select a Vendor ...'],
                                              'pluginOptions' => [
                                                  'allowClear' => true
                                              ],
                                          ]);
                                          ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>

                                  </div>
                              </div>
                          <?php endforeach; ?>
                          </div>
                          <?php DynamicFormWidget::end(); ?>
                      </div>
                  </div>

                  </div>

                    <div class="panel-body"></div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($model, 'orderNote')->textarea(['rows' => 6]) ?>

                      </div>
                    </div>

                    <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>
                    <?= $form->field($model, 'personIncharge')->hiddenInput(['value'=> "1"])->label(false);?>

                    <?= $form->field($model, 'approvedBy')->hiddenInput(['value'=> "1"])->label(false);?>

                    <div class="col-sm-3">
                      <div class="form-group">

                      </div>
                    </div>

                    <div class="panel-body"></div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= Html::submitButton($modelEventOrderfromList->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                    </div>
                  </div>

                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
