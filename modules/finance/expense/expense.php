<?php

namespace app\modules\finance\expense;

/**
 * expense module definition class
 */
class expense extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Bills Sub Module*/
          'bills' => [
                'class' => 'app\modules\finance\expense\bills\bills',
            ],
            /*Office expense*/
          'officeexpense' => [
              'class' => 'app\modules\finance\expense\officeexpense\officeexpense',
          ],
          /*Event Expense*/
          'eventexpense' => [
              'class' => 'app\modules\finance\expense\eventexpense\eventexpense',
          ],
          /*Lead Expense*/
          'leadexpense' => [
              'class' => 'app\modules\finance\expense\leadexpense\leadexpense',
          ],
        ];

        // custom initialization code goes here
    }
}
