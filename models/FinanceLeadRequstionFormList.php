<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_lead_requstion_form_list".
 *
 * @property int $id
 * @property int $reqId
 * @property int $activityId
 * @property int $personelId
 * @property string $personnelTel
 * @property string $description
 * @property double $qty
 * @property double $unitprice
 * @property double $amont
 * @property int $status
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 *
 * @property FinanceLeadRequstionForm $req
 * @property FinanceEventActivity $activity
 * @property FinanceEventActivity $personel
 */
class FinanceLeadRequstionFormList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_lead_requstion_form_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activityId', 'personnelTel', 'qty', 'unitprice', 'amont'], 'required'],
            [['reqId', 'activityId', 'personelId', 'status', 'userId'], 'integer'],
            [['description'], 'string'],
            [['qty', 'unitprice', 'amont'], 'number'],
            [['reqId', 'status', 'userId','createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['personnelTel'], 'string', 'max' => 15],
            [['reqId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceLeadRequstionForm::className(), 'targetAttribute' => ['reqId' => 'id']],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEventActivity::className(), 'targetAttribute' => ['activityId' => 'id']],
            [['personelId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEventActivity::className(), 'targetAttribute' => ['personelId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reqId' => 'Req',
            'activityId' => 'Activity',
            'personelId' => 'Personel',
            'personnelTel' => 'Personnel',
            'description' => 'Description',
            'qty' => 'Qty',
            'unitprice' => 'Unit Price',
            'amont' => 'Amont',
            'status' => 'Status',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReq()
    {
        return $this->hasOne(FinanceLeadRequstionForm::className(), ['id' => 'reqId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(FinanceEventActivity::className(), ['id' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonel()
    {
        return $this->hasOne(FinanceEventActivity::className(), ['id' => 'personelId']);
    }
}
