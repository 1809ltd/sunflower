<?php

namespace app\modules\inventory\asset\assetamortization\controllers;

use yii\web\Controller;

/**
 * Default controller for the `assetamortization` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
