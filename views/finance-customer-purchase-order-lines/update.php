<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrderLines */

$this->title = 'Update Finance Customer Purchase Order Lines: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Customer Purchase Order Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-customer-purchase-order-lines-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
