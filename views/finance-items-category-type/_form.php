<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


use app\models\FinanceItemsCategory;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItemsCategoryType */
/* @var $form yii\widgets\ActiveForm */
?>


<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">

      <div class="finance-items-category-form">

          <?php $form = ActiveForm::begin(); ?>

      </div>

      <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
                <?= $form->field($model, 'financeItemCategoryTypeName')->textInput(['maxlength' => true]) ?>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
            <?= $form->field($model, 'financeItemCategoryTypeDescription')->textarea(['rows' => 6]) ?>
            </div>
          </div>



      </div>

      <div class="row">

        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'financeItemCategoryId')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(FinanceItemsCategory::find()->all(),'id','financeItemCategoryName'),
                'language' => 'en',
                'options' => ['placeholder' => 'Select a Categry ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
            <?= $form->field($model, 'financeItemCategoryTypeStatus')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>

          </div>
        </div>


      </div>

  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
    <!-- /.box -->
