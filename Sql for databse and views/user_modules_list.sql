/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100130
 Source Host           : localhost:3306
 Source Schema         : event_sherehe

 Target Server Type    : MySQL
 Target Server Version : 100130
 File Encoding         : 65001

 Date: 15/02/2019 19:14:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_modules_list
-- ----------------------------
DROP TABLE IF EXISTS `user_modules_list`;
CREATE TABLE `user_modules_list`  (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `moduleName` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `controller` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icon` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `createdBy` int(50) NULL DEFAULT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deletedAt` timestamp(0) NULL,
  `userId` int(50) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_role_module_permission
-- ----------------------------
DROP TABLE IF EXISTS `user_role_module_permission`;
CREATE TABLE `user_role_module_permission`  (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `roleId` int(50) NULL DEFAULT NULL,
  `moduleId` int(50) NULL DEFAULT NULL,
  `new` tinyint(4) NOT NULL COMMENT 'CREATE',
  `view` tinyint(4) NOT NULL COMMENT 'READ',
  `save` tinyint(4) NOT NULL COMMENT 'UPDATE',
  `remove` tinyint(4) NOT NULL COMMENT 'DELETE',
  `createdBy` int(50) NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deletedAt` timestamp(0) NULL,
  `userId` int(50) NOT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for user_role_types
-- ----------------------------
DROP TABLE IF EXISTS `user_role_types`;
CREATE TABLE `user_role_types`  (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `createdBy` int(50) NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deletedAt` timestamp(0) NOT NULL,
  `userId` int(50) NOT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

DROP TABLE IF EXISTS `user_assignment`;
CREATE TABLE `user_assignment`  (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `roleId` int(50) NULL DEFAULT NULL,
  `userId` int(50) NULL DEFAULT NULL,
  `createdBy` int(50) NOT NULL,
  `createdAt` datetime(0) NULL DEFAULT NULL,
  `updatedAt` timestamp(0) Not NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `deletedAt` timestamp(0) NULL,
  `assigner` int(50) NOT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
)
