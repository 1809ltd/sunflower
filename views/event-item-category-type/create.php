<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventItemCategoryType */

$this->title = 'Create Event Item Category Type';
$this->params['breadcrumbs'][] = ['label' => 'Event Item Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-item-category-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
