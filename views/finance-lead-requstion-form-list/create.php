<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequstionFormList */

$this->title = 'Create Finance Lead Requstion Form List';
$this->params['breadcrumbs'][] = ['label' => 'Finance Lead Requstion Form Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-lead-requstion-form-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
