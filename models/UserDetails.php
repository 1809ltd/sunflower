<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_details".
 *
 * @property int $id
 * @property string $userStaffId
 * @property string $userFName
 * @property string $userLName
 * @property string $userPhone
 * @property string $userEmail
 * @property int $userStatus
 * @property string $userLastLogin
 * @property int $userCreatedBy
 * @property string $userDeleteAt
 */
class UserDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'userStaffId', 'userFName', 'userLName', 'userPhone', 'userEmail', 'userStatus', 'userCreatedBy'], 'required'],
            [['id', 'userStatus', 'userCreatedBy'], 'integer'],
            [['userLastLogin', 'userDeleteAt'], 'safe'],
            [['userStaffId'], 'string', 'max' => 50],
            [['userFName', 'userLName', 'userEmail'], 'string', 'max' => 255],
            [['userPhone'], 'string', 'max' => 15],
            [['userStaffId'], 'unique'],
            [['userPhone'], 'unique'],
            [['userEmail'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userStaffId' => 'User Staff ID',
            'userFName' => 'User Fname',
            'userLName' => 'User Lname',
            'userPhone' => 'User Phone',
            'userEmail' => 'User Email',
            'userStatus' => 'User Status',
            'userLastLogin' => 'User Last Login',
            'userCreatedBy' => 'User Created By',
            'userDeleteAt' => 'User Delete At',
        ];
    }
}
