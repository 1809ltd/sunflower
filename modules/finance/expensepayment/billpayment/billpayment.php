<?php

namespace app\modules\finance\expensepayment\billpayment;

/**
 * billpayment module definition class
 */
class billpayment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expensepayment\billpayment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
