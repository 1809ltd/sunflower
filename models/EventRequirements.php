<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_requirements".
 *
 * @property int $id
 * @property int $eventId
 * @property string $eventNumber
 * @property int $department
 * @property string $departmentName
 * @property string $requirementDetails
 * @property double $requirementSize
 * @property string $requirementSIUnit
 * @property string $requirementCreatedAt
 * @property string $requirementLastUpdated
 * @property string $requirementDeletedAt
 * @property int $preparedBy
 * @property int $approvedBy
 * @property int $requirementStatus
 *
 * @property EventDetails $eventNumber0
 * @property UserDetails $preparedBy0
 * @property UserDetails $approvedBy0
 */
class EventRequirements extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_requirements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eventId', 'eventNumber', 'department', 'departmentName', 'requirementDetails', 'requirementSize', 'requirementSIUnit', 'preparedBy', 'requirementStatus'], 'required'],
            [['eventId', 'department', 'preparedBy', 'approvedBy', 'requirementStatus'], 'integer'],
            [['requirementDetails'], 'string'],
            [['requirementSize'], 'number'],
            [['requirementCreatedAt', 'requirementLastUpdated', 'requirementDeletedAt'], 'safe'],
            [['eventNumber', 'departmentName', 'requirementSIUnit'], 'string', 'max' => 100],
            [['eventNumber'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['eventNumber' => 'eventNumber']],
            [['preparedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['preparedBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eventId' => 'Event',
            'eventNumber' => 'Event Number',
            'department' => 'Department',
            'departmentName' => 'Department Name',
            'requirementDetails' => 'Requirement Details',
            'requirementSize' => 'Requirement Size',
            'requirementSIUnit' => 'Requirement Siunit',
            'requirementCreatedAt' => 'Requirement Created At',
            'requirementLastUpdated' => 'Requirement Last Updated',
            'requirementDeletedAt' => 'Requirement Deleted At',
            'preparedBy' => 'Prepared By',
            'approvedBy' => 'Approved By',
            'requirementStatus' => 'Requirement Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventNumber0()
    {
        return $this->hasOne(EventDetails::className(), ['eventNumber' => 'eventNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreparedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'preparedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }
}
