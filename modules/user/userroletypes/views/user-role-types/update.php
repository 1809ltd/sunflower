<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userroletypes\models\UserRoleTypes */

$this->title = 'Update User Role Types: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Role Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-role-types-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelUserRoleModulePermissions' => $modelUserRoleModulePermissions,
    ]) ?>

</div>
