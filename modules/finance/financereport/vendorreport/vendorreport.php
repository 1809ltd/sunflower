<?php

namespace app\modules\finance\financereport\vendorreport;

/**
 * vendorreport module definition class
 */
class vendorreport extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\vendorreport\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
