<?php

namespace app\modules\event\workplan\controllers;

use Yii;
use app\modules\event\workplan\models\EventWorkplan;
use app\modules\event\workplan\models\EventWorkplanSearch;
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventWorkplanController implements the CRUD actions for EventWorkplan model.
 */
class EventWorkplanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...

            $this->updatedAt = new \yii\db\Expression('NOW()');

          }else {
            // code...
            $this->updatedAt = new \yii\db\Expression('NOW()');
          }
          return parent::beforeSave();
        }


        /**
         * Lists all EventWorkplan models.
         * @return mixed
         */
        public function actionIndex()
        {
            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = new EventWorkplan();

            // $searchModel = new EventWorkplanSearch();
            // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            /*If we loading a new page*/

            //checking if its a post
            if ($model->load(Yii::$app->request->post())) {
              // code...

              \Yii::$app->session->set('customerId',$model->customerId);
              \Yii::$app->session->set('eventId',$model->eventId);
              \Yii::$app->session->set('orderId',$model->orderId);

              return $this->redirect(['index']);

            } else {
              // code...

              //load the Delvery Search form
                return $this->render('searchworkplanorder', [
                    // 'searchModel' => $searchModel,
                    // 'dataProvider' => $dataProvider,
                    'model'=>$model,
                ]);
            }

            // return $this->render('searchworkplanorder', [
            //     'searchModel' => $searchModel,
            //     'dataProvider' => $dataProvider,
            //     'model' => $model,
            // ]);
        }
        public function actionClosesearch(){

            unset($_SESSION['customerId']);
            unset($_SESSION['eventId']);
            unset($_SESSION['orderId']);

            return $this->redirect(['index']);
        }

        /**
         * Displays a single EventWorkplan model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
          // $this->layout='addformdatatablelayout';

          $this->layout = '@app/views/layouts/addformdatatablelayout';
          return $this->render('workplan', [
              'orderId' => $id,
          ]);

            // return $this->render('view', [
            //     'model' => $this->findModel($id),
            // ]);
        }

        /**
         * Creates a new EventWorkplan model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
          // $this->layout='addformdatatablelayout';

          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = new EventWorkplan();
            //Getting user Log in details


            //checking if its a post
            if ($model->load(Yii::$app->request->post())) {
              // code...

              //Geting the time created Timestamp
              $model->createdAt= new \yii\db\Expression('NOW()');

              //Getting user Log in details
              $model->userId = Yii::$app->user->identity->id;
              $model->createdBy = Yii::$app->user->identity->id;

              $model->save();

              //after Saving the customer Details
              return $this->redirect(['index']);

              // return $this->redirect(['view', 'id' => $model->id]);

            } else {
              // code...

              //load the create form
              return $this->renderAjax('create', [
                  'model' => $model,
              ]);
            }

        }

        /**
         * Updates an existing EventWorkplan model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
          // $this->layout='addformdatatablelayout';

          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = $this->findModel($id);

            $model->userId = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        /**
         * Deletes an existing EventWorkplan model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
          // $this->layout='addformdatatablelayout';

          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the EventWorkplan model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return EventWorkplan the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
          // $this->layout='addformdatatablelayout';

          $this->layout = '@app/views/layouts/addformdatatablelayout';
            if (($model = EventWorkplan::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
