<?php

namespace app\modules\finance\purchaseorder;

/**
 * purchaseorder module definition class
 */
class purchaseorder extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\purchaseorder\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Customer Purchase order Sub Module*/
          'purchaseordercustomer' => [
              'class' => 'app\modules\finance\purchaseorder\purchaseordercustomer\purchaseordercustomer',
          ],
        ];

        // custom initialization code goes here
    }
}
