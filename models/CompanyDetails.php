<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_details".
 *
 * @property int $id
 * @property string $companyName
 * @property string $companyAddress
 * @property string $companySuite
 * @property string $companyCity
 * @property string $companyCounty
 * @property string $companyPostal
 * @property string $companyCountry
 * @property string $companyLogo
 * @property int $companyStatus
 * @property string $companyTimestamp
 * @property string $companyupdatedAt
 * @property string $companyDeletedAt
 * @property int $companyUserId
 */
class CompanyDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyName', 'companyAddress', 'companySuite', 'companyCity', 'companyCountry', 'companyStatus'], 'required'],
            [['companyLogo'], 'string'],
            [['companyStatus', 'companyUserId'], 'integer'],
            [['companyTimestamp', 'companyupdatedAt', 'companyDeletedAt', 'companyUserId'], 'safe'],
            [['companyName', 'companyAddress', 'companySuite', 'companyCity', 'companyCounty', 'companyPostal', 'companyCountry'], 'string', 'max' => 255],
            [['companyName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'companyName' => 'Company Name',
            'companyAddress' => 'Company Address',
            'companySuite' => 'Company Suite',
            'companyCity' => 'Company City',
            'companyCounty' => 'Company County',
            'companyPostal' => 'Company Postal',
            'companyCountry' => 'Company Country',
            'companyLogo' => 'Company Logo',
            'companyStatus' => 'Company Status',
            'companyTimestamp' => 'Company Timestamp',
            'companyupdatedAt' => 'Companyupdated At',
            'companyDeletedAt' => 'Company Deleted At',
            'companyUserId' => 'Company User ID',
        ];
    }
}
