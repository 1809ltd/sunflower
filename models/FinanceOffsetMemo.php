<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_offset_memo".
 *
 * @property int $id
 * @property int $offsetMemoAccount
 * @property string $refernumber
 * @property string $offsetMemoTxnDate
 * @property string $offsetMemoNote
 * @property int $customerId
 * @property double $offsetAmount
 * @property int $offsetMemoStatus
 * @property string $offsetMemoCreatedAt
 * @property string $offsetMemoUpdatedAt
 * @property string $offsetMemoDeletedAt
 * @property int $userId
 *
 * @property CustomerDetails $customer
 * @property FinanceAccounts $offsetMemoAccount0
 * @property UserDetails $user
 * @property FinanceOffsetMemoLines[] $financeOffsetMemoLines
 */
class FinanceOffsetMemo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_offset_memo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['offsetMemoAccount', 'refernumber', 'offsetMemoTxnDate', 'customerId', 'offsetAmount', 'offsetMemoStatus', 'userId'], 'required'],
            [['offsetMemoAccount', 'customerId', 'offsetMemoStatus', 'userId'], 'integer'],
            [['offsetMemoTxnDate', 'offsetMemoCreatedAt', 'offsetMemoUpdatedAt', 'offsetMemoDeletedAt'], 'safe'],
            [['offsetMemoNote'], 'string'],
            [['offsetAmount'], 'number'],
            [['refernumber'], 'string', 'max' => 50],
            [['refernumber'], 'unique'],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['offsetMemoAccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['offsetMemoAccount' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'offsetMemoAccount' => 'Offset Memo Account',
            'refernumber' => 'Refernumber',
            'offsetMemoTxnDate' => 'Offset Memo Txn Date',
            'offsetMemoNote' => 'Offset Memo Note',
            'customerId' => 'Customer ID',
            'offsetAmount' => 'Offset Amount',
            'offsetMemoStatus' => 'Offset Memo Status',
            'offsetMemoCreatedAt' => 'Offset Memo Created At',
            'offsetMemoUpdatedAt' => 'Offset Memo Updated At',
            'offsetMemoDeletedAt' => 'Offset Memo Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffsetMemoAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'offsetMemoAccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOffsetMemoLines()
    {
        return $this->hasMany(FinanceOffsetMemoLines::className(), ['offsetMemoId' => 'id']);
    }
    public function getOffstnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(refernumber) AS number from finance_offset_memo where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "Offst-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "Offst-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "Offst-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
