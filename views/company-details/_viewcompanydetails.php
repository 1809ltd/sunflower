<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="company-department-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
          ': class' => 'table table-bordered table-striped' ,
          'Width' => '100%' ,
          'Cellspacing' => '0'
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            //'id',
            // 'companyName',
            // 'companyAddress',
            // 'companySuite',
            // 'companyCity',
            'companyCounty',
            'companyPostal',
            'companyCountry',
            //'companyLogo:ntext',
            // 'companyStatus',
            //'companyTimestamp',
            //'companyupdatedAt',
            //'companyDeletedAt',
            //'companyUserId',
        ],
    ]); ?>

</div>
