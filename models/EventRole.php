<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_role".
 *
 * @property int $id
 * @property string $roleName
 * @property string $roleDescription
 * @property int $status
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property EventItemRole[] $eventItemRoles
 * @property FinanceItems[] $items
 */
class EventRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['roleName', 'roleDescription', 'status', 'userId'], 'required'],
            [['roleDescription'], 'string'],
            [['status', 'userId'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['roleName'], 'string', 'max' => 100],
            [['roleName'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'roleName' => 'Role Name',
            'roleDescription' => 'Role Description',
            'status' => 'Status',
            'userId' => 'User ID',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventItemRoles()
    {
        return $this->hasMany(EventItemRole::className(), ['roleId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(FinanceItems::className(), ['id' => 'itemId'])->viaTable('event_item_role', ['roleId' => 'id']);
    }
}
