<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/*Getting the product and service offered, customer, Tax*/


/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Event*/
use app\modules\event\eventInfo\models\EventDetails;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
/*Tax*/
use app\modules\finance\financesetup\tax\models\FinanceTax;
/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimate */
/* @var $form yii\widgets\ActiveForm */



$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
        jQuery(this).html("Module: " + (index + 1))
    });
});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
        jQuery(this).html("Module: " + (index + 1))
    });
});
';

$this->registerJs($js);
?>
<style>
.panel-body {
    padding-bottom: 0px;
}
</style>

<div class="box box-primary">

  <div class="box-body chart-responsive">

    <div class="col-md-12">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="box-body chart-responsive">


                          <?php
                              // necessary for update action.
                              if (! $model->isNewRecord) {
                                // code...
                                  $status = ['0' => 'Cancel', '1' => 'Active', '2' => 'Waiting'];
                                  echo $form->field($model, 'estimateTxnStatus')->dropDownList($status, ['readonly' => true,'prompt'=>'Select Status']);

                                }else{

                                    echo $form->field($model, 'estimateTxnStatus')->hiddenInput(['value'=> "2"])->label(false);

                              }
                          ?>

                      <div class="col-sm-2">
                        <div class="form-group">
                          <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                              'data' => ArrayHelper::map(CustomerDetails::find()->where('`customerstatus` = 1')->asArray()->all(),'id','customerDisplayName'),
                              'language' => 'en',
                              'options' => ['id'=> 'customer-id', 'placeholder' => 'Select a Company ...'],
                              'pluginOptions' => [
                                  'allowClear' => true
                              ],
                          ]); ?>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">

                          <?= $form->field($model, 'estimateProjectId')->widget(DepDrop::classname(), [
                              'options'=>['id'=>'project-id'],
                              'data' => [$model->estimateProjectId => $model->estimateProjectId],
                              'type' => DepDrop::TYPE_SELECT2,
                              'pluginOptions'=>[
                                  'depends'=>['customer-id'],
                                  'initialize' => true,
                                  // 'initDepends'=>['orderId'],
                                  'placeholder'=>'Select Clients Event...',
                                  'url'=>Url::to(['/finance-estimate/events'])
                              ]
                          ]);?>

                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">
                          <?= $form->field($model, 'localpurchaseOrder')->textInput(['maxlength' => true]) ?>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">
                          <?= $form->field($model, 'estimateTxnDate')->widget(\yii\jui\DatePicker::class, [
                                //'language' => 'ru',
                                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                                'inline' => false,
                                'dateFormat' => 'yyyy-MM-dd',
                            ]); ?>

                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">
                          <?= $form->field($model, 'estimateDeadlineDate')->widget(\yii\jui\DatePicker::class, [
                                //'language' => 'ru',
                                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                                'inline' => false,
                                'dateFormat' => 'yyyy-MM-dd',
                            ]); ?>

                        </div>
                      </div>

                      <div class="col-sm-2">
                        <div class="row">
                          <?= $form->field($model, 'estimateEmailStatus')->checkbox() ?>
                          <?= $form->field($model, 'estimateBillEmail')->textInput(['maxlength' => true]) ?>

                        </div>
                      </div>

        </div>



    <div class="panel panel-default" >
        <div class="panel-body">
          <?php DynamicFormWidget::begin([
             'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
             'widgetBody' => '.container-items', // required: css class selector
             'widgetItem' => '.item', // required: css class
             // 'limit' => 80, // the maximum times, an element can be cloned (default 999)
             'min' => 1, // 0 or 1 (default 1)
             'insertButton' => '.add-item', // css class
             'deleteButton' => '.remove-item', // css class
             'model' => $modelsFinanceEstimateLines[0],
             'formId' => 'dynamic-form',
             'formFields' => [

               'itemRefId',
               'itemRefName',
               'estimateLinesDescription',
               'estimateLinespack',
               'estimateLinesQty',
               'estimateLinesUnitPrice',
               'estimateLinesAmount',
               'estimateLinesTaxCodeRef',
               'estimateLinesTaxamount',
               'estimateLinesDiscount',

             ],
         ]); ?>

        <div class="container-items"><!-- widgetContainer -->
            <div class="caption font-red-sunglo" style="font-size: 15px;">
                <i class="fa fa-lock font-red-sunglo" ></i>
                <span class="caption-subject bold uppercase" > Qoute Items</span>
            </div><br />
            <!-- Loopping Items in the Lists -->
          <?php foreach ($modelsFinanceEstimateLines as $i => $modelFinanceEstimateLines): ?>

            <?php
            // foreach ($modelUserRoleModulePermissions as $i => $modelUserRoleModulePermission): ?>
                <div class="item"><!-- widgetBody -->

                    <div>
                      <?php
                          // necessary for update action.
                          if (! $modelFinanceEstimateLines->isNewRecord) {
                              echo Html::activeHiddenInput($modelFinanceEstimateLines, "[{$i}]id");
                          }
                      ?>

                        <div class="row box" style="float: center;width: 100%;">
                          <div class="box-header">
                            <h3 class="box-title"></h3>
                          </div>
                          <!-- /.box-header -->
                          <div class="pull-right">
                              <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                              <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                          </div>
                          <div class="pull-right">

                          </div>
                          <div class= "box-body">
                            <div class="row">
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]itemRefId")->widget(Select2::classname(), [
                                      'data' => ArrayHelper::map(FinanceItems::find()->where('`itemsStatus` = 1')->asArray()->all(),'id','itemsName'),
                                      'language' => 'en',
                                      'options' => ['prompt'=>'Select...'],
                                      'pluginOptions' => [
                                          'allowClear' => true
                                      ],
                                  ]);?>
                                </div>
                              </div>
                              <?php
                              //  $form->field($modelFinanceEstimateLines, "[{$i}]itemRefName")->hiddenInput([
                              // 'readonly' => true,
                              // // 'maxlength' => true,
                              // 'class' => 'prodName form-control'])->label(false);?>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]itemRefName")->textInput() ?>
                                </div>
                              </div><div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinesDescription")->textInput() ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinespack")->textInput([
                                  'maxlength' => true,
                                  'class' => 'packs form-control']) ?>
                                </div>
                              </div>

                            </div>

                            <!--  New row-->
                            <div class= "row">
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinesQty")->textInput([
                                  'maxlength' => true,
                                  'class' => 'qnty form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinesUnitPrice")->textInput([
                                  'maxlength' => true,
                                  'class' => 'price form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinesDiscount")->textInput([
                                  'maxlength' => true,
                                  'class' => 'discountpart form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?php $TaxCodeRef =  ArrayHelper::map(FinanceTax::find()->andWhere(['taxStatus'=>1])->asArray()->all(),'taxRate','taxCode'); ?>
                                  <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinesTaxCodeRef")->dropDownList($TaxCodeRef, [
                                                  'prompt'=>'Select...',
                                                  'class' => 'taxcode form-control']); ?>
                                </div>
                              </div>

                        </div>
                        <!--  Last Row-->
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinesTaxamount")->textInput([
                              'readonly' => true,
                              'maxlength' => true,
                              'class' => 'sumTaxPart form-control']) ?>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <?= $form->field($modelFinanceEstimateLines, "[{$i}]estimateLinesAmount")->textInput([
                              'readonly' => true,
                              'maxlength' => true,
                              'class' => 'amount sumPart  form-control']) ?>
                            </div>
                          </div>


                        </div>

                          </div>

                        </div>
                        <!-- <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div> -->
                    </div>

                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>



    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'estimateNote')->textarea(['rows' => 6]) ?>

        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'estimateFooter')->textarea(['rows' => 6]) ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <?= $form->field($model, 'estimateDiscountAmount')->textInput(['readonly' => true,'class' => 'finaldiscount form-control']) ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <?= $form->field($model, 'estimateTaxAmount')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
        </div>
      </div>

      <div class="col-sm-3">
        <div class="form-group">
          <?= $form->field($model, 'estimateSubAmount')->textInput(['readonly' => true,'class' => 'total form-control']) ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <?= $form->field($model, 'estimateAmount')->textInput(['readonly' => true,'class' => 'amounttotal form-control']) ?>
        </div>
      </div>

      <?= $form->field($model, 'approvedby')->hiddenInput(['value'=> ""])->label(false);?>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>

  </div>

</div>

<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var amount = 0;
        var total = 0;
        var taxation=0;
        var tax=0
        var discount=0;
        var totaldiscount=0;
        var amounttotal= 0;

        items.each(function (index, elem) {

            //getting specific elements
            var qnty = $(elem).find(".qnty").val();
            var price = $(elem).find(".price").val();
            var packs = $(elem).find(".packs").val();
            var taxcode = $(elem).find(".taxcode").val();
            var discountpart = $(elem).find(".discountpart").val();

            //Getting the Value of the Product Selected.
            var prodN = $(elem).find(".prod option:selected").text();

            //Assign the Taxation value to the field
            $(elem).find(".prodName").val(prodN);


            //Check if qnty and price are numeric or something like that
            amount = parseFloat(qnty) * parseFloat(price) * parseFloat(packs);


            //getting disount value
            discount = (parseFloat(amount) * parseFloat(discountpart))/100;

            totaldiscount = parseFloat(totaldiscount) + parseFloat(discount);


            //assigning total discount at the end
            $(".finaldiscount").val(totaldiscount);


            var amountafterdiscount= parseFloat(amount) - parseFloat(discount);
            //Assign the amount value to the field
            $(elem).find(".amount").val(amountafterdiscount);

            var amountValue = $(elem).find(".amount").val();

            taxation= parseFloat(taxcode) * parseFloat(amountafterdiscount);

            //Assign the Taxation value to the field
            $(elem).find(".sumTaxPart").val(taxation);

            //getting the tax amount figures
            var taxValue = $(elem).find(".sumTaxPart").val();

            //getting the total of all figures
            total = parseFloat(total) + parseFloat(amountValue);

            //assing value to the Subtotal amount
            $(".total").val(total);

            //getting total tax amountValue
            tax = parseFloat(tax) + parseFloat(taxValue);

            //assing value to the total tax amount
            $(".tax").val(tax);

            amounttotal = tax+total;

            //assing value to the Amount total tax amount
            $(".amounttotal").val(amounttotal);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   $('#Roles').addClass('active open');
   $('#role').addClass('active open');"
, View::POS_READY);
?>
