<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VendorCompanyDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vendor-company-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vendorCompanyName') ?>

    <?= $form->field($model, 'vendorKraPin') ?>

    <?= $form->field($model, 'vendorContactPersonFName') ?>

    <?= $form->field($model, 'vendorContactPersonLName') ?>

    <?php // echo $form->field($model, 'vendorMobilePhone') ?>

    <?php // echo $form->field($model, 'contactMobilePhone') ?>

    <?php // echo $form->field($model, 'vendorPhone') ?>

    <?php // echo $form->field($model, 'contactEmail') ?>

    <?php // echo $form->field($model, 'vendoremail') ?>

    <?php // echo $form->field($model, 'vendorCompanyAddress') ?>

    <?php // echo $form->field($model, 'vendorCompanySuite') ?>

    <?php // echo $form->field($model, 'vendorCompanyCity') ?>

    <?php // echo $form->field($model, 'vendorCompanyCounty') ?>

    <?php // echo $form->field($model, 'vendorCompanyPostal') ?>

    <?php // echo $form->field($model, 'vendorCompanyCountry') ?>

    <?php // echo $form->field($model, 'vendorCompanyLogo') ?>

    <?php // echo $form->field($model, 'vendorTwitter') ?>

    <?php // echo $form->field($model, 'vendorFb') ?>

    <?php // echo $form->field($model, 'vendorIg') ?>

    <?php // echo $form->field($model, 'vendorType') ?>

    <?php // echo $form->field($model, 'vendorCompanyStatus') ?>

    <?php // echo $form->field($model, 'vendorCompanyTimestamp') ?>

    <?php // echo $form->field($model, 'vendorCompanyupdatedAt') ?>

    <?php // echo $form->field($model, 'vendorCompanyDeletedAt') ?>

    <?php // echo $form->field($model, 'companyUserId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
