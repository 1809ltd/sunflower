<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLeadCommunicationTracker */

$this->title = 'Update Customer Lead Communication Tracker: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Lead Communication Trackers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="customer-lead-communication-tracker-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
