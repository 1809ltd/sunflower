<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="finance-estimate-lines-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'reqId',
            'activityId',
            'personelId',
            'personnelTel',
            'description:ntext',
            'qty',
            'unitprice',
            'amont',
            //'status',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',
            //'userId',
        ],
    ]); ?>

</div>
