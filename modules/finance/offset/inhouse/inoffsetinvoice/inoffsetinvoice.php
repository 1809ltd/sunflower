<?php

namespace app\modules\finance\offset\inhouse\inoffsetinvoice;

/**
 * inoffsetinvoice module definition class
 */
class inoffsetinvoice extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\offset\inhouse\inoffsetinvoice\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
