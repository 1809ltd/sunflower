<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asset_check_in".
 *
 * @property int $id
 * @property int $checkOutId
 * @property int $assetId
 * @property string $returnDate
 * @property int $lcationId
 * @property int $checkinBy
 * @property string $timestamp
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $status
 * @property int $athurizedBy
 *
 * @property AssetCheckOut $checkOut
 * @property AssetRegistration $asset
 * @property UserDetails $athurizedBy0
 * @property CompanySiteLocation $lcation
 * @property UserDetails $checkinBy0
 */
class AssetCheckIn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_check_in';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['checkOutId', 'assetId', 'returnDate', 'lcationId', 'checkinBy', 'status'], 'required'],
            [['checkOutId', 'assetId', 'lcationId', 'checkinBy', 'status', 'athurizedBy'], 'integer'],
            [['returnDate', 'timestamp', 'updatedAt', 'deletedAt'], 'safe'],
            [['checkOutId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetCheckOut::className(), 'targetAttribute' => ['checkOutId' => 'id']],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['athurizedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['athurizedBy' => 'id']],
            [['lcationId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanySiteLocation::className(), 'targetAttribute' => ['lcationId' => 'id']],
            [['checkinBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['checkinBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'checkOutId' => 'Check Out ID',
            'assetId' => 'Asset ID',
            'returnDate' => 'Return Date',
            'lcationId' => 'Lcation ID',
            'checkinBy' => 'Checkin By',
            'timestamp' => 'Timestamp',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'athurizedBy' => 'Athurized By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckOut()
    {
        return $this->hasOne(AssetCheckOut::className(), ['id' => 'checkOutId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAthurizedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'athurizedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLcation()
    {
        return $this->hasOne(CompanySiteLocation::className(), ['id' => 'lcationId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckinBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'checkinBy']);
    }
}
