<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventRequirementsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-requirements-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'eventId') ?>

    <?= $form->field($model, 'eventNumber') ?>

    <?= $form->field($model, 'department') ?>

    <?= $form->field($model, 'departmentName') ?>

    <?php // echo $form->field($model, 'requirementDetails') ?>

    <?php // echo $form->field($model, 'requirementSize') ?>

    <?php // echo $form->field($model, 'requirementSIUnit') ?>

    <?php // echo $form->field($model, 'requirementCreatedAt') ?>

    <?php // echo $form->field($model, 'requirementLastUpdated') ?>

    <?php // echo $form->field($model, 'requirementDeletedAt') ?>

    <?php // echo $form->field($model, 'preparedBy') ?>

    <?php // echo $form->field($model, 'approvedBy') ?>

    <?php // echo $form->field($model, 'requirementStatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
