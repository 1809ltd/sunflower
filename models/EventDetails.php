<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_details".
 *
 * @property int $id
 * @property int $customerId
 * @property string $eventNumber
 * @property string $eventName
 * @property string $eventTheme
 * @property double $eventVistorsNumber
 * @property int $eventCategory
 * @property string $eventStartDate
 * @property string $eventEndDate
 * @property string $eventSetUpDateTime
 * @property string $eventSetDownDateTime
 * @property string $eventLocation
 * @property string $eventDescription
 * @property string $eventNotes
 * @property string $eventCreatedAt
 * @property string $eventLastUpdatedAt
 * @property string $eventDeletedAt
 * @property int $userId
 *
 * @property CustomerDetails $customer
 * @property EventOrderfrom[] $eventOrderfroms
 * @property EventRequirements[] $eventRequirements
 * @property FinanceCustomerPurchaseOrder[] $financeCustomerPurchaseOrders
 * @property FinanceEstimate[] $financeEstimates
 * @property FinanceEventRequstionForm[] $financeEventRequstionForms
 * @property FinanceInvoice[] $financeInvoices
 */
class EventDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerId', 'eventNumber', 'eventName', 'eventTheme', 'eventVistorsNumber', 'eventCategory', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventLocation', 'eventDescription', 'eventNotes', 'userId'], 'required'],
            [['customerId', 'eventCategory', 'userId'], 'integer'],
            [['eventVistorsNumber'], 'number'],
            [['eventStartDate', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventCreatedAt', 'eventLastUpdatedAt', 'eventDeletedAt'], 'safe'],
            [['eventDescription', 'eventNotes'], 'string'],
            [['eventNumber', 'eventName', 'eventTheme'], 'string', 'max' => 100],
            [['eventLocation'], 'string', 'max' => 50],
            [['eventNumber'], 'unique'],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customerId' => 'Customer',
            'eventNumber' => 'Event Number',
            'eventName' => 'Name',
            'eventTheme' => 'Theme',
            'eventVistorsNumber' => 'Guest Number',
            'eventCategory' => 'Category',
            'eventStartDate' => 'Start Date',
            'eventEndDate' => 'End Date',
            'eventSetUpDateTime' => 'Set Up Date Time',
            'eventSetDownDateTime' => 'Set Down Date Time',
            'eventLocation' => 'Location',
            'eventDescription' => 'Description',
            'eventNotes' => 'Notes',
            'eventCreatedAt' => 'Created At',
            'eventLastUpdatedAt' => 'Last Updated At',
            'eventDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfroms()
    {
        return $this->hasMany(EventOrderfrom::className(), ['eventId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRequirements()
    {
        return $this->hasMany(EventRequirements::className(), ['eventNumber' => 'eventNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerPurchaseOrders()
    {
        return $this->hasMany(FinanceCustomerPurchaseOrder::className(), ['purchaseOrderProjectId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEstimates()
    {
        return $this->hasMany(FinanceEstimate::className(), ['estimateProjectId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionForms()
    {
        return $this->hasMany(FinanceEventRequstionForm::className(), ['eventId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoices()
    {
        return $this->hasMany(FinanceInvoice::className(), ['invoiceProjectId' => 'id']);
    }

    // public static function getEvents()
    // {
    //     $projects = self::find()
    //         ->select(['id','eventName as name'])
    //         // ->where(['customerId' => $customer_id])
    //         ->asArray()
    //         ->all();
    //     return $projects;
    // }


    /*Getting All the events from a customer*/
    public static function getCustomerEventList($customer_id)
    {
        $projects = self::find()
            ->select(['id','eventName as name'])
            ->where(['customerId' => $customer_id])
            ->asArray()
            ->all();
        return $projects;
    }
    /*Getting All the events from a customer*/
    public static function getEventCategory($project_id)
    {
        $connection = Yii::$app->db;
        $query= "SELECT event_caterogies.id AS id, event_caterogies.eventCatName AS name FROM event_details, event_caterogies WHERE event_details.id = $project_id AND event_caterogies.id = event_details.eventCategory";
        $eventcategory= $connection->createCommand($query)->queryAll();

        return $eventcategory;
    }


    /*Getting All the events from a Project Name*/
    public static function getEventName($project_id)
    {
        $eventJina = self::find()
            ->select(['id','eventName as name'])
            ->where(['id' => $project_id])
            ->asArray()
            ->all();
        return $eventJina;
        // return self::find()
        //       ->select(['id','eventName as name'])
        //       ->where(['id' => $project_id])->indexBy('id')->column();
    }

    public function getEnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(eventNumber) AS number from event_details where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number = "EVNT-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "EVNT-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "EVNT-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
