<?php

namespace app\modules\event\eventsetup\eventitemrole;

/**
 * eventitemrole module definition class
 */
class eventitemrole extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventsetup\eventitemrole\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
