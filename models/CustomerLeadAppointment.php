<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_lead_appointment".
 *
 * @property int $id
 * @property int $leadId
 * @property string $leadAppointSubject
 * @property string $leadAppointStart
 * @property string $leadAppointEnd
 * @property string $leadAppointStatus
 * @property string $leadAppointNotes
 * @property string $leadAppointCreatedAt
 * @property string $leadAppointLastUpdatedAt
 * @property string $leadAppointDeletedAt
 * @property int $userId
 *
 * @property CustomerLead $lead
 * @property UserDetails $user
 */
class CustomerLeadAppointment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_lead_appointment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['leadId', 'leadAppointSubject', 'leadAppointStart', 'leadAppointEnd', 'leadAppointStatus', 'leadAppointNotes', 'userId'], 'required'],
            [['leadId', 'userId'], 'integer'],
            [['leadAppointSubject', 'leadAppointNotes'], 'string'],
            [['leadAppointStart', 'leadAppointEnd', 'leadAppointStatus', 'leadAppointCreatedAt', 'leadAppointLastUpdatedAt', 'leadAppointDeletedAt'], 'safe'],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerLead::className(), 'targetAttribute' => ['leadId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead',
            'leadAppointSubject' => 'Subject',
            'leadAppointStart' => 'Start',
            'leadAppointEnd' => 'End',
            'leadAppointStatus' => 'Status',
            'leadAppointNotes' => 'Notes',
            'leadAppointCreatedAt' => 'Created At',
            'leadAppointLastUpdatedAt' => 'Last Updated At',
            'leadAppointDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(CustomerLead::className(), ['id' => 'leadId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
