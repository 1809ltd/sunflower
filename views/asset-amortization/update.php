<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetAmortization */

$this->title = 'Update Asset Amortization: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Amortizations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-amortization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
