<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\models\FinanceCategoryType;

use kartik\select2\Select2;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceAccounts */
/* @var $form yii\widgets\ActiveForm  */

?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Accsst Valuation</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="finance-accounts-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'companyId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CompanyDetails::find()->all(),'id','companyName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Company ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                  <?= $form->field($model, 'accountName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'accountNumber')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'accountsDescription')->textarea(['rows' => 6]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'fullyQualifiedName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $subAccount = ['1' => 'True', '2' => 'False']; ?>
                    <?php
                  //   $form->field($model, 'subAccount')->dropDownList($subAccount, ['prompt'=>'Select Status',
                  //         'onChange'=>'$.post("index.php?r=finance-accounts/lists&subAccount='.'"+$(this).val(),function(data){
                  //         $("select#financeaccounts-accountparentid").html(data);
                  //       });'
                  // ]); ?>
                  <?= $form->field($model, 'subAccount')->dropDownList($subAccount, ['prompt'=>'Select Status',
                        'onChange'=>'$.post("../../../../finance-accounts/lists?subAccount='.'"+$(this).val(),function(data){
                        $("select#financeaccounts-accountparentid").html(data);
                      });'
                ]); ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $accountParent =  ArrayHelper::map(FinanceAccounts::find()->all(),'id','fullyQualifiedName'); ?>
                    <?= $form->field($model, 'accountParentId')->dropDownList($accountParent, ['prompt'=>'Select...']); ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <?php $accountsClassification = ['Assets' => 'Assets', 'Equity' => 'Equity', 'Liabilities' => 'Liabilities','Expenses' => 'Expenses','Revenue' => 'Revenue']; ?>
                  <?= $form->field($model, 'accountsClassification')->dropDownList($accountsClassification, ['prompt'=>'Select Status']); ?>
                </div>

                <div class="col-sm-6">

                  <?= $form->field($model, 'accountsType')->widget(Select2::classname(), [
                      'data' => ArrayHelper::map(FinanceCategoryType::find()->all(),'type','type'),
                      'language' => 'en',
                      'options' => ['id'=> 'accountsType-id', 'placeholder' => 'Select a Account Type ...'],
                      'pluginOptions' => [
                          'allowClear' => true
                      ],
                  ]); ?>

                </div>
                <div class="col-sm-6">
                  <?= $form->field($model, 'accountsDetailType')->widget(DepDrop::classname(), [
                      'options'=>['id'=>'accountsDetailType-id'],
                      'data' => [$model->accountsDetailType => $model->accountsDetailType],
                      'type' => DepDrop::TYPE_SELECT2,
                      'pluginOptions'=>[
                          'depends'=>['accountsType-id'],
                          'initialize' => true,
                          // 'initDepends'=>['orderId'],
                          'placeholder'=>'Select sub Category Type...',
                          'url'=>Url::to(['/finance-category-type/lists'])
                      ]
                  ]);?>

                </div>


                <div class="col-sm-4">
                  <div class="form-group">

                    <?= $form->field($model, 'accountsCurrentBalance')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <?php $status = ['no' => 'no', 'yes' => 'yes']; ?>

                    <?= $form->field($model, 'accountsPays')->dropDownList($status, ['prompt'=>'Please select']); ?>

                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'accountsStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                  </div>
                </div>

                <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

                <div class="col-sm-3">
                  <div class="form-group">
                      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>


                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
