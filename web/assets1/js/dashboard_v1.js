"use strict";
var ass = function () {
    "use strict";
    var blue = "#007bff",
        green = "#28a745",
        dark = "#2d353c",
        grey = "#b6c2c9",
        red = "#dc3545";

    var seriesData = [ [], [] ];
    var random = new Rickshaw.Fixtures.RandomData(50);
    for (var i = 0; i < 50; i++) {
        random.addData(seriesData);
    }
    var graph = new Rickshaw.Graph({
        element : document.querySelector("#rs1"),
        height  : 70,
        renderer: 'area',
        series  : [
            {
                data : seriesData[ 0 ],
                color: blue,
                name : 'DB Server'
            }, {
                data : seriesData[ 1 ],
                color: grey,
                name : 'Web Server'
            }
        ]
    });
    var hoverDetail = new Rickshaw.Graph.HoverDetail({
        graph: graph
    });
    random.removeData(seriesData);
    random.addData(seriesData);
    graph.update();
    setInterval(function () {
        random.removeData(seriesData);
        random.addData(seriesData);
        graph.update();
        $("#change_random").text(Math.floor(Math.random() * 100));
        $("#change_random_per").text(Math.floor(Math.random() * 10)+"%");
    }, 1000);
    new ResizeSensor($('#content'), function () {
        graph.configure({
            width : $('#rs1').width(),
            height: $('#rs1').height()
        });
        graph.render();
    });

    /****rs2****/
    var seriesdata = [ [] ];
    var random_1 = new Rickshaw.Fixtures.RandomData(14);
    for (var i = 0; i < 15; i++) {
        random_1.addData(seriesdata);
    }
    var graph_2 = new Rickshaw.Graph({
        element : document.querySelector("#rs2"),
        renderer: 'bar',
        series  : [
            {
                data : seriesdata[ 0 ],
                color: blue,
                name : 'Site Traffic'
            }
        ]
    });
    random_1.removeData(seriesdata);
    random_1.addData(seriesdata);
    graph_2.update();
    setInterval(function () {
        random_1.removeData(seriesdata);
        random_1.addData(seriesdata);
        graph_2.update();
        $("#change_random1").text(Math.floor(Math.random() * 100));
        $("#change_random_per1").text(Math.floor(Math.random() * 10)+"%");
    }, 1000);
    new ResizeSensor($('#content'), function () {
        graph_2.configure({
            width : $('#rs2').width(),
            height: $('#rs2').height()
        });
        graph_2.render();
    });

    var rs3 = new Rickshaw.Graph({
        element : document.querySelector('#rs3'),
        renderer: 'line',
        series  : [ {
            data : [
                {x: 0, y: 5},
                {x: 1, y: 7},
                {x: 2, y: 10},
                {x: 3, y: 11},
                {x: 4, y: 12},
                {x: 5, y: 10},
                {x: 6, y: 9},
                {x: 7, y: 7},
                {x: 8, y: 6},
                {x: 9, y: 8},
                {x: 10, y: 9},
                {x: 11, y: 10},
                {x: 12, y: 7},
                {x: 13, y: 10}
            ],
            color: red
        } ]
    });
    rs3.render();
    // Responsive Mode
    new ResizeSensor($('#content'), function () {
        rs3.configure({
            width : $('#rs3').width(),
            height: $('#rs3').height()
        });
        rs3.render();
    });

    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    var dayNames = [ "S", "M", "T", "W", "T", "F", "S" ];

    var now = new Date(),
        month = now.getMonth() + 1,
        year = now.getFullYear();

    var events = [
        [
            '3/' + month + '/' + year,
            'Popover Title',
            '#',
            'rgb(45, 53, 60)',
            'Some contents here'
        ],
        [
            '14/' + month + '/' + year,
            'Tooltip with link',
            'http://www.pvrtechstudio.com/',
            'rgb(45, 53, 60)'
        ],
        [
            '20/' + month + '/' + year,
            'Popover with HTML Content',
            '#',
            'rgb(45, 53, 60)',
            'Some contents here <div class="text-right"><a href="http://www.google.com">Google It</a></div>'
        ],
        [
            '30/' + month + '/' + year,
            'PVR Taxi App Admin',
            'https://codecanyon.net/user/pvr_tech_studio',
            'rgb(45, 53, 60)',
        ]
    ];
    var calendarTarget = $('#schedule-calendar');
    $(calendarTarget).calendar({
        months         : monthNames,
        days           : dayNames,
        events         : events,
        popover_options: {
            placement: 'top',
            html     : true
        }
    });
    $(calendarTarget).find('td.event').each(function () {
        var backgroundColor = $(this).css('background-color');
        $(this).removeAttr('style');
        $(this).find('a').css('background-color', backgroundColor);
    });
    $(calendarTarget).find('.icon-arrow-left, .icon-arrow-right').parent().on('click', function () {
        $(calendarTarget).find('td.event').each(function () {
            var backgroundColor = $(this).css('background-color');
            $(this).removeAttr('style');
            $(this).find('a').css('background-color', backgroundColor);
        });
    });

    if ($('#ckeditorEmail').length) {
        CKEDITOR.config.uiColor = '#ffffff';
        CKEDITOR.config.toolbar = [ [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'About' ] ];
        CKEDITOR.config.height = 100;
        CKEDITOR.replace('ckeditor1');
    }

    $(function () {
        $("#sortable1, #sortable2").sortable({
            handle     : '.handle',
            connectWith: ".todo",
            update     : countTasks
        }).disableSelection();
    });

    $('.todo .checkbox > input[type="checkbox"]').on("click", function () {
        var $this = $(this).parent().parent().parent();

        if ($(this).prop('checked')) {
            $this.addClass("complete");

            $(this).parent().hide();

            $this.slideUp(500, function () {
                $this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
                $this.remove();
                countTasks();
            });
        } else {
            // insert undo code here...
        }
    });

    function countTasks() {
        $('.todo-group-title').each(function () {
            var $this = $(this);
            $this.find(".num-of-tasks").text($this.next().find("li").size());
        });
    }

};
var Assign = function () {
    "use strict";
    return {
        init: function () {
            ass();
        }
    }
}();
$(function () {
    Assign.init();
});