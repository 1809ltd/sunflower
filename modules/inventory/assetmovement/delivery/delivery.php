<?php

namespace app\modules\inventory\assetmovement\delivery;

/**
 * delivery module definition class
 */
class delivery extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\assetmovement\delivery\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
