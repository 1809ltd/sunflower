<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventItemRole */

$this->title = 'Update Event Service Role: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Service Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-item-role-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
