<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\models\CompanyDetails;
use app\models\AssetRegistration;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\AssetPreciation */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Accsst Valuation</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
             <div class="asset-preciation-form">

                <?php $form = ActiveForm::begin(); ?>



                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'companyId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CompanyDetails::find()->all(),'id','companyName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Company ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'assetId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(AssetRegistration::find()->all(),'id','assetName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Asset ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                  <?= $form->field($model, 'preciableCost')->textInput() ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                  <?= $form->field($model, 'preciationSalvageValue')->textInput() ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'assetLifeMonths')->textInput() ?>

                  </div>
                </div>

                <div class="col-sm-6">

                    <?= $form->field($model, 'preciationDateAcquired')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ]); ?>


                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'preciationStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Straight Line', '2' => 'Redusing Balance']; ?>
                    <?= $form->field($model, 'preciationDateAcquiredDepreciationMethod')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                  </div>
                </div>

                <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

                <div class="col-sm-3">
                  <div class="form-group">
                      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>


                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
