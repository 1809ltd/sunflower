<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/*GEtting the Bills*/
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLinesSearch;
/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments */


$this->title = $model->reqrecepitNumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Requisition Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="no-print finance-requisition-payments-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    // DetailView::widget([
    //     'model' => $model,
    //     'attributes' => [
    //       'id',
    //       'reqpaymentPayType',
    //       'reqpaymentaccount',
    //       'reqrecepitNumber',
    //       'reqpaymentTotalAmt',
    //       'unallocatedAmount',
    //       'date',
    //       'reqpaymenteventReqRef',
    //       'reqpaymentPrivateNote:ntext',
    //       'reqpaymentCreatedAt',
    //       'reqpaymentUpdatedAt',
    //       'reqpaymentDeletedAt',
    //       'userId',
    //       'createdBy',
    //       'approvedBy',
    //       'paymentstatus',
    //     ],
    // ]) ?>

</div>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h6>
           <img style="height:50%;width:50%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h6>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Payment Voucher: <?= $model->reqrecepitNumber ?></th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         To:
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         Contact:
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Tranasaction /Cheque No: #</b><?= $model->reqpaymenteventReqRef ?><br>
         <br>
         <b>Payment Account:</b><?= $model->reqpaymentaccount0["fullyQualifiedName"];?> <br>
         <b>Payment Date:</b> <?= Yii::$app->formatter->asDate($model->date, 'long');?><br>
         <b>Receipt Number:</b> <?= $model->reqrecepitNumber ?>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Requstion Number:</th>
             <th>Sub Amount</th>
           </thead>
           <tbody>
             <?php

             $eventReqs=$model->getallpaymentslinesforApayment($model->id,$model->paymentstatus);

             foreach ($eventReqs as $eventReq) {
               // code...


               ?>
               <tr>
                   <td><?=$eventReq["eventReq"]["requstionNumber"];?></td>
                   <td>Ksh <?= number_format($eventReq["amount"],2) ;?></td>
               </tr>

               <?php

             }

             // die();

              ?>

           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <br>
     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="" style="margin-top: 10px;">
           <?= $model->reqpaymentPrivateNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="" style="margin-top: 10px;">
          <?php
          // $model->invoiceFooter; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Payment Methods:  <?= $model->reqpaymentPayType?></p>
         <div class="col-md-8">
         </div>

         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">

           * If you have any questions concerning this Payment Voucher<br/>
            contact [<?= $model->createdBy0['userFName'].'  '.$model->createdBy0['userLName']?>, <br/>
           Phone Number:    <?= $model->createdBy0['userPhone']?><br/>
           Email:  <?= $model->createdBy0['userEmail']?>]
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">

         <p class="lead">Unallocated Amount <strong>Ksh <?= number_format($model->unallocatedAmount,2) ;?></strong></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Total:</th>
               <td>Ksh <?= number_format($model->reqpaymentTotalAmt,2) ;?></td>
             </tr>

           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->

       </div>
     </div>
   </section>
   <!-- /.content -->
