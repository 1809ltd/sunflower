<?php

namespace app\modules\inventory\asset\assetregistration\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;

/**
 * AssetRegistrationSearch represents the model behind the search form of `app\modules\inventory\asset\assetregistration\models\AssetRegistration`.
 */
class AssetRegistrationSearch extends AssetRegistration
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assetCategory', 'assetCategoryType', 'vendor', 'assetStatus', 'userId'], 'integer'],
            [['assetName', 'assetDescription', 'assetTag', 'assetpurchaseDate', 'assetBrand', 'assetModel', 'assetSerialNumber', 'assetQrCode', 'assetphoto', 'assetTimestamp', 'assetUpdatedAt', 'assetDeletedAt'], 'safe'],
            [['assetCost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetRegistration::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assetCategory' => $this->assetCategory,
            'assetCategoryType' => $this->assetCategoryType,
            'assetpurchaseDate' => $this->assetpurchaseDate,
            'vendor' => $this->vendor,
            'assetCost' => $this->assetCost,
            'assetTimestamp' => $this->assetTimestamp,
            'assetUpdatedAt' => $this->assetUpdatedAt,
            'assetDeletedAt' => $this->assetDeletedAt,
            'assetStatus' => $this->assetStatus,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'assetName', $this->assetName])
            ->andFilterWhere(['like', 'assetDescription', $this->assetDescription])
            ->andFilterWhere(['like', 'assetTag', $this->assetTag])
            ->andFilterWhere(['like', 'assetBrand', $this->assetBrand])
            ->andFilterWhere(['like', 'assetModel', $this->assetModel])
            ->andFilterWhere(['like', 'assetSerialNumber', $this->assetSerialNumber])
            ->andFilterWhere(['like', 'assetQrCode', $this->assetQrCode])
            ->andFilterWhere(['like', 'assetphoto', $this->assetphoto]);

        return $dataProvider;
    }
}
