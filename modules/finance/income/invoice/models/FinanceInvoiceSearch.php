<?php

namespace app\modules\finance\income\invoice\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\income\invoice\models\FinanceInvoice;

/**
 * FinanceInvoiceSearch represents the model behind the search form of `app\modules\finance\income\invoice\models\FinanceInvoice`.
 */
class FinanceInvoiceSearch extends FinanceInvoice
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'invoiceEmailStatus', 'invoicedCreatedby', 'approvedby'], 'integer'],
            [['invoiceNumber', 'customerId', 'localpurchaseOrder', 'invoiceProjectId', 'invoiceTxnDate', 'invoiceDeadlineDate', 'invoiceNote', 'invoiceFooter', 'invoiceTxnStatus', 'invoiceBillEmail', 'invoicedCreatedAt', 'invoicedUpdatedAt', 'invoicedDeletedAt'], 'safe'],
            [['invoiceTaxAmount', 'invoiceDiscountAmount', 'invoiceSubAmount', 'invoiceAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceInvoice::find();
        $query->joinWith(['customer']);
        $query->joinWith(['invoiceProject']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'customerId' => $this->customerId,
            // 'localpurchaseOrder' => $this->localpurchaseOrder,
            // 'invoiceProjectId' => $this->invoiceProjectId,
            'invoiceTxnDate' => $this->invoiceTxnDate,
            'invoiceDeadlineDate' => $this->invoiceDeadlineDate,
            'invoiceEmailStatus' => $this->invoiceEmailStatus,
            'invoiceTaxAmount' => $this->invoiceTaxAmount,
            'invoiceDiscountAmount' => $this->invoiceDiscountAmount,
            'invoiceSubAmount' => $this->invoiceSubAmount,
            'invoiceAmount' => $this->invoiceAmount,
            'invoicedCreatedby' => $this->invoicedCreatedby,
            'invoicedCreatedAt' => $this->invoicedCreatedAt,
            'invoicedUpdatedAt' => $this->invoicedUpdatedAt,
            'invoicedDeletedAt' => $this->invoicedDeletedAt,
            'approvedby' => $this->approvedby,
        ]);

        $query->andFilterWhere(['like', 'invoiceNumber', $this->invoiceNumber])
            ->andFilterWhere(['like', 'invoiceNote', $this->invoiceNote])
            ->andFilterWhere(['like', 'customerDisplayName', $this->customerId])
            ->andFilterWhere(['like', 'eventName', $this->invoiceProjectId])
            ->andFilterWhere(['like', 'invoiceFooter', $this->invoiceFooter])
            ->andFilterWhere(['like', 'invoiceTxnStatus', $this->invoiceTxnStatus])
            ->andFilterWhere(['like', 'invoiceBillEmail', $this->invoiceBillEmail]);

        return $dataProvider;
    }
}
