<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/*GEtting the Bills*/
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLinesSearch;

/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments */

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Office Requisition Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <div class="finance-office-requisition-payments-view">

      <h1><?= Html::encode($this->title) ?></h1>

      <p>
          <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
          <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
              'class' => 'btn btn-danger',
              'data' => [
                  'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                  'method' => 'post',
              ],
          ]) ?>
      </p>

      <?php
      // DetailView::widget([
      //     'model' => $model,
      //     'attributes' => [
      //         'id',
      //         'officeReqPaymentPayType',
      //         'officeReqPaymentaccount',
      //         'reqrecepitNumber',
      //         'officeReqPaymentTotalAmt',
      //         'unallocatedAmount',
      //         'date',
      //         'officeReqPaymentofficeReqRef',
      //         'officeReqPaymentPrivateNote:ntext',
      //         'officeReqPaymentCreatedAt',
      //         'officeReqPaymentUpdatedAt',
      //         'officeReqPaymentDeletedAt',
      //         'userId',
      //         'createdBy',
      //         'approvedBy',
      //         'paymentstatus',
      //     ],
      // ]) ?>

  </div>

</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h6>
           <img style="height:50%;width:50%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h6>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Payment Voucher: <?= $model->reqrecepitNumber ?></th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         To:
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         Contact:
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Tranasaction /Cheque No: #</b><?= $model->officeReqPaymentofficeReqRef ?><br>
         <br>
         <b>Payment Account:</b><?= $model->officeReqPaymentaccount0["fullyQualifiedName"];?> <br>
         <b>Payment Date:</b> <?= Yii::$app->formatter->asDate($model->date, 'long');?><br>
         <b>Receipt Number:</b> <?= $model->reqrecepitNumber ?>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Requstion Number:</th>
             <th>Sub Amount</th>
           </thead>
           <tbody>
             <?php

             echo "model id".$model->id;
             echo "model status".$model->paymentstatus;


             $officeReqPayments=$model->getallpaymentslinesforApayment($model->id,$model->paymentstatus);

             foreach ($officeReqPayments as $officeReqPayment) {
               // code...


               ?>
               <tr>
                   <td><?=$officeReqPayment["officeReq"]["requstionNumber"];?></td>
                   <td>Ksh <?= number_format($officeReqPayment["amount"],2) ;?></td>
               </tr>

               <?php

             }

             // die();

              ?>

           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <br>
     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="" style="margin-top: 10px;">
           <?= $model->officeReqPaymentPrivateNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="" style="margin-top: 10px;">
          <?php
          // $model->invoiceFooter; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Payment Methods:  <?= $model->officeReqPaymentPayType?></p>
         <div class="col-md-8">
         </div>

         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">

           * If you have any questions concerning this Payment Voucher<br/>
            contact [<?= $model->createdBy0['userFName'].'  '.$model->createdBy0['userLName']?>, <br/>
           Phone Number:    <?= $model->createdBy0['userPhone']?><br/>
           Email:  <?= $model->createdBy0['userEmail']?>]
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">

         <p class="lead">Unallocated Amount <strong>Ksh <?= number_format($model->unallocatedAmount,2) ;?></strong></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Total:</th>
               <td>Ksh <?= number_format($model->officeReqPaymentTotalAmt,2) ;?></td>
             </tr>

           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->

       </div>
     </div>
   </section>
   <!-- /.content -->
