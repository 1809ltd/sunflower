<?php

namespace app\modules\finance\financereport\companyfinancials\taxreport;

/**
 * taxreport module definition class
 */
class taxreport extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\companyfinancials\taxreport\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
