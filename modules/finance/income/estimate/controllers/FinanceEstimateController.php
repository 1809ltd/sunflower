<?php

namespace app\modules\finance\income\estimate\controllers;

use Yii;
use app\modules\finance\income\estimate\models\FinanceEstimate;
use app\modules\finance\income\estimate\models\FinanceEstimateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/

/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Estimate Items*/
use app\modules\finance\income\estimate\estimateitems\models\FinanceEstimateLines;
use app\modules\finance\income\estimate\estimateitems\models\FinanceEstimateLinesSearch;

/*Get Event Details*/
/*Event*/
use app\modules\event\eventInfo\models\EventDetails;

/*Generate Pdf*/
use kartik\mpdf\Pdf;

/*Coverting Estimate to Qoute*/
use app\modules\finance\income\invoice\models\FinanceInvoice;
use app\modules\finance\income\invoice\models\FinanceInvoiceSearch;
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLines;
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLinesSearch;

/**
 * FinanceEstimateController implements the CRUD actions for FinanceEstimate model.
 */
class FinanceEstimateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->estimatedUpdatedAt = date('Y-d-m h:i:s');

      }else {
        // code...
        $this->estimatedUpdatedAt = date('Y-d-m h:i:s');
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceEstimate models.
     * @return mixed
     */
    public function actionIndex()
    {

      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new FinanceEstimateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceEstimate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceEstimate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {

      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new FinanceEstimate();

        //Getting user Log in details
        $model->estimatedCreatedby = Yii::$app->user->identity->id;
        $modelsFinanceEstimateLines = [new FinanceEstimateLines];

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          // Geting the time created Timestamp
          $model->estimatedCreatedAt= date('Y-d-m h:i:s');

          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;

          $model->estimatedCreatedby = Yii::$app->user->identity->id;

          //Generating Estimate Number

          $estimateNum=$model->getEstnumber();
          $model->estimateNumber= $estimateNum;
          $model->save();

          //after Saving the customer Details

          $modelsFinanceEstimateLines = Model::createMultiple(FinanceEstimateLines::classname());
          Model::loadMultiple($modelsFinanceEstimateLines, Yii::$app->request->post());


          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceEstimateLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceEstimateLines as $modelFinanceEstimateLines) {

                        $modelFinanceEstimateLines->estimateId = $model->id;
                        $modelFinanceEstimateLines->createdBy = $model->estimatedCreatedby;
                        $modelFinanceEstimateLines->userId = $model->userId;
                        $modelFinanceEstimateLines->estimateLinesStatus = $model->estimateTxnStatus;


                          if (! ($flag = $modelFinanceEstimateLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }

          // return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('create', [
              'model' => $model,
              'modelsFinanceEstimateLines' => (empty($modelsFinanceEstimateLines)) ? [new FinanceEstimateLines] : $modelsFinanceEstimateLines
          ]);
        }

    }
    /**
     * Updates an existing FinanceEstimate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {

      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        $modelsFinanceEstimateLines = $this->getFinanceEstimateLines($model->id);

        if ($model->load(Yii::$app->request->post())) {

              // code...

              //Getting user Log in details
              $model->userId = Yii::$app->user->identity->id;

              $model->save();

              //after Saving the customer Details

              $oldIDs = ArrayHelper::map($modelsFinanceEstimateLines, 'id', 'id');
              $modelsFinanceEstimateLines = Model::createMultiple(FinanceEstimateLines::classname(), $modelsFinanceEstimateLines);
              Model::loadMultiple($modelsFinanceEstimateLines, Yii::$app->request->post());
              $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceEstimateLines, 'id', 'id')));

              // ajax validation
              if (Yii::$app->request->isAjax) {
                  Yii::$app->response->format = Response::FORMAT_JSON;
                  return ArrayHelper::merge(
                      ActiveForm::validateMultiple($modelsFinanceEstimateLines),
                      ActiveForm::validate($model)
                  );
              }

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceEstimateLines) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          if (! empty($deletedIDs)) {
                              FinanceEstimateLines::deleteAll(['id' => $deletedIDs]);
                          }
                          foreach ($modelsFinanceEstimateLines as $modelFinanceEstimateLines) {

                              $modelFinanceEstimateLines->estimateId = $model->id;
                              $modelFinanceEstimateLines->createdBy = $model->estimatedCreatedby;
                              $modelFinanceEstimateLines->userId = $model->userId;
                              $modelFinanceEstimateLines->estimateLinesStatus = $model->estimateTxnStatus;


                              if (! ($flag = $modelFinanceEstimateLines->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }
                  } catch (Exception $e) {
                      $transaction->rollBack();
                  }
              }


        } else {
          // code...

          //load the create form
          return $this->render('update', [
              'model' => $model,
              'modelsFinanceEstimateLines' => (empty($modelsFinanceEstimateLines)) ? [new FinanceEstimateLines] : $modelsFinanceEstimateLines
          ]);
        }

    }

    public function getFinanceEstimateLines($id)
    {
      $model = FinanceEstimateLines::find()->where(['estimateId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing FinanceEstimate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = $this->findModel($id);

      // $model =YourModel::find()-where(['id'=>$id])->one();
      $model->estimateTxnStatus = '0';
      $model->estimatedDeletedAt =date('Y-d-m h:i:s');
      $model->save();
      return $this->redirect(['index']);

        // $this->findModel($id)->delete();
        //
        // return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceEstimate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceEstimate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = FinanceEstimate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*Getting the customers specif Events*/
    public function actionEvents() {
      $out=[];
        // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset(Yii::$app->request->post()['depdrop_parents'])) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                $customer_id = $parents[0];

                $out = EventDetails::getCustomerEventList($customer_id);

                return json_encode(['output'=>$out, 'selected'=>'']);
            }
        }
        return json_encode(['output' => '', 'selected' => '']);
    }

    /*Getting the Project Category Name*/
    public function actionEventcategory() {

      // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      if (isset(Yii::$app->request->post()['depdrop_parents'])) {
          $ids = Yii::$app->request->post('depdrop_parents');
          $customer_id = empty($ids[0]) ? null : $ids[0];
          $project_id = empty($ids[1]) ? null : $ids[1];
          if ($customer_id != null && $project_id != null) {

              // $out = EventDetails::getCustomerEventList($customer_id);
              $out = EventDetails::getEventCategory($project_id);

              return json_encode(['output'=>$out, 'selected'=>'']);

          }
      }
      return json_encode(['output'=>'', 'selected'=>'']);

    }
    /*Getting the Project Category Name*/
    public function actionEventname() {

      // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      if (isset(Yii::$app->request->post()['depdrop_parents'])) {
          $ids = Yii::$app->request->post('depdrop_parents');
          $customer_id = empty($ids[0]) ? null : $ids[0];
          $project_id = empty($ids[1]) ? null : $ids[1];
          if ($customer_id != null && $project_id != null) {

              // $out = EventDetails::getCustomerEventList($customer_id);
              $out = EventDetails::getEventName($project_id);

              return json_encode(['output'=>$out, 'selected'=>'']);

          }
      }
      return json_encode(['output'=>'', 'selected'=>'']);

    }

    public function actionGeneratepdf($id) {

  //     Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
  //
  //     $pdf = new Pdf([
  //     'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
  //     'destination' => Pdf::DEST_BROWSER,
  //     'content' => $this->renderPartial('estimatepdf', [
  //         'model' => $this->findModel($id),
  //     ]),
  //
  //     'options' => [
  //         // any mpdf options you wish to set
  //     ],
  //     'methods' => [
  //         'SetTitle' => 'Qoute',
  //         'SetSubject' => 'Generating PDF files via yii2-mpdf extension has never been easy',
  //         'SetHeader' => ['Krajee Privacy Policy||Generated On: ' . date("r")],
  //         'SetFooter' => ['|Page {PAGENO}|'],
  //         'SetAuthor' => 'Kartik Visweswaran',
  //         'SetCreator' => 'Kartik Visweswaran',
  //         'SetKeywords' => 'Krajee, Yii2, Export, PDF, MPDF, Output, Privacy, Policy, yii2-mpdf',
  //     ]
  // ]);
  // return $pdf->render();

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('estimatepdf', [
            'model' => $this->findModel($id),
        ]);

         // $this->renderPartial('_reportView');

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
             // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>['Krajee Report Header'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }
    public function actionConvertinvoice($id)
    {

      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $modelFinanceEstimate = $this->findModel($id);
        $modelsFinanceEstimateLines = $this->getFinanceEstimateLines($modelFinanceEstimate->id);

        $model = new FinanceInvoice();

        $modelsFinanceInvoiceLines = [new FinanceInvoiceLines];

        /*Assign values of Estimate to Invoice*/

        $model->customerId= $modelFinanceEstimate->customerId;
        $model->localpurchaseOrder=  $modelFinanceEstimate->localpurchaseOrder;
        $model->invoiceProjectId= $modelFinanceEstimate->estimateProjectId;
        $model->invoiceTxnDate= $modelFinanceEstimate->estimateTxnDate;
        $model->invoiceDeadlineDate= $modelFinanceEstimate->estimateDeadlineDate;
        $model->invoiceNote= $modelFinanceEstimate->estimateNote;
        $model->invoiceFooter= $modelFinanceEstimate->estimateFooter;
        $model->invoiceTxnStatus= $modelFinanceEstimate->estimateTxnStatus;
        $model->invoiceEmailStatus= $modelFinanceEstimate->estimateEmailStatus;
        $model->invoiceBillEmail= $modelFinanceEstimate->estimateBillEmail;
        $model->invoiceTaxAmount= $modelFinanceEstimate->estimateTaxAmount;
        $model->invoiceDiscountAmount= $modelFinanceEstimate->estimateDiscountAmount;
        $model->invoiceSubAmount= $modelFinanceEstimate->estimateSubAmount;
        $model->invoiceAmount= $modelFinanceEstimate->estimateAmount;



        /**/


        // print_r($modelsFinanceEstimateLines);

        // echo $modelFinanceEstimateLines[$i]['itemRefId'];
        // die();

        // $modelFinanceInvoiceLines

        // $modelFinanceInvoiceLines = array();

          foreach ($modelsFinanceEstimateLines as $i => $modelsFinanceEstimateLines) {
            // code...
            // /*assigning Estimate Line to invoice Lines*/
            $modelsFinanceInvoiceLines[$i]["itemRefId"] = $modelsFinanceEstimateLines->itemRefId;
            $modelsFinanceInvoiceLines[$i]["itemRefName"] = $modelsFinanceEstimateLines->itemRefName;
            $modelsFinanceInvoiceLines[$i]["invoiceLinesDescription"] = $modelsFinanceEstimateLines->estimateLinesDescription;
            $modelsFinanceInvoiceLines[$i]["invoiceLinespack"] = $modelsFinanceEstimateLines->estimateLinespack;
            $modelsFinanceInvoiceLines[$i]["invoiceLinesQty"] = $modelsFinanceEstimateLines->estimateLinesQty;
            $modelsFinanceInvoiceLines[$i]["invoiceLinesUnitPrice"] = $modelsFinanceEstimateLines->estimateLinesUnitPrice;
            $modelsFinanceInvoiceLines[$i]["invoiceLinesAmount"] = $modelsFinanceEstimateLines->estimateLinesAmount;
            $modelsFinanceInvoiceLines[$i]["invoiceLinesTaxCodeRef"] = $modelsFinanceEstimateLines->estimateLinesTaxCodeRef;
            $modelsFinanceInvoiceLines[$i]["invoiceLinesTaxamount"] = $modelsFinanceEstimateLines->estimateLinesTaxamount;
            $modelsFinanceInvoiceLines[$i]["invoiceLinesDiscount"]= $modelsFinanceEstimateLines->estimateLinesDiscount;

            // $modelsFinanceInvoiceLines[]=$modelFinanceInvoiceLines;

          }
          //
          // var_dump($modelsFinanceEstimateLines);
          // var_dump($modelsFinanceInvoiceLines);
          // die();




        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          // Geting the time created Timestamp
          $model->invoicedCreatedAt= date('Y-d-m h:i:s');

          //Getting user Log in details
          // $model->userId= "1";

          //Getting user Log in details
          $model->invoicedCreatedby = Yii::$app->user->identity->id;
          $model->userId= Yii::$app->user->identity->id;;

          //Generating Estimate Number

          $invoiceNum=$model->getInvnumber();
          $model->invoiceNumber= $invoiceNum;
          $model->save();

          //after Saving the customer Details

          $modelsFinanceInvoiceLines = Model::createMultiple(FinanceInvoiceLines::classname());
          Model::loadMultiple($modelsFinanceInvoiceLines, Yii::$app->request->post());


          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceInvoiceLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceInvoiceLines as $modelFinanceInvoiceLines) {

                        $modelFinanceInvoiceLines->invoiceId = $model->id;
                        $modelFinanceInvoiceLines->createdBy = $model->invoicedCreatedby;
                        $modelFinanceInvoiceLines->userId = $model->userId;
                        $modelFinanceInvoiceLines->invoiceLinesStatus = $model->invoiceTxnStatus;
                        $modelFinanceInvoiceLines->approvedBy = $model->approvedby;


                          if (! ($flag = $modelFinanceInvoiceLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['../../income/invoice/finance-invoice/view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }

          // return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('_convertinvoice', [
              'model' => $model,
              'modelsFinanceInvoiceLines' => (empty($modelsFinanceInvoiceLines)) ? [new FinanceInvoiceLines] : $modelsFinanceInvoiceLines
          ]);
        }

    }
}
