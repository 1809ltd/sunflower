<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_items_category_type".
 *
 * @property int $id
 * @property int $financeItemCategoryId
 * @property string $financeItemCategoryTypeName
 * @property string $financeItemCategoryTypeDescription
 * @property int $financeItemCategoryTypeStatus
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property FinanceItems[] $financeItems
 * @property FinanceItemsCategory $financeItemCategory
 * @property UserDetails $user
 */
class FinanceItemsCategoryType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_items_category_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['financeItemCategoryId', 'financeItemCategoryTypeName', 'financeItemCategoryTypeStatus', 'userId'], 'required'],
            [['financeItemCategoryId', 'financeItemCategoryTypeStatus', 'userId'], 'integer'],
            [['financeItemCategoryTypeDescription'], 'string'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['financeItemCategoryTypeName'], 'string', 'max' => 50],
            [['financeItemCategoryTypeName'], 'unique'],
            [['financeItemCategoryId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItemsCategory::className(), 'targetAttribute' => ['financeItemCategoryId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'financeItemCategoryId' => 'Category',
            'financeItemCategoryTypeName' => 'Category Type Name',
            'financeItemCategoryTypeDescription' => 'Category Type Description',
            'financeItemCategoryTypeStatus' => 'Category Type Status',
            'userId' => 'User ID',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItems()
    {
        return $this->hasMany(FinanceItems::className(), ['itemCategoryType' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItemCategory()
    {
        return $this->hasOne(FinanceItemsCategory::className(), ['id' => 'financeItemCategoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
