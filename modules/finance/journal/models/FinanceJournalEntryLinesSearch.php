<?php

namespace app\modules\finance\journal\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\journal\models\FinanceJournalEntryLines;

/**
 * FinanceJournalEntryLinesSearch represents the model behind the search form of `app\modules\finance\journal\models\FinanceJournalEntryLines`.
 */
class FinanceJournalEntryLinesSearch extends FinanceJournalEntryLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'journalId', 'accountId', 'transactionId', 'status', 'userId'], 'integer'],
            [['postingType', 'description', 'createAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['drAmount', 'crAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceJournalEntryLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'journalId' => $this->journalId,
            'accountId' => $this->accountId,
            'transactionId' => $this->transactionId,
            'drAmount' => $this->drAmount,
            'crAmount' => $this->crAmount,
            'createAt' => $this->createAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'status' => $this->status,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'postingType', $this->postingType])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
