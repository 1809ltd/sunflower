<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceEstimate;

/**
 * FinanceEstimateSearch represents the model behind the search form of `app\models\FinanceEstimate`.
 */
class FinanceEstimateSearch extends FinanceEstimate
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customerId', 'localpurchaseOrder', 'estimateProjectId', 'estimateEmailStatus', 'estimatedCreatedby', 'approvedby'], 'integer'],
            [['estimateNumber', 'estimateTxnDate', 'estimateDeadlineDate', 'estimateNote', 'estimateFooter', 'estimateTxnStatus', 'estimateBillEmail', 'estimatedCreatedAt', 'estimatedUpdatedAt', 'estimatedDeletedAt'], 'safe'],
            [['estimateTaxAmount', 'estimateDiscountAmount', 'estimateSubAmount', 'estimateAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceEstimate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customerId' => $this->customerId,
            'localpurchaseOrder' => $this->localpurchaseOrder,
            'estimateProjectId' => $this->estimateProjectId,
            'estimateTxnDate' => $this->estimateTxnDate,
            'estimateDeadlineDate' => $this->estimateDeadlineDate,
            'estimateEmailStatus' => $this->estimateEmailStatus,
            'estimateTaxAmount' => $this->estimateTaxAmount,
            'estimateDiscountAmount' => $this->estimateDiscountAmount,
            'estimateSubAmount' => $this->estimateSubAmount,
            'estimateAmount' => $this->estimateAmount,
            'estimatedCreatedby' => $this->estimatedCreatedby,
            'estimatedCreatedAt' => $this->estimatedCreatedAt,
            'estimatedUpdatedAt' => $this->estimatedUpdatedAt,
            'estimatedDeletedAt' => $this->estimatedDeletedAt,
            'approvedby' => $this->approvedby,
        ]);

        $query->andFilterWhere(['like', 'estimateNumber', $this->estimateNumber])
            ->andFilterWhere(['like', 'estimateNote', $this->estimateNote])
            ->andFilterWhere(['like', 'estimateFooter', $this->estimateFooter])
            ->andFilterWhere(['like', 'estimateTxnStatus', $this->estimateTxnStatus])
            ->andFilterWhere(['like', 'estimateBillEmail', $this->estimateBillEmail]);

        return $dataProvider;
    }
}
