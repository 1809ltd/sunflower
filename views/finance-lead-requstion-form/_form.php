<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;


/*Getting the product and service offered, customer, Tax*/
use app\models\FinanceLeadRequstionFormList;
use app\models\PersonnelDetails;
// use app\models\EventWorkplan;
use app\models\FinanceEventActivity;

use app\models\CustomerLead;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequstionForm */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row" >

      <div class="finance-lead-requstion-form-form">

          <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

          <div class="col-sm-4">
            <div class="form-group">
              <?php
                  // necessary for update action.
                  if (! $model->isNewRecord) {
                    // code...

                    $requstion = $model->requstionNumber;

                    echo $form->field($model, 'requstionNumber')->textInput(['readonly' => true, 'value' => $requstion]);

                  }

              ?>

            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'leadId')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(CustomerLead::find()->all(),'id','leadprojectName'),
                  'language' => 'en',
                  'options' => ['id'=> 'lead-id', 'placeholder' => 'Select a Lead ...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);
              ?>

            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                    //'language' => 'ru',
                    'options' => ['class' => 'form-control'],
                    'inline' => false,
                    'dateFormat' => 'yyyy-MM-dd',
                ]); ?>

            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
              <?= $form->field($model, 'requstionStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">

            </div>
          </div>


          <div class="panel-body"></div>

          <div class="panel-body">

            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Add Requisition Item</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
              </div>


              <div class="panel-body">
                <?php DynamicFormWidget::begin([
                   'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                   'widgetBody' => '.container-items', // required: css class selector
                   'widgetItem' => '.item', // required: css class
                   'limit' => 80, // the maximum times, an element can be cloned (default 999)
                   'min' => 1, // 0 or 1 (default 1)
                   'insertButton' => '.add-item', // css class
                   'deleteButton' => '.remove-item', // css class
                   'model' => $modelsFinanceLeadRequstionFormList[0],
                   'formId' => 'dynamic-form',
                   'formFields' => [

                     // 'id',
                     // 'reqId',
                     'activityId',
                     'personelId',
                     'personnelTel',
                     'description',
                     'qty',
                     'unitprice',
                     'amont',
                     // 'status',
                     // 'createdAt',
                     // 'updatedAt',
                     // 'deletedAt',
                     // 'userId',


                   ],
               ]); ?>

                  <div class="container-items"><!-- widgetContainer -->
                    <!-- Loopping Items in the Lists -->
                  <?php foreach ($modelsFinanceLeadRequstionFormList as $i => $modelFinanceLeadRequstionFormList): ?>
                      <div class="item box-header with-border"><!-- widgetBody -->
                          <h3 class="box-title">Requisition Item</h3>

                          <div class="box-tools pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>

                          </div>
                          <div class="box-body">
                            <?php
                                // necessary for update action.
                                if (! $modelFinanceLeadRequstionFormList->isNewRecord) {
                                    echo Html::activeHiddenInput($modelFinanceLeadRequstionFormList, "[{$i}]id");
                                }
                            ?>

                            <div class="col-sm-3">
                              <div class="form-group">
                              <?= $form->field($modelFinanceLeadRequstionFormList, "[{$i}]activityId")->widget(Select2::classname(), [
                                  'data' => ArrayHelper::map(FinanceEventActivity::find()->all(),'id','activityName'),
                                  'language' => 'en',
                                  'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]); ?>
                              </div>
                            </div>

                            <div class="col-sm-3">
                              <div class="form-group">
                              <?= $form->field($modelFinanceLeadRequstionFormList, "[{$i}]personelId")->widget(Select2::classname(), [
                                  'data' => ArrayHelper::map(PersonnelDetails::find()->all(),'id','displayName'),
                                  'language' => 'en',
                                  'pluginOptions' => [
                                      'allowClear' => true
                                  ],
                              ]); ?>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceLeadRequstionFormList, "[{$i}]personnelTel")->textInput([
                                'maxlength' => true,
                                'class' => 'form-control']) ?>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceLeadRequstionFormList, "[{$i}]description")->textInput() ?>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceLeadRequstionFormList, "[{$i}]qty")->textInput([
                                'maxlength' => true,
                                'class' => 'qnty form-control']) ?>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceLeadRequstionFormList, "[{$i}]unitprice")->textInput([
                                'maxlength' => true,
                                'class' => 'price form-control']) ?>
                              </div>
                            </div>
                           <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceLeadRequstionFormList, "[{$i}]amont")->textInput([
                                'readonly' => true,
                                'maxlength' => true,
                                'class' => 'sumPart  form-control']) ?>
                              </div>
                            </div>


                            <div class="col-sm-3">
                              <div class="form-group">

                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">

                              </div>
                            </div>

                          </div>
                      </div>
                  <?php endforeach; ?>
                  </div>
                  <?php DynamicFormWidget::end(); ?>
              </div>
          </div>

          </div>

            <div class="panel-body"></div>

            <div class="col-sm-6">
              <div class="form-group">
                <?= $form->field($model, 'requstionNote')->textarea(['rows' => 6]) ?>

              </div>
            </div>

            <div class="col-sm-3">
              <div class="form-group">
                <?= $form->field($model, 'amount')->textInput(['readonly' => true,'class' => 'amounttotal form-control']) ?>
              </div>
            </div>



            <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>
            <?= $form->field($model, 'preparedBy')->hiddenInput(['value'=> "1"])->label(false);?>


            <?= $form->field($model, 'approvedBy')->hiddenInput(['value'=> ""])->label(false);?>


            <div class="col-sm-3">
              <div class="form-group">

              </div>
            </div>

            <div class="panel-body"></div>

      </div>

      </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <?= Html::submitButton($modelFinanceLeadRequstionFormList->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
<!-- /.box -->


<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var amount = 0;
        var total = 0;
        var amounttotal= 0;

        items.each(function (index, elem) {

            //getting specific elements
            var qnty = $(elem).find(".qnty").val();
            var price = $(elem).find(".price").val();

            //Check if qnty and price are numeric or something like that
            amount = parseFloat(qnty) * parseFloat(price);

            //Assign the Taxation value to the field
            $(elem).find(".sumPart").val(amount);

            amounttotal = amounttotal+amount;

            //assing value to the Amount total tax amount
            $(".amounttotal").val(amounttotal);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
