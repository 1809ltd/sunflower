<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOfficeRequstionFormList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-office-requstion-form-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reqId')->textInput() ?>

    <?= $form->field($model, 'activityId')->textInput() ?>

    <?= $form->field($model, 'personelId')->textInput() ?>

    <?= $form->field($model, 'personnelTel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'unitprice')->textInput() ?>

    <?= $form->field($model, 'amont')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
