  <?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;
/*Event Transactions*/
use app\modules\finance\financereport\companyfinancials\models\EventTransaction;

$eventTransactions=  EventTransaction::find()->select(['*'])
                      ->where(['eventId' => $eventId])->all();
//
// print_r($eventTransactions);
//
//


$this->title = $eventTransactions[0]["eventNumber"];
$this->params['breadcrumbs'][] = ['label' => 'Event Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();



?>
<!-- this row will not appear when printing -->
<div class="row no-print">


</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h6>
           <img style="height:85%;width:85%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h6>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Event Transaction</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>

     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">

         <address>
           <strong>Client: </strong> <?= $eventTransactions[0]['customer']['customerCompanyName']?><br/>
           <strong>Kra Pin: </strong><?= $eventTransactions[0]['customer']['customerKraPin']?><br/>
           <strong> No of guests:</strong> <?= $eventTransactions[0]['invoiceProject']['eventVistorsNumber']?> Pax<br/>
             <strong>Date of Function: </strong> <?= $eventTransactions[0]['invoiceProject']['eventStartDate']?><br/>
             <strong>Date of Set Up: </strong> <?= $eventTransactions[0]['invoiceProject']['eventSetUpDateTime']?><br/>

         Attn: <?= $eventTransactions[0]['customer']['customerFullname']?>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

         <strong>Colour Scheme: </strong> <?= $eventTransactions[0]['invoiceProject']['eventTheme']?><br/>
         <strong>Event: </strong> <?= $eventTransactions[0]['invoiceProject']['eventName']?> <br/>
         <strong>Location: </strong> <?= $eventTransactions[0]['invoiceProject']['eventLocation']?><br/>
     </address>

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="text-left">Transactions</th>
             <th class="text-right">Income</th>
             <th class="text-right">Expense</th>
             <th class="text-right">Net</th>
           </thead>
           <tbody>
             <?php
             $net=0;

             foreach ($eventTransactions as $eventTransaction) {
               // code...
               // print_r($eventTransaction);
               if (empty($eventTransaction->dr_amount)) {
                 // code...
                 $eventTransaction->dr_amount=0;
               }
               if (empty($eventTransaction->cr_amount)) {
                 // code...
                 $eventTransaction->cr_amount=0;
               }
               // Brackets if value is negative
               $replaceamountcr = preg_replace(
                  '/(-)([\d\.\,]+)/ui',
                  '($2)',
                  number_format($eventTransaction["cr_amount"],2,'.',',')
              );
              // Brackets if value is negative
              $replaceamountdr = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($eventTransaction["dr_amount"],2,'.',',')
             );
               $net = $net+$eventTransaction->dr_amount-$eventTransaction->cr_amount;

               // Brackets if value is negative
               $replaceamountnet = preg_replace(
                  '/(-)([\d\.\,]+)/ui',
                  '($2)',
                  number_format($net,2,'.',',')
              );


               ?>
               <tr>
                 <td class="text-left"> Transcation Code: <?= $eventTransaction->transactionNumber;?>.
                   <br/>
                   <small> <?= $eventTransaction->transName;?> <br> Description: <?= $eventTransaction->transDesc;?>  .
                   </small>
                   <br/>
                 </td>
                 <td class="text-right"><?= $replaceamountdr ;?></td>
                 <td class="text-right"><?= $replaceamountcr ;?></td>
                 <td class="text-right"><?= $replaceamountnet ;?></td>
               </tr>
               <?php
               // die();
             }
             ?>
           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">


         <div class="col-md-8">
         </div>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">

         <?php
         // Brackets if value is negative
        //  $replaceamountnet = preg_replace(
        //     '/(-)([\d\.\,]+)/ui',
        //     '($2)',
        //     number_format($net,2,'.',',')
        // );


          ?>

         <p class="lead text-right">Event Net: <strong> Ksh <?= $replaceamountnet ;?></strong></p>

         <div class="table-responsive">

        </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>

        <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF -->
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
