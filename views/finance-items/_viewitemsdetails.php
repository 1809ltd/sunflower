<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="finance-items-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

          'id',
          'itemsName',
          'itemsCode',
          'itemsDescription:ntext',
          'itemsTaxable',
          //'itemsCustomerUnitPrice',
          //'itemsSupplierUnitPrice',
          //'itemsClassification',
          //'itemsIncomeAccountRef',
          //'itemsPurchaseDesc:ntext',
          //'itemsPurchaseCost',
          //'itemsExpenseAccountRef',
          //'itemsAssetAccountRef',
          //'itemsCreatedAt',
          //'itemsLastupdatedAt',
          //'itemsDeletedAt',
          //'itemsStatus',
          //'userId',


        ],
    ]); ?>

</div>
