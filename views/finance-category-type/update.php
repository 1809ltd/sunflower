<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCategoryType */

$this->title = 'Update Finance Category Type: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-category-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
