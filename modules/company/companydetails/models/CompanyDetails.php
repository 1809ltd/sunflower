<?php

namespace app\modules\company\companydetails\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "company_details".
 *
 * @property int $id
 * @property string $companyName
 * @property string $companyAddress
 * @property string $companySuite
 * @property string $companyCity
 * @property string $companyCounty
 * @property string $companyPostal
 * @property string $companyCountry
 * @property string $companyLogo
 * @property int $companyStatus
 * @property string $companyTimestamp
 * @property string $companyupdatedAt
 * @property string $companyDeletedAt
 * @property int $createdBy
 * @property int $companyUserId
 *
 * @property CompanyDepartment[] $companyDepartments
 * @property UserDetails $companyUser
 * @property UserDetails $createdBy0
 * @property UserDetails $createdBy1
 * @property CompanySiteDetails[] $companySiteDetails
 */
class CompanyDetails extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyName', 'companyAddress', 'companyStatus'], 'required'],
            [['companyLogo'], 'string'],
            [['companyStatus', 'createdBy', 'companyUserId'], 'integer'],
            [['companyTimestamp', 'companyupdatedAt', 'companyDeletedAt'], 'safe'],
            [['file'],'file'],
            [['companyName', 'companyAddress', 'companySuite', 'companyCity', 'companyCounty', 'companyPostal', 'companyCountry'], 'string', 'max' => 255],
            [['companyUserId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['companyUserId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'companyName' => 'Company Name',
            'companyAddress' => 'Company Address',
            'companySuite' => 'Company Suite',
            'companyCity' => 'Company City',
            'companyCounty' => 'Company County',
            'companyPostal' => 'Company Postal',
            'companyCountry' => 'Company Country',
            'companyLogo' => 'Company Logo',
            'file' => 'Logo',
            'companyStatus' => 'Company Status',
            'companyTimestamp' => 'Company Timestamp',
            'companyupdatedAt' => 'Companyupdated At',
            'companyDeletedAt' => 'Company Deleted At',
            'createdBy' => 'Created By',
            'companyUserId' => 'Company User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDepartments()
    {
        return $this->hasMany(CompanyDepartment::className(), ['companyId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'companyUserId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy1()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySiteDetails()
    {
        return $this->hasMany(CompanySiteDetails::className(), ['companyId' => 'id']);
    }
}
