<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\customer\customerdetails\models\CustomerDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customerNumber') ?>

    <?= $form->field($model, 'customerFullname') ?>

    <?= $form->field($model, 'customerCompanyName') ?>

    <?= $form->field($model, 'customerKraPin') ?>

    <?php // echo $form->field($model, 'customerDisplayName') ?>

    <?php // echo $form->field($model, 'customerPrimaryPhone') ?>

    <?php // echo $form->field($model, 'customerPrimaryEmail') ?>

    <?php // echo $form->field($model, 'customerstatus') ?>

    <?php // echo $form->field($model, 'customercity') ?>

    <?php // echo $form->field($model, 'customerPhyscialAdress') ?>

    <?php // echo $form->field($model, 'customerCounty') ?>

    <?php // echo $form->field($model, 'customerCountry') ?>

    <?php // echo $form->field($model, 'customerAddress') ?>

    <?php // echo $form->field($model, 'customerCreateTime') ?>

    <?php // echo $form->field($model, 'customerUpdatedAt') ?>

    <?php // echo $form->field($model, 'customerDeleteAt') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
