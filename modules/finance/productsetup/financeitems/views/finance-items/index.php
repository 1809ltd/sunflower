<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
use app\modules\finance\productsetup\financeitems\models\FinanceItemsSearch;

// /*Item Category*/
use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory;
/*Categry Type*/
use app\modules\finance\productsetup\financeitemscategorytype\models\FinanceItemsCategoryType;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;
/*Dropdown action Bar*/
use yii\bootstrap\ButtonDropdown;


/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Product and Service';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-default no-print">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="finance-items-index">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <p class="pull-right">
          <?php
          if (Yii::$app->Permission->getSpecificPermission('new')) {
            // code...
            echo Html::a('Add Product and Service', ['create'], ['class' => 'btn btn-success']);
          } else {
            // code...
            ?>
            <p>
                <?= Html::a('Add Product and Service', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            <?php
          }
          ?>
        </p>
        <?= GridView::widget([
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
          'rowOptions'=> function($model)
          {
            // code...
            if ($model->itemsStatus==1) {
              // code...
              return['class'=>'success'];
            } else {
              // code...
              return['class'=>'danger'];
            }

          },
          'columns' => [
              ['class' => 'kartik\grid\ExpandRowColumn',
                'value'=>function ($model,$key,$index,$column)
                {
                  // code...
                  return GridView::ROW_COLLAPSED;
                },
                'detail'=>function ($model,$key,$index,$column)
                {
                  // code...
                  $searchModel = new FinanceItemsSearch();
                  $searchModel->id = $model->id;
                  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                  return Yii::$app->controller->renderPartial('_viewitemsdetails',[
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,

                  ]);
                },
            ],

            // 'id',
            'itemsName',
            'itemsCode',
            'itemsDescription:ntext',
            // 'itemsTaxable',
            // 'itemsCustomerUnitPrice',
            // 'itemsSupplierUnitPrice',
            //'itemsClassification',
            //'itemsIncomeAccountRef',
            //'itemsPurchaseDesc:ntext',
            //'itemsPurchaseCost',
            //'itemsExpenseAccountRef',
            //'itemsAssetAccountRef',
            //'itemsCreatedAt',
            //'itemsLastupdatedAt',
            //'itemsDeletedAt',
            //'itemsStatus',
            //'userId',

            /* ... GridView configuration ... */
            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{all}',
              'buttons' => [
                'all' => function ($url, $model, $key) {
                  return ButtonDropdown::widget([
                    'encodeLabel' => false, // if you're going to use html on the button label
                    'label' => 'Options',
                    'dropdown' => [
                      'encodeLabels' => false, // if you're going to use html on the items' labels
                      'items' => [
                        [
                          'label' => \Yii::t('yii', 'View'),
                          'url' => ['view', 'id' => $key],
                        ],
                        [
                          'label' => \Yii::t('yii', 'Update'),
                          'url' => ['update', 'id' => $key],
                          // 'visible' => Yii::$app->Permission->getSpecificPermission('save'),  // if you want to hide an item based on a condition, use this
                        ],
                        [
                          'label' => \Yii::t('yii', 'Aprrove'),
                          'url' => ['update', 'id' => $key],
                          // 'visible' => Yii::$app->Permission->getSpecificPermission('other'),  // if you want to hide an item based on a condition, use this
                        ],
                        [
                          'label' => \Yii::t('yii', 'Delete'),
                          'linkOptions' => [
                            'data' => [
                                'method' => 'post',
                                'confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'),
                            ],
                          ],
                          'url' => ['delete', 'id' => $key],
                          // 'visible' => Yii::$app->Permission->getSpecificPermission('remove'),   // same as above
                        ],
                      ],
                      'options' => [
                        'class' => 'dropdown-menu-right', // right dropdown
                      ],
                    ],
                    'options' => [
                      'class' => 'btn-default',   // btn-success, btn-info, et cetera
                    ],
                    'split' => true,    // if you want a split button
                  ]);
                },
              ],
            ],
            /* ... additional GridView configuration ... */
            // ['class' => 'yii\grid\ActionColumn'],
          ],
      ]);
      ?>
        <?php Pjax::end(); ?>
    </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">

  </div>
</div>
<!-- /.box -->
<?= $this->render('printservicelist.php') ?>
<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   // $('li.treeview').addClass('active');
   $('li.treeview').addClass('treeview menu-open');
   $('#productsetup').addClass('active');
   $('#finance-items').addClass('active');
", View::POS_READY);
?>
