<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "billsbyvendor".
 *
 * @property int $vendorId
 * @property string $vendorCompanyName
 * @property string $vendorKraPin
 * @property double $biilsLinesAmount
 * @property double $biilsLinesTaxAmount
 * @property double $billLinesTaxCodeRef
 * @property int $accountId
 * @property string $fullyQualifiedName
 * @property int $biilsId
 * @property int $billslineId
 * @property string $biilsLinesDescription
 * @property double $biilsLinesQty
 * @property double $billlinePriceperunit
 * @property string $billsLinesCreatedAt
 * @property string $txnDate
 * @property int $billsLinesBillableStatus
 * @property string $eventNumber
 * @property string $eventCatName
 * @property int $catId
 */
class Billsbyvendor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'billsbyvendor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorId', 'vendorCompanyName', 'vendorKraPin', 'biilsLinesAmount', 'biilsLinesTaxAmount', 'billLinesTaxCodeRef', 'accountId', 'fullyQualifiedName', 'biilsId', 'biilsLinesQty', 'billlinePriceperunit', 'txnDate', 'billsLinesBillableStatus', 'eventNumber', 'eventCatName'], 'required'],
            [['vendorId', 'accountId', 'biilsId', 'billslineId', 'billsLinesBillableStatus', 'catId'], 'integer'],
            [['biilsLinesAmount', 'biilsLinesTaxAmount', 'billLinesTaxCodeRef', 'biilsLinesQty', 'billlinePriceperunit'], 'number'],
            [['biilsLinesDescription'], 'string'],
            [['billsLinesCreatedAt', 'txnDate'], 'safe'],
            [['vendorCompanyName', 'fullyQualifiedName'], 'string', 'max' => 255],
            [['vendorKraPin'], 'string', 'max' => 20],
            [['eventNumber', 'eventCatName'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vendorId' => 'Vendor ID',
            'vendorCompanyName' => 'Vendor Company Name',
            'vendorKraPin' => 'Vendor Kra Pin',
            'biilsLinesAmount' => 'Biils Lines Amount',
            'biilsLinesTaxAmount' => 'Biils Lines Tax Amount',
            'billLinesTaxCodeRef' => 'Bill Lines Tax Code Ref',
            'accountId' => 'Account ID',
            'fullyQualifiedName' => 'Fully Qualified Name',
            'biilsId' => 'Biils ID',
            'billslineId' => 'Billsline ID',
            'biilsLinesDescription' => 'Biils Lines Description',
            'biilsLinesQty' => 'Biils Lines Qty',
            'billlinePriceperunit' => 'Billline Priceperunit',
            'billsLinesCreatedAt' => 'Bills Lines Created At',
            'txnDate' => 'Txn Date',
            'billsLinesBillableStatus' => 'Bills Lines Billable Status',
            'eventNumber' => 'Event Number',
            'eventCatName' => 'Event Cat Name',
            'catId' => 'Cat ID',
        ];
    }
}
