<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanySiteDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-site-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'companyId') ?>

    <?= $form->field($model, 'siteName') ?>

    <?= $form->field($model, 'siteAddress') ?>

    <?= $form->field($model, 'siteSuite') ?>

    <?php // echo $form->field($model, 'siteCity') ?>

    <?php // echo $form->field($model, 'siteCounty') ?>

    <?php // echo $form->field($model, 'sitePostal') ?>

    <?php // echo $form->field($model, 'siteCountry') ?>

    <?php // echo $form->field($model, 'siteStatus') ?>

    <?php // echo $form->field($model, 'siteTimestamp') ?>

    <?php // echo $form->field($model, 'siteUpdatedAt') ?>

    <?php // echo $form->field($model, 'siteDeletedAt') ?>

    <?php // echo $form->field($model, 'companyUserId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
