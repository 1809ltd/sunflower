<?php

namespace app\modules\finance\expensepayment;

/**
 * expensepayment module definition class
 */
class expensepayment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expensepayment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Bills Payment Sub Module*/
          'bill-payment' => [
              'class' => 'app\modules\finance\expensepayment\billpayment\billpayment',
          ],
          /*Office Expense payment*/
          'officeexpensepayment' => [
              'class' => 'app\modules\finance\expensepayment\officeexpensepayment\officeexpensepayment',
          ],
          /*Event Expense payment*/
          'eventexpensepayment' => [
              'class' => 'app\modules\finance\expensepayment\eventexpensepayment\eventexpensepayment',
          ],
          /*Lead Expense Payment*/
          'leadexpensepayment' => [
              'class' => 'app\modules\finance\expensepayment\leadexpensepayment\leadexpensepayment',
          ],
        ];

        // custom initialization code goes here
    }
}
