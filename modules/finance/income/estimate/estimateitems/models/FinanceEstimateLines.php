<?php

namespace app\modules\finance\income\estimate\estimateitems\models;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
/*Estimate*/
use app\modules\finance\income\estimate\models\FinanceEstimate;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "finance_estimate_lines".
 *
 * @property int $id
 * @property int $estimateId
 * @property int $itemRefId
 * @property string $itemRefName
 * @property string $estimateLinesDescription
 * @property double $estimateLinespack
 * @property double $estimateLinesQty
 * @property double $estimateLinesUnitPrice
 * @property double $estimateLinesAmount
 * @property double $estimateLinesTaxCodeRef
 * @property double $estimateLinesTaxamount
 * @property double $estimateLinesDiscount
 * @property string $estimateLinesCreatedAt
 * @property string $estimateLinesUpdatedAt
 * @property string $estimateLinesDeletedAt
 * @property int $estimateLinesStatus
 * @property int $userId
 * @property int $approvedBy
 * @property int $createdBy
 *
 * @property FinanceItems $itemRef
 * @property FinanceEstimate $estimate
 * @property UserDetails $createdBy0
 * @property UserDetails $approvedBy0
 * @property UserDetails $user
 */
class FinanceEstimateLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_estimate_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemRefId', 'itemRefName', 'estimateLinespack', 'estimateLinesQty', 'estimateLinesUnitPrice', 'estimateLinesAmount', 'estimateLinesTaxCodeRef', 'estimateLinesTaxamount', 'estimateLinesDiscount'], 'required'],
            [['estimateId', 'itemRefId', 'estimateLinesStatus', 'userId', 'approvedBy', 'createdBy'], 'integer'],
            [['estimateLinesDescription'], 'string'],
            [['estimateLinespack', 'estimateLinesQty', 'estimateLinesUnitPrice', 'estimateLinesAmount', 'estimateLinesTaxCodeRef', 'estimateLinesTaxamount', 'estimateLinesDiscount'], 'number'],
            [['estimateId','estimateLinesDescription', 'estimateLinesCreatedAt', 'estimateLinesUpdatedAt', 'estimateLinesDeletedAt', 'estimateLinesStatus', 'userId', 'approvedBy', 'createdBy'], 'safe'],
            [['itemRefName'], 'string', 'max' => 255],
            [['itemRefId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemRefId' => 'id']],
            [['estimateId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEstimate::className(), 'targetAttribute' => ['estimateId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estimateId' => 'Qoute',
            'itemRefId' => 'Service',
            'itemRefName' => 'Service Name',
            'estimateLinesDescription' => 'Description',
            'estimateLinespack' => 'Days',
            'estimateLinesQty' => 'Qty',
            'estimateLinesUnitPrice' => 'Unit Price',
            'estimateLinesAmount' => 'Sub Amount',
            'estimateLinesTaxCodeRef' => 'Tax Code',
            'estimateLinesTaxamount' => 'Tax Amount',
            'estimateLinesDiscount' => 'Discount(%)',
            'estimateLinesCreatedAt' => 'Created At',
            'estimateLinesUpdatedAt' => 'Updated At',
            'estimateLinesDeletedAt' => 'Deleted At',
            'estimateLinesStatus' => 'Status',
            'userId' => 'User',
            'approvedBy' => 'Approved By',
            'createdBy' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemRef()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemRefId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstimate()
    {
        return $this->hasOne(FinanceEstimate::className(), ['id' => 'estimateId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
