<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\EventCaterogies */
/* @var $form yii\widgets\ActiveForm */

?>
<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="event-caterogies-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'eventCatName')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'eventCatDesc')->textarea(['rows' => 6]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                    <?= $form->field($model, 'eventCatStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>

                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
