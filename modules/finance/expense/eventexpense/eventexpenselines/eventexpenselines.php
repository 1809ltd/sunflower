<?php

namespace app\modules\finance\expense\eventexpense\eventexpenselines;

/**
 * eventexpenselines module definition class
 */
class eventexpenselines extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\eventexpense\eventexpenselines\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
