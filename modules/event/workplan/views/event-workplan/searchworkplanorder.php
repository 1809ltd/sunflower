<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

// use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;

/*Pop up menu*/
use yii\helpers\Url;
// use  yii\bootstrap\Modal;

/*Getting the models for the workplan*/
use app\modules\event\workplan\models\EventWorkplan;
use app\modules\event\workplan\models\EventWorkplanSearch;
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;

use kartik\select2\Select2;

/*Dependant Drop*/
use kartik\depdrop\DepDrop;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Get Event Details*/
use app\modules\event\eventInfo\models\EventDetails;

?>
<?php
if (isset($_SESSION['eventId'])&&isset($_SESSION['orderId'])&&isset($_SESSION['customerId'])) {
  // code...
  $model->eventId = $_SESSION['eventId'];
  $model->customerId= \Yii::$app->session->get('customerId');
  $model->orderId= \Yii::$app->session->get('orderId');

  // Get Event Details
  $modelEvent = EventDetails::find()
          ->where('id = :eventId', [':eventId' => $model->eventId])
          ->one();

  /*Getting filtering the results based on the event and orderId*/
  $searchModel = new EventWorkplanSearch();
  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

  /*Page header */
  $this->title = $modelEvent->eventNumber;
  $this->params['breadcrumbs'][] = ['label' => 'Workplan for:', 'url' => ['index']];
  $this->params['breadcrumbs'][] = $this->title;

  ?>
  <div class="col-md-4">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Event Details</h3>

          <div class="box-tools pull-right">
          </div>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped table-condensed">
            <thead>
              <tr>
                <th style="width: 40%;">Title</th>
                <th style="width: 60%;">Detail</th>
              </tr>
            </thead>
              <tbody>
                <tr><td><span>Event Name :</span></td><td> <?= $modelEvent->eventName;?> </td></tr>
                <tr><td><span>Event Number :</span></td><td><?= $modelEvent->eventNumber;?> </td></tr>
                <tr><td><span>Customer :</span></td><td><?= $modelEvent->customer['customerFullname']?></td></tr>
                <tr><td><span>Event date :</span></td><td><?= Yii::$app->formatter->asDate($modelEvent->eventStartDate, 'long');?></td></tr>
                <tr><td><span>Set up date :</span></td><td><?= Yii::$app->formatter->asDate($modelEvent->eventSetUpDateTime, 'long');?>
                <tr><td><span>Contract Status :</span></td><td><span class="label label-success">Active</span></td></tr>
                <tr><td colspan="2"><div class="col-md-2">
                  <div class="form-actions">
                    <a href="<?=Url::to(['closesearch']) ?>" class="btn btn-warning btn-sm pull-left text-center" >Close Search</a>
                  </div>
                </div></td></tr>
              </tbody>
          </table>
        </div>
    </div>
  </div>

  <div class="col-md-8">

    <?=
    $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'model'=>$model,
    ]) ?>

  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">WorkPlan Based on Checklist</h3>
            <div class="box-tools pull-right">
            </div>
          </div>
          <div class="box-body">
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                  <tr>
                    <th>Order Number</th>
                    <th>Note</th>
                    <th>Date</th>
                    <th>Person In Charge</th>
                    <th>Action</th>
                  </tr>

                  <?php
                  // echo $model->orderId ?>

                <tr>
                  <td><?= $model["order"]["orderNumber"]?></td>
                  <td><?= $model["order"]["orderNote"]?></td>
                  <td><?= $model["order"]["date"]?></td>
                  <td><?= $model["order"]["personIncharge"]?></td>
                  <td><div class="form-actions">
                      <a href="<?=Url::to(['view', 'id' => $model->orderId]) ?>" class="btn btn-warning btn-sm pull-left text-center" >View Work Plan</a>
                    </div></td>


                </tr>

              </tbody></table>
            </div>

          </div>
        </div>
      </div>
    </div>


  <?php
} else {
  // code...

  $this->title = 'Workplan';
  $this->params['breadcrumbs'][] = ['label' => 'WorkPlan', 'url' => ['index']];
  $this->params['breadcrumbs'][] = $this->title;


  ?>
  <!-- begin row -->
  <div class="box">
    <div class="box-header">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <!-- /.box-header -->
    <div id="toggleSearch" class="box-body delivery-list-index" style="display:block">

      <?php $form = ActiveForm::begin([
                        // 'id' => 'workplansearch-form',
                        // 'enableAjaxValidation' => true,
                    ]);?>
      <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(CustomerDetails::find()->where('`customerstatus` = 1')->asArray()->all(),'id','customerDisplayName'),
                  'language' => 'en',
                  'options' => ['id'=>'customerId','prompt'=>'Select...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);?>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'eventId')->widget(DepDrop::classname(), [
                  'options'=>['id'=>'eventId'],
                  'data' => [$model->eventId => $model->eventId],
                  'type' => DepDrop::TYPE_SELECT2,
                  'pluginOptions'=>[
                      'depends'=>['customerId'],
                      'initialize' => true,
                      // 'initDepends'=>['customerId'],
                      'placeholder'=>'Select...',
                      'url'=>Url::to(['/event/eventdocs/orderfroms/orderformsitems/event-orderfrom-list/eventdetails'])
                  ]
              ]);?>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'orderId')->widget(DepDrop::classname(), [
                  'type' => DepDrop::TYPE_SELECT2,
                  'data' => [$model->orderId => $model->orderId],
                  'pluginOptions'=>[
                      'depends'=>['customerId', 'eventId'],
                      'initialize' => true,
                      'initDepends'=>['customerId', 'eventId'],
                      'placeholder'=>'Select...',
                      'url'=>Url::to(['/event/eventdocs/orderfroms/orderformsitems/event-orderfrom-list/ordernumber'])
                  ]
              ]); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>

            </div>
        </div>
      </div>

    <?php ActiveForm::end(); ?>

    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

  <?php
}



 ?>
