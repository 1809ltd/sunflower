<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCheckIn */

$this->title = 'Create Asset Check In';
$this->params['breadcrumbs'][] = ['label' => 'Check Ins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-check-in-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
