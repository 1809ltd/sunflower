<?php

namespace app\modules\inventory\asset\assetvaluation;

/**
 * assetvaluation module definition class
 */
class assetvaluation extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\asset\assetvaluation\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
