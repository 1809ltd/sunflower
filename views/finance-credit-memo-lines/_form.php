<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCreditMemoLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-credit-memo-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'creditMemoId')->textInput() ?>

    <?= $form->field($model, 'invoiceNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'creditMemoLineDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'creditMemoLineAmount')->textInput() ?>

    <?= $form->field($model, 'creditMemoLineStatus')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
