<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/*Company Site Details*/
use app\modules\company\companysite\models\CompanySiteDetails;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companysitelocation\models\CompanySiteLocation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body company-site-location-form">

  <?= $form->field($model, 'siteId')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(CompanySiteDetails::find()->all(),'id','siteName'),
      'language' => 'en',
      'options' => ['placeholder' => 'Please Select the Site..'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);
  ?>
  <?= $form->field($model, 'locationName')->textInput(['maxlength' => true]) ?>

  <?php $status = ['1' => 'Available', '2' => 'Suspended']; ?>

  <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']); ?>
  <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
