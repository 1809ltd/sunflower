<?php

namespace app\modules\finance\expense\officeexpense\officeexpenselines\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expense\officeexpense\officeexpenselines\models\FinanceOfficeRequstionFormList;

/**
 * FinanceOfficeRequstionFormListSearch represents the model behind the search form of `app\modules\finance\expense\officeexpense\officeexpenselines\models\FinanceOfficeRequstionFormList`.
 */
class FinanceOfficeRequstionFormListSearch extends FinanceOfficeRequstionFormList
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'reqId', 'activityId', 'personelId', 'status', 'userId'], 'integer'],
            [['personnelTel', 'description', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['qty', 'unitprice', 'amont'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceOfficeRequstionFormList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reqId' => $this->reqId,
            'activityId' => $this->activityId,
            'personelId' => $this->personelId,
            'qty' => $this->qty,
            'unitprice' => $this->unitprice,
            'amont' => $this->amont,
            'status' => $this->status,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'personnelTel', $this->personnelTel])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
