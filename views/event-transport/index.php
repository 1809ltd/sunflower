<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\event\transportation\models\EventTransportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Transports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-transport-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Transport', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customerId',
            'eventId',
            'orderId',
            'outsourced',
            //'assetId',
            //'numberPlate',
            //'vendorId',
            //'delivery',
            //'collection',
            //'deliveryTime',
            //'collectionTime',
            //'status',
            //'userId',
            //'approvedBy',
            //'createdBy',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
