<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
  'id' => 'basic',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
  ],
  'modules' => [
    /*Company Module*/
    'company' => [
      'class' => 'app\modules\company\company',
    ],
    /*Vedor Module*/
    'vendor' => [
      'class' => 'app\modules\vendor\vendor',
    ],
    /*Customer Module*/
    'customer' => [
      'class' => 'app\modules\customer\customer',
    ],
    /*Event Module*/
    'event' => [
      'class' => 'app\modules\event\event',
    ],
    /*Finance Module*/
    'finance' => [
      'class' => 'app\modules\finance\finance',
    ],
    /*PErsoneel Module*/
    'personnel' => [
      'class' => 'app\modules\personnel\personnel',
    ],
    /*Inventroyi*/
    'inventory' => [
      'class' => 'app\modules\inventory\inventory',
    ],
    /*User Module*/
    'user' => [
      'class' => 'app\modules\user\user',
    ],
    'gridview' =>  [
      'class' => '\kartik\grid\Module'
      // enter optional module parameters below - only if you need to
      // use your own export download action or custom translation
      // message source
      // 'downloadAction' => 'gridview/export/download',
      // 'i18n' => []
    ]
  ],
    'components' => [
      'request' => [
        // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        'cookieValidationKey' => 'wJVipyh_TaGeXZbkLVHy1f9Oitz1bBeS',
      ],
      'cache' => [
        'class' => 'yii\caching\FileCache',
      ],
      'user' => [
        'identityClass' => 'app\modules\user\userdetails\models\UserDetails',
        'enableAutoLogin' => true,
      ],
      // 'cart' => [
      //     'class' => 'yii2mod\cart\Cart',
      //     // you can change default storage class as following:
      //     'storageClass' => [
      //         'class' => 'yii2mod\cart\storage\DatabaseStorage',
      //         // you can also override some properties
      //         'deleteIfEmpty' => true
      //     ]
      // ],
      // Error Handling Page
      'errorHandler' => [
        'errorAction' => 'site/error',
      ],
      'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@app/mail',
        'transport' => [
          'class' => 'Swift_SmtpTransport',
          'host' => 'smtp.yandex.ru',
          'username' => '<username>@<yourDomain>',
          'password' => '<userPassword>',
          'port' => 465,
          'encryption' => 'ssl',
        ],
        'useFileTransport' => false,
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        // 'useFileTransport' => true,
      ],
      'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
          [
            'class' => 'yii\log\FileTarget',
            'levels' => ['error', 'warning'],
          ],
        ],
      ],
      'db' => $db,
      /*Url Mananger*/
      'urlManager' => [
        'class' => 'yii\web\UrlManager',
        // Disable index.php
        'showScriptName' => false,
        // Disable r= routes
        'enablePrettyUrl' => true,
        'rules' => array(
          '<controller:\w+>/<id:\d+>' => '<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
          '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
        ),
      ],
      // AccessControl
      'Permission' => [
        'class' =>  'app\components\ModulesPermission',
      ],
      // Notification
      'notifier' => [
           'class' => '\tuyakhov\notifications\Notifier',
           'channels' => [
               'mail' => [
                   'class' => '\tuyakhov\notifications\channels\MailChannel',
                   'from' => 'no-reply@example.com'
               ],
               'sms' => [
                   'class' => '\tuyakhov\notifications\channels\TwilioChannel',
                   'accountSid' => '...',
                   'authToken' => '...',
                   'from' => '+1234567890'
               ],
               'database' => [
                    'class' => '\tuyakhov\notifications\channels\ActiveRecordChannel'
               ]
           ],
       ],
    ],
    'controllerMap' => [
      // ...
      'migrate' => [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationNamespaces' => [
          'tuyakhov\notifications\migrations',
        ],
      ],
      // ...
    ],
    'as beforeRequest' => [
      //if guest user access site so, redirect to login page.
      'class' => 'yii\filters\AccessControl',
      'rules' => [
        [
          'actions' => ['login', 'error'],
          'allow' => true,
        ],
        [
          'allow' => true,
          'roles' => ['@'],
        ],
      ],
    ],
    'params' => $params,
  ];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

    // if (YII_ENV_DEV) {
    //       // configuration adjustments for 'dev' environment
    //       $config['bootstrap'][] = 'debug';
    //       $config['modules']['debug'] = [
    //           'class' => 'yii\debug\Module',
    //           // uncomment the following to add your IP if you are not connecting from localhost.
    //           //'allowedIPs' => ['127.0.0.1', '::1'],
    //       ];
    //
    //     $config['modules']['gii'] = [
    //         'class' => 'yii\gii\Module',
    //         'generators' => [ // HERE
    //             'crud' => [
    //                 'class' => 'yii\gii\generators\crud\Generator',
    //                 'templates' => [
    //                     'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
    //                 ]
    //             ]
    //         ],
    //     ];
    // }

return $config;
