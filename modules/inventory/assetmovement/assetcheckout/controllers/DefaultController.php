<?php

namespace app\modules\inventory\assetmovement\assetcheckout\controllers;

use yii\web\Controller;

/**
 * Default controller for the `assetcheckout` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
