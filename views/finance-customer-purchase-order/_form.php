<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;

/*Getting the product and service offered, customer*/
use app\models\FinanceItems;
use app\models\CustomerDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrder */
/* @var $form yii\widgets\ActiveForm */

?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="panel-body finance-customer-purchase-order-form">

                  <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(CustomerDetails::find()->all(),'id','customerDisplayName'),
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Company ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'purchaseOrderNumber')->textInput(['maxlength' => true])?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'purchaseOrderProjectClassification')->textInput(['maxlength' => true]) ?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'purchaseOrderProjectId')->textInput() ?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'purchaseOrderProjectName')->textInput(['maxlength' => true]) ?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'purchaseOrderTxnDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                      <?= $form->field($model, 'purchaseOrderTxnStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>
                    </div>
                  </div>
                 <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'purchaseOrderBillEmail')->textInput(['maxlength' => true]) ?>

                    </div>
                  </div>


                  <div class="col-sm-3">
                    <div class="form-group">

                    </div>
                  </div>

                  <div class="panel-body"></div>

                  <div class="panel-body">

                    <div class="panel panel-pvr panel--style--1">
                      <div class="panel-heading">
                          <div class="panel-heading-btn">
                              <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                              <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                              <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                              <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                          </div>
                          <h4 class="panel-title">Add Purchase Order Item</h4>
                      </div>
                      <div class="panel-body">
                           <?php DynamicFormWidget::begin([
                              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                              'widgetBody' => '.container-items', // required: css class selector
                              'widgetItem' => '.item', // required: css class
                              'limit' => 4, // the maximum times, an element can be cloned (default 999)
                              'min' => 1, // 0 or 1 (default 1)
                              'insertButton' => '.add-item', // css class
                              'deleteButton' => '.remove-item', // css class
                              'model' => $modelsFinanceCustomerPurchaseOrderLines[0],
                              'formId' => 'dynamic-form',
                              'formFields' => [

                                // 'id',
                                // 'purchaseOrderId',
                                'itemRefName',
                                'purchaseOrderLinesDescription',
                                'purchaseOrderLinespack',
                                'purchaseOrderLinesQty',
                                'purchaseOrderLinesUnitPrice',
                                'purchaseOrderLinesAmount',
                                'purchaseOrderLinesTaxCodeRef',
                                'purchaseOrderLinesTaxamount',
                                'purchaseOrderLinesDiscount',
                                //'purchaseOrderLinesCreatedAt',
                                //'purchaseOrderLinesUpdatedAt',
                                //'purchaseOrderLinesDeletedAt',
                                //'userId',

                              ],
                          ]); ?>

                          <div class="container-items"><!-- widgetContainer -->
                            <!-- Loopping Items in the Lists -->
                          <?php foreach ($modelsFinanceCustomerPurchaseOrderLines as $i => $modelFinanceCustomerPurchaseOrderLines): ?>
                              <div class="item panel-pvr panel--style--2 "><!-- widgetBody -->


                                  <div class="panel-heading">
                                      <h3 class="panel-title pull-left">Purchase Order Item</h3>
                                      <div class="pull-right">
                                          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>


                                  <div class="panel-body">
                                      <?php
                                          // necessary for update action.
                                          if (! $modelFinanceCustomerPurchaseOrderLines->isNewRecord) {
                                              echo Html::activeHiddenInput($modelFinanceCustomerPurchaseOrderLines, "[{$i}]id");
                                          }
                                      ?>

                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]itemRefName")->textInput(['maxlength' => true]) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinesDescription")->textInput() ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinespack")->textInput([
                                          'maxlength' => true,
                                          'class' => 'packs form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinesQty")->textInput([
                                          'maxlength' => true,
                                          'class' => 'qnty form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinesUnitPrice")->textInput([
                                          'maxlength' => true,
                                          'class' => 'price form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinesDiscount")->textInput([
                                          'maxlength' => true,
                                          'value' => 0,
                                          'class' => 'discountpart form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinesTaxCodeRef")->textInput(['maxlength' => true,
                                          'class' => 'taxcode form-control']) ?>
                                        </div>
                                      </div>

                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinesTaxamount")->textInput([
                                          'readonly' => true,
                                          'maxlength' => true,
                                          'class' => 'sumTaxPart form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceCustomerPurchaseOrderLines, "[{$i}]purchaseOrderLinesAmount")->textInput([
                                          'readonly' => true,
                                          'maxlength' => true,
                                          'class' => 'amount sumPart  form-control']) ?>
                                        </div>
                                      </div>


                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>

                                  </div>
                              </div>
                          <?php endforeach; ?>
                          </div>
                          <?php DynamicFormWidget::end(); ?>
                      </div>
                  </div>

                  </div>

                    <div class="panel-body"></div>

                    <div class="col-sm-12">
                      <div class="form-group">
                        <?= $form->field($model, 'purchaseOrderNote')->textarea(['rows' => 6]) ?>

                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <?= $form->field($model, 'purchaseOrderDiscountAmount')->textInput(['readonly' => true,'class' => 'finaldiscount form-control']) ?>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <?= $form->field($model, 'purchaseOrderTaxAmount')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
                      </div>
                    </div>

                    <div class="col-sm-3">
                      <div class="form-group">
                        <?= $form->field($model, 'purchaseOrderSubAmount')->textInput(['readonly' => true,'class' => 'total form-control']) ?>
                      </div>
                    </div>


                    <div class="col-sm-3">
                      <div class="form-group">
                        <?= $form->field($model, 'purchaseOrderAmount')->textInput(['readonly' => true,'class' => 'total form-control']) ?>
                      </div>
                    </div>

                    <?= $form->field($model, 'purchaseOrderdCreatedby')->hiddenInput(['value'=> "1"])->label(false);?>

                    <div class="col-sm-3">
                      <div class="form-group">

                      </div>
                    </div>

                    <div class="panel-body"></div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= Html::submitButton($modelFinanceCustomerPurchaseOrderLines->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                    </div>
                  </div>

                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->

<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var amount = 0;
        var total = 0;
        var taxation=0;
        var tax=0
        var discount=0;
        var totaldiscount=0;

        items.each(function (index, elem) {

            //getting specific elements
            var qnty = $(elem).find(".qnty").val();
            var price = $(elem).find(".price").val();
            var packs = $(elem).find(".packs").val();
            var taxcode = $(elem).find(".taxcode").val();
            var discountpart = $(elem).find(".discountpart").val();



            //Check if qnty and price are numeric or something like that
            amount = parseFloat(qnty) * parseFloat(price) * parseFloat(packs);


            //getting disount value
            discount = (parseFloat(amount) * parseFloat(discountpart))/100;

            totaldiscount = parseFloat(totaldiscount) + parseFloat(discount);


            //assigning total discount at the end
            $(".finaldiscount").val(totaldiscount);


            var amountafterdiscount= parseFloat(amount) - parseFloat(discount);
            //Assign the amount value to the field
            $(elem).find(".amount").val(amountafterdiscount);

            var amountValue = $(elem).find(".amount").val();

            taxation= parseFloat(taxcode) * parseFloat(amountafterdiscount);

            //Assign the Taxation value to the field
            $(elem).find(".sumTaxPart").val(taxation);

            //getting the tax amount figures
            var taxValue = $(elem).find(".sumTaxPart").val();

            //getting the total of all figures
            total = parseFloat(total) + parseFloat(amountValue);

            //assing value to the total amount
            $(".total").val(total);

            //getting total tax amountValue
            tax = parseFloat(tax) + parseFloat(taxValue);

            //assing value to the total tax amount
            $(".tax").val(tax);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
