<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\InvoiceAsset;

InvoiceAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="multi_color">
<?php $this->beginBody() ?>

<!-- begin #pvr-container -->
<div id="pvr-container" class="pvr-container fade fixed_sidebar fixed_header">
    <!-- begin #header -->
    <div id="header" class="header navbar navbar-default navbar-fixed-top">
        <!-- begin container-fluid -->
        <div class="container-fluid">
            <!-- begin mobile sidebar expand / collapse button -->
            <div class="navbar-header">
                <a href="#" class="navbar-brand f-w-500">
                    <img src="http://sunflowertents.com/wp-content/uploads/2017/11/unnamed-1.png" alt="logo"> <span class="m-l-10">
                     </span></a>
                <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle p-0 m-r-5" data-toggle="collapse" data-target="#mega_menu">
                    <i class="ion ion-ios-keypad-outline l-h-34"></i>
                </button>
            </div>
            <!-- end mobile sidebar expand / collapse button -->

            <!-- begin navbar-collapse -->
            <div class="collapse navbar-collapse pull-left" id="top-navbar">
                <ul class="nav navbar-nav">
                    <li data-click="sidebar-minify">
                        <a href="javascript:void(0)">
                            <i class="ion ion-ios-keypad-outline"></i>
                        </a>
                    </li>
                    <li class="page_heading">
                        <h4>Sunflower Management System</h4>
                    </li>
                </ul>
            </div>
            <!-- end navbar-collapse -->

            <!-- begin header navigation right -->
            <ul class="nav navbar-nav navbar-right hidden-xs">
                <li id="toggleFullScreen" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"
                    title="Full Screen">
                    <a href="javascript:void(0)" class="">
                        <i class="ion ion-ios-monitor-outline"></i>
                    </a>
                </li>
                <li id="btn-search">
                    <a href="javascript:void(0)" class="">
                        <i class="ion ion-ios-search"></i>
                        <span class="label">5</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle">
                        <i class="ion ion-ios-bell-outline"></i>
                        <span class="label">3</span>
                    </a>
                    <ul class="dropdown-menu media-list pull-right animated fadeInDown">
                        <li class="dropdown-header">Notifications (3)</li>
                        <li class="media">
                            <a href="javascript:void(0)">
                                <div class="media-left">
                                    <img src="http://via.placeholder.com/128x128" class="media-object" alt=""/>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">Andrew</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <div class="text-muted f-s-11">25 minutes ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:void(0)">
                                <div class="media-left">
                                    <img src="http://via.placeholder.com/128x128" class="media-object" alt=""/>
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading">Need Action</h6>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                    <div class="text-muted f-s-11">35 minutes ago</div>
                                </div>
                            </a>
                        </li>
                        <li class="media">
                            <a href="javascript:void(0)">
                                <div class="media-left"><i class="fa fa-plus media-object bg-green"></i></div>
                                <div class="media-body">
                                    <h6 class="media-heading"> New User Registered</h6>
                                    <div class="text-muted f-s-11">1 hour ago</div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown navbar-user">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="ion ion-ios-folder-outline m-r-5 v-a-m"></i>
                        <span class="hidden-xs">Andrew</span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu animated fadeInDown">
                        <li class="arrow"></li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="badge badge-success pull-right m-t-3">2</span>
                                <i class="ion ion-ios-email-outline"></i> Message
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-ios-contact-outline"></i> Profile
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-ios-settings"></i> Setting
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-ios-locked-outline"></i> Lock Screen
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <i class="ion ion-log-out"></i> Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="collapse navbar-collapse pull-right" id="mega_menu">
                <ul class="nav navbar-nav b-r">
                    <li data-toggle="tooltip" data-placement="bottom" data-trigger="hover" class="hidden-xs hidden-sm hide"
                        title="Enter Peek Hours">
                        <div class="input-group input-group-sm width-300 m-t-13">
                            <span class="input-group-addon btn sm_bg_6">Peek Hours</span>
                            <input type="text" class="form-control" placeholder="Peek Hours" value="10:10 - 02:54">
                            <span class="input-group-addon btn sm_bg_6">Save</span>
                        </div>
                    </li>
                    <li class="dropdown dropdown-lg">
                        <a href="javascript:void(0)" class="dropdown-toggle"
                           data-toggle="dropdown">
                            <i class="ion ion-ios-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-lg">

                        </div>
                    </li>
                    <li id="map_screen" data-toggle="tooltip" data-placement="bottom" data-trigger="hover"
                        title="Map Screen">
                        <a href="javascript:void(0)" class="">
                            <i class="ion ion-map"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- end header navigation right -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end #header -->

    <!-- begin #sidebar -->
    <div id="sidebar" class="sidebar">
        <!-- begin sidebar scrollbar -->
        <div data-scrollbar="true" data-height="100%">
            <!-- begin sidebar user -->
            <ul class="nav">
                <li class="nav-profile">
                    <div class="image">
                        <a href="javascript:void(0)">
                            <img src="http://via.placeholder.com/128x128" alt=""/>
                        </a>
                    </div>
                    <div class="info">
                        Andrew
                        <small class="m-b-15">Sunflower - Events</small>
                        <a href="#" class="user_icons" data-toggle="tooltip" title="Dashboard">
                            <i class="ion ion-ios-paper-outline"></i>
                        </a>
                        <a href="javascript:void(0)" class="user_icons" data-toggle="tooltip" title="Notification">
                            <i class="ion ion-ios-bell-outline"></i>
                        </a>
                        <a href="javascript:void(0)" class="user_icons" data-toggle="tooltip" title="Email">
                            <i class="ion ion-ios-email-outline"></i>
                        </a>
                        <a href="javascript:void(0)" class="user_icons" data-toggle="tooltip" title="Logout">
                            <i class="ion ion-log-out"></i>
                        </a>
                    </div>
                </li>
            </ul>
            <!-- end sidebar user -->
            <!-- begin sidebar nav -->
            <ul class="nav">
                <li class="nav-header">Sunflower Management System</li>
                <li class="has-sub">
                    <a href="javascript:void(0)">
                        <b class="caret pull-right"></b>
                        <i class="ion ion-cloud"></i>
                        <span>Master Set up</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="index.php?r=company-details">Company Master</a></li>
                        <li><a href="index.php?r=company-department">Department Master</a></li>
                        <li class="has-sub">
                            <a href="javascript:void(0)">
                                <span>Company Storage Master</span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="index.php?r=company-site-details">Company Site</a></li>
                                <li><a href="index.php?r=company-site-location">Company Site Locations</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:void(0)">
                                <span>Inventory Master</span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-header">Asset Set Up</li>
                                <li><a href="index.php?r=asset-category">Category</a></li>
                                <li><a href="index.php?r=asset-category-type">Category Type</a></li>
                                <li><a href="index.php?r=asset-registration">Asset Registration</a></li>
                                <li><a href="index.php?r=asset-preciation">Asset Valuation</a></li>
                            </ul>
                        </li>


                    </ul>
                </li>
                <li class="has-sub">
                    <a href="javascript:void(0)">
                        <b class="caret pull-right"></b>
                        <i class="ion ion-ios-people-outline"></i>
                        <span>Vendor</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="index.php?r=vendor-company-details">Vendor Master</a></li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="javascript:void(0)">
                        <b class="caret pull-right"></b>
                        <i class="ion ion-android-contacts"></i>
                        <span>Customer</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="index.php?r=customer-details">Customer Master</a></li>
                        <li class="has-sub">
                            <a href="javascript:void(0)">
                                <b class=""></b>
                                <i class="ion ion-android-call">    </i>
                                <span>Follow-up</span>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="index.php?r=customer-lead">Customer Leads</a></li>
                                <li><a href="index.php?r=customer-lead-appointment">Appointment</a></li>
                                <li><a href="index.php?r=customer-lead-communication-tracker">Communication Tracker</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li class="has-sub">
                    <a href="javascript:void(0)">
                        <b class="caret pull-right"></b>
                        <i class="ion ion-ios-home-outline"></i>
                        <span>Event</span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="index.php?r=event-caterogies">Event Category</a></li>
                        <li><a href="index.php?r=event-details">Event</a></li>
                        <li><a href="index.php?r=event-orderfrom">Order Form</a></li>
                        <!-- <li><a href="index.php?r=event-requirements">Event Requirement</a></li> -->
                    </ul>
                </li>
                <li class="has-sub">
                    <a href="javascript:void(0)">
                        <b class="caret pull-right"></b>
                        <i class="ion ion-social-usd-outline"></i>
                        <span>Finance</span>
                    </a>

                    <ul class="sub-menu">
                        <li class="nav-header">Set Up</li>
                        <li><a href="index.php?r=finance-category-type">Finance Category Type</a></li>
                        <li><a href="index.php?r=finance-accounts">Chart of Accounts</a></li>
                        <li><a href="index.php?r=finance-tax">Tax Set up</a></li>
                        <li class="nav-header">Product Set Up</li>
                        <li><a href="index.php?r=finance-items">Product and Service</a></li>
                        <li class="nav-header">Expense</li>
                        <li><a href="index.php?r=finance-bills">Bills</a></li>
                        <li class="nav-header">Expense Payment</li>
                        <li><a href="index.php?r=finance-vendor-credit-memo">Vendor Credit Memo</a></li>
                        <li><a href="index.php?r=finance-bills-payments">Bills Payment</a></li>
                        <li class="nav-header">Income</li>
                        <li><a href="index.php?r=finance-estimate">Estimate</a></li>
                        <li><a href="index.php?r=finance-invoice">Invoice</a></li>
                        <li class="nav-header">Receipt</li>
                        <li><a href="index.php?r=finance-payment">Invoice Payment</a></li>
                        <li><a href="index.php?r=finance-credit-memo">Credit Memo Payment</a></li>
                        <li class="nav-header">Purchase Orders</li>
                        <li><a href="index.php?r=finance-customer-purchase-order">Customer Purchase Order</a></li>
                        <li><a href="index.php?r=finance-category-type">Sunflower Purchase Order</a></li>
                    </ul>
                </li>
            </ul>
            <!-- end sidebar nav -->
        </div>
        <!-- end sidebar scrollbar -->
    </div>
    <div class="sidebar-bg"></div>
    <!-- end #sidebar -->

    <!-- begin #content -->
    <div id="content" class="content">

      <?= Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
      ]) ?>
      <?= Alert::widget() ?>
      <?= $content ?>

    </div>
    <!-- end #content -->

    <!-- begin scroll to top btn -->
    <a href="javascript:void(0)" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade"
       data-click="scroll-top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- end scroll to top btn -->
</div>
<!-- end page container -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
