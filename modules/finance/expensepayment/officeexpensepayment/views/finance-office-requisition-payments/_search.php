<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOfficeRequisitionPaymentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-office-requisition-payments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reqrecepitNumber') ?>

    <?= $form->field($model, 'reqpaymentName') ?>

    <?= $form->field($model, 'reqpaymentAccount') ?>

    <?= $form->field($model, 'reqpaymentTxnDate') ?>

    <?php // echo $form->field($model, 'reqpaymentType') ?>

    <?php // echo $form->field($model, 'reqpaymentRefence') ?>

    <?php // echo $form->field($model, 'reqpaymentTotalAmt') ?>

    <?php // echo $form->field($model, 'reqpaymentProcessduration') ?>

    <?php // echo $form->field($model, 'reqpaymentLinkedTxn') ?>

    <?php // echo $form->field($model, 'reqpaymentLinkedTxnType') ?>

    <?php // echo $form->field($model, 'reqpaymentStatus') ?>

    <?php // echo $form->field($model, 'reqpaymentCreateAt') ?>

    <?php // echo $form->field($model, 'reqpaymentUpdatedAt') ?>

    <?php // echo $form->field($model, 'reqpaymentDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
