<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "age_payables".
 *
 * @property int $vendorId
 * @property string $payables
 * @property double $Coming Due
 * @property double $1-30 Days
 * @property double $31-60 Days
 * @property double $61-90 Days
 * @property double $>90 Days
 * @property double $Total
 */
class AgePayables extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'age_payables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorId'], 'required'],
            [['vendorId'], 'integer'],
            [['Coming Due', '1-30 Days', '31-60 Days', '61-90 Days', '>90 Days', 'Total'], 'number'],
            [['payables'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vendorId' => 'Vendor ID',
            'payables' => 'Payables',
            'Coming Due' => 'Coming  Due',
            '1-30 Days' => '1 30  Days',
            '31-60 Days' => '31 60  Days',
            '61-90 Days' => '61 90  Days',
            '>90 Days' => '>90  Days',
            'Total' => 'Total',
        ];
    }
}
