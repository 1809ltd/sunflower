<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequstionForm */


$this->title = 'Add Lead Requstion Form';
$this->params['breadcrumbs'][] = ['label' => 'Add Requstion Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-lead-requstion-form-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceLeadRequstionFormList'=>$modelsFinanceLeadRequstionFormList, //to enable Dynamic Form
    ]) ?>

</div>
