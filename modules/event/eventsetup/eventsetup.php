<?php

namespace app\modules\event\eventsetup;

/**
 * eventsetup module definition class
 */
class eventsetup extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventsetup\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Event Category Sub Module*/
          'eventcaterogies' => [
              'class' => 'app\modules\event\eventsetup\eventcaterogies\eventcaterogies',
          ],
          /*Event Roles Sub Modules*/
          'eventrole' => [
              'class' => 'app\modules\event\eventsetup\eventrole\eventrole',
          ],
          /*Event Service Roles*/
          'eventitemrole' => [
              'class' => 'app\modules\event\eventsetup\eventitemrole\eventitemrole',
          ],
          /*Asset Service assignment*/
          'eventitemcategorytype' => [
              'class' => 'app\modules\event\eventsetup\eventitemcategorytype\eventitemcategorytype',
          ],
        ];

        // custom initialization code goes here
    }
}
