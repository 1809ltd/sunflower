<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_caterogies".
 *
 * @property int $eventCatId
 * @property string $eventCatName
 * @property string $eventCatDesc
 * @property int $eventCatStatus
 * @property string $eventCatCreatedAt
 * @property string $eventCatLastUpdatedAt
 * @property string $eventCatDeletedAt
 * @property int $userId
 *
 * @property UserDetails $user
 * @property EventDetails[] $eventDetails
 */
class EventCaterogies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_caterogies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eventCatName', 'eventCatDesc', 'eventCatStatus'], 'required'],
            [['eventCatDesc'], 'string'],
            [['eventCatStatus', 'userId'], 'integer'],
            [['eventCatCreatedAt', 'eventCatLastUpdatedAt', 'eventCatDeletedAt', 'userId'], 'safe'],
            [['eventCatName'], 'string', 'max' => 100],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Event Cat ID',
            'eventCatName' => 'Category Name',
            'eventCatDesc' => 'Description',
            'eventCatStatus' => 'Status',
            'eventCatCreatedAt' => 'Event Cat Created At',
            'eventCatLastUpdatedAt' => 'Event Cat Last Updated At',
            'eventCatDeletedAt' => 'Event Cat Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDetails()
    {
        return $this->hasMany(EventDetails::className(), ['eventCategory' => 'id']);
    }
}
