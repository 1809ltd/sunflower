<?php

namespace app\modules\finance\financereport\companyfinancials\pandl\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// for db functions
use yii\db\Expression;
/*All transactions */
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
/*All Aged Receivables */

/**
 * Default controller for the `pandl` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

      // Create a model to allow the search

      $model = new \yii\base\DynamicModel([
        'startDate','endDate'
      ]);
      // Adding Rules for validation
      $model->addRule(['startDate','endDate'], 'required')
            ->validate();

    // Checking if it a load
     if($model->load(Yii::$app->request->post())){
       // do somenthing with model

       $tafuta="we are here";

       $revenue = AllTransactions::find()->select(['SUM(all_transactions.cr_amount) as cr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                       ->Where('transactionCategory = :transactionCategory', [':transactionCategory' => "Revenue"])
                                       ->andWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                       ->andWhere('status > :status', [':status' => 0])
                                       ->groupBy(['accountId'])
                                       ->orderBy(['accountName' => SORT_ASC,])
                                       ->all();
       // Get the $COGS
       $COGS = AllTransactions::find()->select(['SUM(all_transactions.dr_amount) as dr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                     ->Where("transactionClassification = 'Event expense' OR ( transactionClassification = 'Bill Expense' AND projectId IS NOT NULL )
   OR transactionClassification = 'Lead Expense Payment'")
                                       ->andWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                       ->andWhere('status > :status', [':status' => 0])
                                       ->groupBy(['accountId'])
                                       ->orderBy(['accountParentId' => SORT_ASC,])
                                       ->all();
     // Get the Operating Cost
     $operating = AllTransactions::find()->select(['SUM(all_transactions.dr_amount) as dr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                     ->Where("transactionClassification = 'Office Expense' OR ( transactionClassification = 'Bill Expense' AND projectId IS NULL )
   OR transactionClassification = 'Lead expense'")
                                     ->andWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                     ->andWhere('status > :status', [':status' => 0])
                                     ->groupBy(['accountId'])
                                     ->orderBy(['accountParentId' => SORT_ASC,])
                                     ->all();

    if (!empty($tafuta)) {
      // code...
      return $this->render('index', [
                    'revenue' => $revenue,
                    'COGS' => $COGS,
                    'startDate' => $model->startDate,
                    'endDate' => $model->endDate,
                    'operating' => $operating,
                    'model' => $model,
                    'tafuta' => $tafuta,

                ]);
    } else {
      // code...
      return $this->render('index', [
          'model' => $model,
        ]);
      }

    }else {
      // code...
      return $this->render('index', [
        'model' => $model,
      ]);
    }
  }
}
