<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\expense\eventexpense\models\FinanceEventRequstionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Event Requstions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-requstion-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Event Requstion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'requstionNumber',
            'eventId',
            'date',
            'requstionStatus',
            //'requstionNote:ntext',
            //'amount',
            //'preparedBy',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',
            //'userId',
            //'approvedBy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
