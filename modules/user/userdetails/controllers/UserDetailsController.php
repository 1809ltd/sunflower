<?php

namespace app\modules\user\userdetails\controllers;

use app\models\User;
use Yii;
use app\modules\user\userdetails\models\UserDetails;
use app\modules\user\userdetails\models\UserDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

// Get Roles Assign to user
use app\modules\user\userassignment\models\UserAssignment;
use app\modules\user\userassignment\models\UserAssignmentSearch;

/**
 * UserDetailsController implements the CRUD actions for UserDetails model.
 */
class UserDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new UserDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserDetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

      $modelAssignRights = new UserAssignment();
      $modelsUSerslistPErmision = $this->getusersActiveRoles($id);
      // $useraccessrightlist= $searchModel->search(Yii::$app->request->queryParams);

      // code...
      // Getting users access details
      $searchModel = new UserAssignmentSearch();
      $searchModel->userId = $id;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelsUSerslistPErmision' => $modelsUSerslistPErmision,
            'modelAssignRights' => $modelAssignRights,
            'dataProvider' => $dataProvider,
        ]);
    }
    // Getting all the
    public function getusersActiveRoles($id)
    {
      $model = UserAssignment::find()->where(['userId' => $id])->all();
      return $model;
    }

    public function actionAjaxrights()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $modelAssignRights = new UserAssignment();

      //checking if its a post
      if ($modelAssignRights->load(Yii::$app->request->post())) {
        // code...

        //Geting the time created Timestamp

        // print_r($modelAssignRights);
        // die();
        //Getting user Log in details

        $modelAssignRights->createdAt= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $modelAssignRights->createdBy = Yii::$app->user->identity->id;
        $modelAssignRights->assigner = Yii::$app->user->identity->id;

        $modelAssignRights->save();

        //after Saving the assignment of user Rights

        return $this->redirect(['view', 'id' => $modelAssignRights->userId]);

      } else {
        // code...
        //after Saving the assignment of user Rights

        return $this->redirect(['view', 'id' => $modelAssignRights->userId]);

      }

      // if (Yii::$app->request->isAjax) {
      //   // echo "We are here";
      //   // print_r($modelAssignRights);
      //   // die();
      //   Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      //
      //   // if ($modelAssignRights->load(Yii::$app->requset->post()) && $modelAssignRights->save()){
      //   //   // echo "We are here";
      //   //   // print_r($modelAssignRights);
      //   //   // die();
      //   //   // return [
      //   //   //   'data' =>[
      //   //   //     'success' => true,
      //   //   //     'model' => $modelAssignRights,
      //   //   //     'message' => 'Model has been saved.',
      //   //   //   ],
      //   //   //   'code' => 0,
      //   //   // ];
      //   // } else {
      //   //   // return [
      //   //   //   'data' => [
      //   //   //     'success' => false,
      //   //   //     'model' => null,
      //   //   //     'message' => 'An error occured.',
      //   //   //   ],
      //   //   //   'code' => 1, // Some semantic codes that you know them for yourself
      //   //   // ];
      //   // }
      // }
    }

    /**
     * Creates a new UserDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new UserDetails();

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Geting the time created Timestamp
          // $model->customerCreateTime= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          //Getting user Log in details
          // $model->userId= "1";

          //Getting user Log in details
          $model->userCreatedBy = Yii::$app->user->identity->id;

          $model->password= md5($model->password);
          $model->authKey = Yii::$app->security->generateRandomString();
          $model->save();

          //after Saving the customer Details

          return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('create', [
              'model' => $model,
          ]);
        }


    }

    /**
     * Updates an existing UserDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        //Getting user Log in details
        // $model->userId = Yii::$app->user->identity->id;

        $model->password= md5($model->password);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UserDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        // $this->findModel($id)->delete();

        $this->findModel($id)->updateAttributes(['userStatus'=>0,'userDeleteAt' => new \yii\db\Expression('NOW()')]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
