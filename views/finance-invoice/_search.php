<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoiceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-invoice-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'invoiceNumber') ?>

    <?= $form->field($model, 'customerId') ?>

    <?= $form->field($model, 'localpurchaseOrder') ?>

    <?= $form->field($model, 'invoiceProjectClassification') ?>

    <?php // echo $form->field($model, 'invoiceProjectId') ?>

    <?php // echo $form->field($model, 'invoiceProjectName') ?>

    <?php // echo $form->field($model, 'invoiceTxnDate') ?>

    <?php // echo $form->field($model, 'invoiceDeadlineDate') ?>

    <?php // echo $form->field($model, 'invoiceNote') ?>

    <?php // echo $form->field($model, 'invoiceFooter') ?>

    <?php // echo $form->field($model, 'invoiceTxnStatus') ?>

    <?php // echo $form->field($model, 'invoiceEmailStatus') ?>

    <?php // echo $form->field($model, 'invoiceBillEmail') ?>

    <?php // echo $form->field($model, 'invoiceTaxAmount') ?>

    <?php // echo $form->field($model, 'invoiceDiscountAmount') ?>

    <?php // echo $form->field($model, 'invoiceSubAmount') ?>

    <?php // echo $form->field($model, 'invoiceAmount') ?>

    <?php // echo $form->field($model, 'invoicedCreatedby') ?>

    <?php // echo $form->field($model, 'invoicedCreatedAt') ?>

    <?php // echo $form->field($model, 'invoicedUpdatedAt') ?>

    <?php // echo $form->field($model, 'invoicedDeletedAt') ?>

    <?php // echo $form->field($model, 'approvedby') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
