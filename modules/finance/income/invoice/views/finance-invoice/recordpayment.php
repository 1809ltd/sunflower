<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;


/*Invoice*/
use app\modules\finance\income\invoice\models\FinanceInvoice;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;


/* @var $this yii\web\View */
/* @var $payments app\models\FinancePayment */
/* @var $form yii\widgets\ActiveForm */

?>
<!-- begin row -->

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body deliveryform-asset-check-out-form">
<div class="row">

                    <div class="form-group">
                      <?php
                          // necessary for update action.
                          if (! $payments->isNewRecord) {
                            // code...

                            $recepitNumber = $payments->recepitNumber;

                            ?>
                            <?= $form->field($payments, 'recepitNumber')->textInput(['readonly' => true, 'value' => $recepitNumber])?>
                            <?php

                          }

                      ?>

                    </div>


                    <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($payments, 'paymentName')->textInput(['maxlength' => true]) ?>
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">

                        <?= $form->field($payments, 'paymentDepositToAccount')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(FinanceAccounts::find()->where(['like', 'accountsPays', 'yes'])->all(),'id','fullyQualifiedName'),
                            'language' => 'en',
                            'options' => ['placeholder' => 'Select a Depsit Account ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);  ?>

                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">

                        <?= $form->field($payments, 'paymentTxnDate')->widget(\yii\jui\DatePicker::class, [
                              //'language' => 'ru',
                              'options' => ['autocomplete'=>'off','class' => 'form-control'],
                              'inline' => false,
                              'dateFormat' => 'yyyy-MM-dd',
                          ]); ?>

                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                      <?php $data = ['Cash' => 'Cash', 'Debit Card'=> 'Debit Card', 'Credit Card' => 'Credit Card','Transfer' => 'Transfer','Voucher' => 'Voucher','Mobile Payment' => 'Mobile Payment','Internet Payment' => 'Internet Payment']; ?>
                      <?= $form->field($payments, 'paymentType')->widget(Select2::classname(), [
                          'data' => $data,
                          'language' => 'en',
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">

                        <?= $form->field($payments, 'paymentRefence')->textInput(['maxlength' => true]) ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">

                        <?= $form->field($payments, 'paymentTotalAmt')->textInput() ?>

                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">

                        <?= $form->field($payments, 'paymentProcessduration')->textInput() ?>

                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($payments, 'paymentLinkedTxn')->hiddenInput(['value'=>$model->invoiceNumber])->label(false);?>
                        <?= $form->field($payments, 'paymentStatus')->hiddenInput(['value'=>2])->label(false);?>


                    <?= $form->field($payments, 'paymentLinkedTxnType')->hiddenInput(['value'=>'Invoice Payment'])->label(false);?>


                      </div>
                    </div>


</div>



</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($payments->isNewRecord ? 'Record Payment' : 'Update', ['class' => $payments->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
