<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\customerdetails\models\CustomerDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Details';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::button('Add Customer', ['value'=>Url::to((['create'])),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        </p>
      </div>
    </div>
  </div>

  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
   <?php
    Modal::begin([
        // 'header'=>'<h4>Update Model</h4>',
        'id'=>'update-modal',
        'size'=>'modal-lg'
    ]);

    echo "<div id='updateModalContent'></div>";

    Modal::end();
?>

  <div class="box-body customer-details-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->customerstatus==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'customerNumber',
            'customerFullname',
            'customerCompanyName',
            'customerKraPin',
            'customerDisplayName',
            //'customerPrimaryPhone',
            //'customerPrimaryEmail:email',
            //'customerstatus',
            //'customercity',
            //'customerPhyscialAdress',
            //'customerCounty',
            //'customerCountry',
            //'customerAddress',
            //'customerCreateTime',
            //'customerUpdatedAt',
            //'customerDeleteAt',
            //'createdBy',
            //'userId',

            // ['class' => 'yii\grid\ActionColumn'],
        //     [
        //     'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
        //     'items' => [
        //         [
        //             'label' => 'View',
        //             'url'   => ['view'],
        //         ],
        //         [
        //             'label' => 'Update',
        //             'url'   => ['update'],
        //             'options'=>['class'=>'update-modal-click grid-action'],
        //             // 'update'=>function($url,$model,$key){
        //             //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
        //             //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
        //             //           'class'=>'update-modal-click grid-action',
        //             //           'data-toggle'=>'tooltip',
        //             //           'data-placement'=>'bottom',
        //             //           'title'=>'Update'
        //             //       ]);
        //             //       return $btn;
        //             // },
        //             // 'option' 'class'=>'update-modal-click grid-action',
        //             // 'data-toggle'=>'tooltip',
        //             // 'data-placement'=>'bottom',
        //         ],
        //         [
        //             'label'   => 'Disable',
        //             'url'     => ['delete'],
        //             'linkOptions' => [
        //                 'data-method' => 'post'
        //             ],
        //         ],
        //     ]
        // ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => ['view' => function($url, $model) {
                    return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
                },
              'update' => function($url,$model,$key){
                    $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                        'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                        'class'=>'update-modal-click grid-action',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'bottom',
                        'title'=>'Update'
                    ]);
                    return $btn;
              },
                'delete' => function($url, $model) {
                    return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
                }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
<!-- Printing all customer -->
<?= $this->render('allcustomerlist.php') ?>
