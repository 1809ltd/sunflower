<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\event\transportation\models\EventTransport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-transport-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customerId')->textInput() ?>

    <?= $form->field($model, 'eventId')->textInput() ?>

    <?= $form->field($model, 'orderId')->textInput() ?>

    <?= $form->field($model, 'outsourced')->textInput() ?>

    <?= $form->field($model, 'assetId')->textInput() ?>

    <?= $form->field($model, 'numberPlate')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendorId')->textInput() ?>

    <?= $form->field($model, 'delivery')->textInput() ?>

    <?= $form->field($model, 'collection')->textInput() ?>

    <?= $form->field($model, 'deliveryTime')->textInput() ?>

    <?= $form->field($model, 'collectionTime')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'approvedBy')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
