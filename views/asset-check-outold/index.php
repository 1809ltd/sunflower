<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;



/* @var $this yii\web\View */
/* @var $searchModel app\models\AssetCheckOutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asset Check Outs';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::button('Check Out', ['value'=>Url::to((['/asset-check-out/create'])),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        </p>
      </div>
    </div>
  </div>

  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>

  <div class="box-body personnel-details-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->status==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'assetId',
            [
              'attribute'=>'assetId',
              'value'=>'asset.assetName',
            ],

            // 'id',
            // 'assetId',
            // 'locationId',
            'checkOutDate',
            'dueDate',
            'activity',
            //'activityId',
            //'vehicle',
            //'assignto',
            //'timestamp',
            //'updatedAt',
            //'deletedAt',
            //'status',
            //'athurizedBy',
            //'userId',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
