<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\customer\customerdetails\models\CustomerDetails */

$this->title = $model->customerNumber;
$this->params['breadcrumbs'][] = ['label' => 'Customer Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
 <?php
    Modal::begin([
        // 'header'=>'<h4>Update Model</h4>',
        'id'=>'update-modal',
        'size'=>'modal-lg'
    ]);

    echo "<div id='updateModalContent'></div>";

    Modal::end();
?>
<div class="customer-details-view">

    <p>
        <?= Html::button('Update Customer', ['value'=>Url::to((['update','id'=>$model->id])),'class' => 'update-modal-click btn btn-primary']) ?>

        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    //   DetailView::widget([
    //     'model' => $model,
    //     'attributes' => [
    //         // 'id',
    //         // 'customerNumber',
    //         'customerFullname',
    //         'customerCompanyName',
    //         'customerKraPin',
    //         'customerDisplayName',
    //         'customerPrimaryPhone',
    //         'customerPrimaryEmail:email',
    //         'customerstatus',
    //         'customercity',
    //         'customerPhyscialAdress',
    //         'customerCounty',
    //         'customerCountry',
    //         'customerAddress',
    //         'customerCreateTime',
    //         'customerUpdatedAt',
    //         'customerDeleteAt',
    //         'createdBy',
    //         'userId',
    //     ],
    // ]) ?>

</div>

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <!-- <img class="profile-user-img img-responsive img-circle" src="@web/logo/rsz_sunflowerlogo.png" alt="User profile picture"> -->

          <h3 class="profile-username text-center"><?= $model->customerCompanyName;?></h3>

          <p class="text-muted text-center">Contact:  <?= $model->customerFullname;?></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Pending Invoice</b> <a class="pull-right">...</a>
            </li>
            <li class="list-group-item">
              <b>Upcoming Event</b> <a class="pull-right">..</a>
            </li>
            <li class="list-group-item">
              <b>Upcoming Meetings</b> <a class="pull-right">..</a>
            </li>
          </ul>

          <a href="#" class="btn btn-primary btn-block"><b>..</b></a>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <!-- About Me Box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">About Customer</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> KRA Pin</strong>

          <p class="text-muted">
            <?= $model->customerKraPin;?>
          </p>

          <hr>

          <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

          <p class="text-muted">
          Physical Location: <?= $model->customerPhyscialAdress;?><br>
          County: <?= $model->customerCounty;?><br>
          City: <?= $model->customercity;?><br>
          Country: <?= $model->customerCountry;?><br>
          Address: <?= $model->customerAddress;?><br>

          </p>

          <hr>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#quotes" data-toggle="tab">Quotes</a></li>
          <li><a href="#invoice" data-toggle="tab">Invoice</a></li>
          <li><a href="#events" data-toggle="tab">Events</a></li>
          <li><a href="#details" data-toggle="tab">Details</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="quotes">
            <!-- Post -->
            <?= GridView::widget([
                'dataProvider' => $modelQuotes,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'estimateNumber',
                    // 'customerId',
                    // 'localpurchaseOrder',
                    // 'estimateProjectClassification',
                    //'estimateProjectId',
                    'estimateTxnDate',
                    // 'estimateDeadlineDate',
                    //'estimateNote:ntext',
                    //'estimateFooter:ntext',
                    //'estimateTxnStatus:ntext',
                    //'estimateEmailStatus:email',
                    //'estimateBillEmail:email',
                    'estimateTaxAmount',
                    'estimateDiscountAmount',
                    'estimateSubAmount',
                    'estimateAmount',
                    //'estimatedCreatedby',
                    //'estimatedCreatedAt',
                    //'estimatedUpdatedAt',
                    //'estimatedDeletedAt',
                    //'approvedby',
                    [
                        'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                        'items' => [
                            [
                                'label' => 'View',
                                'url'   => ['view'],
                            ],
                            [
                                'label' => 'Export',
                                'url'   => ['#'],
                            ],
                            [
                                'label'   => 'Disable',
                                'url'     => ['disable'],
                                'linkOptions' => [
                                    'data-method' => 'post'
                                ],
                            ],
                        ]
                    ],
                ],
            ]); ?>

          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="invoice">
            <form class="form-horizontal">

              <?= GridView::widget([
                  'dataProvider' => $modelInvoice,
                  'columns' => [
                      ['class' => 'yii\grid\SerialColumn'],
                      // 'id',
                      'invoiceNumber',
                      // 'customerId',
                      [
                        'attribute'=>'customerId',
                        'value'=>'customer.customerDisplayName',
                      ],
                      [
                        'attribute'=>'invoiceProjectId',
                        'value'=>'invoiceProject.eventName',
                      ],
                      // 'localpurchaseOrder',
                      //'invoiceProjectId',
                      'invoiceTxnDate',
                      'invoiceDeadlineDate',
                      //'invoiceNote:ntext',
                      //'invoiceFooter:ntext',
                      //'invoiceTxnStatus:ntext',
                      //'invoiceEmailStatus:email',
                      //'invoiceBillEmail:email',
                      'invoiceTaxAmount',
                      'invoiceDiscountAmount',
                      'invoiceSubAmount',
                      'invoiceAmount',
                      //'invoicedCreatedby',
                      //'invoicedCreatedAt',
                      //'invoicedUpdatedAt',
                      //'invoicedDeletedAt',
                      //'approvedby',
                  ],
              ]); ?>

            </form>
          </div>
          <div class="tab-pane" id="events">
            <?= GridView::widget([
                'dataProvider' => $modelEvents,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    //'customerId',
                    // 'customer.customerDisplayName',
                    'eventNumber',
                    'eventName',
                    'eventTheme',
                    'eventVistorsNumber',
                    // 'eventCategory',
                    'eventCategory0.eventCatName',
                    //'eventStartDate',
                    //'eventEndDate',
                    //'eventStartTime',
                    //'eventEndTime',
                    //'eventSetUpDateTime',
                    //'eventSetDownDateTime',
                    'eventLocation',
                    //'eventDescription:ntext',
                    //'eventNotes:ntext',
                    //'eventCreatedAt',
                    //'eventLastUpdatedAt',
                    //'eventDeletedAt',
                    //'userId',
                ],
            ]); ?>
            <!-- The events -->
            <!-- <ul class="events events-inverse">
              <!-- events time label -->
                <!-- <li class="time-label">
                    <span class="bg-red">
                    </span>
                </li> -->
                <!-- /.events-label -->
              <!-- events time label -->



              <!-- <li>
                <i class="fa fa-clock-o bg-gray"></i>
              </li> -->
            <!-- </ul> --> -->

          </div>
          <!-- /.tab-pane -->

          <div class="tab-pane" id="details">
            <form class="form-horizontal">


                  <?= DetailView::widget([
                      'model' => $model,
                      'attributes' => [
                          // 'id',
                          // 'customerNumber',
                          'customerFullname',
                          'customerCompanyName',
                          'customerKraPin',
                          'customerDisplayName',
                          'customerPrimaryPhone',
                          'customerPrimaryEmail:email',
                          // 'customerstatus',
                          'customercity',
                          'customerPhyscialAdress',
                          'customerCounty',
                          'customerCountry',
                          'customerAddress',
                          // 'customerCreateTime',
                          // 'customerUpdatedAt',
                          // 'customerDeleteAt',
                          // 'createdBy',
                          // 'userId',
                      ],
                  ]) ?>


            </form>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</section>
<!-- /.content -->





<?php
//  GridView::widget([
//     'dataProvider' => $modelInvoice,
//     'columns' => [
//         ['class' => 'yii\grid\SerialColumn'],
//
//         // 'id',
//         'invoiceNumber',
//         'customerId',
//         'localpurchaseOrder',
//         //'invoiceProjectId',
//         'invoiceTxnDate',
//         'invoiceDeadlineDate',
//         //'invoiceNote:ntext',
//         //'invoiceFooter:ntext',
//         //'invoiceTxnStatus:ntext',
//         //'invoiceEmailStatus:email',
//         //'invoiceBillEmail:email',
//         'invoiceTaxAmount',
//         'invoiceDiscountAmount',
//         'invoiceSubAmount',
//         'invoiceAmount',
//         //'invoicedCreatedby',
//         //'invoicedCreatedAt',
//         //'invoicedUpdatedAt',
//         //'invoicedDeletedAt',
//         //'approvedby',
//     ],
// ]); ?>
