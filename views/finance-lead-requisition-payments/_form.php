<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;

use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;
use app\models\FinanceLeadRequstionForm;



/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequisitionPayments */
/* @var $form yii\widgets\ActiveForm */
?>


<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">

      <div class="finance-lead-requisition-payments-form">

          <?php $form = ActiveForm::begin(); ?>

      </div>
      <div class="form-group">
        <?php
            // necessary for update action.
            if (! $model->isNewRecord) {
              // code...

              $reqrecepitNumber = $model->reqrecepitNumber;

              ?>
              <?= $form->field($model, 'reqrecepitNumber')->textInput(['readonly' => true, 'value' => $reqrecepitNumber])?>
              <?php

            }

        ?>

      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'reqpaymentName')->textInput(['maxlength' => true]) ?>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">

            <?= $form->field($model, 'reqpaymentAccount')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(FinanceAccounts::find()->where(['like', 'accountsPays', 'yes'])->all(),'id','fullyQualifiedName'),
                'language' => 'en',
                'options' => ['placeholder' => 'Select a Payment Account ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);  ?>

          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-sm-6">
          <div class="form-group">

            <?= $form->field($model, 'reqpaymentTxnDate')->widget(\yii\jui\DatePicker::class, [
                  //'language' => 'ru',
                  'options' => ['class' => 'form-control'],
                  'inline' => false,
                  'dateFormat' => 'yyyy-MM-dd',
              ]); ?>

          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
          <?php $data = ['Cash' => 'Cash', 'Debit Card'=> 'Debit Card', 'Credit Card' => 'Credit Card','Transfer' => 'Transfer','Voucher' => 'Voucher','Mobile Payment' => 'Mobile Payment','Internet Payment' => 'Internet Payment']; ?>
          <?= $form->field($model, 'reqpaymentType')->widget(Select2::classname(), [
              'data' => $data,
              'language' => 'en',
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ]); ?>
          </div>
        </div>

      </div>


      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">

            <?= $form->field($model, 'reqpaymentRefence')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">

            <?= $form->field($model, 'reqpaymentTotalAmt')->textInput() ?>

          </div>
        </div>
      </div>


      <div class="row">

        <div class="col-sm-6">
          <div class="form-group">

            <?= $form->field($model, 'reqpaymentProcessduration')->textInput() ?>

          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">

            <?= $form->field($model, 'reqpaymentLinkedTxn')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(FinanceLeadRequstionForm::find()->all(),'requstionNumber','requstionNumber'),
                'language' => 'en',
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

          </div>
        </div>


      </div>

      <div class="row">


        <div class="col-sm-6">
          <div class="form-group">

            <?= $form->field($model, 'reqpaymentLinkedTxnType')->textInput(['readonly' => true, 'value' => "LeadReq"]) ?>

          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
            <?= $form->field($model, 'reqpaymentStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

          </div>
        </div>
      </div>


          <div class="col-sm-3">
            <div class="form-group">

            </div>
          </div>

  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
    <!-- /.box -->


<?php


?>

<div class="finance-lead-requisition-payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reqrecepitNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentAccount')->textInput() ?>

    <?= $form->field($model, 'reqpaymentTxnDate')->textInput() ?>

    <?= $form->field($model, 'reqpaymentType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentRefence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentTotalAmt')->textInput() ?>

    <?= $form->field($model, 'reqpaymentProcessduration')->textInput() ?>

    <?= $form->field($model, 'reqpaymentLinkedTxn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentLinkedTxnType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentStatus')->textInput() ?>

    <?= $form->field($model, 'reqpaymentCreateAt')->textInput() ?>

    <?= $form->field($model, 'reqpaymentUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'reqpaymentDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
