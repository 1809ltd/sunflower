<?php

namespace app\modules\finance\transfer\controllers;

use Yii;
use app\modules\finance\transfer\models\FinanceTransferee;
use app\modules\finance\transfer\models\FinanceTransfereeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceTransfereeController implements the CRUD actions for FinanceTransferee model.
 */
class FinanceTransfereeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->transfereeUpdatedAt = date('Y-d-m h:i:s');
        /**/

      }else {
        // code...
        $this->transfereeUpdatedAt = date('Y-d-m h:i:s');
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceTransferee models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinanceTransfereeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceTransferee model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceTransferee model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // $model = new FinanceTransferee();

        $this->layout = '@app/views/layouts/addformdatatablelayout';
        // $this->layout='addformdatatablelayout';
        $model = new FinanceTransferee();
        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;
        $model->createdBy = Yii::$app->user->identity->id;
        $model->transfereeCreatedAt = date('Y-d-m h:i:s');

          if ($model->load(Yii::$app->request->post()) && $model->save()) {
              // return $this->redirect(['view', 'id' => $model->id]);
              return $this->redirect(['index']);
          }

          return $this->renderAjax('create', [
              'model' => $model,
          ]);
    }

    /**
     * Updates an existing FinanceTransferee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        // $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FinanceTransferee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceTransferee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceTransferee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceTransferee::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
