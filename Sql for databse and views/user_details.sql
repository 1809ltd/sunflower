/*
 Navicat Premium Data Transfer

 Source Server         : wateka server
 Source Server Type    : MySQL
 Source Server Version : 50641
 Source Host           : localhost:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 50641
 File Encoding         : 65001

 Date: 08/02/2019 09:26:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_details
-- ----------------------------
DROP TABLE IF EXISTS `user_details`;
CREATE TABLE `user_details`  (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `userStaffId` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `userFName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `userLName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `userPhone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `userEmail` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `authKey` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password_reset_token` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `userImage` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `userStatus` int(5) NOT NULL,
  `userLastLogin` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `userCreatedBy` int(50) NOT NULL,
  `userDeleteAt` timestamp(0) NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `userPhone`(`userPhone`) USING BTREE,
  UNIQUE INDEX `userEmail`(`userEmail`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_details
-- ----------------------------
INSERT INTO `user_details` VALUES (1, ' ', 'Kenedy', 'Maikuma', '254732386906', 'mukhwanak@1809ltd.co.ke', 'lingo', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (2, ' ', 'Ian', 'Kiki', '254735203677', 'ian@1809ltd.co.ke', 'Kiki', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (3, ' ', 'Martin', 'Kip', '254734808007', 'martin@1809ltd.co.ke', 'Marto', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (4, ' ', 'Brian', 'Karoney', '254722140588', 'brian@1809ltd.co.ke', 'K', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (5, '1', 'Peris', 'Wanjiku', '254213213217221', '546546@1809ltd.co.ke', 'Peris', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (6, '2', 'Paul', 'Ngei', '25472480644', '5454@1809ltd.co.ke', 'Paul', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (7, '3', 'Elizabeth', 'Wanjiru', '1232132321', '212154542@1809ltd.co.ke', 'Elizabeth', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (8, '4', 'Stephen', 'Kamau', '254754654654', '2154221@1809ltd.co.ke', 'Stephen', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (9, '5', 'Florence', 'Wandahi', '5468415', '215545421@1809ltd.co.ke', 'Florence', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (10, '6', 'Batroba', 'Nashali', '65498765', '2145421@1809ltd.co.ke', 'Batroba', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (11, '7', 'Maureen', 'Nelima', '2165465', '212654651@1809ltd.co.ke', 'Maureen', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (12, '8', 'Joy', 'Njeri', '6546454', '2221121@1809ltd.co.ke', 'Joy', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (13, '9', 'Jenniffer', 'Anjere', '6465456', '212212121@1809ltd.co.ke', 'Jenniffer', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (14, '10', 'Christine', 'Gakui', '54212165465', '21211221@1809ltd.co.ke', 'Christine', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');
INSERT INTO `user_details` VALUES (15, '11', 'Leshan', '–Events', '5465465', '2121121@1809ltd.co.ke', 'Leshan', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, '2019-02-06 09:24:53', 1, '0000-00-00 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
