-- All Bills with respective Payments
-- Aged Payables
-- ----------------------------
-- View structure for `age_payables`
-- ----------------------------
DROP VIEW IF EXISTS `age_payables`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `age_payables` AS
SELECT
	finance_bills.vendorId,
	vendor_company_details.vendorCompanyName as payables,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesAmount, 0 ))
        + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesTaxAmount, 0 ))
        -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
				-( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
				-( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
		END
  ) AS `Coming Due`,
	(
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 1 AND 30, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 1 AND 30, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 1 AND 30, finance_bills_lines.biilsLinesAmount, 0 ))
        + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  BETWEEN 1 AND 30, finance_bills_lines.biilsLinesTaxAmount, 0 ))
        -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
		END
  ) AS `1-30 Days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesAmount, 0 ))
        + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesTaxAmount, 0 ))
        -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
		END
  ) AS `31-60 Days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 61 AND 90, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 61 AND 90, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 61 AND 90, finance_bills_lines.biilsLinesAmount, 0 ))
        + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  BETWEEN 61 AND 90, finance_bills_lines.biilsLinesTaxAmount, 0 ))
        -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
		END
  ) AS `61-90 Days`,
  (
    CASE
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) >90, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
       THEN 0 -- Output
      WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) >90, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
       THEN
       -- Add the paymensts made to the specifc invoice
        Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) >90, finance_bills_lines.biilsLinesAmount, 0 ))
        + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  >90, finance_bills_lines.biilsLinesTaxAmount, 0 ))
        -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
        -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
		END
  ) AS `>90 Days`,
  (SELECT COALESCE ( Sum(finance_bills_payments.unallocatedAmount), 0 )
      FROM finance_bills_payments
    WHERE
     finance_bills_payments.vendorId = finance_bills.vendorId
  ) as `unlocated`,
  (

    (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
         Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesAmount, 0 ))
         + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  = 0, finance_bills_lines.biilsLinesTaxAmount, 0 ))
         -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
 				-( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
 				-( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
  		END
    )-- Getting the Value for 0 Days
    + (
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 1 AND 30, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 1 AND 30, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 1 AND 30, finance_bills_lines.biilsLinesAmount, 0 ))
          + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  BETWEEN 1 AND 30, finance_bills_lines.biilsLinesTaxAmount, 0 ))
          -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
  		END
    ) --  AS `1-30 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesAmount, 0 ))
          + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 31 AND 60, finance_bills_lines.biilsLinesTaxAmount, 0 ))
          -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
  		END
    ) -- AS `31-60 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 61 AND 90, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 61 AND 90, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) BETWEEN 61 AND 90, finance_bills_lines.biilsLinesAmount, 0 ))
          + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  BETWEEN 61 AND 90, finance_bills_lines.biilsLinesTaxAmount, 0 ))
          -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
  		END
    ) -- AS `61-90 Days`
    +(
      CASE
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) >90, finance_bills_lines.biilsLinesAmount, 0 )) = 0 -- condtion to see if the date has no bills
         THEN 0 -- Output
        WHEN Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) >90, finance_bills_lines.biilsLinesAmount, 0 )) > 0 -- Checking if the value of bills in the date range is greater than Zero
         THEN
         -- Add the paymensts made to the specifc invoice
          Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) ) >90, finance_bills_lines.biilsLinesAmount, 0 ))
          + Sum(IF( DATEDIFF( CURDATE( ), date( finance_bills.billDueDate ) )  >90, finance_bills_lines.biilsLinesTaxAmount, 0 ))
          -( SELECT COALESCE ( SUM( finance_bills_payments_lines.amount ), 0 ) FROM finance_bills_payments_lines WHERE finance_bills_payments_lines.billId = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ), 0 ) FROM finance_vendor_credit_memo_lines WHERE finance_vendor_credit_memo_lines.billRef = finance_bills.id)
          -( SELECT COALESCE ( SUM( finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount ), 0 ) FROM finance_vendor_offset_memo_lines WHERE finance_vendor_offset_memo_lines.billRef = finance_bills.id)
  		END
    ) -- AS `>90 Days`
    +
    (SELECT COALESCE ( Sum(finance_bills_payments.unallocatedAmount), 0 )
        FROM finance_bills_payments
      WHERE
       finance_bills_payments.vendorId = finance_bills.vendorId
    ) -- as `unlocated`,
  ) AS `Total`

	FROM
		finance_bills
		LEFT JOIN finance_bills_lines ON finance_bills_lines.biilsId = finance_bills.id
		LEFT JOIN vendor_company_details ON vendor_company_details.id = finance_bills.vendorId
		where
			finance_bills.billStatus > 0
			AND finance_bills_lines.billsLinesBillableStatus > 0
GROUP BY
finance_bills.vendorId
ORDER BY `payables` ASC
