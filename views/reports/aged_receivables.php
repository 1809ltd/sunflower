<?php

use yii\helpers\Html;
use app\models\AgedReceivables;

/* @var $this yii\web\View */

?>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?= date('Y-m-d'); ?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>
           <strong>SUNFLOWER EVENTS,</strong><br/>
            Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
           Nairobi, 00100 Kenya<br/>
           Phone: +254 (0) 722 790632<br/>
           Email: info@...com
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>


         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>As of: <?= date('Y-m-d H:i:s'); ?></b><br>
         <br>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-hover">
           <thead>
           <tr>

               <th>Receivable</th>
               <th>Coming Due</th>
               <th>1- 30 days</th>
               <th>31- 60 days</th>
               <th>61- 90 days</th>
               <th>90 days</th>
               <th>Total</th>

           </tr>
           </thead>
           <tbody>
             <?php

             $agedreceiables= AgedReceivables::find()->all();

             $Sumcoming=0;
             $Sum30=0;
             $Sum60=0;
             $Sum90=0;
             $Sumc90=0;
             $SumTT=0;



             //

             foreach ($agedreceiables as $agedreceiable) {
               // code...

               $agedreceiable->customerId;

               $Sumcoming+= $agedreceiable["Coming Due"];
               $Sum30+= $agedreceiable["1-30 Days"];
               $Sum60+= $agedreceiable["31-60 Days"];
               $Sum90+= $agedreceiable["61-90 Days"];
               $Sumc90+= $agedreceiable[">90 Days"];
               $SumTT+= $agedreceiable->Total;


               ?>
               <tr>

                 <td><?= $agedreceiable->receivables;?></td>
                 <td><?= number_format($agedreceiable["Coming Due"],2);?></td>
                 <td><?= number_format($agedreceiable["1-30 Days"],2);?></td>
                 <td><?= number_format($agedreceiable["31-60 Days"],2);?></td>
                 <td><?= number_format($agedreceiable["61-90 Days"],2);?></td>
                 <td><?= number_format($agedreceiable[">90 Days"],2);?></td>
                 <td><?= number_format($agedreceiable->Total,2);?></td>

               </tr>


               <?php


             }



              ?>

              <tr>

                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>

              </tr>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <img src="adminlte/dist/img/credit/visa.png" alt="Visa">
          <img src="adminlte/dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="adminlte/dist/img/credit/american-express.png" alt="American Express">
          <img src="adminlte/dist/img/credit/paypal2.png" alt="Paypal">

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            * Make all cheques payable to Sunflower Events<br/>
            * Payment is due within 30 days<br/>
            * If you have any questions concerning this invoice, contact [Name, Phone Number,
            Email]
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead"></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Coming due:</th>
                <td>Ksh <?=  number_format($Sumcoming,2);?></td>
              </tr>
              <tr>
                <th>1-30 Days</th>
                <td>Ksh <?=  number_format($Sum30,2); ?></td>
              </tr>
              <tr>
                <th>31-60 Days:</th>
                <td>Ksh <?=  number_format($Sum60,2); ?></td>
              </tr>
              <tr>
                <th>61-90 Days:</th>
                <td>Ksh <?=  number_format($Sum90,2); ?></td>
              </tr>
              <tr>
                <th>More than 90 Days:</th>
                <td>Ksh <?=  number_format($Sumc90,2); ?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>Ksh <?=  number_format($SumTT,2); ?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
