<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfromList */

$this->title = 'Update Event Orderfrom List: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Orderfrom Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-orderfrom-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
