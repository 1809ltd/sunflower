<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_site_details".
 *
 * @property int $siteId
 * @property int $companyId
 * @property string $siteName
 * @property string $siteAddress
 * @property string $siteSuite
 * @property string $siteCity
 * @property string $siteCounty
 * @property string $sitePostal
 * @property string $siteCountry
 * @property int $siteStatus
 * @property string $siteTimestamp
 * @property string $siteUpdatedAt
 * @property string $siteDeletedAt
 * @property int $companyUserId
 *
 * @property AssetSiteLocation[] $assetSiteLocations
 * @property CompanyDetails $company
 * @property UserDetails $companyUser
 * @property CompanySiteLocation[] $companySiteLocations
 */
class CompanySiteDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_site_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyId', 'siteName', 'siteAddress', 'siteSuite', 'siteCity', 'siteCounty', 'sitePostal', 'siteCountry', 'siteStatus'], 'required'],
            [['companyId', 'siteStatus', 'companyUserId'], 'integer'],
            [['siteTimestamp', 'siteUpdatedAt', 'siteDeletedAt', 'companyUserId'], 'safe'],
            [['siteName', 'siteAddress', 'siteSuite', 'siteCity', 'siteCounty', 'sitePostal', 'siteCountry'], 'string', 'max' => 255],
            [['siteName'], 'unique'],
            [['companyId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyDetails::className(), 'targetAttribute' => ['companyId' => 'id']],
            [['companyUserId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['companyUserId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Site Name',
            'companyId' => 'Company Name',
            'siteName' => 'Site Name',
            'siteAddress' => 'Site Address',
            'siteSuite' => 'Site Suite',
            'siteCity' => 'Site City',
            'siteCounty' => 'Site County',
            'sitePostal' => 'Site Postal',
            'siteCountry' => 'Site Country',
            'siteStatus' => 'Site Status',
            'siteTimestamp' => 'Site Timestamp',
            'siteUpdatedAt' => 'Site Updated At',
            'siteDeletedAt' => 'Site Deleted At',
            'companyUserId' => 'Company User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetSiteLocations()
    {
        return $this->hasMany(AssetSiteLocation::className(), ['siteId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(CompanyDetails::className(), ['id' => 'companyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(UserDetails::className(), ['userId' => 'companyUserId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySiteLocations()
    {
        return $this->hasMany(CompanySiteLocation::className(), ['siteId' => 'siteId']);
    }
}
