<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
/*Pop up menu*/
use yii\helpers\Url;
// use  yii\bootstrap\Modal;

use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform;
use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryformSearch;


use kartik\select2\Select2;

/*Dependant Drop*/
use kartik\depdrop\DepDrop;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Get Event Details*/
use app\modules\event\eventInfo\models\EventDetails;

?>
<?php
if (isset($_SESSION['eventId'])) {
  // code...
  $eventId = $_SESSION['eventId'];
  $orderId= \Yii::$app->session->get('orderId');

  // echo $eventId;


  $modelEvent = EventDetails::find()
          ->where('id = :eventId', [':eventId' => $eventId])
          ->one();

          // die();

  $modelsAssetCheckOut = new AssetDeliveryform();
  $output= $modelsAssetCheckOut->getallassetcheckoutsonhold($eventId);

  $searchModel = new AssetDeliveryformSearch();
  $searchModel->eventId = $eventId;
  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

  ?>
  <div class="col-md-4">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Event Details</h3>

          <div class="box-tools pull-right">
          </div>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped table-condensed">
            <thead>
              <tr>
                <th style="width: 40%;">Title</th>
                <th style="width: 60%;">Detail</th>
              </tr>
            </thead>
              <tbody>
                <tr><td><span>Event Name :</span></td><td> <?= $modelEvent->eventName;?> </td></tr>
                <tr><td><span>Event Number :</span></td><td><?= $modelEvent->eventNumber;?> </td></tr>
                <tr><td><span>Customer :</span></td><td><?= $modelEvent->customer['customerFullname']?></td></tr>
                <tr><td><span>Event date :</span></td><td><?= Yii::$app->formatter->asDate($modelEvent->eventStartDate, 'long');?></td></tr>
                <tr><td><span>Set up date :</span></td><td><?= Yii::$app->formatter->asDate($modelEvent->eventSetUpDateTime, 'long');?>
                <tr><td><span>Contract Status :</span></td><td><span class="label label-success">Active</span></td></tr>

                <tr><td><span>Set up date :</span></td>
                  <td>
                                  </td>
                </tr>
                <tr><td colspan="2"><div class="col-md-2">
                  <div class="form-actions">
                    <a href="<?=Url::to(['closesearch']) ?>" class="btn btn-warning btn-sm pull-left text-center" >Close Search</a>
                  </div>
                </div></td></tr>
              </tbody>
          </table>
        </div>
    </div>
  </div>

  <div class="col-md-8">

    <?php

    // $this->render('@app/inventory/assetmovement/assetcheckout/asset-check-out/index', [
    //       'output' => $output,
        // 'model'=>$model,
        // // 'output'=>$output,
    // ]) ?>
    <?=
    $this->render('_holdassestcheckout', [
        'model' => $model,
        // 'searchModel' => $searchModel,
        'output' => $output,
        // 'model'=>$model,
        // 'output'=>$output,
    ]) ?>

  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Deilvry Note</h3>

            <div class="box-tools pull-right">

            </div>
          </div>
          <div class="box-body">


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    'deliveryNumber',
                    'event.eventName',
                    // 'eventId',
                    // 'orderId',
                    'duedate',
                    'date',
                    // 'deliveryStatus',
                    'delivery',
                    // 'createdAt',
                    // 'updatedAt',
                    // 'deletedAt',
                    // 'userId',
                    'approvedBy',
                    [
                        'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                        'items' => [
                            [
                                'label' => 'View',
                                'url'   => ['view'],
                            ],
                        ]
                    ],


                  // ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>


          </div>
        </div>
      </div>
    </div>


  <?php
} else {
  // code...

  ?>
  <!-- begin row -->
  <div class="box">
    <div class="box-header">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <!-- /.box-header -->
    <div id="toggleSearch" class="box-body delivery-list-index" style="display:block">

      <?php $form = ActiveForm::begin([
                        'id' => 'deliverysearch-form',
                        // 'enableAjaxValidation' => true,
                    ]);?>
      <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(CustomerDetails::find()->where('`customerstatus` = 1')->asArray()->all(),'id','customerDisplayName'),
                  'language' => 'en',
                  'options' => ['id'=>'customerId','prompt'=>'Select...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]);?>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'eventId')->widget(DepDrop::classname(), [
                  'options'=>['id'=>'eventId'],
                  'data' => [$model->eventId => $model->eventId],
                  'type' => DepDrop::TYPE_SELECT2,
                  'pluginOptions'=>[
                      'depends'=>['customerId'],
                      'initialize' => true,
                      // 'initDepends'=>['customerId'],
                      'placeholder'=>'Select...',
                      'url'=>Url::to(['/event/eventdocs/orderfroms/orderformsitems/event-orderfrom-list/eventdetails'])
                  ]
              ]);?>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'orderId')->widget(DepDrop::classname(), [
                  'type' => DepDrop::TYPE_SELECT2,
                  'data' => [$model->orderId => $model->orderId],
                  'pluginOptions'=>[
                      'depends'=>['customerId', 'eventId'],
                      'initialize' => true,
                      'initDepends'=>['customerId', 'eventId'],
                      'placeholder'=>'Select...',
                      'url'=>Url::to(['/event/eventdocs/orderfroms/orderformsitems/event-orderfrom-list/ordernumber'])
                  ]
              ]); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>

            </div>
        </div>
      </div>

    <?php ActiveForm::end(); ?>

    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->

  <?php
}



 ?>
