<?php

namespace app\modules\event\transportation\models;
/*AssetREgisrtartion*/
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;

use app\modules\event\eventInfo\models\EventDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Order form */
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
/*VEndor details*/
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;


use Yii;

/**
 * This is the model class for table "event_transport".
 *
 * @property int $id
 * @property int $customerId
 * @property int $eventId
 * @property int $orderId
 * @property int $outsourced
 * @property int $assetId
 * @property string $numberPlate
 * @property int $vendorId
 * @property int $delivery
 * @property int $collection
 * @property string $deliveryTime
 * @property string $collectionTime
 * @property int $status
 * @property int $userId
 * @property int $approvedBy
 * @property int $createdBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property AssetCheckOut[] $assetCheckOuts
 * @property EventDetails $event
 * @property AssetRegistration $asset
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 * @property UserDetails $approvedBy0
 * @property UserDetails $createdBy0
 * @property CustomerDetails $customer
 * @property EventOrderfrom $order
 */
class EventTransport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_transport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerId', 'eventId', 'orderId', 'outsourced', 'status'], 'required'],
            [['customerId', 'eventId', 'orderId', 'outsourced', 'assetId', 'vendorId', 'delivery', 'collection', 'status', 'userId', 'approvedBy', 'createdBy'], 'integer'],
            [['deliveryTime', 'collectionTime', 'createdAt', 'updatedAt', 'deletedAt', 'userId', 'createdBy', 'createdAt'], 'safe'],
            [['numberPlate'], 'string', 'max' => 50],
            [['eventId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['eventId' => 'id']],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => EventOrderfrom::className(), 'targetAttribute' => ['orderId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customerId' => 'Customer',
            'eventId' => 'Event',
            'orderId' => 'Order',
            'outsourced' => 'Outsourced',
            'assetId' => 'Vehicle',
            'numberPlate' => 'Number Plate',
            'vendorId' => 'Supplier',
            'delivery' => 'Deliver',
            'collection' => 'Collection',
            'deliveryTime' => 'Deliver Time',
            'collectionTime' => 'Collection Time',
            'status' => 'Status',
            'userId' => 'User',
            'approvedBy' => 'Approved',
            'createdBy' => 'Created',
            'createdAt' => 'Created',
            'updatedAt' => 'Updated',
            'deletedAt' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['vehicle' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'eventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(EventOrderfrom::className(), ['id' => 'orderId']);
    }
}
