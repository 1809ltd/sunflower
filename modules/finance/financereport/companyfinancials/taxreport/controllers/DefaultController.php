<?php

namespace app\modules\finance\financereport\companyfinancials\taxreport\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// for db functions
use yii\db\Expression;
/*Tax Reports*/
use app\modules\finance\financereport\companyfinancials\taxreport\models\TaxReport;
// PDf Maneno
use Mpdf\Mpdf;
/**
 * Default controller for the `taxreport` module
**/
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // Create a model to allow the search
      $model = new \yii\base\DynamicModel([
        'startDate','endDate'
      ]);
      // Adding Rules for validation
      $model->addRule(['startDate','endDate'], 'required')
            ->validate();

      

      echo "we are Here";

      // if (Yii::$app->request->isAjax) {
      //   Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      //   if ($model->load(Yii::$app->request->post())) {
      //     // Get Tax Tax Report
      //     $taxPurchase = TaxReport::find()->select(['date','transactionCode','recipientId','referenceId','parties','kraPin','taxRate','taxCode','typeofTransaction','typeofRecipeint','taxCode','taxRate','COALESCE(SUM(taxableamount),0) as taxableamount','COALESCE(SUM(cr_amount),0) as cr_amount','COALESCE(SUM(dr_amount),0) as dr_amount'])
      //                                     ->where(['LIKE', 'typeofRecipeint', 'vendor'])
      //                                     ->andFilterWhere(['between', 'date', $model->startDate, $model->endDate])
      //                                     ->groupBy(['transactionCode', 'referenceId'])
      //                                     ->orderBy(['date' => SORT_ASC,])
      //                                     ->all();
      //
      //     $taxSales = TaxReport::find()->select(['date','transactionCode','recipientId','referenceId','parties','kraPin','taxRate','taxCode','typeofTransaction','typeofRecipeint','taxCode','taxRate','COALESCE(SUM(taxableamount),0) as taxableamount','COALESCE(SUM(cr_amount),0) as cr_amount','COALESCE(SUM(dr_amount),0) as dr_amount'])
      //                                     ->where(['LIKE', 'typeofRecipeint', 'customer'])
      //                                     ->andFilterWhere(['between', 'date', $model->startDate, $model->endDate])
      //                                     ->groupBy(['transactionCode', 'referenceId'])
      //                                     ->orderBy(['date' => SORT_ASC,])
      //                                     ->all();
      //
      //     print_r($taxSales);
      //     print_r($taxPurchase);
      //     die();
      //     // return [
      //     //   'data' => [
      //     //     'success' => true,
      //     //     'model' => $model,
      //     //     'info'=>$this->renderPartial('monthsreport', [
      //     //       'taxPurchase' => $taxPurchase,
      //     //       'taxSales' => $taxSales,
      //     //       'startDate' => $model->startDate,
      //     //       'endDate' => $model->endDate,
      //     //       'model' => $model,
      //     //       'tafuta' => $tafuta,
      //     //     ]),
      //     //     'message' => 'Model has been saved.',
      //     //   ],
      //     //   'code' => 1,
      //     // ];
      //   } else {
      //     return [
      //       'data' => [
      //         'success' => false,
      //         'model' => null,
      //         'info'=>null,
      //         'message' => 'An error occured.',
      //       ],
      //       'code' => 0, // Some semantic codes that you know them for yourself
      //     ];
      //   }
      // }

      // return $this->render('index', [
      //   'model' => $model,
      // ]);


      // Checking if it a load
      if($model->load(Yii::$app->request->post())){
        // do somenthing with model
        $tafuta="we are here";
        // Get Tax Tax Report
        $taxPurchase = TaxReport::find()->select(['date','transactionCode','recipientId','referenceId','parties','kraPin','taxRate','taxCode','typeofTransaction','typeofRecipeint','taxCode','taxRate','COALESCE(SUM(taxableamount),0) as taxableamount','COALESCE(SUM(cr_amount),0) as cr_amount','COALESCE(SUM(dr_amount),0) as dr_amount'])
                                            ->where(['LIKE', 'typeofRecipeint', 'vendor'])
                                            ->andFilterWhere(['between', 'date', $model->startDate, $model->endDate])
                                            ->groupBy(['transactionCode', 'referenceId'])
                                            ->orderBy(['date' => SORT_ASC,])
                                            ->all();

            $taxSales = TaxReport::find()->select(['date','transactionCode','recipientId','referenceId','parties','kraPin','taxRate','taxCode','typeofTransaction','typeofRecipeint','taxCode','taxRate','COALESCE(SUM(taxableamount),0) as taxableamount','COALESCE(SUM(cr_amount),0) as cr_amount','COALESCE(SUM(dr_amount),0) as dr_amount'])
                                            ->where(['LIKE', 'typeofRecipeint', 'customer'])
                                            ->andFilterWhere(['between', 'date', $model->startDate, $model->endDate])
                                            ->groupBy(['transactionCode', 'referenceId'])
                                            ->orderBy(['date' => SORT_ASC,])
                                            ->all();

        if (!empty($tafuta)) {
        // code...
        return $this->render('monthsreport', [
                  'taxPurchase' => $taxPurchase,
                  'taxSales' => $taxSales,
                  'startDate' => $model->startDate,
                  'endDate' => $model->endDate,
                  'model' => $model,
                  'tafuta' => $tafuta,
                ]);


        // $this->layout='addformdatatablelayout';
        // $this->layout = '@app/views/layouts/pdflayout';
        // $mpdf = new mPDF( array(
        //   'mode' => '',
        //   'format' => 'A4',
        //   'default_font_size' => 0,
        //   'default_font' => '',
        //   'margin_left' => 15,
        //   'margin_right' => 15,
        //   'margin_top' => 16,
        //   'margin_bottom' => 16,
        //   'margin_header' => 9,
        //   'margin_footer' => 9,
        //   'orientation' => 'P'
        // ));
        //
        // $pdf_info= $this->renderPartial('monthsreport', [
        //           'taxPurchase' => $taxPurchase,
        //           'taxSales' => $taxSales,
        //           'startDate' => $model->startDate,
        //           'endDate' => $model->endDate,
        //           'model' => $model,
        //           'tafuta' => $tafuta,
        //         ]);
        //
        // $leo = date('M j, Y', strtotime(date('Y-m-d')));
        // // $mpdf->cssInline= '@app/views/layouts/pdflayout' ;
        // $mpdf->SetHeader('As of : '.$leo);
        // $mpdf->shrink_tables_to_fit = 1;
        // $mpdf->WriteHTML($pdf_info);
        // $mpdf->Output();
        // exit;

        } else {
          // code...
          return $this->render('index', [
            'model' => $model,
          ]);
       }
     }else {
       // code...
       return $this->render('index', [
          'model' => $model,
        ]);
    }

  }
  // View
  public function actionView($startDate,$endDate)
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // Create a model to allow the search
    $model = new \yii\base\DynamicModel([
      'startDate','endDate'
    ]);
    // Adding Rules for validation
    $model->addRule(['startDate','endDate'], 'required')
          ->validate();

    $model->startDate=$startDate;
    $model->endDate=$endDate;

    $tafuta="we are here";
    // Get Tax Tax Report
    $tax = TaxReport::find()->select(['date','taxCode','taxRate','typeofTransaction','referenceId','transactionCode','details','taxableamount','cr_amount','dr_amount'])
                                    ->andFilterWhere(['between', 'date', $model->startDate, $model->endDate])
                                    // ->groupBy(['taxCode', 'taxRate'])
                                    // ->orderBy(['taxCode' => SORT_ASC,])
                                    ->all();

    return $this->render('view', [
                  'tax' => $tax,
        'startDate' => $model->startDate,
        'endDate' => $model->endDate,
        'model' => $model,
        'tafuta' => $tafuta,
      ]);

}
}
