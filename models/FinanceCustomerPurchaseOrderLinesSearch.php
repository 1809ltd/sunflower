<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceCustomerPurchaseOrderLines;

/**
 * FinanceCustomerPurchaseOrderLinesSearch represents the model behind the search form of `app\models\FinanceCustomerPurchaseOrderLines`.
 */
class FinanceCustomerPurchaseOrderLinesSearch extends FinanceCustomerPurchaseOrderLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'purchaseOrderId', 'userId'], 'integer'],
            [['itemRefName', 'purchaseOrderLinesDescription', 'purchaseOrderLinesTaxCodeRef', 'purchaseOrderLinesCreatedAt', 'purchaseOrderLinesUpdatedAt', 'purchaseOrderLinesDeletedAt'], 'safe'],
            [['purchaseOrderLinespack', 'purchaseOrderLinesQty', 'purchaseOrderLinesUnitPrice', 'purchaseOrderLinesAmount', 'purchaseOrderLinesTaxamount', 'purchaseOrderLinesDiscount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceCustomerPurchaseOrderLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'purchaseOrderId' => $this->purchaseOrderId,
            'purchaseOrderLinespack' => $this->purchaseOrderLinespack,
            'purchaseOrderLinesQty' => $this->purchaseOrderLinesQty,
            'purchaseOrderLinesUnitPrice' => $this->purchaseOrderLinesUnitPrice,
            'purchaseOrderLinesAmount' => $this->purchaseOrderLinesAmount,
            'purchaseOrderLinesTaxamount' => $this->purchaseOrderLinesTaxamount,
            'purchaseOrderLinesDiscount' => $this->purchaseOrderLinesDiscount,
            'purchaseOrderLinesCreatedAt' => $this->purchaseOrderLinesCreatedAt,
            'purchaseOrderLinesUpdatedAt' => $this->purchaseOrderLinesUpdatedAt,
            'purchaseOrderLinesDeletedAt' => $this->purchaseOrderLinesDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'itemRefName', $this->itemRefName])
            ->andFilterWhere(['like', 'purchaseOrderLinesDescription', $this->purchaseOrderLinesDescription])
            ->andFilterWhere(['like', 'purchaseOrderLinesTaxCodeRef', $this->purchaseOrderLinesTaxCodeRef]);

        return $dataProvider;
    }
}
