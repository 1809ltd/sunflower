<?php

namespace app\modules\customer\marketing\customerleadcommunicationtracker;

/**
 * customerleadcommunicationtracker module definition class
 */
class customerleadcommunicationtracker extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\customer\marketing\customerleadcommunicationtracker\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
