<?php

namespace app\models;


use app\models\EventOrderfromList;
use app\models\FinanceItems;

use app\models\EventDetails;

/*Getting the REquire Asset*/
use app\models\EventItemCategoryType;
use app\models\AssetCategoryType;

/*Getting the REquire Roles*/
use app\models\EventItemRole;
use app\models\EventRole;

use Yii;

/**
 * This is the model class for table "event_orderfrom".
 *
 * @property int $id
 * @property string $orderNumber
 * @property int $eventId
 * @property string $date
 * @property int $orderStatus
 * @property string $orderNote
 * @property int $personIncharge
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $approvedBy
 *
 * @property EventDetails $event
 * @property UserDetails $approvedBy0
 * @property UserDetails $user
 * @property EventOrderfromList[] $eventOrderfromLists
 */
class EventOrderfrom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_orderfrom';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orderNumber', 'eventId', 'date', 'orderStatus', 'userId', 'approvedBy'], 'required'],
            [['eventId', 'orderStatus', 'personIncharge', 'userId', 'approvedBy'], 'integer'],
            [['date', 'createdAt', 'updatedAt', 'personIncharge','deletedAt'], 'safe'],
            [['orderNote'], 'string'],
            [['orderNumber'], 'string', 'max' => 100],
            [['orderNumber'], 'unique'],
            [['eventId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['eventId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderNumber' => 'Order Number',
            'eventId' => 'Event',
            'date' => 'Date',
            'orderStatus' => 'Order Status',
            'orderNote' => 'Order Note',
            'personIncharge' => 'Person Incharge',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'approvedBy' => 'Approved By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'eventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfromLists()
    {
        return $this->hasMany(EventOrderfromList::className(), ['orderId' => 'id']);
    }
    public function geteventOrderfromList($id)
    {
      // return EventOrderfromList::find()
      //   ->where(['and', "orderId=$id"])
      //   ->all();

      $connection = Yii::$app->getDb();
      $command = $connection->createCommand("SELECT
                                            event_orderfrom_list.id AS id,
                                            event_orderfrom_list.itemid AS itemid,
                                            event_orderfrom_list.orderId AS orderId,
                                            event_orderfrom_list.details AS details,
                                            event_orderfrom_list.outsourced AS outsourced,
                                            event_orderfrom_list.vendorId AS vendorId,
                                            event_orderfrom_list.quantity AS quantity,
                                            event_orderfrom_list.`status`,
                                            finance_items.itemsName AS service,
                                            finance_items_category.financeItemCategoryName AS categoryName,
                                            finance_items_category_type.financeItemCategoryTypeName AS typeName,
                                            vendor_company_details.vendorCompanyName AS vendorName
                                            FROM
                                            event_orderfrom_list
                                            LEFT JOIN finance_items ON event_orderfrom_list.itemid = finance_items.id
                                            LEFT JOIN finance_items_category ON finance_items.itemCategory = finance_items_category.id
                                            LEFT JOIN finance_items_category_type ON finance_items_category_type.financeItemCategoryId = finance_items_category.id AND finance_items.itemCategoryType = finance_items_category_type.id
                                            LEFT JOIN vendor_company_details ON event_orderfrom_list.vendorId = vendor_company_details.id
                                            WHERE
                                            event_orderfrom_list.orderId = :orderformId", [':orderformId' => $id]);

      $data = $command->queryAll();

      return $data;
    }

    public function geteventDetailss($id)
    {
      return EventDetails::find()
        ->where(['and', "id=$id"])
        ->all();
    }

    public function getItemsFinance($id)
    {
      return FinanceItems::find()
        ->where(['and', "id=$id"])
        ->all();
    }
    public function getCustomerDetails($id)
    {
      return CustomerDetails::find()
        ->where(['and', "id=$id"])->all();
        // ->indexBy('id')->column();
    }

    /*GEt Asset List*/

    public function getAssetrequired($id)
    {
      return EventItemCategoryType::find()
        ->where(['and', "status=1", "itemId=$id"])->all();
        // ->indexBy('id')->column();

    }

    /*GEt Category Name*/

    public function getCategotryTypeName($id)
    {
      return AssetCategoryType::find()
        ->where(['and', "id=$id"])->all();
        // ->indexBy('id')->column();

    }

    /*Get Work force Details*/

    public function getRolerequired($id)
    {
      return EventItemRole::find()
        ->where(['and', "status=1", "itemId=$id"])->all();
        // ->indexBy('id')->column();

    }

    /*GEt Role Name*/

    public function getRoleName($id)
    {
      return EventRole::find()
        ->where(['and', "id=$id"])->all();
        // ->indexBy('id')->column();

    }

    public function getOrdnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(orderNumber) AS number from event_orderfrom where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "ORD-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "ORD-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "ORD-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
