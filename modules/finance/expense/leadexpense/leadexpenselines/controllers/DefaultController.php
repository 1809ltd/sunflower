<?php

namespace app\modules\finance\expense\leadexpense\leadexpenselines\controllers;

use yii\web\Controller;

/**
 * Default controller for the `leadexpenselines` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
