<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;


/*Asset*/
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;

?>
    <?php
    // echo Menu::widget([
    //   'items' => [
    //         ['label' => 'MAIN NAVIGATION', 'options' => ['class' => 'header']
    //       ], // here
    //         // ... a group items
    //         [
    //             'label' => 'Dashboard',
    //             'url' => ['/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //         ],
    //             ['label' => 'Company',
    //             'icon' => '',
    //             'url' => ['/company/companydetails/company-details/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Department',
    //             'icon' => '',
    //             'url' => ['/company/companydepartment/company-department/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Site',
    //             'icon' => '',
    //             'url' => ['/company/companysite/company-site-details/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Location',
    //             'icon' => '',
    //             'url' => ['/company/companysitelocation/company-site-location/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Asset Category',
    //             'icon' => '',
    //             'url' => ['/inventory/assetsetup/assetcategory/asset-category/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Asset Category Type',
    //             'icon' => '',
    //             'url' => ['/inventory/assetsetup/assetcategorytype/asset-category-type/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Asset Registration',
    //             'icon' => '',
    //             'url' => ['/inventory/asset/assetregistration/asset-registration/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Asset Valuation',
    //             'icon' => '',
    //             'url' => ['/inventory/asset/assetvaluation/asset-preciation/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Delivery Note',
    //             'icon' => '',
    //             'url' => ['/inventory/assetmovement/delivery/asset-deliveryform/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Checkout',
    //             'icon' => '',
    //             'url' => ['/inventory/assetmovement/assetcheckout/asset-check-out/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Asset Location',
    //             'icon' => '',
    //             'url' => ['/inventory/asset/assetlocation/asset-site-location/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Supplier',
    //             'icon' => '',
    //             'url' => ['/vendor/vendordetails/vendor-company-details/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Customer',
    //             'icon' => '',
    //             'url' => ['/customer/customerdetails/customer-details/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Customer Lead',
    //             'icon' => '',
    //             'url' => ['/customer/marketing/customerlead/customer-lead/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Lead Appotintment',
    //             'icon' => '',
    //             'url' => ['/customer/marketing/customerleadappointment/customer-lead-appointment/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Lead Traacker',
    //             'icon' => '',
    //             'url' => ['/customer/marketing/customerleadcommunicationtracker/customer-lead-communication-tracker/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Event Category',
    //             'icon' => '',
    //             'url' => ['/event/eventsetup/eventcaterogies/event-caterogies/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Event role',
    //             'icon' => '',
    //             'url' => ['/event/eventsetup/eventrole/event-role/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Service Role',
    //             'icon' => '',
    //             'url' => ['/event/eventsetup/eventitemrole/event-item-role/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Service and Asset',
    //             'icon' => '',
    //             'url' => ['/event/eventsetup/eventitemcategorytype/event-item-category-type/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Event',
    //             'icon' => '',
    //             'url' => ['/event/eventInfo/event-details/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Order Form',
    //             'icon' => '',
    //             'url' => ['/event/eventdocs/orderfroms/event-orderfrom/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Transportation',
    //             'icon' => '',
    //             'url' => ['/event/transportation/event-transport/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Personnel',
    //             'icon' => '',
    //             'url' => ['/personnel/personneldetails/personnel-details/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Finance Category',
    //             'icon' => '',
    //             'url' => ['/finance/financesetup/financecategorytype/finance-category-type/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'COA',
    //             'icon' => '',
    //             'url' => ['/finance/financesetup/coa/finance-accounts/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Tax',
    //             'icon' => '',
    //             'url' => ['/finance/financesetup/tax/finance-tax/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Service category',
    //             'icon' => '',
    //             'url' => ['/finance/productsetup/financeitemscategory/finance-items-category/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Category Type',
    //             'icon' => '',
    //             'url' => ['/finance/productsetup/financeitemscategorytype/finance-items-category-type/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Product',
    //             'icon' => '',
    //             'url' => ['/finance/productsetup/financeitems/finance-items/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Expense Acvitity',
    //             'icon' => '',
    //             'url' => ['/finance/productsetup/financeeventactivity/finance-event-activity/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Bill',
    //             'icon' => '',
    //             'url' => ['/finance/expense/bills/finance-bills/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Office Expense',
    //             'icon' => '',
    //             'url' => ['/finance/expense/officeexpense/finance-office-requstion-form/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Event Expense',
    //             'icon' => '',
    //             'url' => ['/finance/expense/eventexpense/finance-event-requstion-form/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Lead Expense',
    //             'icon' => '',
    //             'url' => ['/finance/expense/leadexpense/finance-lead-requstion-form/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Bill Payment',
    //             'icon' => '',
    //             'url' => ['/finance/expensepayment/billpayment/finance-bills-payments/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Office Expense Payement',
    //             'icon' => '',
    //             'url' => ['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Event Expense Payement',
    //             'icon' => '',
    //             'url' => ['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Lead Expense Payement',
    //             'icon' => '',
    //             'url' => ['/finance/expensepayment/leadexpensepayment/finance-lead-requisition-payments/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Invoice',
    //             'icon' => '',
    //             'url' => ['/finance/income/invoice/finance-invoice/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Esimate',
    //             'icon' => '',
    //             'url' => ['/finance/income/estimate/finance-estimate/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Invoice Payement',
    //             'icon' => '',
    //             'url' => ['/finance/receipt/invoicepayment/finance-payment/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Customer Purchse Order',
    //             'icon' => '',
    //             'url' => ['/finance/expense/officeexpense/finance-office-requstion-form/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Offset Inhouse',
    //             'icon' => '',
    //             'url' => ['/finance/offset/inhouse/finance-offset-memo/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Offset Supplier',
    //             'icon' => '',
    //             'url' => ['/finance/offset/outhouse/finance-vendor-offset-memo/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Credit Memo Customer',
    //             'icon' => '',
    //             'url' => ['/finance/creditMemo/customercreditMemo/finance-credit-memo/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //             ['label' => 'Credit Memo Supplier',
    //             'icon' => '',
    //             'url' => ['/finance/creditMemo/vendorcreditMemo/finance-vendor-credit-memo/'],
    //             'template'=>'<a href="{url}"><i class="fa fa-circle-o"></i>{label}</a>'
    //       ],
    //     ],
    //   'activateParents'=>true,
    //   'options' => [
    //       'class' => 'sidebar-menu',
    //       'data-widget'=>'tree',
    //   ],
    // ]);
  ?>
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
      <img src="<?=Url::to('@web/adminlte/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image">
    </div>
    <div class="pull-left info">
      <p><?= Yii::$app->user->identity->userFName." ".Yii::$app->user->identity->userLName ?>
      </p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- search form -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
        </button>
      </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <?=
  Nav::widget(
    [
      'encodeLabels' => false,
      'options' => ['class' => 'sidebar-menu','data-widget'=>"tree" ],
      'items' => Yii::$app->Permission->getMenus(),
    ]
  );
  ?>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>
          <li class=""><a href="/sunflower/web/"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-book"></i><span>Master</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?=Url::to(['/company/companydetails/company-details/']) ?>"><i class="fa fa-circle-o"></i> Company Master</a></li>
              <li><a href="<?=Url::to(['/company/companydepartment/company-department/']) ?>"><i class="fa fa-circle-o"></i> Department Master</a></li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-dashboard"></i><span>Company Storage Master</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?=Url::to(['/company/companysite/company-site-details/']) ?>"><i class="fa fa-circle-o"></i> Company Site</a></li>
                  <li ><a href="<?=Url::to(['/company/companysitelocation/company-site-location/']) ?>"><i class="fa fa-circle-o"></i> Company Site Locations</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-barcode"></i><span>Inventory</span>
              <span class="pull-right-container">

                <small class="label pull-right bg-yellow"><?php
                $assetSataus=3;
                $holdAsset=AssetRegistration::find()->where(['>=', 'assetStatus', $assetSataus])
                                                      ->count('asset_registration.id');

                                                      echo $holdAsset;

                ?></small>
                <small class="label pull-right bg-green"><?php
                $assetSatausava=1;
                $availAsset=AssetRegistration::find()->where(['>=', 'assetStatus', $assetSatausava])
                                                      ->count('asset_registration.id');
                                                      echo $availAsset;

                ?></small>
                <small class="label pull-right bg-red"><?php
                $assetSatausunder=4;
                $underilAsset=AssetRegistration::find()->where(['>=', 'assetStatus', $assetSatausunder])
                                                      ->count('asset_registration.id');

                                                      echo $underilAsset;

                ?></small>
              </span>
            </a>
            <ul class="treeview-menu">
              <li ><a href="<?=Url::to(['/inventory/']) ?>"><i class="fa fa-circle-o"></i>Dashboard</a></li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-gears"></i><span>Asset Set Up</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?=Url::to(['/inventory/assetsetup/assetcategory/asset-category/']) ?>"><i class="fa fa-circle-o"></i>Category</a></li>
                  <li ><a href="<?=Url::to(['/inventory/assetsetup/assetcategory/asset-category/']) ?>"><i class="fa fa-circle-o"></i>Category Type</a></li>
                  <li ><a href="<?=Url::to(['/inventory/asset/assetregistration/asset-registration/']) ?>"><i class="fa fa-circle-o"></i>Asset Registration</a></li>
                  <li ><a href="<?=Url::to(['/inventory/asset/assetvaluation/asset-preciation/']) ?>"><i class="fa fa-circle-o"></i>Asset Valuation</a></li>
                  <li ><a href="<?=Url::to(['/inventory/asset/assetlocation/asset-site-location/']) ?>"><i class="fa fa-circle-o"></i>Asset Locations</a></li>

                </ul>
              </li>
              <li><a href="<?=Url::to(['/inventory/assetmovement/delivery/asset-deliveryform/deliverysearch']) ?>"><i class="fa fa-shopping-cart"></i> Delivery Form</a></li>
              <!-- <li><a href="<?=Url::to(['//']) ?>"><i class="fa fa-barcode"></i> Check In</a></li> -->
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-fw fa-group"></i><span>Supplier</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?=Url::to(['/vendor/vendordetails/vendor-company-details/']) ?>"><i class="fa fa-circle-o"></i> Supplier Details</a></li>
            </ul>
          </li>

          <li class="treeview">
            <a href="#">
              <i class="fa fa-fw fa-user"></i><span>Customer</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="<?=Url::to(['/customer']) ?>">
                  <i class="fa fa-fw fa fa-dashboard"></i><span>Customer Dashoard</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
              </li>
              <li><a href="<?=Url::to(['/customer/customerdetails/customer-details/']) ?>"><i class="fa fa-circle-o"></i> Customer Details</a></li>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-phone"></i><span>Follow - Up</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li ><a href="<?=Url::to(['/customer/marketing/customerlead/customer-lead/']) ?>"><i class="fa fa-circle-o"></i> Customer Lead</a></li>
                  <li ><a href="<?=Url::to(['/customer/marketing/customerleadappointment/customer-lead-appointment/']) ?>"><i class="fa fa-circle-o"></i> Appointment</a></li>
                  <li ><a href="<?=Url::to(['/customer/marketing/customerleadcommunicationtracker/customer-lead-communication-tracker/']) ?>"><i class="fa fa-circle-o"></i> Communication Tracker</a></li>
                </ul>
              </li>

            </ul>
          </li>

          <li class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i><span>Event</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="<?=Url::to(['/event']) ?>">
                  <i class="fa fa-fw fa fa-dashboard"></i><span>Event Dashoard</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-gears"></i><span>Event Set Up</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/event/eventsetup/eventcaterogies/event-caterogies/']) ?>"><i class="fa fa-circle-o"></i> Event Category</a></li>
                  <li><a href="<?=Url::to(['/event/eventsetup/eventrole/event-role/']) ?>"><i class="fa fa-circle-o"></i> Roles</a></li>
                  <li><a href="<?=Url::to(['/event/eventsetup/eventitemrole/event-item-role/']) ?>"><i class="fa fa-circle-o"></i> Service Roles</a></li>
                  <li><a href="<?=Url::to(['/event/eventsetup/eventitemcategorytype/event-item-category-type/']) ?>"><i class="fa fa-circle-o"></i> Service Asset</a></li>
                </ul>
              </li>
              <li><a href="<?=Url::to(['/event/eventInfo/event-details/']) ?>"><i class="fa fa-calendar"></i> Event Info</a></li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-book"></i><span>Event Docs</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/event/eventdocs/orderfroms/event-orderfrom/']) ?>"><i class="fa fa-circle-o"></i> Order Form</a></li>

                </ul>
              </li>
              <li><a href="<?=Url::to(['/event/transportation/event-transport/']) ?>"><i class="fa fa-circle-o"></i> Transportation</a></li>
              <li><a href="<?=Url::to(['/event/workplan/event-workplan/']) ?>"><i class="fa fa-circle-o"></i> Workplan</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-table"></i><span>Personnel</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="<?=Url::to(['/personnel']) ?>">
                  <i class="fa fa-fw fa fa-dashboard"></i><span> Staff Dashoard</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
              </li>
              <li><a href="<?=Url::to(['/personnel/personneldetails/personnel-details/']) ?>"><i class="fa fa-circle-o"></i> List</a></li>
            </ul>
          </li>
          <li>

          <li class="treeview">
            <a href="#">
              <i class="fa fa-fw fa-bank"></i><span>Finance</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="<?=Url::to(['/finance']) ?>">
                  <i class="fa fa-fw fa fa-dashboard"></i><span>Finance Dashoard</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-gears"></i><span>Set Up</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <!-- <li><a href="<?=Url::to(['/finance/financesetup/financecategorytype/finance-category-type/']) ?>"><i class="fa fa-circle-o"></i> Finance Category Type</a></li> -->
                  <li><a href="<?=Url::to(['finance-category-type/']) ?>"><i class="fa fa-circle-o"></i> Finance Category Type</a></li>
                  <li><a href="<?=Url::to(['/finance/financesetup/coa/finance-accounts/']) ?>"><i class="fa fa-circle-o"></i> Chart of Accounts</a></li>
                  <li><a href="<?=Url::to(['/finance/financesetup/tax/finance-tax/']) ?>"><i class="fa fa-circle-o"></i> Tax Set Up</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-cart-arrow-down"></i><span>Product Set Up</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/productsetup/financeitemscategory/finance-items-category/']) ?>"><i class="fa fa-circle-o"></i> Service Category</a></li>
                  <li><a href="<?=Url::to(['/finance/productsetup/financeitemscategorytype/finance-items-category-type/']) ?>"><i class="fa fa-circle-o"></i> Category Type</a></li>
                  <li><a href="<?=Url::to(['/finance/productsetup/financeitems/finance-items/']) ?>"><i class="fa fa-circle-o"></i> Product / Service</a></li>
                  <li><a href="<?=Url::to(['/finance/productsetup/financeeventactivity/finance-event-activity/']) ?>"><i class="fa fa-circle-o"></i> Expense Activity</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-money"></i><span>Expense</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/expense/']) ?>"><i class="fa fa-circle-o"></i> Expense Summary</a></li>
                  <li><a href="<?=Url::to(['/finance/expense/bills/finance-bills/']) ?>"><i class="fa fa-circle-o"></i> Bills</a></li>
                  <li><a href="<?=Url::to(['/finance/expense/officeexpense/finance-office-requstion-form/']) ?>"><i class="fa fa-circle-o"></i>Office Expense</a></li>
                  <li><a href="<?=Url::to(['/finance/expense/eventexpense/finance-event-requstion-form/']) ?>"><i class="fa fa-circle-o"></i>Event Expense</a></li>
                  <li><a href="<?=Url::to(['/finance/expense/leadexpense/finance-lead-requstion-form/']) ?>"><i class="fa fa-circle-o"></i>Lead Expense</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-balance-scale"></i><span>Expense Payment</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/expensepayment/bill-payment/finance-bills-payments/']) ?>"><i class="fa fa-circle-o"></i> Bill Payment</a></li>
                  <li><a href="<?=Url::to(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/']) ?>"><i class="fa fa-circle-o"></i> Office Requisition Payment</a></li>
                  <li><a href="<?=Url::to(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/']) ?>"><i class="fa fa-circle-o"></i> Event Requisition Payment</a></li>
                  <li><a href="<?=Url::to(['/finance/expensepayment/leadexpensepayment/finance-lead-requisition-payments/']) ?>"><i class="fa fa-circle-o"></i> Lead Requisition Payment</a></li>

                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-file-text-o"></i><span>Income</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/income/default/view']) ?>"><i class="fa fa-circle-o"></i> Income Summary</a></li>
                  <li><a href="<?=Url::to(['/finance/income/estimate/finance-estimate/']) ?>"><i class="fa fa-circle-o"></i> Quotation</a></li>
                  <li><a href="<?=Url::to(['/finance/income/invoice/finance-invoice/']) ?>"><i class="fa fa-circle-o"></i> Invoice</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-credit-card"></i><span>Receipt</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/receipt/invoicepayment/finance-payment/']) ?>"><i class="fa fa-circle-o"></i> Invoice Payment</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-file-text-o"></i><span>Credit Note</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/creditMemo/customercreditMemo/finance-credit-memo/']) ?>"><i class="fa fa-circle-o"></i> Customer Credit Memo</a></li>
                    <li><a href="<?=Url::to(['/finance/creditMemo/vendorcreditMemo/finance-vendor-credit-memo/']) ?>"><i class="fa fa-circle-o"></i> Supplier Credit Memo</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-file-text-o"></i><span>Off Set</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/offset/inhouse/finance-offset-memo/']) ?>"><i class="fa fa-circle-o"></i> Customer Offset Memo</a></li>
                    <li><a href="<?=Url::to(['/finance/offset/outhouse/finance-vendor-offset-memo/']) ?>"><i class="fa fa-circle-o"></i> Supplier Offset Memo</a></li>
                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-credit-card"></i><span>Transfer</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/transfer/finance-transfer/']) ?>"><i class="fa fa-circle-o"></i> Transfer</a></li>
                </ul>
              </li><li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-credit-card"></i><span>Journal</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/finance/journal/finance-journal-entry/']) ?>"><i class="fa fa-circle-o"></i>Entry</a></li>
                </ul>
              </li>
              <!-- <li class="treeview">
                <a href="#">
                  <i class="fa fa-fw fa-sticky-note-o"></i><span>Purchase Orders</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="<?=Url::to(['/company/companydetails/company-details/']) ?>"><i class="fa fa-circle-o"></i> Customer Purchase Order</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Sunflower Purchase Order</a></li>
                </ul>
              </li> -->
              <li>
                <a href="<?=Url::to(['/finance/report/company']) ?>">
                  <i class="fa fa-fw fa-sticky-note-o"></i><span>Company Financials Report</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
              </li>

            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-fw fa-user"></i><span>Users</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="<?=Url::to(['/user']) ?>">
                  <i class="fa fa-fw fa fa-dashboard"></i><span>User Dashoard</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
              </li>
              <li><a href="<?=Url::to(['/user/userdetails/user-details/']) ?>"><i class="fa fa-circle-o"></i> Users Details</a></li>
              <li><a href="<?=Url::to(['/user/moduleslist/user-modules-list/']) ?>"><i class="fa fa-circle-o"></i> Module List</a></li>
              <li><a href="<?=Url::to(['/user/userroletypes/user-role-types/']) ?>"><i class="fa fa-circle-o"></i> Roles</a></li>
            </ul>
          </li>

        </ul>
