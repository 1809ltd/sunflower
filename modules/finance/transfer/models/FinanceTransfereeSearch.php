<?php

namespace app\modules\finance\transfer\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\transfer\models\FinanceTransferee;

/**
 * FinanceTransfereeSearch represents the model behind the search form of `app\modules\finance\transfer\models\FinanceTransferee`.
 */
class FinanceTransfereeSearch extends FinanceTransferee
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'transferid', 'transfereeFromAccountRef', 'transfereeToAccountRef', 'userId', 'status', 'createdBy'], 'integer'],
            [['transfereeRefernce', 'transfereeTxnDate', 'transfereeCreatedAt', 'transfereeUpdatedAt', 'transfereeDeletedAt'], 'safe'],
            [['transfereeAmount', 'transferCharges'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceTransferee::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'transferid' => $this->transferid,
            'transfereeFromAccountRef' => $this->transfereeFromAccountRef,
            'transfereeToAccountRef' => $this->transfereeToAccountRef,
            'transfereeAmount' => $this->transfereeAmount,
            'transferCharges' => $this->transferCharges,
            'transfereeTxnDate' => $this->transfereeTxnDate,
            'transfereeCreatedAt' => $this->transfereeCreatedAt,
            'transfereeUpdatedAt' => $this->transfereeUpdatedAt,
            'transfereeDeletedAt' => $this->transfereeDeletedAt,
            'userId' => $this->userId,
            'status' => $this->status,
            'createdBy' => $this->createdBy,
        ]);

        $query->andFilterWhere(['like', 'transfereeRefernce', $this->transfereeRefernce]);

        return $dataProvider;
    }
}
