<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_details".
 *
 * @property int $id
 * @property string $customerNumber
 * @property string $customerFullname
 * @property string $customerCompanyName
 * @property string $customerKraPin
 * @property string $customerDisplayName
 * @property string $customerPrimaryPhone
 * @property string $customerPrimaryEmail
 * @property int $customerstatus
 * @property string $customerBillWithParent
 * @property int $customerParentId
 * @property string $customercity
 * @property string $customerSuite
 * @property string $customerCounty
 * @property string $customerCountry
 * @property string $customerPostalCode
 * @property string $customerAddress
 * @property string $customerCreateTime
 * @property string $customerUpdatedAt
 * @property string $customerDeleteAt
 * @property int $userId
 *
 * @property CustomerDetails $customerParent
 * @property CustomerDetails[] $customerDetails
 * @property EventDetails[] $eventDetails
 * @property FinanceCreditMemo[] $financeCreditMemos
 * @property FinanceInvoice[] $financeInvoices
 */
class CustomerDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerNumber', 'customerCompanyName', 'customerDisplayName', 'customerstatus', 'customerBillWithParent', 'userId'], 'required'],
            [['customerstatus', 'customerParentId', 'userId'], 'integer'],
            [['customerCreateTime', 'customerUpdatedAt', 'customerDeleteAt'], 'safe'],
            [['customerNumber', 'customerCompanyName', 'customerKraPin', 'customerDisplayName'], 'string', 'max' => 50],
            [['customerFullname', 'customerPrimaryPhone', 'customerBillWithParent'], 'string', 'max' => 25],
            [['customerPrimaryEmail', 'customercity', 'customerSuite', 'customerCounty', 'customerCountry', 'customerPostalCode', 'customerAddress'], 'string', 'max' => 100],
            [['customerNumber'], 'unique'],
            [['customerKraPin'], 'unique'],
            [['customerParentId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerParentId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customerNumber' => 'Number',
            'customerFullname' => 'Fullname',
            'customerCompanyName' => 'Company Name',
            'customerKraPin' => 'Kra Pin',
            'customerDisplayName' => 'Display Name',
            'customerPrimaryPhone' => 'Primary Phone',
            'customerPrimaryEmail' => 'Primary Email',
            'customerstatus' => 'Status',
            'customerBillWithParent' => 'Bill With Parent',
            'customerParentId' => 'Parent',
            'customercity' => 'City',
            'customerSuite' => 'Suite',
            'customerCounty' => 'County',
            'customerCountry' => 'Country',
            'customerPostalCode' => 'Postal Code',
            'customerAddress' => 'Address',
            'customerCreateTime' => 'Create Time',
            'customerUpdatedAt' => 'Updated At',
            'customerDeleteAt' => 'Delete At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerParent()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerParentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerDetails()
    {
        return $this->hasMany(CustomerDetails::className(), ['customerParentId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDetails()
    {
        return $this->hasMany(EventDetails::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemos()
    {
        return $this->hasMany(FinanceCreditMemo::className(), ['customerId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoices()
    {
        return $this->hasMany(FinanceInvoice::className(), ['customerId' => 'id']);
    }
    /*Generate Customer number */
    public function getCustomerNumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(customerNumber) AS number from customer_details where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number = "CUST-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "CUST-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "CUST-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }

    public function getCustomerListEvent($id) {
      // code...
      return EventDetails::find()
        ->where(['and', "customerId=$id"])
        ->all();
        // ->indexBy('id')->column();

    }
}
