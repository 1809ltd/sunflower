<?php

namespace app\controllers;

use Yii;
use app\models\EventOrderfrom;
use app\models\EventOrderfromSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*Getting the Lines relationship*/
use app\models\EventOrderfromList;
use app\models\EventOrderfromListSearch;


/**
 * EventOrderfromController implements the CRUD actions for EventOrderfrom model.
 */
class EventOrderfromController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }


    /**
     * Lists all EventOrderfrom models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new EventOrderfromSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventOrderfrom model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventOrderfrom model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new EventOrderfrom();

        $modelsEventOrderfromList = [new EventOrderfromList];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsEventOrderfromList = Model::createMultiple(EventOrderfromList::classname());
          Model::loadMultiple($modelsEventOrderfromList, Yii::$app->request->post());

          // ajax validation
          /*
          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsEventOrderfromList),
                    ActiveForm::validate($model)
                );
            }*/

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsEventOrderfromList) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsEventOrderfromList as $modelEventOrderfromList) {

                        $modelEventOrderfromList->orderId = $model->id;
                        $modelEventOrderfromList->userId = $model->userId;
                        $modelEventOrderfromList->approvedBy = $model->approvedBy;
                        $modelEventOrderfromList->status = $model->orderStatus;


                          if (! ($flag = $modelEventOrderfromList->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }


        }

        return $this->render('create', [
            'model' => $model,
              'modelsEventOrderfromList' => (empty($modelsEventOrderfromList)) ? [new EventOrderfromList] : $modelsEventOrderfromList
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Updates an existing EventOrderfrom model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        $modelsEventOrderfromList = $this->getEventOrderfromList($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $oldIDs = ArrayHelper::map($modelsEventOrderfromList, 'id', 'id');
          $modelsEventOrderfromList = Model::createMultiple(EventOrderfromList::classname(), $modelsEventOrderfromList);
          Model::loadMultiple($modelsEventOrderfromList, Yii::$app->request->post());
          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsEventOrderfromList, 'id', 'id')));

          // ajax validation
          if (Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ArrayHelper::merge(
                  ActiveForm::validateMultiple($modelsEventOrderfromList),
                  ActiveForm::validate($model)
              );
          }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsEventOrderfromList) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      if (! empty($deletedIDs)) {
                          EventOrderfromList::deleteAll(['id' => $deletedIDs]);
                      }
                      foreach ($modelsEventOrderfromList as $modelEventOrderfromList) {

                            $modelEventOrderfromList->orderId = $model->id;
                            $modelEventOrderfromList->userId = $model->userId;
                            $modelEventOrderfromList->approvedBy = $model->approvedBy;
                            $modelEventOrderfromList->status = $model->orderStatus;

                          if (! ($flag = $modelEventOrderfromList->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
              }
          }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsEventOrderfromList' => (empty($modelsEventOrderfromList)) ? [new EventOrderfromList] : $modelsEventOrderfromList
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('update', [
        //     'model' => $model,
        // ]);
    }

    public function getEventOrderfromList($id)
    {
      $model = EventOrderfromList::find()->where(['orderId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing EventOrderfrom model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout='addformdatatablelayout';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EventOrderfrom model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventOrderfrom the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventOrderfrom::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
