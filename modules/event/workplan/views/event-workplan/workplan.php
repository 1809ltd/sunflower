<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
// use yii\helpers\Url;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;


/*Get All the event order and details*/
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
use app\modules\event\eventdocs\orderfroms\models\EventOrderfromSearch;


use app\modules\event\workplan\models\EventWorkplan;

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var model app\models\EventOrderfrom */

// use app\models\EventOrderfromList;
// use app\models\EventOrderfromListSearch;

/* @var $this yii\web\View */
/* @var model app\models\EventOrderfrom */


$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();

/*All Payment for this invoice, i.e offset,creditnote,actualpayment*/
// $payments=$model->allPayments($model->invoiceNumber);

$oderdetails = EventOrderfrom::find()
        ->where('id = :id', [':id' => $orderId])
        ->one();

$this->title = $oderdetails->orderNumber;
$this->params['breadcrumbs'][] = ['label' => 'Event  WorkPlan Checklist', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<!-- this row will not appear when printing -->
<div class="row no-print">
    <!-- Pop up Form -->
  <?php
  // Modal::begin([
  //   // 'header'=>'<h3>Add Customer</h3>',
  //   'class'=>'modal-dialog',
  //   'id'=>'modal',
  //   'size'=>'modal-lg',
  // ]);
  //
  // echo "<div class='modal-content' id='modalContent'></div>";
  //
  // Modal::end();
   ?>
</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-sm-8">
         <!-- <h3 class="page-header"> -->
         <h5>
           <img class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h5>
       </div>
       <div class="col-sm-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-sm-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-sm-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;"> Work-Plan Checklist</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">

         <address>
           <strong>Client:- </strong> <?= $oderdetails->event['customer']['customerFullname']?><br/>

           <strong> No of guests:-</strong> <?= $oderdetails->event['eventVistorsNumber']?> Pax<br/>
             <strong>Date of Function:- </strong> <?= $oderdetails->event['eventStartDate']?><br/>
             <strong>Date of Set Up:- </strong> <?= $oderdetails->event['eventSetUpDateTime']?><br/>
             <strong>Colour Scheme:- </strong> <?= $oderdetails->event['eventTheme']?><br/>
             <strong>Event:- </strong> <?= $oderdetails->event['eventName']?> <br/>
             <strong>Location:- </strong> <?= $oderdetails->event['eventLocation']?><br/>
         </address>

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Order No: #<?=$oderdetails->orderNumber;?></b><br>
         <b>Date:- </b> <?= Yii::$app->formatter->asDate($oderdetails->date, 'long');?><br>

         <b>Account:</b> <?= $oderdetails->event['customer']['customerNumber']?>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-sm-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <tr  class="row">

               <th>Service</th>
               <th>Personnel</th>
               <th>Phone</th>
               <th>Role</th>
               <th>Catory</th>
             </tr>

           </thead>
           <tbody>
             <?php
             //Getting all the Event Order Items
             $orderStatus=$oderdetails->orderStatus;
             $id=$oderdetails->id;
             $evenworktOrderfromLists=EventWorkplan::geteventWorkOrderfromList($id,$orderStatus);

             // print_r($eventOrderfromLists);
             // die();

             foreach ($evenworktOrderfromLists as $evenworktOrderfromList) {

               // code...
               // print_r($evenworktOrderfromList);
               // die();
               ?>
               <tr class="row">
                 <td ><?=$evenworktOrderfromList->orderform['item']['itemsName'];?></td>
                 <td><?=$evenworktOrderfromList->personnel['displayName'];?></td>
                 <td><?=$evenworktOrderfromList->personnelPhone;?></td>
                 <td><?=$evenworktOrderfromList->taskAssign0['role']['roleName'];?></td>
                 <td><?=$evenworktOrderfromList->category;?></td>

               </tr>

               <?php

             }

              ?>


           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-sm-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           <?= $oderdetails->orderNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-sm-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          <?= $oderdetails->orderNote; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-sm-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-sm btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->
         <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button> -->

       </div>
     </div>
   </section>
   <!-- /.content -->
