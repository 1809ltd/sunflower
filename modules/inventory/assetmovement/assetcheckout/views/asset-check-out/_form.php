<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
// use kartik\time\TimePicker;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body asset-check-out-form">
    <?= $form->field($model, 'assetId')->textInput() ?>

    <?= $form->field($model, 'locationId')->textInput() ?>

    <?= $form->field($model, 'checkOutDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

    <?= $form->field($model, 'dueDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

    <?= $form->field($model, 'activity')->textInput() ?>

    <?= $form->field($model, 'deliveryId')->textInput() ?>

    <?= $form->field($model, 'activityId')->textInput() ?>

    <?= $form->field($model, 'orderId')->textInput() ?>

    <?= $form->field($model, 'orderlistId')->textInput() ?>

    <?= $form->field($model, 'vehicle')->textInput() ?>

    <?= $form->field($model, 'assignto')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'athurizedBy')->textInput() ?>


</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>