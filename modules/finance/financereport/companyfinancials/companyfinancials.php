<?php

namespace app\modules\finance\financereport\companyfinancials;

/**
 * companyfinancials module definition class
 */
class companyfinancials extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\companyfinancials\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
        $this->modules =[
          /*Compnay finacial report*/
          'p&l' => [
            'class' => 'app\modules\finance\financereport\companyfinancials\pandl\pandl',
          ],
          // All transaction
          'account-transactions' => [
            'class' => 'app\modules\finance\financereport\companyfinancials\alltransaction\alltransaction',
          ],
          'balance-sheet' => [
            'class' => 'app\modules\finance\financereport\companyfinancials\balancesheet\balancesheet',
          ],
          'tax-report' => [
            'class' => 'app\modules\finance\financereport\companyfinancials\taxreport\taxreport',
          ],
          'cash-flow' => [
            'class' => 'app\modules\finance\financereport\companyfinancials\cashflow\cashflow',
          ],
        ];
    }
}
