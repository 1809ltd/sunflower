<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerDetails;

/**
 * CustomerDetailsSearch represents the model behind the search form of `app\models\CustomerDetails`.
 */
class CustomerDetailsSearch extends CustomerDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customerstatus', 'customerParentId', 'userId'], 'integer'],
            [['customerNumber', 'customerFullname', 'customerCompanyName', 'customerKraPin', 'customerDisplayName', 'customerPrimaryPhone', 'customerPrimaryEmail', 'customerBillWithParent', 'customercity', 'customerSuite', 'customerCounty', 'customerCountry', 'customerPostalCode', 'customerAddress', 'customerCreateTime', 'customerUpdatedAt', 'customerDeleteAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customerstatus' => $this->customerstatus,
            'customerParentId' => $this->customerParentId,
            'customerCreateTime' => $this->customerCreateTime,
            'customerUpdatedAt' => $this->customerUpdatedAt,
            'customerDeleteAt' => $this->customerDeleteAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'customerNumber', $this->customerNumber])
            ->andFilterWhere(['like', 'customerFullname', $this->customerFullname])
            ->andFilterWhere(['like', 'customerCompanyName', $this->customerCompanyName])
            ->andFilterWhere(['like', 'customerKraPin', $this->customerKraPin])
            ->andFilterWhere(['like', 'customerDisplayName', $this->customerDisplayName])
            ->andFilterWhere(['like', 'customerPrimaryPhone', $this->customerPrimaryPhone])
            ->andFilterWhere(['like', 'customerPrimaryEmail', $this->customerPrimaryEmail])
            ->andFilterWhere(['like', 'customerBillWithParent', $this->customerBillWithParent])
            ->andFilterWhere(['like', 'customercity', $this->customercity])
            ->andFilterWhere(['like', 'customerSuite', $this->customerSuite])
            ->andFilterWhere(['like', 'customerCounty', $this->customerCounty])
            ->andFilterWhere(['like', 'customerCountry', $this->customerCountry])
            ->andFilterWhere(['like', 'customerPostalCode', $this->customerPostalCode])
            ->andFilterWhere(['like', 'customerAddress', $this->customerAddress]);

        return $dataProvider;
    }
}
