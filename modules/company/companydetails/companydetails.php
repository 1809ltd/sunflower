<?php

namespace app\modules\company\companydetails;

/**
 * companydetails module definition class
 */
class companydetails extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\company\companydetails\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
