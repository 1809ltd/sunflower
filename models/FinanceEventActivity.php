<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_event_activity".
 *
 * @property int $id
 * @property string $activityName
 * @property string $activityDescription
 * @property int $account
 * @property int $activityStatus
 * @property string $updatedAt
 * @property string $deletedAt
 * @property string $createdAt
 * @property int $userId
 *
 * @property FinanceAccounts $account0
 * @property UserDetails $user
 * @property FinanceEventRequstionFormList[] $financeEventRequstionFormLists
 */
class FinanceEventActivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_event_activity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activityName', 'account', 'activityStatus', 'userId'], 'required'],
            [['activityDescription'], 'string'],
            [['account', 'activityStatus', 'userId'], 'integer'],
            [['updatedAt', 'deletedAt', 'createdAt'], 'safe'],
            [['activityName'], 'string', 'max' => 100],
            [['account'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['account' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activityName' => 'Activity Name',
            'activityDescription' => 'Activity Description',
            'account' => 'Account',
            'activityStatus' => 'Activity Status',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'createdAt' => 'Created At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'account']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionFormLists()
    {
        return $this->hasMany(FinanceEventRequstionFormList::className(), ['activityId' => 'id']);
    }
}
