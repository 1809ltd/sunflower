<?php

namespace app\modules\finance\expensepayment\eventexpensepayment;

/**
 * eventexpensepayment module definition class
 */
class eventexpensepayment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expensepayment\eventexpensepayment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
