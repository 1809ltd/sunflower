<?php

namespace app\modules\finance\creditMemo\customercreditMemo\models;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;


use Yii;

/**
 * This is the model class for table "finance_credit_memo".
 *
 * @property int $id
 * @property int $creditMemoAccount
 * @property string $refernumber
 * @property string $creditMemoTxnDate
 * @property string $creditMemoNote
 * @property int $customerId
 * @property double $creditAmount
 * @property double $totalTax
 * @property int $creditMemoStatus
 * @property string $creditMemoCreatedAt
 * @property string $creditMemoUpdatedAt
 * @property string $creditMemoDeletedAt
 * @property int $userId
 *
 * @property CustomerDetails $customer
 * @property FinanceAccounts $creditMemoAccount0
 * @property UserDetails $user
 * @property FinanceCreditMemoLines[] $financeCreditMemoLines
 */
class FinanceCreditMemo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_credit_memo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['creditMemoAccount', 'refernumber', 'creditMemoTxnDate', 'customerId', 'creditAmount', 'totalTax', 'creditMemoStatus', 'userId'], 'required'],
            [['creditMemoAccount', 'customerId', 'creditMemoStatus', 'userId'], 'integer'],
            [['creditMemoTxnDate', 'creditMemoCreatedAt', 'creditMemoUpdatedAt', 'creditMemoDeletedAt'], 'safe'],
            [['creditMemoNote'], 'string'],
            [['creditAmount', 'totalTax'], 'number'],
            [['refernumber'], 'string', 'max' => 50],
            [['refernumber'], 'unique'],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['creditMemoAccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['creditMemoAccount' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creditMemoAccount' => 'Account',
            'refernumber' => 'Refernumber',
            'creditMemoTxnDate' => 'Date',
            'creditMemoNote' => 'Note',
            'customerId' => 'Customer',
            'creditAmount' => 'Credit Amount',
            'totalTax' => 'Total Tax',
            'creditMemoStatus' => 'Status',
            'creditMemoCreatedAt' => 'Created At',
            'creditMemoUpdatedAt' => 'Updated At',
            'creditMemoDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreditMemoAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'creditMemoAccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemoLines()
    {
        return $this->hasMany(FinanceCreditMemoLines::className(), ['creditMemoId' => 'id']);
    }

    public function getFinanceOffsetMemoLines()
    {
        return $this->hasMany(FinanceOffsetMemoLines::className(), ['offsetMemoId' => 'id']);
    }
    public function getCrdtnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(refernumber) AS number from finance_credit_memo where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "Crdt-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "Crdt-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "Crdt-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
