<?php

namespace app\modules\inventory\asset\assetlocation\controllers;

use Yii;
use app\modules\inventory\asset\assetlocation\models\AssetSiteLocation;
use app\modules\inventory\asset\assetlocation\models\AssetSiteLocationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AssetSiteLocationController implements the CRUD actions for AssetSiteLocation model.
 */
class AssetSiteLocationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        $this->updateAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updateAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }


  /**
   * Lists all AssetSiteLocation models.
   * @return mixed
   */
  public function actionIndex()
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // $this->layout='addformdatatablelayout';
      $searchModel = new AssetSiteLocationSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

  /**
   * Displays a single AssetSiteLocation model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // $this->layout='addformdatatablelayout';
      return $this->render('view', [
          'model' => $this->findModel($id),
      ]);
  }

  /**
   * Creates a new AssetSiteLocation model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // $this->layout='addformdatatablelayout';
      $model = new AssetSiteLocation();

      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;

      //checking if its a post
      if ($model->load(Yii::$app->request->post())) {
        // code...

        //Geting the time created Timestamp
        $model->createdAt= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

        //Getting user Log in details
        $model->userId= "1";
        $model->save();

        //after Saving the customer Details

        return $this->redirect(['index', 'id' => $model->id]);

      } else {
        // code...

        //load the create form
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
      }

  }

  /**
   * Updates an existing AssetSiteLocation model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id)
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // $this->layout='addformdatatablelayout';
      $model = $this->findModel($id);

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
          // return $this->redirect(['view', 'id' => $model->id]);
          return $this->redirect(['index', 'id' => $model->id]);
      }

      return $this->renderAjax('update', [
          'model' => $model,
      ]);
  }

  /**
   * Deletes an existing AssetSiteLocation model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDelete($id)
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // $this->layout='addformdatatablelayout';
      $this->findModel($id)->delete();

      return $this->redirect(['index']);
  }

  /**
   * Finds the AssetSiteLocation model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return AssetSiteLocation the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // $this->layout='addformdatatablelayout';
      if (($model = AssetSiteLocation::findOne($id)) !== null) {
          return $model;
      }

      throw new NotFoundHttpException('The requested page does not exist.');
  }
}
