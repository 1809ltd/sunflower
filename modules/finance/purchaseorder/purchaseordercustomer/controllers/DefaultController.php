<?php

namespace app\modules\finance\purchaseorder\purchaseordercustomer\controllers;

use yii\web\Controller;

/**
 * Default controller for the `purchaseordercustomer` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
