<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOffsetMemoLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-offset-memo-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'offsetMemoId')->textInput() ?>

    <?= $form->field($model, 'invoiceNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'offsetMemoLineDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'offsetMemoLineAmount')->textInput() ?>

    <?= $form->field($model, 'offsetMemoLineStatus')->textInput() ?>

    <?= $form->field($model, 'offsetMemoLineCreatedAt')->textInput() ?>

    <?= $form->field($model, 'offsetMemoLineUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'creidtDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
