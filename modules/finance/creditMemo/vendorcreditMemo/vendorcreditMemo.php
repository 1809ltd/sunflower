<?php

namespace app\modules\finance\creditMemo\vendorcreditMemo;

/**
 * vendorcreditMemo module definition class
 */
class vendorcreditMemo extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\creditMemo\vendorcreditMemo\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Vendor Credit items Note */
          'vendorcreditMemo' => [
              'class' => 'app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemo',
          ],
        ];

        // custom initialization code goes here
    }
}
