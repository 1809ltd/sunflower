<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'categoryId') ?>

    <?= $form->field($model, 'companyId') ?>

    <?= $form->field($model, 'categoryName') ?>

    <?= $form->field($model, 'categoryCode') ?>

    <?= $form->field($model, 'categoryTimestamp') ?>

    <?php // echo $form->field($model, 'categoryUpdatedAt') ?>

    <?php // echo $form->field($model, 'categoryDeleteAt') ?>

    <?php // echo $form->field($model, 'categoryStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
