<?php

namespace app\modules\finance\expense\bills\billslines\models;
use app\modules\finance\expense\bills\models\FinanceBills;
use app\modules\user\userdetails\models\UserDetails;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;

use Yii;

/**
 * This is the model class for table "finance_bills_lines".
 *
 * @property int $id
 * @property int $biilsId
 * @property string $biilsLinesVendorsRef
 * @property string $biilsLinesDescription
 * @property double $biilsLinespacks
 * @property double $biilsLinesQty
 * @property double $billlinePriceperunit
 * @property double $biilsLinesAmount
 * @property double $billLinesTaxCodeRef
 * @property double $biilsLinesTaxAmount
 * @property int $eventActivityId
 * @property int $billsLinesBillableStatus
 * @property string $billsLinesCreatedAt
 * @property string $billsLinesDeleteAt
 * @property string $billsLinesUpdatedAt
 * @property int $userId
 *
 * @property FinanceBills $biils
 * @property UserDetails $user
 * @property FinanceEventActivity $eventActivity
 */
class FinanceBillsLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_bills_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['biilsLinespacks', 'biilsLinesQty', 'billlinePriceperunit', 'biilsLinesAmount', 'billLinesTaxCodeRef', 'biilsLinesTaxAmount', 'eventActivityId'], 'required'],
            [['biilsId', 'eventActivityId', 'billsLinesBillableStatus', 'userId'], 'integer'],
            [['biilsLinesDescription'], 'string'],
            [['biilsLinespacks', 'biilsLinesQty', 'billlinePriceperunit', 'biilsLinesAmount', 'billLinesTaxCodeRef', 'biilsLinesTaxAmount'], 'number'],
            [['biilsId', 'billsLinesCreatedAt', 'billsLinesDeleteAt', 'billsLinesUpdatedAt', 'billsLinesBillableStatus', 'userId'], 'safe'],
            [['biilsLinesVendorsRef'], 'string', 'max' => 50],
            [['biilsId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBills::className(), 'targetAttribute' => ['biilsId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['eventActivityId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEventActivity::className(), 'targetAttribute' => ['eventActivityId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'biilsId' => 'Biils',
            'biilsLinesVendorsRef' => 'Service',
            'biilsLinesDescription' => 'Description',
            'biilsLinespacks' => 'Packs/Days',
            'biilsLinesQty' => 'Qty',
            'billlinePriceperunit' => 'Unit Price',
            'biilsLinesAmount' => 'Amount',
            'billLinesTaxCodeRef' => 'Tax Code',
            'eventActivityId' => 'Expense Activity',
            'biilsLinesTaxAmount' => 'Tax Amount',
            'billsLinesBillableStatus' => 'Status',
            'billsLinesCreatedAt' => 'Bills Lines Created At',
            'billsLinesDeleteAt' => 'Bills Lines Delete At',
            'billsLinesUpdatedAt' => 'Bills Lines Updated At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBiils()
    {
        return $this->hasOne(FinanceBills::className(), ['id' => 'biilsId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventActivity()
    {
        return $this->hasOne(FinanceEventActivity::className(), ['id' => 'eventActivityId']);
    }
}
