<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceEstimateLines;

/**
 * FinanceEstimateLinesSearch represents the model behind the search form of `app\models\FinanceEstimateLines`.
 */
class FinanceEstimateLinesSearch extends FinanceEstimateLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'estimateId', 'itemRefId', 'estimateLinesStatus', 'userId'], 'integer'],
            [['itemRefName', 'estimateLinesDescription', 'estimateLinesCreatedAt', 'estimateLinesUpdatedAt', 'estimateLinesDeletedAt'], 'safe'],
            [['estimateLinespack', 'estimateLinesQty', 'estimateLinesUnitPrice', 'estimateLinesAmount', 'estimateLinesTaxCodeRef', 'estimateLinesTaxamount', 'estimateLinesDiscount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceEstimateLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'estimateId' => $this->estimateId,
            'itemRefId' => $this->itemRefId,
            'estimateLinespack' => $this->estimateLinespack,
            'estimateLinesQty' => $this->estimateLinesQty,
            'estimateLinesUnitPrice' => $this->estimateLinesUnitPrice,
            'estimateLinesAmount' => $this->estimateLinesAmount,
            'estimateLinesTaxCodeRef' => $this->estimateLinesTaxCodeRef,
            'estimateLinesTaxamount' => $this->estimateLinesTaxamount,
            'estimateLinesDiscount' => $this->estimateLinesDiscount,
            'estimateLinesCreatedAt' => $this->estimateLinesCreatedAt,
            'estimateLinesUpdatedAt' => $this->estimateLinesUpdatedAt,
            'estimateLinesDeletedAt' => $this->estimateLinesDeletedAt,
            'estimateLinesStatus' => $this->estimateLinesStatus,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'itemRefName', $this->itemRefName])
            ->andFilterWhere(['like', 'estimateLinesDescription', $this->estimateLinesDescription]);

        return $dataProvider;
    }
}
