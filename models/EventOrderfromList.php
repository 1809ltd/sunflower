<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_orderfrom_list".
 *
 * @property int $id
 * @property int $orderId
 * @property int $itemid
 * @property string $details
 * @property int $outsourced
 * @property int $vendorId
 * @property double $quantity
 * @property int $status
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $approvedBy
 *
 * @property EventOrderfrom $order
 * @property FinanceItems $item
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 * @property UserDetails $approvedBy0
 */
class EventOrderfromList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_orderfrom_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemid', 'details', 'outsourced', 'quantity'], 'required'],
            [['orderId', 'itemid', 'outsourced', 'vendorId', 'status', 'userId', 'approvedBy'], 'integer'],
            [['details'], 'string'],
            [['quantity'], 'number'],
            [['orderId', 'createdAt', 'updatedAt', 'deletedAt', 'status', 'userId', 'approvedBy'], 'safe'],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => EventOrderfrom::className(), 'targetAttribute' => ['orderId' => 'id']],
            [['itemid'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemid' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orderId' => 'Order Number',
            'itemid' => 'Service',
            'details' => 'Details',
            'outsourced' => 'Outsourced',
            'vendorId' => 'Vendor',
            'quantity' => 'Quantity',
            'status' => 'Status',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'approvedBy' => 'Approved By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(EventOrderfrom::className(), ['id' => 'orderId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /*Get the List of all the items in an order*/
    public static function getOderformList($orderId)
    {
      // code...
      $out = [];

      $data = EventOrderfromList::find()
                ->where(['orderId' => $orderId])
                ->joinWith('item')
                ->andWhere(['=', 'status', '1'])
                ->asArray()
                ->all();

        foreach ($data as $dat) {

            $out[] = ['id' => $dat['id'], 'name' => $dat['item']['itemsName']];
        }

        return $output = [
            'output' => $out,
            'selected' => ''
        ];

    }
    /*Get the Job Desription*/
    public static function getJob($orderId, $orderformId)
    {
      // code...
      $out = [];
      $connection = Yii::$app->getDb();
      $command = $connection->createCommand("SELECT event_orderfrom_list.id As id, event_orderfrom_list.itemid,event_item_role.roleId AS roleId,
                            event_role.roleName AS roleName FROM event_orderfrom_list
                            INNER JOIN event_item_role ON event_item_role.itemId = event_orderfrom_list.itemid
                            INNER JOIN event_role ON event_item_role.roleId = event_role.id
                            WHERE
                            event_orderfrom_list.id = :orderformId", [':orderformId' => $orderformId]);

      $data = $command->queryAll();

        $selected = '';



        foreach ($data as $dat => $datas) {

                    $out[] = ['id' => $datas['roleId'], 'name' => $datas['roleName']];

            if($dat == 0){
                    $aux = $datas['id'];
                }

            ($datas['id'] == $orderId) ? $selected = $orderId : $selected = $aux;

        }
        return $output = [
            'output' => $out,
            'selected' => $selected
        ];
    }
}
