<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
// use yii\helpers\Url;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;


/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\journal\models\FinanceJournalEntry */

/*GEtting the Lines relationship*/
use app\modules\finance\journal\models\FinanceJournalEntryLines;
use app\modules\finance\journal\models\FinanceJournalEntryLinesSearch;

$this->title = $model->journalEntryNo;
$this->params['breadcrumbs'][] = ['label' => 'Finance Journal Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="finance-journal-entry-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>

<?php

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();

?>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <p>
      <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
  </p>
  <!-- Pop up Form -->
  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h6>
           <img style="height:85%;width:85%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h6>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Jounral Entry</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">

         <address>
           <strong>Date: </strong></b> <?= Yii::$app->formatter->asDate($model->journalEntrydate, 'long');?><br><br/>

                  </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

     </address>

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Jounral No: #<?=$model->journalEntryNo;?></b><br>
         <b>Created Date: </b> <?= Yii::$app->formatter->asDate($model->createdate, 'long');?><br>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>

             <th>Account</th>
             <th>Description</th>
             <th>Dr Amount</th>
             <th>Cr Amount</th>
           </thead>
           <tbody>
             <?php
             $journalItems=$model->getJournalItems($model->id,$model->status);

             // print_r($journalItems);
             //
             // die();

             foreach ($journalItems as $journalItem) {
               // code...

               ?>
               <tr>
                 <td><?= $journalItem->account['fullyQualifiedName'];?>:</td>
                 <td><?= $journalItem->description;?></td>
                 <td>Ksh <?= number_format($journalItem->drAmount,2) ;?></td>
                 <td>Ksh <?= number_format($journalItem->crAmount,2) ;?></td>
               </tr>
               <?php
             }
             ?>
           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">

          </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">

          </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <div class="col-md-8">
         </div>

         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           *Prepared By [<?= $model->preparedby0['userFName'].'  '.$model->preparedby0['userLName']?>, <br/>
           Phone Number:<?= $model->preparedby0['userPhone']?><br/>
           Email:  <?= $model->preparedby0['userEmail']?>]<br/>

           *Approved By <?= $model->approvedby0['userFName']?>]<br/>

         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">


         <div class="table-responsive">

           <table class="table">
             <tr>
               <th><p class="lead">Total: </strong></p></th>
               <td><p class="lead"> <strong>Ksh <?= number_format($model->total,2) ;?></strong></td>
             </tr>

           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->

         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
