<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerLeadCommunicationTracker */

$this->title = 'Create Customer Lead Communication Tracker';
$this->params['breadcrumbs'][] = ['label' => 'Customer Lead Communication Trackers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-lead-communication-tracker-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
