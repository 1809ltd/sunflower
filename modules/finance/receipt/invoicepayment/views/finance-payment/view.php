<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;


/* @var $this yii\web\View */
/* @var $model app\models\FinancePayment */

$this->title = $model->recepitNumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();
?>
<div class="finance-payment-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    // echo  DetailView::widget([
    //     'model' => $model,
    //     'attributes' => [
    //       'id',
    //       'invoicePaymentPayType',
    //       'invoicePaymentaccount',
    //       'recepitNumber',
    //       'invoicePaymentTotalAmt',
    //       'unallocatedAmount',
    //       'date',
    //       'invoicePaymentinvoiceRef',
    //       'customerId',
    //       'invoicePaymentPrivateNote:ntext',
    //       'invoicePaymentCreatedAt',
    //       'invoicePaymentUpdatedAt',
    //       'invoicePaymentDeletedAt',
    //       'userId',
    //       'createdBy',
    //       'approvedBy',
    //       'paymentstatus',
    //     ],
    // ]) ?>

</div>
<!-- Main content -->
<section class="invoice">
 <!-- title row -->
 <div class="row">
   <div class="col-xs-8">
     <!-- <h3 class="page-header"> -->
     <h6>
       <img style="height:85%;width:85%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
     </h6>
   </div>
   <div class="col-xs-4">
     <!-- <h3 class="page-header"> -->
       <address>
         <strong><?= $companydetails->companyName?>,</strong><br/>
          <?= $companydetails->companyAddress?><br/>
        <?= $companydetails->companyPostal?><br/>
         Phone: +254 (0) 722 790632<br/>
         Email: info@sunflowertents.com
       </address>
     <!-- </h3> -->
   </div>
   <!-- /.col -->
 </div>

 <div class="row">
   <!-- <h5 class="page-header"></h5> -->
   <div class="col-xs-12 table-responsive">
     <table class="table table-striped">
       <thead>
         <th class="page-header" style="text-align: center;width: 100%;">Recepit Number: #  <?=$model->recepitNumber;?></th>
       </thead>
       <tbody>
       </tbody>
     </table>
   </div>
 </div>


 <!-- info row -->
 <div class="row invoice-info">
   <div class="col-sm-4 invoice-col">

     <address>

       <strong>Client: </strong> <?= $model->customer['customerCompanyName']?><br/>
       <strong>Kra Pin: </strong><?= $model->customer['customerKraPin']?><br/>
       <strong>Phone: </strong><?= $model->customer['customerPrimaryPhone']?><br/>
       <strong>Email: </strong><?= $model->customer['customerPrimaryEmail']?><br/>


     Attn: <?= $model->customer['customerFullname']?>
   </div>
   <!-- /.col -->
   <div class="col-sm-4 invoice-col">
     Contact:
     <address>

     </address>

 </address>

   </div>
   <!-- /.col -->
   <div class="col-sm-4 invoice-col">

       <b>Tranasaction /Cheque No: #</b><?= $model->invoicePaymentinvoiceRef ?><br>
       <br>
       <b>Payment Account:</b><?= $model->invoicePaymentaccount0["fullyQualifiedName"];?> <br>
       <b>Payment Date:</b> <?= Yii::$app->formatter->asDate($model->date, 'long');?><br>
       <b>Account:</b> <?= $model->customer['customerNumber']?>

   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->

 <!-- Table row -->
 <div class="row">
   <div class="col-xs-12 table-responsive">
     <table class="table table-striped">
       <thead>
         <th>Invoice Number:</th>
         <th>Detials</th>
         <th>Sub Amount</th>
       </thead>
       <tbody>
         <?php

         $paymentItems=$model->getinvoicepaymentItems($model->id,$model->paymentstatus);

         foreach ($paymentItems as $paymentItem) {
           // code...

           ?>
           <tr>
               <td><?=$paymentItem["invoice"]["invoiceNumber"];?></td>
               <td>This was for this event <?= $paymentItem["invoice"]["invoiceProject"]['eventName'];?> </br>
                 Held on <?= Yii::$app->formatter->asDate($paymentItem["invoice"]["invoiceProject"]['eventStartDate'], 'long');?>
                 at <?= $paymentItem["invoice"]["invoiceProject"]['eventLocation'];?></td>
               <td>Ksh <?= number_format($paymentItem->amount,2) ;?></td>

           </tr>

           <?php

         }

         // die();

          ?>

       </tbody>
     </table>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->

 <br>
 <div class="row">
   <!-- accepted payments column -->
   <div class="col-xs-6">
     <p class="" style="margin-top: 10px;">
       <?= $model->invoicePaymentPrivateNote; ?>
     </p>
   </div>
   <!-- /.col -->
   <div class="col-xs-6">
     <p class="" style="margin-top: 10px;">
      <?php
      // $model->invoiceFooter; ?>
     </p>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->

 <div class="row">
   <!-- accepted payments column -->
   <div class="col-xs-6">
     <p class="lead">Payment Methods:  <?= $model->invoicePaymentPayType?></p>
     <div class="col-md-8">
     </div>

     <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">

       * If you have any questions concerning this Payment Voucher<br/>
        contact [<?= $model->createdBy0['userFName'].'  '.$model->createdBy0['userLName']?>, <br/>
       Phone Number:    <?= $model->createdBy0['userPhone']?><br/>
       Email:  <?= $model->createdBy0['userEmail']?>]
     </p>
   </div>
   <!-- /.col -->
   <div class="col-xs-6">

     <p class="lead">Unallocated Amount <strong>Ksh <?= number_format($model->unallocatedAmount,2) ;?></strong></p>

     <div class="table-responsive">
       <table class="table">
         <tr>
           <th>Total:</th>
           <td>Ksh <?= number_format($model->invoicePaymentTotalAmt,2) ;?></td>
         </tr>

       </table>
     </div>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->






 <!-- this row will not appear when printing -->
 <div class="row no-print">
   <div class="col-xs-12">
     <a href="javascript:void(0)" onclick="window.print()"
        class="btn btn-xs btn-success m-b-10"><i
             class="fa fa-print m-r-5"></i> Print</a>
    <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
     </button> -->
     <?php
     // Html::button('Submit Payment', ['value'=>Url::to((['makepayments', 'id' => $model['id']])),'class' => 'btn btn-success pull-right','id'=>'modalButton']) ?>
     <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
       <i class="fa fa-download"></i> Generate PDF
     </button> -->
   </div>
 </div>
</section>
   <!-- /.content -->
