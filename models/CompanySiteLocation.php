<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_site_location".
 *
 * @property int $locationId
 * @property int $siteId
 * @property string $locationName
 * @property string $locationtimestamp
 * @property string $locationUpdatedAt
 * @property string $locationDeleteAt
 * @property int $userId
 *
 * @property AssetSiteLocation[] $assetSiteLocations
 * @property CompanySiteDetails $site
 * @property UserDetails $user
 */
class CompanySiteLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_site_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['siteId', 'locationName', 'userId'], 'required'],
            [['siteId', 'userId'], 'integer'],
            [['locationtimestamp', 'locationUpdatedAt', 'locationDeleteAt'], 'safe'],
            [['locationName'], 'string', 'max' => 255],
            [['locationName'], 'unique'],
            [['siteId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanySiteDetails::className(), 'targetAttribute' => ['siteId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Location Name',
            'siteId' => 'Site Name',
            'locationName' => 'Location Name',
            'locationtimestamp' => 'Locationtimestamp',
            'locationUpdatedAt' => 'Location Updated At',
            'locationDeleteAt' => 'Location Delete At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetSiteLocations()
    {
        return $this->hasMany(AssetSiteLocation::className(), ['loactionId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(CompanySiteDetails::className(), ['id' => 'siteId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['userId' => 'userId']);
    }
}
