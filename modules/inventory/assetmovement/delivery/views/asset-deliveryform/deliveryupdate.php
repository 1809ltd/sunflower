<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */

$this->title = 'Update Delivery Asset Check Out: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Check Outs', 'url' => ['deliverysearch']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-check-out-update">

    <?= $this->render('deliveryfromadd', [
        'model' => $model,
    ]) ?>

</div>
