SELECT COALESCE(Sum( finance_invoice_lines.invoiceLinesAmount ), 0)
-
(SELECT COALESCE(sum( `finance_payment`.`paymentTotalAmt` ), 0 ) FROM `finance_payment` WHERE ( `finance_payment`.`paymentLinkedTxn` = `finance_invoice`.`invoiceNumber` ) and DATE(  `finance_payment`.paymentTxnDate ) <= "2019-03-25" )
-
(SELECT COALESCE(sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 ) FROM `finance_credit_memo_lines` WHERE ( `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_credit_memo_lines.creditMemoLineCreatedAt ) <= "2019-03-25" )
-
( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 ) FROM `finance_offset_memo_lines` WHERE ( `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_offset_memo_lines.offsetMemoLineCreatedAt ) <= "2019-03-25" )

FROM
finance_invoice_lines
INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
WHERE
DATE( finance_invoice.invoiceTxnDate ) <= "2019-03-25"

SELECT COALESCE(Sum( finance_invoice_lines.invoiceLinesAmount ), 0)
-
(SELECT COALESCE(sum( `finance_payment`.`paymentTotalAmt` ), 0 ) FROM `finance_payment` WHERE ( `finance_payment`.`paymentLinkedTxn` = `finance_invoice`.`invoiceNumber` ) and DATE(`finance_payment`.`paymentTxnDate` ) <= :start_date)
-
(SELECT COALESCE(sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 ) FROM `finance_credit_memo_lines` WHERE ( `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_credit_memo_lines.creditMemoLineCreatedAt ) <= :start_date)
-
( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 ) FROM `finance_offset_memo_lines` WHERE ( `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_offset_memo_lines.offsetMemoLineCreatedAt ) <= :start_date)
as receivables FROM
finance_invoice_lines
INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
WHERE
DATE( finance_invoice.invoiceTxnDate ) <= :start_date









$connection = Yii::$app->getDb();
$command = $connection->createCommand("SELECT COALESCE(Sum( finance_invoice_lines.invoiceLinesAmount ), 0)
-
(SELECT COALESCE(sum( `finance_payment`.`paymentTotalAmt` ), 0 ) FROM `finance_payment` WHERE ( `finance_payment`.`paymentLinkedTxn` = `finance_invoice`.`invoiceNumber` ) and DATE(`finance_payment`.`paymentTxnDate` ) <= :start_date)
-
(SELECT COALESCE(sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 ) FROM `finance_credit_memo_lines` WHERE ( `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_credit_memo_lines.creditMemoLineCreatedAt ) <= :start_date)
-
( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 ) FROM `finance_offset_memo_lines` WHERE ( `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_offset_memo_lines.offsetMemoLineCreatedAt ) <= :start_date)
as receivables FROM
finance_invoice_lines
INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
WHERE
DATE( finance_invoice.invoiceTxnDate ) <= :start_date", [':start_date' => '2019-03-25']);

$result = $command->queryAll();
