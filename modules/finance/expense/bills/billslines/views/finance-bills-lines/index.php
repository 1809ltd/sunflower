<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceBillsLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Bills Lines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-bills-lines-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Bills Lines', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'biilsId',
            'biilsLinesVendorsRef',
            'biilsLinesDescription:ntext',
            'biilsLinespacks',
            //'biilsLinesQty',
            //'billlinePriceperunit',
            //'biilsLinesAmount',
            //'billLinesTaxCodeRef',
            //'biilsLinesTaxAmount',
            //'eventActivityId',
            //'billsLinesBillableStatus',
            //'billsLinesCreatedAt',
            //'billsLinesDeleteAt',
            //'billsLinesUpdatedAt',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
