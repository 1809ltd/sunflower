<?php

namespace app\modules\finance\creditMemo\customercreditMemo;

/**
 * customercreditMemo module definition class
 */
class customercreditMemo extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\creditMemo\customercreditMemo\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Customer Credit items Note */
          'customercreditMemoitems' => [
              'class' => 'app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\customercreditMemoitems',
          ],
        ];


        // custom initialization code goes here
    }
}
