<?php

namespace app\modules\finance\receipt\invoicepayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines;

/**
 * FinancePaymentLinesSearch represents the model behind the search form of `app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines`.
 */
class FinancePaymentLinesSearch extends FinancePaymentLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'invoicepaymentId', 'invoiceId', 'customerId', 'status', 'userId'], 'integer'],
            [['amount'], 'number'],
            [['deletedAt', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinancePaymentLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'invoicepaymentId' => $this->invoicepaymentId,
            'invoiceId' => $this->invoiceId,
            'customerId' => $this->customerId,
            'amount' => $this->amount,
            'status' => $this->status,
            'deletedAt' => $this->deletedAt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'userId' => $this->userId,
        ]);

        return $dataProvider;
    }
}
