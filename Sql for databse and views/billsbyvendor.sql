/*Get all bills*/
-- ----------------------------
-- View structure for `billsbyvendor`
-- ----------------------------
DROP VIEW IF EXISTS `billsbyvendor`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `billsbyvendor` AS SELECT
	finance_bills.vendorId,
	vendor_company_details.vendorCompanyName,
	vendor_company_details.vendorKraPin,
	finance_bills_lines.biilsLinesAmount,
	finance_bills_lines.biilsLinesTaxAmount,
	finance_bills_lines.billLinesTaxCodeRef,
	finance_event_activity.activityName,
	finance_accounts.fullyQualifiedName,
	finance_bills_lines.biilsId,
	finance_bills_lines.id AS billslineId,
	finance_bills_lines.biilsLinesDescription,
	finance_bills_lines.biilsLinesQty,
	finance_bills_lines.billlinePriceperunit,
	finance_bills_lines.billsLinesCreatedAt,
	finance_bills.billDueDate AS txnDate,
	finance_bills_lines.billsLinesBillableStatus,
	event_details.eventNumber,
	event_caterogies.eventCatName,
	event_caterogies.id AS catId
FROM
	finance_bills_lines
	inner JOIN finance_bills ON finance_bills_lines.biilsId = finance_bills.id
	inner JOIN vendor_company_details ON finance_bills.vendorId = vendor_company_details.id
	inner JOIN finance_event_activity ON finance_bills_lines.eventActivityId = finance_event_activity.id
	inner JOIN finance_accounts ON finance_event_activity.account = finance_accounts.id
	LEFT JOIN event_details ON finance_bills.projectId = event_details.id
	LEFT JOIN event_caterogies ON event_details.eventCategory = event_caterogies.id
WHERE
		finance_bills.billStatus > 0
		AND finance_bills_lines.billsLinesBillableStatus > 0
