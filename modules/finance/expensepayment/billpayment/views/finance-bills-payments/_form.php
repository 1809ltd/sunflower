<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;

/*Payment Lines Items*/
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments */
/* @var $form yii\widgets\ActiveForm */

?>
<div class="finance-bills-payments-form">
  <?php // echo $form = ActiveForm::begin(); ?>
  <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,]); ?>
  
  <?php // $form = ActiveForm::begin(['action' => ['create'],'options' => ['method' => 'post']]);?>
  <?php echo $form->field($model, 'vendorId')->hiddenInput(['value'=>$_SESSION['vendorInvoiceId']])->label(false);?>

  <div class="row">
    <div class="col-sm-6">
      <div class="form-group">
        <?= $form->field($model, 'billPaymentaccount')->widget(Select2::classname(), [
          'data' => ArrayHelper::map(FinanceAccounts::find()->where(['like', 'accountsPays', 'yes'])->all(),'id','fullyQualifiedName'),
          'language' => 'en',
          'options' => ['placeholder' => 'Select a Payment Account ...'],
          'pluginOptions' => [
            'allowClear' => true
          ],
        ]);  ?>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <?= $form->field($model, 'billPaymentBillRef')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
      </div>
    </div>
      <div class="col-sm-6">
        <div class="form-group">
          <?php $data = ['Cash' => 'Cash', 'Cheque'=> 'Cheque','Debit Card'=> 'Debit Card', 'Credit Card' => 'Credit Card','Transfer' => 'Transfer','Voucher' => 'Voucher','Mobile Payment' => 'Mobile Payment','Internet Payment' => 'Internet Payment']; ?>
          <?= $form->field($model, 'billPaymentPayType')->widget(Select2::classname(), [
            'data' => $data,
            'language' => 'en',
            'options' => ['placeholder' => 'Select a Payment Method ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'billPaymentTotalAmt')->textInput(['maxlength' => true,'autocomplete'=>"off",'class' => 'billPaymentTotalAmt form-control']) ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                'inline' => false,
                'dateFormat' => 'yyyy-MM-dd',
            ]); ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'unallocatedAmount')->textInput(['readonly' => true,'class' => 'unlocatedamount form-control']) ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'billPaymentPrivateNote')->textarea(['rows' => 6]) ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <?php
          // necessary for update action.
          if (! $model->isNewRecord) {
            // code...
            $status = ['0' => 'Cancel', '1' => 'Aproved', '2' => 'Waiting'];
            echo $form->field($model, 'paymentstatus')->dropDownList($status, ['readonly' => true,'prompt'=>'Select Status']);
          }else{
            echo $form->field($model, 'paymentstatus')->hiddenInput(['value'=> "2"])->label(false);
          }

          ?>
        </div>
      </div>

    </div>

    <div class="form-group">
      <?= Html::submitButton($model->isNewRecord ? 'Record Payment' : 'Update Payment', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php
         // Html::submitButton('Record Payment', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

if (! $model->isNewRecord) {
  // code...
  // if its not a new record been saved

  $paymentsonHoldamount = FinanceBillsPaymentsLines::find()
      ->joinWith('bill')
      ->where(['finance_bills_payments_lines.vendorId' => $_SESSION['vendorInvoiceId']])
      ->Andwhere(['finance_bills_payments_lines.billId'=>$model->id])
      ->Andwhere(['finance_bills_payments_lines.status'=>$model->paymentstatus])->sum('finance_bills_payments_lines.amount');

    if (empty($paymentsonHoldamount)){
      $paymentsonHoldamount= $model->unallocatedAmount;


    }
}else{

  $paymentsonHoldamount = FinanceBillsPaymentsLines::find()
      ->joinWith('bill')
      ->where(['finance_bills_payments_lines.vendorId' => $_SESSION['vendorInvoiceId']])
      ->Andwhere(['finance_bills_payments_lines.status'=>3])->sum('finance_bills_payments_lines.amount');


  if (empty($paymentsonHoldamount)) {
    // code...
    $paymentsonHoldamount= 0;
  }
}



    // ->asArray()
    // ->all();
// echo $paymentsonHoldamount;
$script = <<<JS
var getAmount = function() {

  // Getting the values
  var billPaymentTotalAmt = $(".billPaymentTotalAmt").val();
  var holdamountfull = $paymentsonHoldamount;
  // get the full amount
  var diffamount = parseInt(billPaymentTotalAmt) - parseInt(holdamountfull);

  // amount = parseInt(qnty) * parseInt(unlocatedamount);

  //Assign the sum value to the field
  $(".unlocatedamount").val(diffamount);
};
//Bind new elements to support the function too
$(".billPaymentTotalAmt").on("change", function() {
  getAmount();
});
JS;
$this->registerJs($script);
?>
