<?php

namespace app\modules\finance\income\estimate\estimateitems;

/**
 * estimateitems module definition class
 */
class estimateitems extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\income\estimate\estimateitems\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
