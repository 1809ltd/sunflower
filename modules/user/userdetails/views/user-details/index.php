<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\userdetails\models\UserDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Details';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
          <?= Html::a('Add User', ['create'], ['class' => 'btn btn-success']) ?>

        </p>
      </div>
    </div>
  </div>

  <div class="box-body user-details-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->userStatus==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // [
            //     'attribute' => 'userStatus',
            //     'value' =>  function($data) { return $data->userStatus == 1 ? 'Yes' : 'No'; }
            // ],

            // 'id',
            // 'userStaffId',
            'userFName',
            'userLName',
            'userPhone',
            //'userEmail:email',
            'username',
            // 'password',
            //'authKey',
            //'password_reset_token',
            //'userImage',
            //'userStatus',
            //'userLastLogin',
            //'userCreatedBy',
            //'userDeleteAt',

            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
