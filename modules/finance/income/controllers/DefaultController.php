<?php
namespace app\modules\finance\income\controllers;
use Yii;
use yii\web\Controller;
// Invoice Details
use app\modules\finance\income\invoice\models\FinanceInvoice;
use app\modules\finance\income\invoice\models\FinanceInvoiceSearch;

/**
 * Default controller for the `income` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    // Get the invoices for a specific months
    public function actionView()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = new \yii\base\DynamicModel([
        'tarehe'
      ]);
      $model->addRule(['tarehe'], 'required')
          ->validate();

      if($model->load(Yii::$app->request->post())){
        // do somenthing with model
        $tarehe1= $model->tarehe;
        $date = $model->tarehe;
        $date = explode('-', $date);

        $month = $date[0];
        $year  = $date[1];

        // echo "this is the month-> ".$month." This is the year-> ".$year;

        // print_r($this->findModels($month,$year));

        if (!empty($date)) {
          // code...
          return $this->render('searchmonths', [
              'allinvoice' => $this->findInvoices($month,$year),
              'model' => $model,
              'resultdate' => $tarehe1,
          ]);

        } else {
          // code...
        }


      }
      return $this->render('searchmonths', ['model'=>$model]);
    }
    // Get all invoice Based on the date
    protected function findInvoices($month,$year)
    {
        if (($model = FinanceInvoice::find()->joinWith('invoiceProject')->joinWith('customer')
                ->where('MONTH(invoiceTxnDate) = :month', [':month' => $month])
                ->Andwhere('YEAR(invoiceTxnDate) = :year', [':year' => $year])
                ->Andwhere('invoiceTxnStatus > :status', [':status' => 0])
                ->orderBy([
                  // 'invoiceTxnDate' => SORT_DESC
                  'invoiceTxnDate'=>SORT_ASC
                ])
                ->all()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
