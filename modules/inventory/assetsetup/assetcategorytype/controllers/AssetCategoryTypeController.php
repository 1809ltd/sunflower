<?php

namespace app\modules\inventory\assetsetup\assetcategorytype\controllers;

use app\modules\inventory\assetsetup\assetcategory\models\AssetCategory;
use Yii;
use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType;
use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AssetCategoryTypeController implements the CRUD actions for AssetCategoryType model.
 */
class AssetCategoryTypeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all AssetCategoryType models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $searchModel = new AssetCategoryTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AssetCategoryType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AssetCategoryType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $model = new AssetCategoryType();

        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['index', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AssetCategoryType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AssetCategoryType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AssetCategoryType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AssetCategoryType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AssetCategoryType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    /**/

    public function actionLists($id)
    {
      // code...
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';

      $assetCategoryTypeNames = AssetCategoryType::find()
                    ->where(['assetCategoryId'=>$id])
                    ->all();

      $countassetCategoryTypeNames = AssetCategoryType::find()
                    ->where(['assetCategoryId'=>$id])
                    ->count();
       // print_r($assetCategoryTypeNames);
       // die();

      if ($countassetCategoryTypeNames>0) {
        // code...

        foreach ($assetCategoryTypeNames as $assetCategoryTypeName) {
          // code...
          echo "<option value= '".$assetCategoryTypeName->id."'>".$assetCategoryTypeName->assetCategoryTypeName."</option>";

        }

      } else {
        // code...

        echo "<option> No Details </option>";

      }



    }

    // CONTROLLER
    public function actionChildType() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $id = end($_POST['depdrop_parents']);
            $list = AssetCategoryType::find()->andWhere(['assetCategoryId'=>$id])->asArray()->all();
            $selected  = null;
            if ($id != null && count($list) > 0) {
                $selected = '';
                foreach ($list as $i => $model) {
                    $out[] = ['id' => $model['id'], 'assetCategoryTypeName' => $model['assetCategoryTypeName']];
                    if ($i == 0) {
                        $selected = $model['id'];
                    }
                }
                // Shows how you can preselect a value
                echo Json::encode(['output' => $out, 'selected'=>$selected]);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }
}
