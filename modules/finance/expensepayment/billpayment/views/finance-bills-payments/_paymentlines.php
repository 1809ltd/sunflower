<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="finance-bills-payments-lines-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
    				// 'billpaymentId',
    				// 'billId',
    				[
    					'label' => 'Invoice No:',
    					'attribute'=>'billId',
    					'value'=>'bill.billRef',
    				],
    				// 'billRef',
    				// 'vendorId',
    				// 'billRef',
    				'referenceNo',
    				'amount',
    				//'status',
    				//'deletedAt',
    				//'created_at',
    				//'updated_at',
    				//'userId',
        ],
    ]); ?>

</div>
