<?php

namespace app\modules\finance\financereport\vendorreport\controllers;

use Yii;
use yii\web\Controller;
// Vendor Statements
use app\modules\finance\financereport\vendorreport\models\VendorStatement;
// Vendor Details
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/**
 * Default controller for the `vendorreport` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = new \yii\base\DynamicModel([
        'startDate','endDate','vendor'
      ]);
      $model->addRule(['startDate','endDate','vendor'], 'required')
            ->validate();

      if($model->load(Yii::$app->request->post())){
        // do somenthing with model

        // print_r($model->Attributes);
        //
        // echo "Tumefika hapa?";

        // Get Vendor details
        $vendor_details = VendorCompanyDetails::find()
                                              ->Where('id = :vendorId', [':vendorId' => $model->vendor])
                                              ->all();

        // Get Amount paid
        $amountpaid= VendorStatement::find()->Where('vendorId = :vendorId', [':vendorId' => $model->vendor])
                                              ->andWhere(['<', 'date', $model->startDate])
                                              ->sum('amountPaid');

        // Get Amount Invoiced
        $amountinvoiced= VendorStatement::find()->Where('vendorId = :vendorId', [':vendorId' => $model->vendor])
                                              ->andWhere(['<', 'date', $model->startDate])
                                              ->sum('amountinvoiced');

        // Get Balance brought forward
        $balbroughtforward= $amountinvoiced-$amountpaid;

        // Generate Vendor Statments based on the search
        $vendor_statement = VendorStatement::find()->Where('vendorId = :vendorId', [':vendorId' => $model->vendor])
                                              ->andWhere(['between', 'date', $model->startDate, $model->endDate])
                                              ->orderBy([
                                                          'date' => SORT_ASC,
                                                          // 'name' => SORT_DESC,
                                                      ])
                                              ->all();
        // Check if statement is empty

        // print_r($vendor_statement);
        //
        // die();
        if (!empty($vendor_statement)) {
          // code...
          return $this->render('index', [
              'vendor_statement' => $vendor_statement,
              'vendor' => $vendor_details,
              'startDate' => $model->startDate,
              'endDate' => $model->endDate,
              'balbroughtforward' => $balbroughtforward,
              'model' => $model,
          ]);

        } else {
          // code...
          return $this->render('index', [
              'model' => $model,
              // 'dataProvider' => $dataProvider,
              // 'events'=> $shereheglobal,
          ]);

        }


      }
      return $this->render('index', [
          'model' => $model,
          // 'dataProvider' => $dataProvider,
          // 'events'=> $shereheglobal,
      ]);


        // return $this->render('index');
    }
}
