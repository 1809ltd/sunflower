<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<div class="finance-customer-purchase-order-lines-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'purchaseOrderId',
            'itemRefName',
            'purchaseOrderLinesDescription:ntext',
            'purchaseOrderLinespack',
            'purchaseOrderLinesQty',
            'purchaseOrderLinesUnitPrice',
            'purchaseOrderLinesAmount',
            'purchaseOrderLinesTaxCodeRef',
            'purchaseOrderLinesTaxamount',
            //'purchaseOrderLinesDiscount',
            //'purchaseOrderLinesCreatedAt',
            //'purchaseOrderLinesUpdatedAt',
            //'purchaseOrderLinesDeletedAt',
            //'userId',

        ],
    ]); ?>

</div>
