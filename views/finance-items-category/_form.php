<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-items-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'financeItemCategoryName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'financeItemCategoryDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'financeItemCategoryParentId')->textInput() ?>

    <?= $form->field($model, 'financeItemCategoryStatus')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
