<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_invoice".
 *
 * @property int $id
 * @property string $invoiceNumber
 * @property int $customerId
 * @property int $localpurchaseOrder
 * @property int $invoiceProjectId
 * @property string $invoiceTxnDate
 * @property string $invoiceDeadlineDate
 * @property string $invoiceNote
 * @property string $invoiceFooter
 * @property string $invoiceTxnStatus
 * @property int $invoiceEmailStatus
 * @property string $invoiceBillEmail
 * @property double $invoiceTaxAmount
 * @property double $invoiceDiscountAmount
 * @property double $invoiceSubAmount
 * @property double $invoiceAmount
 * @property int $invoicedCreatedby
 * @property string $invoicedCreatedAt
 * @property string $invoicedUpdatedAt
 * @property string $invoicedDeletedAt
 * @property int $approvedby
 *
 * @property FinanceCreditMemoLines[] $financeCreditMemoLines
 * @property UserDetails $invoicedCreatedby0
 * @property FinanceCustomerPurchaseOrder $localpurchaseOrder0
 * @property EventDetails $invoiceProject
 * @property CustomerDetails $customer
 * @property FinanceInvoiceLines[] $financeInvoiceLines
 * @property FinancePayment[] $financePayments
 */
class FinanceInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoiceNumber', 'customerId', 'invoiceProjectId', 'invoiceTxnDate', 'invoiceDeadlineDate', 'invoiceTxnStatus', 'invoiceBillEmail', 'invoiceTaxAmount', 'invoiceDiscountAmount', 'invoiceSubAmount', 'invoiceAmount', 'invoicedCreatedby'], 'required'],
            [['customerId', 'localpurchaseOrder', 'invoiceProjectId', 'invoiceEmailStatus', 'invoicedCreatedby', 'approvedby'], 'integer'],
            [['invoiceTxnDate', 'invoiceDeadlineDate', 'invoicedCreatedAt', 'invoicedUpdatedAt', 'invoicedDeletedAt'], 'safe'],
            [['invoiceNote', 'invoiceFooter', 'invoiceTxnStatus'], 'string'],
            [['invoiceTaxAmount', 'invoiceDiscountAmount', 'invoiceSubAmount', 'invoiceAmount'], 'number'],
            [['invoiceNumber'], 'string', 'max' => 50],
            [['invoiceBillEmail'], 'string', 'max' => 100],
            [['invoiceNumber'], 'unique'],
            [['invoicedCreatedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['invoicedCreatedby' => 'id']],
            [['localpurchaseOrder'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceCustomerPurchaseOrder::className(), 'targetAttribute' => ['localpurchaseOrder' => 'id']],
            [['invoiceProjectId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['invoiceProjectId' => 'id']],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoiceNumber' => 'Invoice Number',
            'customerId' => 'Customer',
            'localpurchaseOrder' => 'Localpurchase Order',
            'invoiceProjectId' => 'Project',
            'invoiceTxnDate' => 'Date',
            'invoiceDeadlineDate' => 'Due Date',
            'invoiceNote' => 'Note',
            'invoiceFooter' => 'Footer',
            'invoiceTxnStatus' => 'Txn Status',
            'invoiceEmailStatus' => 'Email Status',
            'invoiceBillEmail' => 'Bill Email',
            'invoiceTaxAmount' => 'Tax Amount',
            'invoiceDiscountAmount' => 'Discount Amount',
            'invoiceSubAmount' => 'Sub Amount',
            'invoiceAmount' => 'Amount',
            'invoicedCreatedby' => 'Createdby',
            'invoicedCreatedAt' => 'Created At',
            'invoicedUpdatedAt' => 'Updated At',
            'invoicedDeletedAt' => 'Deleted At',
            'approvedby' => 'Approvedby',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemoLines()
    {
        return $this->hasMany(FinanceCreditMemoLines::className(), ['invoiceNumber' => 'invoiceNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoicedCreatedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'invoicedCreatedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalpurchaseOrder0()
    {
        return $this->hasOne(FinanceCustomerPurchaseOrder::className(), ['id' => 'localpurchaseOrder']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceProject()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'invoiceProjectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoiceLines()
    {
        return $this->hasMany(FinanceInvoiceLines::className(), ['invoiceId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePayments()
    {
        return $this->hasMany(FinancePayment::className(), ['paymentLinkedTxn' => 'invoiceNumber']);
    }
    public function getinvoiceItems($id)
    {
      return FinanceInvoiceLines::find()
        ->where(['and', "invoiceId=$id"])
        ->all();
    }
    public function getCustomerDetails($id)
    {
      return CustomerDetails::find()
        ->where(['and', "id=$id"])->all();
        // ->indexBy('id')->column();
    }

    public function getInvnumber()
    {
        //select Invoice code

        $connection = Yii::$app->db;
        $query= "Select MAX(invoiceNumber) AS number from finance_invoice where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "INV-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "INV-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "INV-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
