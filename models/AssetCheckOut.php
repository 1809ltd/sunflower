<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asset_check_out".
 *
 * @property int $id
 * @property int $assetId
 * @property int $locationId
 * @property string $checkOutDate
 * @property string $dueDate
 * @property int $activity
 * @property int $activityId
 * @property int $vehicle
 * @property int $assignto
 * @property string $timestamp
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $status
 * @property int $athurizedBy
 * @property int $userId
 *
 * @property AssetCheckIn[] $assetCheckIns
 * @property PersonnelDetails $assignto0
 * @property UserDetails $user
 * @property EventDetails $activity0
 * @property AssetRegistration $asset
 * @property AssetSiteLocation $location
 * @property EventTransport $vehicle0
 */
class AssetCheckOut extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_check_out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'checkOutDate', 'dueDate', 'status', 'userId'], 'required'],
            [['assetId', 'locationId', 'activity', 'activityId', 'vehicle', 'assignto', 'status', 'athurizedBy', 'userId'], 'integer'],
            [['checkOutDate', 'dueDate', 'timestamp', 'updatedAt', 'deletedAt'], 'safe'],
            [['assignto'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelDetails::className(), 'targetAttribute' => ['assignto' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['activityId' => 'id']],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['locationId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetSiteLocation::className(), 'targetAttribute' => ['locationId' => 'id']],
            [['vehicle'], 'exist', 'skipOnError' => true, 'targetClass' => EventTransport::className(), 'targetAttribute' => ['vehicle' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetId' => 'Asset ID',
            'locationId' => 'Location ID',
            'checkOutDate' => 'Check Out Date',
            'dueDate' => 'Due Date',
            'activity' => 'Activity',
            'activityId' => 'Activity ID',
            'vehicle' => 'Vehicle',
            'assignto' => 'Assignto',
            'timestamp' => 'Timestamp',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'athurizedBy' => 'Athurized By',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckIns()
    {
        return $this->hasMany(AssetCheckIn::className(), ['checkOutId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignto0()
    {
        return $this->hasOne(PersonnelDetails::className(), ['id' => 'assignto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity0()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(AssetSiteLocation::className(), ['id' => 'locationId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle0()
    {
        return $this->hasOne(EventTransport::className(), ['id' => 'vehicle']);
    }
}
