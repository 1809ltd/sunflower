<!-- <div class="site-error">

    <div class="alert alert-danger">
        <span class="fa fa-warning"></span>
    </div>

    <p>
        You don't currently have permission to access this module.
    </p>
    <p>
        Please refer to your system administration. Thank you.
    </p>

</div> -->

<div class="error-page">
  <h2 class="headline text-yellow"> ...</h2>
  <div class="error-content">
    <h3><i class="fa fa-warning text-yellow"></i> Oops! Access is denied.</h3>
    <p>
      You don't currently have permission to access this module.
    </p>
    <p>
      Please refer to your system administration. Thank you.
    </p>
    <!-- <p>
      We could not find the page you were looking for.
      Meanwhile, you may <a href="../../index.html">return to dashboard</a> or try using the search form.
    </p> -->
    <form class="search-form">
      <div class="input-group">
        <input type="text" name="search" class="form-control" placeholder="Search">
        <div class="input-group-btn">
          <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
          </button>
        </div>
      </div>
      <!-- /.input-group -->
    </form>
  </div>
  <!-- /.error-content -->
</div>
