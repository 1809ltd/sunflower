<?php

namespace app\modules\finance\productsetup\financeitems\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\productsetup\financeitems\models\FinanceItems;

/**
 * FinanceItemsSearch represents the model behind the search form of `app\modules\finance\productsetup\financeitems\models\FinanceItems`.
 */
class FinanceItemsSearch extends FinanceItems
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'itemCategory', 'itemCategoryType', 'itemsIncomeAccountRef', 'itemsExpenseAccountRef', 'itemsAssetAccountRef', 'itemsStatus', 'userId'], 'integer'],
            [['itemsName', 'itemsCode', 'itemsDescription', 'itemsTaxable', 'itemsClassification', 'itemsPurchaseDesc', 'itemsCreatedAt', 'itemsLastupdatedAt', 'itemsDeletedAt'], 'safe'],
            [['itemsCustomerUnitPrice', 'itemsSupplierUnitPrice', 'itemsPurchaseCost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceItems::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'itemCategory' => $this->itemCategory,
            'itemCategoryType' => $this->itemCategoryType,
            'itemsCustomerUnitPrice' => $this->itemsCustomerUnitPrice,
            'itemsSupplierUnitPrice' => $this->itemsSupplierUnitPrice,
            'itemsIncomeAccountRef' => $this->itemsIncomeAccountRef,
            'itemsPurchaseCost' => $this->itemsPurchaseCost,
            'itemsExpenseAccountRef' => $this->itemsExpenseAccountRef,
            'itemsAssetAccountRef' => $this->itemsAssetAccountRef,
            'itemsCreatedAt' => $this->itemsCreatedAt,
            'itemsLastupdatedAt' => $this->itemsLastupdatedAt,
            'itemsDeletedAt' => $this->itemsDeletedAt,
            'itemsStatus' => $this->itemsStatus,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'itemsName', $this->itemsName])
            ->andFilterWhere(['like', 'itemsCode', $this->itemsCode])
            ->andFilterWhere(['like', 'itemsDescription', $this->itemsDescription])
            ->andFilterWhere(['like', 'itemsTaxable', $this->itemsTaxable])
            ->andFilterWhere(['like', 'itemsClassification', $this->itemsClassification])
            ->andFilterWhere(['like', 'itemsPurchaseDesc', $this->itemsPurchaseDesc]);

        return $dataProvider;
    }
}
