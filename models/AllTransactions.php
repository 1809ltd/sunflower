<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "all_transactions".
 *
 * @property int $transactionId
 * @property int $referenceId
 * @property string $referenceCode
 * @property string $transactionCode
 * @property string $projectId
 * @property int $accountParentId
 * @property string $accountsclassfication
 * @property int $accountId
 * @property string $accountName
 * @property string $transactionName
 * @property string $tranctionDescription
 * @property string $dr_amount
 * @property string $cr_amount
 * @property string $transactionDate
 * @property string $createdAt
 * @property int $status
 * @property string $transactionCategory
 * @property string $transactionClassification
 * @property string $transactionTable
 * @property string $referenceTable
 */
class AllTransactions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'all_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transactionId', 'referenceId', 'accountParentId', 'accountId', 'status'], 'integer'],
            [['transactionName', 'tranctionDescription'], 'string'],
            [['transactionDate', 'createdAt'], 'safe'],
            [['referenceCode'], 'string', 'max' => 100],
            [['transactionCode', 'projectId'], 'string', 'max' => 50],
            [['accountsclassfication', 'accountName'], 'string', 'max' => 255],
            [['dr_amount', 'cr_amount'], 'string', 'max' => 22],
            [['transactionCategory'], 'string', 'max' => 33],
            [['transactionClassification'], 'string', 'max' => 31],
            [['transactionTable'], 'string', 'max' => 35],
            [['referenceTable'], 'string', 'max' => 29],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'transactionId' => 'Transaction ID',
            'referenceId' => 'Reference ID',
            'referenceCode' => 'Reference Code',
            'transactionCode' => 'Transaction Code',
            'projectId' => 'Project ID',
            'accountParentId' => 'Account Parent ID',
            'accountsclassfication' => 'Accountsclassfication',
            'accountId' => 'Account ID',
            'accountName' => 'Account Name',
            'transactionName' => 'Transaction Name',
            'tranctionDescription' => 'Tranction Description',
            'dr_amount' => 'Dr Amount',
            'cr_amount' => 'Cr Amount',
            'transactionDate' => 'Transaction Date',
            'createdAt' => 'Created At',
            'status' => 'Status',
            'transactionCategory' => 'Transaction Category',
            'transactionClassification' => 'Transaction Classification',
            'transactionTable' => 'Transaction Table',
            'referenceTable' => 'Reference Table',
        ];
    }

    /*Get Revenue P&L*/

    public function getRevenueIncs()
    {
      $connection = Yii::$app->getDb();
      $command = $connection->createCommand("SELECT
                                        	all_transactions.accountParentId,
                                        	all_transactions.accountsclassfication,
                                        	all_transactions.accountId,
                                        	all_transactions.accountName,
                                        	Sum( all_transactions.dr_amount ),
                                        	Sum( all_transactions.cr_amount ),
                                        	all_transactions.transactionClassification,
                                        	all_transactions.transactionCategory
                                        FROM
                                        	all_transactions
                                        WHERE
                                        	all_transactions.accountsclassfication = 'Revenue'
                                        GROUP BY
                                        	all_transactions.accountId,
                                        	all_transactions.transactionClassification
                                        ORDER BY all_transactions.accountParentId ASC");

      $data = $command->queryAll();

      return $data;
    }
}
