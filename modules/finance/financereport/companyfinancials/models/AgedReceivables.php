<?php

namespace app\modules\finance\financereport\companyfinancials\models;

use Yii;

/**
 * This is the model class for table "aged_receivables".
 *
 * @property int $customerId
 * @property string $receivables
 * @property double $Coming Due
 * @property double $1-30 Days
 * @property double $31-60 Days
 * @property double $61-90 Days
 * @property double $>90 Days
 * @property double $unlocated
 */
class AgedReceivables extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aged_receivables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerId'], 'required'],
            [['customerId'], 'integer'],
            [['Coming Due', '1-30 Days', '31-60 Days', '61-90 Days', '>90 Days', 'unlocated'], 'number'],
            [['receivables'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customerId' => 'Customer ID',
            'receivables' => 'Receivables',
            'Coming Due' => 'Coming  Due',
            '1-30 Days' => '1 30  Days',
            '31-60 Days' => '31 60  Days',
            '61-90 Days' => '61 90  Days',
            '>90 Days' => '>90  Days',
            'unlocated' => 'Unlocated',
        ];
    }
}
