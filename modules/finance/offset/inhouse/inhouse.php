<?php

namespace app\modules\finance\offset\inhouse;

/**
 * inhouse module definition class
 */
class inhouse extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\offset\inhouse\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->modules =[
          /*Fiance inoffsetinvoice Sub Module*/
          'inoffsetinvoice' => [
              'class' => 'app\modules\finance\offset\inhouse\inoffsetinvoice\inoffsetinvoice',
          ],
        ];

        // custom initialization code goes here
    }
}
