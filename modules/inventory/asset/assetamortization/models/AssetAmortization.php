<?php

namespace app\modules\inventory\asset\assetamortization\models;
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "asset_amortization".
 *
 * @property int $id
 * @property int $assetId
 * @property int $repayment
 * @property double $interestAmount
 * @property double $principalAmount
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $status
 * @property string $amortizationDate
 * @property double $startBalance
 * @property double $endBalance
 * @property double $cummulativeInterest
 * @property double $cummulativePrincipal
 *
 * @property AssetRegistration $asset
 * @property UserDetails $user
 */
class AssetAmortization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_amortization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'repayment', 'interestAmount', 'principalAmount', 'userId', 'status', 'amortizationDate'], 'required'],
            [['assetId', 'repayment', 'userId', 'status'], 'integer'],
            [['interestAmount', 'principalAmount', 'startBalance', 'endBalance', 'cummulativeInterest', 'cummulativePrincipal'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt', 'amortizationDate'], 'safe'],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetId' => 'Asset ID',
            'repayment' => 'Repayment',
            'interestAmount' => 'Interest Amount',
            'principalAmount' => 'Principal Amount',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'status' => 'Status',
            'amortizationDate' => 'Amortization Date',
            'startBalance' => 'Start Balance',
            'endBalance' => 'End Balance',
            'cummulativeInterest' => 'Cummulative Interest',
            'cummulativePrincipal' => 'Cummulative Principal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
