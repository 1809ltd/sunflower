<?php

namespace app\modules\user\userrolemodulepermission;

/**
 * userrolemodulepermission module definition class
 */
class userrolemodulepermission extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\user\userrolemodulepermission\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
