<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */

$this->title = 'Delivery Check Out';
$this->params['breadcrumbs'][] = ['label' => 'Asset Check Outs', 'url' => ['deliverysearch']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-check-out-create">
    <?= $this->render('deliveryfromadd', [
      'checkoutitem' => $checkoutitem,
      'model' => $model,
    ]) ?>

</div>
