<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PersonnelDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="personnel-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nationalId') ?>

    <?= $form->field($model, 'displayName') ?>

    <?= $form->field($model, 'firstName') ?>

    <?= $form->field($model, 'sName') ?>

    <?php // echo $form->field($model, 'lName') ?>

    <?php // echo $form->field($model, 'primaryPhone') ?>

    <?php // echo $form->field($model, 'secondaryPhone') ?>

    <?php // echo $form->field($model, 'printOnChequeName') ?>

    <?php // echo $form->field($model, 'primaryAddr') ?>

    <?php // echo $form->field($model, 'town') ?>

    <?php // echo $form->field($model, 'area') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'hiredDate') ?>

    <?php // echo $form->field($model, 'createdAt') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'billable') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
