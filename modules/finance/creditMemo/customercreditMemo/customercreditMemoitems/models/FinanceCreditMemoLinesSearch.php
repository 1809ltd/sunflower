<?php

namespace app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\models\FinanceCreditMemoLines;

/**
 * FinanceCreditMemoLinesSearch represents the model behind the search form of `app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\models\FinanceCreditMemoLines`.
 */
class FinanceCreditMemoLinesSearch extends FinanceCreditMemoLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'creditMemoId', 'creditMemoLineStatus', 'userId'], 'integer'],
            [['invoiceNumber', 'creditMemoLineDescription', 'creditMemoLineCreatedAt', 'creditMemoLineUpdatedAt', 'creidtDeletedAt'], 'safe'],
            [['creditMemoLineAmount', 'creditTaxCodeRef', 'taxamount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceCreditMemoLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'creditMemoId' => $this->creditMemoId,
            'creditMemoLineAmount' => $this->creditMemoLineAmount,
            'creditTaxCodeRef' => $this->creditTaxCodeRef,
            'taxamount' => $this->taxamount,
            'creditMemoLineStatus' => $this->creditMemoLineStatus,
            'creditMemoLineCreatedAt' => $this->creditMemoLineCreatedAt,
            'creditMemoLineUpdatedAt' => $this->creditMemoLineUpdatedAt,
            'creidtDeletedAt' => $this->creidtDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'invoiceNumber', $this->invoiceNumber])
            ->andFilterWhere(['like', 'creditMemoLineDescription', $this->creditMemoLineDescription]);

        return $dataProvider;
    }
}
