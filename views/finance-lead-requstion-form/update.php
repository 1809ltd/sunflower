<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequstionForm */

$this->title = 'Update Lead Requstion Form: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Lead Requstion Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-lead-requstion-form-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceLeadRequstionFormList'=>$modelsFinanceLeadRequstionFormList, //to enable Dynamic Form
    ]) ?>

</div>
