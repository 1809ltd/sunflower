<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PersonnelDetails */

$this->title = 'Create Personnel Details';
$this->params['breadcrumbs'][] = ['label' => 'Personnel Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personnel-details-create">
  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
