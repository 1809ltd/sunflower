<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\journal\models\FinanceJournalEntrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*Dropdown action Bar*/
use yii\bootstrap\ButtonDropdown;

$this->title = 'Finance Journal Entries';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="finance-journal-entry-index">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <p class="pull-right">
          <?php
          if (Yii::$app->Permission->getSpecificPermission('new')) {
            // code...
            echo Html::a('Record Journal Entry', ['create'], ['class' => 'btn btn-success']);
          } else {
            // code...
          }
          ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                'journalEntryNo',
                'journalEntrydate',
                'total',
                // 'preparedby',
                // 'approvedby',
                //'createdate',
                //'deletedAt',
                //'updatedAt',
                //'status',
                /* ... GridView configuration ... */
                [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{all}',
                  'buttons' => [
                    'all' => function ($url, $model, $key) {
                      return ButtonDropdown::widget([
                        'encodeLabel' => false, // if you're going to use html on the button label
                        'label' => 'Options',
                        'dropdown' => [
                          'encodeLabels' => false, // if you're going to use html on the items' labels
                          'items' => [
                            [
                              'label' => \Yii::t('yii', 'View'),
                              'url' => ['view', 'id' => $key],
                            ],
                            [
                              'label' => \Yii::t('yii', 'Update'),
                              'url' => ['update', 'id' => $key],
                              'visible' => Yii::$app->Permission->getSpecificPermission('save'),  // if you want to hide an item based on a condition, use this
                            ],
                            [
                              'label' => \Yii::t('yii', 'Aprrove'),
                              'url' => ['update', 'id' => $key],
                              'visible' => Yii::$app->Permission->getSpecificPermission('other'),  // if you want to hide an item based on a condition, use this
                            ],
                            [
                              'label' => \Yii::t('yii', 'Delete'),
                              'linkOptions' => [
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'),
                                ],
                              ],
                              'url' => ['delete', 'id' => $key],
                              'visible' => Yii::$app->Permission->getSpecificPermission('remove'),   // same as above
                            ],
                          ],
                          'options' => [
                            'class' => 'dropdown-menu-right', // right dropdown
                          ],
                        ],
                        'options' => [
                          'class' => 'btn-default',   // btn-success, btn-info, et cetera
                        ],
                        'split' => true,    // if you want a split button
                      ]);
                    },
                  ],
                ],
                /* ... additional GridView configuration ... */
                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">

  </div>
</div>
<!-- /.box -->
<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   // $('li.treeview').addClass('active');
   $('li.treeview').addClass('treeview menu-open');
   $('#journal').addClass('active');
   $('#finance-journal-entry').addClass('active');
", View::POS_READY);
?>
