<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\transfer\models\FinanceTransferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Transfers';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <div class="finance-transfer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Transfer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tranferRefernce',
            // 'tranferFromAccountRef',
            [
              'attribute'=>'tranferFromAccountRef',
              'value'=>'tranferFromAccountRef0.fullyQualifiedName',
            ],
            // 'tranferToAccountRef',
            [
              'attribute'=>'tranferToAccountRef',
              'value'=>'tranferToAccountRef0.fullyQualifiedName',
            ],
            'tranferAmount',
            //'transferCharges',
            //'tranferTxnDate',
            //'tranferCreatedAt',
            //'tranferUpdatedAt',
            //'tranferDeletedAt',
            //'userId',
            //'status',
            //'createdBy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div> -->



<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::button('Add Transfer', ['value'=>Url::to((['create'])),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        </p>
      </div>
    </div>
  </div>

  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
   <?php
    Modal::begin([
        'header'=>'<h4>Update Model</h4>',
        'id'=>'update-modal',
        'size'=>'modal-lg'
    ]);

    echo "<div id='updateModalContent'></div>";

    Modal::end();
?>

  <div class="box-body finance-transfer-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->status==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


                  // 'id',
                  'tranferRefernce',
                  // 'tranferFromAccountRef',
                  // 'tranferToAccountRef',
                  [
                    'attribute'=>'tranferFromAccountRef',
                    'value'=>'tranferFromAccountRef0.fullyQualifiedName',
                  ],
                  // 'tranferToAccountRef',
                  [
                    'attribute'=>'tranferToAccountRef',
                    'value'=>'tranferToAccountRef0.fullyQualifiedName',
                  ],
                  'tranferAmount',
                  'transferCharges',
                  //'tranferTxnDate',
                  //'tranferCreatedAt',
                  //'tranferUpdatedAt',
                  //'tranferDeletedAt',
                  //'userId',
                  //'status',
                  //'createdBy',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => ['view' => function($url, $model) {
            	    return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
            	},
              'update' => function($url,$model,$key){
                    $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                        'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                        'class'=>'update-modal-click grid-action',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'bottom',
                        'title'=>'Update'
                    ]);
                    return $btn;
              },
            	'delete' => function($url, $model) {
            	    return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
            	}
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
