<?php

namespace app\modules\finance\expensepayment\leadexpensepayment\controllers;

use Yii;
use app\modules\finance\expensepayment\leadexpensepayment\models\FinanceLeadRequisitionPayments;
use app\modules\finance\expensepayment\leadexpensepayment\models\FinanceLeadRequisitionPaymentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceLeadRequisitionPaymentsController implements the CRUD actions for FinanceLeadRequisitionPayments model.
 */
class FinanceLeadRequisitionPaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...
            //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
            $this->reqpaymentUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          }else {
            // code...
            $this->reqpaymentUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
          }
          return parent::beforeSave();
        }


        /**
         * Lists all FinanceLeadRequisitionPayments models.
         * @return mixed
         */
        public function actionIndex()
        {
            $searchModel = new FinanceLeadRequisitionPaymentsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single FinanceLeadRequisitionPayments model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new FinanceLeadRequisitionPayments model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
            $model = new FinanceLeadRequisitionPayments();
            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;


            $model->reqrecepitNumber = $model->getRcpnumber();

            //Getting user Log in details
            $model->userId= "1";

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }

        /**
         * Updates an existing FinanceLeadRequisitionPayments model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
            $model = $this->findModel($id);
            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;


            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        /**
         * Deletes an existing FinanceLeadRequisitionPayments model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the FinanceLeadRequisitionPayments model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return FinanceLeadRequisitionPayments the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = FinanceLeadRequisitionPayments::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
