<?php
/*Models Used to help with the movement*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
use app\modules\finance\productsetup\financeitems\models\FinanceItemsSearch;

// /*Item Category*/
use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory;
/*Categry Type*/
use app\modules\finance\productsetup\financeitemscategorytype\models\FinanceItemsCategoryType;

/*Pop up menu*/
use yii\helpers\Url;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
?>

<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <img style="height:25%;width:25%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
            <i class=""></i> Productlist.
            <small class="pull-right">As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">

        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">


        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Code</th>
              <th>Service Name</th>
              <th>Description</th>
              <th>Classification</th>
              <th>Income A/C</th>
            </tr>

            </thead>
            <tbody>
              <?php
              // Getting the Parent Category  Name:
              $parentId= FinanceItemsCategory::find()
                ->where('financeItemCategoryStatus=1')
                ->orderBy(
                  [
                    new \yii\db\Expression('COALESCE(financeItemCategoryParentId,id)')
                  ]
                  )
                ->all();

              foreach ($parentId as $parentId) {
                // code...
                // Getting the Parent Category Name
                if ($parentId["financeItemCategoryParentId"]==0) {
                  // code...

                  $childId= FinanceItemsCategoryType::find()
                    ->where('financeItemCategoryId = :financeItemCategoryId', [':financeItemCategoryId' => $parentId["id"]])
                    ->orderBy(
                      [
                        new \yii\db\Expression('id')
                      ]
                      )
                    ->all();

                  if (!empty($childId)) {
                    // code...
                    // Getting the Name of the Service Category
                    ?>
                    <tr>
                      <th style="background-color: gold;"><span class="align-numbers"><?= $parentId["financeItemCategoryName"] ;?></span></th>
                      <th style="background-color: gold;"></th>
                      <th style="background-color: gold;"></th>
                      <th style="background-color: gold;"></span></th>
                      <th style="background-color: gold;"></th>
                    </tr>
                    <?php
                    foreach ($childId as $childId) {

                      // Getting the Service Based on the child Id
                      $service= FinanceItems::find()
                        ->where('itemCategoryType = :itemCategoryType', [':itemCategoryType' => $childId["id"]])
                        ->orderBy(
                          [
                            new \yii\db\Expression('id')
                          ]
                          )
                        ->all();

                      if (!empty($service)) {
                        // code...
                        ?>
                        <tr>
                          <th style="background-color: violet;"></th>
                          <th style="background-color: violet;"><span class="align-numbers"><?= $childId["financeItemCategoryTypeName"] ;?></span></th>
                          <th style="background-color: violet;"></th>
                          <th style="background-color: violet;"></span></th>
                          <th style="background-color: violet;"></th>
                        </tr>
                        <?php

                        foreach ($service as $service) {
                          // code...
                          ?>
                          <tr>
                            <td><?= $service["itemsCode"] ?></td>
                            <td><span class="align-numbers"><?= $service["itemsName"] ?></span></td>
                            <td><?= $service["itemsDescription"] ?></td>
                            <td><?= $service["itemsClassification"] ?></td>
                            <td><?= $service["itemsIncomeAccountRef0"]["fullyQualifiedName"]?></td>
                          </tr>
                          <?php
                        }
                      }

                    }
                  }


                }else {

                  // code...
                  // Getting the Name of the Service Category if parent its not null
                  $childId= FinanceItemsCategoryType::find()
                    ->where('financeItemCategoryId = :financeItemCategoryId', [':financeItemCategoryId' => $parentId["id"]])
                    ->orderBy(
                      [
                        new \yii\db\Expression('id')
                      ]
                      )
                    ->all();

                  if (!empty($childId)) {
                    // code...
                    // Getting the Name of the Service Category
                    ?>
                    <tr>
                      <th style="background-color: gold;"><span class="align-numbers"><?= $parentId["financeItemCategoryName"] ;?></span></th>
                      <th style="background-color: gold;"></th>
                      <th style="background-color: gold;"></th>
                      <th style="background-color: gold;"></span></th>
                      <th style="background-color: gold;"></th>
                    </tr>
                    <?php
                    foreach ($childId as $childId) {

                      // Getting the Service Based on the child Id
                      $service= FinanceItems::find()
                        ->where('itemCategoryType = :itemCategoryType', [':itemCategoryType' => $childId["id"]])
                        ->orderBy(
                          [
                            new \yii\db\Expression('id')
                          ]
                          )
                        ->all();

                      if (!empty($service)) {
                        // code...
                        ?>
                        <tr>
                          <th style="background-color: violet;"></th>
                          <th style="background-color: violet;"><span class="align-numbers"><?= $childId["financeItemCategoryTypeName"] ;?></span></th>
                          <th style="background-color: violet;"></th>
                          <th style="background-color: violet;"></span></th>
                          <th style="background-color: violet;"></th>
                        </tr>
                        <?php

                        foreach ($service as $service) {
                          // code...
                          ?>
                          <tr>
                            <td><?= $service["itemsCode"] ?></td>
                            <td><span class="align-numbers"><?= $service["itemsName"] ?></span></td>
                            <td><?= $service["itemsDescription"] ?></td>
                            <td><?= $service["itemsClassification"] ?></td>
                            <td><?= $service["itemsIncomeAccountRef0"]["fullyQualifiedName"]?></td>
                          </tr>
                          <?php
                        }
                      }

                    }
                  }
                }
              }
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>
        </div>
      </div>
    </section>
