<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personnel_details".
 *
 * @property int $id
 * @property string $nationalId
 * @property string $displayName
 * @property string $firstName
 * @property string $sName
 * @property string $lName
 * @property string $primaryPhone
 * @property string $secondaryPhone
 * @property int $printOnChequeName
 * @property string $primaryAddr
 * @property string $town
 * @property string $area
 * @property string $email
 * @property int $status
 * @property string $hiredDate
 * @property string $createdAt
 * @property string $deletedAt
 * @property string $updatedAt
 * @property int $billable
 *
 * @property AssetCheckOut[] $assetCheckOuts
 * @property EventWorkplan[] $eventWorkplans
 * @property FinanceCustomerLeadRequstionFormList[] $financeCustomerLeadRequstionFormLists
 * @property FinanceEventRequstionFormList[] $financeEventRequstionFormLists
 */
class PersonnelDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personnel_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nationalId', 'displayName', 'firstName', 'status', 'hiredDate', 'billable'], 'required'],
            [['printOnChequeName', 'status', 'billable'], 'integer'],
            [['primaryAddr'], 'string'],
            [['hiredDate', 'createdAt', 'deletedAt', 'updatedAt'], 'safe'],
            [['nationalId', 'town'], 'string', 'max' => 50],
            [['displayName', 'firstName', 'sName', 'lName', 'area', 'email'], 'string', 'max' => 100],
            [['primaryPhone'], 'string', 'max' => 25],
            [['secondaryPhone'], 'string', 'max' => 15],
            [['nationalId'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nationalId' => 'National ID',
            'displayName' => 'Display Name',
            'firstName' => 'First Name',
            'sName' => 'Surname',
            'lName' => 'Other Name',
            'primaryPhone' => 'Primary Phone',
            'secondaryPhone' => 'Secondary Phone',
            'printOnChequeName' => 'Print On Cheque Name',
            'primaryAddr' => 'Primary Addr',
            'town' => 'Town',
            'area' => 'Area',
            'email' => 'Email',
            'status' => 'Status',
            'hiredDate' => 'Hired Date',
            'createdAt' => 'Created At',
            'deletedAt' => 'Deleted At',
            'updatedAt' => 'Updated At',
            'billable' => 'Billable',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['assignto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventWorkplans()
    {
        return $this->hasMany(EventWorkplan::className(), ['personnelid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerLeadRequstionFormLists()
    {
        return $this->hasMany(FinanceCustomerLeadRequstionFormList::className(), ['personelId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionFormLists()
    {
        return $this->hasMany(FinanceEventRequstionFormList::className(), ['personelId' => 'id']);
    }
}
