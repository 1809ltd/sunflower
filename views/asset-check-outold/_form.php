<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;

use kartik\depdrop\DepDrop;

/*Array*/
use yii\helpers\ArrayHelper;
use app\models\AssetRegistration;
use app\models\AssetCheckOut;
use app\models\CompanySiteLocation;
use app\models\EventTransport;
use app\models\EventDetails;

use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\AssetCheckOut */
/* @var $form yii\widgets\ActiveForm */
?>

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
  </div>
  <?php $form = ActiveForm::begin(); ?>

  <div class="modal-body asset-check-out-form">

    <?= $form->field($model, 'assetId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AssetRegistration::find()->all(),'id','assetName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Asset ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?= $form->field($model, 'locationId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CompanySiteLocation::find()->all(),'id','locationName'),
        'language' => 'en',
        'id'=>'location',
        'options' => ['placeholder' => 'Select a Asset Location ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'checkOutDate')->widget(\yii\jui\DatePicker::class, [
          //'language' => 'ru',
          'options' => ['class' => 'form-control'],
          'inline' => false,
          'dateFormat' => 'yyyy-MM-dd',
      ]) ?>

    <?= $form->field($model, 'dueDate')->widget(\yii\jui\DatePicker::class, [
          //'language' => 'ru',
          'options' => ['class' => 'form-control'],
          'inline' => false,
          'dateFormat' => 'yyyy-MM-dd',
      ]) ?>
    <?= $form->field($model, 'activityId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(EventDetails::find()->all(),'id','eventNumber'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Event ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?= $form->field($model, 'vehicle')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(EventTransport::find()->all(),'id','numberPlate'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Vehicle ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?= $form->field($model, 'assignto')->textInput() ?>


    <?php $status = ['1' => 'Available', '3' => 'Event', '4' => 'Suspended']; ?>
    <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']); ?>
    

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

  </div>
  <?php ActiveForm::end(); ?>
