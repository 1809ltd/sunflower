<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="company-department-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

          //'id',
          //'customerId',
          //'customer.customerDisplayName',
          //'eventNumber',
          // 'eventName',
          // 'eventTheme',
          // 'eventVistorsNumber',
          // 'eventCategory',
          'eventStartDate',
          'eventEndDate',
          'eventSetUpDateTime',
          'eventSetDownDateTime',
          //'eventLocation',
          'eventDescription:ntext',
          'eventNotes:ntext',
          'eventCreatedAt',
          //'eventLastUpdatedAt',
          //'eventDeletedAt',
          //'userId',

        ],
    ]); ?>

</div>
