<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanySiteDetails */

$this->title = 'Create Company Site Details';
$this->params['breadcrumbs'][] = ['label' => 'Company Site Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-site-details-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
