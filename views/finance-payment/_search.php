<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\receipt\invoicepayment\models\FinancePaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'invoicePaymentPayType') ?>

    <?= $form->field($model, 'invoicePaymentaccount') ?>

    <?= $form->field($model, 'recepitNumber') ?>

    <?= $form->field($model, 'invoicePaymentTotalAmt') ?>

    <?php // echo $form->field($model, 'unallocatedAmount') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'invoicePaymentinvoiceRef') ?>

    <?php // echo $form->field($model, 'customerId') ?>

    <?php // echo $form->field($model, 'invoicePaymentPrivateNote') ?>

    <?php // echo $form->field($model, 'invoicePaymentCreatedAt') ?>

    <?php // echo $form->field($model, 'invoicePaymentUpdatedAt') ?>

    <?php // echo $form->field($model, 'invoicePaymentDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <?php // echo $form->field($model, 'approvedBy') ?>

    <?php // echo $form->field($model, 'paymentstatus') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
