<?php

namespace app\modules\finance\offset\outhouse\controllers;

use Yii;
use app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemo;
use app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines;
use app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLinesSearch;



/**
 * FinanceVendorOffsetMemoController implements the CRUD actions for FinanceVendorOffsetMemo model.
 */
class FinanceVendorOffsetMemoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->vendorOffsetMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->vendorOffsetMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceVendorOffsetMemo models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $searchModel = new FinanceVendorOffsetMemoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceVendorOffsetMemo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceVendorOffsetMemo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $model = new FinanceVendorOffsetMemo();

        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        $modelsFinanceVendorOffsetMemoLines = [new FinanceVendorOffsetMemoLines];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsFinanceVendorOffsetMemoLines = Model::createMultiple(FinanceVendorOffsetMemoLines::classname());
          Model::loadMultiple($modelsFinanceVendorOffsetMemoLines, Yii::$app->request->post());


          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFinanceVendorOffsetMemoLines),
                    ActiveForm::validate($model)
                );
            }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceVendorOffsetMemoLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceVendorOffsetMemoLines as $modelFinanceVendorOffsetMemoLines) {

                        $modelFinanceVendorOffsetMemoLines->vendorOffsetMemoId = $model->id;
                        $modelFinanceVendorOffsetMemoLines->userId = $model->userId;
                        $modelFinanceVendorOffsetMemoLines->vendorOffsetMemoLineStatus = $model->vendorOffsetMemoStatus;
                        $modelFinanceVendorOffsetMemoLines->vendorId = $model->vendorId;

                          if (! ($flag = $modelFinanceVendorOffsetMemoLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }


        }

        return $this->render('create', [
            'model' => $model,
              'modelsFinanceVendorOffsetMemoLines' => (empty($modelsFinanceVendorOffsetMemoLines)) ? [new FinanceVendorOffsetMemoLines] : $modelsFinanceVendorOffsetMemoLines
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Updates an existing FinanceVendorOffsetMemo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {

      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;


        $modelsFinanceVendorOffsetMemoLines = $this->getFinanceVendorOffsetMemoLines($model->id);

        if ($model->load(Yii::$app->request->post())) {

              // code...

              //Geting the time created Timestamp
              // $model->customerCreateTime= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

              //Getting user Log in details
              // $model->userId= "1";

              //Generating Estimate Number

              // $estimateNum=$model->getEstnumber();
              // $model->estimateNumber= $estimateNum;
              $model->save();

              //after Saving the customer Details

              $oldIDs = ArrayHelper::map($modelsFinanceVendorOffsetMemoLines, 'id', 'id');
              $modelsFinanceVendorOffsetMemoLines = Model::createMultiple(FinanceVendorOffsetMemoLines::classname(), $modelsFinanceVendorOffsetMemoLines);
              Model::loadMultiple($modelsFinanceVendorOffsetMemoLines, Yii::$app->request->post());
              $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceVendorOffsetMemoLines, 'id', 'id')));

              // ajax validation
              if (Yii::$app->request->isAjax) {
                  Yii::$app->response->format = Response::FORMAT_JSON;
                  return ArrayHelper::merge(
                      ActiveForm::validateMultiple($modelsFinanceVendorOffsetMemoLines),
                      ActiveForm::validate($model)
                  );
              }

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceVendorOffsetMemoLines) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          if (! empty($deletedIDs)) {
                              FinanceVendorOffsetMemoLines::deleteAll(['id' => $deletedIDs]);
                          }
                          foreach ($modelsFinanceVendorOffsetMemoLines as $modelFinanceVendorOffsetMemoLines) {

                            $modelFinanceVendorOffsetMemoLines->vendorOffsetMemoId = $model->id;
                            $modelFinanceVendorOffsetMemoLines->userId = $model->userId;
                            $modelFinanceVendorOffsetMemoLines->vendorOffsetMemoLineStatus = $model->vendorOffsetMemoStatus;
                            $modelFinanceVendorOffsetMemoLines->vendorId = $model->vendorId;


                              if (! ($flag = $modelFinanceVendorOffsetMemoLines->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }
                  } catch (Exception $e) {
                      $transaction->rollBack();
                  }
              }

              // return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('update', [
              'model' => $model,
              'modelsFinanceVendorOffsetMemoLines' => (empty($modelsFinanceVendorOffsetMemoLines)) ? [new FinanceVendorOffsetMemoLines] : $modelsFinanceVendorOffsetMemoLines
          ]);
        }

    }



    public function getFinanceVendorOffsetMemoLines($id)
    {
      $model = FinanceVendorOffsetMemoLines::find()->where(['vendorOffsetMemoId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing FinanceVendorOffsetMemo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceVendorOffsetMemo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceVendorOffsetMemo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceVendorOffsetMemo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    }
