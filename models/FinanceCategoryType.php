<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_category_type".
 *
 * @property int $id
 * @property string $type
 * @property string $detailType
 * @property int $typeStatus
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 */
class FinanceCategoryType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_category_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'detailType', 'typeStatus', 'userId'], 'required'],
            [['typeStatus', 'userId'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['type'], 'string', 'max' => 150],
            [['detailType'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'detailType' => 'Detail Type',
            'typeStatus' => 'Type Status',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }
    /*Getting All the events from a customer*/
    public static function getcategorytypeAccountList($accountsType_id)
    {
        $accountcategorylisttype = self::find()
            ->select(['detailType as id' ,'detailType as name'])
            ->where(['type' => $accountsType_id])
            ->asArray()
            ->all();
        return $accountcategorylisttype;
    }
}
