<?php

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfromList */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Orderfrom Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-orderfrom-list-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'orderId',
            'itemid',
            'details:ntext',
            'outsourced',
            'vendorId',
            'quantity',
            'status',
            'createdAt',
            'updatedAt',
            'deletedAt',
            'userId',
            'approvedBy',
        ],
    ]) ?>

</div>
