<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfromList */

$this->title = 'Create Event Orderfrom List';
$this->params['breadcrumbs'][] = ['label' => 'Event Orderfrom Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-orderfrom-list-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
