-- ----------------------------
-- View structure for `invoice_balances`
-- ----------------------------

DROP VIEW IF EXISTS `invoice_balances`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `invoice_balances` AS

 SELECT
`all_transactions`.`transactionId` AS `transactionId`,
`all_transactions`.`referenceId` AS `referenceId`,
`all_transactions`.`referenceCode` AS `referenceCode`,
`all_transactions`.`recipientId` AS `recipientId`,
`all_transactions`.`recipientName` AS `recipientName`,
sum( `all_transactions`.`dr_amount` ) AS `Sum( all_transactions.dr_amount )`,
sum( `all_transactions`.`cr_amount` ) AS `Sum( all_transactions.cr_amount )`,
( sum( `all_transactions`.`cr_amount` ) - sum( `all_transactions`.`dr_amount` ) ) AS `owed`,
`finance_invoice`.`invoiceTxnDate` AS `invoiceTxnDate`
FROM
	( `all_transactions` LEFT JOIN `finance_invoice` ON ( ( `all_transactions`.`referenceId` = `finance_invoice`.`id` ) ) )
WHERE
	( ( `all_transactions`.`transactionCategory` = 'Revenue Payment' ) OR ( `all_transactions`.`transactionCategory` = 'Revenue' ) )
GROUP BY
	`all_transactions`.`referenceCode`;
