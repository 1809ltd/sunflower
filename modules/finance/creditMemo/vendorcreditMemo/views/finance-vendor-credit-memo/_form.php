<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;

use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*Bills*/
use app\modules\finance\expense\bills\models\FinanceBills;

use app\modules\finance\financesetup\tax\models\FinanceTax;

/* @var $this yii\web\View */
/* @var $model app\models\FinancevendorCreditMemo */
/* @var $form yii\widgets\ActiveForm */


$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
        jQuery(this).html("Module: " + (index + 1))
    });
});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
        jQuery(this).html("Module: " + (index + 1))
    });
});
';

$this->registerJs($js);

?>
<style>
.panel-body {
    padding-bottom: 0px;
}
</style>

<div class="box box-primary">

  <div class="box-body chart-responsive">

    <div class="col-md-12">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="box-body chart-responsive">
      <?= $form->field($model, 'creditMemoAccount')->hiddenInput(['value'=> "88"])->label(false);?>
      <?php
      // necessary for update action.
      if (! $model->isNewRecord) {
        // code...
        $status = ['0' => 'Cancel', '1' => 'Active', '2' => 'Waiting'];
        echo $form->field($model, 'vendorCreditMemoStatus')->dropDownList($status, ['readonly' => true,'prompt'=>'Select Status']);
      }else{
        echo $form->field($model, 'vendorCreditMemoStatus')->hiddenInput(['value'=> "2"])->label(false);
      }
      ?>
      <div class="col-sm-4">
        <div class="form-group">

            <?php $vendor = ArrayHelper::map(VendorCompanyDetails::find()->all(),'id','vendorCompanyName'); ?>
            <?= $form->field($model, 'vendorId')->widget(Select2::classname(), [
                'data' => $vendor,
                'language' => 'en',
                'options' => ['placeholder' => 'Select a Supplier ...',
                              'onChange'=>'$.post("index.php?r=finance-invoice/lists&customerId='.'"+$(this).val(),function(data){
                                $("select#billRef").html(data);
                              });'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <?= $form->field($model, 'refernumber')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <?= $form->field($model, 'vendorCreditMemoTxnDate')->widget(\yii\jui\DatePicker::class, [
            //'language' => 'ru',
            'options' => ['autocomplete'=>'off','class' => 'form-control'],
            'inline' => false,
            'dateFormat' => 'yyyy-MM-dd',
          ]); ?>
        </div>
      </div>
    </div>
    <div class="panel panel-default" >
        <div class="panel-body">
          <?php DynamicFormWidget::begin([
             'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
             'widgetBody' => '.container-items', // required: css class selector
             'widgetItem' => '.item', // required: css class
             // 'limit' => 80, // the maximum times, an element can be cloned (default 999)
             'min' => 1, // 0 or 1 (default 1)
             'insertButton' => '.add-item', // css class
             'deleteButton' => '.remove-item', // css class
             'model' => $modelsFinanceVendorCreditMemoLines[0],
             'formId' => 'dynamic-form',
             'formFields' => [
                 'invoiceNumber',
                 'vendorCreditMemoLineDescription',
                 'vendorCreditMemoLineAmount',
                 //'vendorCreditMemoLineStatus',

             ],
         ]); ?>

        <div class="container-items"><!-- widgetContainer -->
            <div class="caption font-red-sunglo" style="font-size: 15px;">
                <i class="fa fa-lock font-red-sunglo" ></i>
                <span class="caption-subject bold uppercase" > Invoice Items</span>
            </div><br />
            <!-- Loopping Items in the Lists -->
          <?php foreach ($modelsFinanceVendorCreditMemoLines as $i => $modelFinanceVendorCreditMemoLines): ?>

            <?php
            // foreach ($modelUserRoleModulePermissions as $i => $modelUserRoleModulePermission): ?>
                <div class="item"><!-- widgetBody -->

                    <div>
                      <?php
                          // necessary for update action.
                          if (! $modelFinanceVendorCreditMemoLines->isNewRecord) {
                              echo Html::activeHiddenInput($modelFinanceVendorCreditMemoLines, "[{$i}]id");
                          }
                      ?>

                      <div class="row box" style="float: center;width: 100%;">
                          <div class="box-header">
                            <h3 class="box-title"></h3>
                          </div>
                          <!-- /.box-header -->
                          <div class="pull-right">
                              <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                              <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                          </div>
                          <div class="pull-right">

                          </div>
                          <div class= "box-body">
                            <div class="row">
                              <div class="col-sm-3">
                                <div class="form-group">
                                    <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]billRef")->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(FinanceBills::find()->all(),'id','billRef'),
                                        'language' => 'en',
                                        'class' => 'billRef form-control',
                                        'options' => ['placeholder' => 'Select a Invoice ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]); ?>
                                </div>
                              </div>
                              <?php
                              //  $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]itemRefName")->hiddenInput([
                              // 'readonly' => true,
                              // // 'maxlength' => true,'autocomplete'=>"off",
                              // 'class' => 'prodName form-control'])->label(false);?>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]vendorCreditMemoLineDescription")->textarea(['rows' => 6]) ?>
                                </div>
                              </div><div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]vendorCreditMemoLineAmount")->textInput([
                                  'maxlength' => true,
                                  'class' => 'lineamount form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">

                                    <?php $TaxCodeRef =  ArrayHelper::map(FinanceTax::find()->andWhere(['taxStatus'=>1])->asArray()->all(),'taxRate','taxCode'); ?>
                                    <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]creditTaxCodeRef")->dropDownList($TaxCodeRef, [
                                                    'prompt'=>'Select...',
                                                    'class' => 'taxcode form-control']); ?>
                                </div>
                              </div>

                            </div>

                            <!--  New row-->
                            <div class= "row">
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]taxamount")->textInput([
                                    'readonly' => true,
                                    'maxlength' => true,
                                    'class' => 'sumTaxPart form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">

                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">

                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">

                                </div>
                              </div>

                        </div>
                      </div>

                        </div>
                        <!-- <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div> -->
                    </div>

                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>



    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'vendorCreditMemoNote')->textarea(['rows' => 6]) ?>

        </div>
      </div>

      <div class="col-sm-3">
        <div class="form-group">
          <?= $form->field($model, 'totalTax')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
        </div>
      </div>

      <div class="col-sm-3">
        <div class="form-group">

            <?= $form->field($model, 'creditAmount')->textInput(['readonly' => true,'class' => 'total sum  form-control']) ?>
        </div>
      </div>

      <?= $form->field($model, 'approvedby')->hiddenInput(['value'=> ""])->label(false);?>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    </div>

  </div>

</div>
<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
var getAmount = function() {

    //getting the elemetent ID
    var items = $(".item");

    //intialization on amount figure and the total figure
    var amount = 0;
    var total = 0;
    var finaltotal=0;
    var taxation=0;
    var tax=0

    items.each(function (index, elem) {

        //getting specific elements
        var lineamount = $(elem).find(".lineamount").val();
        var price = $(elem).find(".price").val();
        var packs = $(elem).find(".packs").val();
        var taxcode = $(elem).find(".taxcode").val();

        //Check if lineamount and price are numeric or something like that
        amount = parseFloat(lineamount) * parseFloat(price) * parseFloat(packs);

        //Assign the amount value to the field
        $(elem).find(".amount").val(amount);

        var amountValue = $(elem).find(".lineamount").val();

        taxation= parseFloat(lineamount)*parseFloat(taxcode);

        // alert(taxation);

        //Assign the Taxation value to the field
        $(elem).find(".sumTaxPart").val(taxation);

        //getting the tax amount figures
        var taxValue = $(elem).find(".sumTaxPart").val();

        //getting total tax amountValue
        tax = parseFloat(tax) + parseFloat(taxValue);

        //assing value to the total tax amount
        $(".tax").val(tax);

        //getting the total of all figures
        total = parseFloat(total) + parseFloat(amountValue);

        //Getting the total value of the credit Memo
        finaltotal= total+tax;


        //assing value to the total amount
        $(".total").val(total);

    });
};

//Bind new elements to support the function too
$(".container-items").on("change", function() {
    getAmount();
});
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   $('#Roles').addClass('active open');
   $('#role').addClass('active open');"
, View::POS_READY);
?>
