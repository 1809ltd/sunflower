<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AssetCategoryType */

$this->title = 'Create Asset Category Type';
$this->params['breadcrumbs'][] = ['label' => 'Asset Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-category-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
