<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userroletypes\models\UserRoleTypes */

$this->title = $model->roleName;
$this->params['breadcrumbs'][] = ['label' => 'User Role Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<style>.summary{display: none;}</style>
<div class="box box-primary">

  <div class="box-body chart-responsive">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'roleName',
            [
                'attribute'=>'status',
                'value'=>$model->status ? 'Yes' : 'No',
            ],
        ],
    ]) ?>

  </div>

</div>

<div class="box box-primary">

  <div class="box-body chart-responsive">

    <div class="caption font-red-sunglo" style="font-size: 15px;">
        <i class="fa fa-lock font-red-sunglo"></i>
        <span class="caption-subject bold uppercase"> Modules Permissions</span>
    </div><br />

    <?= GridView::widget([
        'dataProvider' => $modelRolePermissions,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'module.moduleName',
            //'role.role_name',
            [
              // Url::to('@web/yesno/rsz_thumbsup.png')

              // src="<?=Url::to('@web/adminlte/dist/img/user2-160x160.jpg') "

                'attribute' => 'new',
                'format' => 'html',
                'value' =>  function($data) { return $data->new ?
                  '<img src ="'.Url::to('@web/yesno/rsz_thumbsup.png').'" height="25"' . '>' :'<img src ="'.Url::to('@web/yesno/rsz_thumbsdown.png').'" height="15"' . '>'; }
            ],
            [
                'attribute' => 'view',
                'format' => 'html',
                'value' =>  function($data) { return $data->view ? ' <img src ="'.Url::to('@web/yesno/rsz_thumbsup.png').'" height="25"' . '>' : ' <img src ="'.Url::to('@web/yesno/rsz_thumbsdown.png').'" height="25"' . '>'; }
            ],
            [
                'attribute' => 'save',
                'format' => 'html',
                'value' =>  function($data) { return $data->save ? ' <img src ="'.Url::to('@web/yesno/rsz_thumbsup.png').'" height="25"' . '>' : ' <img src ="'.Url::to('@web/yesno/rsz_thumbsdown.png').'" height="25"' . '>'; }
            ],
            [
                'attribute' => 'remove',
                'format' => 'html',
                'value' =>  function($data) { return $data->remove ? ' <img src ="'.Url::to('@web/yesno/rsz_thumbsup.png').'" height="25"' . '>' : ' <img src ="'.Url::to('@web/yesno/rsz_thumbsdown.png').'" height="25"' . '>'; }
            ],
            [
                'attribute' => 'other',
                'format' => 'html',
                'value' =>  function($data) { return $data->other ? ' <img src ="'.Url::to('@web/yesno/rsz_thumbsup.png').'" height="25"' . '>' : ' <img src ="'.Url::to('@web/yesno/rsz_thumbsdown.png').'" height="25"' . '>'; }
            ],
        ],
    ]); ?>

    </div>

  </div>


<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   $('#Roles').addClass('active open');
   $('#role').addClass('active open');
", View::POS_READY);
?>
