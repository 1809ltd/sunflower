<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoice */

$this->title = 'Convert to Invoice';
$this->params['breadcrumbs'][] = ['label' => 'Finance Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-invoice-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceInvoiceLines'=>$modelsFinanceInvoiceLines, //to enable Dynamic Form

    ]) ?>

</div>
