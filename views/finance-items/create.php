<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceItems */

$this->title = 'Create Finance Items';
$this->params['breadcrumbs'][] = ['label' => 'Finance Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-items-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
