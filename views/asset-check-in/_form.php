<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;

use kartik\depdrop\DepDrop;

/*Array*/
use yii\helpers\ArrayHelper;
use app\models\AssetRegistration;
use app\models\AssetCheckOut;
use app\models\CompanySiteLocation;
use app\models\CompanyDepartment;

use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\AssetCheckIn */
/* @var $form yii\widgets\ActiveForm */
?>

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
  </div>
  <?php $form = ActiveForm::begin(); ?>

  <div class="modal-body asset-check-in-form">

    <?= $form->field($model, 'checkOutId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AssetCheckOut::find()->all(),'id','id'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Check out ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?= $form->field($model, 'assetId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AssetRegistration::find()->all(),'id','assetName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Asset ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?= $form->field($model, 'returnDate')->widget(\yii\jui\DatePicker::class, [
          //'language' => 'ru',
          'options' => ['class' => 'form-control'],
          'inline' => false,
          'dateFormat' => 'yyyy-MM-dd',
      ]) ?>

    <?= $form->field($model, 'lcationId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CompanySiteLocation::find()->all(),'id','locationName'),
        'language' => 'en',
        'id'=>'location',
        'options' => ['placeholder' => 'Select a Asset Location ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
    <?php $status = ['1' => 'Available', '2' => 'Moved', '3' => 'Event', '4' => 'Suspended']; ?>
    <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']); ?>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

  </div>
  <?php ActiveForm::end(); ?>
