<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-vendor-offset-memo-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vendorId')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoId')->textInput() ?>

    <?= $form->field($model, 'billRef')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoLineDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'vendorOffsetMemoLineAmount')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoLineStatus')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoLineCreatedAt')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoLineUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'creidtDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
