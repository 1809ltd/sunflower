<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoiceLines */

$this->title = 'Create Finance Invoice Lines';
$this->params['breadcrumbs'][] = ['label' => 'Finance Invoice Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-invoice-lines-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
