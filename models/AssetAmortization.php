<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asset_amortization".
 *
 * @property int $id
 * @property int $assetId
 * @property int $repayment this is the value of depreciation / appretiation of an asset
 * @property double $interestAmount this is the value of depreciation of the asset
 * @property double $principalAmount
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $status
 * @property int $userId
 *
 * @property AssetRegistration $asset
 * @property UserDetails $user
 */
class AssetAmortization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_amortization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'repayment', 'interestAmount', 'principalAmount', 'status', 'userId'], 'required'],
            [['assetId', 'repayment', 'status', 'userId'], 'integer'],
            [['interestAmount', 'principalAmount'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetId' => 'Asset ID',
            'repayment' => 'Repayment',
            'interestAmount' => 'Interest Amount',
            'principalAmount' => 'Principal Amount',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
