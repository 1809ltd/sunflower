<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Customer Statement';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <div class="customerreport-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<!-- Main content -->
<!-- Search Date And the rest -->
<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label class="col-lg-4 control-label">Customer: </label>
          <div class="col-lg-8">
            <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(CustomerDetails::find()->where('`customerstatus` = 1')->asArray()->all(),'id','customerDisplayName'),
                  'language' => 'en',
                  'options' => ['autocomplete'=>'off','id'=> 'customer-id', 'placeholder' => 'Select a Company ...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ])->label(false); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date From: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
              //'language' => 'ru',
              'options' => ['autocomplete'=>'off','class' => 'form-control'],
              'inline' => false,
              'dateFormat' => 'yyyy-MM-dd',
              ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date To: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
              //'language' => 'ru',
              'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <div class="col-lg-8 col-lg-offset-4">
            <div class="center-align">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
            </div>
          </div>
        </div>
      </div>
    </div>
      <?php ActiveForm::end(); ?>
  </div>
</div>
<?php
if (!empty($customer_statement)) {
  // code...
// print_r($customer);
// echo $customer[0]["customerCompanyName"];

// die();
  ?>
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title"><?= $customer[0]["customerCompanyName"] ?></h3>
      <h5 class="box-title">Reporting period:  <?= Yii::$app->formatter->asDate($startDate, 'long');?> to <?= Yii::$app->formatter->asDate($endDate, 'long');?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>

     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <?php

              ?>
             <th class="page-header" style="text-align: center;width: 85%;">STATEMENT OF ACCOUNT: <?php
             // Yii::$app->formatter->asDate($tarehe, 'long');?></th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>
     <!-- info row -->
     <div class="row invoice-info">
     </div>
     <!-- /.row -->
     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Date:</th>
             <th>Transaction No:</th>
             <th>Details:</th>
             <th class="text-right">Amount Invoiced: (Ksh)</th>
             <th class="text-right">Amount Paid: (Ksh)</th>
             <th class="text-right">Balance (Ksh)</th>
           </thead>
             <tbody>

               <?php

                ?>

               <tr>
                 <td> Balance brought forward as at<?= Yii::$app->formatter->asDate($startDate, 'long');?></td>
                 <td> </td>
                 <td>  </td>
                 <td> </td>
                 <td></td>
                <td class="text-right"><?= number_format($balbroughtforward,2) ;?>
                </td>
              </tr>
               <?php
               // Get all the specific Invoice

               $SubamountPaid=0;
               $SubamountInvoiced=0;
               $SubBal=$balbroughtforward;

               foreach ($customer_statement as $customer_statement) {
                 // code...
                 $SubamountPaid+=$customer_statement->amountPaid;
                 $SubamountInvoiced+=$customer_statement->amountinvoiced;
                 $SubBal = $customer_statement->amountinvoiced-$customer_statement->amountPaid-$SubBal;
                 ?>
                 <tr>
                   <td><?= Yii::$app->formatter->asDate($customer_statement["date"], 'long');?></td>
                   <td><?= $customer_statement->transactionNo;?></td>
                   <td><?= $customer_statement->Details;?> </td>
                   <td class="text-right">
                     <?php
                     $amountInvoicedamount = preg_replace(
                        '/(-)([\d\.\,]+)/ui',
                        '($2)',
                        number_format($customer_statement->amountinvoiced,2,'.',',')
                    );
                    echo $amountInvoicedamount;
                     ?>
                   </td>
                   <td class="text-right">
                     <?php
                     $amountPaidamount = preg_replace(
                        '/(-)([\d\.\,]+)/ui',
                        '($2)',
                        number_format($customer_statement->amountPaid,2,'.',',')
                    );
                    echo $amountPaidamount;
                     ?>
                   </td>
                  <td class="text-right">
                    <?php
                    $amountSubBal = preg_replace(
                       '/(-)([\d\.\,]+)/ui',
                       '($2)',
                       number_format($SubBal,2,'.',',')
                   );
                   echo $amountSubBal;
                    ?>

                  </td>
                </tr>
                <?php
              }
              ?>
            </tbody>
              <th>Total:</th>
              <th></th>
              <th></th>
              <th class="text-right">
                <?php
                $SubamountInvoicedamount = preg_replace(
                   '/(-)([\d\.\,]+)/ui',
                   '($2)',
                   number_format($SubamountInvoiced,2,'.',',')
               );

                echo $SubamountInvoicedamount;

                ?>
              </th>
              <th class="text-right"><?php
            $SubamountPaidamount = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($SubamountPaid,2,'.',',')
           );

            echo $SubamountPaidamount;

            ?> </th>
              <th class="text-right"><?php
              $SubBalamount = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($SubBal,2,'.',',')
             );

              echo $SubBalamount;

              ?>
            </th>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <br>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
            CHEQUES SHOULD BE MADE PAYABLE TO SUNFLOWER TENTS & DÉCOR LTD
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
            <?php
            // $model->invoiceFooter; ?>
          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- /.row -->
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
              class="btn btn-xs btn-success m-b-10"><i
                   class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
        </button> -->
        </div>
      </div>
    </section>
    <!-- /.content -->
    <?php
  } else {
    // code...

    echo "No Invoices were found Here";
  }
  ?>
