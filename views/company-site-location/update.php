<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompanySiteLocation */

$this->title = 'Update Company Site Location: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Company Site Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-site-location-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
