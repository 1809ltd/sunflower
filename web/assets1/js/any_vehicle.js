"use strict";
var ass = function () {
    "use strict";
    $('#model_list').bootstrapDualListbox();
};
var Assign = function () {
    "use strict";
    return {
        init: function () {
            ass();
        }
    }
}();
$(function () {
    Assign.init();
});