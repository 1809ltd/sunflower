<?php

namespace app\modules\finance\financereport\customerreport\controllers;

use Yii;
use yii\web\Controller;
// Customer Statements
use app\modules\finance\financereport\customerreport\models\CustomerStatement;
// Customer Details
use app\modules\customer\customerdetails\models\CustomerDetails;

/**
 * Default controller for the `customerreport` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = new \yii\base\DynamicModel([
        'startDate','endDate','customerId'
      ]);
      $model->addRule(['startDate','endDate','customerId'], 'required')
            ->validate();

      if($model->load(Yii::$app->request->post())){
        // do somenthing with model

        // $tarehe1= $model->tarehe;
        // $date = $model->tarehe;
        // $date = explode('-', $date);

        $customer_details = CustomerDetails::find()
                                              ->Where('id = :customerId', [':customerId' => $model->customerId])
                                              // ->andWhere(['between', 'date', $model->startDate, $model->endDate])
                                              // ->orderBy([
                                              //             'date' => SORT_ASC,
                                              //             // 'name' => SORT_DESC,
                                              //         ])
                                              ->all();

        $amountpaid= CustomerStatement::find()->Where('customerId = :customerId', [':customerId' => $model->customerId])
                                              ->andWhere(['<', 'date', $model->startDate])
                                              ->sum('amountPaid');

        $amountinvoiced= CustomerStatement::find()->Where('customerId = :customerId', [':customerId' => $model->customerId])
                                              ->andWhere(['<', 'date', $model->startDate])
                                              ->sum('amountinvoiced');

        $balbroughtforward= $amountinvoiced-$amountpaid;

        $customer_statement = CustomerStatement::find()
        ->Where('customerId = :customerId', [':customerId' => $model->customerId])
        ->andWhere(['between', 'date', $model->startDate, $model->endDate])
        ->orderBy([
                    'date' => SORT_ASC,
                    // 'name' => SORT_DESC,
                ])
        ->all();

        if (!empty($customer_statement)) {
          // code...
          return $this->render('index', [
              'customer_statement' => $customer_statement,
              'customer' => $customer_details,
              'startDate' => $model->startDate,
              'endDate' => $model->endDate,
              'balbroughtforward' => $balbroughtforward,
              'model' => $model,
          ]);

        } else {
          // code...
          return $this->render('index', [
              'model' => $model,
              // 'dataProvider' => $dataProvider,
              // 'events'=> $shereheglobal,
          ]);

        }


      }
      return $this->render('index', [
          'model' => $model,
          // 'dataProvider' => $dataProvider,
          // 'events'=> $shereheglobal,
      ]);

        // return $this->render('index');
    }

}
