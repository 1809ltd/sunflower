<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-estimate-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'estimateNumber') ?>

    <?= $form->field($model, 'customerId') ?>

    <?= $form->field($model, 'localpurchaseOrder') ?>

    <?= $form->field($model, 'estimateProjectClassification') ?>

    <?php // echo $form->field($model, 'estimateProjectId') ?>

    <?php // echo $form->field($model, 'estimateProjectName') ?>

    <?php // echo $form->field($model, 'estimateTxnDate') ?>

    <?php // echo $form->field($model, 'estimateDeadlineDate') ?>

    <?php // echo $form->field($model, 'estimateNote') ?>

    <?php // echo $form->field($model, 'estimateFooter') ?>

    <?php // echo $form->field($model, 'estimateTxnStatus') ?>

    <?php // echo $form->field($model, 'estimateEmailStatus') ?>

    <?php // echo $form->field($model, 'estimateBillEmail') ?>

    <?php // echo $form->field($model, 'estimateTaxAmount') ?>

    <?php // echo $form->field($model, 'estimateDiscountAmount') ?>

    <?php // echo $form->field($model, 'estimateSubAmount') ?>

    <?php // echo $form->field($model, 'estimateAmount') ?>

    <?php // echo $form->field($model, 'estimatedCreatedby') ?>

    <?php // echo $form->field($model, 'estimatedCreatedAt') ?>

    <?php // echo $form->field($model, 'estimatedUpdatedAt') ?>

    <?php // echo $form->field($model, 'estimatedDeletedAt') ?>

    <?php // echo $form->field($model, 'approvedby') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
