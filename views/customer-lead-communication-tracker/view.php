<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLeadCommunicationTracker */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Lead Communication Trackers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-lead-communication-tracker-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'leadAppointId',
            'leadId',
            'commLeadTxnDate',
            'commLeadMeetingSubject:ntext',
            'commLeadPartiesInvolved:ntext',
            'commLeadFormOfCommunication:ntext',
            'commLeadNoted:ntext',
            'commLeadNextDate',
            'commLeadCreatedAt',
            'commLeadLastUpdatedAt',
            'commLeadDeletedAt',
            'userId',
        ],
    ]) ?>

</div>
