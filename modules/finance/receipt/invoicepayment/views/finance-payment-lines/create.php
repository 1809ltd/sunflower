<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines */

$this->title = Yii::t('app', 'Create Finance Payment Lines');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Payment Lines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-payment-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
