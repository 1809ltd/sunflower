  <?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use app\models\EventTransaction;
use app\models\EventOrderfrom;


$eventTransactions=  EventTransaction::find()->select(['*'])
                      ->where(['eventId' => $eventId])->all();

$this->title = $eventTransactions[0]["eventNumber"];
$this->params['breadcrumbs'][] = ['label' => 'Event Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date:  <?= date('Y-m-d');?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         From
         <address>
           <strong>SUNFLOWER EVENTS,</strong><br/>
            Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
           Nairobi, 00100 Kenya<br/>
           Phone: +254 (0) 722 790632<br/>
           Email: info@...com
         </address>
       </div>
       <!-- /.col -->

       <?php $eventinfo=EventOrderfrom::geteventDetailss($eventId); ?>
       <?php foreach ($eventinfo as $eventinfo): ?>

       <div class="col-sm-4 invoice-col">

         To
         <address>

           <?php $customerinfo= EventOrderfrom::getCustomerDetails($eventinfo->customerId);

           // print_r($customerinfo);

           foreach ($customerinfo as $customerinfo) {
             // code...

           ?>
           <strong><?= $customerinfo->customerDisplayName;?></strong><br/>
           Email: <?= $customerinfo->customerPrimaryEmail;?><br/>
           <?= $customerinfo->customercity;?>, <?= $customerinfo->customerPostalCode;?><br/>
           Phone: <?= $customerinfo->customerPrimaryPhone;?><br/>

             <?php

           }

           ?>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">



           <b>Event Number: #<?=$eventTransactions[0]["eventNumber"];;?></b><br>
           <br>
           <b>Event: </b> <?= $eventinfo->eventName;?><br>
           <b>Type: <?= $eventinfo->eventCategory;?><br>
           <b>Theme: <?= $eventinfo->eventTheme;?><br/>
           <b>Event Date : <?= $eventinfo->eventStartDate;?><br/>
           <b>Set Up: <?= $eventinfo->eventSetUpDateTime;?><br/>


       </div>
        <?php endforeach; ?>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Transactions</th>
             <th>Dr</th>
             <th>Cr</th>
             <th>Net</th>
           </thead>

           <tbody>

             <?php

             $net=0;

             foreach ($eventTransactions as $eventTransaction) {
               // code...
               // print_r($eventTransaction);

               if (empty($eventTransaction->dr_amount)) {
                 // code...
                 $eventTransaction->dr_amount=0;
               }
               if (empty($eventTransaction->cr_amount)) {
                 // code...
                 $eventTransaction->cr_amount=0;
               }

               $net = $net+$eventTransaction->dr_amount-$eventTransaction->cr_amount;

               ?>
               <tr>
                 <td> Transcation Code: <?= $eventTransaction->transactionNumber;?>.

                 <br/>
                   <small> Description:  <?= $eventTransaction->transName;?>.
                   </small>

                   <br/>
                 </td>
                 <td><?= number_format($eventTransaction->dr_amount,2) ;?></td>
                 <td><?= number_format($eventTransaction->cr_amount,2) ;?></td>
                 <td><?= number_format($net,2) ;?></td>
               </tr>

               <?php

               // die();
             }

              ?>



              <tr>
                  <td>
                       <br/>
                      <small>.
                      </small>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>

              </tr>


           </tbody>


         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           *
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>


         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
