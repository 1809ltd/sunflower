<?php

namespace app\modules\customer\marketing;

/**
 * marketing module definition class
 */
class marketing extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\customer\marketing\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Customer Lead details Sub Module*/
          'customerlead' => [
                'class' => 'app\modules\customer\marketing\customerlead\customerlead',
            ],
          /*Appointment*/
          'customerleadappointment' => [
              'class' => 'app\modules\customer\marketing\customerleadappointment\customerleadappointment',
          ],
          'customerleadcommunicationtracker' => [
              'class' => 'app\modules\customer\marketing\customerleadcommunicationtracker\customerleadcommunicationtracker',
          ],
        ];


        // custom initialization code goes here
    }
}
