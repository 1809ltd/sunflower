<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemo */

$this->title = 'Create Finance Vendor Credit Memo';
$this->params['breadcrumbs'][] = ['label' => 'Vendor Credit Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-credit-memo-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceVendorCreditMemoLines'=>$modelsFinanceVendorCreditMemoLines, //to enable Dynamic Form
    ]) ?>

</div>
