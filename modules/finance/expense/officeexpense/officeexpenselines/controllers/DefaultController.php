<?php

namespace app\modules\finance\expense\officeexpense\officeexpenselines\controllers;

use yii\web\Controller;

/**
 * Default controller for the `officeexpenselines` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
