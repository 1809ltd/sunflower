<?php

namespace app\modules\finance\expensepayment\billpayment\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\finance\expense\bills\models\FinanceBills;
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments;

use Yii;

/**
 * This is the model class for table "finance_bills_payments_lines".
 *
 * @property int $id
 * @property int $billpaymentId the main payment cheque
 * @property int $billId invoice and bill number
 * @property int $vendorId
 * @property string $referenceNo
 * @property double $amount
 * @property int $status
 * @property string $deletedAt
 * @property string $created_at
 * @property string $updated_at
 * @property int $userId
 *
 * @property FinanceBillsPayments $billpayment
 * @property FinanceBills $bill
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 */
class FinanceBillsPaymentsLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_bills_payments_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billpaymentId', 'billId', 'vendorId', 'status', 'userId'], 'integer'],
            [['amount', 'status', 'userId'], 'required'],
            [['amount'], 'number'],
            [['deletedAt', 'created_at', 'updated_at'], 'safe'],
            [['referenceNo'], 'string', 'max' => 100],
            [['billpaymentId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBillsPayments::className(), 'targetAttribute' => ['billpaymentId' => 'id']],
            [['billId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBills::className(), 'targetAttribute' => ['billId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'billpaymentId' => 'Billpayment ID',
            'billId' => 'Invoice No:',
            'vendorId' => 'Vendor',
            'referenceNo' => 'Reference No',
            'amount' => 'Amount',
            'status' => 'Status',
            'deletedAt' => 'Deleted At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillpayment()
    {
        return $this->hasOne(FinanceBillsPayments::className(), ['id' => 'billpaymentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(FinanceBills::className(), ['id' => 'billId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
    /*Getting All the Invoices from a Vendor*/
    public static function getVendorBills($vendor_id)
    {
        $bills = FinanceBills::find()
            ->select(['id','billRef as name'])
            ->where(['vendorId' => $vendor_id])
            ->asArray()
            ->all();
        return $bills;
    }

}
