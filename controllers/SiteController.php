<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

/*Get Event Details*/

use app\models\EventDetails;
use app\models\EventDetailsSearch;

/*End Event Details*/

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $events= EventDetails::find()->all();
        $shereheglobal=[];

        foreach ($events as $sherehe) {
          // code...
          $event = new \yii2fullcalendar\models\Event();
          $event->id = $sherehe->id;
          // $event->title = 'Testing';
          $event->title =$sherehe->eventName;
          // $event->start = date('Y-m-d\Th:m:s\Z',strtotime('tomorrow 6am'));
          // date('Y-m-d\Th:i:s\Z',strtotime($time->date_start.' '.$time->time_start));
          // $event->start = $sherehe->eventStartDate;
          $event->start = date('Y-m-d\Th:i:s\Z',strtotime($sherehe->eventStartDate));
          $event->end = date('Y-m-d\Th:i:s\Z',strtotime($sherehe->eventEndDate));
          $shereheglobal[] = $event;
        }

        return $this->render('index', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            'events'=> $shereheglobal,
        ]);

        // return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout='loginlayout';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        // return $this->goHome();

        $this->layout='loginlayout';

        $model = new LoginForm();
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    // Permission details
    public function actionPermission()
    {
      // code...
      $this->layout='addformdatatablelayout';
      return $this->render('denied');
    }

}
