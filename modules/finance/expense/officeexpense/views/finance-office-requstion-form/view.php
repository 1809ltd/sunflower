<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use app\models\FinanceInvoiceLines;
use app\models\FinanceInvoiceLinesSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOfficeRequstionForm */

$this->title = $model->requstionNumber;
$this->params['breadcrumbs'][] = ['label' => 'Office Requisition Form', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*All Payment for this invoice, i.e offset,creditnote,actualpayment*/
$payments=$model->allPayments($model->id);

// echo $payments;

?>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?=$model->date;?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>
           <strong>SUNFLOWER EVENTS,</strong><br/>
            Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
           Nairobi, 00100 Kenya<br/>
           Phone: +254 (0) 722 790632<br/>
           Email: info@...com
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>


         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Office Req:  #<?=$model->requstionNumber;?></b><br>
         <br>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
           <tr>
               <th>Office Expenses</th>
               <th class="text-right">Quantity</th>
               <th class="text-right">Price</th>
               <th class="text-right">Amount(Ksh)</th>
           </tr>
           </thead>
           <tbody>
             <?php
             $FinanceOfficeRequstionFormLists=$model->getFinanceOfficeRequstionFormListItems($model->id,$model->requstionStatus);



             $subtotal=0;

             foreach ($FinanceOfficeRequstionFormLists as $FinanceOfficeRequstionFormList) {
               // code...


               $subtotal=$subtotal+$FinanceOfficeRequstionFormList->amont;

               $FinanceOfficeRequstionFormList->activityId;
               $FinanceOfficeRequstionFormList->personelId;
               $FinanceOfficeRequstionFormList->personnelTel;
               $FinanceOfficeRequstionFormList->description;
               $FinanceOfficeRequstionFormList->qty;
               $FinanceOfficeRequstionFormList->unitprice;
               $FinanceOfficeRequstionFormList->amont;

               ?>
               <tr>
                   <td>
                     <?php
                       $FinanceOfficeRequstionFormListsactivityName=$model->getActivity($FinanceOfficeRequstionFormList->activityId);
                     ?>
                     <?php foreach ($FinanceOfficeRequstionFormListsactivityName as $FinanceOfficeRequstionFormListsactivityName){

                       echo $FinanceOfficeRequstionFormListsactivityName->activityName;
                     }
                     ?><br/>
                       <small><?= $FinanceOfficeRequstionFormList->description;?>.
                       </small>
                   </td>
                   <td class="text-right"><?= number_format($FinanceOfficeRequstionFormList->qty,2);?></td>
                   <td class="text-right"><?= number_format($FinanceOfficeRequstionFormList->unitprice,2);?></td>
                   <td class="text-right"><?= number_format($FinanceOfficeRequstionFormList->amont,2);?></td>
               </tr>

               <?php

             }

              ?>

              <tr>
                  <td>
                       <br/>
                      <small>.
                      </small>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>

              </tr>


           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Office Requistion Note:</p>


         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           * <?= $model->requstionNote; ?><br/>
         </p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Prepared by:</th>
               <td>Ksh <?= $model->preparedBy; ?></td>
             </tr>
             <tr>
               <th>Requested by:</th>
               <td>Ksh <?= $model->amount; ?></td>
             </tr>
             <tr>
               <th>Approved By:</th>
               <td>Ksh <?= $model->approvedBy; ?></td>
             </tr>
           </table>
         </div>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <?php $amountdue=$subtotal-$payments;?>
         <p class="lead">Amount Due <strong> Ksh <?= number_format($amountdue,2) ;?></strong></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Total:</th>
               <td class="text-right">Ksh <?= number_format($subtotal,2); ?></td>
             </tr>
             <tr>
               <th>Total Payment:</th>
               <td class="text-right"><?= number_format($payments,2) ;?></td>
             </tr>
           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- this row will not appear when printing -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
                  class="btn btn-xs btn-success m-b-10"><i
                       class="fa fa-print m-r-5"></i> Print</a>
       </div>
     </div>
   </section>
   <!-- /.content -->
