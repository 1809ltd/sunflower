<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEventRequstionFormListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Event Requstion Form Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-requstion-form-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Event Requstion Form List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'reqId',
            'activityId',
            'personelId',
            'workplanId',
            //'personnelTel',
            //'description:ntext',
            //'qty',
            //'unitprice',
            //'amont',
            //'status',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
