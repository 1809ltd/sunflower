<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\event\transportation\models\EventTransport */

$this->title = 'Create Event Transport';
$this->params['breadcrumbs'][] = ['label' => 'Event Transports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-transport-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
