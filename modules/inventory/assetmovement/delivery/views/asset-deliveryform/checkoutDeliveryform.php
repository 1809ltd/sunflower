<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform */
/* @var $form yii\widgets\ActiveForm */
// echo \Yii::$app->session->get('eventId');
?>

<div class="asset-deliveryform-form">

    <?php $form = ActiveForm::begin(['action' => ['checkout'],'options' => ['method' => 'post']]); ?>
  
    <?php echo $form->field($model, 'eventId')->hiddenInput(['value'=>$_SESSION['eventId']])->label(false);?>
    <?php echo $form->field($model, 'orderId')->hiddenInput(['value'=>$_SESSION['orderId']])->label(false);?>
    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                'inline' => false,
                'dateFormat' => 'yyyy-MM-dd',

            ]); ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <?= $form->field($model, 'duedate')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                'inline' => false,
                'dateFormat' => 'yyyy-MM-dd',
            ]); ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
            <?= $form->field($model, 'delivery')->textarea(['rows' => 3]) ?>
        </div>
      </div>
      <?= $form->field($model, 'deliveryStatus')->hiddenInput(['value'=>1])->label(false);?>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Checkout', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
