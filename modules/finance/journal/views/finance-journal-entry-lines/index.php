<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\journal\models\FinanceJournalEntryLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Journal Entry Lines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-journal-entry-lines-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Journal Entry Lines', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'journalId',
            'accountId',
            'transactionId',
            'postingType',
            //'description:ntext',
            //'drAmount',
            //'crAmount',
            //'createAt',
            //'updatedAt',
            //'deletedAt',
            //'status',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
