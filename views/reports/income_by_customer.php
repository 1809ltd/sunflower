<?php

use yii\helpers\Html;
use app\models\Customerincome;

/* @var $this yii\web\View */

?>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?= date('Y-m-d'); ?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>
           <strong>SUNFLOWER EVENTS,</strong><br/>
            Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
           Nairobi, 00100 Kenya<br/>
           Phone: +254 (0) 722 790632<br/>
           Email: info@...com
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>


         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>As of: <?= date('Y-m-d H:i:s'); ?></b><br>
         <br>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-hover">
             <thead>
               <tr>
                   <th style="width:75%">Customer</th>
                   <th>Income</th>
               </tr>
             </thead>
           <tbody>
             <?php

             $connection = Yii::$app->getDb();
             $command1 = $connection->createCommand("SELECT
                                                  	customerincome.customerId,
                                                  	customerincome.customerNumber,
                                                  	customerincome.customerKraPin,
                                                  	customerincome.customerDisplayName,
                                                  	Sum( customerincome.invoiceLinesAmount ),
                                                  	customerincome.invoiceLinesTaxamount,
                                                  	customerincome.invoiceLinesTaxCodeRef,
                                                  	customerincome.itemRefId,
                                                  	customerincome.itemRefName,
                                                  	customerincome.invoiceId,
                                                  	customerincome.invoicelineId,
                                                  	customerincome.invoiceLinesDescription,
                                                  	customerincome.invoiceLinesQty,
                                                  	customerincome.invoiceLinesUnitPrice,
                                                  	customerincome.invoiceLinesCreatedAt,
                                                  	customerincome.invoiceLinesDiscount,
                                                  	customerincome.txnDate,
                                                  	customerincome.invoiceLinesStatus,
                                                  	customerincome.eventNumber,
                                                  	customerincome.eventCatName,
                                                  	customerincome.catId
                                                  FROM
                                                  	customerincome
                                                  GROUP BY
                                                  	customerincome.customerId");

             $customerincomes = $command1->query();


             // $customerincomes=Customerincome::find()
             //                        ->sum('invoiceLinesAmount')
             //                        // ->groupBy('customerId')
             //                        // ->sum('invoiceLinesAmount');
             //                        // ->queryAll();
             //                        ->all();
             //                    //      Customerincome::find()->select('*')
             //                    // ->sum('invoiceLinesAmount')
             //                    // // ->select('count(*) as counters, type')
             //                    // ->groupBy('customerId')
             //                    // ->createCommand()
             //                    // ->queryAll();
             //                    // // ->groupBy(['customerId']);
             //                    // ->all();
                                //
                                // print_r($customerincomes);
                                // var_dump($customerincomes);
                                // die();

             foreach ($customerincomes as $customerincomes) {
               // code...

               // $customerincomes->customerId;
               //
               // print_r($customerincomes);
               //
               // die();

               ?>
               <tr>
                 <td><?= $customerincomes["customerDisplayName"];?></td>
                 <td><?= number_format($customerincomes["Sum( customerincome.invoiceLinesAmount )"],2);?></td>
               </tr>

               <?php


             }

              ?>

              <tr>

                  <td></td>
                  <td></td>
              </tr>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->

          <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead"></p>

          <div class="table-responsive">
            <table class="table">


            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
