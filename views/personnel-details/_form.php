<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\PersonnelDetails */
/* @var $form yii\widgets\ActiveForm */
?>


<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">

      <div class="event-workplan-form">

          <?php $form = ActiveForm::begin(); ?>

      </div>

      <div class="row">
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'nationalId')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'firstName')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'sName')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'lName')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
      </div>
      <div class="row">

        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'primaryPhone')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'secondaryPhone')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'displayName')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'printOnChequeName')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
      </div>


      <div class="row">

        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'primaryAddr')->textarea(['rows' => 6]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'town')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
          </div>
        </div>
      </div>


      <div class="row">

        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'hiredDate')->widget(\yii\jui\DatePicker::class, [
                  //'language' => 'ru',
                  'options' => ['class' => 'form-control'],
                  'inline' => false,
                  'dateFormat' => 'yyyy-MM-dd',
              ]); ?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?php $status = ['1' => 'Yes', '2' => 'No']; ?>
            <?= $form->field($model, 'billable')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
            <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="form-group">
            <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>
          </div>
        </div>
      </div>


          <div class="col-sm-3">
            <div class="form-group">

            </div>
          </div>

  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
    <!-- /.box -->
