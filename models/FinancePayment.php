<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_payment".
 *
 * @property int $id
 * @property string $recepitNumber
 * @property string $paymentName
 * @property int $paymentDepositToAccount
 * @property string $paymentTxnDate
 * @property string $paymentType
 * @property string $paymentRefence
 * @property double $paymentTotalAmt
 * @property int $paymentProcessduration
 * @property string $paymentLinkedTxn Number of invoice or credit note
 * @property string $paymentLinkedTxnType Can be invoice or Credit Note
 * @property int $paymentStatus
 * @property string $paymentCreateAt
 * @property string $paymentUpdatedAt
 * @property string $paymentDeletedAt
 * @property int $userId
 *
 * @property FinanceInvoice $paymentLinkedTxn0
 * @property FinanceAccounts $paymentDepositToAccount0
 */
class FinancePayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['recepitNumber', 'paymentName', 'paymentDepositToAccount', 'paymentTxnDate', 'paymentType', 'paymentRefence', 'paymentTotalAmt', 'paymentProcessduration', 'paymentLinkedTxn', 'paymentLinkedTxnType', 'paymentStatus', 'userId'], 'required'],
            [['paymentDepositToAccount', 'paymentProcessduration', 'paymentStatus', 'userId'], 'integer'],
            [['paymentTxnDate', 'paymentCreateAt', 'paymentUpdatedAt', 'paymentDeletedAt'], 'safe'],
            [['paymentTotalAmt'], 'number'],
            [['recepitNumber', 'paymentName', 'paymentLinkedTxn'], 'string', 'max' => 50],
            [['paymentType', 'paymentRefence', 'paymentLinkedTxnType'], 'string', 'max' => 100],
            [['recepitNumber'], 'unique'],
            [['paymentLinkedTxn'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceInvoice::className(), 'targetAttribute' => ['paymentLinkedTxn' => 'invoiceNumber']],
            [['paymentDepositToAccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['paymentDepositToAccount' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recepitNumber' => 'Recepit Number',
            'paymentName' => 'Payment Name',
            'paymentDepositToAccount' => 'Payment Deposit To Account',
            'paymentTxnDate' => 'Payment Txn Date',
            'paymentType' => 'Payment Type',
            'paymentRefence' => 'Payment Reference',
            'paymentTotalAmt' => 'Payment Total Amt',
            'paymentProcessduration' => 'Payment Processduration',
            'paymentLinkedTxn' => 'Payment Linked Txn',
            'paymentLinkedTxnType' => 'Payment Linked Txn Type',
            'paymentStatus' => 'Payment Status',
            'paymentCreateAt' => 'Payment Create At',
            'paymentUpdatedAt' => 'Payment Updated At',
            'paymentDeletedAt' => 'Payment Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLinkedTxn0()
    {
        return $this->hasOne(FinanceInvoice::className(), ['invoiceNumber' => 'paymentLinkedTxn']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentDepositToAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'paymentDepositToAccount']);
    }

    public function getRcpnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(recepitNumber) AS number from finance_payment where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "RCP-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "RCP-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "RCP-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
