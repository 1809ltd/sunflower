<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


	/*to enable modal pop up*/
	use yii\bootstrap\Modal;
	use yii\helpers\Url;
// echo \Yii::$app->session->get('eventId');
?>
<div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Add an Checkout Item</h3>

      <div class="box-tools pull-right">
        <?= Html::button('Add Checkout Item', ['value'=>Url::to((['/inventory/assetmovement/delivery/asset-deliveryform/deliveryformcreate'])),'class' => 'btn btn-sm btn-success','id'=>'modalButton']) ?>
      </div>
    </div>

      <?php
      Modal::begin([
        // 'header'=>'<h3>Add Customer</h3>',
        'class'=>'modal-dialog',
        'id'=>'modal',
        'size'=>'modal-lg',
      ]);

      echo "<div class='modal-content' id='modalContent'></div>";

      Modal::end();
       ?>
       <?php
        Modal::begin([
            'header'=>'<h4>Update Model</h4>',
            'id'=>'update-modal',
            'size'=>'modal-lg'
        ]);

        echo "<div id='updateModalContent'></div>";

        Modal::end();
    ?>

    <div class="box-body">

      <?php Pjax::begin(['id' => 'holdasset']); ?>
      <?php // echo $this->render('_search', ['model' => $searchModel]);

      $dataProvider = new \yii\data\ArrayDataProvider([
        'key'=>'id',
        'allModels' => $output,
        // 'sort' => [
        //     'attributes' => ['id', 'name', 'email'],
        // ],
]);

// print_r($dataProvider);
// die();
       ?>
      <?= GridView::widget([
          'dataProvider' => $dataProvider,
          // 'filterModel' => $searchModel,
          // 'dataProvider' => $output,
          'columns' => [
              ['class' => 'yii\grid\SerialColumn'],

              // 'id',
              // 'locationId',
              'locationName',
              // 'assetId',
              'assetTag',
              'numberPlate',
              'itemsName',
              // 'checkOutDate',
              // 'dueDate',
              // 'activity',
              // 'deliveryId',
              // 'delivery.deliveryNumber',
              // 'activityId',
              // 'activity0.eventName',
              // 'orderId',
              // 'orderlist.item.itemsName',
              // 'orderlistId',
              // 'vehicle',
              // 'vehicle0.numberPlate',
              // 'assignto',
              // // 'assignto0.displayName',
              // 'timestamp',
              // 'updatedAt',
              // 'deletedAt',
              // 'status',
              // 'athurizedBy',
              // 'userId',
              // 'createdBy',

              //
              // // 'id',
              // 'locationId',
              // 'assetId',
              // // 'checkOutDate',
              // // 'dueDate',
              // //'activity',
              // //'deliveryId',
              // //'activityId',
              // //'orderId',
              // 'orderlistId',
              // 'vehicle',
              // 'assignto',
              // //'timestamp',
              // //'updatedAt',
              // //'deletedAt',
              // //'status',
              // //'athurizedBy',
              // //'userId',
              // //'createdBy',


              // ['class' => 'yii\grid\ActionColumn'],
              [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{view}{update}{delete}',
                  'buttons' => ['view' => function($url, $model) {
                      return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
                  },
                'update' => function($url,$model,$key){
                      $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                          'value'=>Url::to((['/inventory/assetmovement/delivery/asset-deliveryform/deliveryformupdate','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                          'class'=>'update-modal-click grid-action',
                          'data-toggle'=>'tooltip',
                          'data-placement'=>'bottom',
                          'title'=>'Update'
                      ]);
                      return $btn;
                },
                  'delete' => function($url, $model) {
                      return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
                  }
                  ]
              ],
          ],
      ]); ?>
      <?php Pjax::end(); ?>



        <br>
        <div class="row">
          <div class="col-md-12">
            <?php

            // $checkou->eventId=$_SESSION['eventId'];
            // $checkou->orderId= \Yii::$app->session->get('orderId');

             ?>
            <?= $this->render('checkoutDeliveryform', [
                'model' => $model
            ]) ?>
          </div>
        </div>


    </div>
</div>
