<?php

namespace app\modules\user\moduleslist;

/**
 * moduleslist module definition class
 */
class moduleslist extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\user\moduleslist\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
