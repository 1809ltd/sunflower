-- This is the income generated from the vendors
-- ----------------------------
-- View structure for `vednor_statement`
-- ----------------------------
DROP VIEW
IF
	EXISTS `vendor_statement`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root` @`localhost` SQL SECURITY DEFINER VIEW `vendor_statement` AS
SELECT
	finance_bills.vendorId AS vendorId,
	finance_bills.billDueDate AS date,
	finance_bills.billRef AS transactionNo,
	COALESCE ( CONCAT( "Bill for Event  : ", event_details.eventName, " held on", event_details.eventStartDate ), " Bill " ) AS Details,
	( Sum( finance_bills_lines.biilsLinesAmount ) + Sum( finance_bills_lines.biilsLinesTaxAmount ) ) AS amountinvoiced,
	0 AS amountPaid
FROM
	finance_bills_lines
	INNER JOIN finance_bills ON finance_bills_lines.biilsId = finance_bills.id
	LEFT JOIN event_details ON finance_bills.projectId = event_details.id
WHERE
	finance_bills.billStatus > 0
	AND finance_bills_lines.billsLinesBillableStatus > 0
GROUP BY
	finance_bills.id
  UNION ALL
SELECT
	finance_bills_payments_lines.vendorId  AS vendorId,
	finance_bills_payments.date AS date,
	finance_bills_payments.billPaymentBillRef AS transactionNo,
	CONCAT( "Payments for invoice  : ", finance_bills.billRef, " ,payment via ", finance_bills_payments.billPaymentPayType, " reference number : ", finance_bills_payments.billPaymentBillRef ) AS Details,
	0 AS amountinvoiced,
	finance_bills_payments_lines.amount AS amountPaid
FROM
	finance_bills_payments_lines
	INNER JOIN finance_bills_payments ON finance_bills_payments_lines.billpaymentId = finance_bills_payments.id
	INNER JOIN finance_bills ON finance_bills_payments_lines.billId = finance_bills.id
WHERE
	finance_bills_payments_lines.`status` = 1
	OR finance_bills_payments_lines.`status` = 2
	AND finance_bills_payments.paymentstatus > 0
UNION ALL
SELECT
	finance_bills_payments.vendorId,
	finance_bills_payments.date AS date,
	finance_bills_payments.billPaymentBillRef AS transactionNo,
	CONCAT( "Unallocated amount done payment via ", finance_bills_payments.billPaymentPayType, " reference number : ", finance_bills_payments.billPaymentBillRef ) AS Details,
	0 AS amountinvoiced,
	finance_bills_payments.unallocatedAmount AS amountPaid
FROM
	finance_bills_payments
WHERE
	finance_bills_payments.paymentstatus > 0
	AND finance_bills_payments.unallocatedAmount > 0
union all
SELECT
	finance_vendor_credit_memo.vendorId AS vendorId,
	finance_vendor_credit_memo.vendorCreditMemoTxnDate AS date,
	finance_vendor_credit_memo.refernumber AS transactionNo,
	CONCAT( "Credit Note for Invoice : ", finance_vendor_credit_memo_lines.billRef ) AS Details,
	0 AS amountinvoiced,
	( Sum( finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount ) + Sum( finance_vendor_credit_memo_lines.taxamount ) ) AS amountPaid
FROM
	finance_vendor_credit_memo_lines
	INNER JOIN finance_vendor_credit_memo ON finance_vendor_credit_memo_lines.vendorCreditMemoId = finance_vendor_credit_memo.id
WHERE
	finance_vendor_credit_memo.vendorCreditMemoStatus > 0
	AND finance_vendor_credit_memo_lines.vendorCreditMemoLineStatus > 0
union ALL
SELECT
  finance_vendor_offset_memo_lines.vendorId,
  finance_vendor_offset_memo.vendorOffsetMemoTxnDate AS DATE,
  finance_vendor_offset_memo.refernumber AS transactionNo,
  CONCAT (
    "Credit Note for Invoice : ",
    finance_vendor_offset_memo_lines.billRef
  ) AS Details,
  0 AS amountinvoiced,
  finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount AS amountPaid
FROM
  finance_vendor_offset_memo_lines
  INNER JOIN finance_vendor_offset_memo
    ON finance_vendor_offset_memo_lines.vendorOffsetMemoId = finance_vendor_offset_memo.id
WHERE finance_vendor_offset_memo_lines.vendorOffsetMemoLineStatus > 0
  AND finance_vendor_offset_memo.vendorOffsetMemoStatus > 0
