<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBillsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-bills-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vendorId') ?>

    <?= $form->field($model, 'billRef') ?>

    <?= $form->field($model, 'billDueDate') ?>

    <?php // echo $form->field($model, 'billAmount') ?>

    <?php // echo $form->field($model, 'billsTaxAmount') ?>

    <?php // echo $form->field($model, 'billCreatetime') ?>

    <?php // echo $form->field($model, 'billUpdatedTime') ?>

    <?php // echo $form->field($model, 'billDeletedAt') ?>

    <?php // echo $form->field($model, 'billStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
