<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetAmortization */

$this->title = 'Create Asset Amortization';
$this->params['breadcrumbs'][] = ['label' => 'Asset Amortizations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-amortization-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
