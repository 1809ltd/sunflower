<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceVendorCreditMemoLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Vendor Credit Memo Lines';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-credit-memo-lines-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Vendor Credit Memo Lines', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'vendorId',
            'vendorCreditMemoId',
            'billRef',
            'vendorCreditMemoLineDescription:ntext',
            //'vendorCreditMemoLineAmount',
            //'vendorCreditMemoLineStatus',
            //'vendorCreditMemoLineCreatedAt',
            //'vendorCreditMemoLineUpdatedAt',
            //'creidtDeletedAt',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
