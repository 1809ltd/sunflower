<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinancePayment;

/**
 * FinancePaymentSearch represents the model behind the search form of `app\models\FinancePayment`.
 */
class FinancePaymentSearch extends FinancePayment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'paymentDepositToAccount', 'paymentProcessduration', 'paymentStatus', 'userId'], 'integer'],
            [['paymentName', 'paymentTxnDate', 'paymentType', 'paymentRefence', 'paymentLinkedTxn', 'paymentLinkedTxnType', 'paymentCreateAt', 'paymentUpdatedAt', 'paymentDeletedAt'], 'safe'],
            [['paymentTotalAmt'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinancePayment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'paymentDepositToAccount' => $this->paymentDepositToAccount,
            'paymentTxnDate' => $this->paymentTxnDate,
            'paymentTotalAmt' => $this->paymentTotalAmt,
            'paymentProcessduration' => $this->paymentProcessduration,
            'paymentStatus' => $this->paymentStatus,
            'paymentCreateAt' => $this->paymentCreateAt,
            'paymentUpdatedAt' => $this->paymentUpdatedAt,
            'paymentDeletedAt' => $this->paymentDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'paymentName', $this->paymentName])
            ->andFilterWhere(['like', 'paymentType', $this->paymentType])
            ->andFilterWhere(['like', 'paymentRefence', $this->paymentRefence])
            ->andFilterWhere(['like', 'paymentLinkedTxn', $this->paymentLinkedTxn])
            ->andFilterWhere(['like', 'paymentLinkedTxnType', $this->paymentLinkedTxnType]);

        return $dataProvider;
    }
}
