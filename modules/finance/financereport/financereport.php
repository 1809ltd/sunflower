<?php

namespace app\modules\finance\financereport;

/**
 * financereport module definition class
 */
class financereport extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here

                $this->modules =[
                  /*Fiance Compnay Sub Module*/
                  'company' => [
                          'class' => 'app\modules\finance\financereport\companyfinancials\companyfinancials',
                      ],
                  'customer' => [
                      'class' => 'app\modules\finance\financereport\customerreport\customerreport',
                  ],
                  'vendor' => [
                      'class' => 'app\modules\finance\financereport\vendorreport\vendorreport',
                  ],
                ];
    }
}
