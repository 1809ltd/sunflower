<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VendorCompanyDetails */

$this->title = 'Create Supplier Company Details';
$this->params['breadcrumbs'][] = ['label' => 'Supplier Company Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-company-details-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
