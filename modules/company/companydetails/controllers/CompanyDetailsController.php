<?php

namespace app\modules\company\companydetails\controllers;

use Yii;
use app\modules\company\companydetails\models\CompanyDetails;
use app\modules\company\companydetails\models\CompanyDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CompanyDetailsController implements the CRUD actions for CompanyDetails model.
 */
class CompanyDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->companyupdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->companyupdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }


        /**
         * Lists all CompanyDetails models.
         * @return mixed
         */
        public function actionIndex()
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $searchModel = new CompanyDetailsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single CompanyDetails model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
            // // $this->layout='addformdatatablelayout';
            $this->layout = '@app/views/layouts/addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new CompanyDetails model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = new CompanyDetails();

            //checking if its a post
            if ($model->load(Yii::$app->request->post())) {
              // code...

              //Getting user Log in details
              $model->companyUserId = Yii::$app->user->identity->id;
              $model->createdBy = Yii::$app->user->identity->id;
              $model->companyTimestamp = date('Y-m-d H:i:s');

              $imageName= $model->companyName;
              $model->file= UploadedFile::getInstance($model,'file');
              $model->file->saveAs('logo/company/'.$imageName.'.'.$model->file->extension);

              $model->companyLogo='logo/company/'.$imageName.'.'.$model->file->extension;

              $model->save();


              return $this->redirect(['view', 'id' => $model->id]);


            } else {
              // code...
              /*Load the create Page*/
              return $this->render('create', [
                  'model' => $model,
              ]);

            }

        }

        /**
         * Updates an existing CompanyDetails model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = new CompanyDetails();

            $model = $this->findModel($id);
            $oldogo= $model->companyLogo;

            $model->file= $model->companyLogo;

            //Getting user Log in details
            $model->companyUserId = Yii::$app->user->identity->id;

            //checking if its a post
            if ($model->load(Yii::$app->request->post())) {
              // code...

              //Getting user Log in details
              $model->companyUserId = Yii::$app->user->identity->id;

              $imageName= $model->companyName;
              $model->file= UploadedFile::getInstance($model,'file');

              if(isset($model->file)){

                $model->file->saveAs('logo/company/'.$imageName.'.'.$model->file->extension);

                $model->companyLogo='logo/company/'.$imageName.'.'.$model->file->extension;

              } else {
                  $model->companyLogo = $oldogo;
              }

              $model->save();


              return $this->redirect(['view', 'id' => $model->id]);


            } else {
              // code...
              /*Load the create Page*/
              return $this->render('update', [
                  'model' => $model,
              ]);

            }
        }

        /**
         * Deletes an existing CompanyDetails model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the CompanyDetails model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return CompanyDetails the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = CompanyDetails::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
