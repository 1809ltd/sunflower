<?php

namespace app\modules\personnel\personneldetails;

/**
 * personneldetails module definition class
 */
class personneldetails extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\personnel\personneldetails\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
