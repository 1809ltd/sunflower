<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\transfer\models\FinanceTransferee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-transferee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'transferid')->textInput() ?>

    <?= $form->field($model, 'transfereeRefernce')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'transfereeFromAccountRef')->textInput() ?>

    <?= $form->field($model, 'transfereeToAccountRef')->textInput() ?>

    <?= $form->field($model, 'transfereeAmount')->textInput() ?>

    <?= $form->field($model, 'transferCharges')->textInput() ?>

    <?= $form->field($model, 'transfereeTxnDate')->textInput() ?>

    <?= $form->field($model, 'transfereeCreatedAt')->textInput() ?>

    <?= $form->field($model, 'transfereeUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'transfereeDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
