<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\customer\customerdetails\models\CustomerDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="customer-details-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customerNumber',
            'customerFullname',
            'customerCompanyName',
            'customerKraPin',
            'customerDisplayName',
            'customerPrimaryPhone',
            'customerPrimaryEmail:email',
            'customerstatus',
            'customercity',
            'customerPhyscialAdress',
            'customerCounty',
            'customerCountry',
            'customerAddress',
            'customerCreateTime',
            'customerUpdatedAt',
            'customerDeleteAt',
            'createdBy',
            'userId',
        ],
    ]) ?>

</div>
