<!-- <div class="balancesheet-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
// Geetng BalanceSheet models;
use app\modules\finance\financereport\companyfinancials\balancesheet\models\BalanceSheet;

// Bill Payments
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments;
// Event Payments
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments;
// Office Requstion Overpaymnet
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Balance Sheet';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = ['label' => 'Balance Sheet', 'url' => ['index']];

?>
<!-- Main content -->
<!-- Search Date And the rest -->
<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <?php $form = ActiveForm::begin(); ?>
      <div class="col-md-6">
        <div class="form-group">
          <label class="col-lg-4 control-label">As Of: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                    //'language' => 'ru',
                    'options' => ['autocomplete'=>'off','class' => 'form-control'],
                    'inline' => false,
                    'dateFormat' => 'yyyy-MM-dd',
                ])->label(false) ?>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <div class="col-lg-8 col-lg-offset-4">
            <div class="center-align">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
            </div>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
</div>
</div>

<?php
// Ccheking is search is not empty
if (!empty($getserach)) {
  // code...
  ?>
  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Balance Sheet</h3>
      <h5 class="box-title">Reporting period: As of  <?php echo date('M j, Y', strtotime($model->endDate));?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>

    <div class="box">
      <div class="box-body">
        <!-- <h4 class="box-title">ASSETS</h4> -->

        <!-- <h6 class="box-title"><strong>Current Assets:</strong></h6> -->
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th style="background-color: gold;">ASSETS:</th>
              <th style="background-color: gold;"></th>
            </tr>
            <tr>
              <th style="background-color: violet;">Current Assets:</th>
              <th style="background-color: violet;"></th>
            </tr>
            <tr>
              <th class="text-left">Account</th>
              <th class="text-right">Balance</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $bal=0;
            foreach ($cureentAsstCash as $cureentAsstCash) {
              // code...
              $bal+=$cureentAsstCash->balance_amount;
              $replaceamount = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($cureentAsstCash->balance_amount,2,'.',',')
             );
             ?>
              <tr>
                <td  class="text-left"><?= $cureentAsstCash->accountName; ?> </td>
                <td  class="text-right"><span class="align-numbers"><?= $replaceamount ;?></span></td>
              </tr>
               <?php

            }

            $replacerecviableTTamount = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($recviableTT,2,'.',',')
           );
           ?>

           <tr>
             <td  class="text-left">Accounts Receivables</td>
             <td  class="text-right"><span class="align-numbers"><?= $replacerecviableTTamount ;?></span></td>
           </tr>
           <tr>
             <td  class="text-left">Inventory</td>
             <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
           </tr>
           <tr>
             <?php
             $replaceprepaidilssamount = preg_replace(
                '/(-)([\d\.\,]+)/ui',
                '($2)',
                number_format($prepaidbills,2,'.',',')
            );

             ?>
             <td  class="text-left">Prepaid Expenses</td>
             <td  class="text-right"><span class="align-numbers"><?= $replaceprepaidilssamount ;?></span></td>
           </tr>
           <tr>
             <td  class="text-left">Employee Advances</td>
             <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
           </tr>
           <tr>
             <th  class="text-left">Total Current Assets:</th>
             <?php
             $currentAssetTotal=$bal+$recviableTT+$prepaidbills;
             $replacecurrentAssetTotal = preg_replace(
                '/(-)([\d\.\,]+)/ui',
                '($2)',
                number_format($currentAssetTotal,2,'.',',')
            );
              ?>
             <td  class="text-right"><?= $replacecurrentAssetTotal; ?></td>
           </tr>
         </tbody>
       </table>

     <!-- <h6 class="box-title">Fixed Assets</h6> -->
     <table class="table  table-striped table-condensed">
       <thead>
         <tr>
           <th style="background-color: violet;">Fixed Assets:</th>
           <th style="background-color: violet;"></th>
         </tr>
         <tr>
           <th class="text-left">Asset</th>
           <th class="text-right">Value</th>
         </tr>
       </thead>
       <tbody>
         <?php
           $currentFixedAssetTotal=0;
           foreach ($fixedAssets as $fixedAsset){
             // code...
             $currentFixedAssetTotal+=$fixedAsset["Sum( asset_amortization.endBalance )"];

             ?>
             <tr>
               <td  class="text-left"><?= $fixedAsset["assetCategoryName"];?></td>
               <td  class="text-right"><span class="align-numbers"><?= number_format($fixedAsset["Sum( asset_amortization.endBalance )"],2) ;?></span></td>
             </tr>
            <?php
          }
          ?>

          <tr>
            <th  class="text-left">Total Fixed Assets:</th>
            <td  class="text-right"><?= number_format($currentFixedAssetTotal,2); ?></td>
          </tr>

      </tbody>
    </table>
    <!-- <h6 class="box-title">Other Assets</h6> -->
    <table class="table  table-striped table-condensed">
      <thead>

          <tr>
            <th style="background-color: violet;">Other Assets:</th>
            <th style="background-color: violet;"></th>
          </tr>
        <tr>
          <th class="text-left">Asset</th>
          <th class="text-right">Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
         <th  class="text-left">Total Other Assets:</th>
         <th  class="text-right"><?= number_format(0,2); ?></th>
       </tr>
       <tr>
         <th class="text-left">TOTAL ASSETS</th>
         <?php
         $replaceAssetTotal = preg_replace(
              '/(-)([\d\.\,]+)/ui',
              '($2)',
              number_format($currentAssetTotal+$currentFixedAssetTotal,2,'.',',')
          );
          ?>
          <th class="text-right"><?= $replaceAssetTotal; ?></th>
        </tr>
     </tbody>
   </table>
    </br>

    <!-- <h5 class="box-title">LIABILITIES</h5> -->
    <!-- <h6 class="box-title">Current Liabilities</h6> -->
    <table class="table  table-striped table-condensed">
      <thead>
        <tr>
          <th style="background-color: gold;">LIABILITIES:</th>
          <th style="background-color: gold;"></th>
        <tr>
          <th style="background-color: violet;">Current Liabilities:</th>
          <th style="background-color: violet;"></th>
        </tr>
        <tr>
          <th class="text-left">Account</th>
          <th class="text-right">Balance</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $replacepayableTTamount = preg_replace(
           '/(-)([\d\.\,]+)/ui',
           '($2)',
           number_format($payableTT,2,'.',',')
       );
       $replaceeventPrepayemntamount = preg_replace(
          '/(-)([\d\.\,]+)/ui',
          '($2)',
          number_format($eventPrepayemnt,2,'.',',')
      );
       ?>

       <tr>
         <td  class="text-left">Accounts Payables</td>
         <td  class="text-right"><span class="align-numbers"><?= $replacepayableTTamount ;?></span></td>
       </tr>

       <?php
       $taxpayabletotal=0;

       foreach ($taxpayables as $taxpayable) {
         // code...
         $taxpayabletotal+=$taxpayable->balance_amount;

         $replacetaxpayablesamount = preg_replace(
            '/(-)([\d\.\,]+)/ui',
            '($2)',
            number_format($taxpayable->balance_amount,2,'.',',')
        );
         ?>
         <tr>
            <td  class="text-left">Sale Tax Payables (<?= $taxpayable->taxCode ?>)  </td>
           <td  class="text-right"><span class="align-numbers"><?= $replacetaxpayablesamount ;?></span></td>
         </tr>
         <?php
       }
       ?>
       <tr>
          <td  class="text-left">Event Prepayments</td>
         <td  class="text-right"><span class="align-numbers"><?= $replaceeventPrepayemntamount ;?></span></td>
       </tr>
       <?php

       $totalcurrentLiablity = $payableTT+$eventPrepayemnt+$taxpayabletotal;
       $replacetotalcurrentLiablityamount = preg_replace(
          '/(-)([\d\.\,]+)/ui',
          '($2)',
          number_format($totalcurrentLiablity,2,'.',',')
      );

       ?>
       <tr>
         <th  class="text-left">Total Current Liabilities:</th>
         <td  class="text-right"><?= $replacetotalcurrentLiablityamount; ?></td>
       </tr>
     </tbody>
    </table>

    <!-- <h6 class="box-title">Long-Term Liabilities</h6> -->
    <table class="table  table-striped table-condensed">
    <thead>
      <tr>
        <th style="background-color: violet;">Long-Term Liabilities:</th>
        <th style="background-color: violet;"></th>
      </tr>
     <tr>
       <th class="text-left">Asset</th>
       <th class="text-right">Value</th>
     </tr>
    </thead>
    <tbody>
     <?php
     ?>
     <tr>
      <td  class="text-left"><a href="#"> Mortgage Payable</a></td>
      <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
    </tr>
    <tr>
     <td  class="text-left"><a href="#"> Less: Current portion of Long-term debt</a></td>
     <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
   </tr>
    <tr>
      <?php
      $totalongliablities=0;
      ?>
      <th  class="text-left"> Total Long-Term Liabilities:</th>
      <td  class="text-right"><?= number_format(0,2); ?></td>
    </tr>
    </tbody>
    </table>
    </br>

    <!-- <h5 class="box-title">EQUITY</h5> -->
    <!-- <h6 class="box-title">Current Liabilities</h6> -->
    <table class="table  table-striped table-condensed">
    <thead>
      <tr>
        <th style="background-color: gold;">EQUITY:</th>
        <th style="background-color: gold;"></th>
      </tr>
      <tr>
        <th></th>
        <th></th>
      </tr>

      <tr>
        <th class="text-left">Account</th>
        <th class="text-right">Balance</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $onwersbal=0;
      foreach ($ownerEquity as $ownerEquity) {
        // code...
        $onwersbal+=$ownerEquity->balance_amount;
        $replaceownerEquityamount = preg_replace(
           '/(-)([\d\.\,]+)/ui',
           '($2)',
           number_format($ownerEquity->balance_amount,2,'.',',')
       );
       ?>
        <tr>
          <td  class="text-left"><?= $ownerEquity->accountName; ?> </td>
          <td  class="text-right"><span class="align-numbers"><?= $replaceownerEquityamount ;?></span></td>
        </tr>
        <?php
      }

      // Get Previous Year Earnings
      $replacelastyearEarningamount = preg_replace(
         '/(-)([\d\.\,]+)/ui',
         '($2)',
         number_format($lastyearEarning,2,'.',',')
     );

     // Get This years Earning
     $replacethisyearEarningamount = preg_replace(
        '/(-)([\d\.\,]+)/ui',
        '($2)',
        number_format($thisyearEarning,2,'.',',')
    );

  // Get total Equity Value
    $totalEquity= $onwersbal+$lastyearEarning+$thisyearEarning;

    $replacetotalEquityamount = preg_replace(
       '/(-)([\d\.\,]+)/ui',
       '($2)',
       number_format($totalEquity,2,'.',',')
   );

   // Getting the value of total Equity + Total LIABILITIES
      $totalEquitynaLiability= $totalEquity+$totalcurrentLiablity+$totalongliablities;

     $replacetotalEquitynaLiabilityamount = preg_replace(
        '/(-)([\d\.\,]+)/ui',
        '($2)',
        number_format($totalEquitynaLiability,2,'.',',')
    );

      ?>
      <tr>
       <td  class="text-left">Previous Year(s) Earnings</td>
       <td  class="text-right"><span class="align-numbers"><?= $replacelastyearEarningamount;?></span></td>
     </tr>
     <tr>
        <td  class="text-left">Current Year Earnings - Net Income (Loss)</td>
        <td  class="text-right"><span class="align-numbers"><?= $replacethisyearEarningamount ;?></span></td>
      </tr>
     <tr>
       <th  class="text-left"> Total Equity:</th>
       <td  class="text-right"><?= $replacetotalEquityamount; ?></td>
     </tr>
    <tr>
      <th  class="text-left"> TOTAL LIABILITIES & EQUITY :</th>
      <th  class="text-right"><?= $replacetotalEquitynaLiabilityamount; ?></th>
    </tr>
    </tbody>
    </table>

    </br>
    <!-- <h6 class="box-title">Current Liabilities</h6> -->
    <table class="table  table-striped table-condensed">
    <thead>
      <tr>
        <th style="background-color: gold;">Balance Sheet Check:</th>
        <th style="background-color: gold;"></th>
      </tr>
    </thead>
    <tbody>
      <?php
      // Getting the value of total Equity + Total LIABILITIES
      $totalbalancesheetcheck= $currentAssetTotal+$currentFixedAssetTotal-$totalEquitynaLiability;

     $replacetotalbalancesheetcheck = preg_replace(
        '/(-)([\d\.\,]+)/ui',
        '($2)',
        number_format($totalbalancesheetcheck,2,'.',',')
    );

      ?>

    <tr>
      <th  class="text-left"> </th>
      <th  class="text-right"><?= $replacetotalbalancesheetcheck; ?></th>
    </tr>
    </tbody>
    </table>

    </div>
    </div>
    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="javascript:void(0)" onclick="window.print()"
           class="btn btn-xs btn-success m-b-10"><i
                class="fa fa-print m-r-5"></i> Print</a>

          <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
          <i class="fa fa-download"></i> Generate PDF -->
        <!-- </button> -->
      </div>
  </div>
  </section>



  <?php
} else {
  // code...
  $model->endDate= date('Y-m-d');


  ?>
  <section class="invoice">
  <div class="text-center">
    <h6 class="box-title">
      <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
    </h6>
    <h3 class="box-title">Balance Sheet</h3>
    <h5 class="box-title">Reporting period: As of  <?php echo date('M j, Y', strtotime($model->endDate));?></h5>
    <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
  </div>

  <div class="box">
    <div class="box-body">
      <!-- <h4 class="box-title">ASSETS</h4> -->

      <!-- <h6 class="box-title"><strong>Current Assets:</strong></h6> -->
      <table class="table  table-striped table-condensed">
        <thead>
          <tr>
            <th style="background-color: gold;">ASSETS:</th>
            <th style="background-color: gold;"></th>
          </tr>
          <tr>
            <th style="background-color: violet;">Current Assets:</th>
            <th style="background-color: violet;"></th>
          </tr>
          <tr>
            <th class="text-left">Account</th>
            <th class="text-right">Balance</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $bal=0;


          $cureentAsstCash = $modelBal->getallBlancecashtime($model->endDate);

          foreach ($cureentAsstCash as $cureentAsstCash) {
            // code...
            $bal+=$cureentAsstCash->balance_amount;
            $replaceamount = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($cureentAsstCash->balance_amount,2,'.',',')
           );

            ?>
            <tr>
              <td  class="text-left"><?= $cureentAsstCash->accountName; ?> </td>
              <td  class="text-right"><span class="align-numbers"><?= $replaceamount ;?></span></td>
            </tr>
             <?php

          }

          $recviableTT = $modelBal->getreceivableAmount($model->endDate);
          $replacerecviableTTamount = preg_replace(
             '/(-)([\d\.\,]+)/ui',
             '($2)',
             number_format($recviableTT,2,'.',',')
         );
         ?>

         <tr>
           <td  class="text-left">Accounts Receivables</td>
           <td  class="text-right"><span class="align-numbers"><?= $replacerecviableTTamount ;?></span></td>
         </tr>
         <tr>
           <td  class="text-left">Inventory</td>
           <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
         </tr>
         <tr>
           <?php

           $prepaidbills=$modelBal->getPrepaidAmount($model->endDate);
           $replaceprepaidilssamount = preg_replace(
              '/(-)([\d\.\,]+)/ui',
              '($2)',
              number_format($prepaidbills,2,'.',',')
          );

           ?>
           <td  class="text-left">Prepaid Expenses</td>
           <td  class="text-right"><span class="align-numbers"><?= $replaceprepaidilssamount ;?></span></td>
         </tr>
         <tr>
           <td  class="text-left">Employee Advances</td>
           <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
         </tr>
         <tr>
           <th  class="text-left">Total Current Assets:</th>
           <?php
           $currentAssetTotal=$bal+$recviableTT+$prepaidbills;
           $replacecurrentAssetTotal = preg_replace(
              '/(-)([\d\.\,]+)/ui',
              '($2)',
              number_format($currentAssetTotal,2,'.',',')
          );
            ?>
           <td  class="text-right"><?= $replacecurrentAssetTotal; ?></td>
         </tr>
       </tbody>
     </table>

   <!-- <h6 class="box-title">Fixed Assets</h6> -->
   <table class="table  table-striped table-condensed">
     <thead>
       <tr>
         <th style="background-color: violet;">Fixed Assets:</th>
         <th style="background-color: violet;"></th>
       </tr>
       <tr>
         <th class="text-left">Asset</th>
         <th class="text-right">Value</th>
       </tr>
     </thead>
     <tbody>
       <?php
         $currentFixedAssetTotal=0;
         $fixedAssets = $modelBal->getfixedAssetAmount($model->endDate);
        foreach ($fixedAssets as $fixedAsset){
           // code...
           $currentFixedAssetTotal+=$fixedAsset["Sum( asset_amortization.endBalance )"];

           ?>
           <tr>
             <td  class="text-left"><?= $fixedAsset["assetCategoryName"];?></td>
             <td  class="text-right"><span class="align-numbers"><?= number_format($fixedAsset["Sum( asset_amortization.endBalance )"],2) ;?></span></td>
           </tr>
          <?php
        }
        ?>

        <tr>
          <th  class="text-left">Total Fixed Assets:</th>
          <td  class="text-right"><?= number_format($currentFixedAssetTotal,2); ?></td>
        </tr>

    </tbody>
  </table>
  <!-- <h6 class="box-title">Other Assets</h6> -->
  <table class="table  table-striped table-condensed">
    <thead>

        <tr>
          <th style="background-color: violet;">Other Assets:</th>
          <th style="background-color: violet;"></th>
        </tr>
      <tr>
        <th class="text-left">Asset</th>
        <th class="text-right">Value</th>
      </tr>
    </thead>
    <tbody>
      <tr>
       <th  class="text-left">Total Other Assets:</th>
       <th  class="text-right"><?= number_format(0,2); ?></th>
     </tr>
     <tr>
       <th class="text-left">TOTAL ASSETS</th>
       <?php
       $replaceAssetTotal = preg_replace(
            '/(-)([\d\.\,]+)/ui',
            '($2)',
            number_format($currentAssetTotal+$currentFixedAssetTotal,2,'.',',')
        );
        ?>
        <th class="text-right"><?= $replaceAssetTotal; ?></th>
      </tr>
   </tbody>
 </table>
  </br>

  <!-- <h5 class="box-title">LIABILITIES</h5> -->
  <!-- <h6 class="box-title">Current Liabilities</h6> -->
  <table class="table  table-striped table-condensed">
    <thead>
      <tr>
        <th style="background-color: gold;">LIABILITIES:</th>
        <th style="background-color: gold;"></th>
      <tr>
        <th style="background-color: violet;">Current Liabilities:</th>
        <th style="background-color: violet;"></th>
      </tr>
      <tr>
        <th class="text-left">Account</th>
        <th class="text-right">Balance</th>
      </tr>
    </thead>
    <tbody>
      <?php

      // Get the Payables Figure
      $payableTT = $modelBal->getpayablesAmount($model->endDate);

      // print_r($payableTT);
      // die();
      $replacepayableTTamount = preg_replace(
         '/(-)([\d\.\,]+)/ui',
         '($2)',
         number_format($payableTT,2,'.',',')
     );

     // Get Event Prepayemnt
     $eventPrepayemnt = $modelBal->getEventPrepayments($model->endDate);

     // print_r($payableTT);
     // die();
     $replaceeventPrepayemntamount = preg_replace(
        '/(-)([\d\.\,]+)/ui',
        '($2)',
        number_format($eventPrepayemnt,2,'.',',')
    );
     ?>

     <tr>
       <td  class="text-left">Accounts Payables</td>
       <td  class="text-right"><span class="align-numbers"><?= $replacepayableTTamount ;?></span></td>
     </tr>

     <?php
     // Get Tax amount
     $taxpayables = $modelBal->getTaxpayablesAmount($model->endDate);

     $taxpayabletotal=0;

     foreach ($taxpayables as $taxpayable) {
       // code...

       // echo "this is the amount".$taxpayable->balance_amount;

       // print_r($taxpayable);
       // die();
       //
       $taxpayabletotal+=$taxpayable->balance_amount;

       $replacetaxpayablesamount = preg_replace(
          '/(-)([\d\.\,]+)/ui',
          '($2)',
          number_format($taxpayable->balance_amount,2,'.',',')
      );
       ?>
       <tr>
          <td  class="text-left">Sale Tax Payables (<?= $taxpayable->taxCode ?>)  </td>
         <td  class="text-right"><span class="align-numbers"><?= $replacetaxpayablesamount ;?></span></td>
       </tr>

       <?php


     }


    ?>

     <tr>
        <td  class="text-left">Event Prepayments</td>
       <td  class="text-right"><span class="align-numbers"><?= $replaceeventPrepayemntamount ;?></span></td>
     </tr>
     <?php

     $totalcurrentLiablity = $payableTT+$eventPrepayemnt+$taxpayabletotal;
     $replacetotalcurrentLiablityamount = preg_replace(
        '/(-)([\d\.\,]+)/ui',
        '($2)',
        number_format($totalcurrentLiablity,2,'.',',')
    );

     ?>
     <tr>
       <th  class="text-left">Total Current Liabilities:</th>
       <td  class="text-right"><?= $replacetotalcurrentLiablityamount; ?></td>
     </tr>
   </tbody>
  </table>

  <!-- <h6 class="box-title">Long-Term Liabilities</h6> -->
  <table class="table  table-striped table-condensed">
  <thead>
    <tr>
      <th style="background-color: violet;">Long-Term Liabilities:</th>
      <th style="background-color: violet;"></th>
    </tr>
   <tr>
     <th class="text-left">Asset</th>
     <th class="text-right">Value</th>
   </tr>
  </thead>
  <tbody>
   <?php
   ?>
   <tr>
    <td  class="text-left"><a href="#"> Mortgage Payable</a></td>
    <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
  </tr>
  <tr>
   <td  class="text-left"><a href="#"> Less: Current portion of Long-term debt</a></td>
   <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
 </tr>
  <tr>
    <?php
    $totalongliablities=0;
    ?>
    <th  class="text-left"> Total Long-Term Liabilities:</th>
    <td  class="text-right"><?= number_format(0,2); ?></td>
  </tr>
  </tbody>
  </table>
  </br>

  <!-- <h5 class="box-title">EQUITY</h5> -->
  <!-- <h6 class="box-title">Current Liabilities</h6> -->
  <table class="table  table-striped table-condensed">
  <thead>
    <tr>
      <th style="background-color: gold;">EQUITY:</th>
      <th style="background-color: gold;"></th>
    </tr>
    <tr>
      <th></th>
      <th></th>
    </tr>

    <tr>
      <th class="text-left">Account</th>
      <th class="text-right">Balance</th>
    </tr>
  </thead>
  <tbody>
    <?php
    $onwersbal=0;
    $ownerEquity = $modelBal->getOwnersinputAmount($model->endDate);
    foreach ($ownerEquity as $ownerEquity) {
      // code...
      $onwersbal+=$ownerEquity->balance_amount;
      $replaceownerEquityamount = preg_replace(
         '/(-)([\d\.\,]+)/ui',
         '($2)',
         number_format($ownerEquity->balance_amount,2,'.',',')
     );
     ?>
      <tr>
        <td  class="text-left"><?= $ownerEquity->accountName; ?> </td>
        <td  class="text-right"><span class="align-numbers"><?= $replaceownerEquityamount ;?></span></td>
      </tr>
      <?php
    }

    // Get Previous Year Earnings
    $lastyearEarning = $modelBal->getpreviousyearAmount($model->endDate);
    $replacelastyearEarningamount = preg_replace(
       '/(-)([\d\.\,]+)/ui',
       '($2)',
       number_format($lastyearEarning,2,'.',',')
   );

   // Get This years Earning
   $thisyearEarning = $modelBal->getcurrentyearAmount($model->endDate);
   $replacethisyearEarningamount = preg_replace(
      '/(-)([\d\.\,]+)/ui',
      '($2)',
      number_format($thisyearEarning,2,'.',',')
  );

// Get total Equity Value
  $totalEquity= $onwersbal+$lastyearEarning+$thisyearEarning;

  $replacetotalEquityamount = preg_replace(
     '/(-)([\d\.\,]+)/ui',
     '($2)',
     number_format($totalEquity,2,'.',',')
 );

 // Getting the value of total Equity + Total LIABILITIES
    $totalEquitynaLiability= $totalEquity+$totalcurrentLiablity+$totalongliablities;

   $replacetotalEquitynaLiabilityamount = preg_replace(
      '/(-)([\d\.\,]+)/ui',
      '($2)',
      number_format($totalEquitynaLiability,2,'.',',')
  );

    ?>
    <tr>
     <td  class="text-left">Previous Year(s) Earnings</td>
     <td  class="text-right"><span class="align-numbers"><?= $replacelastyearEarningamount;?></span></td>
   </tr>
   <tr>
      <td  class="text-left">Current Year Earnings - Net Income (Loss)</td>
      <td  class="text-right"><span class="align-numbers"><?= $replacethisyearEarningamount ;?></span></td>
    </tr>
   <tr>
     <th  class="text-left"> Total Equity:</th>
     <td  class="text-right"><?= $replacetotalEquityamount; ?></td>
   </tr>
  <tr>
    <th  class="text-left"> TOTAL LIABILITIES & EQUITY :</th>
    <th  class="text-right"><?= $replacetotalEquitynaLiabilityamount; ?></th>
  </tr>
  </tbody>
  </table>

  </br>
  <!-- <h6 class="box-title">Current Liabilities</h6> -->
  <table class="table  table-striped table-condensed">
  <thead>
    <tr>
      <th style="background-color: gold;">Balance Sheet Check:</th>
      <th style="background-color: gold;"></th>
    </tr>
  </thead>
  <tbody>
    <?php
    // Getting the value of total Equity + Total LIABILITIES
    $totalbalancesheetcheck= $currentAssetTotal+$currentFixedAssetTotal-$totalEquitynaLiability;

   $replacetotalbalancesheetcheck = preg_replace(
      '/(-)([\d\.\,]+)/ui',
      '($2)',
      number_format($totalbalancesheetcheck,2,'.',',')
  );

    ?>

  <tr>
    <th  class="text-left"> </th>
    <th  class="text-right"><?= $replacetotalbalancesheetcheck; ?></th>
  </tr>
  </tbody>
  </table>

  </div>
  </div>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="javascript:void(0)" onclick="window.print()"
         class="btn btn-xs btn-success m-b-10"><i
              class="fa fa-print m-r-5"></i> Print</a>

        <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
        <i class="fa fa-download"></i> Generate PDF -->
      <!-- </button> -->
    </div>
</div>
</section>
<?php
}

?>
