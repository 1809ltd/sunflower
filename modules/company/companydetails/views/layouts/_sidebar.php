<!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class=""><a href="/sunflower/web/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?r=company-details"><i class="fa fa-circle-o"></i> Company Master</a></li>
            <li><a href="index.php?r=company-department"><i class="fa fa-circle-o"></i> Department Master</a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Company Storage Master</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li ><a href="index.php?r=company-site-details"><i class="fa fa-circle-o"></i> Company Site</a></li>
                <li ><a href="index.php?r=company-site-location"><i class="fa fa-circle-o"></i> Company Site Locations</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-barcode"></i> <span>Inventory</span>
            <span class="pull-right-container">
              
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-gears"></i> <span>Asset Set Up</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li ><a href="index.php?r=asset-category"><i class="fa fa-circle-o"></i>Category</a></li>
                <li ><a href="index.php?r=asset-category-type"><i class="fa fa-circle-o"></i>Category Type</a></li>
                <li ><a href="index.php?r=asset-registration"><i class="fa fa-circle-o"></i>Asset Registration</a></li>
                <li ><a href="index.php?r=asset-preciation"><i class="fa fa-circle-o"></i>Asset Valuation</a></li>
                <li ><a href="index.php?r=asset-site-location"><i class="fa fa-circle-o"></i>Asset Locations</a></li>

              </ul>
            </li>
            <li><a href="index.php?r=asset-check-out"><i class="fa fa-shopping-cart"></i> Check Out</a></li>
            <li><a href="index.php?r=asset-check-in"><i class="fa fa-barcode"></i> Check In</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-group"></i> <span>Supplier</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?r=vendor-company-details"><i class="fa fa-circle-o"></i> Supplier Details</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-user"></i> <span>Customer</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?r=customer-details"><i class="fa fa-circle-o"></i> Customer Details</a></li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-phone"></i> <span>Follow - Up</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li ><a href="index.php?r=customer-lead"><i class="fa fa-circle-o"></i> Customer Lead</a></li>
                <li ><a href="index.php?r=customer-lead-appointment"><i class="fa fa-circle-o"></i> Appointment</a></li>
                <li ><a href="index.php?r=customer-lead-communication-tracker"><i class="fa fa-circle-o"></i> Communication Tracker</a></li>
              </ul>
            </li>

          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-calendar"></i> <span>Event</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-gears"></i> <span>Event Set Up</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=event-caterogies"><i class="fa fa-circle-o"></i> Event Category</a></li>
                <li><a href="index.php?r=event-role"><i class="fa fa-circle-o"></i> Roles</a></li>
                <li><a href="index.php?r=event-item-role"><i class="fa fa-circle-o"></i> Service Roles</a></li>
                <li><a href="index.php?r=event-item-category-type"><i class="fa fa-circle-o"></i> Service Asset</a></li>
              </ul>
            </li>
            <li><a href="index.php?r=event-details"><i class="fa fa-calendar"></i> Event Info</a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Event Docs</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=event-orderfrom"><i class="fa fa-circle-o"></i> Order Form</a></li>

              </ul>
            </li>
            <li><a href="index.php?r=event-transport"><i class="fa fa-circle-o"></i> Transportation</a></li>
            <li><a href="index.php?r=event-workplan"><i class="fa fa-circle-o"></i> Workplan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Personnel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?r=personnel-details"><i class="fa fa-circle-o"></i> List</a></li>
          </ul>
        </li>
        <li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-bank"></i> <span>Finance</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-gears"></i> <span>Set Up</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=finance-category-type"><i class="fa fa-circle-o"></i> Finance Category Type</a></li>
                <li><a href="index.php?r=finance-accounts"><i class="fa fa-circle-o"></i> Chart of Accounts</a></li>
                <li><a href="index.php?r=finance-tax"><i class="fa fa-circle-o"></i> Tax Set Up</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-cart-arrow-down"></i> <span>Product Set Up</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=finance-items-category"><i class="fa fa-circle-o"></i> Service Category</a></li>
                <li><a href="index.php?r=finance-items-category-type"><i class="fa fa-circle-o"></i> Category Type</a></li>
                <li><a href="index.php?r=finance-items"><i class="fa fa-circle-o"></i> Product / Service</a></li>
                <li><a href="index.php?r=finance-event-activity"><i class="fa fa-circle-o"></i> Activity</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-money"></i> <span>Expense</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=finance-bills"><i class="fa fa-circle-o"></i> Bills</a></li>
                <li><a href="index.php?r=finance-office-requstion-form"><i class="fa fa-circle-o"></i>Office Expense</a></li>
                <li><a href="index.php?r=finance-event-requstion-form"><i class="fa fa-circle-o"></i>Event Expense</a></li>
                <li><a href="index.php?r=finance-lead-requstion-form"><i class="fa fa-circle-o"></i>Lead Expense</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-balance-scale"></i> <span>Expense Payment</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=finance-vendor-credit-memo"><i class="fa fa-circle-o"></i> Vendor Credit Memo</a></li>
                <li><a href="index.php?r=finance-bills-payments"><i class="fa fa-circle-o"></i> Bill Payment</a></li>
                <li><a href="index.php?r=finance-office-requisition-payments"><i class="fa fa-circle-o"></i> Office Requisition Payment</a></li>
                <li><a href="index.php?r=finance-requisition-payments"><i class="fa fa-circle-o"></i> Event Requisition Payment</a></li>
                <li><a href="index.php?r=finance-lead-requisition-payments"><i class="fa fa-circle-o"></i> Lead Requisition Payment</a></li>

              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-file-text-o"></i> <span>Income</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=finance-estimate"><i class="fa fa-circle-o"></i> Estimate</a></li>
                <li><a href="index.php?r=finance-invoice"><i class="fa fa-circle-o"></i> Invoice</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-credit-card"></i> <span>Receipt</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=finance-payment"><i class="fa fa-circle-o"></i> Invoice Payment</a></li>
                <li><a href="index.php?r=finance-credit-memo"><i class="fa fa-circle-o"></i> Credit Memo Payment</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-fw fa-sticky-note-o"></i> <span>Purchase Orders</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?r=finance-customer-purchase-order"><i class="fa fa-circle-o"></i> Customer Purchase Order</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Sunflower Purchase Order</a></li>
              </ul>
            </li>

            <li>
              <a href="index.php?r=reports">
                <i class="fa fa-fw fa-sticky-note-o"></i> <span>Report</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>

            </li>
          </ul>
        </li>

      </ul>
