<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceAccountsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-accounts-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'companyId') ?>

    <?= $form->field($model, 'subAccount') ?>

    <?= $form->field($model, 'accountParentId') ?>

    <?= $form->field($model, 'accountName') ?>

    <?php // echo $form->field($model, 'accountNumber') ?>

    <?php // echo $form->field($model, 'accountsDescription') ?>

    <?php // echo $form->field($model, 'fullyQualifiedName') ?>

    <?php // echo $form->field($model, 'accountsClassification') ?>

    <?php // echo $form->field($model, 'accountsType') ?>

    <?php // echo $form->field($model, 'accountsDetailType') ?>

    <?php // echo $form->field($model, 'accountsStatus') ?>

    <?php // echo $form->field($model, 'accountsCurrentBalance') ?>

    <?php // echo $form->field($model, 'accountsCreateTime') ?>

    <?php // echo $form->field($model, 'accountsUpdatedAt') ?>

    <?php // echo $form->field($model, 'accountsDeleteAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
