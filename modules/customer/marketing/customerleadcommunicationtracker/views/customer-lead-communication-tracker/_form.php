<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
/*Customer Lead*/
use app\modules\customer\marketing\customerlead\models\CustomerLead;

use kartik\select2\Select2;
use app\modules\customer\marketing\customerleadappointment\models\CustomerLeadAppointment;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLeadCommunicationTracker */
/* @var $form yii\widgets\ActiveForm */

?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Communication</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="customer-lead-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'leadAppointId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CustomerLeadAppointment::find()->all(),'id','leadAppointSubject'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Project ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'leadId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CustomerLead::find()->all(),'id','leadprojectName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Project ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                  </div>
                </div>


                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'commLeadTxnDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ]); ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'commLeadMeetingSubject')->textarea(['rows' => 6]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'commLeadPartiesInvolved')->textarea(['rows' => 6]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'commLeadNoted')->textarea(['rows' => 6]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'commLeadNextDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ]); ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'commLeadFormOfCommunication')->textInput() ?>

                  </div>
                </div>

                <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>
                <div class="col-sm-6">
                  <div class="form-group">
                      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>

                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
