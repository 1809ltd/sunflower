<?php

namespace app\modules\finance\offset\outhouse\outoffsetbiils\controllers;

use yii\web\Controller;

/**
 * Default controller for the `outoffsetbiils` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
