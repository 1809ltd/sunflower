<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyDetails */
/* @var $form yii\widgets\ActiveForm */

?>

    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Register Company</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row" >

          <div class="company-details-form">

              <?php $form = ActiveForm::begin(); ?>

          </div>
              <div class="col-sm-3">
                <div class="form-group">
                    <?= $form->field($model, 'companyName')->textInput(['maxlength' => true]) ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                    <?= $form->field($model, 'companyAddress')->textInput(['maxlength' => true]) ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                    <?= $form->field($model, 'companySuite')->textInput(['maxlength' => true]) ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                      <?= $form->field($model, 'companyCity')->textInput(['maxlength' => true]) ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                    <?= $form->field($model, 'companyCounty')->textInput(['maxlength' => true]) ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                    <?= $form->field($model, 'companyPostal')->textInput(['maxlength' => true]) ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                      <?= $form->field($model, 'companyCountry')->textInput(['maxlength' => true]) ?>
                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">

                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">
                    <?= $form->field($model, 'companyLogo')->fileInput() ?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                  <?= $form->field($model, 'companyStatus')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">

                </div>
              </div>

          </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
    <!-- /.box -->
    <!-- Page script -->
