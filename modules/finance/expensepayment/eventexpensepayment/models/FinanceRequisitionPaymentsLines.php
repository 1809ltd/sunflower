<?php

namespace app\modules\finance\expensepayment\eventexpensepayment\models;
// PAyment Model
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
// Event Requstion been paid for
use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionForm;


use Yii;

/**
 * This is the model class for table "finance_requisition_payments_lines".
 *
 * @property int $id
 * @property int $reqpaymentId the main payment cheque
 * @property int $eventReqId Requsition number been paid for
 * @property double $amount
 * @property int $status
 * @property string $deletedAt
 * @property string $created_at
 * @property string $updated_at
 * @property int $userId
 *
 * @property FinanceRequisitionPayments $reqpayment
 * @property FinanceEventRequstionForm $eventReq
 * @property UserDetails $user
 */
class FinanceRequisitionPaymentsLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_requisition_payments_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reqpaymentId', 'eventReqId', 'status', 'userId'], 'integer'],
            [['amount', 'status', 'userId'], 'required'],
            [['amount'], 'number'],
            [['deletedAt', 'created_at', 'updated_at'], 'safe'],
            [['reqpaymentId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceRequisitionPayments::className(), 'targetAttribute' => ['reqpaymentId' => 'id']],
            [['eventReqId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEventRequstionForm::className(), 'targetAttribute' => ['eventReqId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reqpaymentId' => 'Event Payment',
            'eventReqId' => 'Event Requstion Number',
            'amount' => 'Amount',
            'status' => 'Status',
            'deletedAt' => 'Deleted At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'userId' => 'User ID',
        ];
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReqpayment()
    {
        return $this->hasOne(FinanceRequisitionPayments::className(), ['id' => 'reqpaymentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventReq()
    {
        return $this->hasOne(FinanceEventRequstionForm::className(), ['id' => 'eventReqId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
