<?php

namespace app\modules\finance\expense\bills\billslines;

/**
 * billslines module definition class
 */
class billslines extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\bills\billslines\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
