<?php

namespace app\modules\event\eventsetup\eventcaterogies;

/**
 * eventcaterogies module definition class
 */
class eventcaterogies extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventsetup\eventcaterogies\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
