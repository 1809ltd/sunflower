<?php

namespace app\modules\finance\income\invoice\invoiceitems\controllers;

use yii\web\Controller;

/**
 * Default controller for the `invoiceitems` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
