<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventActivity */

$this->title = 'Add Expense Activity';
$this->params['breadcrumbs'][] = ['label' => 'Expense Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-activity-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
