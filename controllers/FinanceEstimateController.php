<?php

namespace app\controllers;

use Yii;
use app\models\FinanceEstimate;
use app\models\FinanceEstimateSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\models\FinanceEstimateLines;
use app\models\FinanceEstimateLinesSearch;

/*Get Event Details*/
use app\models\EventDetails;



/**
 * FinanceEstimateController implements the CRUD actions for FinanceEstimate model.
 */
class FinanceEstimateController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->estimatedUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->estimatedUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceEstimate models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->layout='addformdatatablelayout';
        $searchModel = new FinanceEstimateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceEstimate model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceEstimate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //
    //     $model = new FinanceEstimate();
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }
    //
    //     return $this->render('create', [
    //         'model' => $model,
    //     ]);
    // }
    //
    // /**
    //  * Updates an existing FinanceEstimate model.
    //  * If update is successful, the browser will be redirected to the 'view' page.
    //  * @param integer $id
    //  * @return mixed
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    // public function actionUpdate($id)
    // {
    //
    //     $model = $this->findModel($id);
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     }
    //
    //     return $this->render('update', [
    //         'model' => $model,
    //     ]);
    // }

    public function actionCreate()
    {

        $this->layout='addformdatatablelayout';
        $model = new FinanceEstimate();
        $modelsFinanceEstimateLines = [new FinanceEstimateLines];

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Geting the time created Timestamp
          // $model->customerCreateTime= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          //Getting user Log in details
          // $model->userId= "1";

          //Generating Estimate Number

          $estimateNum=$model->getEstnumber();
          $model->estimateNumber= $estimateNum;
          $model->save();

          //after Saving the customer Details

          $modelsFinanceEstimateLines = Model::createMultiple(FinanceEstimateLines::classname());
          Model::loadMultiple($modelsFinanceEstimateLines, Yii::$app->request->post());


          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceEstimateLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceEstimateLines as $modelFinanceEstimateLines) {

                        $modelFinanceEstimateLines->estimateId = $model->id;
                        $modelFinanceEstimateLines->userId = $model->estimatedCreatedby;
                        $modelFinanceEstimateLines->estimateLinesStatus = $model->estimateTxnStatus;


                          if (! ($flag = $modelFinanceEstimateLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }

          // return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('create', [
              'model' => $model,
              'modelsFinanceEstimateLines' => (empty($modelsFinanceEstimateLines)) ? [new FinanceEstimateLines] : $modelsFinanceEstimateLines
          ]);
        }

    }

    /**
     * Updates an existing FinanceEstimate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);


        $modelsFinanceEstimateLines = $this->getFinanceEstimateLines($model->id);

        if ($model->load(Yii::$app->request->post())) {

              // code...

              //Geting the time created Timestamp
              // $model->customerCreateTime= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

              //Getting user Log in details
              // $model->userId= "1";

              //Generating Estimate Number

              // $estimateNum=$model->getEstnumber();
              // $model->estimateNumber= $estimateNum;
              $model->save();

              //after Saving the customer Details

              $oldIDs = ArrayHelper::map($modelsFinanceEstimateLines, 'id', 'id');
              $modelsFinanceEstimateLines = Model::createMultiple(FinanceEstimateLines::classname(), $modelsFinanceEstimateLines);
              Model::loadMultiple($modelsFinanceEstimateLines, Yii::$app->request->post());
              $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceEstimateLines, 'id', 'id')));

              // ajax validation
              if (Yii::$app->request->isAjax) {
                  Yii::$app->response->format = Response::FORMAT_JSON;
                  return ArrayHelper::merge(
                      ActiveForm::validateMultiple($modelsFinanceEstimateLines),
                      ActiveForm::validate($model)
                  );
              }

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceEstimateLines) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          if (! empty($deletedIDs)) {
                              FinanceEstimateLines::deleteAll(['id' => $deletedIDs]);
                          }
                          foreach ($modelsFinanceEstimateLines as $modelFinanceEstimateLines) {

                              $modelFinanceEstimateLines->estimateId = $model->id;
                              $modelFinanceEstimateLines->userId = $model->estimatedCreatedby;
                              $modelFinanceEstimateLines->estimateLinesStatus = $model->estimateTxnStatus;


                              if (! ($flag = $modelFinanceEstimateLines->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }
                  } catch (Exception $e) {
                      $transaction->rollBack();
                  }
              }

              // return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('update', [
              'model' => $model,
              'modelsFinanceEstimateLines' => (empty($modelsFinanceEstimateLines)) ? [new FinanceEstimateLines] : $modelsFinanceEstimateLines
          ]);
        }

    }

    public function getFinanceEstimateLines($id)
    {
      $model = FinanceEstimateLines::find()->where(['estimateId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing FinanceEstimate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceEstimate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceEstimate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = FinanceEstimate::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*Getting the customers specif Events*/
    public function actionEvents() {
      $out=[];
        // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (isset(Yii::$app->request->post()['depdrop_parents'])) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                $customer_id = $parents[0];

                $out = EventDetails::getCustomerEventList($customer_id);

                return json_encode(['output'=>$out, 'selected'=>'']);
            }
        }
        return json_encode(['output' => '', 'selected' => '']);
    }

    /*Getting the Project Category Name*/
    public function actionEventcategory() {

      // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      if (isset(Yii::$app->request->post()['depdrop_parents'])) {
          $ids = Yii::$app->request->post('depdrop_parents');
          $customer_id = empty($ids[0]) ? null : $ids[0];
          $project_id = empty($ids[1]) ? null : $ids[1];
          if ($customer_id != null && $project_id != null) {

              // $out = EventDetails::getCustomerEventList($customer_id);
              $out = EventDetails::getEventCategory($project_id);

              return json_encode(['output'=>$out, 'selected'=>'']);

          }
      }
      return json_encode(['output'=>'', 'selected'=>'']);

    }
    /*Getting the Project Category Name*/
    public function actionEventname() {

      // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      if (isset(Yii::$app->request->post()['depdrop_parents'])) {
          $ids = Yii::$app->request->post('depdrop_parents');
          $customer_id = empty($ids[0]) ? null : $ids[0];
          $project_id = empty($ids[1]) ? null : $ids[1];
          if ($customer_id != null && $project_id != null) {

              // $out = EventDetails::getCustomerEventList($customer_id);
              $out = EventDetails::getEventName($project_id);

              return json_encode(['output'=>$out, 'selected'=>'']);

          }
      }
      return json_encode(['output'=>'', 'selected'=>'']);

    }
}
