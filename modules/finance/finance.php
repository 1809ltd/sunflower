<?php

namespace app\modules\finance;

/**
 * finance module definition class
 */
class finance extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Finance Set-up Sub Module*/
          'financesetup' => [
              'class' => 'app\modules\finance\financesetup\financesetup',
          ],
          /*Product or Service Set up*/
          'productsetup' => [
              'class' => 'app\modules\finance\productsetup\productsetup',
          ],
          /*Expense*/
          'expense' => [
              'class' => 'app\modules\finance\expense\expense',
          ],
          /*Expense Payment*/
          'expensepayment' => [
              'class' => 'app\modules\finance\expensepayment\expensepayment',
          ],
          /*Income*/
          'income' => [
              'class' => 'app\modules\finance\income\income',
          ],
          /*Income Payment*/
          'receipt' => [
              'class' => 'app\modules\finance\receipt\receipt',
          ],
          /*Purchase Order*/
          'purchaseorder' => [
              'class' => 'app\modules\finance\purchaseorder\purchaseorder',
          ],
          /*Finance Report*/
          'report' => [
              'class' => 'app\modules\finance\financereport\financereport',
          ],
          /*Credit Memo*/
          'creditMemo' => [
              'class' => 'app\modules\finance\creditMemo\creditMemo',
          ],
          /*Offset*/
          'offset' => [
              'class' => 'app\modules\finance\offset\offset',
          ],
          /*Transfer*/
          'transfer' => [
              'class' => 'app\modules\finance\transfer\transfer',
          ],
          /*Journal entry*/
          'journal' => [
            'class' => 'app\modules\finance\journal\journal',
        ],

        ];

        // custom initialization code goes here
    }
}
