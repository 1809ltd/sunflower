<?php

namespace app\modules\finance\financereport\companyfinancials\balancesheet\models;
/*All transactions */
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
// Customer Statements to get the receivables amount
use app\modules\finance\financereport\customerreport\models\CustomerStatement;
// Vendor Statements for Prepaid expense and payables
use app\modules\finance\financereport\vendorreport\models\VendorStatement;
// Get pre madePayment
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines;
/*Tax Reports*/
use app\modules\finance\financereport\companyfinancials\taxreport\models\TaxReport;
// Asset Amortizations
use app\modules\inventory\asset\assetamortization\models\AssetAmortization;
use Yii;

/**
 * This is the model class for table "customer_statement".
 *
 * @property int endDate
 * @property string $date
 * @property string $transactionNo
 * @property string $Details
 * @property string $amountinvoiced
 * @property string $amountPaid
 */
class BalanceSheet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    // public static function tableName()
    // {
    //     return 'customer_statement';
    // }
    public $endDate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
          [['endate'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'endDate' => 'As of',
        ];
    }

    /**
    * Start of
    * Current Asset for the balance Sheet alone
    **/

    // Getting the cash accounts only based time
    public function getallBlancecashtime($endDate)
    {
      $cureentAsstCash = AllTransactions::find()->select(['all_transactions.accountName,all_transactions.accountId,all_transactions.accountParentId,Sum( all_transactions.dr_amount ) AS dr_amount,Sum( all_transactions.cr_amount ) AS cr_amount,COALESCE ( SUM( all_transactions.dr_amount ) - SUM( all_transactions.`cr_amount` ), 0 ) AS balance_amount'])
                                      ->Where('Date( `all_transactions`.`transactionDate` ) <= :date', [':date' => $endDate])
                                      ->andWhere('all_transactions.`status` NOT IN ( 0, 3 )')
                                      ->andWhere('all_transactions.accountParentId = 6')
                                      ->groupBy(['accountId'])
                                      ->orderBy(['accountParentId' => SORT_ASC,])
                                      ->all();
        // print_r($paymentsonHold);

        return $cureentAsstCash;

    }

    // Getting the Total Amount based time
    public function getreceivableAmount($endDate)
    {
      $recviableTT = CustomerStatement::find()->select(['COALESCE(Sum(amountinvoiced) - Sum(amountPaid ),0) AS balance_amount'])
                                      ->Where('date <= :date', [':date' => $endDate])
                                      ->all();

      return $recviableTT[0]["balance_amount"];


    }
    // Getting the TotalValue of Stock Amount based time
    public function getInventoryAmount($endDate)
    {
      $inventoryStock=0;
      return $inventoryStock;

    }
    // Getting the TotalValue of PrepaidExpense
    public function getPrepaidAmount($endDate)
    {
      $prepaidbills=VendorStatement::find()->select(['COALESCE(Sum(amountinvoiced) - Sum(amountPaid ),0) AS balance_amount'])
                                      ->Where('date >= :date', [':date' => $endDate])
                                      ->all();

      return $prepaidbills[0]["balance_amount"];;

    }
    // Getting the TotalValue of Stock Amount based time
    public function getEmployeeAdvance($endDate)
    {
      $inventoryStock=0;
      return $inventoryStock;

    }
    // Getting the Total for the Fixed Assets Amount based time
    public function getfixedAssetAmount($endDate)
    {

      $connectionfixed = Yii::$app->getDb();
      $connectionfixed = $connectionfixed->createCommand("SELECT
                                                           asset_amortization.amortizationDate,
                                                           Sum( asset_amortization.endBalance ),
                                                           asset_registration.assetDescription,
                                                           asset_registration.assetCategory,
                                                           asset_category.assetCategoryName
                                                         FROM
                                                           asset_amortization
                                                           LEFT JOIN asset_registration ON asset_amortization.assetId = asset_registration.id
                                                           LEFT JOIN asset_category ON asset_registration.assetCategory = asset_category.id
                                                         WHERE
                                                           MONTH (asset_amortization.amortizationDate) = MONTH ('$endDate')
                                                           AND YEAR (asset_amortization.amortizationDate) = YEAR ('$endDate')
                                                         GROUP BY
                                                           asset_registration.assetCategory
                                                         ORDER BY
                                                           asset_category.assetCategoryName ASC
                                                           ");
      $fixedAssetAmount = $connectionfixed->query();
      // $fixedAssetAmount = AssetAmortization::find()->select(['asset_amortization.amortizationDate','COALESCE(Sum(asset_amortization.endBalance),0) AS endBalance','asset_registration.assetDescription','asset_registration.assetCategory','asset_category.assetCategoryName'])
      //                                         ->innerJoinWith('asset')
      //                                         ->innerJoinWith('asset.assetCategory0')
      //                                         ->Where('asset_registration.assetStatus <> 0')
      //                                         ->andWhere('MONTH (asset_amortization.amortizationDate ) = MONTH(:date)', [':date' => $endDate])
      //                                         ->andWhere('YEAR (asset_amortization.amortizationDate ) = YEAR(:date)', [':date' => $endDate])
      //                                         ->groupBy(['asset_registration.assetCategory'])
      //                                         ->orderBy(['asset_category.assetCategoryName' => SORT_ASC,])
      //                                         ->all();

      return $fixedAssetAmount;

    }
    /**
    * End of
    * Current Asset for the balance Sheet alone
    **/

    /**
    * Start of
    * Current LIABILITIES for the balance Sheet alone
    **/
    // Getting the Total Amount based time
    public function getpayablesAmount($endDate)
    {
      $resultpayables = VendorStatement::find()->select(['COALESCE(Sum(amountinvoiced) - Sum(amountPaid ),0) AS balance_amount'])
                                      ->Where('date <= :date', [':date' => $endDate])
                                      ->all();

      return $resultpayables[0]["balance_amount"];

    }
    // Getting the Total Amount based time
    public function getEventPrepayments($endDate)
    {
      $eventPrepayemnt = FinancePaymentLines::find()->innerJoinWith('invoice')
                                                   ->innerJoinWith('invoice.invoiceProject')
                                                   ->Where('finance_invoice.invoiceTxnStatus <> 0')
                                                   ->andWhere('finance_payment_lines.`status` NOT IN ( 0, 3 )')
                                                   ->andWhere('event_details.eventStartDate >= :date', [':date' => $endDate])
                                                   ->sum('finance_payment_lines.amount');

      return $eventPrepayemnt;

    }
    // Getting the Total Amount based time
    public function getTaxpayablesAmount($endDate)
    {
      $resultTaxpayables = TaxReport::find()->select(['taxCode', 'taxRate','COALESCE(Sum(dr_amount) - Sum(cr_amount),0) AS balance_amount'])
                                      ->Where('date <= :date', [':date' => $endDate])
                                      ->groupBy(['taxCode'])
                                      ->andWhere('typeofRecipeint >= :typeofRecipeint', [':typeofRecipeint' => "customer"])
                                      ->all();

      return $resultTaxpayables;
    }
    /**
    * End of
    * Current LIABILITIES for the balance Sheet alone
    **/

    /**
    * Start of
    * Equity for the balance Sheet alone
    **/

    // Getting the Total Amount based time
    public function getOwnersinputAmount($endDate)
    {
      $ownerEquity = AllTransactions::find()->select(['all_transactions.accountName,all_transactions.accountId,all_transactions.accountParentId,Sum( all_transactions.dr_amount ) AS dr_amount,Sum( all_transactions.cr_amount ) AS cr_amount,COALESCE ( SUM( all_transactions.dr_amount ) - SUM( all_transactions.`cr_amount` ), 0 ) AS balance_amount'])
                                      ->Where('Date( `all_transactions`.`transactionDate` ) <= :date', [':date' => $endDate])
                                      ->andWhere('all_transactions.`status` NOT IN ( 0, 3 )')
                                      ->andWhere('all_transactions.accountParentId = 14')
                                      ->groupBy(['accountId'])
                                      ->orderBy(['accountParentId' => SORT_ASC,])
                                      ->all();

      return $ownerEquity;

    }
    // Getting the PRevious Year Total Amount based time
    public function getpreviousyearAmount($endDate)
    {
      // code...

      $startDate= date('Y-01-01');

      $revenue = AllTransactions::find()
      // ->select(['SUM(all_transactions.cr_amount) as cr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                      ->Where('status > :status', [':status' => 0])
                                      ->andWhere('transactionCategory = :transactionCategory', [':transactionCategory' => "Revenue"])
                                      ->andFilterWhere(['between', 'transactionDate', $startDate, $endDate])
                                      ->sum('cr_amount');

      // Get the $COGS
      $COGS = AllTransactions::find()
      // ->select(['SUM(all_transactions.dr_amount) as dr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                      ->Where('status > :status', [':status' => 0])
                                      ->orWhere('transactionClassification = :transactionClassification', [':transactionClassification' => "Event expense"])
                                      ->andWhere('transactionClassification = :transactionClassification and projectId IS NOT NULL', [':transactionClassification' => "Bill Expense"])
                                      ->andFilterWhere(['between', 'transactionDate', $startDate, $endDate])
                                      ->sum('dr_amount');
    // Get the Operating Cost
    $operating = AllTransactions::find()
    // ->select(['SUM(all_transactions.dr_amount) as dr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                    ->Where('status > :status', [':status' => 0])
                                    ->orWhere('transactionClassification = :transactionClassification', [':transactionClassification' => "Office Expense"])
                                    ->orWhere('transactionClassification = :transactionClassification', [':transactionClassification' => "Lead expense"])
                                    ->andWhere('transactionClassification = :transactionClassification and projectId IS NULL', [':transactionClassification' => "Bill Expense"])
                                    ->andFilterWhere(['between', 'transactionDate', $startDate, $endDate])
                                    ->sum('dr_amount');


      $netincome =$revenue-$COGS-$operating;

      return $netincome;
    }
    // Getting the PRevious Year Total Amount based time
    public function getcurrentyearAmount($endDate)
    {
      // code...
      $startDate= date('Y-01-01');

      // $endDate= date('Y-m-d');
      $revenue = AllTransactions::find()
      // ->select(['SUM(all_transactions.cr_amount) as cr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                      ->Where('status > :status', [':status' => 0])
                                      ->andWhere('transactionCategory = :transactionCategory', [':transactionCategory' => "Revenue"])
                                      ->andFilterWhere(['between', 'transactionDate', $startDate, $endDate])
                                      ->sum('cr_amount');

      // Get the $COGS
      $COGS = AllTransactions::find()
      // ->select(['SUM(all_transactions.dr_amount) as dr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                      ->Where('status > :status', [':status' => 0])
                                      ->orWhere('transactionClassification = :transactionClassification', [':transactionClassification' => "Event expense"])
                                      ->andWhere('transactionClassification = :transactionClassification and projectId IS NOT NULL', [':transactionClassification' => "Bill Expense"])
                                      ->andFilterWhere(['between', 'transactionDate', $startDate, $endDate])
                                      ->sum('dr_amount');
    // Get the Operating Cost
    $operating = AllTransactions::find()
    // ->select(['SUM(all_transactions.dr_amount) as dr_amount','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                    ->Where('status > :status', [':status' => 0])
                                    ->orWhere('transactionClassification = :transactionClassification', [':transactionClassification' => "Office Expense"])
                                    ->orWhere('transactionClassification = :transactionClassification', [':transactionClassification' => "Lead expense"])
                                    ->andWhere('transactionClassification = :transactionClassification and projectId IS NULL', [':transactionClassification' => "Bill Expense"])
                                    ->andFilterWhere(['between', 'transactionDate', $startDate, $endDate])
                                    ->sum('dr_amount');

      $netincome =$revenue-$COGS-$operating;

      return $netincome;
    }





}
