<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AddFormDatatableEventAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       // 'css/site.css',
       /*BEGIN BASE CSS STYLE*/

       /*Additional Css*/
       'adminlte/dist/css/pvr.css',
       /* Bootstrap 3.3.7 */
      'adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css',
      /* Font Awesome */
      'adminlte/bower_components/font-awesome/css/font-awesome.min.css',
      /* Ionicons */
      'adminlte/bower_components/Ionicons/css/ionicons.min.css',
      /* daterange picker */
      'adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css',
      /* bootstrap datepicker */
      'adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
      /* iCheck for checkboxes and radio inputs */
      'adminlte/plugins/iCheck/all.css',
      /* Bootstrap Color Picker */
      'adminlte/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css',
      /* Bootstrap time Picker */
      'adminlte/plugins/timepicker/bootstrap-timepicker.min.css',
      /* Theme style */
      'adminlte/dist/css/AdminLTE.min.css',
      /* AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. */
      'adminlte/dist/css/skins/_all-skins.min.css',
      /*Full calendar*/
      'adminlte/bower_components/fullcalendar/dist/fullcalendar.min.css',
      /* Google Font */
      'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
      /* END BASE CSS STYLE */
      /* BEGIN PAGE LEVEL STYLE */
      /* END PAGE LEVEL STYLE */
    ];
    public $js = [
      /* BEGIN BASE JS */
      /*pop up Js*/
      'popup/popupform.js',
      /* Select2 */
      'adminlte/bower_components/select2/dist/js/select2.full.min.js',
      /* InputMask */
      'adminlte/plugins/input-mask/jquery.inputmask.js',
      'adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js',
      'adminlte/plugins/input-mask/jquery.inputmask.extensions.js',
      'adminlte/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
      // /* bootstrap time picker */
      'adminlte/plugins/timepicker/bootstrap-timepicker.min.js',
      // /* SlimScroll */
      'adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
      // /* iCheck 1.0.1 */
      'adminlte/plugins/iCheck/icheck.min.js',
      // /* FastClick */
      'adminlte/bower_components/fastclick/lib/fastclick.js',
      // /* AdminLTE App */
      'adminlte/dist/js/adminlte.min.js',
      /* AdminLTE for demo purposes */
      'adminlte/dist/js/demo.js',
      // /* Page script */
      //
      // /*DataTable*/
      //
      // 'bower_components/datatables.net/js/jquery.dataTables.min.js',
      // 'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
      //
      // /* ChartJS */
      'adminlte/bower_components/chart.js/Chart.js',
      /*Morris.js charts*/
      'adminlte/bower_components/raphael/raphael.min.js',
      'adminlte/bower_components/morris.js/morris.min.js',
      'adminlte/bower_components/morris.js/morris.min.js',
      /*fullCalendar Js*/
      'adminlte/bower_components/moment/moment.js',
      'adminlte/bower_components/fullcalendar/dist/fullcalendar.min.js',
      /* END BASE JS */
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
