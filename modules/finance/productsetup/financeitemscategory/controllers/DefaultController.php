<?php

namespace app\modules\finance\productsetup\financeitemscategory\controllers;

use yii\web\Controller;

/**
 * Default controller for the `financeitemscategory` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
