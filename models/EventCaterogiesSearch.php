<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventCaterogies;

/**
 * EventCaterogiesSearch represents the model behind the search form of `app\models\EventCaterogies`.
 */
class EventCaterogiesSearch extends EventCaterogies
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'eventCatStatus', 'userId'], 'integer'],
            [['eventCatName', 'eventCatDesc', 'eventCatCreatedAt', 'eventCatLastUpdatedAt', 'eventCatDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventCaterogies::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'eventCatStatus' => $this->eventCatStatus,
            'eventCatCreatedAt' => $this->eventCatCreatedAt,
            'eventCatLastUpdatedAt' => $this->eventCatLastUpdatedAt,
            'eventCatDeletedAt' => $this->eventCatDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'eventCatName', $this->eventCatName])
            ->andFilterWhere(['like', 'eventCatDesc', $this->eventCatDesc]);

        return $dataProvider;
    }
}
