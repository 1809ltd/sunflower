<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AssetDispose;

/**
 * AssetDisposeSearch represents the model behind the search form of `app\models\AssetDispose`.
 */
class AssetDisposeSearch extends AssetDispose
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assetId', 'userId'], 'integer'],
            [['dateDisposed', 'disposeTo', 'disposal Reason', 'disposeTimeStamp', 'disposeUpdatedAt', 'disposeDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetDispose::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assetId' => $this->assetId,
            'dateDisposed' => $this->dateDisposed,
            'disposeTimeStamp' => $this->disposeTimeStamp,
            'disposeUpdatedAt' => $this->disposeUpdatedAt,
            'disposeDeletedAt' => $this->disposeDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'disposeTo', $this->disposeTo]);
            //->andFilterWhere(['like', 'disposal Reason', $this->disposal Reason]);

        return $dataProvider;
    }
}
