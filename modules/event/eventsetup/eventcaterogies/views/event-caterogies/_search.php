<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventCaterogiesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-caterogies-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'eventCatName') ?>

    <?= $form->field($model, 'eventCatDesc') ?>

    <?= $form->field($model, 'eventCatStatus') ?>

    <?= $form->field($model, 'eventCatCreatedAt') ?>

    <?php // echo $form->field($model, 'eventCatLastUpdatedAt') ?>

    <?php // echo $form->field($model, 'eventCatDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
