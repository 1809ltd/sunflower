<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines */

$this->title = 'Update Finance Vendor Offset Memo Lines: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Offset Memo Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-vendor-offset-memo-lines-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
