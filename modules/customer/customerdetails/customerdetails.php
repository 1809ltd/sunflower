<?php

namespace app\modules\customer\customerdetails;

/**
 * customerdetails module definition class
 */
class customerdetails extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\customer\customerdetails\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
