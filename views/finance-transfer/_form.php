<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\transfer\models\FinanceTransfer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-transfer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tranferRefernce')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tranferFromAccountRef')->textInput() ?>

    <?= $form->field($model, 'tranferToAccountRef')->textInput() ?>

    <?= $form->field($model, 'tranferAmount')->textInput() ?>

    <?= $form->field($model, 'transferCharges')->textInput() ?>

    <?= $form->field($model, 'tranferTxnDate')->textInput() ?>

    <?= $form->field($model, 'tranferCreatedAt')->textInput() ?>

    <?= $form->field($model, 'tranferUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'tranferDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
