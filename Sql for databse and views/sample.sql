-- General Ledger
-- 1) All Office requstion form
SELECT
  finance_office_requstion_form_list.id AS transactionId,
  finance_office_requstion_form_list.reqId AS referenceId,
  finance_office_requstion_form.requstionNumber AS referenceCode,
  '' AS transactionCode,
  finance_office_requstion_form_list.activityId AS projectId,
  '' AS recipientId,
  '' AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_event_activity.activityName AS transactionName,
  CONCAT(
    "Office Requstion: ",
    finance_office_requstion_form.requstionNumber,
    " ->",
    finance_office_requstion_form_list.description
  ) AS tranctionDescription,
  finance_office_requstion_form_list.amont AS dr_amount,
  '0' AS cr_amount,
  finance_office_requstion_form.date AS transactionDate,
  finance_office_requstion_form_list.createdAt AS createdAt,
  finance_office_requstion_form_list.`status` AS `status`,
  'Expense' AS transactionCategory,
  'Office Expense' AS transactionClassification,
  'finance_office_requstion_form_list' AS transactionTable,
  'finance_office_requstion_form' AS referenceTable
FROM
  (
    (
      (
        finance_office_requstion_form_list
        JOIN finance_office_requstion_form ON (
          (
            finance_office_requstion_form_list.reqId = finance_office_requstion_form.id
          )
        )
      )
      JOIN finance_event_activity ON (
        (
          finance_office_requstion_form_list.activityId = finance_event_activity.id
        )
      )
    )
    JOIN finance_accounts ON (
      (
        finance_event_activity.account = finance_accounts.id
      )
    )
  )
UNION ALL
  -- Office Requstion payment
  -- 2 Office Requstion Payment
SELECT
  `finance_office_requisition_payments`.`id` AS `transactionId`,
  `finance_office_requstion_form`.`id` AS `referenceId`,
  `finance_office_requstion_form`.`requstionNumber` AS `referenceCode`,
  `finance_office_requisition_payments`.`reqrecepitNumber` AS `transactionCode`,
  '' AS `projectId`,
  '' AS recipientId,
  '' AS recipientName,
  `finance_accounts`.`accountParentId` AS `accountParentId`,
  `finance_accounts`.`accountsClassification` AS `accountsclassfication`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `accountName`,
  `finance_office_requisition_payments`.`reqpaymentRefence` AS `transactionName`,
  CONCAT(
    "Office Requstion Payment: ",
    `finance_office_requisition_payments`.`reqpaymentName`,
    " -> for ",
    finance_office_requstion_form.requstionNumber
  ) AS `tranctionDescription`,
  '0' AS `dr_amount`,
  `finance_office_requisition_payments`.`reqpaymentTotalAmt` AS `cr_amount`,
  `finance_office_requisition_payments`.`reqpaymentTxnDate` AS `transactionDate`,
  `finance_office_requisition_payments`.`reqpaymentCreateAt` AS `createdAt`,
  `finance_office_requisition_payments`.`reqpaymentStatus` AS `status`,
  'Expense Payment' AS `transactionCategory`,
  'Office Expense Payment' AS `transactionClassification`,
  'finance_office_requisition_payments' AS `transactionTable`,
  'finance_office_requstion_form' AS `referenceTable`
FROM
  (
    (
      `finance_office_requisition_payments`
      JOIN `finance_office_requstion_form` ON (
        (
          `finance_office_requisition_payments`.`reqpaymentLinkedTxn` = `finance_office_requstion_form`.`requstionNumber`
        )
      )
    )
    JOIN `finance_accounts` ON (
      (
        `finance_office_requisition_payments`.`reqpaymentAccount` = `finance_accounts`.`id`
      )
    )
  )
UNION ALL
  -- 3 Lead Requistion
SELECT
  `finance_lead_requstion_form_list`.`id` AS `transactionId`,
  `finance_lead_requstion_form_list`.`reqId` AS `referenceId`,
  `finance_lead_requstion_form`.`requstionNumber` AS `referenceCode`,
  '' AS `transactionCode`,
  `finance_lead_requstion_form_list`.`activityId` AS `projectId`,
  '' AS recipientId,
  '' AS recipientName,
  `finance_accounts`.`accountParentId` AS `accountParentId`,
  `finance_accounts`.`accountsClassification` AS `accountsclassfication`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `accountName`,
  `finance_event_activity`.`activityName` AS `transactionName`,
  CONCAT(
    "Marketing Expense for Lead: ",
    finance_lead_requstion_form.requstionNumber,
    " -> for ",
    `finance_lead_requstion_form_list`.`description`
  ) AS `tranctionDescription`,
  `finance_lead_requstion_form_list`.`amont` AS `dr_amount`,
  '0' AS `cr_amount`,
  `finance_lead_requstion_form`.`date` AS `transactionDate`,
  `finance_lead_requstion_form_list`.`createdAt` AS `createdAt`,
  `finance_lead_requstion_form_list`.`status` AS `status`,
  'Expense' AS `transactionCategory`,
  'Lead expense' AS `transactionClassification`,
  'finance_lead_requstion_form_list' AS `transactionTable`,
  'finance_lead_requstion_form' AS `referenceTable`
FROM
  (
    (
      (
        `finance_lead_requstion_form_list`
        JOIN `finance_lead_requstion_form` ON (
          (
            `finance_lead_requstion_form_list`.`reqId` = `finance_lead_requstion_form`.`id`
          )
        )
      )
      JOIN `finance_event_activity` ON (
        (
          `finance_lead_requstion_form_list`.`activityId` = `finance_event_activity`.`id`
        )
      )
    )
    JOIN `finance_accounts` ON (
      (
        `finance_event_activity`.`account` = `finance_accounts`.`id`
      )
    )
  )
UNION ALL
  -- 4 Lead Payement
SELECT
  `finance_lead_requisition_payments`.`id` AS `transactionId`,
  `finance_lead_requstion_form`.`id` AS `referenceId`,
  `finance_lead_requstion_form`.`requstionNumber` AS `referenceCode`,
  `finance_lead_requisition_payments`.`reqrecepitNumber` AS `transactionCode`,
  '' AS `projectId`,
  '' AS recipientId,
  '' AS recipientName,
  `finance_accounts`.`accountParentId` AS `accountParentId`,
  `finance_accounts`.`accountsClassification` AS `accountsclassfication`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `accountName`,
  `finance_lead_requisition_payments`.`reqpaymentRefence` AS `transactionName`,
  CONCAT(
    "Marketing Expense for Lead Payment: : ",
    finance_lead_requisition_payments.reqrecepitNumber,
    " -> for ",
    `finance_lead_requisition_payments`.`reqpaymentName`
  ) AS `tranctionDescription`,
  '0' AS `dr_amount`,
  `finance_lead_requisition_payments`.`reqpaymentTotalAmt` AS `cr_amount`,
  `finance_lead_requisition_payments`.`reqpaymentTxnDate` AS `transactionDate`,
  `finance_lead_requisition_payments`.`reqpaymentCreateAt` AS `createdAt`,
  `finance_lead_requisition_payments`.`reqpaymentStatus` AS `status`,
  'Expense Payment' AS `transactionCategory`,
  'Lead Expense Payment' AS `transactionClassification`,
  'finance_lead_requisition_payments' AS `transactionTable`,
  'finance_lead_requstion_form' AS `referenceTable`
FROM
  (
    (
      `finance_lead_requisition_payments`
      JOIN `finance_lead_requstion_form` ON (
        (
          `finance_lead_requisition_payments`.`reqpaymentLinkedTxn` = `finance_lead_requstion_form`.`requstionNumber`
        )
      )
    )
    JOIN `finance_accounts` ON (
      (
        `finance_lead_requisition_payments`.`reqpaymentAccount` = `finance_accounts`.`id`
      )
    )
  )
UNION ALL
  -- 5 Event Requstion
SELECT
  `finance_event_requstion_form_list`.`id` AS `transactionId`,
  `finance_event_requstion_form_list`.`reqId` AS `referenceId`,
  `finance_event_requstion_form`.`requstionNumber` AS `referenceCode`,
  '' AS `transactionCode`,
  `finance_event_requstion_form_list`.`activityId` AS `projectId`,
  '' AS recipientId,
  '' AS recipientName,
  `finance_accounts`.`accountParentId` AS `accountParentId`,
  `finance_accounts`.`accountsClassification` AS `accountsclassfication`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `accountName`,
  `finance_event_activity`.`activityName` AS `transactionName`,
  CONCAT(
    "Event Requstion: ",
    finance_event_requstion_form.requstionNumber,
    " ->",
    finance_event_requstion_form_list.description
  ) AS `tranctionDescription`,
  `finance_event_requstion_form_list`.`amont` AS `dr_amount`,
  '0' AS `cr_amount`,
  `finance_event_requstion_form`.`date` AS `transactionDate`,
  `finance_event_requstion_form_list`.`createdAt` AS `createdAt`,
  `finance_event_requstion_form_list`.`status` AS `status`,
  'Expense' AS `transactionCategory`,
  'Event Expense' AS `transactionClassification`,
  'finance_event_requstion_form_list' AS `transactionTable`,
  'finance_event_requstion_form' AS `referenceTable`
FROM
  (
    (
      (
        `finance_event_requstion_form_list`
        JOIN `finance_event_requstion_form` ON (
          (
            `finance_event_requstion_form_list`.`reqId` = `finance_event_requstion_form`.`id`
          )
        )
      )
      JOIN `finance_event_activity` ON (
        (
          `finance_event_requstion_form_list`.`activityId` = `finance_event_activity`.`id`
        )
      )
    )
    JOIN `finance_accounts` ON (
      (
        `finance_event_activity`.`account` = `finance_accounts`.`id`
      )
    )
  )
UNION ALL
  -- 6  Event Requstion Payment
SELECT
  `finance_requisition_payments`.`id` AS `transactionId`,
  `finance_event_requstion_form`.`id` AS `referenceId`,
  `finance_event_requstion_form`.`requstionNumber` AS `referenceCode`,
  `finance_requisition_payments`.`reqrecepitNumber` AS `transactionCode`,
  '' AS `projectId`,
  '' AS recipientId,
  '' AS recipientName,
  `finance_accounts`.`accountParentId` AS `accountParentId`,
  `finance_accounts`.`accountsClassification` AS `accountsclassfication`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `accountName`,
  `finance_requisition_payments`.`reqpaymentRefence` AS `transactionName`,
  CONCAT(
    "Event Requstion Payment: ",
    `finance_requisition_payments`.`reqpaymentName`,
    " ->",
    `finance_requisition_payments`.`reqrecepitNumber`
  ) AS `tranctionDescription`,
  '0' AS `dr_amount`,
  `finance_requisition_payments`.`reqpaymentTotalAmt` AS `cr_amount`,
  `finance_requisition_payments`.`reqpaymentTxnDate` AS `transactionDate`,
  `finance_requisition_payments`.`reqpaymentCreateAt` AS `createdAt`,
  `finance_requisition_payments`.`reqpaymentStatus` AS `status`,
  'Expense Payment' AS `transactionCategory`,
  'Event expense Payment' AS `transactionClassification`,
  'finance_requisition_payments' AS `transactionTable`,
  'finance_event_requstion_form' AS `referenceTable`
FROM
  (
    (
      `finance_requisition_payments`
      JOIN `finance_event_requstion_form` ON (
        (
          `finance_requisition_payments`.`reqpaymentLinkedTxn` = `finance_event_requstion_form`.`requstionNumber`
        )
      )
    )
    JOIN `finance_accounts` ON (
      (
        `finance_requisition_payments`.`reqpaymentAccount` = `finance_accounts`.`id`
      )
    )
  )
UNION ALL
  -- 7 Bill Lines
SELECT
  finance_bills_lines.id AS transactionId,
  finance_bills.id AS referenceId,
  finance_bills.billRef AS referenceCode,
  '' AS transactionCode,
  finance_bills.projectId AS projectId,
  finance_bills.vendorId AS recipientId,
  vendor_company_details.vendorCompanyName AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_bills_lines.biilsLinesVendorsRef AS transactionName,
  CONCAT(
    "Bill Expense: ",
    `finance_bills`.`billRef`,
    " ->",
    `finance_bills_lines`.`biilsLinesDescription`
  ) AS tranctionDescription,
  finance_bills_lines.biilsLinesAmount AS dr_amount,
  '0' AS cr_amount,
  finance_bills.billDueDate AS transactionDate,
  finance_bills_lines.billsLinesCreatedAt AS createdAt,
  finance_bills_lines.billsLinesBillableStatus AS `status`,
  'Expense' AS transactionCategory,
  'Bill Expense' AS transactionClassification,
  'finance_bills_lines' AS transactionTable,
  'finance_bills' AS referenceTable
FROM
  (
    (
      finance_bills_lines
      JOIN finance_bills ON (
        (finance_bills_lines.biilsId = finance_bills.id)
      )
    )
    JOIN finance_accounts ON (
      (
        finance_bills_lines.accountId = finance_accounts.id
      )
    )
  )
  INNER JOIN vendor_company_details ON finance_bills.vendorId = vendor_company_details.id
UNION ALL
  -- 8  Payment for bills
SELECT
  `finance_bills_payments`.`id` AS `transactionId`,
  `finance_bills`.`id` AS `referenceId`,
  `finance_bills`.`billRef` AS `referenceCode`,
  '' AS `transactionCode`,
  '' AS `projectId`,
  '' AS recipientId,
  '' AS recipientName,
  `finance_accounts`.`accountParentId` AS `accountParentId`,
  `finance_accounts`.`accountsClassification` AS `accountsclassfication`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `accountName`,
  `finance_bills_payments`.`billPaymentPrivateNote` AS `transactionName`,
  CONCAT(
    "Bill Expense Payment for : ",
    `finance_bills`.`billRef`,
    " ->",
    `finance_bills_payments`.`billPaymentPrivateNote`
  ) AS `tranctionDescription`,
  '0' AS `dr_amount`,
  `finance_bills_payments`.`billPaymentTotalAmt` AS `cr_amount`,
  `finance_bills_payments`.`billPaymentCreatedAt` AS `transactionDate`,
  `finance_bills_payments`.`billPaymentCreatedAt` AS `createdAt`,
  `finance_bills_payments`.`userId` AS `status`,
  'Expense Payment' AS `transactionCategory`,
  'Bill Payment expense' AS `transactionClassification`,
  'finance_bills_payments' AS `transactionTable`,
  'finance_bills' AS `referenceTable`
FROM
  (
    (
      `finance_bills_payments`
      JOIN `finance_bills` ON (
        (
          `finance_bills_payments`.`billId` = `finance_bills`.`id`
        )
      )
    )
    JOIN `finance_accounts` ON (
      (
        `finance_bills_payments`.`billPaymentaccount` = `finance_accounts`.`id`
      )
    )
  )
UNION ALL
  -- 9 Credit Memo Vendor
SELECT
  finance_vendor_credit_memo_lines.id AS transactionId,
  finance_vendor_credit_memo.id AS referenceId,
  finance_vendor_credit_memo.refernumber AS referenceCode,
  finance_bills.billRef AS transactionCode,
  '' AS projectId,
  finance_vendor_credit_memo.vendorId AS recipientId,
  vendor_company_details.vendorCompanyName AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_bills.billRef AS transactionName,
  CONCAT(
    "Bill Expense Payment Credit Memo for : ",
    `finance_bills`.`billRef`,
    " ->",
    `finance_vendor_credit_memo_lines`.`vendorCreditMemoLineDescription`
  ) AS tranctionDescription,
  '0' AS dr_amount,
  finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount AS cr_amount,
  finance_vendor_credit_memo.vendorCreditMemoTxnDate AS transactionDate,
  finance_vendor_credit_memo_lines.vendorCreditMemoLineCreatedAt AS createdAt,
  finance_vendor_credit_memo_lines.vendorCreditMemoLineStatus AS `status`,
  'Expense Vendor CreditNote Payment' AS transactionCategory,
  'Bill Payment CreditNote expense' AS transactionClassification,
  'finance_vendor_credit_memo_lines' AS transactionTable,
  'finance_bills' AS referenceTable
FROM
  (
    (
      (
        finance_vendor_credit_memo_lines
        JOIN finance_vendor_credit_memo ON (
          (
            finance_vendor_credit_memo_lines.vendorCreditMemoId = finance_vendor_credit_memo.id
          )
        )
      )
      JOIN finance_bills ON (
        (
          finance_vendor_credit_memo_lines.billRef = finance_bills.id
        )
      )
    )
    JOIN finance_accounts ON (
      (
        finance_vendor_credit_memo.vendorCreditMemoAccount = finance_accounts.id
      )
    )
  )
  INNER JOIN vendor_company_details ON finance_vendor_credit_memo.vendorId = vendor_company_details.id
  AND finance_bills.vendorId = vendor_company_details.id
UNION All
  -- 10  Offset Memo Vendor
SELECT
  finance_vendor_offset_memo_lines.id AS transactionId,
  finance_vendor_offset_memo.id AS referenceId,
  finance_vendor_offset_memo.refernumber AS referenceCode,
  finance_bills.billRef AS transactionCode,
  '' AS projectId,
  finance_vendor_offset_memo.vendorId AS recipientId,
  vendor_company_details.vendorCompanyName AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_bills.billRef AS transactionName,
  CONCAT(
    "Bill Expense Payment Offset for : ",
    `finance_bills`.`billRef`,
    " ->",
    `finance_vendor_offset_memo_lines`.`vendorOffsetMemoLineDescription`
  ) AS tranctionDescription,
  '0' AS dr_amount,
  finance_vendor_offset_memo_lines.vendorOffsetMemoLineAmount AS cr_amount,
  finance_vendor_offset_memo.vendorOffsetMemoTxnDate AS transactionDate,
  finance_vendor_offset_memo_lines.vendorOffsetMemoLineCreatedAt AS createdAt,
  finance_vendor_offset_memo_lines.vendorOffsetMemoLineStatus AS `status`,
  'Expense Vendor Offset Payment' AS transactionCategory,
  'Bill Payment Offset expense' AS transactionClassification,
  'finance_vendor_offset_memo_lines' AS transactionTable,
  'finance_bills' AS referenceTable
FROM
  (
    (
      (
        finance_vendor_offset_memo_lines
        JOIN finance_vendor_offset_memo ON (
          (
            finance_vendor_offset_memo_lines.vendorOffsetMemoId = finance_vendor_offset_memo.id
          )
        )
      )
      JOIN finance_bills ON (
        (
          finance_vendor_offset_memo_lines.billRef = finance_bills.id
        )
      )
    )
    JOIN finance_accounts ON (
      (
        finance_vendor_offset_memo.vendorOffsetMemoAccount = finance_accounts.id
      )
    )
  )
  INNER JOIN vendor_company_details ON finance_vendor_offset_memo_lines.vendorId = vendor_company_details.id
  AND finance_vendor_offset_memo.vendorId = vendor_company_details.id
  AND finance_bills.vendorId = vendor_company_details.id
UNION ALL
  -- 11 Invoice
  -- Invoice Items
SELECT
  finance_invoice_lines.id AS transactionId,
  finance_invoice.id AS referenceId,
  finance_invoice.invoiceNumber AS referenceCode,
  '' AS transactionCode,
  finance_invoice.invoiceProjectId AS projectId,
  finance_invoice.customerId AS recipientId,
  customer_details.customerCompanyName AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_invoice_lines.itemRefName AS transactionName,
  CONCAT(
    "Revenue From : ",
    event_details.eventName,
    " Invoice Number ",
    `finance_invoice`.`invoiceNumber`,
    " ->",
    `finance_invoice_lines`.`invoiceLinesDescription`
  ) AS tranctionDescription,
  '0' AS dr_amount,
  finance_invoice_lines.invoiceLinesAmount AS cr_amount,
  finance_invoice.invoiceTxnDate AS transactionDate,
  finance_invoice_lines.invoiceLinesCreatedAt AS createdAt,
  finance_invoice_lines.invoiceLinesStatus AS `status`,
  'Revenue' AS transactionCategory,
  'Invoice' AS transactionClassification,
  'finance_invoice_lines' AS transactionTable,
  'finance_invoice' AS referenceTable
FROM
  (
    (
      (
        finance_invoice_lines
        JOIN finance_invoice ON (
          (
            finance_invoice_lines.invoiceId = finance_invoice.id
          )
        )
      )
      JOIN finance_items ON (
        (
          (
            finance_invoice_lines.itemRefId = finance_items.id
          )
          AND (
            finance_invoice_lines.itemRefName = finance_items.itemsName
          )
        )
      )
    )
    JOIN finance_accounts ON (
      (
        finance_items.itemsIncomeAccountRef = finance_accounts.id
      )
    )
  )
  INNER JOIN customer_details ON finance_invoice.customerId = customer_details.id
  INNER JOIN event_details ON event_details.customerId = customer_details.id
  AND finance_invoice.invoiceProjectId = event_details.id
UNION ALL
  -- 12 invoice Payment
SELECT
  finance_payment.id AS transactionId,
  finance_invoice.id AS referenceId,
  finance_invoice.invoiceNumber AS referenceCode,
  finance_payment.recepitNumber AS transactionCode,
  '' AS projectId,
  customer_details.id AS recipientId,
  customer_details.customerCompanyName AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_payment.paymentRefence AS transactionName,
  CONCAT(
    "Revenue Payment for : ",
    event_details.eventName,
    " Invoice Number ",
    `finance_invoice`.`invoiceNumber`,
    " ->",
    `finance_payment`.`paymentName`
  ) AS tranctionDescription,
  finance_payment.paymentTotalAmt AS dr_amount,
  '0' AS cr_amount,
  finance_payment.paymentTxnDate AS transactionDate,
  finance_payment.paymentCreateAt AS createdAt,
  finance_payment.paymentStatus AS `status`,
  'Revenue Payment' AS transactionCategory,
  'Invoice Payment' AS transactionClassification,
  'finance_payment' AS transactionTable,
  'finance_invoice' AS referenceTable
FROM
  (
    (
      finance_payment
      JOIN finance_invoice ON (
        (
          finance_payment.paymentLinkedTxn = finance_invoice.invoiceNumber
        )
      )
    )
    JOIN finance_accounts ON (
      (
        finance_payment.paymentDepositToAccount = finance_accounts.id
      )
    )
  )
  INNER JOIN customer_details ON finance_invoice.customerId = customer_details.id
  INNER JOIN event_details ON event_details.customerId = customer_details.id
  AND finance_invoice.invoiceProjectId = event_details.id
UNION ALL
  -- 13 Customer credit Memo
SELECT
  finance_credit_memo_lines.id AS transactionId,
  finance_credit_memo.id AS referenceId,
  finance_credit_memo.refernumber AS referenceCode,
  finance_credit_memo_lines.invoiceNumber AS transactionCode,
  finance_invoice.invoiceProjectId AS projectId,
  customer_details.id AS recipientId,
  customer_details.customerCompanyName AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_credit_memo_lines.invoiceNumber AS transactionName,
  CONCAT(
    "Revenue Payment Customer Credit for : ",
    event_details.eventName,
    " Invoice Number ",
    `finance_invoice`.`invoiceNumber`,
    " ->",
    "Reference: ",
    finance_credit_memo.refernumber,
    finance_credit_memo_lines.creditMemoLineDescription
  ) AS tranctionDescription,
  '0' AS dr_amount,
  finance_credit_memo_lines.creditMemoLineAmount AS cr_amount,
  finance_credit_memo.creditMemoTxnDate AS transactionDate,
  finance_credit_memo_lines.creditMemoLineCreatedAt AS createdAt,
  finance_credit_memo_lines.creditMemoLineStatus AS `status`,
  'Expense Credit Payment' AS transactionCategory,
  'Invoice Payment' AS transactionClassification,
  'finance_credit_memo_lines' AS transactionTable,
  'finance_invoice' AS referenceTable
FROM
  (
    (
      (
        finance_credit_memo_lines
        JOIN finance_credit_memo ON (
          (
            finance_credit_memo_lines.creditMemoId = finance_credit_memo.id
          )
        )
      )
      JOIN finance_invoice ON (
        (
          finance_credit_memo_lines.invoiceNumber = finance_invoice.invoiceNumber
        )
      )
    )
    JOIN finance_accounts ON (
      (
        finance_credit_memo.creditMemoAccount = finance_accounts.id
      )
    )
  )
  INNER JOIN event_details ON finance_invoice.invoiceProjectId = event_details.id
  INNER JOIN customer_details ON finance_credit_memo.customerId = customer_details.id
  AND finance_invoice.customerId = customer_details.id
  AND event_details.customerId = customer_details.id
UNION ALL
  -- 14 Offset Customer invoice
SELECT
  finance_offset_memo_lines.id AS transactionId,
  finance_offset_memo.id AS referenceId,
  finance_offset_memo.refernumber AS referenceCode,
  finance_offset_memo_lines.invoiceNumber AS transactionCode,
  finance_invoice.invoiceProjectId AS projectId,
  customer_details.id AS recipientId,
  customer_details.customerCompanyName AS recipientName,
  finance_accounts.accountParentId AS accountParentId,
  finance_accounts.accountsClassification AS accountsclassfication,
  finance_accounts.id AS accountId,
  finance_accounts.fullyQualifiedName AS accountName,
  finance_offset_memo_lines.invoiceNumber AS transactionName,
  CONCAT(
    "Revenue Payment Offset for : ",
    event_details.eventName,
    " Invoice Number ",
    `finance_invoice`.`invoiceNumber`,
    " ->",
    "Reference: ",
    `finance_offset_memo`.`refernumber`,
    `finance_offset_memo_lines`.`offsetMemoLineDescription`
  ) AS tranctionDescription,
  finance_offset_memo_lines.offsetMemoLineAmount AS dr_amount,
  '0' AS cr_amount,
  finance_offset_memo.offsetMemoTxnDate AS transactionDate,
  finance_offset_memo_lines.offsetMemoLineCreatedAt AS createdAt,
  finance_offset_memo_lines.offsetMemoLineStatus AS `status`,
  'Offset Payment' AS transactionCategory,
  'Invoice Payment' AS transactionClassification,
  'finance_offset_memo_lines' AS transactionTable,
  'finance_invoice' AS referenceTable
FROM
  (
    (
      (
        finance_offset_memo_lines
        JOIN finance_offset_memo ON (
          (
            finance_offset_memo_lines.offsetMemoId = finance_offset_memo.id
          )
        )
      )
      JOIN finance_invoice ON (
        (
          finance_offset_memo_lines.invoiceNumber = finance_invoice.invoiceNumber
        )
      )
    )
    JOIN finance_accounts ON (
      (
        finance_offset_memo.offsetMemoAccount = finance_accounts.id
      )
    )
  )
  INNER JOIN event_details ON finance_invoice.invoiceProjectId = event_details.id
  INNER JOIN customer_details ON finance_offset_memo.customerId = customer_details.id
  AND finance_invoice.customerId = customer_details.id
  AND event_details.customerId = customer_details.id
UNION ALL
-- 15 Account Transfer
SELECT
	finance_transfer.id AS transactionId,
	finance_transferee.id AS referenceId,
	finance_transferee.transfereeRefernce AS referenceCode,
	finance_transfer.tranferRefernce AS transactionCode,
	'' AS projectId,
	'' AS recipientId,
	'' AS recipientName,
	finance_accounts.accountParentId AS accountParentId,
	finance_accounts.accountsClassification AS accountsclassfication,
	finance_transfer.tranferFromAccountRef AS accountId,
	finance_accounts.fullyQualifiedName AS accountName,
	finance_transfer.tranferRefernce AS transactionName,
	CONCAT(
	"Account Transfer from: ",
	finance_accounts.fullyQualifiedName,
	" to ",
	( SELECT finance_accounts.fullyQualifiedName FROM finance_accounts WHERE finance_accounts.id = finance_transfer.tranferToAccountRef ),
	" ->",
	"Reference: ",
	finance_transfer.tranferRefernce
	) AS tranctionDescription,
	'0' AS dr_amount,
	finance_transfer.tranferAmount AS cr_amount,
	finance_transfer.tranferTxnDate AS transactionDate,
	finance_transfer.tranferCreatedAt AS createdAt,
	finance_transfer.`status` AS `status`,
	'Transfer' AS transactionCategory,
	'Transfer' AS transactionClassification,
	'finance_transfer' AS transactionTable,
	'finance_transferee' AS referenceTable
FROM
	finance_transfer
	INNER JOIN finance_transferee ON finance_transferee.transferid = finance_transfer.id
	INNER JOIN finance_accounts ON finance_transfer.tranferFromAccountRef = finance_accounts.id
  UNION ALL
  -- 16 Transferee
  SELECT
  	finance_transferee.id AS transactionId,
  	finance_transfer.id AS referenceId,
  	finance_transfer.tranferRefernce AS referenceCode,
  	finance_transferee.transfereeRefernce AS transactionCode,
  	'' AS projectId,
  	'' AS recipientId,
  	'' AS recipientName,
  	finance_accounts.accountParentId AS accountParentId,
  	finance_accounts.accountsClassification AS accountsclassfication,
  	finance_transferee.transfereeToAccountRef AS accountId,
  	finance_accounts.fullyQualifiedName AS accountName,
  	finance_transferee.transfereeRefernce AS transactionName,
  	CONCAT(
  	"Account Transfer from: ",
  	finance_accounts.fullyQualifiedName,
  	" to ",
  	( SELECT finance_accounts.fullyQualifiedName FROM finance_accounts WHERE finance_accounts.id = finance_transferee.transfereeFromAccountRef ),
  	" ->",
  	"Reference: ",
  	finance_transferee.transfereeRefernce
  	) AS tranctionDescription,
  	finance_transferee.transfereeAmount AS dr_amount,
  	'0' AS cr_amount,
  	finance_transferee.transfereeTxnDate AS transactionDate,
  	finance_transferee.transfereeCreatedAt AS createdAt,
  	finance_transferee.`status` AS `status`,
  	'Transfer' AS transactionCategory,
  	'Transfer' AS transactionClassification,
  	'finance_transferee' AS transactionTable,
  	'finance_transfer' AS referenceTable
  FROM
  	finance_transferee
  	INNER JOIN finance_transfer ON finance_transfer.id = finance_transferee.transferid
  	INNER JOIN finance_accounts ON finance_transferee.transfereeToAccountRef = finance_accounts.id
    UNION ALL
    -- Transfer Cost 17
    SELECT
	finance_transfer.id AS transactionId,
	finance_transferee.id AS referenceId,
	finance_transferee.transfereeRefernce AS referenceCode,
	finance_transfer.tranferRefernce AS transactionCode,
	'' AS projectId,
	'' AS recipientId,
	'' AS recipientName,
	finance_accounts.accountParentId AS accountParentId,
	finance_accounts.accountsClassification AS accountsclassfication,
	finance_transfer.tranferFromAccountRef AS accountId,
	finance_accounts.fullyQualifiedName AS accountName,
	finance_transfer.tranferRefernce AS transactionName,
	CONCAT(
	"Account Transfer from: ",
	finance_accounts.fullyQualifiedName,
	" to ",
	( SELECT finance_accounts.fullyQualifiedName FROM finance_accounts WHERE finance_accounts.id = finance_transfer.tranferToAccountRef ),
	" ->",
	"Reference: ",
	finance_transfer.tranferRefernce
	) AS tranctionDescription,
	'0' AS dr_amount,
	finance_transfer.transferCharges AS cr_amount,
	finance_transfer.tranferTxnDate AS transactionDate,
	finance_transfer.tranferCreatedAt AS createdAt,
	finance_transfer.`status` AS `status`,
	'Expense Transfer Charges' AS transactionCategory,
	'Transfer Cost' AS transactionClassification,
	'finance_transfer' AS transactionTable,
	'finance_transferee' AS referenceTable
FROM
	finance_transfer
	INNER JOIN finance_transferee ON finance_transferee.transferid = finance_transfer.id
	INNER JOIN finance_accounts ON finance_transfer.tranferFromAccountRef = finance_accounts.id
  UNION ALL
-- 18 Transfer Cost
SELECT
finance_transferee.id AS transactionId,
finance_transfer.id AS referenceId,
finance_transfer.tranferRefernce AS referenceCode,
finance_transferee.transfereeRefernce AS transactionCode,
'' AS projectId,
'' AS recipientId,
'' AS recipientName,
finance_accounts.accountParentId AS accountParentId,
finance_accounts.accountsClassification AS accountsclassfication,
finance_transferee.transfereeToAccountRef AS accountId,
finance_accounts.fullyQualifiedName AS accountName,
finance_transferee.transfereeRefernce AS transactionName,
CONCAT(
	"Account Transfer from: ",
	finance_accounts.fullyQualifiedName,
	" to ",
	( SELECT finance_accounts.fullyQualifiedName FROM finance_accounts WHERE finance_accounts.id = finance_transferee.transfereeFromAccountRef ),
	" ->",
	"Reference: ",
	finance_transferee.transfereeRefernce
	) AS tranctionDescription,
'0' AS dr_amount,
finance_transferee.transferCharges AS cr_amount,
finance_transferee.transfereeTxnDate AS transactionDate,
finance_transferee.transfereeCreatedAt AS createdAt,
finance_transferee.`status` AS `status`,
'Expense Transfer Charges' AS transactionCategory,
'Transfer Cost' AS transactionClassification,
'finance_transferee' AS transactionTable,
'finance_transfer' AS referenceTable
FROM
finance_transferee
INNER JOIN finance_transfer ON finance_transfer.id = finance_transferee.transferid
INNER JOIN finance_accounts ON finance_transferee.transfereeToAccountRef = finance_accounts.id
