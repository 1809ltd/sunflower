<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemoLines */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Credit Memo Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-credit-memo-lines-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vendorId',
            'vendorCreditMemoId',
            'billRef',
            'vendorCreditMemoLineDescription:ntext',
            'vendorCreditMemoLineAmount',
            'vendorCreditMemoLineStatus',
            'vendorCreditMemoLineCreatedAt',
            'vendorCreditMemoLineUpdatedAt',
            'creidtDeletedAt',
            'userId',
        ],
    ]) ?>

</div>
