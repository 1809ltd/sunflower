<?php

namespace app\modules\inventory\assetsetup\assetcategorytype\controllers;

use yii\web\Controller;

/**
 * Default controller for the `assetcategorytype` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
