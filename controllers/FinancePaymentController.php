<?php

namespace app\controllers;

use Yii;
use app\models\FinancePayment;
use app\models\FinancePaymentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinancePaymentController implements the CRUD actions for FinancePayment model.
 */
class FinancePaymentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->paymentUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->paymentUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinancePayment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new FinancePaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinancePayment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinancePayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new FinancePayment();

        $model->recepitNumber = $model->getRcpnumber();

        //Geting the time created Timestamp
        // $model->customerCreateTime= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

        //Getting user Log in details
        $model->userId= "1";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FinancePayment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FinancePayment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout='addformdatatablelayout';
        $this->findModel($id)->delete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the FinancePayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinancePayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinancePayment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
