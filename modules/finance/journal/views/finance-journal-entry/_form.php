<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\journal\models\FinanceJournalEntry */
/* @var $form yii\widgets\ActiveForm */
?>

<?php

$js = '
jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
        jQuery(this).html("Module: " + (index + 1))
    });
});


jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
    jQuery(".dynamicform_wrapper .panel-title").each(function(index) {
        jQuery(this).html("Module: " + (index + 1))
    });
});
';

$this->registerJs($js);
?>
<style>
.panel-body {
    padding-bottom: 0px;
}
</style>

<div class="box box-primary">

  <div class="box-body chart-responsive">

    <div class="col-md-12">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <div class="box-body finance-journal-entry-form">


                          <?php
                              // necessary for update action.
                              if (! $model->isNewRecord) {
                                // code...
                                  $status = ['0' => 'Cancel', '1' => 'Active', '2' => 'Waiting'];
                                  echo $form->field($model, 'status')->dropDownList($status, ['readonly' => true,'prompt'=>'Select Status']);

                                }else{

                                    echo $form->field($model, 'status')->hiddenInput(['value'=> "2"])->label(false);

                              }
                          ?>

                      <div class="col-sm-2">
                        <div class="form-group">
                          <?= $form->field($model, 'journalEntrydate')->widget(\yii\jui\DatePicker::class, [
                                //'language' => 'ru',
                                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                                'inline' => false,
                                'dateFormat' => 'yyyy-MM-dd',
                            ]); ?>

                        </div>
                      </div>

        </div>
        <div class="panel panel-default" >
        <div class="panel-body">
          <?php DynamicFormWidget::begin([
             'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
             'widgetBody' => '.container-items', // required: css class selector
             'widgetItem' => '.item', // required: css class
             // 'limit' => 80, // the maximum times, an element can be cloned (default 999)
             'min' => 1, // 0 or 1 (default 1)
             'insertButton' => '.add-item', // css class
             'deleteButton' => '.remove-item', // css class
             'model' => $modelFinanceJournalEntryLiness[0],
             'formId' => 'dynamic-form',
             'formFields' => [
               'accountId',
               'transactionId',
               'postingType',
               'description',
               'drAmount',
               'crAmount',
             ],
         ]); ?>

        <div class="container-items"><!-- widgetContainer -->
            <div class="caption font-red-sunglo" style="font-size: 15px;">
                <i class="fa fa-lock font-red-sunglo" ></i>
                <span class="caption-subject bold uppercase" > Jounral Items</span>
            </div><br />
            <!-- Loopping Items in the Lists -->
          <?php foreach ($modelFinanceJournalEntryLiness as $i => $modelFinanceJournalEntryLines): ?>

            <?php
            // foreach ($modelUserRoleModulePermissions as $i => $modelUserRoleModulePermission): ?>
                <div class="item"><!-- widgetBody -->

                    <div>
                      <?php
                          // necessary for update action.
                          if (! $modelFinanceJournalEntryLines->isNewRecord) {
                              echo Html::activeHiddenInput($modelFinanceJournalEntryLines, "[{$i}]id");
                          }
                      ?>
                      <div class="row box" style="float: center;width: 100%;">
                        <div class="box-header">
                          <h3 class="box-title"></h3>
                        </div>
                        <div class= "box-body">
                          <!-- /.box-header -->
                          <div class="row" style="float: center;width: 100%;">
                            <div class="pull-right">
                              <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                              <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceJournalEntryLines,  "[{$i}]accountId")->widget(Select2::classname(), [
                                    'data' =>ArrayHelper::map(FinanceAccounts::find()
                                                ->where(['accountsStatus'=>1])
                                                ->all(),'id','fullyQualifiedName'),
                                                'language' => 'en',
                                                'options' => ['placeholder' => 'Select a Accounts ...'],
                                                'pluginOptions' => [
                                                  'allowClear' => true
                                                ],
                                              ]); ?>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceJournalEntryLines, "[{$i}]description")->textInput() ?>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceJournalEntryLines, "[{$i}]drAmount")->textInput([
                                'maxlength' => true,
                                'class' => 'dr form-control']) ?>
                              </div>
                            </div>
                            <div class="col-sm-3">
                              <div class="form-group">
                                <?= $form->field($modelFinanceJournalEntryLines, "[{$i}]crAmount")->textInput([
                                'maxlength' => true,
                                'class' => 'cr form-control']) ?>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--  Last Row-->
                        <div class="row">

                        </div>
                      </div>
                    </div>
                    <!-- <div class="pull-right">
                    <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                    <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                    </div> -->
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
          </div>
        </div>



            <div class="row">

              <div class="col-sm-3">
                <div class="form-group">

                </div>
              </div>

              <div class="col-sm-3">
                <div class="form-group">

                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <?= $form->field($model, 'total')->textInput(['readonly' => true,'class' => 'amounttotal form-control']) ?>
                </div>
              </div>


            </div>

        <div class="row">
        </div>
        <div class="form-group">
          <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
  <?php
  /*getting the totalamount and the total tax amount */
  $script = <<<EOD
  var getAmount = function() {
    //getting the elemetent ID
    var items = $(".item");

        //intialization on amount figure and the total figure
        var amount = 0;
        var crAmount=0;
        var drAmount=0;
        var total = 0;
        var amounttotal= 0;

        items.each(function (index, elem) {

            //getting specific elements
            var dr = $(elem).find(".dr").val();
            var cr = $(elem).find(".cr").val();

            // Getting the Total for Cr Amount
            crAmount = parseFloat(crAmount) + parseFloat(cr);

            // Getting the Total for Dr Amount
            drAmount = parseFloat(drAmount) + parseFloat(dr);

            if (drAmount==crAmount) {
              // code...
              amounttotal= crAmount;


              //assing value to the Amount total tax amount
              $(".amounttotal").val(amounttotal);
              
            } else {
              // code...
            }




        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   $('#Roles').addClass('active open');
   $('#role').addClass('active open');"
, View::POS_READY);
?>
