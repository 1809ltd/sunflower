<?php

namespace app\modules\finance\income\estimate;

/**
 * estimate module definition class
 */
class estimate extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\income\estimate\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Estimate Items Sub Module*/
          'estimateitems' => [
              'class' => 'app\modules\finance\income\estimate\estimateitems\estimateitems',
          ],
        ];

        // custom initialization code goes here
    }
}
