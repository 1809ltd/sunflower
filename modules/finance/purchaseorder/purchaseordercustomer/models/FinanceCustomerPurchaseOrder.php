<?php

namespace app\modules\finance\purchaseorder\purchaseordercustomer\models;

/*Event*/
use app\modules\event\eventInfo\models\EventDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "finance_customer_purchase_order".
 *
 * @property int $id
 * @property string $purchaseOrderNumber
 * @property int $customerId
 * @property string $purchaseOrderProjectClassification
 * @property int $purchaseOrderProjectId
 * @property string $purchaseOrderProjectName
 * @property string $purchaseOrderNote
 * @property string $purchaseOrderTxnDate
 * @property string $purchaseOrderBillEmail
 * @property double $purchaseOrderDiscountAmount
 * @property double $purchaseOrderTaxAmount
 * @property double $purchaseOrderSubAmount
 * @property double $purchaseOrderAmount
 * @property string $purchaseOrderTxnStatus
 * @property int $purchaseOrderdCreatedby
 * @property string $purchaseOrderdCreatedAt
 * @property string $purchaseOrderdUpdatedAt
 * @property string $purchaseOrderdDeletedAt
 *
 * @property EventDetails $purchaseOrderProject
 * @property UserDetails $purchaseOrderdCreatedby0
 * @property FinanceCustomerPurchaseOrderLines[] $financeCustomerPurchaseOrderLines
 * @property FinanceEstimate[] $financeEstimates
 * @property FinanceInvoice[] $financeInvoices
 */
class FinanceCustomerPurchaseOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_customer_purchase_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchaseOrderNumber', 'customerId', 'purchaseOrderProjectClassification', 'purchaseOrderProjectId', 'purchaseOrderProjectName', 'purchaseOrderNote', 'purchaseOrderTxnDate', 'purchaseOrderBillEmail', 'purchaseOrderDiscountAmount', 'purchaseOrderTaxAmount', 'purchaseOrderSubAmount', 'purchaseOrderAmount', 'purchaseOrderTxnStatus', 'purchaseOrderdCreatedby'], 'required'],
            [['customerId', 'purchaseOrderProjectId', 'purchaseOrderdCreatedby'], 'integer'],
            [['purchaseOrderNote', 'purchaseOrderBillEmail', 'purchaseOrderTxnStatus'], 'string'],
            [['purchaseOrderTxnDate', 'purchaseOrderdCreatedAt', 'purchaseOrderdUpdatedAt', 'purchaseOrderdDeletedAt'], 'safe'],
            [['purchaseOrderDiscountAmount', 'purchaseOrderTaxAmount', 'purchaseOrderSubAmount', 'purchaseOrderAmount'], 'number'],
            [['purchaseOrderNumber'], 'string', 'max' => 50],
            [['purchaseOrderProjectClassification', 'purchaseOrderProjectName'], 'string', 'max' => 100],
            [['purchaseOrderNumber'], 'unique'],
            [['purchaseOrderProjectId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['purchaseOrderProjectId' => 'id']],
            [['purchaseOrderdCreatedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['purchaseOrderdCreatedby' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchaseOrderNumber' => 'Purchase Order Number',
            'customerId' => 'Customer ID',
            'purchaseOrderProjectClassification' => 'Purchase Order Project Classification',
            'purchaseOrderProjectId' => 'Purchase Order Project ID',
            'purchaseOrderProjectName' => 'Purchase Order Project Name',
            'purchaseOrderNote' => 'Purchase Order Note',
            'purchaseOrderTxnDate' => 'Purchase Order Txn Date',
            'purchaseOrderBillEmail' => 'Purchase Order Bill Email',
            'purchaseOrderDiscountAmount' => 'Purchase Order Discount Amount',
            'purchaseOrderTaxAmount' => 'Purchase Order Tax Amount',
            'purchaseOrderSubAmount' => 'Purchase Order Sub Amount',
            'purchaseOrderAmount' => 'Purchase Order Amount',
            'purchaseOrderTxnStatus' => 'Purchase Order Txn Status',
            'purchaseOrderdCreatedby' => 'Purchase Orderd Createdby',
            'purchaseOrderdCreatedAt' => 'Purchase Orderd Created At',
            'purchaseOrderdUpdatedAt' => 'Purchase Orderd Updated At',
            'purchaseOrderdDeletedAt' => 'Purchase Orderd Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderProject()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'purchaseOrderProjectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderdCreatedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'purchaseOrderdCreatedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerPurchaseOrderLines()
    {
        return $this->hasMany(FinanceCustomerPurchaseOrderLines::className(), ['purchaseOrderId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEstimates()
    {
        return $this->hasMany(FinanceEstimate::className(), ['localpurchaseOrder' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoices()
    {
        return $this->hasMany(FinanceInvoice::className(), ['localpurchaseOrder' => 'id']);
    }
}
