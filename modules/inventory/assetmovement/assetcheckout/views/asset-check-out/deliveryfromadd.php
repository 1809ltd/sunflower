<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
// use kartik\time\TimePicker;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(['id'=>'deliveryformaddone']); ?>

<div class="modal-body deliveryform-asset-check-out-form">

  <?= $form->field($model, 'activityId')->hiddenInput()->label(false);?>
  <?= $form->field($model, 'orderId')->hiddenInput()->label(false);?>
  <?= $form->field($model, 'status')->hiddenInput(['value'=>2])->label(false);?>

  <?= $form->field($model, 'activity')->hiddenInput()->label(false);?>

    <?= $form->field($model, 'locationId')->textInput() ?>

    <?= $form->field($model, 'assetId')->textInput() ?>

    <?= $form->field($model, 'orderlistId')->textInput() ?>

    <?= $form->field($model, 'vehicle')->textInput() ?>

    <?= $form->field($model, 'assignto')->textInput() ?>


</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
<?php
$script =<<< JS
$('form#deliveryformaddone').on('beforeSubmit',function(e){
  var \$form = $(this);
  $.post(
    \$form.attr("action"),//serialize yii2 form
    \$form.serialize()
    )
    .done(function(result) {
      if (result ==1 {
        // code...
        $(document).find('#modal').modal('hide');
        $.pjax.reload({container:'#holdasset'});
      } else {
        // code...
        $(\$form).trigger("reset");
        $("#message").html(result);
      }
    }).fail(function()
    {
      console.log(server error);
    });
    return false;
});
JS;
$this->registerJS($script);


 ?>
