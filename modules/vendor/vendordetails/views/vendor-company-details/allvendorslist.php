<?php

use yii\helpers\Url;
/*Vendor Details*/
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
?>
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <img style="height:25%;width:25%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
        <i class=""></i> Vendor List.
        <small class="pull-right">As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?></small>
      </h2>
    </div>
    <!-- /.col -->
  </div>
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
    <div class="col-sm-4 invoice-col">
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Contact Person</th>
            <th>Company Details:</th>
            <th>Contact Details:</th>
            <th>Description</th>
          </tr>
        </thead>
        <tbody>
          <?php
          // Get All active Vendor List
          $vendordetails = VendorCompanyDetails::find()
                              ->where('vendorCompanyStatus=1')
                              ->orderBy(
                                [
                                  new \yii\db\Expression('vendorCompanyName')
                                ]
                                )
                                ->all();

          foreach ($vendordetails as $vendordetails) {
            // code...
            
            ?>
            <tr>
              <td> <?= $vendordetails["vendorCompanyName"] ?></td>
              <td> <?= $vendordetails["vendorContactPersonFullName"] ?></td>
              <td>
                <p class="text-muted">
                Email: <?= $vendordetails["vendoremail"] ?><br>
                Phone: <?= $vendordetails["vendorPhone"] ?><br>
                </p>
              </td>
              <td>
                <p class="text-muted">
                Email: <?= $vendordetails["contactEmail"] ?><br>
                Phone: <?= $vendordetails["contactMobilePhone"] ?><br>
                </p>
              </td>
              <td>
                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                <p class="text-muted">
                  County: <?= $vendordetails["vendorCompanyCounty"] ?><br>
                  City: <?= $vendordetails["vendorCompanyCity"] ?><br>
                  Country: <?= $vendordetails["vendorCompanyCountry"] ?><br>
                  Address: <?= $vendordetails["vendorCompanyAddress"] ?><br>
                </p>
              </td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="javascript:void(0)" onclick="window.print()" class="btn btn-xs btn-success m-b-10">
        <i class="fa fa-print m-r-5"></i> Print</a>
    </div>
  </div>
</section>
