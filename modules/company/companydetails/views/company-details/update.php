<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companydetails\models\CompanyDetails */

$this->title = 'Update Company Details: ' . $model->companyName;
$this->params['breadcrumbs'][] = ['label' => 'Company Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->companyName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-details-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
