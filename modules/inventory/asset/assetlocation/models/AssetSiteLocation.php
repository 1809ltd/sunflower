<?php

namespace app\modules\inventory\asset\assetlocation\models;
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
use app\modules\company\companysitelocation\models\CompanySiteLocation;
use app\modules\company\companydepartment\models\CompanyDepartment;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "asset_site_location".
 *
 * @property int $id
 * @property int $assetId
 * @property int $siteId
 * @property int $loactionId
 * @property int $departmentId
 * @property string $createdAt
 * @property string $updateAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $status
 *
 * @property AssetCheckOut[] $assetCheckOuts
 * @property AssetRegistration $asset
 * @property CompanySiteLocation $loaction
 * @property CompanySiteLocation $site
 * @property CompanyDepartment $department
 */
class AssetSiteLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_site_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'siteId', 'loactionId', 'userId', 'status'], 'required'],
            [['assetId', 'siteId', 'loactionId', 'departmentId', 'userId', 'status'], 'integer'],
            [['createdAt', 'updateAt', 'deletedAt'], 'safe'],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['loactionId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanySiteLocation::className(), 'targetAttribute' => ['loactionId' => 'id']],
            [['siteId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanySiteLocation::className(), 'targetAttribute' => ['siteId' => 'id']],
            [['departmentId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyDepartment::className(), 'targetAttribute' => ['departmentId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetId' => 'Asset',
            'siteId' => 'Site',
            'loactionId' => 'Loaction',
            'departmentId' => 'Department',
            'createdAt' => 'Created At',
            'updateAt' => 'Update At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['locationId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoaction()
    {
        return $this->hasOne(CompanySiteLocation::className(), ['id' => 'loactionId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(CompanySiteLocation::className(), ['id' => 'siteId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(CompanyDepartment::className(), ['id' => 'departmentId']);
    }
}
