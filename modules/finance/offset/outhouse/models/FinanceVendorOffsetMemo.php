<?php

namespace app\modules\finance\offset\outhouse\models;

use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use Yii;

/**
 * This is the model class for table "finance_vendor_offset_memo"
 *
 * @property int $id
 * @property int $vendorOffsetMemoAccount
 * @property string $refernumber
 * @property string $vendorOffsetMemoTxnDate
 * @property string $vendorOffsetMemoNote
 * @property int $vendorId
 * @property double $offsetAmount
 * @property int $vendorOffsetMemoStatus
 * @property string $vendorOffsetMemoCreatedAt
 * @property string $vendorOffsetMemoUpdatedAt
 * @property string $vendorOffsetMemoDeletedAt
 * @property int $userId
 *
 * @property VendorCompanyDetails $vendor
 * @property FinanceAccounts $vendorOffsetMemoAccount0
 * @property FinanceVendorOffsetMemoLines[] $financeVendorOffsetMemoLines
 */
class FinanceVendorOffsetMemo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_vendor_offset_memo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorOffsetMemoAccount', 'refernumber', 'vendorOffsetMemoTxnDate', 'vendorOffsetMemoNote', 'vendorId', 'offsetAmount', 'vendorOffsetMemoStatus', 'userId'], 'required'],
            [['vendorOffsetMemoAccount', 'vendorId', 'vendorOffsetMemoStatus', 'userId'], 'integer'],
            [['vendorOffsetMemoTxnDate', 'vendorOffsetMemoCreatedAt', 'vendorOffsetMemoUpdatedAt', 'vendorOffsetMemoDeletedAt'], 'safe'],
            [['vendorOffsetMemoNote'], 'string'],
            [['offsetAmount'], 'number'],
            [['refernumber'], 'string', 'max' => 50],
            [['refernumber', 'vendorId'], 'unique', 'targetAttribute' => ['refernumber', 'vendorId']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['vendorOffsetMemoAccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['vendorOffsetMemoAccount' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorOffsetMemoAccount' => 'Account',
            'refernumber' => 'Refernumber',
            'vendorOffsetMemoTxnDate' => 'Date',
            'vendorOffsetMemoNote' => 'Note',
            'vendorId' => 'Vendor',
            'offsetAmount' => 'Offset Amount',
            'vendorOffsetMemoStatus' => 'Status',
            'vendorOffsetMemoCreatedAt' => 'Created At',
            'vendorOffsetMemoUpdatedAt' => 'Updated At',
            'vendorOffsetMemoDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorOffsetMemoAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'vendorOffsetMemoAccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorOffsetMemoLines()
    {
        return $this->hasMany(FinanceVendorOffsetMemoLines::className(), ['vendorOffsetMemoId' => 'id']);
    }
}
