<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\event\transportation\models\EventTransport */

$this->title = 'Assign a Car to an Event';
$this->params['breadcrumbs'][] = ['label' => 'Event Transports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-transport-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
