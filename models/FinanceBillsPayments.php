<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_bills_payments".
 *
 * @property int $id
 * @property int $billId
 * @property string $billPaymentPayType
 * @property int $billPaymentaccount
 * @property double $billPaymentTotalAmt
 * @property string $billPaymentBillRef
 * @property string $billPaymentPrivateNote
 * @property string $billPaymentCreatedAt
 * @property string $billPaymentUpdatedAt
 * @property string $billPaymentDeletedAt
 * @property int $userId
 *
 * @property FinanceBills $bill
 * @property FinanceAccounts $billPaymentaccount0
 * @property UserDetails $user
 */
class FinanceBillsPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_bills_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billId', 'billPaymentPayType', 'billPaymentaccount', 'billPaymentTotalAmt', 'billPaymentBillRef', 'userId'], 'required'],
            [['billId', 'billPaymentaccount', 'userId'], 'integer'],
            [['billPaymentTotalAmt'], 'number'],
            [['billPaymentPrivateNote'], 'string'],
            [['billPaymentCreatedAt', 'billPaymentUpdatedAt', 'billPaymentDeletedAt'], 'safe'],
            [['billPaymentPayType', 'billPaymentBillRef'], 'string', 'max' => 50],
            [['billId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBills::className(), 'targetAttribute' => ['billId' => 'id']],
            [['billPaymentaccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['billPaymentaccount' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'billId' => 'Bill Ref',
            'billPaymentPayType' => 'Payment Method',
            'billPaymentaccount' => 'Payment Account',
            'billPaymentTotalAmt' => 'Total Amount',
            'billPaymentBillRef' => 'Payement Ref:',
            'billPaymentPrivateNote' => 'Private Note',
            'billPaymentCreatedAt' => 'Created At',
            'billPaymentUpdatedAt' => 'Updated At',
            'billPaymentDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBill()
    {
        return $this->hasOne(FinanceBills::className(), ['id' => 'billId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPaymentaccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'billPaymentaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
