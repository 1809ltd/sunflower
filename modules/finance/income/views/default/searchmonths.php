<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
// use kartik\daterange\DateRangePicker;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Income Summary';
$this->params['breadcrumbs'][] = ['label' => 'Finace Inovices', 'url' => ['/finance/income/invoice/finance-invoice']];
$this->params['breadcrumbs'][] = ['label' => 'Revenue', 'url' => ['index']];

?>
<div class="box box-success no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Income Summary</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row">
      <?php $form = ActiveForm::begin(); ?>

      <div class="col-md-6">
        <div class="form-group">
          <?php
          echo $form->field($model, 'tarehe')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                'inline' => false,
                'dateFormat' => 'MM-yyyy',
                // 'dateFormat' => 'yyyy-MM-dd',
            ]);
          ?>
        </div>
      </div>
      <!-- /.col -->
      <div class="col-md-6">
        <p></p>
      <!-- /.form-group -->
      <div class="form-group">

          <?= Html::submitButton('Search for Transactions', ['class' => 'btn btn-primary']) ?>
      </div>
      <!-- /.form-group -->
      </div>
      <!-- /.col -->

      <?php ActiveForm::end(); ?>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <!-- Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
    the plugin. -->
  </div>
</div>
<?php
if (!empty($allinvoice)) {
  // code...

  echo "This is the total result set ->".count($allinvoice);

  // echo array_count_values($allinvoice);

  ?>
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-8">
        <!-- <h3 class="page-header"> -->
        <h6>
          <img style="height:75%;width:75%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
        </h6>
      </div>
      <div class="col-xs-4">
        <p>
        <address>
          <strong>
            <?= $companydetails->companyName?>,</strong><br/>
            <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
               Phone: +254 (0) 722 790632<br/>
               Email: info@sunflowertents.com
        </address>
      </p>
      </div>
      <!-- /.col -->
     </div>
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <?php
             // echo "This is the Month ->".$resultdate[0];
             // echo "This is the Year ->".$resultdate[1];
             // print_r($resultdate);

             echo "This is the Month ->".$resultdate;

              ?>
             <th class="page-header" style="text-align: center;width: 85%;">Income Based on Invoces for: <?php
             // Yii::$app->formatter->asDate($tarehe, 'long');?></th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>
     <!-- info row -->
     <div class="row invoice-info">
     </div>
     <!-- /.row -->
     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Invoice Date:</th>
             <th>Function Date:</th>
             <th>Invoice No:</th>
             <th>Client Name:</th>
             <th>Venue:</th>
             <!-- <th>Description:</th> -->
             <th>Sub Total</th>
             <th>V.A.T</th>
             <th>W.H.T</th>
             <th>Payments</th>
             <th>Balance</th>
           </thead>
             <tbody>
               <?php
               // Get all the specific Invoice

               $Subtoalm=0;
               $VATtotalm=0;
               $WHTtotalm=0;
               $taxtotalm=0;

               foreach ($allinvoice as $allinvoice) {
                 // code...
                 $Subtoal=0;
                 $VATtotal=0;
                 $WHTtotal=0;
                 $taxtotal=0;
                 // print_r($allinvoice->getAttributes());
                 $invoicesItems=$allinvoice->getinvoiceItems($allinvoice->id,$allinvoice->invoiceTxnStatus);

                 ?>
                 <tr>
                   <td><?= Yii::$app->formatter->asDate($allinvoice["invoiceTxnDate"], 'long');?></td>
                   <td><?= Yii::$app->formatter->asDate($allinvoice["invoiceProject"]["eventStartDate"], 'long');?></td>
                   <td><?= $allinvoice->invoiceNumber;?></td>
                   <td><strong>Customer : </strong><?=$allinvoice["customer"]["customerDisplayName"];?><br/><strong>Event </strong>: <?=$allinvoice["invoiceProject"]["eventName"];?></td>
                   <td><?=$allinvoice["invoiceProject"]["eventLocation"];?></td>
                   <!-- <td> -->
                     <?php
                     foreach ($invoicesItems as $invoicesItems) {
                       // code...
                       $Subtoal=$Subtoal+$invoicesItems["invoiceLinesAmount"];

                       if ($invoicesItems["invoiceLinesTaxCodeRef"]== 0.16) {
                         // code...
                         $VATtotal=$VATtotal+$invoicesItems["invoiceLinesTaxamount"];

                       }elseif ($invoicesItems["invoiceLinesTaxCodeRef"]== 0.05) {
                         // code...
                         $WHTtotal=$WHTtotal+$invoicesItems["invoiceLinesTaxamount"];

                       } else {
                         // code...
                         $taxtotal=$taxtotal+$invoicesItems["invoiceLinesTaxamount"];
                       }
                       // echo $invoicesItems["invoiceLinesQty"]." ".$invoicesItems["itemRefName"].",";
                     }
                     // To get the overal sub total;
                     $Subtoalm+=$Subtoal;

                     // To get the overal VAT  total;
                     $VATtotalm+=$VATtotal;

                     // To get the overal VAT  total;
                     $WHTtotalm+=$WHTtotal;

                     // To get the overal Total Tax  total;
                     $taxtotalm+=$taxtotal;
                     ?>
                   <!-- </td> -->
                   <td>Ksh <?= number_format($Subtoal,2) ;?></td>
                   <td>Ksh <?= number_format($VATtotal,2) ;?></td>
                   <td>Ksh <?= number_format($WHTtotal,2) ;?></td>
                   <td>

                     <?php
                     $malipo=$allinvoice->allPayments($allinvoice->id);

                     $malipoamount = preg_replace(
                        '/(-)([\d\.\,]+)/ui',
                        '($2)',
                        number_format($malipo,2,'.',',')
                    );

                     echo $malipoamount;

                     ?>
                  </td>
                  <td>
                    <?php
                    $bal = $Subtoal+$VATtotal+$WHTtotal-$malipo;
                    echo number_format($bal,2);
                    ?>
                  </td>
                </tr>
                <?php
              }
              // Bracjets if the figure is negatove
            $Subtoalmamount = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($Subtoalm,2,'.',',')
           );
           // Brackets if value is negative
           $VATtotalmamount = preg_replace(
              '/(-)([\d\.\,]+)/ui',
              '($2)',
              number_format($VATtotalm,2,'.',',')
          );
          // Brackets if value is negative
          $WHTtotalmamount = preg_replace(
             '/(-)([\d\.\,]+)/ui',
             '($2)',
             number_format($WHTtotalm,2,'.',',')
         );
         // Brackets if value is negative
         $taxtotalmamount = preg_replace(
            '/(-)([\d\.\,]+)/ui',
            '($2)',
            number_format($taxtotalm,2,'.',',')
        );
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <br>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
            <?php
            // $model->invoiceFooter; ?>
          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">


          <div class="col-md-8">
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <?php
           // $amountdue= $model->invoiceAmount-$payments;?>
          <p class="lead">Amount Due  <strong>Ksh <?= number_format($bal,2) ;?></strong></p>

        </br>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Currency:</th>
                <th class="text-right">(Ksh)</th>
              </tr>
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td class="text-right"><?= $Subtoalmamount?></td>
              </tr>
              <tr>
                <th>VAT</th>
                <td class="text-right"><?= number_format($VATtotal,2) ;?></td>
              </tr>
              <tr>
                <th>WHT</th>
                <td class="text-right"><?= $VATtotalmamount?></td>
              </tr>
              <tr>
                <th>Other Tax</th>
                <td class="text-right"><?= $taxtotalmamount?></td>
              </tr>

              <tr>
                <th>Total:</th>
                <td class="text-right"><?php

                // $Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm;

                // Brackets if value is negative
                $totalmamount = preg_replace(
                   '/(-)([\d\.\,]+)/ui',
                   '($2)',
                   number_format($Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm,2,'.',',')
               );

                echo $totalmamount ;?></td>
              </tr>
              <tr>
                <th>Total Payment:</th>
                <td class="text-right"><?= number_format(0,2) ;?></td>
              </tr>

            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- /.row -->
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
              class="btn btn-xs btn-success m-b-10"><i
                   class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
        </button> -->
        </div>
      </div>
    </section>
    <!-- /.content -->
    <?php
  } else {
    // code...

    echo "No Invoices were found Here";
  }
  ?>
