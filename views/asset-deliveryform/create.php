<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform */

$this->title = 'Create Asset Deliveryform';
$this->params['breadcrumbs'][] = ['label' => 'Asset Deliveryforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-deliveryform-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
