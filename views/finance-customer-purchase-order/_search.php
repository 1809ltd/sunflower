<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-customer-purchase-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'purchaseOrderNumber') ?>

    <?= $form->field($model, 'customerId') ?>

    <?= $form->field($model, 'purchaseOrderProjectClassification') ?>

    <?= $form->field($model, 'purchaseOrderProjectId') ?>

    <?php // echo $form->field($model, 'purchaseOrderProjectName') ?>

    <?php // echo $form->field($model, 'purchaseOrderNote') ?>

    <?php // echo $form->field($model, 'purchaseOrderTxnDate') ?>

    <?php // echo $form->field($model, 'purchaseOrderBillEmail') ?>

    <?php // echo $form->field($model, 'purchaseOrderDiscountAmount') ?>

    <?php // echo $form->field($model, 'purchaseOrderTaxAmount') ?>

    <?php // echo $form->field($model, 'purchaseOrderSubAmount') ?>

    <?php // echo $form->field($model, 'purchaseOrderAmount') ?>

    <?php // echo $form->field($model, 'purchaseOrderTxnStatus') ?>

    <?php // echo $form->field($model, 'purchaseOrderdCreatedby') ?>

    <?php // echo $form->field($model, 'purchaseOrderdCreatedAt') ?>

    <?php // echo $form->field($model, 'purchaseOrderdUpdatedAt') ?>

    <?php // echo $form->field($model, 'purchaseOrderdDeletedAt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
