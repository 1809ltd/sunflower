<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\models\FinanceEstimate;
use app\models\FinanceEstimateSearch;
use app\models\FinanceEstimateLines;
use app\models\FinanceEstimateLinesSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Quotation';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div class="form-group">
      <p class="pull-right">
          <?= Html::a('Create Quotation', ['create'], ['class' => 'btn btn-success']) ?>
      </p>
    </div>
  </div>
  <!-- /.box-header -->
  <!-- <div class="pull-right"> -->
    <!-- <div class="col-sm-3"> -->
      <!-- <div class="form-group">
        <p class="pull-right">
            <?php
             // Html::a('Create Quotation', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div> -->
    <!-- </div> -->
  <!-- </div> -->

  <div class="box-body finance-estimate-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->estimateTxnStatus ==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'estimateNumber',
            // 'customerId',
            [
              'attribute'=>'customerId',
              'value'=>'customer.customerDisplayName',
            ],
            'localpurchaseOrder',
            // 'estimateProjectClassification',
            //'estimateProjectId',
            'estimateTxnDate',
            // 'estimateDeadlineDate',
            //'estimateNote:ntext',
            //'estimateFooter:ntext',
            //'estimateTxnStatus:ntext',
            //'estimateEmailStatus:email',
            //'estimateBillEmail:email',
            'estimateTaxAmount',
            'estimateDiscountAmount',
            'estimateSubAmount',
            'estimateAmount',
            //'estimatedCreatedby',
            //'estimatedCreatedAt',
            //'estimatedUpdatedAt',
            //'estimatedDeletedAt',
            //'approvedby',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                'items' => [
                    [
                        'label' => 'View',
                        'url'   => ['view'],
                    ],
                    [
                        'label' => 'Update',
                        'url'   => ['update'],
                        // 'options'=>['class'=>'update-modal-click grid-action'],
                        // 'update'=>function($url,$model,$key){
                        //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                        //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                        //           'class'=>'update-modal-click grid-action',
                        //           'data-toggle'=>'tooltip',
                        //           'data-placement'=>'bottom',
                        //           'title'=>'Update'
                        //       ]);
                        //       return $btn;
                        // },
                        // 'option' 'class'=>'update-modal-click grid-action',
                        // 'data-toggle'=>'tooltip',
                        // 'data-placement'=>'bottom',
                    ],
                    [
                        'label'   => 'Disable',
                        'url'     => ['delete'],
                        'linkOptions' => [
                            'data-method' => 'post'
                        ],
                    ],
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



<!-- begin row -->
<!-- <div class="row">
    <div class="col-md-12"> -->
        <!-- begin panel -->
        <!-- <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="finance-estimate-index">
                <div class="table-responsive">
                  <p>
                      <?= Html::a('Create Quotation', ['create'], ['class' => 'btn btn-success']) ?>
                  </p>
              <?php Pjax::begin(); ?>

                  <?=  GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
                          'rowOptions'=> function($model)
                          {
                            // code...
                            if ($model->estimateTxnStatus==1) {
                              // code...
                              return['class'=>'success'];
                            } else {
                              // code...
                              return['class'=>'danger'];
                            }

                          },
                          'columns' => [
                              ['class' => 'kartik\grid\ExpandRowColumn',
                                'value'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  return GridView::ROW_COLLAPSED;
                                },
                                'detail'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  $searchModel = new FinanceEstimateLinesSearch();
                                  $searchModel->estimateId = $model->id;
                                  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                                  return Yii::$app->controller->renderPartial('_financeestimatelines',[
                                    'searchModel' => $searchModel,
                                    'dataProvider' => $dataProvider,

                                  ]);
                                },
                            ],

                            // 'id',
                            'estimateNumber',
                            'customerId',
                            'localpurchaseOrder',
                            // 'estimateProjectClassification',
                            //'estimateProjectId',
                            'estimateTxnDate',
                            // 'estimateDeadlineDate',
                            //'estimateNote:ntext',
                            //'estimateFooter:ntext',
                            //'estimateTxnStatus:ntext',
                            //'estimateEmailStatus:email',
                            //'estimateBillEmail:email',
                            'estimateTaxAmount',
                            'estimateDiscountAmount',
                            'estimateSubAmount',
                            'estimateAmount',
                            //'estimatedCreatedby',
                            //'estimatedCreatedAt',
                            //'estimatedUpdatedAt',
                            //'estimatedDeletedAt',
                            //'approvedby',


                            ['class' => 'yii\grid\ActionColumn'],
                          ],
                      ]);
                      ?>

                </div>

                <?php Pjax::end(); ?>
             </div>
            </div>
        </div> -->
        <!-- end panel -->
    <!-- </div>
</div> -->
<!-- end row -->
