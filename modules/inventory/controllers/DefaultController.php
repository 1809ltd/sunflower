<?php

namespace app\modules\inventory\controllers;

/*Asset Check Out */
use app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut;

use yii\web\Controller;

use yii\data\ActiveDataProvider;

/**
 * Default controller for the `inventory` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        // return $this->render('index');
        $this->layout = '@app/views/layouts/addformdatatablelayout';

        /*Getting all Check out Items*/
        $queryCheckOut = AssetCheckOut::find();
        $dataProviderCheckOut = new ActiveDataProvider([
            'query' => $queryCheckOut,
            'pagination'=> ['defaultPageSize' => 50],
        ]);
        $checkoutStatus="1";

        $queryCheckOut->andFilterWhere(['status' => $checkoutStatus]);

        /*Getting all Check out Items*/
        $queryCheckin = AssetCheckOut::find();
        $dataProviderCheckin = new ActiveDataProvider([
            'query' => $queryCheckin,
            'pagination'=> ['defaultPageSize' => 50],
        ]);
        $checkinStatus="3";

        $queryCheckin->andFilterWhere(['status' => $checkinStatus]);


        /*Getting All Invoices*/
        /*Getting all Check out Items*/
        $queryRepair = AssetCheckOut::find();
        $dataProviderRepair = new ActiveDataProvider([
            'query' => $queryRepair,
            'pagination'=> ['defaultPageSize' => 50],
        ]);
        $checkinStatus="4";

        $queryRepair->andFilterWhere(['status' => $checkinStatus]);

        return $this->render('index', [

            'modelCheckOut' => $dataProviderCheckOut,
            'modelCheckin' => $dataProviderCheckin,
            'modelRepair' => $dataProviderRepair,
        ]);
    }
}
