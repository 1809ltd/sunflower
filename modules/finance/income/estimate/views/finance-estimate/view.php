<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/*Estimate Items*/
use app\modules\finance\income\estimate\estimateitems\models\FinanceEstimateLines;
use app\modules\finance\income\estimate\estimateitems\models\FinanceEstimateLinesSearch;

/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimate */

$this->title = $model->estimateNumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Quotation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();


?>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <p>
      <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
  </p>
</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h4>
           <img class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h4>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Quotation</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
           <strong>Client:- </strong> <?= $model->customer['customerCompanyName']?><br/>
           <strong>Kra Pin:- </strong><?= $model->customer['customerKraPin']?><br/>
           <strong> No of guests:-</strong> <?= $model->estimateProject['eventVistorsNumber']?> Pax<br/>
             <strong>Date of Function:- </strong> <?= $model->estimateProject['eventStartDate']?><br/>
             <strong>Date of Set Up:- </strong> <?= $model->estimateProject['eventSetUpDateTime']?><br/>
             <strong>Colour Scheme:- </strong> <?= $model->estimateProject['eventTheme']?><br/>
             <strong>Event:- </strong> <?= $model->estimateProject['eventName']?> <br/>
             <strong>Location:- </strong> <?= $model->estimateProject['eventLocation']?><br/>


         Attn: <?= $model->customer['customerFullname']?>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Estimate #<?=$model->estimateNumber;?></b><br>
         <b>Quotation Date:- </b> <?= Yii::$app->formatter->asDate($model->estimateTxnDate, 'long');?><br>
         <b>Account:</b> <?= $model->customer['customerNumber']?>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <tr class="row">
               <th>Qty</th>
               <th>Service & Description</th>
               <th>Days</th>
               <th>Price</th>
               <th>Discount(%)</th>
               <th>Tax Amount</th>
               <th>Sub Amount</th>
             </tr>
           </thead>
           <tbody>
             <?php
             $estimatesItems=$model->getestimatesItems($model->id);

             foreach ($estimatesItems as $estimatesItem) {
               // code...

             ?>
               <tr class="row">
                   <td><?=$estimatesItem->estimateLinesQty;?></td>
                   <td><?= $estimatesItem->itemRefName;?>:<?= $estimatesItem->estimateLinesDescription;?></td>
                   <td><?=$estimatesItem->estimateLinespack;?></td>
                   <td>Ksh <?= number_format($estimatesItem->estimateLinesUnitPrice,2) ;?></td>
                   <td><?= $estimatesItem->estimateLinesDiscount;?></td>
                   <td>Ksh <?= number_format($estimatesItem->estimateLinesTaxamount,2) ;?></td>
                   <td>Ksh <?= number_format($estimatesItem->estimateLinesAmount,2) ;?></td>
               </tr>

               <?php

             }

              ?>

           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           <?= $model->estimateNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          <?= $model->estimateFooter; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Payment Methods:</p>

         <img src="<?=Url::to('@web/adminlte/dist/img/credit/visa.png');?>" alt="Visa">
         <img src="<?=Url::to('@web/adminlte/dist/img/credit/mastercard.png');?>" alt="Mastercard">
         <img src="<?=Url::to('@web/adminlte/dist/img/credit/american-express.png');?>" alt="American Express">
         <img src="<?=Url::to('@web/adminlte/dist/img/credit/paypal2.png');?>" alt="Paypal">
         <table class="table table-borderless table-sm">
           <tbody>
             <tr>
               <td>Bank name:</td>
               <td class="text-right">NIC Bank, Kenya</td>
             </tr>
             <tr>
               <td>Acc name:</td>
               <td class="text-right">Sunflower Limited</td>
             </tr>
             <tr>
               <td>Account Number:</td>
               <td class="text-right">12313231231321</td>
             </tr>
             <tr>
               <td>SWIFT code:</td>
               <td class="text-right">...</td>
             </tr>
             <tr>
               <td>KRA Company Name:</td>
               <td class="text-right">...</td>
             </tr>

             <tr>
               <td>KRA PIN:</td>
               <td class="text-right">...</td>
             </tr>
           </tbody>
         </table>

         <div class="col-md-8">
         </div>

         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           * Quotation is confirmed upon receipt of full  payment in cash or 60% with LSO and balance on delivery<br/>
           * Quoted prices are valid for 30 days from original quote<br/>
           * Sunflower Tents requires that client provide 24- hour security of equipment during setup & set down<br/>
           * All pricing information, discounts and equipment packaging contained in this document is confidential<br/>
           * Cancellation charge; 50% charge with less than 7 days notice<br/>
           * Late payment on the final invoice may result in the forfeiture of discounts included in this quote<br/>
           * If you have any questions concerning this invoice,<br/>
            contact [<?= $model->estimatedCreatedby0['userFName'].'  '.$model->estimatedCreatedby0['userLName']?>, <br/>
           Phone Number:    <?= $model->estimatedCreatedby0['userPhone']?><br/>
           Email:  <?= $model->estimatedCreatedby0['userEmail']?>]
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="lead">Quotation Vadility <?= Yii::$app->formatter->asDate($model->estimateDeadlineDate, 'long');?></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th style="width:50%">Subtotal:</th>
               <td>Ksh
                 Ksh <?= number_format($model->estimateSubAmount,2) ;?></td>
             </tr>
             <tr>
               <th>Total Tax</th>
               <td>Ksh <?= number_format($model->estimateTaxAmount,2) ;?></td>
             </tr>
             <tr>
               <th>Discount:</th>
               <td>Ksh <?= number_format($model->estimateDiscountAmount,2) ;?></td>
             </tr>
             <tr>
               <th>Total:</th>
               <td>Ksh <?= number_format($model->estimateAmount,2) ;?></td>
             </tr>

           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
         <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Convert to Invoice
         </button> -->
         <?php
        //  Html::a('<i class="fa fa-download"></i> Privacy Statement', ['generatepdf', 'id' => $model['id']], [
        //     'class'=>'btn btn-primary',
        //     'target'=>'_blank',
        //     'data-toggle'=>'tooltip',
        //     'title'=>'Will open the generated PDF file in a new window'
        // ]);?>
        <?= Html::a('<i class="fa fa-credit-card"></i> Convert to Invoice', ['convertinvoice', 'id' => $model['id']], [
           'class'=>'btn btn-success pull-right',
           'target'=>'_blank',
           'data-toggle'=>'tooltip',
           'title'=>'Will open the generated PDF file in a new window'
       ]);?>
         <?php
          // Html::a(" Generate PDF","[ConvertInvoice]",['class'=>'fa fa-download btn btn-primary','target'=>'_blank']) ?>
         <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button> -->
       </div>
     </div>
   </section>
   <!-- /.content -->
   <!-- this row will not appear when printing -->
