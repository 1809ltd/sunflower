<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCheckOut */

$this->title = 'Create Asset Check Out';
$this->params['breadcrumbs'][] = ['label' => 'Check Outs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-check-out-create">
      <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
