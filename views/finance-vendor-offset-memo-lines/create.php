<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines */

$this->title = 'Create Finance Vendor Offset Memo Lines';
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Offset Memo Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-offset-memo-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
