<?php

namespace app\modules\finance\financesetup\tax;

/**
 * tax module definition class
 */
class tax extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financesetup\tax\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
