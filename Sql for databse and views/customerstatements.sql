-- This is the income generated from the customers
-- ----------------------------
-- View structure for `customer customer_statement`
-- ----------------------------
DROP VIEW
IF
	EXISTS `customer_statement`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root` @`localhost` SQL SECURITY DEFINER VIEW `customer_statement` AS
SELECT
	finance_invoice.customerId,
  	finance_invoice.invoiceTxnDate AS date,
	finance_invoice.invoiceNumber AS transactionNo,
	CONCAT( "Invoice for Event  : ", event_details.eventName, " held on", event_details.eventStartDate ) AS Details,
	( Sum( finance_invoice_lines.invoiceLinesAmount ) + Sum( finance_invoice_lines.invoiceLinesTaxamount ) ) AS amountinvoiced,
	0 AS amountPaid
FROM
	finance_invoice_lines
	INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
	LEFT JOIN event_details ON finance_invoice.invoiceProjectId = event_details.id
WHERE
	finance_invoice.invoiceTxnStatus > 0
	AND finance_invoice_lines.invoiceLinesStatus > 0
GROUP BY
	finance_invoice.id
-- ORDER BY
-- 	date ASC
UNION ALL

SELECT
	finance_payment_lines.customerId,
	finance_payment.date AS date,
	finance_payment.recepitNumber AS transactionNo,
	CONCAT( "Payments for invoice  : ", finance_invoice.invoiceNumber, " ,payment via ",finance_payment.invoicePaymentPayType," reference number :-", finance_payment.invoicePaymentinvoiceRef ) AS Details,
	0 AS amountinvoiced,
	finance_payment_lines.amount AS amountPaid
FROM
	finance_payment_lines
	INNER JOIN finance_payment ON finance_payment_lines.invoicepaymentId = finance_payment.id
	LEFT JOIN finance_invoice ON finance_payment_lines.invoiceId = finance_invoice.id
WHERE
	finance_payment_lines.`status` = 1
	OR finance_payment_lines.`status` = 2
	AND finance_payment.paymentstatus > 0
-- ORDER BY
-- 	date ASC
UNION ALL
SELECT
	finance_payment.customerId,
	finance_payment.date AS date,
	finance_payment.recepitNumber AS transactionNo,
	CONCAT( "Unallocated amount done payment via ", finance_payment.invoicePaymentPayType, " reference number :-", finance_payment.invoicePaymentinvoiceRef ) AS Details,
	0 AS amountinvoiced,
	finance_payment.unallocatedAmount AS amountPaid
FROM
	finance_payment
WHERE
	finance_payment.paymentstatus > 0
	AND finance_payment.unallocatedAmount > 0
UNION ALL
SELECT
	finance_credit_memo.customerId,
	finance_credit_memo.creditMemoTxnDate AS date,
	finance_credit_memo.refernumber AS transactionNo,
	CONCAT( "Credit Note for Invoice : ", finance_credit_memo_lines.invoiceNumber ) AS Details,
	0 AS amountinvoiced,
	( Sum( finance_credit_memo_lines.creditMemoLineAmount ) + Sum( finance_credit_memo_lines.taxamount ) ) AS amountPaid
FROM
	finance_credit_memo_lines
	INNER JOIN finance_credit_memo ON finance_credit_memo_lines.creditMemoId = finance_credit_memo.id
WHERE
	finance_credit_memo.creditMemoStatus > 0
	AND finance_credit_memo_lines.creditMemoLineStatus > 0
UNION ALL
SELECT
	finance_offset_memo.customerId,
	finance_offset_memo.offsetMemoTxnDate AS date,
	finance_offset_memo_lines.invoiceNumber AS transactionNo,
	CONCAT( "Off set  Note for Invoice : ", finance_offset_memo_lines.invoiceNumber ) AS Details,
	0 AS amountinvoiced,
	finance_offset_memo_lines.offsetMemoLineAmount AS amountPaid
FROM
	finance_offset_memo_lines
	INNER JOIN finance_offset_memo ON finance_offset_memo_lines.offsetMemoId = finance_offset_memo.id
WHERE
	finance_offset_memo_lines.offsetMemoLineStatus > 0
	AND finance_offset_memo.offsetMemoStatus > 0
