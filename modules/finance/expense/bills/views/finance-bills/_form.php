<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
use kartik\select2\Select2;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;
use app\modules\finance\financesetup\tax\models\FinanceTax;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBills */
/* @var $form yii\widgets\ActiveForm */

?>



<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="panel-body finance-bills-form">

                <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
                <?php // $form = ActiveForm::begin(['id' => 'dynamic-form','enableAjaxValidation' => true,]); ?>
                  <div class="col-sm-3">
                    <div class="form-group">
                        <?= $form->field($model, 'projectId')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(EventDetails::find()->all(),'id','eventName'),
                            'language' => 'en',
                            'options' => ['placeholder' => 'Select a Project ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                      </div>
                    </div>

                  <div class="col-sm-3">

                    <div class="form-group">
                      <?= $form->field($model, 'vendorId')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(VendorCompanyDetails::find()->where('`vendorCompanyStatus` = 1')->asArray()->all(),'id','vendorCompanyName'),
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Vendor ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'billRef')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'billDueDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['autocomplete'=>'off','class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">

                      <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                      <?= $form->field($model, 'billStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                    </div>
                  </div>

                  <?php
                  // $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

                  <div class="panel-body"></div>

                  <div class="panel-body">

                    <div class="panel panel-pvr panel--style--1">
                      <div class="panel-heading">
                          <div class="panel-heading-btn">
                              <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                              <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                              <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                              <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                          </div>
                          <h4 class="panel-title">Add Bill Item</h4>
                      </div>
                      <div class="panel-body">
                           <?php DynamicFormWidget::begin([
                              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                              'widgetBody' => '.container-items', // required: css class selector
                              'widgetItem' => '.item', // required: css class
                              'limit' => 999, // the maximum times, an element can be cloned (default 999)
                              'min' => 1, // 0 or 1 (default 1)
                              'insertButton' => '.add-item', // css class
                              'deleteButton' => '.remove-item', // css class
                              'model' => $modelsFinanceBillsLines[0],
                              'formId' => 'dynamic-form',
                              'formFields' => [
                                'biilsLinesVendorsRef',
                                'biilsLinesDescription',
                                'biilsLinespacks',
                                'biilsLinesQty',
                                'billlinePriceperunit',
                                'biilsLinesAmount',
                                'billLinesTaxCodeRef',
                                'biilsLinesTaxAmount',
                                'accountId',

                              ],
                          ]); ?>

                          <div class="container-items"><!-- widgetContainer -->
                            <!-- Loopping Items in the Lists -->
                          <?php foreach ($modelsFinanceBillsLines as $i => $modelFinanceBillsLines): ?>
                              <div class="item panel-pvr panel--style--2 "><!-- widgetBody -->


                                  <div class="panel-heading">
                                      <h3 class="panel-title pull-left">Bill Item</h3>
                                      <div class="pull-right">
                                          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>


                                  <div class="panel-body">
                                      <?php
                                          // necessary for update action.
                                          if (! $modelFinanceBillsLines->isNewRecord) {
                                              echo Html::activeHiddenInput($modelFinanceBillsLines, "[{$i}]id");
                                          }
                                      ?>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]biilsLinesVendorsRef")->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]biilsLinesDescription")->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]biilsLinespacks")->textInput([
                                          'maxlength' => true,'autocomplete'=>"off",
                                          'class' => 'packs form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]biilsLinesQty")->textInput([
                                          'maxlength' => true,'autocomplete'=>"off",
                                          'class' => 'qnty form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]billlinePriceperunit")->textInput([
                                          'maxlength' => true,'autocomplete'=>"off",
                                          'class' => 'price form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?php $TaxCodeRef =  ArrayHelper::map(FinanceTax::find()->andWhere(['taxStatus'=>1])->asArray()->all(),'taxRate','taxCode'); ?>
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]billLinesTaxCodeRef")->dropDownList($TaxCodeRef, [
                                                          'prompt'=>'Select...',
                                                          'class' => 'taxcode form-control']); ?>

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]biilsLinesTaxAmount")->textInput([
                                          'readonly' => true,
                                          'maxlength' => true,
                                          'class' => 'sumTaxPart form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]biilsLinesAmount")->textInput([
                                          'readonly' => true,
                                          'maxlength' => true,
                                          'class' => 'amount sumPart form-control']) ?>
                                        </div>
                                      </div>

                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceBillsLines, "[{$i}]eventActivityId")->widget(Select2::classname(), [
                                              'data' => ArrayHelper::map(FinanceEventActivity::find()->where('`activityStatus` = 1')->asArray()->all(),'id','activityName'),
                                              'language' => 'en',
                                              'options' => ['prompt'=>'Select...'],
                                              'pluginOptions' => [
                                                  'allowClear' => true
                                              ],
                                          ]);?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>

                                  </div>
                              </div>
                          <?php endforeach; ?>
                          </div>
                          <?php DynamicFormWidget::end(); ?>
                      </div>
                  </div>

                  </div>

                    <div class="panel-body"></div>

                    <div class="col-sm-4">
                      <div class="form-group">
                        <?= $form->field($model, 'billsTaxAmount')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <?= $form->field($model, 'billAmount')->textInput(['readonly' => true,'class' => 'total form-control']) ?>
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                        <?= $form->field($model, 'billTotal')->textInput(['readonly' => true,'class' => 'sum form-control']) ?>
                      </div>
                    </div>

                    <div class="panel-body"></div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= Html::submitButton($modelFinanceBillsLines->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                    </div>
                  </div>

                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->

<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var amount = 0;
        var total = 0;
        var taxation=0;
        var tax=0
        var sum= 0;

        items.each(function (index, elem) {

            //getting specific elements
            var qnty = $(elem).find(".qnty").val();
            var price = $(elem).find(".price").val();
            var packs = $(elem).find(".packs").val();
            var taxcode = $(elem).find(".taxcode").val();

            //Check if qnty and price are numeric or something like that
            amount = parseFloat(qnty) * parseFloat(price) * parseFloat(packs);

            //Assign the amount value to the field
            $(elem).find(".amount").val(amount);

            var amountValue = $(elem).find(".amount").val();

            taxation= parseFloat(taxcode) * parseFloat(amount);

            //Assign the Taxation value to the field
            $(elem).find(".sumTaxPart").val(taxation);

            //getting the tax amount figures
            var taxValue = $(elem).find(".sumTaxPart").val();

            //getting the total of all figures
            total = parseFloat(total) + parseFloat(amountValue);

            //assing value to the total amount
            $(".total").val(total);

            //getting total tax amountValue
            tax = parseFloat(tax) + parseFloat(taxValue);
            //assing value to the total tax amount
            $(".tax").val(tax);

            sum= parseFloat(tax)+parseFloat(total);

            //assing value to the total amount
            $(".sum").val(sum);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
