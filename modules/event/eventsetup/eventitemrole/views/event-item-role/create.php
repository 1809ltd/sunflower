<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventItemRole */

$this->title = 'Create Event Item Role';
$this->params['breadcrumbs'][] = ['label' => 'Event Service Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-item-role-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
