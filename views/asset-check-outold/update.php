<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCheckOut */

$this->title = 'Update Asset Check Out: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Check Outs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-check-out-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
