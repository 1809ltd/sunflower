<?php

namespace app\modules\finance\offset\inhouse\controllers;

use Yii;
use app\modules\finance\offset\inhouse\models\FinanceOffsetMemo;
use app\modules\finance\offset\inhouse\models\FinanceOffsetMemoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\modules\finance\offset\inhouse\inoffsetinvoice\models\FinanceOffsetMemoLines;
use app\modules\finance\offset\inhouse\inoffsetinvoice\models\FinanceOffsetMemoLinesSearch;
/**
 * FinanceOffsetMemoController implements the CRUD actions for FinanceOffsetMemo model.
 */
class FinanceOffsetMemoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...

            $this->offsetMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          }else {
            // code...
            $this->offsetMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
          }
          return parent::beforeSave();
        }

        /**
         * Lists all FinanceOffsetMemo models.
         * @return mixed
         */
        public function actionIndex()
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $searchModel = new FinanceOffsetMemoSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single FinanceOffsetMemo model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new FinanceOffsetMemo model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $model = new FinanceOffsetMemo();

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;

            $modelsFinanceOffsetMemoLines = [new FinanceOffsetMemoLines];

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

              $modelsFinanceOffsetMemoLines = Model::createMultiple(FinanceOffsetMemoLines::classname());
              Model::loadMultiple($modelsFinanceOffsetMemoLines, Yii::$app->request->post());

              // ajax validation
              /*
              if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ArrayHelper::merge(
                        ActiveForm::validateMultiple($modelsFinanceOffsetMemoLines),
                        ActiveForm::validate($model)
                    );
                }*/

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceOffsetMemoLines) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          foreach ($modelsFinanceOffsetMemoLines as $modelFinanceOffsetMemoLines) {

                            $modelFinanceOffsetMemoLines->offsetMemoId = $model->id;
                            $modelFinanceOffsetMemoLines->userId = $model->userId;
                            $modelFinanceOffsetMemoLines->offsetMemoLineStatus = $model->offsetMemoStatus;

                              if (! ($flag = $modelFinanceOffsetMemoLines->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }

                  } catch (Exception $e) {

                      $transaction->rollBack();

                  }
              }


            }

            return $this->render('create', [
                'model' => $model,
                  'modelsFinanceOffsetMemoLines' => (empty($modelsFinanceOffsetMemoLines)) ? [new FinanceOffsetMemoLines] : $modelsFinanceOffsetMemoLines
            ]);


            // if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //     return $this->redirect(['view', 'id' => $model->id]);
            // }
            //
            // return $this->render('create', [
            //     'model' => $model,
            // ]);
        }

        /**
         * Updates an existing FinanceOffsetMemo model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */

        public function actionUpdate($id)
        {

          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = $this->findModel($id);

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;


            $modelsFinanceOffsetMemoLines = $this->getFinanceOffsetMemoLines($model->id);

            if ($model->load(Yii::$app->request->post())) {

                  // code...

                  //Geting the time created Timestamp
                  // $model->customerCreateTime= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

                  //Getting user Log in details
                  // $model->userId= "1";

                  //Generating Estimate Number

                  // $estimateNum=$model->getEstnumber();
                  // $model->estimateNumber= $estimateNum;
                  $model->save();

                  //after Saving the customer Details

                  $oldIDs = ArrayHelper::map($modelsFinanceOffsetMemoLines, 'id', 'id');
                  $modelsFinanceOffsetMemoLines = Model::createMultiple(FinanceOffsetMemoLines::classname(), $modelsFinanceOffsetMemoLines);
                  Model::loadMultiple($modelsFinanceOffsetMemoLines, Yii::$app->request->post());
                  $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceOffsetMemoLines, 'id', 'id')));

                  // ajax validation
                  if (Yii::$app->request->isAjax) {
                      Yii::$app->response->format = Response::FORMAT_JSON;
                      return ArrayHelper::merge(
                          ActiveForm::validateMultiple($modelsFinanceOffsetMemoLines),
                          ActiveForm::validate($model)
                      );
                  }

                  // validate all models
                  $valid = $model->validate();
                  $valid = Model::validateMultiple($modelsFinanceOffsetMemoLines) && $valid;

                  if ($valid) {
                      $transaction = \Yii::$app->db->beginTransaction();
                      try {
                          if ($flag = $model->save(false)) {
                              if (! empty($deletedIDs)) {
                                  FinanceOffsetMemoLines::deleteAll(['id' => $deletedIDs]);
                              }
                              foreach ($modelsFinanceOffsetMemoLines as $modelFinanceOffsetMemoLines) {

                                $modelFinanceOffsetMemoLines->offsetMemoId = $model->id;
                                $modelFinanceOffsetMemoLines->userId = $model->userId;
                                $modelFinanceOffsetMemoLines->offsetMemoLineStatus = $model->offsetMemoStatus;


                                  if (! ($flag = $modelFinanceOffsetMemoLines->save(false))) {
                                      $transaction->rollBack();
                                      break;
                                  }
                              }
                          }
                          if ($flag) {
                              $transaction->commit();
                              return $this->redirect(['view', 'id' => $model->id]);
                          }
                      } catch (Exception $e) {
                          $transaction->rollBack();
                      }
                  }

                  // return $this->redirect(['view', 'id' => $model->id]);

            } else {
              // code...

              //load the create form
              return $this->render('update', [
                  'model' => $model,
                  'modelsFinanceOffsetMemoLines' => (empty($modelsFinanceOffsetMemoLines)) ? [new FinanceOffsetMemoLines] : $modelsFinanceOffsetMemoLines
              ]);
            }

        }




        public function getFinanceOffsetMemoLines($id)
        {
          $model = FinanceOffsetMemoLines::find()->where(['offsetMemoId' => $id])->all();
          return $model;
        }

        /**
         * Deletes an existing FinanceOffsetMemo model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the FinanceOffsetMemo model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return FinanceOffsetMemo the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */

        protected function findModel($id)
        {
            if (($model = FinanceOffsetMemo::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
