<?php

namespace app\modules\user\userassignment\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*User roles Details*/
use app\modules\user\userroletypes\models\UserRoleTypes;

use Yii;

/**
 * This is the model class for table "user_assignment".
 *
 * @property int $id
 * @property int $roleId
 * @property int $userId
 * @property int $createdBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $assigner
 * @property int $status
 *
 * @property UserRoleTypes $role
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property UserDetails $assigner0
 */
class UserAssignment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_assignment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['roleId', 'userId'], 'required'],
            [['roleId', 'userId', 'createdBy', 'assigner', 'status'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt', 'createdBy', 'assigner'], 'safe'],
            [['roleId'], 'exist', 'skipOnError' => true, 'targetClass' => UserRoleTypes::className(), 'targetAttribute' => ['roleId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['assigner'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['assigner' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'roleId' => 'Role',
            'userId' => 'User',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'assigner' => 'Assigner',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRoleTypes::className(), ['id' => 'roleId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssigner0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'assigner']);
    }
}
