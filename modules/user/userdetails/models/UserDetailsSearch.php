<?php

namespace app\modules\user\userdetails\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\userdetails\models\UserDetails;

/**
 * UserDetailsSearch represents the model behind the search form of `app\modules\user\userdetails\models\UserDetails`.
 */
class UserDetailsSearch extends UserDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'userStatus', 'userCreatedBy'], 'integer'],
            [['userStaffId', 'userFName', 'userLName', 'userPhone', 'userEmail', 'username', 'password', 'authKey', 'password_reset_token', 'userImage', 'userLastLogin', 'userDeleteAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'userStatus' => $this->userStatus,
            'userLastLogin' => $this->userLastLogin,
            'userCreatedBy' => $this->userCreatedBy,
            'userDeleteAt' => $this->userDeleteAt,
        ]);

        $query->andFilterWhere(['like', 'userStaffId', $this->userStaffId])
            ->andFilterWhere(['like', 'userFName', $this->userFName])
            ->andFilterWhere(['like', 'userLName', $this->userLName])
            ->andFilterWhere(['like', 'userPhone', $this->userPhone])
            ->andFilterWhere(['like', 'userEmail', $this->userEmail])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'authKey', $this->authKey])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'userImage', $this->userImage]);

        return $dataProvider;
    }
}
