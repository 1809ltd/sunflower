<?php
namespace app\modules\finance\receipt\invoicepayment\models;
/*Invoice details*/
use app\modules\finance\income\invoice\models\FinanceInvoice;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;

use Yii;

/**
 * This is the model class for table "finance_payment_lines".
 *
 * @property int $id
 * @property int $invoicepaymentId the main payment cheque
 * @property int $invoiceId invoice and invoice number
 * @property int $customerId
 * @property double $amount
 * @property int $status
 * @property string $deletedAt
 * @property string $created_at
 * @property string $updated_at
 * @property int $userId
 *
 * @property FinancePayment $invoicepayment
 * @property CustomerDetails $customer
 * @property UserDetails $user
 * @property FinanceInvoice $invoice
 */
class FinancePaymentLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_payment_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoicepaymentId', 'invoiceId', 'customerId', 'status', 'userId'], 'integer'],
            [['amount', 'status', 'userId'], 'required'],
            [['amount'], 'number'],
            [['deletedAt', 'created_at', 'updated_at'], 'safe'],
            [['invoicepaymentId'], 'exist', 'skipOnError' => true, 'targetClass' => FinancePayment::className(), 'targetAttribute' => ['invoicepaymentId' => 'id']],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['invoiceId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceInvoice::className(), 'targetAttribute' => ['invoiceId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'invoicepaymentId' => 'Invoice Payment',
          'invoiceId' => 'Invoice Number',
          'customerId' => 'Customer',
          'amount' => 'Amount',
          'status' => 'Status',
          'deletedAt' => 'Deleted At',
          'created_at' => 'Created At',
          'updated_at' => 'Updated At',
          'userId' => 'User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoicepayment()
    {
        return $this->hasOne(FinancePayment::className(), ['id' => 'invoicepaymentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(FinanceInvoice::className(), ['id' => 'invoiceId']);
    }
}
