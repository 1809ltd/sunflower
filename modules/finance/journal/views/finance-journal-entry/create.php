<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\journal\models\FinanceJournalEntry */

$this->title = 'Create Finance Journal Entry';
$this->params['breadcrumbs'][] = ['label' => 'Finance Journal Entries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-journal-entry-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelFinanceJournalEntryLiness'=>$modelFinanceJournalEntryLiness, //to enable Dynamic Form

    ]) ?>

</div>
