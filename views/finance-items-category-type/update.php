<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItemsCategoryType */

$this->title = 'Update Serive Category Type: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Items Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-items-category-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
