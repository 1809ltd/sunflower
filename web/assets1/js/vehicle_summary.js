"use strict";
var ass = function () {
    "use strict";


    $(document).ready(function () {
        var table = "";
        table = $('#table').DataTable({
            "scrollX"     : true,
            "ajax"        : "assets/php/Data/vehicle_summary",
            "initComplete": function (settings, json) {
            },
            "columns"     : [
                {"data": null},
                {"data": 'name'},
                {"data": 'count'}
            ],
            "order"       : [ [ 0, 'asc' ] ]
        });

        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
};
var Assign = function () {
    "use strict";
    return {
        init: function () {
            ass();
        }
    }
}();
$(function () {
    Assign.init();
});