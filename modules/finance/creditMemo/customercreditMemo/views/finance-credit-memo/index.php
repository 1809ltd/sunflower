<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\modules\finance\creditMemo\customercreditMemo\models\FinanceCreditMemo;
use app\modules\finance\creditMemo\customercreditMemo\models\FinanceCreditMemoSearch;
use app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\models\FinanceCreditMemoLines;
use app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\models\FinanceCreditMemoLinesSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceCreditMemoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Credit Memos';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div></div>
    <div class="form-group">
      <?php
      $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        // 'id',
        // 'creditMemoAccount',
        [
          'attribute'=>'customerId',
          'value'=>'customer.customerDisplayName',
        ],
        'refernumber',
        'creditMemoTxnDate',
        'creditMemoNote:ntext',
        // 'creditMemoStatus',
        //'customerId',
        'totalTax',
        'creditAmount',
        //'creditMemoCreatedAt',
        //'creditMemoUpdatedAt',
        //'creditMemoDeletedAt',
        //'userId',
        ['class' => 'yii\grid\ActionColumn'],
      ];
      // Renders a export dropdown menu
      echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
          // 'filterModel' => $searchModel,
        'columns' => $gridColumns
      ]);
      // You can choose to render your own GridView separately
      // echo \kartik\grid\GridView::widget([
      //   'dataProvider' => $dataProvider,
      //   'filterModel' => $searchModel,
      //   'columns' => $gridColumns
      // ]);
      ?>
      <p class="pull-right">
          <?= Html::a('Create Credit Memo', ['create'], ['class' => 'btn btn-success']) ?>
      </p>
    </div>
  </div>

  <div class="box-body finance-credit-memo-index">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?=  GridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
      'rowOptions'=> function($model)
      {
        // code...
        if ($model->creditMemoStatus==1) {
          // code...
          return['class'=>'success'];
        } else {
          // code...
          return['class'=>'danger'];
        }

      },
      'columns' => [
          ['class' => 'kartik\grid\ExpandRowColumn',
            'value'=>function ($model,$key,$index,$column)
            {
              // code...
              return GridView::ROW_COLLAPSED;
            },
            'detail'=>function ($model,$key,$index,$column)
            {
              // code...
              $searchModel = new FinanceCreditMemoLinesSearch();
              $searchModel->creditMemoId = $model->id;
              $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

              return Yii::$app->controller->renderPartial('creditmemodetails',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

              ]);
            },
        ],

        // 'id',
        // 'creditMemoAccount',
        [
          'attribute'=>'customerId',
          'value'=>'customer.customerDisplayName',
        ],
        'refernumber',
        'creditMemoTxnDate',
        'creditMemoNote:ntext',
        // 'creditMemoStatus',
        //'customerId',
        'totalTax',
        'creditAmount',
        //'creditMemoCreatedAt',
        //'creditMemoUpdatedAt',
        //'creditMemoDeletedAt',
        //'userId',

        // ['class' => 'yii\grid\ActionColumn'],
              // ['class' => 'yii\grid\ActionColumn'],
              [
                  'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                  'items' => [
                      [
                          'label' => 'View',
                          'url'   => ['view'],
                      ],
                      [
                          'label' => 'Update',
                          'url'   => ['update'],
                          // 'options'=>['class'=>'update-modal-click grid-action'],
                          // 'update'=>function($url,$model,$key){
                          //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                          //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                          //           'class'=>'update-modal-click grid-action',
                          //           'data-toggle'=>'tooltip',
                          //           'data-placement'=>'bottom',
                          //           'title'=>'Update'
                          //       ]);
                          //       return $btn;
                          // },
                          // 'option' 'class'=>'update-modal-click grid-action',
                          // 'data-toggle'=>'tooltip',
                          // 'data-placement'=>'bottom',
                      ],
                      [
                          'label'   => 'Disable',
                          'url'     => ['delete'],
                          'linkOptions' => [
                              'data-method' => 'post'
                          ],
                      ],
                  ]
              ],
            ],
        ]);
        ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
