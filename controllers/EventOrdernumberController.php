<?php

namespace app\controllers;


use Yii;

use app\models\EventOrdernumber;
use app\models\EventItemRole;
use app\models\EventItemRoleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class EventOrdernumberController extends \yii\web\Controller
{
    public function actionIndex()
    {

      $this->layout='addformdatatablelayout';
      $searchModel = new EventItemRoleSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);


        return $this->render('index');
    }

}
