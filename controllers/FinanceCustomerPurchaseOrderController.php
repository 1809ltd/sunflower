<?php

namespace app\controllers;

use Yii;
use app\models\FinanceCustomerPurchaseOrder;
use app\models\FinanceCustomerPurchaseOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\models\FinanceCustomerPurchaseOrderLines;
use app\models\FinanceCustomerPurchaseOrderLinesSearch;


/**
 * FinanceCustomerPurchaseOrderController implements the CRUD actions for FinanceCustomerPurchaseOrder model.
 */
class FinanceCustomerPurchaseOrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->purchaseOrderdUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->purchaseOrderdUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceCustomerPurchaseOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new FinanceCustomerPurchaseOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceCustomerPurchaseOrder model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceCustomerPurchaseOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new FinanceCustomerPurchaseOrder();

        $modelsFinanceCustomerPurchaseOrderLines = [new FinanceCustomerPurchaseOrderLines];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsFinanceCustomerPurchaseOrderLines = Model::createMultiple(FinanceCustomerPurchaseOrderLines::classname());
          Model::loadMultiple($modelsFinanceCustomerPurchaseOrderLines, Yii::$app->request->post());

          // ajax validation
          /*
          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFinanceCustomerPurchaseOrderLines),
                    ActiveForm::validate($model)
                );
            }*/

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceCustomerPurchaseOrderLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceCustomerPurchaseOrderLines as $modelFinanceCustomerPurchaseOrderLines) {

                        $modelFinanceCustomerPurchaseOrderLines->purchaseOrderId = $model->id;
                        $modelFinanceCustomerPurchaseOrderLines->userId = $model->purchaseOrderdCreatedby;


                          if (! ($flag = $modelFinanceCustomerPurchaseOrderLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }


        }

        return $this->render('create', [
            'model' => $model,
              'modelsFinanceCustomerPurchaseOrderLines' => (empty($modelsFinanceCustomerPurchaseOrderLines)) ? [new FinanceCustomerPurchaseOrderLines] : $modelsFinanceCustomerPurchaseOrderLines
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Updates an existing FinanceCustomerPurchaseOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        $modelsFinanceCustomerPurchaseOrderLines = $this->getFinanceCustomerPurchaseOrderLines($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $oldIDs = ArrayHelper::map($modelsFinanceCustomerPurchaseOrderLines, 'id', 'id');
          $modelsFinanceCustomerPurchaseOrderLines = Model::createMultiple(FinanceCustomerPurchaseOrderLines::classname(), $modelsFinanceCustomerPurchaseOrderLines);
          Model::loadMultiple($modelsFinanceCustomerPurchaseOrderLines, Yii::$app->request->post());
          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceCustomerPurchaseOrderLines, 'id', 'id')));

          // ajax validation
          if (Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ArrayHelper::merge(
                  ActiveForm::validateMultiple($modelsFinanceCustomerPurchaseOrderLines),
                  ActiveForm::validate($model)
              );
          }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceCustomerPurchaseOrderLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      if (! empty($deletedIDs)) {
                          FinanceCustomerPurchaseOrderLines::deleteAll(['id' => $deletedIDs]);
                      }
                      foreach ($modelsFinanceCustomerPurchaseOrderLines as $modelFinanceCustomerPurchaseOrderLines) {
                          $modelFinanceCustomerPurchaseOrderLines->purchaseOrderId = $model->id;
                          if (! ($flag = $modelFinanceCustomerPurchaseOrderLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
              }
          }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsFinanceCustomerPurchaseOrderLines' => (empty($modelsFinanceCustomerPurchaseOrderLines)) ? [new FinanceCustomerPurchaseOrderLines] : $modelsFinanceCustomerPurchaseOrderLines
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('update', [
        //     'model' => $model,
        // ]);
    }

    public function getFinanceCustomerPurchaseOrderLines($id)
    {
      $model = FinanceCustomerPurchaseOrderLines::find()->where(['purchaseOrderId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing FinanceCustomerPurchaseOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout='addformdatatablelayout';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceCustomerPurchaseOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceCustomerPurchaseOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $this->layout='addformdatatablelayout';
        if (($model = FinanceCustomerPurchaseOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
