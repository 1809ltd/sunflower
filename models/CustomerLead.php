<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_lead".
 *
 * @property int $id
 * @property int $customerId
 * @property string $leadprojectName
 * @property string $leadProjectclassification
 * @property string $leadGenerator
 * @property string $leadReferredby
 * @property string $leadReferredbyRelation
 * @property string $leadCreatedAt
 * @property string $leadLastUpdatedAt
 * @property string $leadDeletedAt
 * @property int $userId
 *
 * @property CustomerDetails $customer
 * @property UserDetails $user
 * @property CustomerLeadAppointment[] $customerLeadAppointments
 * @property FinanceCustomerLeadRequstionForm[] $financeCustomerLeadRequstionForms
 * @property FinanceLeadRequstionForm[] $financeLeadRequstionForms
 */
class CustomerLead extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_lead';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerId', 'leadprojectName', 'leadProjectclassification', 'leadGenerator', 'userId'], 'required'],
            [['customerId', 'userId'], 'integer'],
            [['leadCreatedAt', 'leadLastUpdatedAt', 'leadDeletedAt'], 'safe'],
            [['leadprojectName', 'leadProjectclassification', 'leadGenerator', 'leadReferredby', 'leadReferredbyRelation'], 'string', 'max' => 100],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customerId' => 'Customer',
            'leadprojectName' => 'Project Name',
            'leadProjectclassification' => 'Project Classification',
            'leadGenerator' => 'Marketer',
            'leadReferredby' => 'Referred By',
            'leadReferredbyRelation' => 'Referred By Relation',
            'leadCreatedAt' => 'Lead Created At',
            'leadLastUpdatedAt' => 'Lead Last Updated At',
            'leadDeletedAt' => 'Lead Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLeadAppointments()
    {
        return $this->hasMany(CustomerLeadAppointment::className(), ['leadId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerLeadRequstionForms()
    {
        return $this->hasMany(FinanceCustomerLeadRequstionForm::className(), ['leadId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceLeadRequstionForms()
    {
        return $this->hasMany(FinanceLeadRequstionForm::className(), ['leadId' => 'id']);
    }
}
