<?php

namespace app\modules\finance\productsetup\financeitems\models;

/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*Item Category*/
use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory;
/*Categry Type*/
use app\modules\finance\productsetup\financeitemscategorytype\models\FinanceItemsCategoryType;
/*USer details*/
use app\models\UserDetails;
use Yii;

/**
 * This is the model class for table "finance_items".
 *
 * @property int $id
 * @property string $itemsName
 * @property string $itemsCode
 * @property string $itemsDescription
 * @property int $itemCategory
 * @property int $itemCategoryType
 * @property string $itemsTaxable
 * @property double $itemsCustomerUnitPrice
 * @property double $itemsSupplierUnitPrice
 * @property string $itemsClassification
 * @property int $itemsIncomeAccountRef
 * @property string $itemsPurchaseDesc
 * @property double $itemsPurchaseCost
 * @property int $itemsExpenseAccountRef
 * @property int $itemsAssetAccountRef
 * @property string $itemsCreatedAt
 * @property string $itemsLastupdatedAt
 * @property string $itemsDeletedAt
 * @property int $itemsStatus
 * @property int $userId
 *
 * @property EventItemCategoryType[] $eventItemCategoryTypes
 * @property AssetCategoryType[] $categorytypes
 * @property EventItemRole[] $eventItemRoles
 * @property EventRole[] $roles
 * @property EventOrderfromList[] $eventOrderfromLists
 * @property FinanceEstimateLines[] $financeEstimateLines
 * @property FinanceEstimateLines[] $financeEstimateLines0
 * @property FinanceInvoiceLines[] $financeInvoiceLines
 * @property FinanceInvoiceLines[] $financeInvoiceLines0
 * @property FinanceItemsCategory $itemCategory0
 * @property FinanceItemsCategoryType $itemCategoryType0
 * @property FinanceAccounts $itemsIncomeAccountRef0
 * @property FinanceAccounts $itemsExpenseAccountRef0
 * @property UserDetails $user
 */
class FinanceItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemsName', 'itemsDescription', 'itemCategory', 'itemCategoryType', 'itemsTaxable', 'itemsCustomerUnitPrice', 'itemsSupplierUnitPrice', 'itemsClassification', 'itemsIncomeAccountRef', 'itemsPurchaseDesc', 'itemsPurchaseCost', 'itemsExpenseAccountRef', 'itemsStatus', 'userId'], 'required'],
            [['itemsDescription', 'itemsPurchaseDesc'], 'string'],
            [['itemCategory', 'itemCategoryType', 'itemsIncomeAccountRef', 'itemsExpenseAccountRef', 'itemsAssetAccountRef', 'itemsStatus', 'userId'], 'integer'],
            [['itemsCustomerUnitPrice', 'itemsSupplierUnitPrice', 'itemsPurchaseCost'], 'number'],
            [['itemsCreatedAt', 'itemsLastupdatedAt', 'itemsDeletedAt'], 'safe'],
            [['itemsName', 'itemsCode', 'itemsClassification'], 'string', 'max' => 100],
            [['itemsTaxable'], 'string', 'max' => 255],
            [['itemsName'], 'unique'],
            [['itemCategory'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItemsCategory::className(), 'targetAttribute' => ['itemCategory' => 'id']],
            [['itemCategoryType'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItemsCategoryType::className(), 'targetAttribute' => ['itemCategoryType' => 'id']],
            [['itemsIncomeAccountRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['itemsIncomeAccountRef' => 'id']],
            [['itemsExpenseAccountRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['itemsExpenseAccountRef' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'itemsName' => 'Product or Service',
          'itemsCode' => 'Code',
          'itemsDescription' => 'Description',
          'itemCategory' => 'Category',
          'itemCategoryType' => 'Category Type',
          'itemsTaxable' => 'Taxable',
          'itemsCustomerUnitPrice' => 'Customer Unit Price',
          'itemsSupplierUnitPrice' => 'Supplier Unit Price',
          'itemsClassification' => 'Classification',
          'itemsIncomeAccountRef' => 'Income Account Ref',
          'itemsPurchaseDesc' => 'Purchase Desc',
          'itemsPurchaseCost' => 'Purchase Cost',
          'itemsExpenseAccountRef' => 'Expense Account Ref',
          'itemsAssetAccountRef' => 'Asset Account Ref',
          'itemsCreatedAt' => 'Created At',
          'itemsLastupdatedAt' => 'Lastupdated At',
          'itemsDeletedAt' => 'Deleted At',
          'itemsStatus' => 'Status',
          'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventItemCategoryTypes()
    {
        return $this->hasMany(EventItemCategoryType::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorytypes()
    {
        return $this->hasMany(AssetCategoryType::className(), ['id' => 'categorytypeId'])->viaTable('event_item_category_type', ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventItemRoles()
    {
        return $this->hasMany(EventItemRole::className(), ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoles()
    {
        return $this->hasMany(EventRole::className(), ['id' => 'roleId'])->viaTable('event_item_role', ['itemId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfromLists()
    {
        return $this->hasMany(EventOrderfromList::className(), ['itemid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEstimateLines()
    {
        return $this->hasMany(FinanceEstimateLines::className(), ['itemRefId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEstimateLines0()
    {
        return $this->hasMany(FinanceEstimateLines::className(), ['itemRefName' => 'itemsName']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoiceLines()
    {
        return $this->hasMany(FinanceInvoiceLines::className(), ['itemRefId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoiceLines0()
    {
        return $this->hasMany(FinanceInvoiceLines::className(), ['itemRefName' => 'itemsName']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategory0()
    {
        return $this->hasOne(FinanceItemsCategory::className(), ['id' => 'itemCategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategoryType0()
    {
        return $this->hasOne(FinanceItemsCategoryType::className(), ['id' => 'itemCategoryType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsIncomeAccountRef0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'itemsIncomeAccountRef']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsExpenseAccountRef0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'itemsExpenseAccountRef']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
    public static function getFinanceITems()
    {
        return Self::find()->select(['itemsName', 'id'])->indexBy('id')->column();
    }
    
}
