<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'itemsName') ?>

    <?= $form->field($model, 'itemsCode') ?>

    <?= $form->field($model, 'itemsDescription') ?>

    <?= $form->field($model, 'itemsTaxable') ?>

    <?php // echo $form->field($model, 'itemsCustomerUnitPrice') ?>

    <?php // echo $form->field($model, 'itemsSupplierUnitPrice') ?>

    <?php // echo $form->field($model, 'itemsClassification') ?>

    <?php // echo $form->field($model, 'itemsIncomeAccountRef') ?>

    <?php // echo $form->field($model, 'itemsPurchaseDesc') ?>

    <?php // echo $form->field($model, 'itemsPurchaseCost') ?>

    <?php // echo $form->field($model, 'itemsExpenseAccountRef') ?>

    <?php // echo $form->field($model, 'itemsAssetAccountRef') ?>

    <?php // echo $form->field($model, 'itemsCreatedAt') ?>

    <?php // echo $form->field($model, 'itemsLastupdatedAt') ?>

    <?php // echo $form->field($model, 'itemsDeletedAt') ?>

    <?php // echo $form->field($model, 'itemsStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
