<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\PdfAsset;

PdfAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="multi_color">
<?php $this->beginBody() ?>
<!-- begin row -->
<div class="row">
  <div class="col-md-12">
    <?php // Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
  </div>
  <!-- end panel -->
</div>
<!-- end row -->
<!-- begin scroll to top btn -->
<a href="javascript:void(0)" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade"
data-click="scroll-top">
<i class="fa fa-angle-up"></i>
</a>
<!-- end scroll to top btn -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
