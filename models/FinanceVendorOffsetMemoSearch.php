<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceVendorOffsetMemo;

/**
 * FinanceVendorOffsetMemoSearch represents the model behind the search form of `app\models\FinanceVendorOffsetMemo`.
 */
class FinanceVendorOffsetMemoSearch extends FinanceVendorOffsetMemo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vendorOffsetMemoAccount', 'vendorId', 'vendorOffsetMemoStatus', 'userId'], 'integer'],
            [['refernumber', 'vendorOffsetMemoTxnDate', 'vendorOffsetMemoNote', 'vendorOffsetMemoCreatedAt', 'vendorOffsetMemoUpdatedAt', 'vendorOffsetMemoDeletedAt'], 'safe'],
            [['offsetAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceVendorOffsetMemo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vendorOffsetMemoAccount' => $this->vendorOffsetMemoAccount,
            'vendorOffsetMemoTxnDate' => $this->vendorOffsetMemoTxnDate,
            'vendorId' => $this->vendorId,
            'offsetAmount' => $this->offsetAmount,
            'vendorOffsetMemoStatus' => $this->vendorOffsetMemoStatus,
            'vendorOffsetMemoCreatedAt' => $this->vendorOffsetMemoCreatedAt,
            'vendorOffsetMemoUpdatedAt' => $this->vendorOffsetMemoUpdatedAt,
            'vendorOffsetMemoDeletedAt' => $this->vendorOffsetMemoDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'refernumber', $this->refernumber])
            ->andFilterWhere(['like', 'vendorOffsetMemoNote', $this->vendorOffsetMemoNote]);

        return $dataProvider;
    }
}
