<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\journal\models\FinanceJournalEntryLines */

$this->title = 'Create Finance Journal Entry Lines';
$this->params['breadcrumbs'][] = ['label' => 'Finance Journal Entry Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-journal-entry-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
