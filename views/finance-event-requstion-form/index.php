<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\models\FinanceEstimate;
use app\models\FinanceEstimateSearch;
use app\models\FinanceEventRequstionFormList;
use app\models\FinanceEventRequstionFormListSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEventRequstionFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Event Requstion Forms';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="finance-event-requstion-form-index">
                <div class="table-responsive">
                  <p>
                      <?= Html::a('Create Requstion', ['create'], ['class' => 'btn btn-success']) ?>
                  </p>
              <?php Pjax::begin(); ?>

                  <?=  GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
                          'rowOptions'=> function($model)
                          {
                            // code...
                            if ($model->requstionStatus==1) {
                              // code...
                              return['class'=>'success'];
                            } else {
                              // code...
                              return['class'=>'danger'];
                            }

                          },
                          'columns' => [
                              ['class' => 'kartik\grid\ExpandRowColumn',
                                'value'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  return GridView::ROW_COLLAPSED;
                                },
                                'detail'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  $searchModel = new FinanceEventRequstionFormListSearch();
                                  $searchModel->reqId = $model->id;
                                  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                                  return Yii::$app->controller->renderPartial('_requisitionlines',[
                                    'searchModel' => $searchModel,
                                    'dataProvider' => $dataProvider,

                                  ]);
                                },
                            ],

                            // 'id',
                            'requstionNumber',
                            'event.eventNumber',
                            'date',
                            // 'requstionStatus',
                            'requstionNote:ntext',
                            'amount',
                            //'preparedBy',
                            //'createdAt',
                            //'updatedAt',
                            //'deletedAt',
                            //'userId',
                            //'approvedBy',


                            ['class' => 'yii\grid\ActionColumn'],
                          ],
                      ]);
                      ?>

                </div>

                <?php Pjax::end(); ?>
             </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
