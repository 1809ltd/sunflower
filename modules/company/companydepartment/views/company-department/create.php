<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companydepartment\models\CompanyDepartment */

$this->title = 'Create Company Department';
$this->params['breadcrumbs'][] = ['label' => 'Company Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-department-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
