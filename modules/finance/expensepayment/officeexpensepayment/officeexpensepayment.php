<?php

namespace app\modules\finance\expensepayment\officeexpensepayment;

/**
 * officeexpensepayment module definition class
 */
class officeexpensepayment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expensepayment\officeexpensepayment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
