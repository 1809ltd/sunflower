<?php

namespace app\modules\finance\offset\inhouse\inoffsetinvoice\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\offset\inhouse\inoffsetinvoice\models\FinanceOffsetMemoLines;

/**
 * FinanceOffsetMemoLinesSearch represents the model behind the search form of `app\modules\finance\offset\inhouse\inoffsetinvoice\models\FinanceOffsetMemoLines`.
 */
class FinanceOffsetMemoLinesSearch extends FinanceOffsetMemoLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'offsetMemoId', 'offsetMemoLineStatus', 'userId'], 'integer'],
            [['invoiceNumber', 'offsetMemoLineDescription', 'offsetMemoLineCreatedAt', 'offsetMemoLineUpdatedAt', 'creidtDeletedAt'], 'safe'],
            [['offsetMemoLineAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceOffsetMemoLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'offsetMemoId' => $this->offsetMemoId,
            'offsetMemoLineAmount' => $this->offsetMemoLineAmount,
            'offsetMemoLineStatus' => $this->offsetMemoLineStatus,
            'offsetMemoLineCreatedAt' => $this->offsetMemoLineCreatedAt,
            'offsetMemoLineUpdatedAt' => $this->offsetMemoLineUpdatedAt,
            'creidtDeletedAt' => $this->creidtDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'invoiceNumber', $this->invoiceNumber])
            ->andFilterWhere(['like', 'offsetMemoLineDescription', $this->offsetMemoLineDescription]);

        return $dataProvider;
    }
}
