<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
// use yii\helpers\Url;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/*GEtting the Bills*/
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLinesSearch;

/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBillsPayments */

$this->title = $model->billPaymentBillRef;
$this->params['breadcrumbs'][] = ['label' => 'Bill Payment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();

/*All Payment for this invoice, i.e offset,creditnote,actualpayment*/
// $payments=$model->allPayments($model->invoiceNumber);

?>
<!-- <div class="finance-bills-payments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
    //  DetailView::widget([
    //     'model' => $model,
    //     'attributes' => [
    //       'id',
    //       'billPaymentPayType',
    //       'billPaymentaccount',
    //       'billPaymentTotalAmt',
    //       'billPaymentBillRef',
    //       'vendorId',
    //       'billPaymentPrivateNote:ntext',
    //       'billPaymentCreatedAt',
    //       'billPaymentUpdatedAt',
    //       'billPaymentDeletedAt',
    //       'userId',
    //       'paymentstatus',
    //     ],
    // ]) ?>

</div> -->

<!-- this row will not appear when printing -->
<div class="row no-print">
  <p>
      <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
  </p>
  <!-- Pop up Form -->
  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h6>
           <img style="height:50%;width:50%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h6>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Payment Voucher</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         To:
         <address>
           <strong><?= $model["vendor"]["vendorCompanyName"];?>.</strong><br>
           Kra Pin <?= $model["vendor"]["vendorKraPin"];?><br>
           <?= $model["vendor"]["vendorCompanyAddress"];?><br>
           Phone: <?= $model["vendor"]["vendorPhone"];?><br>
           Email: <?= $model["vendor"]["vendoremail"];?>
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         Contact:
         <address>
           <strong><?= $model["vendor"]["vendorContactPersonFullName"];?></strong><br>
           <!-- 795 Folsom Ave, Suite 600<br>
           San Francisco, CA 94107<br> -->
           Phone: <?= $model["vendor"]["contactMobilePhone"];?><br>
           Email: <?= $model["vendor"]["contactEmail"];?>
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Tranasaction /Cheque No: #</b><?= $model->billPaymentBillRef ?><br>
         <br>
         <b>Payment Account:</b><?= $model->billPaymentaccount0["fullyQualifiedName"];?> <br>
         <b>Payment Date:</b> <?= Yii::$app->formatter->asDate($model->date, 'long');?><br>
         <!-- <b>Account:</b> 968-34567 -->
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Invoice Number:</th>
             <th>Vendor Receipt:</th>
             <th>Sub Amount</th>
           </thead>
           <tbody>
             <?php

             $billpayementItems=$model->getbillpaymentItems($model->id,$model->paymentstatus);

             // print_r($billpayementItems);
             // die();

             foreach ($billpayementItems as $billpayementItem) {
               // code...

               ?>
               <tr>
                   <td><?=$billpayementItem["bill"]["billRef"];?></td>
                   <td><?= $billpayementItem->referenceNo;?></td>
                   <td>Ksh <?= number_format($billpayementItem->amount,2) ;?></td>

               </tr>

               <?php

             }

             // die();

              ?>

           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <br>
     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="" style="margin-top: 10px;">
           <?= $model->billPaymentPrivateNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="" style="margin-top: 10px;">
          <?php
          // $model->invoiceFooter; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Payment Methods:  <?= $model->billPaymentPayType?></p>
         <div class="col-md-8">
         </div>

         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">

           * If you have any questions concerning this Payment Voucher<br/>
            contact [<?= $model->createdBy0['userFName'].'  '.$model->createdBy0['userLName']?>, <br/>
           Phone Number:    <?= $model->createdBy0['userPhone']?><br/>
           Email:  <?= $model->createdBy0['userEmail']?>]
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">

         <p class="lead">Unallocated Amount <strong>Ksh <?= number_format($model->unallocatedAmount,2) ;?></strong></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Total:</th>
               <td>Ksh <?= number_format($model->billPaymentTotalAmt,2) ;?></td>
             </tr>

           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->
         <?= Html::button('Submit Payment', ['value'=>Url::to((['makepayments', 'id' => $model['id']])),'class' => 'btn btn-success pull-right','id'=>'modalButton']) ?>
         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
