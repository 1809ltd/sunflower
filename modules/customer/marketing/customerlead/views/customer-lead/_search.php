<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLeadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-lead-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'customerId') ?>

    <?= $form->field($model, 'leadprojectName') ?>

    <?= $form->field($model, 'leadProjectclassification') ?>

    <?= $form->field($model, 'leadGenerator') ?>

    <?php // echo $form->field($model, 'leadGeneratorId') ?>

    <?php // echo $form->field($model, 'leadReferredby') ?>

    <?php // echo $form->field($model, 'leadReferredbyRelation') ?>

    <?php // echo $form->field($model, 'leadCreatedAt') ?>

    <?php // echo $form->field($model, 'leadLastUpdatedAt') ?>

    <?php // echo $form->field($model, 'leadDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
