<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AssetAmortizationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asset Amortizations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-amortization-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Asset Amortization', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'assetId',
            'repayment',
            'interestAmount',
            'principalAmount',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',
            //'status',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
