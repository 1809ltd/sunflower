<?php

namespace app\modules\customer;

/**
 * customer module definition class
 */
class customer extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\customer\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Customer details Sub Module*/
          'customerdetails' => [
                'class' => 'app\modules\customer\customerdetails\customerdetails',
            ],
            /*marketing Sub module*/
          'marketing' => [
              'class' => 'app\modules\customer\marketing\marketing',
          ],
        ];

        // custom initialization code goes here
    }
}
