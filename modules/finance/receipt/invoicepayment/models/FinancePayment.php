<?php
namespace app\modules\finance\receipt\invoicepayment\models;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
// Invoice Payemnts
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLinesSearch;

use Yii;

/**
 * This is the model class for table "finance_payment".
 *
 * @property int $id
 * @property string $invoicePaymentPayType
 * @property int $invoicePaymentaccount
 * @property string $recepitNumber
 * @property double $invoicePaymentTotalAmt
 * @property double $unallocatedAmount
 * @property string $date
 * @property string $invoicePaymentinvoiceRef
 * @property int $customerId
 * @property string $invoicePaymentPrivateNote
 * @property string $invoicePaymentCreatedAt
 * @property string $invoicePaymentUpdatedAt
 * @property string $invoicePaymentDeletedAt
 * @property int $userId
 * @property int $createdBy
 * @property int $approvedBy
 * @property int $paymentstatus
 *
 * @property FinanceAccounts $invoicePaymentaccount0
 * @property UserDetails $user
 * @property CustomerDetails $customer
 * @property UserDetails $createdBy0
 * @property UserDetails $approvedBy0
 * @property FinancePaymentLines[] $financePaymentLines
 */
class FinancePayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoicePaymentaccount', 'recepitNumber', 'invoicePaymentTotalAmt', 'unallocatedAmount', 'date', 'invoicePaymentinvoiceRef', 'userId', 'createdBy', 'paymentstatus'], 'required'],
            [['invoicePaymentaccount', 'customerId', 'userId', 'createdBy', 'approvedBy', 'paymentstatus'], 'integer'],
            [['invoicePaymentTotalAmt', 'unallocatedAmount'], 'number'],
            ['unallocatedAmount', 'match', 'pattern' => '/^[0-9]*$/'],
            ['unallocatedAmount', 'integer', 'min' => 0, 'max' => 1000000],
            [['date', 'invoicePaymentCreatedAt', 'invoicePaymentUpdatedAt', 'invoicePaymentDeletedAt'], 'safe'],
            [['invoicePaymentPrivateNote'], 'string'],
            [['invoicePaymentPayType', 'invoicePaymentinvoiceRef'], 'string', 'max' => 50],
            [['recepitNumber'], 'string', 'max' => 100],
            [['recepitNumber'], 'unique'],
            [['invoicePaymentaccount', 'invoicePaymentinvoiceRef'], 'unique', 'message'=>'The account Transaction already exist for this Account. Please try another one.', 'targetAttribute' => ['invoicePaymentaccount', 'invoicePaymentinvoiceRef']],
            [['invoicePaymentaccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['invoicePaymentaccount' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'invoicePaymentPayType' => 'Payment Method',
          'invoicePaymentaccount' => 'Payment Account',
          'recepitNumber' => 'Recepit Number',
          'invoicePaymentTotalAmt' => 'Value',
          'unallocatedAmount' => 'Unallocated Amount',
          'date' => 'Date',
          'invoicePaymentinvoiceRef' => 'Transaction Code',
          'customerId' => 'Customer',
          'invoicePaymentPrivateNote' => 'Private Note',
          'invoicePaymentCreatedAt' => 'Invoice Payment Created At',
          'invoicePaymentUpdatedAt' => 'Invoice Payment Updated At',
          'invoicePaymentDeletedAt' => 'Invoice Payment Deleted At',
          'userId' => 'User',
          'createdBy' => 'Created By',
          'approvedBy' => 'Approved By',
          'paymentstatus' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoicePaymentaccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'invoicePaymentaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePaymentLines()
    {
        return $this->hasMany(FinancePaymentLines::className(), ['invoicepaymentId' => 'id']);
    }
    // Generate Recepitnumbers
    public function getRcpnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(recepitNumber) AS number from finance_payment where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "RCP-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "RCP-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "RCP-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
    public function getallpaymentslinesonhold($customerId)
    {
        $paymentsonHold = FinancePaymentLines::find()
            ->joinWith('invoice')
            ->where(['finance_payment_lines.customerId' => $customerId])
            ->Andwhere(['finance_payment_lines.status'=>3])
            ->asArray()
            ->all();
        // print_r($paymentsonHold);

        return $paymentsonHold;

        // return self::find()
        //       ->select(['id','eventName as name'])
        //       ->where(['id' => $project_id])->indexBy('id')->column();
    }

    public function getinvoicepaymentItems($id,$paymentstatus)
    {
      return FinancePaymentLines::find()
          ->joinWith('invoice')
          ->where(['finance_payment_lines.invoicepaymentId' => $id])
          ->Andwhere(['finance_payment_lines.status'=>$paymentstatus])
          ->all();
    }
}
