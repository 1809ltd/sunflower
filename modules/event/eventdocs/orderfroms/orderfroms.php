<?php

namespace app\modules\event\eventdocs\orderfroms;

/**
 * orderfroms module definition class
 */
class orderfroms extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventdocs\orderfroms\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[

          /*Event Order form lines Sub Module*/
          'orderformsitems' => [
              'class' => 'app\modules\event\eventdocs\orderfroms\orderformsitems\orderformsitems',
          ],
        ];

        // custom initialization code goes here
    }
}
