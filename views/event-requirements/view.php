<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EventRequirements */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Requirements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-requirements-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'eventId',
            'eventNumber',
            'department',
            'departmentName',
            'requirementDetails:ntext',
            'requirementSize',
            'requirementSIUnit',
            'requirementCreatedAt',
            'requirementLastUpdated',
            // 'requirementDeletedAt',
            'preparedBy',
            'approvedBy',
            // 'requirementStatus',
        ],
    ]) ?>

</div>
