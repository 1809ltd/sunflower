<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use app\models\FinanceInvoiceLines;
use app\models\FinanceInvoiceLinesSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoice */

$this->title = $model->invoiceNumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<p>
    <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
</p>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?=$model->invoiceTxnDate;?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         From
         <address>
           <strong>SUNFLOWER EVENTS,</strong><br/>
            Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
           Nairobi, 00100 Kenya<br/>
           Phone: +254 (0) 722 790632<br/>
           Email: info@...com
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         To
         <address>
           <?php $customerinfo=$model->getCustomerDetails($model->customerId);

           // print_r($customerinfo);

           foreach ($customerinfo as $customerinfo) {
             // code...

           ?>
           <strong><?= $customerinfo->customerDisplayName;?></strong><br/>
           <?= $customerinfo->customercity;?>, <?= $customerinfo->customerPostalCode;?><br/>
           Phone: <?= $customerinfo->customerPrimaryPhone;?><br/>
           Email: <?= $customerinfo->customerPrimaryEmail;?><br/>
             <?php
           }

           ?>
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Invoice #<?=$model->invoiceNumber;?></b><br>
         <br>
         <b>Event:</b> <<?php
         // =$model->invoiceProjectName;?><br>
         <b>Payment Due:</b> <?=$model->invoiceTxnDate;?><br>
         <b>Account:</b> ...
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Product or Service Description</th>
             <th>Days</th>
             <th>Qty</th>
             <th>Price</th>
             <th>Discount(%)</th>
             <th>Amount</th>
           </thead>
           <tbody>
             <?php
             $invoiceItems=$model->getinvoiceItems($model->id);

             // print_r($invoiceItems);
             //
             // die();

             foreach ($invoiceItems as $invoiceItem) {
               // code...
               // $invoiceItem->invoiceId;
               // $invoiceItem->itemRefId;
               // $invoiceItem->itemRefName;
               // $invoiceItem->invoiceLinesDescription;
               // $invoiceItem->invoiceLinespack;
               // $invoiceItem->invoiceLinesQty;
               // $invoiceItem->invoiceLinesUnitPrice;
               // $invoiceItem->invoiceLinesAmount;
               // $invoiceItem->invoiceLinesTaxCodeRef;
               // $invoiceItem->invoiceLinesTaxamount;
               // $invoiceItem->invoiceLinesDiscount;
               // $invoiceItem->invoiceLinesCreatedAt;
               // $invoiceItem->invoiceLinesUpdatedAt;
               // $invoiceItem->invoiceLinesDeletedAt;
               // $invoiceItem->invoiceLinesStatus;
               ?>
               <tr>
                   <td>
                       <?= $invoiceItem->itemRefName;?><br/>
                       <small><?= $invoiceItem->invoiceLinesDescription;?>.
                       </small>
                   </td>
                   <td><?=$invoiceItem->invoiceLinespack;?></td>
                   <td><?=$invoiceItem->invoiceLinesQty;?></td>
                   <td>Ksh <?= $invoiceItem->invoiceLinesUnitPrice;?></td>
                   <td><?= $invoiceItem->invoiceLinesDiscount;?></td>
                   <td>Ksh <?= $invoiceItem->invoiceLinesAmount;?></td>
               </tr>

               <?php

             }

              ?>

           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           <?= $model->invoiceNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          <?= $model->invoiceFooter; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Payment Methods:</p>
         <img src="adminlte/dist/img/credit/visa.png" alt="Visa">
         <img src="adminlte/dist/img/credit/mastercard.png" alt="Mastercard">
         <img src="adminlte/dist/img/credit/american-express.png" alt="American Express">
         <img src="adminlte/dist/img/credit/paypal2.png" alt="Paypal">

         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           * Make all cheques payable to Sunflower Events<br/>
           * Payment is due within 30 days<br/>
           * If you have any questions concerning this invoice, contact [Name, Phone Number,
           Email]
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="lead">Amount Due <?= $model->invoiceDeadlineDate; ?></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th style="width:50%">Subtotal:</th>
               <td>Ksh <?= $model->invoiceSubAmount;?></td>
             </tr>
             <tr>
               <th>Total Tax</th>
               <td>Ksh <?= $model->invoiceTaxAmount; ?></td>
             </tr>
             <tr>
               <th>Discount:</th>
               <td>Ksh <?= $model->invoiceDiscountAmount; ?></td>
             </tr>
             <tr>
               <th>Total:</th>
               <td>Ksh <?= $model->invoiceAmount; ?></td>
             </tr>
           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
         <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button>
         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
