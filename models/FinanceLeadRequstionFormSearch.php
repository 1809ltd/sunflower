<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceLeadRequstionForm;

/**
 * FinanceLeadRequstionFormSearch represents the model behind the search form of `app\models\FinanceLeadRequstionForm`.
 */
class FinanceLeadRequstionFormSearch extends FinanceLeadRequstionForm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'requstionStatus', 'preparedBy', 'userId', 'approvedBy'], 'integer'],
            [['requstionNumber', 'date', 'requstionNote', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceLeadRequstionForm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'leadId' => $this->leadId,
            'date' => $this->date,
            'requstionStatus' => $this->requstionStatus,
            'amount' => $this->amount,
            'preparedBy' => $this->preparedBy,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'approvedBy' => $this->approvedBy,
        ]);

        $query->andFilterWhere(['like', 'requstionNumber', $this->requstionNumber])
            ->andFilterWhere(['like', 'requstionNote', $this->requstionNote]);

        return $dataProvider;
    }
}
