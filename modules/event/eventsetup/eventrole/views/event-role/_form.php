<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\EventRole */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body company-site-location-form">

  <?= $form->field($model, 'roleName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

  <?= $form->field($model, 'roleDescription')->textarea(['rows' => 6]) ?>

  <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
  <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']); ?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
<?php
// $script =<<< JS
// $('form#{$model->formName()}').on('beforeSubmit',function(e){
//   var \$form = $(this);
//   $.post(
//     \$form.attr("action"),//serialize yii2 form
//     \$form.serialize()
//     )
//     .done(function(result) {
//       if (result.message=='success') {
//         // code...
//         // $(document).find('#modal').modal('hide');
//         // $.pjax.reload({container:'#commodity-grid'});
//       } else {
//         // code...
//         $(\$form).trigger("reset");
//         $("#message").html(result.message);
//       }
//     }).fail(function()
//     {
//       console.log(server error);
//     });
//     return false;
// });
//
// JS;
// $this->registerJS($script);


 ?>
