<?php

namespace app\modules\finance\expensepayment\officeexpensepayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;

/**
 * FinanceOfficeRequisitionPaymentsLinesSearch represents the model behind the search form of `app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines`.
 */
class FinanceOfficeRequisitionPaymentsLinesSearch extends FinanceOfficeRequisitionPaymentsLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'officeReqpaymentId', 'officeReqId', 'status', 'userId'], 'integer'],
            [['amount'], 'number'],
            [['deletedAt', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceOfficeRequisitionPaymentsLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'officeReqpaymentId' => $this->officeReqpaymentId,
            'officeReqId' => $this->officeReqId,
            'amount' => $this->amount,
            'status' => $this->status,
            'deletedAt' => $this->deletedAt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'userId' => $this->userId,
        ]);

        return $dataProvider;
    }
}
