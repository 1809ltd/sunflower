<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

if (isset($_SESSION['startDate'])&&isset($_SESSION['endDate'])) {
  // code...
  // $model->eventId = $_SESSION['startDate'];
  $model->startDate= \Yii::$app->session->get('startDate');
  $model->endDate= \Yii::$app->session->get('endDate');
  // echo "we are here";
  // echo "start Date".$model->startDate;
  // echo "End Date".$model->endDate;
  ?>
  <!-- Main content -->
<!-- Search Date And the rest -->
  <div class="box no-print">
      <div class="box-header with-border">
        <h3 class="box-title">Search</h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <?php $form = ActiveForm::begin(); ?>
          <div class="col-md-4">
            <div class="form-group">
              <label class="col-lg-4 control-label">Date From: </label>
              <div class="col-lg-8">
                <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="col-lg-4 control-label">Date To: </label>
                <div class="col-lg-8">
                  <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['autocomplete'=>'off','class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ])->label(false) ?>
                </div>
              </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <div class="col-lg-8 col-lg-offset-4">
                <div class="center-align">
                  <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
                </div>
              </div>
            </div>
          </div>
          <?php ActiveForm::end(); ?>
        </div>
    </div>
  </div>

  <section class="invoice">
        <div class="text-center">
          <h6 class="box-title">
            <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
          </h6>
          <h3 class="box-title">Income Statement</h3>
          <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
          <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
        </div>

        <div class="box">
          <div class="box-body">
            <h5 class="box-title">REVENUE</h5>
            <table class="table  table-striped table-condensed">
                <thead>
                  <tr>
                    <th class="text-left">Account</th>
                    <th class="text-right">Balance</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  $revenueTotal=0;

                  $connection = Yii::$app->getDb();
                  $command1 = $connection->createCommand("SELECT
                                all_transactions.accountParentId,
                                all_transactions.accountsclassfication,
                                all_transactions.accountId,
                                all_transactions.accountName,
                                Sum( all_transactions.dr_amount ),
                                Sum( all_transactions.cr_amount ),
                                all_transactions.transactionClassification,
                                all_transactions.transactionCategory
                              FROM
                                all_transactions
                              WHERE
                                all_transactions.`transactionCategory` LIKE 'Revenue' and
                                (all_transactions.`transactionDate` BETWEEN '$model->startDate' and '$model->endDate')
                              GROUP BY
                                all_transactions.accountId,
                                all_transactions.transactionClassification
                              ORDER BY
                                all_transactions.accountParentId ASC");
                  $revenue = $command1->query();

                  foreach ($revenue as $revenue){
                    // code...
                    $revenueTotal+= $revenue["Sum( all_transactions.cr_amount )"];
                    $transactionAccountId= $revenue["accountId"];
                    ?>
                    <tr>
                      <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $transactionAccountId]) ?>"><?= $revenue["accountName"];?></a></td>
                      <td  class="text-right"><span class="align-numbers"><?= number_format($revenue["Sum( all_transactions.cr_amount )"],2) ;?></span></td>
                    </tr>
                   <?php
                 }
                 ?>
                 <tr>
                   <th  class="text-left">Total Revenue:</th>
                   <td  class="text-right"><?= number_format($revenueTotal,2); ?></td>
                 </tr>
               </tbody>
            </table>
            </br>
            <h5 class="box-title">COST OF GOODS SOLD</h5>
            <table class="table  table-striped table-condensed">
               <thead>
                 <tr>
                   <th class="text-left">Account</th>
                   <th class="text-right">Balance</th>
                 </tr>
               </thead>
               <tbody>
                 <?php
                 $command2 = $connection->createCommand("SELECT
                                                             all_transactions.accountParentId,
                                                             all_transactions.accountsclassfication,
                                                             all_transactions.accountId,
                                                             all_transactions.accountName,
                                                             Sum( all_transactions.dr_amount ),
                                                             Sum( all_transactions.cr_amount ),
                                                             all_transactions.transactionClassification,
                                                             all_transactions.transactionCategory
                                                           FROM
                                                             all_transactions
                                                           WHERE
                                                             all_transactions.transactionClassification LIKE 'Event expense'
                                                             OR all_transactions.transactionClassification LIKE 'Bill Expense' and
                                                             (all_transactions.`transactionDate` BETWEEN '$model->startDate' and '$model->endDate')

                                                           GROUP BY
                                                             all_transactions.accountId,
                                                             all_transactions.transactionClassification
                                                           ORDER BY
                                                             all_transactions.accountParentId ASC");

                $COGS = $command2->query();
                $COGSTotal=0;

                foreach ($COGS as $COGS){
                  // code...
                  $COGSTotal+= $COGS["Sum( all_transactions.dr_amount )"];

                  $cogsAccountId= $COGS["accountId"];
                  ?>
                  <tr>
                    <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $cogsAccountId]) ?>"><?= $COGS["accountName"];?></a></td>
                    <td  class="text-right"><span class="align-numbers"><?= number_format($COGS["Sum( all_transactions.dr_amount )"],2) ;?></span></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <th class="text-left">Total Cost of Goods Sold (COGS):</th>
                  <th class="text-right"><?= number_format($COGSTotal,2); ?></td>
                </tr>
                <tr>
                  <th class="text-left">Gross Profit:</th>
                  <td class="text-right"><?= number_format($revenueTotal-$COGSTotal,2); ?></td>
                </tr>
              </tbody>
            </table>
            </br>
            <h5 class="box-title">OPERATING EXPENSE</h5>
            <table class="table  table-striped table-condensed">
              <thead>
                <tr>
                  <th class="text-left">Account</th>
                  <th class="text-right">Balance</th>
                </tr>
              </thead>
              <tbody>
                <?php
                // echo $operation_result
                $command3 = $connection->createCommand("SELECT
                  all_transactions.accountParentId,
                  all_transactions.accountsclassfication,
                  all_transactions.accountId,
                  all_transactions.accountName,
                  Sum( all_transactions.dr_amount ),
                  Sum( all_transactions.cr_amount ),
                  all_transactions.transactionClassification,
                  all_transactions.transactionCategory
                  FROM
                  all_transactions
                  WHERE
                  all_transactions.transactionClassification LIKE 'Office Expense'
                  OR all_transactions.transactionClassification LIKE 'Lead expense' and
                  (all_transactions.`transactionDate` BETWEEN '$model->startDate' and '$model->endDate')
                  GROUP BY
                  all_transactions.accountId,
                  all_transactions.transactionClassification
                  ORDER BY
                  all_transactions.accountParentId ASC");
                  $operating = $command3->query();

                  $operatingTotal=0;
                  foreach ($operating as $operating){
                    // code...
                    $operatingTotal+= $operating["Sum( all_transactions.dr_amount )"];
                    $operatingAccountId= $operating["accountId"];
                    ?>
                    <tr>
                      <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $operatingAccountId]) ?>"><?= $operating["accountName"];?></a></td>
                      <td  class="text-right"><span class="align-numbers"><?= number_format($operating["Sum( all_transactions.dr_amount )"],2) ;?></span></td>
                    </tr>
                    <?php
                  }
                  ?>
                  <tr>
                    <th class="text-left">Total Operating Expenses:</th>
                    <th class="text-right"><?= number_format($operatingTotal,2); ?></th>
                  </tr>
                  <tr>
                    <th class="text-left">Operating Profit (Loss)</th>
                    <th class="text-right"><?= number_format($revenueTotal-$COGSTotal-$operatingTotal,2); ?></th>
                  </tr>
                </table>
              </tbody>
            </table>
            </br>
            <h5 class="box-title">INTEREST (INCOME), EXPENSE & TAXES</h5>
            <table class="table  table-striped table-condensed">
              <thead>
                <tr>
                  <th class="text-left"></th>
                  <th class="text-right"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="text-left"><strong>NET Profit</strong></th>
                  <th class="text-right"><?php // echo number_format($total_income - $total_cog - $total_operational_amount,2)?></th>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>

            <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF -->
          <!-- </button> -->
        </div>
      </div>
  </section>
  <?php
} else {
  // code...

  $model->startDate= date('Y-01-01');
  $model->endDate= date('Y-m-d');
  ?>
  <!-- Main content -->
<!-- Search Date And the rest -->
  <div class="box no-print">
      <div class="box-header with-border">
        <h3 class="box-title">Search</h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <?php $form = ActiveForm::begin(); ?>
          <div class="col-md-4">
            <div class="form-group">
              <label class="col-lg-4 control-label">Date From: </label>
              <div class="col-lg-8">
                <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="form-group">
                <label class="col-lg-4 control-label">Date To: </label>
                <div class="col-lg-8">
                  <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['autocomplete'=>'off','class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ])->label(false) ?>
                </div>
              </div>
            </div>
          <div class="col-md-4">
            <div class="form-group">
              <div class="col-lg-8 col-lg-offset-4">
                <div class="center-align">
                  <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
                </div>
              </div>
            </div>
          </div>
          <?php ActiveForm::end(); ?>
        </div>
    </div>
  </div>

  <section class="invoice">
        <div class="text-center">
          <h6 class="box-title">
            <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
          </h6>
          <h3 class="box-title">Income Statement</h3>
          <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
          <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
        </div>

        <div class="box">
          <div class="box-body">
            <h5 class="box-title">REVENUE</h5>
            <table class="table  table-striped table-condensed">
                <thead>
                  <tr>
                    <th class="text-left">Account</th>
                    <th class="text-right">Balance</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                  $revenueTotal=0;

                  $connection = Yii::$app->getDb();
                  $command1 = $connection->createCommand("SELECT
                                all_transactions.accountParentId,
                                all_transactions.accountsclassfication,
                                all_transactions.accountId,
                                all_transactions.accountName,
                                Sum( all_transactions.dr_amount ),
                                Sum( all_transactions.cr_amount ),
                                all_transactions.transactionClassification,
                                all_transactions.transactionCategory
                              FROM
                                all_transactions
                              WHERE
                                all_transactions.`transactionCategory` LIKE 'Revenue' and
                                (all_transactions.`transactionDate` BETWEEN '$model->startDate' and '$model->endDate')
                              GROUP BY
                                all_transactions.accountId,
                                all_transactions.transactionClassification
                              ORDER BY
                                all_transactions.accountParentId ASC");
                  $revenue = $command1->query();

                  foreach ($revenue as $revenue){
                    // code...
                    $revenueTotal+= $revenue["Sum( all_transactions.cr_amount )"];
                    $transactionAccountId= $revenue["accountId"];
                    ?>
                    <tr>
                      <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $transactionAccountId]) ?>"><?= $revenue["accountName"];?></a></td>
                      <td  class="text-right"><span class="align-numbers"><?= number_format($revenue["Sum( all_transactions.cr_amount )"],2) ;?></span></td>
                    </tr>
                   <?php
                 }
                 ?>
                 <tr>
                   <th  class="text-left">Total Revenue:</th>
                   <td  class="text-right"><?= number_format($revenueTotal,2); ?></td>
                 </tr>
               </tbody>
            </table>
            </br>
            <h5 class="box-title">COST OF GOODS SOLD</h5>
            <table class="table  table-striped table-condensed">
               <thead>
                 <tr>
                   <th class="text-left">Account</th>
                   <th class="text-right">Balance</th>
                 </tr>
               </thead>
               <tbody>
                 <?php
                 $command2 = $connection->createCommand("SELECT
                                                             all_transactions.accountParentId,
                                                             all_transactions.accountsclassfication,
                                                             all_transactions.accountId,
                                                             all_transactions.accountName,
                                                             Sum( all_transactions.dr_amount ),
                                                             Sum( all_transactions.cr_amount ),
                                                             all_transactions.transactionClassification,
                                                             all_transactions.transactionCategory
                                                           FROM
                                                             all_transactions
                                                           WHERE
                                                             all_transactions.transactionClassification LIKE 'Event expense'
                                                             OR all_transactions.transactionClassification LIKE 'Bill Expense' and
                                                             (all_transactions.`transactionDate` BETWEEN '$model->startDate' and '$model->endDate')

                                                           GROUP BY
                                                             all_transactions.accountId,
                                                             all_transactions.transactionClassification
                                                           ORDER BY
                                                             all_transactions.accountParentId ASC");

                $COGS = $command2->query();
                $COGSTotal=0;

                foreach ($COGS as $COGS){
                  // code...
                  $COGSTotal+= $COGS["Sum( all_transactions.dr_amount )"];

                  $cogsAccountId= $COGS["accountId"];
                  ?>
                  <tr>
                    <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $cogsAccountId]) ?>"><?= $COGS["accountName"];?></a></td>
                    <td  class="text-right"><span class="align-numbers"><?= number_format($COGS["Sum( all_transactions.dr_amount )"],2) ;?></span></td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <th class="text-left">Total Cost of Goods Sold (COGS):</th>
                  <th class="text-right"><?= number_format($COGSTotal,2); ?></td>
                </tr>
                <tr>
                  <th class="text-left">Gross Profit:</th>
                  <td class="text-right"><?= number_format($revenueTotal-$COGSTotal,2); ?></td>
                </tr>
              </tbody>
            </table>
            </br>
            <h5 class="box-title">OPERATING EXPENSE</h5>
            <table class="table  table-striped table-condensed">
              <thead>
                <tr>
                  <th class="text-left">Account</th>
                  <th class="text-right">Balance</th>
                </tr>
              </thead>
              <tbody>
                <?php
                // echo $operation_result
                $command3 = $connection->createCommand("SELECT
                  all_transactions.accountParentId,
                  all_transactions.accountsclassfication,
                  all_transactions.accountId,
                  all_transactions.accountName,
                  Sum( all_transactions.dr_amount ),
                  Sum( all_transactions.cr_amount ),
                  all_transactions.transactionClassification,
                  all_transactions.transactionCategory
                  FROM
                  all_transactions
                  WHERE
                  all_transactions.transactionClassification LIKE 'Office Expense'
                  OR all_transactions.transactionClassification LIKE 'Lead expense' and
                  (all_transactions.`transactionDate` BETWEEN '$model->startDate' and '$model->endDate')
                  GROUP BY
                  all_transactions.accountId,
                  all_transactions.transactionClassification
                  ORDER BY
                  all_transactions.accountParentId ASC");
                  $operating = $command3->query();

                  $operatingTotal=0;
                  foreach ($operating as $operating){
                    // code...
                    $operatingTotal+= $operating["Sum( all_transactions.dr_amount )"];
                    $operatingAccountId= $operating["accountId"];
                    ?>
                    <tr>
                      <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $operatingAccountId]) ?>"><?= $operating["accountName"];?></a></td>
                      <td  class="text-right"><span class="align-numbers"><?= number_format($operating["Sum( all_transactions.dr_amount )"],2) ;?></span></td>
                    </tr>
                    <?php
                  }
                  ?>
                  <tr>
                    <th class="text-left">Total Operating Expenses:</th>
                    <th class="text-right"><?= number_format($operatingTotal,2); ?></th>
                  </tr>
                  <tr>
                    <th class="text-left">Operating Profit (Loss)</th>
                    <th class="text-right"><?= number_format($revenueTotal-$COGSTotal-$operatingTotal,2); ?></th>
                  </tr>
                </table>
              </tbody>
            </table>
            </br>
            <h5 class="box-title">INTEREST (INCOME), EXPENSE & TAXES</h5>
            <table class="table  table-striped table-condensed">
              <thead>
                <tr>
                  <th class="text-left"></th>
                  <th class="text-right"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th class="text-left"><strong>NET Profit</strong></th>
                  <th class="text-right"><?php // echo number_format($total_income - $total_cog - $total_operational_amount,2)?></th>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>

            <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF -->
          <!-- </button> -->
        </div>
      </div>
  </section>
    <?php
  }
  ?>
