<?php

namespace app\modules\user\userrolemodulepermission\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*User roles Details*/
use app\modules\user\userroletypes\models\UserRoleTypes;
/*Module List*/
use app\modules\user\moduleslist\models\UserModulesList;


use Yii;

/**
 * This is the model class for table "user_role_module_permission".
 *
 * @property int $id
 * @property int $roleId
 * @property int $moduleId
 * @property int $new CREATE
 * @property int $view READ
 * @property int $save UPDATE
 * @property int $remove DELETE
 * @property int $other other Actions not listed in the list
 * @property int $createdBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $status
 *
 * @property UserRoleTypes $role
 * @property UserModulesList $module
 * @property UserDetails $createdBy0
 * @property UserDetails $user
 */
class UserRoleModulePermission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_role_module_permission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['roleId', 'moduleId', 'new', 'view', 'save', 'remove', 'other', 'createdBy', 'userId', 'status'], 'integer'],
            [['new', 'view', 'save', 'remove', 'other'], 'required'],
            [['createdAt', 'updatedAt', 'deletedAt', 'createdBy', 'userId'], 'safe'],
            [['roleId'], 'exist', 'skipOnError' => true, 'targetClass' => UserRoleTypes::className(), 'targetAttribute' => ['roleId' => 'id']],
            [['moduleId'], 'exist', 'skipOnError' => true, 'targetClass' => UserModulesList::className(), 'targetAttribute' => ['moduleId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'roleId' => 'Role ID',
            'moduleId' => 'Module ID',
            'new' => 'New',
            'view' => 'View',
            'save' => 'Save',
            'remove' => 'Remove',
            'other' => 'Other',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(UserRoleTypes::className(), ['id' => 'roleId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(UserModulesList::className(), ['id' => 'moduleId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
