<?php

namespace app\modules\finance\transfer\models;
/*Transer Finance*/
use app\modules\finance\transfer\models\FinanceTransfer;
/*Chart of Accounts*/
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*User Details*/
use app\models\UserDetails;

use Yii;

/**
 * This is the model class for table "finance_transferee".
 *
 * @property int $id
 * @property int $transferid
 * @property string $transfereeRefernce
 * @property int $transfereeFromAccountRef
 * @property int $transfereeToAccountRef
 * @property double $transfereeAmount
 * @property double $transferCharges
 * @property string $transfereeTxnDate
 * @property string $transfereeCreatedAt
 * @property string $transfereeUpdatedAt
 * @property string $transfereeDeletedAt
 * @property int $userId
 * @property int $status
 * @property int $createdBy
 *
 * @property FinanceTransfer $transfer
 * @property FinanceAccounts $transfereeFromAccountRef0
 * @property FinanceAccounts $transfereeToAccountRef0
 * @property UserDetails $createdBy0
 * @property UserDetails $user
 */
class FinanceTransferee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_transferee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transfereeFromAccountRef', 'transfereeToAccountRef'], 'required'],
            [['transferid', 'transfereeFromAccountRef', 'transfereeToAccountRef', 'userId', 'status', 'createdBy'], 'integer'],
            [['transfereeAmount', 'transferCharges'], 'number'],
            [['transferid', 'transfereeTxnDate', 'transfereeCreatedAt', 'transfereeUpdatedAt', 'transfereeDeletedAt', 'userId', 'status', 'createdBy', 'transfereeAmount'], 'safe'],
            [['transfereeRefernce'], 'string', 'max' => 100],
            [['transferid'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceTransfer::className(), 'targetAttribute' => ['transferid' => 'id']],
            [['transfereeFromAccountRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['transfereeFromAccountRef' => 'id']],
            [['transfereeToAccountRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['transfereeToAccountRef' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transferid' => 'Transferid',
            'transfereeRefernce' => 'Transferee Refernce',
            'transfereeFromAccountRef' => 'Transferee From Account Ref',
            'transfereeToAccountRef' => 'Transferee To Account Ref',
            'transfereeAmount' => 'Transferee Amount',
            'transferCharges' => 'To Transfer Charges',
            'transfereeTxnDate' => 'Transferee Txn Date',
            'transfereeCreatedAt' => 'Transferee Created At',
            'transfereeUpdatedAt' => 'Transferee Updated At',
            'transfereeDeletedAt' => 'Transferee Deleted At',
            'userId' => 'User ID',
            'status' => 'Status',
            'createdBy' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransfer()
    {
        return $this->hasOne(FinanceTransfer::className(), ['id' => 'transferid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransfereeFromAccountRef0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'transfereeFromAccountRef']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransfereeToAccountRef0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'transfereeToAccountRef']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
