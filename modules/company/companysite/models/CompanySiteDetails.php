<?php

namespace app\modules\company\companysite\models;
/*Company Details*/
use app\modules\company\companydetails\models\CompanyDetails;
/*Company Site Location*/
use app\modules\company\companysitelocation\models\CompanySiteLocation;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use Yii;

/**
 * This is the model class for table "company_site_details".
 *
 * @property int $id
 * @property int $companyId
 * @property string $siteName
 * @property string $siteAddress
 * @property string $siteSuite
 * @property string $siteCity
 * @property string $siteCounty
 * @property string $sitePostal
 * @property string $siteCountry
 * @property int $siteStatus
 * @property string $siteTimestamp
 * @property string $siteUpdatedAt
 * @property string $siteDeletedAt
 * @property int $companyUserId
 *
 * @property CompanyDetails $company
 * @property UserDetails $companyUser
 * @property CompanySiteLocation[] $companySiteLocations
 */
class CompanySiteDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_site_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyId', 'siteName', 'siteAddress', 'siteStatus', 'companyUserId'], 'required'],
            [['companyId', 'siteStatus', 'companyUserId'], 'integer'],
            [['siteTimestamp', 'siteUpdatedAt', 'siteDeletedAt'], 'safe'],
            [['siteName', 'siteAddress', 'siteSuite', 'siteCity', 'siteCounty', 'sitePostal', 'siteCountry'], 'string', 'max' => 255],
            [['companyId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyDetails::className(), 'targetAttribute' => ['companyId' => 'id']],
            [['companyUserId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['companyUserId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'companyId' => 'Company ID',
            'siteName' => 'Site Name',
            'siteAddress' => 'Site Address',
            'siteSuite' => 'Site Suite',
            'siteCity' => 'Site City',
            'siteCounty' => 'Site County',
            'sitePostal' => 'Site Postal',
            'siteCountry' => 'Site Country',
            'siteStatus' => 'Site Status',
            'siteTimestamp' => 'Site Timestamp',
            'siteUpdatedAt' => 'Site Updated At',
            'siteDeletedAt' => 'Site Deleted At',
            'companyUserId' => 'Company User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(CompanyDetails::className(), ['id' => 'companyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'companyUserId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySiteLocations()
    {
        return $this->hasMany(CompanySiteLocation::className(), ['siteId' => 'id']);
    }
}
