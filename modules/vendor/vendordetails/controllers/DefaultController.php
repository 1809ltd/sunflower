<?php

namespace app\modules\vendor\vendordetails\controllers;

use yii\web\Controller;

/**
 * Default controller for the `vendordetails` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
