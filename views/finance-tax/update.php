<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceTax */

$this->title = 'Update Finance Tax: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Taxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-tax-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
