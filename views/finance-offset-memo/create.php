<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOffsetMemo */

$this->title = 'Create Finance Offset Memo';
$this->params['breadcrumbs'][] = ['label' => 'Finance Offset Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-offset-memo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceOffsetMemoLines'=>$modelsFinanceOffsetMemoLines, //to enable Dynamic Form
    ]) ?>

</div>
