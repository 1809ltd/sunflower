<?php

namespace app\modules\event;

/**
 * event module definition class
 */
class event extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Event Set-up Sub Module*/
          'eventsetup' => [
                'class' => 'app\modules\event\eventsetup\eventsetup',
            ],
            /*Event Details*/
          'eventInfo' => [
              'class' => 'app\modules\event\eventInfo\eventInfo',
          ],
          /*Event Documents*/
          'eventdocs' => [
              'class' => 'app\modules\event\eventdocs\eventdocs',
          ],
          /*Transportation*/
          'transportation' => [
              'class' => 'app\modules\event\transportation\transportation',
          ],
          /*Workplan*/
          'workplan' => [
              'class' => 'app\modules\event\workplan\workplan',
          ],
        ];

        // custom initialization code goes here
    }
}
