<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\event\transportation\models\EventTransportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-transport-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'eventId') ?>

    <?= $form->field($model, 'outsourced') ?>

    <?= $form->field($model, 'assetId') ?>

    <?= $form->field($model, 'numberPlate') ?>

    <?php // echo $form->field($model, 'vendorId') ?>

    <?php // echo $form->field($model, 'delivery') ?>

    <?php // echo $form->field($model, 'collection') ?>

    <?php // echo $form->field($model, 'deliveryTime') ?>

    <?php // echo $form->field($model, 'collectionTime') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'approvedBy') ?>

    <?php // echo $form->field($model, 'createdAt') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
