<?php
use app\models\AllTransactions;

use yii\grid\GridView;
/*Asset*/
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
/*Event Details*/
use app\models\EventDetails;

/* @var $this yii\web\View */

$this->title = 'Asset Management Dashoard';
?>
<!-- Main content -->
<section class="content">
  <!-- Info boxes -->
  <!-- =========================================================== -->

  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Number of Asset</span>
          <span class="info-box-number">..</span>

          <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
              <span class="progress-description">
                70% Increase in 30 Days
              </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Feedback</span>
          <span class="info-box-number">5</span>

          <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
              <span class="progress-description">
                70% Increase in 30 Days
              </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Upcoming Events</span>
          <span class="info-box-number">

            <?php

            $leo= date('Y-m-d H:i:s');

            $upcoimingEvent=EventDetails::find()->where(['>=', 'eventStartDate', $leo])
                                                  ->count('event_details.id');
                                                  echo $upcoimingEvent;

             ?>

          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-red">
        <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">....</span>
          <span class="info-box-number">...</span>

          <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
              <span class="progress-description">
                70% Increase in 30 Days
              </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <!-- =========================================================== -->

  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <div class="col-md-8">
      <!-- MAP & BOX PANE -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Event Calender</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
          <div class="row">
            <div class="col-md-12 col-sm-8">
              <div class="pad">
                <!-- Event Will be displayed here -->
                  <!-- THE CALENDAR -->
                  <div>
                    <?php
                    // \yii2fullcalendar\yii2fullcalendar::widget(array(
                    //     'events'=> $events,
                    // ));
                   ?>
                  </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <div class="row">


      </div>
      <!-- /.row -->

      <!-- TABLE: Upcoming Events ORDERS -->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">Asset Feed</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="table-responsive">

              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Check out</a></li>
                  <li><a href="#timeline" data-toggle="tab">Check in</a></li>
                  <li><a href="#settings" data-toggle="tab">Repair</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <?= GridView::widget([
                        'dataProvider' => $modelCheckOut,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id',
                            // 'locationId',
                            // 'location.loaction.locationName',
                            // 'assetId',
                            'asset.assetName',
                            'asset.assetTag',
                            'checkOutDate',
                            'dueDate',
                            // 'activity',
                            // 'deliveryId',
                            'delivery.deliveryNumber',
                            // 'activityId',
                            'activity0.eventName',
                            // 'orderId',
                            'orderlist.item.itemsName',
                            // 'orderlistId',
                            // 'vehicle',
                            // 'vehicle0.numberPlate',
                            // 'assignto',
                            // 'assignto0.displayName',
                            // 'timestamp',
                            // 'updatedAt',
                            // 'deletedAt',
                            // 'status',
                            // 'athurizedBy',
                            // 'userId',
                            // 'createdBy',
                            // [
                            //     'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                            //     'items' => [
                            //         [
                            //             'label' => 'View',
                            //             'url'   => ['view'],
                            //         ],
                            //         [
                            //             'label' => 'Export',
                            //             'url'   => ['#'],
                            //         ],
                            //         [
                            //             'label'   => 'Disable',
                            //             'url'     => ['disable'],
                            //             'linkOptions' => [
                            //                 'data-method' => 'post'
                            //             ],
                            //         ],
                            //     ]
                            // ],
                        ],
                    ]); ?>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="timeline">
                    <?= GridView::widget([
                        'dataProvider' => $modelCheckin,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id',
                            // 'locationId',
                            // 'location.loaction.locationName',
                            // 'assetId',
                            'asset.assetName',
                            'asset.assetTag',
                            'checkOutDate',
                            'dueDate',
                            // 'activity',
                            // 'deliveryId',
                            'delivery.deliveryNumber',
                            // 'activityId',
                            'activity0.eventName',
                            // 'orderId',
                            'orderlist.item.itemsName',
                            // 'orderlistId',
                            // 'vehicle',
                            // 'vehicle0.numberPlate',
                            // 'assignto',
                            // 'assignto0.displayName',
                            // 'timestamp',
                            // 'updatedAt',
                            // 'deletedAt',
                            // 'status',
                            // 'athurizedBy',
                            // 'userId',
                            // 'createdBy',
                        ],
                    ]); ?>

                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <?= GridView::widget([
                        'dataProvider' => $modelCheckin,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            // 'id',
                            // 'locationId',
                            // 'location.loaction.locationName',
                            // 'assetId',
                            'asset.assetName',
                            'asset.assetTag',
                            'checkOutDate',
                            'dueDate',
                            // 'activity',
                            // 'deliveryId',
                            'delivery.deliveryNumber',
                            // 'activityId',
                            'activity0.eventName',
                            // 'orderId',
                            'orderlist.item.itemsName',
                            // 'orderlistId',
                            // 'vehicle',
                            // 'vehicle0.numberPlate',
                            // 'assignto',
                            // 'assignto0.displayName',
                            // 'timestamp',
                            // 'updatedAt',
                            // 'deletedAt',
                            // 'status',
                            // 'athurizedBy',
                            // 'userId',
                            // 'createdBy',
                        ],
                    ]); ?>


                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div>
              <!-- /.nav-tabs-custom -->

          </div>
          <!-- /.table-responsive -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
          <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-md-4">
      <!-- Info Boxes Style 2 -->
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Inventory On Hold</span>
          <span class="info-box-number"><?php
          $assetSataus=3;
          $holdAsset=AssetRegistration::find()->where(['>=', 'assetStatus', $assetSataus])
                                                ->count('asset_registration.id');

                                                echo $holdAsset;

          ?></span>

          <!-- <div class="progress">
            <div class="progress-bar" style="width: 50%"></div>
          </div>
          <span class="progress-description">
                50% Increase in 30 Days
              </span> -->
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Available Asset</span>
          <span class="info-box-number"><?php
          $assetSatausava=1;
          $availAsset=AssetRegistration::find()->where(['>=', 'assetStatus', $assetSatausava])
                                                ->count('asset_registration.id');
                                                echo $availAsset;

          ?></span>

          <!-- <div class="progress">
            <div class="progress-bar" style="width: 20%"></div>
          </div>
          <span class="progress-description">
                20% Increase in 30 Days
              </span> -->
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
      <div class="info-box bg-red">
        <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Under Repair</span>
          <span class="info-box-number"><?php
          $assetSatausunder=4;
          $underilAsset=AssetRegistration::find()->where(['>=', 'assetStatus', $assetSatausunder])
                                                ->count('asset_registration.id');

                                                echo $underilAsset;

          ?></span>

          <!-- <div class="progress">
            <div class="progress-bar" style="width: 70%"></div>
          </div>
          <span class="progress-description">
                70% Increase in 30 Days
              </span> -->
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
      <div class="info-box bg-aqua">
        <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">CheckOut</span>
          <span class="info-box-number"><?php
          $assetSatausout=2;
          $outilAsset=AssetRegistration::find()->where(['>=', 'assetStatus', $assetSatausout])
                                                ->count('asset_registration.id');

                                                echo $outilAsset;

          ?></span>

          <!-- <div class="progress">
            <div class="progress-bar" style="width: 40%"></div>
          </div>
          <span class="progress-description">
                40% Increase in 30 Days
              </span> -->
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->

      <!-- PRODUCT LIST -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Recently Added Asset</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <ul class="products-list product-list-in-box">



              <li class="item">
                <div class="product-img">
                  <img src="logo/rsz_sunflowerlogo.png" alt="Product Image">
                </div>

              </li>
          </ul>
        </div>
        <!-- /.box-body -->
        <div class="box-footer text-center">
          <a href="javascript:void(0)" class="uppercase">View All Asset</a>
        </div>
        <!-- /.box-footer -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->




<!--
<div class="inventory-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
