<?php

namespace app\modules\finance\expense\officeexpense\models;

/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\finance\expense\officeexpense\officeexpenselines\models\FinanceOfficeRequstionFormList;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;
use app\modules\personnel\personneldetails\models\PersonnelDetails;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;

use Yii;

/**
 * This is the model class for table "finance_office_requstion_form".
 *
 * @property int $id
 * @property string $requstionNumber
 * @property string $date
 * @property int $requstionStatus
 * @property string $requstionNote
 * @property double $amount
 * @property int $preparedBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $approvedBy
 *
 * @property FinanceOfficeRequisitionPaymentsLines[] $financeOfficeRequisitionPaymentsLines
 * @property UserDetails $user
 * @property UserDetails $approvedBy0
 * @property FinanceOfficeRequstionFormList[] $financeOfficeRequstionFormLists
 */
class FinanceOfficeRequstionForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_office_requstion_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requstionNumber', 'date', 'requstionStatus', 'amount', 'preparedBy', 'userId'], 'required'],
            [['date', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['requstionStatus', 'preparedBy', 'userId', 'approvedBy'], 'integer'],
            [['requstionNote'], 'string'],
            [['amount'], 'number'],
            [['requstionNumber'], 'string', 'max' => 100],
            [['requstionNumber'], 'unique'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requstionNumber' => 'Requstion Number',
            'date' => 'Date',
            'requstionStatus' => 'Requstion Status',
            'requstionNote' => 'Requstion Note',
            'amount' => 'Amount',
            'preparedBy' => 'Prepared By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'approvedBy' => 'Approved By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequisitionPaymentsLines()
    {
        return $this->hasMany(FinanceOfficeRequisitionPaymentsLines::className(), ['officeReqId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequstionFormLists()
    {
        return $this->hasMany(FinanceOfficeRequstionFormList::className(), ['reqId' => 'id']);
    }
    // Get all the invoice amount
    public function getFinanceOfficeRequstionFormListItems($id,$status)
    {
      return FinanceOfficeRequstionFormList::find()
              ->where('reqId = :reqId', [':reqId' => $id])
              ->Andwhere('status = :status', [':status' => $status])
              ->all();
    }
    public function getRQnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(requstionNumber) AS number from finance_office_requstion_form where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "ORQEV-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "ORQEV-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "ORQEV-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }

      public function geteventDetailss($id)
      {
        return EventDetails::find()
          ->where(['and', "id=$id"])
          ->all();
      }

      public function getActivity($id)
      {
        return FinanceEventActivity::find()
          ->where(['and', "id=$id"])
          ->all();
      }
      // All Payments
      public function allPayments($id){
        // Get the actual Payments
        $officepayment= $this->find()->joinWith('financeOfficeRequisitionPaymentsLines')
                             ->where(['finance_office_requisition_payments_lines.officeReqId' => $id])
                             ->Andwhere('finance_office_requisition_payments_lines.status >0')
                             ->sum('finance_office_requisition_payments_lines.amount');

       return $officepayment;
      }

}
