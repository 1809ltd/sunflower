<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;

use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;

/*Getting the product and service offered, customer*/

use app\models\FinanceBills;
use app\models\VendorCompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemo */
/* @var $form yii\widgets\ActiveForm */

?>
<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="panel-body finance-vendor-credit-memo-form">

                  <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>


                  <div class="col-sm-3">
                    <div class="form-group">

                      <?php $customer = ArrayHelper::map(VendorCompanyDetails::find()->all(),'id','vendorCompanyName'); ?>
                      <?= $form->field($model, 'vendorId')->widget(Select2::classname(), [
                          'data' => $customer,
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Customer ...',
                                        'onChange'=>'$.post("index.php?r=finance-invoice/lists&customerId='.'"+$(this).val(),function(data){
                                          $("select#billRef").html(data);
                                        });'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'refernumber')->textInput()?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'vendorCreditMemoAccount')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(FinanceAccounts::find()->all(),'id','fullyQualifiedName'),
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Account ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'vendorCreditMemoTxnDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

                    </div>
                  </div>



                  <div class="col-sm-3">
                    <div class="form-group">


                    </div>
                  </div>

                  <div class="panel-body"></div>

                  <div class="panel-body">

                    <div class="panel panel-pvr panel--style--1">
                      <div class="panel-heading">
                          <div class="panel-heading-btn">
                              <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                              <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                              <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                              <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                          </div>
                          <h4 class="panel-title">Add Credit Memo Item</h4>
                      </div>
                      <div class="panel-body">
                           <?php DynamicFormWidget::begin([
                              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                              'widgetBody' => '.container-items', // required: css class selector
                              'widgetItem' => '.item', // required: css class
                              'limit' => 10, // the maximum times, an element can be cloned (default 999)
                              'min' => 1, // 0 or 1 (default 1)
                              'insertButton' => '.add-item', // css class
                              'deleteButton' => '.remove-item', // css class
                              'model' => $modelsFinanceVendorCreditMemoLines[0],
                              'formId' => 'dynamic-form',
                              'formFields' => [

                                'billRef',
                                'vendorCreditMemoLineDescription',
                                'vendorCreditMemoLineAmount',
                                //'vendorCreditMemoLineStatus',

                              ],
                          ]); ?>

                          <div class="container-items"><!-- widgetContainer -->
                            <!-- Loopping Items in the Lists -->
                          <?php foreach ($modelsFinanceVendorCreditMemoLines as $i => $modelFinanceVendorCreditMemoLines): ?>
                              <div class="item panel-pvr panel--style--2 "><!-- widgetBody -->


                                  <div class="panel-heading">
                                      <h3 class="panel-title pull-left">Credit Memo Item</h3>
                                      <div class="pull-right">
                                          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>


                                  <div class="panel-body">
                                      <?php
                                          // necessary for update action.
                                          if (! $modelFinanceVendorCreditMemoLines->isNewRecord) {
                                              echo Html::activeHiddenInput($modelFinanceVendorCreditMemoLines, "[{$i}]id");
                                          }
                                      ?>
                                      <div class="col-sm-4">
                                        <div class="form-group">

                                          <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]billRef")->widget(Select2::classname(), [
                                              'data' => ArrayHelper::map(FinanceBills::find()->all(),'id','billRef'),
                                              'language' => 'en',
                                              'class' => 'billRef form-control',
                                              'options' => ['placeholder' => 'Select a Invoice ...'],
                                              'pluginOptions' => [
                                                  'allowClear' => true
                                              ],
                                          ]); ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]vendorCreditMemoLineDescription")->textarea(['rows' => 6]) ?>
                                        </div>
                                      </div>

                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceVendorCreditMemoLines, "[{$i}]vendorCreditMemoLineAmount")->textInput([
                                          'maxlength' => true,
                                          'class' => 'sumTaxPart form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>

                                  </div>
                              </div>
                          <?php endforeach; ?>
                          </div>
                          <?php DynamicFormWidget::end(); ?>
                      </div>
                  </div>

                  </div>

                    <div class="panel-body"></div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($model, 'vendorCreditMemoNote')->textarea(['rows' => 6]) ?>

                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <?= $form->field($model, 'creditAmount')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
                      </div>
                    </div>

                    <div class="col-sm-3">
                      <div class="form-group">
                        <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                        <?= $form->field($model, 'vendorCreditMemoStatus')->dropDownList($status, ['prompt'=>'Select Status']);?>
                      </div>
                    </div>


                    <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>


                    <div class="panel-body"></div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= Html::submitButton($modelFinanceVendorCreditMemoLines->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                    </div>
                  </div>

                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->

<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var tax=0

        items.each(function (index, elem) {

            //getting specific elements

            //getting the tax amount figures
            var taxValue = $(elem).find(".sumTaxPart").val();

            //getting total tax amountValue
            tax = parseFloat(tax) + parseFloat(taxValue);

            //assing value to the total tax amount
            $(".tax").val(tax);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>




<?php

/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-vendor-credit-memo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vendorCreditMemoAccount')->textInput() ?>

    <?= $form->field($model, 'refernumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendorCreditMemoTxnDate')->textInput() ?>

    <?= $form->field($model, 'vendorCreditMemoNote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'vendorId')->textInput() ?>

    <?= $form->field($model, 'creditAmount')->textInput() ?>

    <?= $form->field($model, 'vendorCreditMemoStatus')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
