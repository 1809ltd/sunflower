"use strict";
var ass = function () {
    "use strict";
    $(document).ready(function () {
        var table = "";

		table = $('#table').DataTable({
			"ajax": {
            "url": "assets/php_data/top_customers.txt",
				"dataSrc": ""
			},
			"fnDrawCallback": function () {

			},
			"columns": [
				{"data": null},
				{"data": "uniqueid"},
				{"data": "name"},
				{"data": "primary_mobile"},
				{"data": "last_week_usage"},
				{"data": "last_month_usage"},
				{"data": "total_usage"},
			],
			"order": [[0, 'asc']]
		});

		table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
};
var Assign = function () {
    "use strict";
    return {
        init: function () {
            ass();
        }
    }
}();
$(function () {
    Assign.init();
});