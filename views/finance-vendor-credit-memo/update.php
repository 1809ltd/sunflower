<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemo */

$this->title = 'Update Finance Vendor Credit Memo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Credit Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-vendor-credit-memo-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceVendorCreditMemoLines'=>$modelsFinanceVendorCreditMemoLines, //to enable Dynamic Form
    ]) ?>

</div>
