<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use app\models\VendorCompanyDetails;
use app\models\AssetRegistration;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\VendorCompanyDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Supplier</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="vendor-company-details-form">
                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanyName')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorKraPin')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorContactPersonFullName')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'contactMobilePhone')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorPhone')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendoremail')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'contactEmail')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanyAddress')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanySuite')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanyCity')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanyCounty')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanyPostal')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanyCountry')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorCompanyLogo')->fileInput() ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorTwitter')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorFb')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'vendorIg')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $vendorType = ['Strick' => 'Strick', 'Open' => 'Open']; ?>
                    <?= $form->field($model, 'vendorType')->dropDownList($vendorType, ['prompt'=>'Select Status']); ?>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'vendorCompanyStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                  </div>
                </div>

                <?= $form->field($model, 'companyUserId')->hiddenInput(['value'=> "1"])->label(false);?>

                <div class="col-sm-3">
                  <div class="form-group">
                      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                  </div>
                </div>


                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
