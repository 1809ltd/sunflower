<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
// use yii\helpers\Url;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;


/*Get All the event order and details*/
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
use app\modules\event\eventdocs\orderfroms\models\EventOrderfromSearch;

/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfrom */

// use app\models\EventOrderfromList;
// use app\models\EventOrderfromListSearch;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfrom */

$this->title = $model->orderNumber;
$this->params['breadcrumbs'][] = ['label' => 'Event Checklist', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();

/*All Payment for this invoice, i.e offset,creditnote,actualpayment*/
// $payments=$model->allPayments($model->invoiceNumber);

?>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <p>
      <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
  </p>
  <!-- Pop up Form -->
  <?php
  // Modal::begin([
  //   // 'header'=>'<h3>Add Customer</h3>',
  //   'class'=>'modal-dialog',
  //   'id'=>'modal',
  //   'size'=>'modal-lg',
  // ]);
  //
  // echo "<div class='modal-content' id='modalContent'></div>";
  //
  // Modal::end();
   ?>
</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-sm-8">
         <!-- <h3 class="page-header"> -->
         <h5>
           <img class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h5>
       </div>
       <div class="col-sm-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-sm-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-sm-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Checklist</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">

         <address>
           <strong>Client:- </strong> <?= $model->event['customer']['customerFullname']?><br/>

           <strong> No of guests:-</strong> <?= $model->event['eventVistorsNumber']?> Pax<br/>
             <strong>Date of Function:- </strong> <?= $model->event['eventStartDate']?><br/>
             <strong>Date of Set Up:- </strong> <?= $model->event['eventSetUpDateTime']?><br/>
             <strong>Colour Scheme:- </strong> <?= $model->event['eventTheme']?><br/>
             <strong>Event:- </strong> <?= $model->event['eventName']?> <br/>
             <strong>Location:- </strong> <?= $model->event['eventLocation']?><br/>
         </address>

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Order No: #<?=$model->orderNumber;?></b><br>
         <b>Date:- </b> <?= Yii::$app->formatter->asDate($model->date, 'long');?><br>

         <b>Account:</b> <?= $model->event['customer']['customerNumber']?>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-sm-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <tr  class="row">

               <th class="col-sm-3">Requirements</th>
               <th class="col-sm-7">Details</th>
               <th class="col-sm-2">Size/ Quantity</th>
             </tr>

           </thead>
           <tbody>
             <?php
             //Getting all the Event Order Items
             $orderStatus=$model->orderStatus;
             $id=$model->id;

             $eventOrderfromLists=$model->geteventOrderfromList($id,$orderStatus);

             // print_r($eventOrderfromLists);
             // die();

             foreach ($eventOrderfromLists as $eventOrderfromList) {
               // code...

               ?>
               <tr class="row">
                   <td class="col-sm-3"><?=$eventOrderfromList->item['itemCategory0']['financeItemCategoryName'];?>: <?=$eventOrderfromList->item['itemCategoryType0']['financeItemCategoryTypeName'];?></td>
                   <td class="col-sm-7">
                     <?=$eventOrderfromList->item['itemsName'];?>
                     <ul>
                     <?php
                     //Getting the Asset Required List
                     $eventAssetrequireds=$model->getAssetrequired($eventOrderfromList->itemid);
                     foreach ($eventAssetrequireds as $eventAssetrequired) {
                       // code...
                       ?>
                       <li><?= number_format(($eventOrderfromList->quantity*$eventAssetrequired->qty))?>  <?= $eventAssetrequired->categorytype['assetCategoryTypeName']?></li>
                       <?php

                     }
                     ?>
                     </ul>

                     <ul>
                     <?php
                     ///*Getting the Role REquired for the Event*/
                     $eventRolerequireds=$model->getRolerequired($eventOrderfromList->itemid);
                     foreach ($eventRolerequireds as $eventRolerequired) {
                       // code...
                       ?>
                       <li><?= number_format(($eventOrderfromList->quantity*$eventRolerequired->qty))?>  <?= $eventRolerequired->role['roleName']?></li>
                       <?php

                     }
                     ?>
                     </ul>
                   </td>
                   <td class="col-sm-2"><?= number_format($eventOrderfromList->quantity,2) ;?></td>
               </tr>

               <?php

             }

              ?>


           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-sm-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           <?= $model->orderNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-sm-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          <?= $model->orderNote; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-sm-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-sm btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->
         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
         <?php
        //  Html::a('<i class="fa fa-download"></i>Generate PDF', ['generatepdf', 'id' => $model['id']], [
        //     'class'=>'btn btn-primary',
        //     'target'=>'_blank',
        //     'data-toggle'=>'tooltip',
        //     'title'=>'Will open the generated PDF file in a new window'
        // ]);?>
       </div>
     </div>
   </section>
   <!-- /.content -->
