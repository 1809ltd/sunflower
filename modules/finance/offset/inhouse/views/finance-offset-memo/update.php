<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOffsetMemo */

$this->title = 'Update Finance Offset Memo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Offset Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-offset-memo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceOffsetMemoLines'=>$modelsFinanceOffsetMemoLines, //to enable Dynamic Form
    ]) ?>

</div>
