<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\customer\customerdetails\models\CustomerDetails */

$this->title = 'Update Customer Details: ' . $model->customerNumber;
$this->params['breadcrumbs'][] = ['label' => 'Customer Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->customerNumber, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="customer-details-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
