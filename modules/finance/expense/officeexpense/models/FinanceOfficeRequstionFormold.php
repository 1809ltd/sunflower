<?php

namespace app\modules\finance\expense\officeexpense\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\finance\expense\officeexpense\officeexpenselines\models\FinanceOfficeRequstionFormList;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;
use app\modules\personnel\personneldetails\models\PersonnelDetails;


use Yii;

/**
 * This is the model class for table "finance_office_requstion_form".
 *
 * @property int $id
 * @property string $requstionNumber
 * @property string $date
 * @property int $requstionStatus
 * @property string $requstionNote
 * @property double $amount
 * @property int $preparedBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $approvedBy
 *
 * @property FinanceOfficeRequisitionPayments[] $financeOfficeRequisitionPayments
 * @property UserDetails $user
 * @property UserDetails $approvedBy0
 * @property FinanceOfficeRequstionFormList[] $financeOfficeRequstionFormLists
 */
class FinanceOfficeRequstionForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_office_requstion_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requstionNumber', 'date', 'requstionStatus', 'amount', 'preparedBy', 'userId'], 'required'],
            [['date', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['requstionStatus', 'preparedBy', 'userId', 'approvedBy'], 'integer'],
            [['requstionNote'], 'string'],
            [['amount'], 'number'],
            [['requstionNumber'], 'string', 'max' => 100],
            [['requstionNumber'], 'unique'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requstionNumber' => 'Requstion Number',
            'date' => 'Date',
            'requstionStatus' => 'Requstion Status',
            'requstionNote' => 'Requstion Note',
            'amount' => 'Amount',
            'preparedBy' => 'Prepared By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'approvedBy' => 'Approved By',
        ];
    }

    public function getRQnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(requstionNumber) AS number from finance_office_requstion_form where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "ORQEV-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "ORQEV-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "ORQEV-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }

      public function geteventDetailss($id)
      {
        return EventDetails::find()
          ->where(['and', "id=$id"])
          ->all();
      }

      public function getFinanceOfficeRequstionFormList($id)
      {
        return FinanceOfficeRequstionFormList::find()
          ->where(['and', "reqId=$id"])
          ->all();
      }
      public function getActivity($id)
      {
        return FinanceEventActivity::find()
          ->where(['and', "id=$id"])
          ->all();
      }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequisitionPayments()
    {
        return $this->hasMany(FinanceOfficeRequisitionPayments::className(), ['reqpaymentLinkedTxn' => 'requstionNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequstionFormLists()
    {
        return $this->hasMany(FinanceOfficeRequstionFormList::className(), ['reqId' => 'id']);
    }
    public function allPayments($id){
      // Get the actual Payments
      $officepayment= $this->find()->joinWith('financePaymentLines')
                           ->where(['finance_payment_lines.invoiceId' => $id])
                           ->Andwhere('finance_payment_lines.status >0')
                           ->sum('finance_payment_lines.amount');

      // Get all the Offest cancel payments
     //  $offsetPayments=$this->find()->joinWith('financeOffsetMemoLines')
     //                       ->where(['finance_offset_memo_lines.invoiceNumber' => $id])
     //                       ->Andwhere('finance_offset_memo_lines.offsetMemoLineStatus >0')
     //                       ->sum('offsetMemoLineAmount');
     //
     //  // Get all the Credit Payments
     //  $creditPayments=$this->find()->joinWith('financeCreditMemoLines')
     //                       ->where(['finance_credit_memo_lines.invoiceNumber' => $id])
     //                       ->Andwhere('finance_credit_memo_lines.creditMemoLineStatus >0')
     //                       ->sum('creditMemoLineAmount');
     //
     // // This is the total payments of all the amount
     // $totalpaymentmade=$inoicepayment+$offsetPayments+$creditPayments;

     return $officepayment;
    }
}
