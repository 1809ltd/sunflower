<?php

namespace app\modules\vendor\vendordetails;

/**
 * vendordetails module definition class
 */
class vendordetails extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\vendor\vendordetails\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
