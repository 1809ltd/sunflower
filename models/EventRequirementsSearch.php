<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventRequirements;

/**
 * EventRequirementsSearch represents the model behind the search form of `app\models\EventRequirements`.
 */
class EventRequirementsSearch extends EventRequirements
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'eventId', 'department', 'preparedBy', 'approvedBy', 'requirementStatus'], 'integer'],
            [['eventNumber', 'departmentName', 'requirementDetails', 'requirementSIUnit', 'requirementCreatedAt', 'requirementLastUpdated', 'requirementDeletedAt'], 'safe'],
            [['requirementSize'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventRequirements::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'eventId' => $this->eventId,
            'department' => $this->department,
            'requirementSize' => $this->requirementSize,
            'requirementCreatedAt' => $this->requirementCreatedAt,
            'requirementLastUpdated' => $this->requirementLastUpdated,
            'requirementDeletedAt' => $this->requirementDeletedAt,
            'preparedBy' => $this->preparedBy,
            'approvedBy' => $this->approvedBy,
            'requirementStatus' => $this->requirementStatus,
        ]);

        $query->andFilterWhere(['like', 'eventNumber', $this->eventNumber])
            ->andFilterWhere(['like', 'departmentName', $this->departmentName])
            ->andFilterWhere(['like', 'requirementDetails', $this->requirementDetails])
            ->andFilterWhere(['like', 'requirementSIUnit', $this->requirementSIUnit]);

        return $dataProvider;
    }
}
