<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_item_role".
 *
 * @property int $id
 * @property int $itemId
 * @property int $roleId
 * @property double $qty
 * @property int $status
 * @property int $userid
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property FinanceItems $item
 * @property EventRole $role
 * @property EventWorkplan[] $eventWorkplans
 */
class EventItemRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_item_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemId', 'roleId', 'qty', 'status', 'userid'], 'required'],
            [['itemId', 'roleId', 'status', 'userid'], 'integer'],
            [['qty'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['itemId', 'roleId'], 'unique', 'targetAttribute' => ['itemId', 'roleId']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['roleId'], 'exist', 'skipOnError' => true, 'targetClass' => EventRole::className(), 'targetAttribute' => ['roleId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'itemId' => 'Service',
            'roleId' => 'Role',
            'qty' => 'Qty',
            'status' => 'Status',
            'userid' => 'Userid',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(EventRole::className(), ['id' => 'roleId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventWorkplans()
    {
        return $this->hasMany(EventWorkplan::className(), ['taskAssign' => 'id']);
    }
}
