"use strict";

var allMarkers = [];
var map, marker, markerCluster, infowindow, bounds;
var styles = [[{
    url: 'assets/img/culster.png',
    height: 98,
    width: 106,
    anchor: [35, 37],
    textColor: '#ff00ff',
    textSize: 11
}]];

var full_screen = function () {
    "use strict";
    var a = [ {
        featureType: "all",
        elementType: "all",
      /*  stylers    : [ {invert_lightness: false}, {saturation: 15}, {lightness: 15}, {gamma: .8}, {hue: "#4e54c8"} ] */
    }, {featureType: "water", /* stylers: [ {visibility: "on"}, {color: "#293036"} ] */ } ];

    function t() {
        var t = {
            zoom            : 13,
            center          : new google.maps.LatLng(11.0136, 76.957164),
            mapTypeId       : google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        e = new google.maps.Map(document.getElementById("google-map"), t);
        e.setOptions({styles: a});
		map = e;
		
		loadMap(map, vehicleList);
    }

    var e;
    google.maps.event.addDomListener(window, "load", t);
    $(window).on("resize", function () {
        google.maps.event.trigger(e, "resize")
    });
	
	bounds = new google.maps.LatLngBounds();
    infowindow = new google.maps.InfoWindow({maxWidth: 200});

};

var icons = {
	taxi: {
		path: MAP_PIN,
		fillColor: '#348fe2',
		fillOpacity: 1,
		strokeColor: '',
		strokeWeight: 0
	},
	start: {
		path: ROUTE,
		fillColor: '#1998F7',
		fillOpacity: 1,
		strokeColor: '',
		strokeWeight: 0
	},
	end: {
		path: ROUTE,
		fillColor: '#6331AE',
		fillOpacity: 1,
		strokeColor: '',
		strokeWeight: 0
	}
};

var loadMap = function(map, locations){
	var i, latlng, markPos, icon;
	for (i in locations) {
        var color_array = {
            "free": "#00DEAF",
            "assigned": "#CD11EE",
            "allotted": "#2B1AAB",
            "running": "#9999FF",
            "break_down": "#BE394F",
            "break": "#F0AD4E",
            "leave": "#E0690A"
        };

        icons.taxi.fillColor = color_array[locations[i]["running_status"]];
        markPos = new google.maps.LatLng(locations[i]['current_lat'], locations[i]['current_lng']);
        marker = makeMarker(markPos, icons.taxi, locations[i]["vehicle_no"], '<span class="map-icon m-b-38"><i class="fa fa-taxi f-s-16" aria-hidden="true"></i></span>');

		// console.log(marker);
		
        var geocoder = new google.maps.Geocoder();
        var LocationAddress;

        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
            return function () {
                // Get the address of marker starts
                latlng = new google.maps.LatLng(locations[i]['current_lat'], locations[i]["current_lng"]);
                geocoder.geocode({
                    "latLng": latlng
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var placeName = results[0].address_components[0].long_name;
                        LocationAddress = results[0].formatted_address;

                        var content = LocationAddress.substring(0, 30) + "...<br /><b> Vehicle No: </b>" + locations[i]["vehicle_no"] + " <br><span class='hide'><b> Model: </b> " + locations[i]["model"] + "<br/><b> Seat Capacity: </b>" + locations[i]["seat_capacity"] + "<br/></span><b> Status: </b>" + show_as_label(locations[i]["running_status"]);

                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    }
                });
                google.maps.event.addListener(map, 'click', function () {
                    infowindow.close();
                });

                // Get the address of marker ends
            }
        })(marker, i));

        function show_as_label(text) {
            text = text.replace("_", " ");
            var opt = text.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                return letter.toUpperCase();
            });

            return opt;
        }

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                if ($("#allot_now_div").is(":visible") == true) {
                    var veh_id = locations[i]["vehicle_id"];
                    pick_veh_forAllot("marker", veh_id);
                }
            }
        })(marker, i));
        allMarkers.push(marker);
    }

    // >>>>> Fit all markers within the visible map area
    for (var i = 0; i < allMarkers.length; i++) {
        bounds.extend(allMarkers[i].getPosition());
    }
    // map.fitBounds(bounds);
    if (allMarkers.length == 1) {
        map.setCenter(allMarkers[0].getPosition());
        map.setZoom(15);
    }
    // console.log(allMarkers);
    // <<<<< Fit all markers within the visible map area
	
    markerCluster = new MarkerClusterer(map, allMarkers, {
        gridSize: 50,
        styles: styles[0]
    });
}

function makeMarker(position, icon, vid, icon_label) {
    marker = new Marker({
        position: position,
        id: vid,
        icon: icon,
        map: map,
        //animation: google.maps.Animation.DROP,
        draggable: false,
        map_icon_label: icon_label
    });
    return marker;
}

var map_init = function () {
    "use strict";
    return {
        init: function () {
            full_screen();
        }
    }
}();
$(function () {
    map_init.init();
});