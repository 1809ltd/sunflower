<?php

namespace app\modules\finance\financereport\companyfinancials\models;

use Yii;

/**
 * This is the model class for table "all_transactions".
 *
 * @property int $transactionId
 * @property int $referenceId
 * @property string $referenceCode
 * @property string $transactionCode
 * @property string $projectId
 * @property string $recipientId
 * @property string $recipientName
 * @property int $accountParentId
 * @property string $accountsclassfication
 * @property int $accountId
 * @property string $accountName
 * @property string $transactionName
 * @property string $tranctionDescription
 * @property string $dr_amount
 * @property string $cr_amount
 * @property string $transactionDate
 * @property string $createdAt
 * @property int $status
 * @property string $transactionCategory
 * @property string $transactionClassification
 * @property string $transactionTable
 * @property string $referenceTable
 */
class AllTransactions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

     /*Search Paramenters*/
     public $startDate;
     public $endDate;
     public $mwishoDate;
     public $dr_sum;
     public $balance_amount;


    public static function tableName()
    {
        return 'all_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transactionId', 'referenceId', 'accountParentId', 'accountId', 'status'], 'integer'],
            [['transactionName', 'tranctionDescription'], 'string'],
            [['transactionDate', 'createdAt','startDate','endDate','mwishoDate'], 'safe'],
            [['referenceCode', 'transactionCode'], 'string', 'max' => 100],
            [['projectId'], 'string', 'max' => 50],
            [['recipientId'], 'string', 'max' => 11],
            [['recipientName', 'accountsclassfication', 'accountName'], 'string', 'max' => 255],
            [['dr_amount', 'cr_amount'], 'string', 'max' => 22],
            [['transactionCategory'], 'string', 'max' => 33],
            [['transactionClassification'], 'string', 'max' => 31],
            [['transactionTable'], 'string', 'max' => 35],
            [['referenceTable'], 'string', 'max' => 29],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'mwishoDate'=> 'As of',
            'transactionId' => 'Transaction ID',
            'referenceId' => 'Reference ID',
            'referenceCode' => 'Reference Code',
            'transactionCode' => 'Transaction Code',
            'projectId' => 'Project ID',
            'recipientId' => 'Recipient ID',
            'recipientName' => 'Recipient Name',
            'accountParentId' => 'Account Parent ID',
            'accountsclassfication' => 'Accountsclassfication',
            'accountId' => 'Account ID',
            'accountName' => 'Account Name',
            'transactionName' => 'Transaction Name',
            'tranctionDescription' => 'Tranction Description',
            'dr_amount' => 'Dr Amount',
            'cr_amount' => 'Cr Amount',
            'transactionDate' => 'Transaction Date',
            'createdAt' => 'Created At',
            'status' => 'Status',
            'transactionCategory' => 'Transaction Category',
            'transactionClassification' => 'Transaction Classification',
            'transactionTable' => 'Transaction Table',
            'referenceTable' => 'Reference Table',
        ];
    }
}
