<?php

namespace app\modules\finance\expensepayment\billpayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;

/**
 * FinanceBillsPaymentsLinesSearch represents the model behind the search form of `app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines`.
 */
class FinanceBillsPaymentsLinesSearch extends FinanceBillsPaymentsLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'billpaymentId', 'billId', 'vendorId', 'status', 'userId'], 'integer'],
            [['referenceNo', 'deletedAt', 'created_at', 'updated_at'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBillsPaymentsLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'billpaymentId' => $this->billpaymentId,
            'billId' => $this->billId,
            'vendorId' => $this->vendorId,
            'amount' => $this->amount,
            'status' => $this->status,
            'deletedAt' => $this->deletedAt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'referenceNo', $this->referenceNo]);

        return $dataProvider;
    }
}
