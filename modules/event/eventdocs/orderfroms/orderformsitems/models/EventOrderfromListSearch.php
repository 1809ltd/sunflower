<?php

namespace app\modules\event\eventdocs\orderfroms\orderformsitems\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromList;

/**
 * EventOrderfromListSearch represents the model behind the search form of `app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromList`.
 */
class EventOrderfromListSearch extends EventOrderfromList
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'orderId', 'itemid', 'outsourced', 'vendorId', 'status', 'userId', 'approvedBy'], 'integer'],
            [['details', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['quantity'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventOrderfromList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'orderId' => $this->orderId,
            'itemid' => $this->itemid,
            'outsourced' => $this->outsourced,
            'vendorId' => $this->vendorId,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'approvedBy' => $this->approvedBy,
        ]);

        $query->andFilterWhere(['like', 'details', $this->details]);

        return $dataProvider;
    }
}
