<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemoLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-vendor-credit-memo-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vendorId') ?>

    <?= $form->field($model, 'vendorCreditMemoId') ?>

    <?= $form->field($model, 'invoiceNumber') ?>

    <?= $form->field($model, 'vendorCreditMemoLineDescription') ?>

    <?php // echo $form->field($model, 'vendorCreditMemoLineAmount') ?>

    <?php // echo $form->field($model, 'vendorCreditMemoLineStatus') ?>

    <?php // echo $form->field($model, 'vendorCreditMemoLineCreatedAt') ?>

    <?php // echo $form->field($model, 'vendorCreditMemoLineUpdatedAt') ?>

    <?php // echo $form->field($model, 'creidtDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
