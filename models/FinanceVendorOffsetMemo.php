<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_vendor_offset_memo".
 *
 * @property int $id
 * @property int $vendorOffsetMemoAccount
 * @property string $refernumber
 * @property string $vendorOffsetMemoTxnDate
 * @property string $vendorOffsetMemoNote
 * @property int $vendorId
 * @property double $offsetAmount
 * @property int $vendorOffsetMemoStatus
 * @property string $vendorOffsetMemoCreatedAt
 * @property string $vendorOffsetMemoUpdatedAt
 * @property string $vendorOffsetMemoDeletedAt
 * @property int $userId
 *
 * @property VendorCompanyDetails $vendor
 * @property FinanceAccounts $vendorOffsetMemoAccount0
 * @property FinanceVendorOffsetMemoLines[] $financeVendorOffsetMemoLines
 */
class FinanceVendorOffsetMemo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_vendor_offset_memo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorOffsetMemoAccount', 'refernumber', 'vendorOffsetMemoTxnDate', 'vendorOffsetMemoNote', 'vendorId', 'offsetAmount', 'vendorOffsetMemoStatus', 'userId'], 'required'],
            [['vendorOffsetMemoAccount', 'vendorId', 'vendorOffsetMemoStatus', 'userId'], 'integer'],
            [['vendorOffsetMemoTxnDate', 'vendorOffsetMemoCreatedAt', 'vendorOffsetMemoUpdatedAt', 'vendorOffsetMemoDeletedAt'], 'safe'],
            [['vendorOffsetMemoNote'], 'string'],
            [['offsetAmount'], 'number'],
            [['refernumber'], 'string', 'max' => 50],
            [['refernumber', 'vendorId'], 'unique', 'targetAttribute' => ['refernumber', 'vendorId']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['vendorOffsetMemoAccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['vendorOffsetMemoAccount' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorOffsetMemoAccount' => 'Vendor Offset Memo Account',
            'refernumber' => 'Refernumber',
            'vendorOffsetMemoTxnDate' => 'Vendor Offset Memo Txn Date',
            'vendorOffsetMemoNote' => 'Vendor Offset Memo Note',
            'vendorId' => 'Vendor ID',
            'offsetAmount' => 'Offset Amount',
            'vendorOffsetMemoStatus' => 'Vendor Offset Memo Status',
            'vendorOffsetMemoCreatedAt' => 'Vendor Offset Memo Created At',
            'vendorOffsetMemoUpdatedAt' => 'Vendor Offset Memo Updated At',
            'vendorOffsetMemoDeletedAt' => 'Vendor Offset Memo Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorOffsetMemoAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'vendorOffsetMemoAccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorOffsetMemoLines()
    {
        return $this->hasMany(FinanceVendorOffsetMemoLines::className(), ['vendorOffsetMemoId' => 'id']);
    }
}
