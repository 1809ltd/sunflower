<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments */

$this->title = Yii::t('app', 'Create Finance Requisition Payments');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Requisition Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-requisition-payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
