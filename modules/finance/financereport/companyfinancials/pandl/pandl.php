<?php

namespace app\modules\finance\financereport\companyfinancials\pandl;

/**
 * pandl module definition class
 */
class pandl extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\companyfinancials\pandl\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
