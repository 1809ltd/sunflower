"use strict";
var initz = function () {
    "use strict";
    $(document).ready(function () {
        var table = "";
        table = $('.table').DataTable({
            "scrollX"     : true,
        });
    });
};
var table = function () {
    "use strict";
    return {
        init: function () {
            initz();
        }
    }
}();
$(function () {
    table.init();
});