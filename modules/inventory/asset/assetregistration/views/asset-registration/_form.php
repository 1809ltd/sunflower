<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\bootstrap4\Modal;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;

use app\modules\inventory\assetsetup\assetcategory\models\AssetCategory;
use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType;
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;


/* @var $this yii\web\View */
/* @var $model app\models\AssetRegistration */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Department</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="asset-registration-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'assetName')->textInput(['maxlength' => true]) ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'assetDescription')->textarea(['rows' => 6]) ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'assetTag')->textInput(['maxlength' => true]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">

                    <?php $assetCategory=ArrayHelper::map(AssetCategory::find()->where('`assetCategoryStatus` = 1')->asArray()->all(), 'id', 'assetCategoryName');?>
                    <?= $form->field($model, 'assetCategory')->dropDownList($assetCategory, [
                      'prompt'=>'Select...',
                      'onChange'=>'$.post("index.php?r=asset-category-type/lists&id='.'"+$(this).val(),function(data){
                        $("select#assetregistration-assetcategorytype").html(data);
                      });'
                    ]); ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">

                    <?php $assetCategoryType = ArrayHelper::map(AssetCategoryType::find()->all(),'id','assetCategoryTypeName'); ?>
                    <?= $form->field($model, 'assetCategoryType')->dropDownList($assetCategoryType, ['prompt'=>'Select...']); ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'assetpurchaseDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ]) ?>

                  </div>

                </div>

                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'vendor')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(VendorCompanyDetails::find()->all(),'id','vendorCompanyName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Vendor ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'assetCost')->textInput() ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                      <?= $form->field($model, 'assetBrand')->textInput(['maxlength' => true]) ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                        <?= $form->field($model, 'assetModel')->textInput(['maxlength' => true]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <?= $form->field($model, 'assetSerialNumber')->textInput(['maxlength' => true]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'assetQrCode')->textInput(['maxlength' => true]) ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'assetphoto')->fileInput() ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Repair', '3' => 'Dispose']; ?>

                    <?= $form->field($model, 'assetStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                  </div>
                </div>

                <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

                <div class="col-sm-3">
                  <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>

                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
