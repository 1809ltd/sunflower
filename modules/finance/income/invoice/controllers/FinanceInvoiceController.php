<?php

namespace app\modules\finance\income\invoice\controllers;

use Yii;
use app\modules\finance\income\invoice\models\FinanceInvoice;
use app\modules\finance\income\invoice\models\FinanceInvoiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLines;
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLinesSearch;


/*Get Event Details*/
use app\modules\event\eventInfo\models\EventDetails;


/*Payment Pop up*/

use app\modules\finance\receipt\invoicepayment\models\FinancePayment;


/**
 * FinanceInvoiceController implements the CRUD actions for FinanceInvoice model.
 */
class FinanceInvoiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    // public function beforeAction($event){
    //     if(Yii::$app->Permission->getPermission())
    //         return parent::beforeAction($event);
    //     else
    //       // $this->redirect(\yii\helpers\Url::to(['/site/permission']));
    //         $this->redirect(['/site/permission']);
    // }

        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...

            $this->invoicedUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          }else {
            // code...
            $this->invoicedUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
          }
          return parent::beforeSave();
        }

        /**
         * Lists all FinanceInvoice models.
         * @return mixed
         */
        public function actionIndex()
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $searchModel = new FinanceInvoiceSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single FinanceInvoice model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new FinanceInvoice model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
         public function actionCreate()
         {

           // $this->layout='addformdatatablelayout';
           $this->layout = '@app/views/layouts/addformdatatablelayout';
             $model = new FinanceInvoice();

             $modelsFinanceInvoiceLines = [new FinanceInvoiceLines];

             //checking if its a post
             if ($model->load(Yii::$app->request->post())) {
               // code...

               // Geting the time created Timestamp
               $model->invoicedCreatedAt= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

               //Getting user Log in details
               //Getting user Log in details

               $model->invoicedCreatedby = Yii::$app->user->identity->id;
               $model->userId= Yii::$app->user->identity->id;;

               //Generating Invoice Number
               // $invoiceNum=$model->getInvnumber();
               // $model->invoiceNumber= $invoiceNum;

               $model->save();

               // $recipient = "mukhwanak@gmail.com,+254732386906";
               // $notification = "we are here and the invoice iko to sawa";
               // Yii::$app->notifier->send($recipient, $notification);

               //after Saving the customer Details
               $modelsFinanceInvoiceLines = Model::createMultiple(FinanceInvoiceLines::classname());
               Model::loadMultiple($modelsFinanceInvoiceLines, Yii::$app->request->post());

               // validate all models
               $valid = $model->validate();
               $valid = Model::validateMultiple($modelsFinanceInvoiceLines) && $valid;

               if ($valid) {
                   $transaction = \Yii::$app->db->beginTransaction();
                   try {
                       if ($flag = $model->save(false)) {
                           foreach ($modelsFinanceInvoiceLines as $modelFinanceInvoiceLines) {

                             $modelFinanceInvoiceLines->invoiceId = $model->id;
                             $modelFinanceInvoiceLines->createdBy = $model->invoicedCreatedby;
                             $modelFinanceInvoiceLines->userId = $model->userId;
                             $modelFinanceInvoiceLines->invoiceLinesStatus = $model->invoiceTxnStatus;
                             $modelFinanceInvoiceLines->approvedBy = $model->approvedby;

                             if (! ($flag = $modelFinanceInvoiceLines->save(false))) {
                               $transaction->rollBack();
                               break;
                             }
                           }
                       }
                       if ($flag) {
                         $transaction->commit();
                         return $this->redirect(['view', 'id' => $model->id]);
                       }

                   } catch (Exception $e) {

                       $transaction->rollBack();

                   }
               }
               // return $this->render('create', [
               //     'model' => $model,
               //     'modelsFinanceInvoiceLines' => (empty($modelsFinanceInvoiceLines)) ? [new FinanceInvoiceLines] : $modelsFinanceInvoiceLines
               // ]);

               return $this->redirect(['index']);

             } else {
               // code...

               //load the create form
               return $this->render('create', [
                   'model' => $model,
                   'modelsFinanceInvoiceLines' => (empty($modelsFinanceInvoiceLines)) ? [new FinanceInvoiceLines] : $modelsFinanceInvoiceLines
               ]);
             }

         }

         /**
          * Updates an existing FinanceInvoice model.
          * If update is successful, the browser will be redirected to the 'view' page.
          * @param integer $id
          * @return mixed
          * @throws NotFoundHttpException if the model cannot be found
          */
         public function actionUpdate($id)
         {

           // $this->layout='addformdatatablelayout';
           $this->layout = '@app/views/layouts/addformdatatablelayout';
             $model = $this->findModel($id);


             $modelsFinanceInvoiceLines = $this->getFinanceInvoiceLines($model->id);

             if ($model->load(Yii::$app->request->post())) {
               // code...

               //Getting user Log in details
               $model->userId = Yii::$app->user->identity->id;

               $model->save();

               //after Saving the customer Details

               $oldIDs = ArrayHelper::map($modelsFinanceInvoiceLines, 'id', 'id');
               $modelsFinanceInvoiceLines = Model::createMultiple(FinanceInvoiceLines::classname(), $modelsFinanceInvoiceLines);
               Model::loadMultiple($modelsFinanceInvoiceLines, Yii::$app->request->post());
               $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceInvoiceLines, 'id', 'id')));

               // ajax validation
               if (Yii::$app->request->isAjax) {
                   Yii::$app->response->format = Response::FORMAT_JSON;
                   return ArrayHelper::merge(
                       ActiveForm::validateMultiple($modelsFinanceInvoiceLines),
                       ActiveForm::validate($model)
                   );
               }

               // validate all models
               $valid = $model->validate();
               $valid = Model::validateMultiple($modelsFinanceInvoiceLines) && $valid;

               if ($valid) {
                   $transaction = \Yii::$app->db->beginTransaction();
                   try {
                       if ($flag = $model->save(false)) {
                           if (! empty($deletedIDs)) {
                               FinanceInvoiceLines::deleteAll(['id' => $deletedIDs]);
                           }
                           foreach ($modelsFinanceInvoiceLines as $modelFinanceInvoiceLines) {

                               $modelFinanceInvoiceLines->invoiceId = $model->id;
                               $modelFinanceInvoiceLines->createdBy = $model->invoicedCreatedby;
                               $modelFinanceInvoiceLines->userId = $model->userId;
                               $modelFinanceInvoiceLines->invoiceLinesStatus = $model->invoiceTxnStatus;
                               $modelFinanceInvoiceLines->approvedBy = $model->approvedby;


                               if (! ($flag = $modelFinanceInvoiceLines->save(false))) {
                                   $transaction->rollBack();
                                   break;
                               }
                           }
                       }
                       if ($flag) {
                           $transaction->commit();
                           return $this->redirect(['view', 'id' => $model->id]);
                       }
                   } catch (Exception $e) {
                       $transaction->rollBack();
                   }
               }

               // return $this->redirect(['view', 'id' => $model->id]);

             } else {
               // code...

               //load the create form
               return $this->render('update', [
                   'model' => $model,
                   'modelsFinanceInvoiceLines' => (empty($modelsFinanceInvoiceLines)) ? [new FinanceInvoiceLines] : $modelsFinanceInvoiceLines
               ]);
             }

         }
        public function getFinanceInvoiceLines($id)
        {
          $model = FinanceInvoiceLines::find()->where(['invoiceId' => $id])->all();
          return $model;
        }

        public function actionMakepayments($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';

          // $model = new FinanceInvoice();
          $model= $this->findModel($id);
          $payments = new FinancePayment();

          $model->invoiceNumber;

          $payments->paymentLinkedTxn=$model->invoiceNumber;

          $payments->paymentLinkedTxn;

          // print_r($model);
          // die();


            if ($payments->load(Yii::$app->request->post())) {
              // code...

              //Geting the time created Timestamp
              // $checkoutitem->timestamp= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));



              //Getting user Log in details
              $payments->recepitNumber = $payments->getRcpnumber();

              $payments->userId = Yii::$app->user->identity->id;

              $payments->save();
              return $this->redirect(['view', 'id' => $model->id]);

              // return $this->redirect(['deliverysearch']);

            } else {
              // code...

              //load the create form
              return $this->renderAjax('makepayment', [
                  'payments' => $payments,
                  'model' => $model,
              ]);
            }


        }

        /**
         * Deletes an existing FinanceInvoice model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          $model = $this->findModel($id);
          // $this->findModel($id)->delete();
          $this->findModel($id)->updateAttributes(['invoiceTxnStatus'=>0,'invoicedDeletedAt' => new \yii\db\Expression('NOW()')]);
          // Confriming that the post is not empty
          if (!empty($id)) {
            // code...
            $invoicelines = FinanceInvoiceLines::find()
                  ->Where('invoiceId = :invoiceId', [':invoiceId' => $id])
                  // ->andWhere('status = :status', [':status' => 3])
                  ->all();

          foreach ($invoicelines as $invoicelines) {
            // code...
            // Update all the data based on the payment made and status
            FinanceInvoiceLines::findOne($invoicelines["id"])->updateAttributes(['invoiceLinesStatus'=> 0,'invoiceLinesDeletedAt' => new \yii\db\Expression('NOW()')]);
          }
        }

        return $this->redirect(['index']);
        }

        /**
         * Finds the FinanceInvoice model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return FinanceInvoice the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            if (($model = FinanceInvoice::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }

        /*Getting the customers specif Events*/
        public function actionEvents() {
          $out=[];
            // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (isset(Yii::$app->request->post()['depdrop_parents'])) {
                $parents = Yii::$app->request->post('depdrop_parents');
                if ($parents != null) {
                    $customer_id = $parents[0];

                    $out = EventDetails::getCustomerEventList($customer_id);

                    return json_encode(['output'=>$out, 'selected'=>'']);
                }
            }
            return json_encode(['output' => '', 'selected' => '']);
        }

        /*Getting the Project Category Name*/
        public function actionEventcategory() {

          // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          if (isset(Yii::$app->request->post()['depdrop_parents'])) {
              $ids = Yii::$app->request->post('depdrop_parents');
              $customer_id = empty($ids[0]) ? null : $ids[0];
              $project_id = empty($ids[1]) ? null : $ids[1];
              if ($customer_id != null && $project_id != null) {

                  // $out = EventDetails::getCustomerEventList($customer_id);
                  $out = EventDetails::getEventCategory($project_id);

                  return json_encode(['output'=>$out, 'selected'=>'']);

              }
          }
          return json_encode(['output'=>'', 'selected'=>'']);

        }
        /*Getting the Project Category Name*/
        public function actionEventname() {

          // \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
          if (isset(Yii::$app->request->post()['depdrop_parents'])) {
              $ids = Yii::$app->request->post('depdrop_parents');
              $customer_id = empty($ids[0]) ? null : $ids[0];
              $project_id = empty($ids[1]) ? null : $ids[1];
              if ($customer_id != null && $project_id != null) {

                  // $out = EventDetails::getCustomerEventList($customer_id);
                  $out = EventDetails::getEventName($project_id);

                  return json_encode(['output'=>$out, 'selected'=>'']);

              }
          }
          return json_encode(['output'=>'', 'selected'=>'']);

        }
    }
