-- ----------------------------
-- View structure for `event_transaction`
-- ----------------------------
DROP VIEW IF EXISTS `event_transaction`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `event_transaction` AS
SELECT
  `finance_invoice_lines`.`id` AS `transactionId`,
  `finance_invoice_lines`.`invoiceId` AS `referenceId`,
  `finance_invoice`.`invoiceProjectId` AS `eventId`,
  finance_invoice.`customerId` AS `customerId`,
  `event_details`.`eventNumber` AS `eventNumber`,
  `event_details`.`eventName` AS `eventName`,
  `event_caterogies`.`eventCatName` AS `eventCatName`,
  `finance_invoice`.`invoiceNumber` AS `transactionNumber`,
  `finance_invoice_lines`.`invoiceLinesQty` AS `qty`,
  `finance_invoice_lines`.`invoiceLinesUnitPrice` AS `unitprice`,
  `finance_invoice_lines`.`invoiceLinesAmount` AS `dr_amount`,
  '0' AS `cr_amount`,
  `finance_invoice_lines`.`itemRefName` AS `transName`,
  `finance_invoice_lines`.`invoiceLinesDescription` AS `transDesc`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `fullyQualifiedName`,
  `finance_invoice_lines`.`invoiceLinesCreatedAt` AS `createdAt`,
  `finance_invoice_lines`.`invoiceLinesStatus` AS `status`,
  `finance_invoice`.`invoiceTxnDate` AS `txnDate`
FROM
  `finance_invoice_lines`
  INNER JOIN `finance_invoice`
    ON `finance_invoice_lines`.`invoiceId` = `finance_invoice`.`id`
  INNER JOIN `finance_items`
    ON `finance_invoice_lines`.`itemRefId` = `finance_items`.`id`
  INNER JOIN `finance_accounts`
    ON `finance_items`.`itemsIncomeAccountRef` = `finance_accounts`.id
  INNER JOIN `event_details`
    ON `finance_invoice`.`invoiceProjectId` = `event_details`.`id`
  LEFT JOIN `event_caterogies`
    ON `event_details`.`eventCategory` = `event_caterogies`.`id`
  LEFT JOIN `customer_details`
    ON `finance_invoice`.`customerId` = `customer_details`.`id`

Union All
SELECT
  `finance_event_requstion_form_list`.`id` AS `transactionId`,
  `finance_event_requstion_form_list`.`reqId` AS `referenceId`,
  `finance_event_requstion_form`.`eventId` AS `eventId`,
  `event_details`.`customerId` AS `customerId`,
  `event_details`.`eventNumber` AS `eventNumber`,
  `event_details`.`eventName` AS `eventName`,
  `event_caterogies`.`eventCatName` AS `eventCatName`,
  `finance_event_requstion_form`.`requstionNumber` AS `transactionNumber`,
  `finance_event_requstion_form_list`.`qty` AS `qty`,
  `finance_event_requstion_form_list`.`unitprice` AS `unitprice`,
  '0' AS `dr_amount`,
  `finance_event_requstion_form_list`.`amont` AS `cr_amount`,
  `finance_event_activity`.`activityName` AS `transName`,
  `finance_event_activity`.`activityDescription` AS `transDesc`,
  `finance_event_activity`.`account` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `fullyQualifiedName`,
  `finance_event_requstion_form_list`.`createdAt` AS `createdAt`,
  `finance_event_requstion_form_list`.`status` AS `status`,
  `finance_event_requstion_form`.`date` AS `txnDate`
FROM
  `finance_event_requstion_form_list`
  INNER JOIN `finance_event_requstion_form`
    ON `finance_event_requstion_form_list`.`reqId` = `finance_event_requstion_form`.`id`
  INNER JOIN `finance_event_activity`
    ON `finance_event_requstion_form_list`.`activityId` = `finance_event_activity`.`id`
  INNER JOIN `event_details`
    ON `finance_event_requstion_form`.`eventId` = `event_details`.`id`
  LEFT JOIN `event_caterogies`
    ON `event_details`.`eventCategory` = `event_caterogies`.`id`
  INNER JOIN `finance_accounts`
    ON `finance_event_activity`.`account` = `finance_accounts`.`id`
  INNER JOIN `customer_details`
    ON `event_details`.`customerId` = `customer_details`.`id`
Union All
-- UNION ALL
 SELECT
  `finance_bills_lines`.`id` AS `transactionId`,
  `finance_bills_lines`.`biilsId` AS `referenceId`,
  `finance_bills`.`projectId` AS `eventId`,
  `event_details`.`customerId` AS `customerId`,
  `event_details`.`eventNumber` AS `eventNumber`,
  `event_details`.`eventName` AS `eventName`,
  `event_caterogies`.`eventCatName` AS `eventCatName`,
  `finance_bills`.`billRef` AS `transctionNumber`,
  `finance_bills_lines`.`biilsLinesQty` AS `qty`,
  `finance_bills_lines`.`billlinePriceperunit` AS `unitprice`,
  '0' AS `dr_amount`,
  `finance_bills_lines`.`biilsLinesAmount` AS `as cr_amount`,
  `finance_bills_lines`.`biilsLinesVendorsRef` AS `transName`,
  `finance_bills_lines`.`biilsLinesDescription` AS `transsDesc`,
  `finance_accounts`.`id` AS `accountId`,
  `finance_accounts`.`fullyQualifiedName` AS `fullyQualifiedName`,
  `finance_bills_lines`.`billsLinesCreatedAt` AS `createdAt`,
  `finance_bills_lines`.`billsLinesBillableStatus` AS `status`,
  `finance_bills`.`billDueDate` AS `txtDate`
FROM
  `finance_bills_lines`
  INNER JOIN `finance_bills`
    ON `finance_bills_lines`.`biilsId` = `finance_bills`.`id`
  INNER JOIN `event_details`
    ON `finance_bills`.`projectId` = `event_details`.`id`
  INNER JOIN `finance_event_activity`
    ON `finance_bills_lines`.eventActivityId = `finance_event_activity`.`id`
  INNER JOIN `customer_details`
    ON `event_details`.`customerId` = `customer_details`.`id`
  INNER JOIN `event_caterogies`
    ON `event_details`.`eventCategory` = `event_caterogies`.`id`
  INNER JOIN finance_accounts
    ON finance_event_activity.account = finance_accounts.id


ORDER BY txtDate ASC
