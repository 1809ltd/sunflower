<?php

namespace app\modules\finance\expense\eventexpense\controllers;

use Yii;
use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionForm;
use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionFormSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\modules\finance\expense\eventexpense\eventexpenselines\models\FinanceEventRequstionFormList;
use app\modules\finance\expense\eventexpense\eventexpenselines\models\FinanceEventRequstionFormListSearch;

/*Get Event Details*/
use app\modules\event\eventInfo\models\EventDetails;
/**
 * FinanceEventRequstionFormController implements the CRUD actions for FinanceEventRequstionForm model.
 */
class FinanceEventRequstionFormController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...

            $this->updatedAt = new \yii\db\Expression('NOW()');

          }else {
            // code...
            $this->updatedAt = new \yii\db\Expression('NOW()');
          }
          return parent::beforeSave();
        }


        /**
         * Lists all FinanceEventRequstionForm models.
         * @return mixed
         */
        public function actionIndex()
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $searchModel = new FinanceEventRequstionFormSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single FinanceEventRequstionForm model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new FinanceEventRequstionForm model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */

        public function actionCreate()
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = new FinanceEventRequstionForm();
            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;


            $modelsFinanceEventRequstionFormList = [new FinanceEventRequstionFormList];

            // Validate if all the data is inserted
            if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
              Yii :: $app->response->format = 'json';
              return \yii\bootstrap\ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

              $modelsFinanceEventRequstionFormList = Model::createMultiple(FinanceEventRequstionFormList::classname());
              Model::loadMultiple($modelsFinanceEventRequstionFormList, Yii::$app->request->post());

              // ajax validation
              /*
              if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ArrayHelper::merge(
                        ActiveForm::validateMultiple($modelsFinanceEventRequstionFormList),
                        ActiveForm::validate($model)
                    );
                }*/

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceEventRequstionFormList) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          foreach ($modelsFinanceEventRequstionFormList as $modelFinanceEventRequstionFormList) {

                            $modelFinanceEventRequstionFormList->reqId = $model->id;
                            $modelFinanceEventRequstionFormList->userId = $model->userId;
                            $modelFinanceEventRequstionFormList->status = $model->requstionStatus;


                              if (! ($flag = $modelFinanceEventRequstionFormList->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }

                  } catch (Exception $e) {

                      $transaction->rollBack();

                  }
              }


            }

            return $this->render('create', [
                'model' => $model,
                  'modelsFinanceEventRequstionFormList' => (empty($modelsFinanceEventRequstionFormList)) ? [new FinanceEventRequstionFormList] : $modelsFinanceEventRequstionFormList
            ]);
        }

        /**
         * Updates an existing FinanceEventRequstionForm model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = $this->findModel($id);

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;


            $modelsFinanceEventRequstionFormList = $this->getFinanceEventRequstionFormList($model->id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

              $oldIDs = ArrayHelper::map($modelsFinanceEventRequstionFormList, 'id', 'id');
              $modelsFinanceEventRequstionFormList = Model::createMultiple(FinanceEventRequstionFormList::classname(), $modelsFinanceEventRequstionFormList);
              Model::loadMultiple($modelsFinanceEventRequstionFormList, Yii::$app->request->post());
              $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceEventRequstionFormList, 'id', 'id')));

              // ajax validation
              if (Yii::$app->request->isAjax) {
                  Yii::$app->response->format = Response::FORMAT_JSON;
                  return ArrayHelper::merge(
                      ActiveForm::validateMultiple($modelsFinanceEventRequstionFormList),
                      ActiveForm::validate($model)
                  );
              }

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceEventRequstionFormList) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          if (! empty($deletedIDs)) {
                              FinanceEventRequstionFormList::deleteAll(['id' => $deletedIDs]);
                          }
                          foreach ($modelsFinanceEventRequstionFormList as $modelFinanceEventRequstionFormList) {

                              $modelFinanceEventRequstionFormList->reqId = $model->id;
                              $modelFinanceEventRequstionFormList->userId = $model->userId;
                              $modelFinanceEventRequstionFormList->status = $model->requstionStatus;


                              if (! ($flag = $modelFinanceEventRequstionFormList->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }
                  } catch (Exception $e) {
                      $transaction->rollBack();
                  }
              }
            }

            return $this->render('update', [
                'model' => $model,
                'modelsFinanceEventRequstionFormList' => (empty($modelsFinanceEventRequstionFormList)) ? [new FinanceEventRequstionFormList] : $modelsFinanceEventRequstionFormList
            ]);

        //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //         return $this->redirect(['view', 'id' => $model->id]);
        //     }
        //
        //     return $this->render('update', [
        //         'model' => $model,
        //     ]);
        }
        public function getFinanceEventRequstionFormList($id)
        {
          $model = FinanceEventRequstionFormList::find()->where(['reqId' => $id])->all();
          return $model;
        }


        /**
         * Deletes an existing FinanceEventRequstionForm model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            // $this->findModel($id)->delete();
          $this->findModel($id)->updateAttributes(['requstionStatus'=>0,'deletedAt' => new \yii\db\Expression('NOW()')]);
          // Confriming that the post is not empty

          if (!empty($id)) {
                // code...
                $eventreqlines = FinanceEventRequstionFormList::find()->Where('reqId = :reqId', [':reqId' => $id])
                                                                // ->andWhere('status = :status', [':status' => 3])
                                                                ->all();

                foreach ($eventreqlines as $eventreqlines) {
                  // code...
                  // Update all the data based on the payment made and status
                  FinanceEventRequstionFormList::findOne($eventreqlines["id"])->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);

                }
              }


            return $this->redirect(['index']);
        }

        /**
         * Finds the FinanceEventRequstionForm model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return FinanceEventRequstionForm the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
          // $this->layout='addformdatatablelayout';
          $this->layout = '@app/views/layouts/addformdatatablelayout';
            if (($model = FinanceEventRequstionForm::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
