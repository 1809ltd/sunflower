<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceAccountsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="finance-accounts-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

          //'id',
          //'companyId',
          'subAccount',
          //'accountParentId',
          // 'accountName',
          // 'accountNumber',
          // 'accountsDescription:ntext',
          // 'fullyQualifiedName',
          'accountsClassification',
          'accountsType',
          'accountsDetailType',
          'accountsStatus',
          'accountsCurrentBalance',
          //'accountsCreateTime',
          //'accountsUpdatedAt',
          //'accountsDeleteAt',
          //'userId',

        ],
    ]); ?>

</div>
