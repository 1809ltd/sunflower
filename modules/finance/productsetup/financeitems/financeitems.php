<?php

namespace app\modules\finance\productsetup\financeitems;

/**
 * financeitems module definition class
 */
class financeitems extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\productsetup\financeitems\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
