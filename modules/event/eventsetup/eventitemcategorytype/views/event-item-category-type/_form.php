<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;


/* @var $this yii\web\View */
/* @var $model app\models\EventItemCategoryType */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body event-item-category-type-form">

  <?= $form->field($model, 'itemId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(FinanceItems::find()->all(),'id','itemsName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Service ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>

  <?= $form->field($model, 'categorytypeId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(AssetCategoryType::find()->all(),'id','assetCategoryTypeName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select Asset Type ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>

   <?= $form->field($model, 'qty')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']);?>


  <?= $form->field($model, 'userid')->hiddenInput(['value'=> "1"])->label(false);?>


</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
