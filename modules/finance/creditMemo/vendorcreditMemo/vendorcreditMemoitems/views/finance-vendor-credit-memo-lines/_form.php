<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemoLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-vendor-credit-memo-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vendorId')->textInput() ?>

    <?= $form->field($model, 'vendorCreditMemoId')->textInput() ?>

    <?= $form->field($model, 'invoiceNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendorCreditMemoLineDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'vendorCreditMemoLineAmount')->textInput() ?>

    <?= $form->field($model, 'vendorCreditMemoLineStatus')->textInput() ?>

    <?= $form->field($model, 'vendorCreditMemoLineCreatedAt')->textInput() ?>

    <?= $form->field($model, 'vendorCreditMemoLineUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'creidtDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
