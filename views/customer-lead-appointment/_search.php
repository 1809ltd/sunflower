<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLeadAppointmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-lead-appointment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'leadId') ?>

    <?= $form->field($model, 'leadAppointSubject') ?>

    <?= $form->field($model, 'leadAppointStart') ?>

    <?= $form->field($model, 'leadAppointEnd') ?>

    <?php // echo $form->field($model, 'leadAppointStatus') ?>

    <?php // echo $form->field($model, 'leadAppointNotes') ?>

    <?php // echo $form->field($model, 'leadAppointCreatedAt') ?>

    <?php // echo $form->field($model, 'leadAppointLastUpdatedAt') ?>

    <?php // echo $form->field($model, 'leadAppointDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
