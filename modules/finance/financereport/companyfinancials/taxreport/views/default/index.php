<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Tax Reports*/
use app\modules\finance\financereport\companyfinancials\taxreport\models\TaxReport;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Tax Statement';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
// $form = ActiveForm::begin([
    // 'action'  => ['search'],
    // 'method'  => 'get',
    // 'options' => ['class' => 'form-inline'],
// ]);
?>
<!-- <div class="form-group">
  <label class="control-label" for="search">Search: </label>
  <input id="search" name="search" placeholder="Search Here" class="form-control input-md" required value="" type="text">
</div>
      <div class="form-group">
        <?=Html::submitButton('Search', ['class' => 'btn btn-primary'])?>
    </div> -->
    <?php //ActiveForm::end();?>
<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <?php //$form = ActiveForm::begin([
          // 'action'  => ['search'],
          // 'method'  => 'get',
          // 'options' => ['class' => 'form-inline'],
      //]); ?>
      <?php
      $form = ActiveForm::begin([
        // 'action' => [''],
        // 'options' => [
        //   'class' => 'tax-form'
        // ]
      ]);
      ?>
    <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date From: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
              //'language' => 'ru',
              'options' => ['autocomplete'=>'off','class' => 'form-control'],
              'inline' => false,
              'dateFormat' => 'yyyy-MM-dd',
              ])->label(false) ?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="col-lg-4 control-label">Date To: </label>
            <div class="col-lg-8">
              <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                //'language' => 'ru',
                'options' => ['autocomplete'=>'off','class' => 'form-control'],
                'inline' => false,
                'dateFormat' => 'yyyy-MM-dd',
                ])->label(false) ?>
              </div>
            </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <div class="col-lg-8 col-lg-offset-4">
              <div class="center-align">
                <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
              </div>
            </div>
          </div>
        </div>
        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
    <!-- this row will not appear when printing -->
    <!-- <div class="row no-print">
      <div class="col-xs-12">
        <a href="<?php //Url::to(['/finance/report/company/tax-report/default/view','startDate'=>$startDate,'endDate'=>$endDate]) ?>"
        class="btn btn-xs btn-success m-b-10"><i
                      class="fa fa-print m-r-5"></i> Show Detail Audit Report</a>
      </div>
    </div> -->
<?php
// $script = <<<JS
// jQuery(document).ready(function($) {
//   $(".tax-form").submit(function(event) {
//     event.preventDefault(); // stopping submitting
//     var data = $(this).serializeArray();
//     var url = $(this).attr('action');
//     $.ajax({
//       url: url,
//       type: 'post',
//       dataType: 'json',
//       data: data
//     })
//     .done(function(response) {
//       if (response.data.success == true) {
//         alert("Wow you commented");
//       }
//     })
//     .fail(function() {
//       console.log("error");
//     });
//   });
// });
// JS;
// $this->registerJs($script);
?>
