<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AssetRegistration;

/**
 * AssetRegistrationSearch represents the model behind the search form of `app\models\AssetRegistration`.
 */
class AssetRegistrationSearch extends AssetRegistration
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vendor', 'assetStatus', 'userId'], 'integer'],
            [['assetName', 'assetDescription', 'assetCategory', 'assetCategoryType', 'assetTag', 'assetpurchaseDate', 'assetBrand', 'assetModel', 'assetSerialNumber', 'assetQrCode', 'assetphoto', 'assetTimestamp', 'assetUpdatedAt', 'assetDeletedAt'], 'safe'],
            [['assetCost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetRegistration::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assetpurchaseDate' => $this->assetpurchaseDate,
            'vendor' => $this->vendor,
            'assetCost' => $this->assetCost,
            'assetTimestamp' => $this->assetTimestamp,
            'assetUpdatedAt' => $this->assetUpdatedAt,
            'assetDeletedAt' => $this->assetDeletedAt,
            'assetStatus' => $this->assetStatus,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'assetName', $this->assetName])
            ->andFilterWhere(['like', 'assetDescription', $this->assetDescription])
            ->andFilterWhere(['like', 'assetCategory', $this->assetCategory])
            ->andFilterWhere(['like', 'assetCategoryType', $this->assetCategoryType])
            ->andFilterWhere(['like', 'assetTag', $this->assetTag])
            ->andFilterWhere(['like', 'assetBrand', $this->assetBrand])
            ->andFilterWhere(['like', 'assetModel', $this->assetModel])
            ->andFilterWhere(['like', 'assetSerialNumber', $this->assetSerialNumber])
            ->andFilterWhere(['like', 'assetQrCode', $this->assetQrCode])
            ->andFilterWhere(['like', 'assetphoto', $this->assetphoto]);

        return $dataProvider;
    }
}
