<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
// Get the related payments based on the payemtn
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;
/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
  <div class="box-header with-border">
    <h3 class="box-title">Payment Details</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <div class="finance-office-requisition-payments-form">
      <?php $form = ActiveForm::begin(['enableAjaxValidation' => true,]); ?>
      <?php
      // $form = ActiveForm::begin(['action' => ['create'],'options' => ['method' => 'post']]);?>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'reqrecepitNumber')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'officeReqPaymentaccount')->widget(Select2::classname(), [
              'data' => ArrayHelper::map(FinanceAccounts::find()->where(['like', 'accountsPays', 'yes'])->all(),'id','fullyQualifiedName'),
              'language' => 'en',
              'options' => ['placeholder' => 'Select a Payment Account ...'],
              'pluginOptions' => [
                'allowClear' => true
              ],
            ]);  ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'officeReqPaymentofficeReqRef')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <?php $data = ['Cash' => 'Cash', 'Cheque'=> 'Cheque','Debit Card'=> 'Debit Card', 'Credit Card' => 'Credit Card','Transfer' => 'Transfer','Voucher' => 'Voucher','Mobile Payment' => 'Mobile Payment','Internet Payment' => 'Internet Payment']; ?>
            <?= $form->field($model, 'officeReqPaymentPayType')->widget(Select2::classname(), [
                'data' => $data,
                'language' => 'en',
                'options' => ['placeholder' => 'Select a Payment Method ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'officeReqPaymentTotalAmt')->textInput(['maxlength' => true,'autocomplete'=>"off",'class' => 'valueamount form-control']) ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                    //'language' => 'ru',
                    'options' => ['autocomplete'=>'off','class' => 'form-control'],
                    'inline' => false,
                    'dateFormat' => 'yyyy-MM-dd',
                ]); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'unallocatedAmount')->textInput(['readonly' => true,'class' => 'unlocatedamount form-control']) ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <?= $form->field($model, 'officeReqPaymentPrivateNote')->textarea(['rows' => 6]) ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <?php
            // necessary for update action.
            if (! $model->isNewRecord) {
              // code...
              $status = ['0' => 'Cancel', '1' => 'Aproved', '2' => 'Waiting'];
              echo $form->field($model, 'paymentstatus')->dropDownList($status, ['readonly' => true,'prompt'=>'Select Status']);

            }else{

              echo $form->field($model, 'paymentstatus')->hiddenInput(['value'=> "2"])->label(false);
            }
            ?>
          </div>
        </div>
      </div>
      <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
<?php
if (! $model->isNewRecord) {
  // code...
  // if its not a new record been saved
    $paymentsonHoldamount = FinanceOfficeRequisitionPaymentsLines::find()
                                ->where(['officeReqpaymentId' => $model->id])
                                ->Andwhere(['status'=>$model->paymentstatus])
                                ->sum('amount');

    if (empty($paymentsonHoldamount)){

      $paymentsonHoldamount= $model->unallocatedAmount;
    }
  }else{

  $paymentsonHoldamount = FinanceOfficeRequisitionPaymentsLines::find()
                              ->Andwhere(['status'=>3])->Andwhere(['finance_office_requisition_payments_lines.userId'=>Yii::$app->user->identity->id])->sum('amount');

  if (empty($paymentsonHoldamount)) {
    // code...
    $paymentsonHoldamount= 0;
  }
}

// echo $paymentsonHoldamount;
$script = <<<JS
var getAmount = function() {
  // Getting the values
  var valueamount = $(".valueamount").val();
  var holdamountfull = $paymentsonHoldamount;
  // get the full amount
  var diffamount = parseInt(valueamount) - parseInt(holdamountfull);
  // amount = parseInt(qnty) * parseInt(unlocatedamount);
  //Assign the sum value to the field
  $(".unlocatedamount").val(diffamount);
};
//Bind new elements to support the function too
$(".valueamount").on("change", function() {
  getAmount();
});
JS;
$this->registerJs($script);
?>
