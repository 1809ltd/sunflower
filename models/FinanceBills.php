<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_bills".
 *
 * @property int $id
 * @property int $projectId
 * @property int $vendorId
 * @property string $billRef
 * @property string $billDueDate
 * @property double $billAmount
 * @property double $billsTaxAmount
 * @property string $billCreatetime
 * @property string $billUpdatedTime
 * @property string $billDeletedAt
 * @property int $billStatus
 * @property int $userId
 *
 * @property EventDetails $project
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 * @property FinanceBillsLines[] $financeBillsLines
 * @property FinanceBillsPayments[] $financeBillsPayments
 * @property FinanceVendorCreditMemoLines[] $financeVendorCreditMemoLines
 */
class FinanceBills extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_bills';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['projectId', 'vendorId', 'billStatus', 'userId'], 'integer'],
            [['vendorId', 'billRef', 'billDueDate', 'billAmount', 'billsTaxAmount', 'billStatus', 'userId'], 'required'],
            [['billDueDate', 'billCreatetime', 'billUpdatedTime', 'billDeletedAt'], 'safe'],
            [['billAmount', 'billsTaxAmount'], 'number'],
            [['billRef'], 'string', 'max' => 50],
            [['projectId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['projectId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'projectId' => 'Project',
            'vendorId' => 'Vendor',
            'billRef' => 'Invoice No:',
            'billDueDate' => 'Due Date',
            'billAmount' => 'Amount',
            'billsTaxAmount' => 'Bills Tax Amount',
            'billCreatetime' => 'Bill Createtime',
            'billUpdatedTime' => 'Bill Updated Time',
            'billDeletedAt' => 'Bill Deleted At',
            'billStatus' => 'Bill Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'projectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsLines()
    {
        return $this->hasMany(FinanceBillsLines::className(), ['biilsId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsPayments()
    {
        return $this->hasMany(FinanceBillsPayments::className(), ['billId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemoLines()
    {
        return $this->hasMany(FinanceVendorCreditMemoLines::className(), ['billRef' => 'id']);
    }
}
