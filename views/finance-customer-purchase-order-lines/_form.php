<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrderLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-customer-purchase-order-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'purchaseOrderId')->textInput() ?>

    <?= $form->field($model, 'itemRefName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'purchaseOrderLinesDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'purchaseOrderLinespack')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesQty')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesUnitPrice')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesAmount')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesTaxCodeRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'purchaseOrderLinesTaxamount')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesDiscount')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesCreatedAt')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'purchaseOrderLinesDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
