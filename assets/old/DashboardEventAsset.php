<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardEventAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       // 'css/site.css',

        /*Begin Base Css Style*/
        'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        'assets1/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css',
        'assets1/plugins/bootstrap/css/bootstrap.min.css',
        'assets1/css/style.css',
        'assets1/css/style-responsive.css',
        'assets1/css/default.css',
        /*End Base Css Style*/


    ];
    public $js = [
        /* BEGIN BASE JS */
        'assets1/plugins/jquery/jquery-1.12.4.min.js',
        'assets1/plugins/jquery/jquery-migrate-1.4.1.min.js',
        'assets1/plugins/jquery-ui/ui/minified/jquery-ui.min.js',
        'assets1/plugins/bootstrap/js/bootstrap.min.js',

        'assets1/plugins/slimscroll/jquery.slimscroll.min.js',
        'assets1/plugins/jquery-cookie/jquery.cookie.js',
        'assets1/plugins/jquery-cookie/js.cookie.js',
        'assets1/plugins/pace/pace.min.js',
        'assets1/plugins/chartjs/Chart.min.js',
        'assets1/plugins/typeit/typeit.js',
        'assets1/plugins/countup/countUp.min.js',
        'assets1/js/app.js',
        /* END BASE JS */

        /* BEGIN Dashboard PAGE LEVEL JS */
        'assets1/plugins/chartjs/Chart.min.js',
        'assets1/plugins/jquery.sparkline/jquery.sparkline.min.js',
        'assets1/js/dashboard_v2.js',
        /* END  Dashoboard PAGE LEVEL JS */


    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
