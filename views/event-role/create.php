<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventRole */

$this->title = 'Create Event Role';
$this->params['breadcrumbs'][] = ['label' => 'Event Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
