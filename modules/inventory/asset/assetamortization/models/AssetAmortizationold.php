<?php

namespace app\modules\inventory\asset\assetamortization\models;
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
use app\modules\company\companydetails\models\CompanyDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "asset_amortization".
 *
 * @property int $id
 * @property int $assetId
 * @property int $repayment
 * @property double $interestAmount
 * @property double $principalAmount
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $status
 * @property string $amortizationDate
 * @property double $startBalance
 * @property double $endBalance
 * @property double $cummulativeInterest
 * @property double $cummulativePrincipal
 */
class AssetAmortization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_amortization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'repayment', 'interestAmount', 'principalAmount', 'status', 'userId'], 'required'],
            [['assetId', 'repayment', 'status', 'userId'], 'integer'],
            [['interestAmount', 'principalAmount'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetId' => 'Asset ID',
            'repayment' => 'Repayment',
            'interestAmount' => 'Interest Amount',
            'principalAmount' => 'Principal Amount',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
    //Getting the value of asset as of a specif date
    // public functionassetvalueasof($date){
    //
    // }
  }
