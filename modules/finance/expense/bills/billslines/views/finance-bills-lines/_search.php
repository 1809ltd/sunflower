<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBillsLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-bills-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'biilsId') ?>

    <?= $form->field($model, 'biilsLinesVendorsRef') ?>

    <?= $form->field($model, 'biilsLinesDescription') ?>

    <?= $form->field($model, 'biilsLinespacks') ?>

    <?php // echo $form->field($model, 'biilsLinesQty') ?>

    <?php // echo $form->field($model, 'billlinePriceperunit') ?>

    <?php // echo $form->field($model, 'biilsLinesAmount') ?>

    <?php // echo $form->field($model, 'billLinesTaxCodeRef') ?>

    <?php // echo $form->field($model, 'biilsLinesTaxAmount') ?>

    <?php // echo $form->field($model, 'accountId') ?>

    <?php // echo $form->field($model, 'billsLinesBillableStatus') ?>

    <?php // echo $form->field($model, 'billsLinesCreatedAt') ?>

    <?php // echo $form->field($model, 'billsLinesDeleteAt') ?>

    <?php // echo $form->field($model, 'billsLinesUpdatedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
