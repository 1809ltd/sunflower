<!-- <div class="alltransaction-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/*Chart of Accounts*/
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*All trasnaction  */
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

/*Getting the Select2 to work */
use kartik\select2\Select2;

/* @var $this yii\web\View */
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

// $model->accountId= \Yii::$app->session->get('transactionaccountId');
// $model->startDate= \Yii::$app->session->get('startDate');
// $model->endDate= \Yii::$app->session->get('endDate');

$this->title = 'Account Transactions';
$this->params['breadcrumbs'][] = ['label' => 'Account Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<!-- Search Date And the rest -->
<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <?php $form = ActiveForm::begin([
                          'action' => ['index'],
                          'options' => ['method' => 'post'],
                      ]);
                      // begin(['action' => ['builder/saveform'],'options' => ['method' => 'post']]) ?>
       <div class="col-md-4">
         <div class="form-group">
           <label class="col-lg-4 control-label">Account: </label>
           <div class="col-lg-8">
             <?= $form->field($model,'account')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(FinanceAccounts::find()
                                                              ->where(['accountsStatus'=>1])
                                                              ->all(),'id','fullyQualifiedName'),
                    'language' => 'en',
                    'options' => ['prompt'=>'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false) ?>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="col-lg-4 control-label">From: </label>
              <div class="col-lg-8">
                <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="col-lg-4 control-label">To: </label>
                <div class="col-lg-8">
                  <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['autocomplete'=>'off','class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ])->label(false) ?>
                </div>
              </div>
            </div>
          <div class="col-md-2">
            <div class="form-group">
              <div class="col-lg-8 col-lg-offset-4">
                <div class="center-align">
                  <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
                </div>
              </div>
            </div>
          </div>
          <?php ActiveForm::end(); ?>
        </div>
    </div>
  </div>
<?php
if (!empty($transactionaccounts)) {
  // code...
  // if the array is not empty
  ?>
  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Accounts Transactions</h3>
      <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($startDate, 'long');?> to <?= Yii::$app->formatter->asDate($endDate, 'long');?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>

    <div class="box">
      <div class="box-body">
        <h5 class="box-title"><?= $accountName["fullyQualifiedName"];?></h5>
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th class="text-left">Date</th>
              <th class="">Details</th>
              <th class="text-right">Dr</th>
              <th class="text-right">Cr</th>
            </tr>
          </thead>
          <tbody>
            <?php

            $transactionCreditTotal=0;
            $transactionDrTotal=0;
            // Getting all the transactions for that period
            foreach ($transactionaccounts as $transactionaccount){
              // code...

              $transactionCreditTotal+= $transactionaccount->cr_amount;
              $transactionDrTotal+= $transactionaccount->dr_amount;

              // Assign Url based on the transaction
              // Office Expense
              if ($transactionaccount->transactionClassification=="Office Expense") {
                // code...
                $url= Url::to(['/finance/expense/officeexpense/finance-office-requstion-form/view','id'=>$transactionaccount->referenceId]);

              }
              // Office Expenses Payement
              elseif ($transactionaccount->transactionClassification=="Office Expense Payment") {
                // code...
                $url= Url::to(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/view','id'=>$transactionaccount->referenceId]);

              }
              //Lead Expense
              elseif ($transactionaccount->transactionClassification=="Lead expense") {
                // code...

              }
              //Lead Expense Payement
               elseif ($transactionaccount->transactionClassification=="Lead Expense Payment") {
                // code...
                // $url= Url::to(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/view','id'=>$transactionaccount->referenceId]);

              }
              //Event  Expense
              elseif ($transactionaccount->transactionClassification=="Event Expense") {
                // code...
                $url= Url::to(['/finance/expense/eventexpense/finance-event-requstion-form/view','id'=>$transactionaccount->referenceId]);

              }
              //Event Expense Payment
              elseif ($transactionaccount->transactionClassification=="Event expense Payment") {
                // code...
                $url= Url::to(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/view','id'=>$transactionaccount->referenceId]);

              }
              //Bill Expense
              elseif ($transactionaccount->transactionClassification=="Bill Expense") {
                // code...

                $url= Url::to(['/finance/expense/bills/finance-bills/view','id'=>$transactionaccount->referenceId]);

              } elseif ($transactionaccount->transactionClassification=="Bill Payment expense") {
                // code...
                $url= Url::to(['/finance/expensepayment/bill-payment/finance-bills-payments/view','id'=>$transactionaccount->referenceId]);

              } elseif ($transactionaccount->transactionClassification=="Bill Payment CreditNote expense") {
                // code...
                $url= Url::to(['/finance/creditMemo/vendorcreditMemo/finance-vendor-credit-memo/view','id'=>$transactionaccount->referenceId]);

              } elseif ($transactionaccount->transactionClassification=="Bill Payment Offset expense") {
                // code...
                $url= Url::to(['/finance/offset/outhouse/finance-vendor-offset-memo/view','id'=>$transactionaccount->referenceId]);

              }elseif ($transactionaccount->transactionClassification=="Invoice") {
                // code...

                $url= Url::to(['/finance/income/invoice/finance-invoice/view','id'=>$transactionaccount->referenceId]);

              } elseif ($transactionaccount->transactionClassification=="Invoice Payment") {
                // code...
                $url= Url::to(['/finance/receipt/invoicepayment/finance-payment/view','id'=>$transactionaccount->referenceId]);

              } elseif (($transactionaccount->transactionClassification=="Invoice Payment") & ($transactionaccount->transactionCategory=="Expense Credit Payment")) {
                // code...
                $url= Url::to(['/finance/creditMemo/customercreditMemo/finance-credit-memo/view','id'=>$transactionaccount->referenceId]);

              } elseif (($transactionaccount->transactionClassification=="Invoice Payment") & ($transactionaccount->transactionCategory=="Offset Payment")) {
                // code...
                $url= Url::to(['/finance/offset/inhouse/finance-offset-memo/view','id'=>$transactionaccount->referenceId]);

              } else {
                // code...

              }

              ?>
              <tr>
                <td class="text-left"><?= Yii::$app->formatter->asDate($transactionaccount->transactionDate, 'long');?></td>
                <td  class=""><a href="<?= $url ?>"><?= $transactionaccount->tranctionDescription;?></a></td>
                <td  class="text-right"><span class="align-numbers"><?= number_format($transactionaccount->dr_amount,2) ;?></span></td>
                <td  class="text-right"><span class="align-numbers"><?= number_format($transactionaccount->cr_amount,2) ;?></span></td>
              </tr>
             <?php
           }
           ?>
           <tr>
             <th class="text-left">Total</th>
             <th class=""></th>
             <th class="text-right"><?= number_format($transactionDrTotal,2); ?></th>
             <th class="text-right"><?= number_format($transactionCreditTotal,2); ?></th>
           </tr>
         </tbody>
       </table>
     </br>

  </div>
  </div>
  <!-- this row will not appear when printing -->
  <!-- <div class="row no-print">
    <div class="col-xs-12">
      <a href="javascript:void(0)" onclick="window.print()"
         class="btn btn-xs btn-success m-b-10"><i
              class="fa fa-print m-r-5"></i> Print</a>

    </div>
  </div> -->
  </section>
  <?php

} else {
  // code...


}




?>
