<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrderLines */

$this->title = 'Create Finance Customer Purchase Order Lines';
$this->params['breadcrumbs'][] = ['label' => 'Finance Customer Purchase Order Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-customer-purchase-order-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
