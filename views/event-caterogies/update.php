<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventCaterogies */

$this->title = 'Update Event Caterogies: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Caterogies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-caterogies-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
