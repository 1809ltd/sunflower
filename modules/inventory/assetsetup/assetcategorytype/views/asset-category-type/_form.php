<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;

/*Asset Category*/
use app\modules\inventory\assetsetup\assetcategory\models\AssetCategory;
/* @var $this yii\web\View */
/* @var $model app\models\AssetCategoryType */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body asset-category-type-form">

  <?= $form->field($model, 'assetCategoryId')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(AssetCategory::find()->where('`assetCategoryStatus` = 1')->asArray()->all(), 'id', 'assetCategoryName'),
                            'options' => ['placeholder' => 'Select Category'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);?>

  <?= $form->field($model, 'assetCategoryTypeName')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'assetCategoryTypeDescription')->textarea(['rows' => 6]) ?>

  <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                    <?= $form->field($model, 'assetCategoryTypeStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>


  <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>


</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>