<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<div class="finance-credit-memo-lines-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'creditMemoId',
            'invoiceNumber',
            'creditMemoLineDescription:ntext',
            'creditMemoLineAmount',
            //'creditMemoLineStatus',
            //'creditMemoLineCreatedAt',
            //'creditMemoLineUpdatedAt',
            //'creidtDeletedAt',
            //'userId',

        ],
    ]); ?>

</div>
