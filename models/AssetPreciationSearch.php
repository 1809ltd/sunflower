<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AssetPreciation;

/**
 * AssetPreciationSearch represents the model behind the search form of `app\models\AssetPreciation`.
 */
class AssetPreciationSearch extends AssetPreciation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assetId', 'companyId', 'userId'], 'integer'],
            [['preciableCost', 'preciationSalvageValue', 'assetLifeMonths'], 'number'],
            [['preciationDateAcquired', 'preciationDateAcquiredDepreciationMethod', 'preciationStatus', 'preciationTimestamp', 'preciationUpdatedAt', 'preciationDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetPreciation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assetId' => $this->assetId,
            'companyId' => $this->companyId,
            'preciableCost' => $this->preciableCost,
            'preciationSalvageValue' => $this->preciationSalvageValue,
            'assetLifeMonths' => $this->assetLifeMonths,
            'preciationDateAcquired' => $this->preciationDateAcquired,
            'preciationTimestamp' => $this->preciationTimestamp,
            'preciationUpdatedAt' => $this->preciationUpdatedAt,
            'preciationDeletedAt' => $this->preciationDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'preciationDateAcquiredDepreciationMethod', $this->preciationDateAcquiredDepreciationMethod])
            ->andFilterWhere(['like', 'preciationStatus', $this->preciationStatus]);

        return $dataProvider;
    }
}
