<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItemsCategoryType */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body event-item-role-form">

  <?= $form->field($model, 'financeItemCategoryTypeName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

  <?= $form->field($model, 'financeItemCategoryTypeDescription')->textarea(['rows' => 6]) ?>

  <?= $form->field($model, 'financeItemCategoryId')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(FinanceItemsCategory::find()->all(),'id','financeItemCategoryName'),
      'language' => 'en',
      'options' => ['placeholder' => 'Select a Categry ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);
  ?>

  <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
  <?= $form->field($model, 'financeItemCategoryTypeStatus')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
