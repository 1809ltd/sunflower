<!-- <div class="expense-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Expense Summary';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <div class="customerreport-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<!-- Main content -->
<!-- Search Date And the rest -->
<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <?php $form = ActiveForm::begin(); ?>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date From: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                      //'language' => 'ru',
                      'options' => ['autocomplete'=>'off','class' => 'form-control'],
                      'inline' => false,
                      'dateFormat' => 'yyyy-MM-dd',
                  ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date To: </label>
          <div class="col-lg-8">
                <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <div class="col-lg-8 col-lg-offset-4">
            <div class="center-align">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
            </div>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
<?php
if (!empty($allExpense)) {
  // code...
// print_r($customer);
// echo $customer[0]["customerCompanyName"];

// die();
  ?>

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="<?=Url::to(['/finance/expense/default/pdf','startDate'=>$startDate,'endDate'=>$endDate]) ?>"
      class="btn btn-xs btn-success m-b-10"><i
                    class="fa fa-print m-r-5"></i> Generate PDf</a>
    </div>
  </div>

  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-8">
        <!-- <h3 class="page-header"> -->
        <h6>
          <img style="height:75%;width:75%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
        </h6>
      </div>
      <div class="col-xs-4">
        <p>
        <address>
          <strong>
            <?= $companydetails->companyName?>,</strong><br/>
            <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
               Phone: +254 (0) 722 790632<br/>
               Email: info@sunflowertents.com
        </address>
      </p>
      </div>
      <!-- /.col -->
     </div>
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <?php
             // echo "This is the Month ->".$resultdate[0];
             // echo "This is the Year ->".$resultdate[1];
             // print_r($resultdate);

             // echo "This is the Month ->".$resultdate;

              ?>
             <th class="page-header" style="text-align: center;width: 85%;">Expense Based on Invoces from Supppliers,Site Expense and Office Requstions: <?php
             // Yii::$app->formatter->asDate($tarehe, 'long');?></th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>
     <!-- info row -->
     <div class="row invoice-info">
     </div>
     <!-- /.row -->
     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Invoice Date:</th>
             <th>Invoice No:</th>
             <th>Description:</th>
             <th>Sub Total</th>
             <th>V.A.T</th>
             <th>W.H.T</th>
             <th>Other Tax</th>
             <th>Payments</th>
             <th>Balance</th>
           </thead>
             <tbody>
               <?php
               // Get all the specific Invoice

               $Subtoalm=0;
               $VATtotalm=0;
               $WHTtotalm=0;
               $taxtotalm=0;
               $paymenttotalm=0;

               foreach ($allExpense as $allExpense) {
                 // code...
                 $Subtoal=0;
                 $VATtotal=0;
                 $WHTtotal=0;
                 $taxtotal=0;
                 // print_r($allExpense->getAttributes());
                 $expenseItems=$allExpense->getexpenseItems($allExpense->transactionId,$allExpense->status,$allExpense->referenceTable);

                 ?>
                 <tr>
                   <td><?= Yii::$app->formatter->asDate($allExpense["transactionDate"], 'long');?></td>

                   <td><?= $allExpense->referenceCode;?></td>
                   <td><strong>Supplier : </strong><?= $allExpense->recipientName;?><br/><strong>Details: </strong>: <?= $allExpense->tranctionDescription;?></td>

                     <?php
                     foreach ($expenseItems as $expenseItems) {
                       // code...
                       if (($allExpense->referenceTable=="finance_office_requstion_form")) {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["amont"];

                       }elseif ($allExpense->referenceTable=="finance_lead_requstion_form") {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["amont"];

                       }elseif ($allExpense->referenceTable=="finance_event_requstion_form") {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["amont"];

                       }elseif ($allExpense->referenceTable=="finance_bills") {
                         // code...
                         $Subtoal=$Subtoal+$expenseItems["biilsLinesAmount"];

                         if ($expenseItems["billLinesTaxCodeRef"]== 0.16) {
                           // code...
                           $VATtotal=$VATtotal+$expenseItems["biilsLinesTaxAmount"];

                         }elseif ($expenseItems["billLinesTaxCodeRef"]== 0.05) {
                           // code...
                           $WHTtotal=$WHTtotal+$expenseItems["biilsLinesTaxAmount"];

                         } else {
                           // code...
                           $taxtotal=$taxtotal+$expenseItems["biilsLinesTaxAmount"];
                         }

                       }else {
                         // code...
                         $Subtoal=$Subtoal+0;
                         $taxtotal=$taxtotal+0;
                       }

                     }
                     // To get the overal sub total;
                     $Subtoalm+=$Subtoal;

                     // To get the overal VAT  total;
                     $VATtotalm+=$VATtotal;

                     // To get the overal VAT  total;
                     $WHTtotalm+=$WHTtotal;

                     // To get the overal Total Tax  total;
                     $taxtotalm+=$taxtotal;
                     ?>
                   <!-- </td> -->
                   <td class="text-right"><?= number_format($Subtoal,2) ;?></td>
                   <td class="text-right"><?= number_format($VATtotal,2) ;?></td>
                   <td class="text-right"><?= number_format($WHTtotal,2) ;?></td>
                   <td class="text-right"><?= number_format($taxtotal,2) ;?></td>
                   <td class="text-right">

                     <?php
                     $malipo=$allExpense->allPayments($allExpense->transactionId,$allExpense->referenceTable);
                     $paymenttotalm+=$malipo;

                     $malipoamount = preg_replace(
                        '/(-)([\d\.\,]+)/ui',
                        '($2)',
                        number_format($malipo,2,'.',',')
                    );

                     echo $malipoamount;

                     ?>
                  </td>
                  <td>
                    <?php
                    $bal = ($Subtoal+$VATtotal+$WHTtotal+$taxtotal)-$malipo;
                    echo number_format($bal,2);
                    ?>
                  </td>
                </tr>
                <?php
              }
              // Bracjets if the figure is negatove
            $Subtoalmamount = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($Subtoalm,2,'.',',')
           );
           // Brackets if value is negative
           $VATtotalmamount = preg_replace(
              '/(-)([\d\.\,]+)/ui',
              '($2)',
              number_format($VATtotalm,2,'.',',')
          );
          // Brackets if value is negative
          $WHTtotalmamount = preg_replace(
             '/(-)([\d\.\,]+)/ui',
             '($2)',
             number_format($WHTtotalm,2,'.',',')
         );
         // Brackets if value is negative
         $taxtotalmamount = preg_replace(
            '/(-)([\d\.\,]+)/ui',
            '($2)',
            number_format($taxtotalm,2,'.',',')
        );
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <br>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
            <?php
            // $model->invoiceFooter; ?>
          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">


          <div class="col-md-8">
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <?php
           // $amountdue= $model->invoiceAmount-$payments;?>
          <p class="lead">Amount Due  <strong>Ksh
            <?php

            // $Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm;

            // Brackets if value is negative
            $Due = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm-$paymenttotalm,2,'.',',')
           );

            echo $Due ;?>
          </strong></p>

        </br>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Currency:</th>
                <th class="text-right">(Ksh)</th>
              </tr>
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td class="text-right"><?= $Subtoalmamount?></td>
              </tr>
              <tr>
                <th>VAT</th>
                <td class="text-right"><?= $VATtotalmamount ;?></td>
              </tr>
              <tr>
                <th>WHT</th>
                <td class="text-right"><?= $WHTtotalmamount?></td>
              </tr>
              <tr>
                <th>Other Tax</th>
                <td class="text-right"><?= $taxtotalmamount?></td>
              </tr>

              <tr>
                <th>Total:</th>
                <td class="text-right"><?php

                // $Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm;

                // Brackets if value is negative
                $totalmamount = preg_replace(
                   '/(-)([\d\.\,]+)/ui',
                   '($2)',
                   number_format($Subtoalm+$VATtotalm+$WHTtotalm+$taxtotalm,2,'.',',')
               );

                echo $totalmamount ;?></td>
              </tr>
              <tr>
                <th>Total Payment:</th>
                <td class="text-right">
                  <?php

                  // Brackets if value is negative
                  $totalPaymentmamount = preg_replace(
                     '/(-)([\d\.\,]+)/ui',
                     '($2)',
                     number_format($paymenttotalm,2,'.',',')
                 );

                  echo $totalPaymentmamount ;?>
                </td>
              </tr>

            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- /.row -->
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
              class="btn btn-xs btn-success m-b-10"><i
                   class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
        </button> -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  <?php
  } else {
    // code...

    echo "No Expense were found Here";
  }
  ?>
