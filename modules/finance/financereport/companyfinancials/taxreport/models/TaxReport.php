<?php

namespace app\modules\finance\financereport\companyfinancials\taxreport\models;

use Yii;

/**
 * This is the model class for table "tax_report".
 *
 * @property int $transactionId
 * @property int $referenceId
 * @property string $date
 * @property int $recipientId
 * @property string $transactionCode
 * @property string $parties
 * @property string $details
 * @property double $taxableamount
 * @property string $dr_amount
 * @property string $cr_amount
 * @property string $taxCode
 * @property double $taxRate
 * @property string $kraPin
 * @property string $typeofRecipeint
 * @property string $typeofTransaction
 */
class TaxReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tax_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transactionId', 'referenceId', 'recipientId'], 'integer'],
            [['date'], 'safe'],
            [['taxableamount', 'taxRate'], 'number'],
            [['transactionCode', 'taxCode', 'kraPin'], 'string', 'max' => 50],
            [['parties'], 'string', 'max' => 255],
            [['details'], 'string', 'max' => 363],
            [['dr_amount', 'cr_amount'], 'string', 'max' => 22],
            [['typeofRecipeint'], 'string', 'max' => 8],
            [['typeofTransaction'], 'string', 'max' => 18],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'transactionId' => 'Transaction ID',
            'referenceId' => 'Reference ID',
            'date' => 'Date',
            'recipientId' => 'Recipient ID',
            'transactionCode' => 'Transaction Code',
            'parties' => 'Parties',
            'details' => 'Details',
            'taxableamount' => 'Taxableamount',
            'dr_amount' => 'Dr Amount',
            'cr_amount' => 'Cr Amount',
            'taxCode' => 'Tax Code',
            'taxRate' => 'Tax Rate',
            'kraPin' => 'Kra Pin',
            'typeofRecipeint' => 'Typeof Recipeint',
            'typeofTransaction' => 'Typeof Transaction',
        ];
    }
}
