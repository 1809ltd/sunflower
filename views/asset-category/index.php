<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AssetCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asset Categories';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="asset-category-index">

                  <?php Pjax::begin(); ?>
                  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                  <p>
                      <?= Html::a('Add Category', ['create'], ['class' => 'btn btn-success']) ?>
                  </p>

                  <?= GridView::widget([
                      'dataProvider' => $dataProvider,
                      'filterModel' => $searchModel,
                      'columns' => [
                          ['class' => 'yii\grid\SerialColumn'],

                          // 'id',
                          'assetCategoryName',
                          'assetCategoryDescription:ntext',
                          'assetCategoryParentId',
                          'assetCategoryStatus',
                          //'userId',
                          //'createdAt',
                          //'updatedAt',
                          //'deletedAt',

                          ['class' => 'yii\grid\ActionColumn'],
                      ],
                  ]); ?>
                  <?php Pjax::end(); ?>
              </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
