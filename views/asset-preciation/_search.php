<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AssetPreciationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-preciation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'assetId') ?>

    <?= $form->field($model, 'companyId') ?>

    <?= $form->field($model, 'preciableCost') ?>

    <?= $form->field($model, 'preciationSalvageValue') ?>

    <?php // echo $form->field($model, 'assetLifeMonths') ?>

    <?php // echo $form->field($model, 'preciationDateAcquired') ?>

    <?php // echo $form->field($model, 'preciationDateAcquiredDepreciationMethod') ?>

    <?php // echo $form->field($model, 'preciationStatus') ?>

    <?php // echo $form->field($model, 'preciationTimestamp') ?>

    <?php // echo $form->field($model, 'preciationUpdatedAt') ?>

    <?php // echo $form->field($model, 'preciationDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
