<?php

namespace app\modules\user\userroletypes\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\userroletypes\models\UserRoleTypes;

/**
 * UserRoleTypesSearch represents the model behind the search form of `app\modules\user\userroletypes\models\UserRoleTypes`.
 */
class UserRoleTypesSearch extends UserRoleTypes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'createdBy', 'userId', 'status'], 'integer'],
            [['roleName', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserRoleTypes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'createdBy' => $this->createdBy,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'roleName', $this->roleName]);

        return $dataProvider;
    }
}
