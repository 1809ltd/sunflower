<?php

namespace app\modules\finance\income\invoice;

/**
 * invoice module definition class
 */
class invoice extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\income\invoice\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Invoice Items Sub Module*/
          'invoiceitems' => [
              'class' => 'app\modules\finance\income\invoice\invoiceitems\invoiceitems',
          ],
        ];

        // custom initialization code goes here
    }
}
