<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_items_category".
 *
 * @property int $id
 * @property string $financeItemCategoryName
 * @property string $financeItemCategoryDescription
 * @property int $financeItemCategoryParentId
 * @property int $financeItemCategoryStatus
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property FinanceItems[] $financeItems
 * @property UserDetails $user
 * @property FinanceItemsCategoryType[] $financeItemsCategoryTypes
 */
class FinanceItemsCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_items_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['financeItemCategoryName', 'financeItemCategoryStatus', 'userId'], 'required'],
            [['financeItemCategoryDescription'], 'string'],
            [['financeItemCategoryParentId', 'financeItemCategoryStatus', 'userId'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['financeItemCategoryName'], 'string', 'max' => 50],
            [['financeItemCategoryName'], 'unique'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'financeItemCategoryName' => 'Category Name',
            'financeItemCategoryDescription' => 'Category Description',
            'financeItemCategoryParentId' => 'Category Parent ID',
            'financeItemCategoryStatus' => 'Category Status',
            'userId' => 'User ID',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItems()
    {
        return $this->hasMany(FinanceItems::className(), ['itemCategory' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItemsCategoryTypes()
    {
        return $this->hasMany(FinanceItemsCategoryType::className(), ['financeItemCategoryId' => 'id']);
    }
}
