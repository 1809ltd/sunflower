<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\modules\finance\financereport\companyfinancials\models\AgedReceivables;



/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

?>
<section class="invoice">
  <div class="text-center">
    <h6 class="box-title">
      <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
    </h6>
    <h3 class="box-title">Aged Receivables</h3>
    <h5 class="box-title">As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h5>
    <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
  </div>

  <div class="box">
    <div class="box-body">
      <h5 class="box-title">REVENUE</h5>
      <table class="table  table-striped table-condensed">
        <thead>
          <tr>
            <th class="text-left">Receivables</th>
            <th class="text-right">Coming Due	</th>
            <th class="text-right">	1- 30 days</th>
            <th class="text-right">31- 60 days</th>
            <th class="text-right">61- 90 days</th>
            <th class="text-right">90 days</th>
            <th class="text-right">Unllocated</th>
            <th class="text-right">Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $agedreceiables= AgedReceivables::find()->all();

          $Sumcoming=0;
          $Sum30=0;
          $Sum60=0;
          $Sum90=0;
          $Sumc90=0;
          $Sumunlocated=0;
          $SumTT=0;



          //

          foreach ($agedreceiables as $agedreceiable) {
            // code...

            $agedreceiable->customerId;

            $total= $agedreceiable["Coming Due"] + $agedreceiable["1-30 Days"] + $agedreceiable["31-60 Days"] + $agedreceiable["61-90 Days"] + $agedreceiable[">90 Days"];

            $Sumcoming+= $agedreceiable["Coming Due"];
            $Sum30+= $agedreceiable["1-30 Days"];
            $Sum60+= $agedreceiable["31-60 Days"];
            $Sum90+= $agedreceiable["61-90 Days"];
            $Sumc90+= $agedreceiable[">90 Days"];
            $Sumunlocated+= $agedreceiable->unlocated;
            $SumTT+= $total;


            ?>
            <tr>

              <td class="text-left"><?= $agedreceiable->receivables;?></td>
              <td class="text-right"><?= number_format($agedreceiable["Coming Due"],2);?></td>
              <td class="text-right"><?= number_format($agedreceiable["1-30 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedreceiable["31-60 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedreceiable["61-90 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedreceiable[">90 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedreceiable->unlocated,2);?></td>
              <td class="text-right"><?= number_format($total,2);?></td>

            </tr>


            <?php


          }



           ?>

           <tr>

               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>

           </tr>
           <tr>

               <th class="text-left">Total Receivable</th>
               <td class="text-right"><?= number_format($Sumcoming,2);?></td>
               <td class="text-right"><?= number_format($Sum30,2);?></td>
               <td class="text-right"><?= number_format($Sum60,2);?></td>
               <td class="text-right"><?= number_format($Sum90,2);?></td>
               <td class="text-right"><?= number_format($Sumc90,2);?></td>
               <th class="text-right"><?= number_format($Sumunlocated,2);?></th>
               <th class="text-right"><?= number_format($SumTT,2);?></th>

           </tr>

         </tbody>
     </table>
   </br>

  </div>
</div>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <div class="col-xs-12">
    <a href="javascript:void(0)" onclick="window.print()"
       class="btn btn-xs btn-success m-b-10"><i
            class="fa fa-print m-r-5"></i> Print</a>

      <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
      <i class="fa fa-download"></i> Generate PDF -->
    <!-- </button> -->
  </div>
</div>
</section>
