<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceCreditMemoLines */

$this->title = 'Create Finance Credit Memo Lines';
$this->params['breadcrumbs'][] = ['label' => 'Finance Credit Memo Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-credit-memo-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
