<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfrom */

$this->title = 'Create Event Check List';
$this->params['breadcrumbs'][] = ['label' => 'Event Check List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-orderfrom-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsEventOrderfromList'=>$modelsEventOrderfromList, //to enable Dynamic Form
    ]) ?>

</div>
