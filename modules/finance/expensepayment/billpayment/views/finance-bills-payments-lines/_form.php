<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
// Bills to be paid
use app\modules\finance\expense\bills\models\FinanceBills;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\bill-payment\models\FinanceBillsPaymentsLines */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- <div class="finance-bills-payments-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'billpaymentId')->textInput() ?>

    <?= $form->field($model, 'billId')->textInput() ?>

    <?= $form->field($model, 'vendorId')->textInput() ?>

    <?= $form->field($model, 'referenceNo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> -->
<?php

/* @var $this yii\web\View */
/* @var $model app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-payment-lines-form">

</div>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body finance-payment-lines-form">

  <?= $form->field($model, 'billId')->widget(Select2::classname(), [
          'data' => ArrayHelper::map(FinanceBills::find()
                  ->where(['vendorId' => $_SESSION['vendorInvoiceId']])
                  ->Andwhere('`billStatus` != 0')
                  ->all(),'id','billRef'),
          'language' => 'en',
          'options' => ['placeholder' => 'Select a Vendor Bill ...'],
          'pluginOptions' => [
              'allowClear' => true
          ],
  ]);?>

  <?= $form->field($model, 'amount')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
  <?php
  // necessary for update action.
  if ($model->isNewRecord&&((!empty($_GET)))) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput(['value'=> $_GET["status"]])->label(false);
    echo  $form->field($model, 'billpaymentId')->hiddenInput(['value'=> $_GET["billpaymentId"]])->label(false);

  }elseif (!$model->isNewRecord) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput()->label(false);
    echo  $form->field($model, 'vendorId')->hiddenInput()->label(false);

  }else {
    // code...
    echo  $form->field($model, 'vendorId')->hiddenInput(['value'=>$_SESSION['vendorInvoiceId']])->label(false);
    echo  $form->field($model, 'status')->hiddenInput(['value'=>3])->label(false);

  }
  ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
