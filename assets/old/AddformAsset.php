<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AddformEventAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

       /*BEGIN BASE CSS STYLE*/

          /* BEGIN BASE CSS STYLE */
       'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
       'assets1/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css',
       'assets1/plugins/bootstrap/css/bootstrap.min.css',
       'assets1/css/style.css',
       'assets1/css/style-responsive.css',
       'assets1/css/default.css',
          /* END BASE CSS STYLE */

          /* BEGIN PAGE LEVEL STYLE */
       'assets1/plugins/select2/dist/css/select2.min.css',
       'assets1/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css',
       'assets1/plugins/parsley/src/parsley.css',
          /* END PAGE LEVEL STYLE */



    ];
    public $js = [
        /* BEGIN BASE JS */

        /* BEGIN BASE JS*/
        //'assets1/plugins/jquery/jquery-1.12.4.min.js',
        //'assets1/plugins/jquery/jquery-migrate-1.4.1.min.js',
        //'assets1/plugins/jquery-ui/ui/minified/jquery-ui.min.js',
        //'assets1/plugins/bootstrap/js/bootstrap.min.js',

        'assets1/plugins/slimscroll/jquery.slimscroll.min.js',
        'assets1/plugins/jquery-cookie/jquery.cookie.js',
        'assets1/plugins/jquery-cookie/js.cookie.js',
        'assets1/plugins/pace/pace.min.js',
        'assets1/plugins/chartjs/Chart.min.js',
        'assets1/plugins/typeit/typeit.js',
        'assets1/plugins/countup/countUp.min.js',
        'assets1/js/app.js',
        /* END BASE JS*/

        /* BEGIN PAGE LEVEL JS*/
        //'assets1/plugins/select2/dist/js/select2.min.js',
        //'assets1/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js',
        //'assets1/plugins/parsley/dist/parsley.js',
        /* END PAGE LEVEL JS*/



    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
