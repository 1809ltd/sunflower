<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_customer_purchase_order".
 *
 * @property int $id
 * @property string $purchaseOrderNumber
 * @property int $customerId
 * @property string $purchaseOrderProjectClassification
 * @property int $purchaseOrderProjectId
 * @property string $purchaseOrderProjectName
 * @property string $purchaseOrderNote
 * @property string $purchaseOrderTxnDate
 * @property string $purchaseOrderBillEmail
 * @property double $purchaseOrderDiscountAmount
 * @property double $purchaseOrderTaxAmount
 * @property double $purchaseOrderSubAmount
 * @property double $purchaseOrderAmount
 * @property string $purchaseOrderTxnStatus
 * @property int $purchaseOrderdCreatedby
 * @property string $purchaseOrderdCreatedAt
 * @property string $purchaseOrderdUpdatedAt
 * @property string $purchaseOrderdDeletedAt
 *
 * @property EventDetails $purchaseOrderProject
 * @property UserDetails $purchaseOrderdCreatedby0
 * @property FinanceCustomerPurchaseOrderLines[] $financeCustomerPurchaseOrderLines
 * @property FinanceInvoice[] $financeInvoices
 */
class FinanceCustomerPurchaseOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_customer_purchase_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchaseOrderNumber', 'customerId', 'purchaseOrderProjectClassification', 'purchaseOrderProjectId', 'purchaseOrderProjectName', 'purchaseOrderNote', 'purchaseOrderTxnDate','purchaseOrderDiscountAmount', 'purchaseOrderTaxAmount', 'purchaseOrderSubAmount', 'purchaseOrderAmount', 'purchaseOrderTxnStatus', 'purchaseOrderdCreatedby'], 'required'],
            [['customerId', 'purchaseOrderProjectId', 'purchaseOrderdCreatedby'], 'integer'],
            [['purchaseOrderNote', 'purchaseOrderBillEmail', 'purchaseOrderTxnStatus'], 'string'],
            [['purchaseOrderTxnDate', 'purchaseOrderdCreatedAt', 'purchaseOrderdUpdatedAt', 'purchaseOrderBillEmail',  'purchaseOrderdDeletedAt'], 'safe'],
            [['purchaseOrderDiscountAmount', 'purchaseOrderTaxAmount', 'purchaseOrderSubAmount', 'purchaseOrderAmount'], 'number'],
            [['purchaseOrderNumber'], 'string', 'max' => 50],
            [['purchaseOrderProjectClassification', 'purchaseOrderProjectName'], 'string', 'max' => 100],
            [['purchaseOrderNumber'], 'unique'],
            [['purchaseOrderProjectId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['purchaseOrderProjectId' => 'id']],
            [['purchaseOrderdCreatedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['purchaseOrderdCreatedby' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchaseOrderNumber' => 'Order Number',
            'customerId' => 'Customer',
            'purchaseOrderProjectClassification' => 'Project Classification',
            'purchaseOrderProjectId' => 'Project ID',
            'purchaseOrderProjectName' => 'Project Name',
            'purchaseOrderNote' => 'Note',
            'purchaseOrderTxnDate' => 'Date',
            'purchaseOrderBillEmail' => 'Email',
            'purchaseOrderDiscountAmount' => 'Discount Amount',
            'purchaseOrderTaxAmount' => 'Tax Amount',
            'purchaseOrderSubAmount' => 'Sub Amount',
            'purchaseOrderAmount' => 'Amount',
            'purchaseOrderTxnStatus' => 'Status',
            'purchaseOrderdCreatedby' => 'Createdby',
            'purchaseOrderdCreatedAt' => 'Created At',
            'purchaseOrderdUpdatedAt' => 'Updated At',
            'purchaseOrderdDeletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderProject()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'purchaseOrderProjectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderdCreatedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'purchaseOrderdCreatedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerPurchaseOrderLines()
    {
        return $this->hasMany(FinanceCustomerPurchaseOrderLines::className(), ['purchaseOrderId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoices()
    {
        return $this->hasMany(FinanceInvoice::className(), ['localpurchaseOrder' => 'id']);
    }
}
