<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventWorkplan;

/**
 * EventWorkplanSearch represents the model behind the search form of `app\models\EventWorkplan`.
 */
class EventWorkplanSearch extends EventWorkplan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'orderId', 'orderformId', 'personnelid', 'status', 'userId', 'approvedId'], 'integer'],
            [['personnelPhone', 'category', 'taskAssign', 'detail', 'createdAt', 'updateAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventWorkplan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'orderId' => $this->orderId,
            'orderformId' => $this->orderformId,
            'personnelid' => $this->personnelid,
            'status' => $this->status,
            'createdAt' => $this->createdAt,
            'updateAt' => $this->updateAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'approvedId' => $this->approvedId,
        ]);

        $query->andFilterWhere(['like', 'personnelPhone', $this->personnelPhone])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'taskAssign', $this->taskAssign])
            ->andFilterWhere(['like', 'detail', $this->detail]);

        return $dataProvider;
    }
}
