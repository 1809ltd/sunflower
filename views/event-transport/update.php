<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\event\transportation\models\EventTransport */

$this->title = 'Update Event Transport: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Transports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-transport-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
