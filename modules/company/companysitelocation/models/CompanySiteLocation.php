<?php

namespace app\modules\company\companysitelocation\models;


use app\modules\company\companysite\models\CompanySiteDetails;

use app\models\UserDetails;

use Yii;

/**
 * This is the model class for table "company_site_location".
 *
 * @property int $id
 * @property int $siteId
 * @property string $locationName
 * @property string $locationtimestamp
 * @property string $locationUpdatedAt
 * @property string $locationDeleteAt
 * @property int $status
 * @property int $userId
 *
 * @property AssetCheckIn[] $assetCheckIns
 * @property AssetSiteLocation[] $assetSiteLocations
 * @property AssetSiteLocation[] $assetSiteLocations0
 * @property CompanySiteDetails $site
 * @property UserDetails $user
 */
class CompanySiteLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_site_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['siteId', 'locationName', 'status', 'userId'], 'required'],
            [['siteId', 'status', 'userId'], 'integer'],
            [['locationtimestamp', 'locationUpdatedAt', 'locationDeleteAt'], 'safe'],
            [['locationName'], 'string', 'max' => 255],
            [['locationName'], 'unique'],
            [['siteId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanySiteDetails::className(), 'targetAttribute' => ['siteId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'siteId' => 'Site',
            'locationName' => 'Location Name',
            'locationtimestamp' => 'Locationtimestamp',
            'locationUpdatedAt' => 'Location Updated At',
            'locationDeleteAt' => 'Location Delete At',
            'status' => 'Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckIns()
    {
        return $this->hasMany(AssetCheckIn::className(), ['lcationId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetSiteLocations()
    {
        return $this->hasMany(AssetSiteLocation::className(), ['loactionId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetSiteLocations0()
    {
        return $this->hasMany(AssetSiteLocation::className(), ['siteId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(CompanySiteDetails::className(), ['id' => 'siteId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
