<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AssetDispose */

$this->title = $model->disposeId;
$this->params['breadcrumbs'][] = ['label' => 'Asset Disposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-dispose-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->disposeId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->disposeId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'disposeId',
            'assetId',
            'dateDisposed',
            'disposeTo',
            'disposal Reason:ntext',
            'disposeTimeStamp',
            'disposeUpdatedAt',
            'disposeDeletedAt',
            'userId',
        ],
    ]) ?>

</div>
