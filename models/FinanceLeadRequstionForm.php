<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_lead_requstion_form".
 *
 * @property int $id
 * @property string $requstionNumber
 * @property int $leadId
 * @property string $date
 * @property int $requstionStatus
 * @property string $requstionNote
 * @property double $amount
 * @property int $preparedBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $approvedBy
 *
 * @property UserDetails $preparedBy0
 * @property UserDetails $user
 * @property UserDetails $approvedBy0
 * @property CustomerLead $lead
 * @property FinanceLeadRequstionFormList[] $financeLeadRequstionFormLists
 */
class FinanceLeadRequstionForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_lead_requstion_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['requstionNumber', 'leadId', 'date', 'requstionStatus', 'amount', 'preparedBy', 'userId'], 'required'],
            [['leadId', 'requstionStatus', 'preparedBy', 'userId', 'approvedBy'], 'integer'],
            [['date', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['requstionNote'], 'string'],
            [['amount'], 'number'],
            [['requstionNumber'], 'string', 'max' => 100],
            [['requstionNumber'], 'unique'],
            [['preparedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['preparedBy' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerLead::className(), 'targetAttribute' => ['leadId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'requstionNumber' => 'Requstion Number',
            'leadId' => 'Lead ID',
            'date' => 'Date',
            'requstionStatus' => 'Requstion Status',
            'requstionNote' => 'Requstion Note',
            'amount' => 'Amount',
            'preparedBy' => 'Prepared By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'approvedBy' => 'Approved By',
        ];
    }
      public function getRQnumber()
      {
          //select product code

          $connection = Yii::$app->db;
          $query= "Select MAX(requstionNumber) AS number from finance_lead_requstion_form where id>0";
          $rows= $connection->createCommand($query)->queryAll();

          if ($rows) {
            // code...

            $number=$rows[0]['number'];
            $number++;
            if ($number == 1) {
              // code...
              $number =   "LRQEV-".date('Y')."-".date('m')."-0001";
            }
            if ($number == 1) {
              // code...
              $number = "LRQEV-".date('Y')."-".date('m')."-0001";
            }
          } else {
            // code...
            //generating numbers
            $number = "LRQEV-".date('Y')."-".date('m')."-0001";
          }

        return $number;
      }

      public function geteventDetailss($id)
      {
        return CustomerLead::find()
          ->where(['and', "id=$id"])
          ->all();
      }

      public function getFinanceLeadRequstionFormList($id)
      {
        return FinanceLeadRequstionFormList::find()
          ->where(['and', "reqId=$id"])
          ->all();
      }
      public function getActivity($id)
      {
        return FinanceEventActivity::find()
          ->where(['and', "id=$id"])
          ->all();
      }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreparedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'preparedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(CustomerLead::className(), ['id' => 'leadId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceLeadRequstionFormLists()
    {
        return $this->hasMany(FinanceLeadRequstionFormList::className(), ['reqId' => 'id']);
    }
}
