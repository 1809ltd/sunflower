<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequisitionPayments */

$this->title = 'Create Finance Lead Requisition Payments';
$this->params['breadcrumbs'][] = ['label' => 'Finance Lead Requisition Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-lead-requisition-payments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
