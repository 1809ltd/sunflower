<?php

namespace app\modules\finance\expense\officeexpense\officeexpenselines\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;
use app\modules\finance\expense\officeexpense\models\FinanceOfficeRequstionForm;

use app\modules\personnel\personneldetails\models\PersonnelDetails;

use Yii;

/**
 * This is the model class for table "finance_office_requstion_form_list".
 *
 * @property int $id
 * @property int $reqId
 * @property int $activityId
 * @property int $personelId
 * @property string $personnelTel
 * @property string $description
 * @property double $qty
 * @property double $unitprice
 * @property double $amont
 * @property int $status
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 *
 * @property FinanceOfficeRequstionForm $req
 * @property FinanceEventActivity $activity
 * @property PersonnelDetails $personel
 * @property UserDetails $user
 */
class FinanceOfficeRequstionFormList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_office_requstion_form_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
          [['activityId','qty', 'unitprice', 'amont'], 'required'],
          [['reqId', 'activityId', 'personelId', 'status', 'userId'], 'integer'],
          [['description'], 'string'],
          [['qty', 'unitprice', 'amont'], 'number'],
          [['reqId',  'personnelTel', 'status', 'userId', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
          [['personnelTel'], 'string', 'max' => 15],
          [['reqId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceOfficeRequstionForm::className(), 'targetAttribute' => ['reqId' => 'id']],
          [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEventActivity::className(), 'targetAttribute' => ['activityId' => 'id']],
          [['personelId'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelDetails::className(), 'targetAttribute' => ['personelId' => 'id']],
          [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
      ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reqId' => 'Req ID',
            'activityId' => 'Expense Activity',
            'personelId' => 'Personel',
            'personnelTel' => 'Personnel Tel',
            'description' => 'Description',
            'qty' => 'Qty',
            'unitprice' => 'Unitprice',
            'amont' => 'Amount',
            'status' => 'Status',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReq()
    {
        return $this->hasOne(FinanceOfficeRequstionForm::className(), ['id' => 'reqId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(FinanceEventActivity::className(), ['id' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonel()
    {
        return $this->hasOne(PersonnelDetails::className(), ['id' => 'personelId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
