<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrder */

$this->title = 'Update Finance Customer Purchase Order: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Customer Purchase Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-customer-purchase-order-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceCustomerPurchaseOrderLines'=>$modelsFinanceCustomerPurchaseOrderLines, //to enable Dynamic Form
    ]) ?>

</div>
