<?php

namespace app\controllers;

use Yii;
use app\models\FinanceCreditMemo;
use app\models\FinanceCreditMemoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\models\FinanceCreditMemoLines;
use app\models\FinanceCreditMemoLinesSearch;


/**
 * FinanceCreditMemoController implements the CRUD actions for FinanceCreditMemo model.
 */
class FinanceCreditMemoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->creditMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->creditMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceCreditMemo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new FinanceCreditMemoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceCreditMemo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceCreditMemo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new FinanceCreditMemo();

        $modelsFinanceCreditMemoLines = [new FinanceCreditMemoLines];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsFinanceCreditMemoLines = Model::createMultiple(FinanceCreditMemoLines::classname());
          Model::loadMultiple($modelsFinanceCreditMemoLines, Yii::$app->request->post());

          // ajax validation
          /*
          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFinanceCreditMemoLines),
                    ActiveForm::validate($model)
                );
            }*/

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceCreditMemoLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceCreditMemoLines as $modelFinanceCreditMemoLines) {

                        $modelFinanceCreditMemoLines->creditMemoId = $model->id;
                        $modelFinanceCreditMemoLines->userId = $model->userId;
                        $modelFinanceCreditMemoLines->creditMemoLineStatus = $model->creditMemoStatus;

                          if (! ($flag = $modelFinanceCreditMemoLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }


        }

        return $this->render('create', [
            'model' => $model,
              'modelsFinanceCreditMemoLines' => (empty($modelsFinanceCreditMemoLines)) ? [new FinanceCreditMemoLines] : $modelsFinanceCreditMemoLines
        ]);


        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Updates an existing FinanceCreditMemo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        $modelsFinanceCreditMemoLines = $this->getFinanceCreditMemoLines($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $oldIDs = ArrayHelper::map($modelsFinanceCreditMemoLines, 'id', 'id');
          $modelsFinanceCreditMemoLines = Model::createMultiple(FinanceCreditMemoLines::classname(), $modelsFinanceCreditMemoLines);
          Model::loadMultiple($modelsFinanceCreditMemoLines, Yii::$app->request->post());
          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceCreditMemoLines, 'id', 'id')));

          // ajax validation
          if (Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ArrayHelper::merge(
                  ActiveForm::validateMultiple($modelsFinanceCreditMemoLines),
                  ActiveForm::validate($model)
              );
          }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceCreditMemoLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      if (! empty($deletedIDs)) {
                          FinanceCreditMemoLines::deleteAll(['id' => $deletedIDs]);
                      }
                      foreach ($modelsFinanceCreditMemoLines as $modelFinanceCreditMemoLines) {
                          $modelFinanceCreditMemoLines->creditMemoId = $model->id;
                          if (! ($flag = $modelFinanceCreditMemoLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
              }
          }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsFinanceCreditMemoLines' => (empty($modelsFinanceCreditMemoLines)) ? [new FinanceCreditMemoLines] : $modelsFinanceCreditMemoLines
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('update', [
        //     'model' => $model,
        // ]);
    }
    public function getFinanceCreditMemoLines($id)
    {
      $model = FinanceCreditMemoLines::find()->where(['creditMemoId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing FinanceCreditMemo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout='addformdatatablelayout';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceCreditMemo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceCreditMemo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id)
    {
        if (($model = FinanceCreditMemo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
