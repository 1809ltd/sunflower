<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceBills */

$this->title = 'Record Bills';
$this->params['breadcrumbs'][] = ['label' => 'Record', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-bills-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceBillsLines'=>$modelsFinanceBillsLines, //to enable Dynamic Form
        
    ]) ?>

</div>
