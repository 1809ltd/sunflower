<?php

namespace app\controllers;

use Yii;
use app\models\VendorCompanyDetails;
use app\models\VendorCompanyDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VendorCompanyDetailsController implements the CRUD actions for VendorCompanyDetails model.
 */
class VendorCompanyDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->vendorCompanyupdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->vendorCompanyupdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all VendorCompanyDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new VendorCompanyDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VendorCompanyDetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VendorCompanyDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new VendorCompanyDetails();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing VendorCompanyDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VendorCompanyDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout='addformdatatablelayout';

        // $this->findModel($id)->delete();

        $this->findModel($id)->updateAttributes(['vendorCompanyStatus'=>0,'vendorCompanyDeletedAt' => new \yii\db\Expression('NOW()')]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the VendorCompanyDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VendorCompanyDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VendorCompanyDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
