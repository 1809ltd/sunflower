<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_category".
 *
 * @property int $categoryId
 * @property int $companyId
 * @property string $categoryName
 * @property string $categoryCode
 * @property string $categoryTimestamp
 * @property string $categoryUpdatedAt
 * @property string $categoryDeleteAt
 * @property int $categoryStatus
 * @property int $userId
 *
 * @property AssetSiteLocation[] $assetSiteLocations
 * @property CompanyDetails $company
 * @property UserDetails $user
 */
class CompanyCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyId', 'categoryName', 'categoryCode', 'categoryStatus', 'userId'], 'required'],
            [['companyId', 'categoryStatus', 'userId'], 'integer'],
            [['categoryTimestamp', 'categoryUpdatedAt', 'categoryDeleteAt'], 'safe'],
            [['categoryName'], 'string', 'max' => 255],
            [['categoryCode'], 'string', 'max' => 50],
            [['companyId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyDetails::className(), 'targetAttribute' => ['companyId' => 'companyId']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'userId']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'categoryId' => 'Category',
            'companyId' => 'Company',
            'categoryName' => 'Category Name',
            'categoryCode' => 'Category Code',
            'categoryTimestamp' => 'Category Timestamp',
            'categoryUpdatedAt' => 'Category Updated At',
            'categoryDeleteAt' => 'Category Delete At',
            'categoryStatus' => 'Category Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetSiteLocations()
    {
        return $this->hasMany(AssetSiteLocation::className(), ['categoryId' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(CompanyDetails::className(), ['companyId' => 'companyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['userId' => 'userId']);
    }
}
