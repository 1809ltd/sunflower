<?php

namespace app\modules\finance\creditMemo\vendorcreditMemo\models;


/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Bills*/
use app\modules\finance\expense\bills\models\FinanceBills;

use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Accounts */


use Yii;

/**
 * This is the model class for table "finance_vendor_credit_memo".
 *
 * @property int $id
 * @property int $vendorCreditMemoAccount
 * @property string $refernumber
 * @property string $vendorCreditMemoTxnDate
 * @property string $vendorCreditMemoNote
 * @property int $vendorId
 * @property double $creditAmount
 * @property double $totalTax
 * @property int $vendorCreditMemoStatus
 * @property string $vendorCreditMemoCreatedAt
 * @property string $vendorCreditMemoUpdatedAt
 * @property string $vendorCreditMemoDeletedAt
 * @property int $userId
 *
 * @property UserDetails $user
 * @property VendorCompanyDetails $vendor
 * @property FinanceVendorCreditMemoLines[] $financeVendorCreditMemoLines
 */
class FinanceVendorCreditMemo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_vendor_credit_memo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorCreditMemoAccount', 'refernumber', 'vendorCreditMemoTxnDate', 'vendorId', 'creditAmount', 'totalTax', 'vendorCreditMemoStatus', 'userId'], 'required'],
            [['vendorCreditMemoAccount', 'vendorId', 'vendorCreditMemoStatus', 'userId'], 'integer'],
            [['vendorCreditMemoTxnDate', 'vendorCreditMemoCreatedAt', 'vendorCreditMemoUpdatedAt', 'vendorCreditMemoDeletedAt'], 'safe'],
            [['vendorCreditMemoNote'], 'string'],
            [['creditAmount', 'totalTax'], 'number'],
            [['refernumber'], 'string', 'max' => 50],
            [['refernumber'], 'unique'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorCreditMemoAccount' => 'Account',
            'refernumber' => 'Refernumber',
            'vendorCreditMemoTxnDate' => 'Date',
            'vendorCreditMemoNote' => 'Note',
            'vendorId' => 'Supplier',
            'creditAmount' => 'Credit Amount',
            'totalTax' => 'Total Tax',
            'vendorCreditMemoStatus' => 'Status',
            'vendorCreditMemoCreatedAt' => 'Created At',
            'vendorCreditMemoUpdatedAt' => 'Updated At',
            'vendorCreditMemoDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemoLines()
    {
        return $this->hasMany(FinanceVendorCreditMemoLines::className(), ['vendorCreditMemoId' => 'id']);
    }
}
