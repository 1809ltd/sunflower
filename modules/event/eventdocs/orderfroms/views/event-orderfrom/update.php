<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfrom */

$this->title = 'Update Event Check-List: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Check-List', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-orderfrom-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsEventOrderfromList'=>$modelsEventOrderfromList, //to enable Dynamic Form
    ]) ?>

</div>
