<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCheckIn */

$this->title = 'Update Asset Check In: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Check Ins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-check-in-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
