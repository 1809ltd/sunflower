<?php

namespace app\modules\finance\expense\bills\billslines\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expense\bills\billslines\models\FinanceBillsLines;

/**
 * FinanceBillsLinesSearch represents the model behind the search form of `app\modules\finance\expense\bills\billslines\models\FinanceBillsLines`.
 */
class FinanceBillsLinesSearch extends FinanceBillsLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'biilsId', 'eventActivityId', 'billsLinesBillableStatus', 'userId'], 'integer'],
            [['biilsLinesVendorsRef', 'biilsLinesDescription', 'billsLinesCreatedAt', 'billsLinesDeleteAt', 'billsLinesUpdatedAt'], 'safe'],
            [['biilsLinespacks', 'biilsLinesQty', 'billlinePriceperunit', 'biilsLinesAmount', 'billLinesTaxCodeRef', 'biilsLinesTaxAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBillsLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'biilsId' => $this->biilsId,
            'biilsLinespacks' => $this->biilsLinespacks,
            'biilsLinesQty' => $this->biilsLinesQty,
            'billlinePriceperunit' => $this->billlinePriceperunit,
            'biilsLinesAmount' => $this->biilsLinesAmount,
            'billLinesTaxCodeRef' => $this->billLinesTaxCodeRef,
            'biilsLinesTaxAmount' => $this->biilsLinesTaxAmount,
            'eventActivityId' => $this->eventActivityId,
            'billsLinesBillableStatus' => $this->billsLinesBillableStatus,
            'billsLinesCreatedAt' => $this->billsLinesCreatedAt,
            'billsLinesDeleteAt' => $this->billsLinesDeleteAt,
            'billsLinesUpdatedAt' => $this->billsLinesUpdatedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'biilsLinesVendorsRef', $this->biilsLinesVendorsRef])
            ->andFilterWhere(['like', 'biilsLinesDescription', $this->biilsLinesDescription]);

        return $dataProvider;
    }
}
