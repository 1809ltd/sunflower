<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AssetRegistrationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-registration-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'assetName') ?>

    <?= $form->field($model, 'assetDescription') ?>

    <?= $form->field($model, 'assetCategory') ?>

    <?= $form->field($model, 'assetCategoryType') ?>

    <?php // echo $form->field($model, 'assetTag') ?>

    <?php // echo $form->field($model, 'assetpurchaseDate') ?>

    <?php // echo $form->field($model, 'vendor') ?>

    <?php // echo $form->field($model, 'assetCost') ?>

    <?php // echo $form->field($model, 'assetBrand') ?>

    <?php // echo $form->field($model, 'assetModel') ?>

    <?php // echo $form->field($model, 'assetSerialNumber') ?>

    <?php // echo $form->field($model, 'assetQrCode') ?>

    <?php // echo $form->field($model, 'assetphoto') ?>

    <?php // echo $form->field($model, 'assetTimestamp') ?>

    <?php // echo $form->field($model, 'assetUpdatedAt') ?>

    <?php // echo $form->field($model, 'assetDeletedAt') ?>

    <?php // echo $form->field($model, 'assetStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
