<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItems */

$this->title = 'Update Finance Items: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-items-update">
  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
