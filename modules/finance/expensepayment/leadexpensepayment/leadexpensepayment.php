<?php

namespace app\modules\finance\expensepayment\leadexpensepayment;

/**
 * leadexpensepayment module definition class
 */
class leadexpensepayment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expensepayment\leadexpensepayment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
