<?php

namespace app\modules\user\userdetails;

/**
 * userdetails module definition class
 */
class userdetails extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\user\userdetails\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
      
        // custom initialization code goes here
    }
}
