<?php

namespace app\modules\finance\financesetup\coa\controllers;

use Yii;
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceAccountsController implements the CRUD actions for FinanceAccounts model.
 */
class FinanceAccountsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinanceAccounts models.
     * @return mixed
     */
    public function actionIndex()
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $searchModel = new FinanceAccountsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceAccounts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceAccounts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $model = new FinanceAccounts();
        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FinanceAccounts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $model = $this->findModel($id);
        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FinanceAccounts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      // $this->layout='addformdatatablelayout';
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        // $this->findModel($id)->delete();
        $this->findModel($id)->updateAttributes(['accountsStatus'=>0,'accountsDeleteAt' => new \yii\db\Expression('NOW()')]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceAccounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceAccounts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceAccounts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
