<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBills */
// Getting Bills $bills Items
use app\modules\finance\expense\bills\billslines\models\FinanceBillsLines;
use app\modules\finance\expense\bills\billslines\models\FinanceBillsLinesSearch;

$this->title = $model->billRef;
$this->params['breadcrumbs'][] = ['label' => 'Finance Bills', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/*All Payment for this invoice, i.e offset,creditnote,actualpayment*/
$payments=$model->allPayments($model->id);


?>
<div class="finance-bills-view no-print">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>
<!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
        <i class="fa fa-globe"></i> Vendor Bill
        <small class="pull-right"> As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?>
        </small>
      </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong><?= $model["vendor"]["vendorCompanyName"];?>.</strong><br>
            Kra Pin <?= $model["vendor"]["vendorKraPin"];?><br>
            <?= $model["vendor"]["vendorCompanyAddress"];?><br>
            Phone: <?= $model["vendor"]["vendorPhone"];?><br>
            Email: <?= $model["vendor"]["vendoremail"];?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          Contact:
          <address>
            <strong><?= $model["vendor"]["vendorContactPersonFullName"];?></strong><br>
            <!-- 795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br> -->
            Phone: <?= $model["vendor"]["contactMobilePhone"];?><br>
            Email: <?= $model["vendor"]["contactEmail"];?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #<?= $model->billRef;?></b><br>
          <br>
          <!-- <b>Order ID:</b> 4F3S8J<br> -->
          <b>Payment Due:</b> <?= Yii::$app->formatter->asDate($model->billDueDate, 'long');?><br>
          <!-- <b>Account:</b> 968-34567 -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>

              <th>Qty</th>
              <th>Service & Description</th>
              <th>Days</th>
              <th>Price</th>
              <!-- <th>Discount(%)</th> -->
              <th>Tax Amount</th>
              <th>Sub Amount</th>
            </thead>
            <tbody>
              <?php
              $billslines= FinanceBillsLines::find()
                              ->where(['billsLinesBillableStatus'=>$model->billStatus])
                              ->Andwhere(['biilsId'=>$model->id])
                              // ->orderBy(
                              //   [
                              //     new \yii\db\Expression('COALESCE(accountsClassification,accountParentId),accountsClassification,accountParentId,accountNumber')
                              //   ]
                              //   )
                              ->all();
                // print_r($billslines);

                $subtotal=0;
                $taxtotla=0;

                foreach ($billslines as $billslines) {
                  // code...

                  $subtotal=$subtotal+$billslines["biilsLinesAmount"];
                  $taxtotla= $taxtotla+$billslines["biilsLinesTaxAmount"];

                  ?>
                  <tr>
                    <td><?= $billslines["biilsLinesQty"] ?></td>
                    <td> <?= $billslines["biilsLinesVendorsRef"] ?></br>
                      <?= $billslines["biilsLinesDescription"] ?></td>
                    <td><?= $billslines["biilsLinespacks"] ?></td>
                    <td>Ksh <?= number_format($billslines["billlinePriceperunit"],2) ;?></td>
                    <td><?= number_format($billslines["biilsLinesTaxAmount"],2) ;?> </td>
                    <td>Ksh <?= number_format($billslines["biilsLinesAmount"],2) ;?></td>
                  </tr>
                  <?php

                }

               ?>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <!-- <p class="lead">Payment Methods:</p> -->
          <!-- <img src="../../dist/img/credit/visa.png" alt="Visa">
          <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="../../dist/img/credit/american-express.png" alt="American Express">
          <img src="../../dist/img/credit/paypal2.png" alt="Paypal"> -->

          <!-- <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;"> -->
            <!-- Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. -->
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td class="text-right"><?= number_format($subtotal,2) ;?></td>
              </tr>
              <tr>
                <th>Tax</th>
                <td class="text-right"><?= number_format($taxtotla,2) ;?></td>
              </tr>
              <tr>
                <!-- <th>Shipping:</th>
                <td>$5.80</td> -->
              </tr>
              <tr>
                <th>Total:</th>
                <?php $total=$subtotal+$taxtotla?>
                <td class="text-right"><?= number_format($total,2) ;?></td>
              </tr>

              <tr>
                <th>Total Payment:</th>
                <td class="text-right"><?= number_format($payments,2) ;?></td>
              </tr>
              <tr>
                <th>Amount Due:</th>
                <?php $amountdue= +$taxtotla+$subtotal-$payments;?>
                <!-- <p class="lead">Amount Due <strong>Ksh <?= number_format($amountdue,2) ;?></strong></p> -->
                <td class="text-right"><?= number_format($amountdue,2) ;?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
                   class="btn btn-xs btn-success m-b-10"><i
                        class="fa fa-print m-r-5"></i> Print</a>
        </div>
      </div>
    </section>
    <!-- /.content -->
