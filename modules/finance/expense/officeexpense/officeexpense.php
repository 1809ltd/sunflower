<?php

namespace app\modules\finance\expense\officeexpense;

/**
 * officeexpense module definition class
 */
class officeexpense extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\officeexpense\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->modules =[
          /*Fiance office expense Lines Sub Module*/
          'officeexpenselines' => [
              'class' => 'app\modules\finance\expense\officeexpense\officeexpenselines\officeexpenselines',
          ],
        ];

        // custom initialization code goes here
    }
}
