s<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerDetails */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-details-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>

<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="logo/rsz_sunflowerlogo.png" alt="User profile picture">

          <h3 class="profile-username text-center"><?= $model->customerCompanyName;?></h3>

          <p class="text-muted text-center">Contact:  <?= $model->customerFullname;?></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Pending Invoice</b> <a class="pull-right">...</a>
            </li>
            <li class="list-group-item">
              <b>Upcoming Event</b> <a class="pull-right">..</a>
            </li>
            <li class="list-group-item">
              <b>Upcoming Meetings</b> <a class="pull-right">..</a>
            </li>
          </ul>

          <a href="#" class="btn btn-primary btn-block"><b>..</b></a>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <!-- About Me Box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">About Customer</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> KRA Pin</strong>

          <p class="text-muted">
            <?= $model->customerKraPin;?>
          </p>

          <hr>

          <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

          <p class="text-muted">
          Suite: <?= $model->customerSuite;?><br>
          County: <?= $model->customerCounty;?><br>
          City: <?= $model->customercity;?><br>
          Country: <?= $model->customerCountry;?><br>
          Address: <?= $model->customerAddress;?><br>
          Postal Code: <?= $model->customerPostalCode;?>
          </p>

          <hr>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
          <li><a href="#timeline" data-toggle="tab">Upcoming Event</a></li>
          <li><a href="#settings" data-toggle="tab">Details</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="activity">
            <!-- Post -->

            ....

          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="timeline">
            <!-- The timeline -->
            <ul class="timeline timeline-inverse">
              <!-- timeline time label -->
                <li class="time-label">
                    <span class="bg-red">
                    </span>
                </li>
                <!-- /.timeline-label -->
              <!-- timeline time label -->
              <?php $eventcustomerLists=$model->getCustomerListEvent($model->id);


              foreach ($eventcustomerLists as $eventcustomerList) {
                // code...

                $eventcustomerList->customerId;
                $eventcustomerList->eventNumber;
                $eventcustomerList->eventName;
                $eventcustomerList->eventTheme;
                $eventcustomerList->eventVistorsNumber;
                $eventcustomerList->eventCategory;
                $eventcustomerList->eventStartDate;
                $eventcustomerList->eventEndDate;
                $eventcustomerList->eventSetUpDateTime;
                $eventcustomerList->eventSetDownDateTime;
                $eventcustomerList->eventLocation;
                $eventcustomerList->eventDescription;
                $eventcustomerList->eventNotes;
                $eventcustomerList->eventCreatedAt;
                $eventcustomerList->eventLastUpdatedAt;
                $eventcustomerList->eventDeletedAt;
                $eventcustomerList->userId;

                ?>


                  <!-- END timeline item -->

                <?php } ?>
              <li>
                <i class="fa fa-clock-o bg-gray"></i>
              </li>
            </ul>

          </div>
          <!-- /.tab-pane -->

          <div class="tab-pane" id="settings">
            <form class="form-horizontal">

              <?= DetailView::widget([
                  'model' => $model,
                  'attributes' => [
                    'id',
                    'customerNumber',
                    'customerFullname',
                    'customerCompanyName',
                    'customerKraPin',
                    'customerDisplayName',
                    'customerPrimaryPhone',
                    'customerPrimaryEmail:email',
                    'customerstatus',
                    'customerBillWithParent',
                    'customerParentId',
                    'customercity',
                    'customerSuite',
                    'customerCounty',
                    'customerCountry',
                    'customerPostalCode',
                    'customerAddress',
                    'customerCreateTime',
                    'customerUpdatedAt',
                    // 'customerDeleteAt',
                    // 'userId',
                  ],
              ]) ?>

            </form>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</section>
<!-- /.content -->
