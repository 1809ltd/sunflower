<?php

namespace app\modules\finance\receipt\invoicepayment\controllers;

use Yii;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLinesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinancePaymentLinesController implements the CRUD actions for FinancePaymentLines model.
 */
class FinancePaymentLinesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinancePaymentLines models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinancePaymentLinesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinancePaymentLines model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinancePaymentLines model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = new FinancePaymentLines();

      //Geting the time created Timestamp
      $model->created_at= new \yii\db\Expression('NOW()');
      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
        // return $this->redirect(['view', 'id' => $model->id]);

        if (!empty($model->invoicepaymentId)) {
          // code...
          return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/update', 'id' => $model->invoicepaymentId]);

        } else {
          // code...
          return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/create']);
        }
      }
      return $this->renderAjax('create', [
        'model' => $model,
      ]);

    }

    /**
     * Updates an existing FinancePaymentLines model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);

            if (!empty($model->invoicepaymentId)) {
              // code...
              return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/update', 'id' => $model->invoicepaymentId]);

            } else {
              // code...
              return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/create']);

            }

            // return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/create']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing FinancePaymentLines model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();
        //
        // return $this->redirect(['index']);
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        // Update all the data based on the payment made and status
        $this->findModel($id)->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);
        // Get the parent Id to determin if its and update or delete
        $invoicepaymentId = FinancePaymentLines::find()->where(['id' => $id])->one();

        if (!empty($invoicepaymentId["invoicepaymentId"])) {
            // code...
            return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/update', 'id' => $invoicepaymentId]);

          } else {
            // code...
            return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/create']);

          }

    }

    /**
     * Finds the FinancePaymentLines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinancePaymentLines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinancePaymentLines::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
