<?php
use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\select2\Select2;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;
// use  yii\bootstrap\Modal;
// Payment Model
use app\modules\finance\receipt\invoicepayment\models\FinancePayment;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentSearch;
// Invoices Paid
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLinesSearch;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\receipt\invoicepayment\models\FinancePayment */

$this->title = 'Record Invoice Payment';
$this->params['breadcrumbs'][] = ['label' => 'Finance Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="finance-payment-create">

    <?php
    //  $this->render('_form', [
    //     'model' => $model,
    // ]) ?>

</div>
<?php

if (isset($_SESSION['customerInvoiceId'])) {
  // code...
  // If Session of the customer is set based on the search
  $customerInvoiceId = $_SESSION['customerInvoiceId'];
  $modelCustomer = CustomerDetails::find()
          ->where('id = :customerId', [':customerId' => $customerInvoiceId])
          ->one();
  ?>
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Customer Details</h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-striped table-condensed">
          <thead>
            <tr>
              <th style="width: 40%;">Title</th>
              <th style="width: 60%;">Detail</th>
            </tr>
          </thead>
          <tbody>
            <tr><td><span>Customer Number:</span></td><td><?= $modelCustomer->customerNumber;?> </td></tr>
            <tr><td><span>Customer Name :</span></td><td> <?= $modelCustomer->customerCompanyName;?> </td></tr>
            <tr><td><span>KRA Pin :</span></td><td><?= $modelCustomer->customerKraPin;?> </td></tr>
            <tr><td><span>Customer Address :</span></td><td><?= $modelCustomer->customerAddress;?> </td></tr>
            <tr><td><span>Customer Phone:</span></td><td><?= $modelCustomer->customerPrimaryPhone;?> </td></tr>
            <tr><td><span>Customer Email:</span></td><td><?= $modelCustomer->customerPrimaryEmail;?> </td></tr>
            <tr><td><span>Customer Contact:</span></td><td><?= $modelCustomer->customerFullname;?> </td></tr>
            <tr><td colspan="2"><div class="col-md-2">
              <div class="form-actions">
                <a href="<?=Url::to(['/finance/receipt/invoicepayment/default/closesearch']) ?>" class="btn btn-warning btn-sm pull-left text-center" >Close Search</a>
              </div>
            </div></td></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- Payments on Hold -->
  <div class="col-md-8">
    <?php
    // This is to get all the Invoices from Client whose Payments that are on hold
    $modelsFinancePaymentLines1 = new FinancePayment();
    $output= $modelsFinancePaymentLines1->getallpaymentslinesonhold($customerInvoiceId);

    //Calling the forms with the list of payments on hold plus passing the variables for form
    echo $this->render('_holdbillpaymentsines', [
      // This is th model for the Bill Payments
      'model' => $model,
      // This is the result of the clients hold payments that are not yet assigned
      'output' => $output,
      ])
    ?>

  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Invoice Payment Receipt</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <?php
         // To get only payment from the client
         $searchModel = new FinancePaymentSearch();
         $searchModel->customerId = $modelCustomer->customerDisplayName;
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        ?>
         <div class="box-body finance-payment-index">
           <?php Pjax::begin(); ?>
           <?php // echo $this->render('_search', ['model' => $searchModel]);
           ?>
           <?= GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'rowOptions'=> function($model)
              {
                // code...
                if ($model->paymentstatus ==1) {
                  // code...
                  return['class'=>'success'];
                } elseif ($model->paymentstatus==2) {
                  // code...

                  return['class'=>'warning'];
                }else {
                  // code...
                  return['class'=>'danger'];
                }

              },
              'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                ['class' => 'kartik\grid\ExpandRowColumn',
                  'value'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    return GridView::ROW_COLLAPSED;
                  },
                  'detail'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    $searchModel = new FinancePaymentLinesSearch();
                    $searchModel->invoiceId = $model->id;
                    $searchModel->status = $model->paymentstatus;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return Yii::$app->controller->renderPartial('_paymentlines',[
                      'searchModel' => $searchModel,
                      'dataProvider' => $dataProvider,

                    ]);
                  },
              ],

                  // 'id',
                  // 'customerId',
                  [
                    // 'attribute'=>'customerId',
                    'value'=>'customer.customerDisplayName',
                  ],
                  'invoicePaymentinvoiceRef',
                  [
                    'attribute'=>'invoicePaymentaccount',
                    'value'=>'invoicePaymentaccount0.fullyQualifiedName',
                  ],
                  // 'invoicePaymentaccount',
                  'invoicePaymentPayType',
                  'recepitNumber',
                  'invoicePaymentTotalAmt',
                  'unallocatedAmount',
                  //'date',
                  //'invoicePaymentPrivateNote:ntext',
                  //'invoicePaymentCreatedAt',
                  //'invoicePaymentUpdatedAt',
                  //'invoicePaymentDeletedAt',
                  //'userId',
                  //'createdBy',
                  //'approvedBy',
                  //'paymentstatus',

                  // ['class' => 'yii\grid\ActionColumn'],
                  [
                      'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                      'items' => [
                          [
                              'label' => 'View',
                              'url'   => ['view'],
                          ],
                          [
                              'label' => 'Update',
                              'url'   => ['update'],
                              'linkOptions' => [
                                  'data-method' => 'post'
                              ],
                              // 'options'=>['class'=>'update-modal-click grid-action'],
                              // 'update'=>function($url,$model,$key){
                              //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                              //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                              //           'class'=>'update-modal-click grid-action',
                              //           'data-toggle'=>'tooltip',
                              //           'data-placement'=>'bottom',
                              //           'title'=>'Update'
                              //       ]);
                              //       return $btn;
                              // },
                              // 'option' 'class'=>'update-modal-click grid-action',
                              // 'data-toggle'=>'tooltip',
                              // 'data-placement'=>'bottom',
                          ],
                          [
                              'label'   => 'Disable',
                              'url'     => ['delete'],
                              'linkOptions' => [
                                  'data-method' => 'post'
                              ],
                          ],
                      ]
                  ],
              ],
          ]); ?>
          <?php Pjax::end(); ?>
         </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
  <?php

} else {
  // code...
  ?>
  <div class="box-body">
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-ban"></i> Alert!</h4>
      Please Select the Custome you wish to add data for
    </div>
    <div class="alert alert-info alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-info"></i> Alert!</h4>
      Please Select the Custome you wish to add data for
    </div>
    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
      Please Select the Custome you wish to add data for
    </div>
    <div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-check"></i> Alert!</h4>
      Success alert preview. This alert is dismissable.
    </div>
  </div>
  <?php
}
?>
