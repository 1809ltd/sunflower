<?php

namespace app\modules\inventory\asset\assetamortization\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\inventory\asset\assetamortization\models\AssetAmortization;

/**
 * AssetAmortizationSearch represents the model behind the search form of `app\modules\inventory\asset\assetamortization\models\AssetAmortization`.
 */
class AssetAmortizationSearch extends AssetAmortization
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assetId', 'repayment', 'userId', 'status'], 'integer'],
            [['interestAmount', 'principalAmount', 'startBalance', 'endBalance', 'cummulativeInterest', 'cummulativePrincipal'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt', 'amortizationDate'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetAmortization::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assetId' => $this->assetId,
            'repayment' => $this->repayment,
            'interestAmount' => $this->interestAmount,
            'principalAmount' => $this->principalAmount,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'status' => $this->status,
            'amortizationDate' => $this->amortizationDate,
            'startBalance' => $this->startBalance,
            'endBalance' => $this->endBalance,
            'cummulativeInterest' => $this->cummulativeInterest,
            'cummulativePrincipal' => $this->cummulativePrincipal,
        ]);

        return $dataProvider;
    }
}
