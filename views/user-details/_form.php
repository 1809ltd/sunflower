<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userdetails\models\UserDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'userStaffId')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userFName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userLName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userPhone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userEmail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'authKey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userImage')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userStatus')->textInput() ?>

    <?= $form->field($model, 'userLastLogin')->textInput() ?>

    <?= $form->field($model, 'userCreatedBy')->textInput() ?>

    <?= $form->field($model, 'userDeleteAt')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
