<?php

namespace app\modules\finance\financereport\customerreport\models;

use Yii;

/**
 * This is the model class for table "customer_statement".
 *
 * @property int $customerId
 * @property string $date
 * @property string $transactionNo
 * @property string $Details
 * @property string $amountinvoiced
 * @property string $amountPaid
 */
class CustomerStatement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

     public $balance_amount;

    public static function tableName()
    {
        return 'customer_statement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerId'], 'integer'],
            [['date'], 'safe'],
            [['transactionNo'], 'string', 'max' => 100],
            [['Details'], 'string', 'max' => 208],
            [['amountinvoiced'], 'string', 'max' => 23],
            [['amountPaid'], 'string', 'max' => 53],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customerId' => 'Customer',
            'date' => 'Date',
            'transactionNo' => 'Transaction No',
            'Details' => 'Details',
            'amountinvoiced' => 'Amountinvoiced',
            'amountPaid' => 'Amount Paid',
        ];
    }
}
