<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetPreciation */

$this->title = 'Update Asset Preciation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Preciations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-preciation-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
