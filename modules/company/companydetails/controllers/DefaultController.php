<?php

namespace app\modules\company\companydetails\controllers;

use yii\web\Controller;

/**
 * Default controller for the `companydetails` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
