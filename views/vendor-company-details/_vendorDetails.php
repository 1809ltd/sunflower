<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VendorCompanyDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="vendor-company-details-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

          // 'id',
          // 'vendorCompanyName',
          // 'vendorKraPin',
          // 'vendorContactPersonFullName',
          // 'contactMobilePhone',
          // 'vendorPhone',
          // 'contactEmail:email',
          // 'vendoremail:email',
          'vendorCompanyAddress',
          'vendorCompanySuite',
          'vendorCompanyCity',
          'vendorCompanyCounty',
          'vendorCompanyPostal',
          'vendorCompanyCountry',
          'vendorCompanyLogo:ntext',
          'vendorTwitter',
          'vendorFb',
          'vendorIg',
          'vendorType',
          // 'vendorCompanyStatus',
          // 'vendorCompanyTimestamp',
          // 'vendorCompanyupdatedAt',
          // 'vendorCompanyDeletedAt',
          // 'companyUserId',


        ],
    ]); ?>

</div>
