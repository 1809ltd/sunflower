<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-check-out-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'assetId')->textInput() ?>

    <?= $form->field($model, 'locationId')->textInput() ?>

    <?= $form->field($model, 'checkOutDate')->textInput() ?>

    <?= $form->field($model, 'dueDate')->textInput() ?>

    <?= $form->field($model, 'activity')->textInput() ?>

    <?= $form->field($model, 'deliveryId')->textInput() ?>

    <?= $form->field($model, 'activityId')->textInput() ?>

    <?= $form->field($model, 'orderId')->textInput() ?>

    <?= $form->field($model, 'orderlistId')->textInput() ?>

    <?= $form->field($model, 'vehicle')->textInput() ?>

    <?= $form->field($model, 'assignto')->textInput() ?>

    <?= $form->field($model, 'timestamp')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'athurizedBy')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
