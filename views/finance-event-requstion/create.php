<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expense\eventexpense\models\FinanceEventRequstion */

$this->title = 'Create Finance Event Requstion';
$this->params['breadcrumbs'][] = ['label' => 'Finance Event Requstions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-requstion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
