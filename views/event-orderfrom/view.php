<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfrom */

// use app\models\EventOrderfromList;
// use app\models\EventOrderfromListSearch;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfrom */

$this->title = $model->orderNumber;
$this->params['breadcrumbs'][] = ['label' => 'Event Orderfroms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?><p>
    <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
</p>

        <!-- Main content -->
           <section class="invoice">
             <!-- title row -->
             <div class="row">
               <div class="col-xs-12">
                 <h2 class="page-header">
                   <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
                   <small class="pull-right">Date: <?= $model->date;?></small>
                 </h2>
                 
               </div>
               <!-- /.col -->
             </div>
             <!-- info row -->
             <div class="row invoice-info">
               <div class="col-sm-4 invoice-col">
                 From
                 <address>
                   <strong>SUNFLOWER EVENTS,</strong><br/>
                    Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
                   Nairobi, 00100 Kenya<br/>
                   Phone: +254 (0) 722 790632<br/>
                   Email: info@...com
                 </address>
               </div>
               <!-- /.col -->

               <?php $eventinfo=$model->geteventDetailss($model->eventId); ?>
               <?php foreach ($eventinfo as $eventinfo): ?>

               <div class="col-sm-4 invoice-col">

                 To
                 <address>

                   <?php $customerinfo=$model->getCustomerDetails($eventinfo->customerId);

                   // print_r($customerinfo);

                   foreach ($customerinfo as $customerinfo) {
                     // code...

                   ?>
                   <strong><?= $customerinfo->customerDisplayName;?></strong><br/>
                   Email: <?= $customerinfo->customerPrimaryEmail;?><br/>
                   <?= $customerinfo->customercity;?>, <?= $customerinfo->customerPostalCode;?><br/>
                   Phone: <?= $customerinfo->customerPrimaryPhone;?><br/>

                     <?php

                   }

                   ?>

                 </address>
               </div>
               <!-- /.col -->
               <div class="col-sm-4 invoice-col">



                   <b>Order Number: #<?=$model->orderNumber;?></b><br>
                   <br>
                   <b>Event: </b> <?= $eventinfo->eventName;?><br>
                   <b>Type: <?= $eventinfo->eventCategory;?><br>
                   <b>Theme: <?= $eventinfo->eventTheme;?><br/>
                   <b>Event Date : <?= $eventinfo->eventStartDate;?><br/>
                   <b>Set Up: <?= $eventinfo->eventSetUpDateTime;?><br/>


               </div>
                <?php endforeach; ?>
               <!-- /.col -->
             </div>
             <!-- /.row -->

             <!-- Table row -->
             <div class="row">
               <div class="col-xs-12 table-responsive">
                 <table class="table table-striped">
                   <thead>
                     <th>Requirements</th>
                     <th>Size</th>
                     <th>Outsourced</th>
                   </thead>

                   <tbody>
                     <?php
                     $eventOrderfromLists=$model->geteventOrderfromList($model->id);


                     foreach ($eventOrderfromLists as $key => $eventOrderfromList) {
                       // code...
                       // print_r($eventOrderfromList);
                       // echo $eventOrderfromList['service'];
                       //
                       // die();

                       $eventOrderfromList['itemid'];

                       $eventOrderfromList['itemid'];
                       $eventOrderfromList['outsourced'];
                       $eventOrderfromList['vendorId'];
                       $eventOrderfromList['quantity'];
                       $eventOrderfromList['status'];

                       if ($eventOrderfromList['outsourced']==1) {
                         // code...
                         $outsourced= "No";
                       }

                       ?>
                       <tr>
                           <td>
                             Service: <?= $eventOrderfromList['service'];?>.

                             <br/>
                               <small> Description:  <?= $eventOrderfromList['details'];?>.
                               </small>

                               <br/>

                               <?php

                               /*Getting the Asset REquired for the Event*/

                               $eventAssetrequireds=$model->getAssetrequired($eventOrderfromList['itemid']);

                                if (!empty($eventAssetrequireds)): ?>


                                <br/>
                                Asset Required for the Event<br/>

                                <div class="table-responsive">
                                  <table class="table">

                                      <?php

                                            foreach ($eventAssetrequireds as $eventAssetrequired){

                                              $eventCategotryTypeName=$model->getCategotryTypeName($eventAssetrequired->categorytypeId);

                                              /*GEtting the Name of the Category for the Asset*/
                                              foreach ($eventCategotryTypeName as $eventCategotryTypeName){

                                                ?>

                                                     <tr>
                                                       <td style="width:50%">Asset Type: <?= $eventCategotryTypeName->assetCategoryTypeName;?></td>
                                                       <td>Quantity: <?= $eventAssetrequired->qty;?></td>
                                                     </tr>

                                               <!-- /.col -->

                                                <?php

                                              }
                                              ?>
                                              <?php

                                            }

                                          ?>

                                        </table>
                                      </div>

                               <?php else: ?>

                               <?php endif; ?>


                               <?php

                               /*Getting the Role REquired for the Event*/

                               $eventRolerequireds=$model->getRolerequired($eventOrderfromList['itemid']);

                                if (!empty($eventRolerequireds)): ?>


                                <br/>
                                Workforce Required for the Event<br/>

                                <div class="table-responsive">
                                  <table class="table">

                                      <?php

                                            foreach ($eventRolerequireds as $eventRolerequired){

                                              $eventRoleName=$model->getRoleName($eventRolerequired->roleId);

                                              /*GEtting the Name of the Category for the Asset*/
                                              foreach ($eventRoleName as $eventRoleName){

                                                ?>

                                                     <tr>
                                                       <td style="width:50%">Role : <?= $eventRoleName->roleName;?></td>
                                                       <td>Number: <?= $eventRolerequired->qty;?></td>
                                                     </tr>

                                               <!-- /.col -->

                                                <?php

                                              }
                                              ?>
                                              <?php

                                            }

                                          ?>

                                        </table>
                                      </div>

                               <?php else: ?>

                               <?php endif; ?>



                           </td>
                           <td><?= $eventOrderfromList['quantity']?></td>
                           <td><?= $outsourced;?></td>
                       </tr>

                       <?php

                     }

                      ?>

                      <tr>
                          <td>
                               <br/>
                              <small>.
                              </small>
                          </td>
                          <td></td>
                          <td></td>

                      </tr>


                   </tbody>


                 </table>
               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->

             <div class="row">
               <!-- accepted payments column -->
               <div class="col-xs-6">
                 <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                   *<?= $model->orderNote; ?>
                 </p>
               </div>
               <!-- /.col -->
               <div class="col-xs-6">
                 <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


                 </p>
               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->


             <!-- this row will not appear when printing -->
             <div class="row no-print">
               <div class="col-xs-12">
                 <a href="javascript:void(0)" onclick="window.print()"
                    class="btn btn-xs btn-success m-b-10"><i
                         class="fa fa-print m-r-5"></i> Print</a>


                 <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
                   <i class="fa fa-download"></i> Generate PDF
                 </button>
               </div>
             </div>
           </section>
           <!-- /.content -->
