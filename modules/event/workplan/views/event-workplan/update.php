<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventWorkplan */

$this->title = 'Update Event Workplan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Workplans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

// print_r($model);
?>
<div class="event-workplan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
