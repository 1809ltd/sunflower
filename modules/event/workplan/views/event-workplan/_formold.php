<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\EventOrderfromList;
use app\models\EventOrderfrom;
use app\models\PersonnelDetails;
use kartik\select2\Select2;


/*Dependant Drop*/
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\EventWorkplan */
/* @var $form yii\widgets\ActiveForm */
?>


<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Add Event Workplan</h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row" >

      <div class="event-workplan-form">

          <?php $form = ActiveForm::begin(['id'=>$model->formName(),
          'enableAjaxValidation'=>true,]); ?>

      </div>
          <div class="col-sm-3">
            <div class="form-group">

              <?php

              // Top most parent
              $orderId= ArrayHelper::map(EventOrderfrom::find()->where('`orderStatus` = 1')->asArray()->all(),'id','orderNumber');

              ?>
                <?= $form->field($model, 'orderId')->dropDownList($orderId,['id'=>'orderId','prompt'=>'Select...']); ?>
            </div>
          </div>
          <?php

          // Additional input fields passed as params to the child dropdown's pluginOptions


          ?>
          <div class="col-sm-3">
            <div class="form-group">
                <?= $form->field($model, 'orderformId')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'orderformId'],
                    'pluginOptions'=>[
                        'depends'=>['orderId'],
                        'placeholder'=>'Select...',
                        'url'=>Url::to(['/event-orderfrom-list/orderdetails'])
                    ]
                ]);?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">

                <?= $form->field($model, 'taskAssign')->widget(DepDrop::classname(), [
                    'pluginOptions'=>[
                        'depends'=>['orderId', 'orderformId'],
                        'placeholder'=>'Select...',
                        'url'=>Url::to(['/event-orderfrom-list/jobname'])
                    ]
                ]); ?>


            </div>

          </div>
          <div class="col-sm-3">
            <div class="form-group">
                <?= $form->field($model, 'personnelid')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(PersonnelDetails::find()->all(),'id','displayName'),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Select a Personnel ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);?>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
                <?= $form->field($model, 'personnelPhone')->textInput(['maxlength' => true]) ?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
                  <?php $category = ['Permanent' => 'Permanent','On Site Casual' => 'On Site Casual','off Site Casual' => 'off Site Casual',]; ?>
                  <?= $form->field($model, 'category')->dropDownList($category, ['prompt'=>'Select Category','class'=>'form-control']);?>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
                <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
              <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">

            </div>
          </div>

         <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

      </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <div class="modal-footer">
      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
  </div>
  <?php ActiveForm::end(); ?>
</div>
    <!-- /.box -->
