<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceAccounts */

$this->title = 'Create Finance Accounts';
$this->params['breadcrumbs'][] = ['label' => 'Finance Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-accounts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
