<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expense\eventexpense\models\FinanceEventRequstion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-event-requstion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'requstionNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eventId')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'requstionStatus')->textInput() ?>

    <?= $form->field($model, 'requstionNote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'preparedBy')->textInput() ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'approvedBy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
