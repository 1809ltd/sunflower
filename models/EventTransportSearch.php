<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventTransport;

/**
 * EventTransportSearch represents the model behind the search form of `app\models\EventTransport`.
 */
class EventTransportSearch extends EventTransport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'eventId', 'outsourced', 'assetId', 'vendorId', 'delivery', 'collection', 'status', 'userId', 'approvedBy'], 'integer'],
            [['numberPlate', 'deliveryTime', 'collectionTime', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventTransport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'eventId' => $this->eventId,
            'outsourced' => $this->outsourced,
            'assetId' => $this->assetId,
            'vendorId' => $this->vendorId,
            'delivery' => $this->delivery,
            'collection' => $this->collection,
            'deliveryTime' => $this->deliveryTime,
            'collectionTime' => $this->collectionTime,
            'status' => $this->status,
            'userId' => $this->userId,
            'approvedBy' => $this->approvedBy,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'numberPlate', $this->numberPlate]);

        return $dataProvider;
    }
}
