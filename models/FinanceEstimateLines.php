<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_estimate_lines".
 *
 * @property int $id
 * @property int $estimateId
 * @property int $itemRefId
 * @property string $itemRefName
 * @property string $estimateLinesDescription
 * @property double $estimateLinespack
 * @property double $estimateLinesQty
 * @property double $estimateLinesUnitPrice
 * @property double $estimateLinesAmount
 * @property double $estimateLinesTaxCodeRef
 * @property double $estimateLinesTaxamount
 * @property double $estimateLinesDiscount
 * @property string $estimateLinesCreatedAt
 * @property string $estimateLinesUpdatedAt
 * @property string $estimateLinesDeletedAt
 * @property int $estimateLinesStatus
 * @property int $userId
 *
 * @property FinanceItems $itemRef
 * @property FinanceItems $itemRefName0
 */
class FinanceEstimateLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_estimate_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemRefId', 'itemRefName', 'estimateLinesDescription', 'estimateLinespack', 'estimateLinesQty', 'estimateLinesUnitPrice', 'estimateLinesAmount', 'estimateLinesTaxCodeRef', 'estimateLinesTaxamount', 'estimateLinesDiscount'], 'required'],
            [['estimateId', 'itemRefId', 'estimateLinesStatus', 'userId'], 'integer'],
            [['estimateLinesDescription'], 'string'],
            [['estimateLinespack', 'estimateLinesQty', 'estimateLinesUnitPrice', 'estimateLinesAmount', 'estimateLinesTaxCodeRef', 'estimateLinesTaxamount', 'estimateLinesDiscount'], 'number'],
            [['estimateId', 'estimateLinesCreatedAt', 'estimateLinesUpdatedAt', 'estimateLinesDeletedAt', 'estimateLinesStatus', 'userId'], 'safe'],
            [['itemRefName'], 'string', 'max' => 255],
            [['itemRefId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemRefId' => 'id']],
            [['itemRefName'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemRefName' => 'itemsName']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estimateId' => 'Quote',
            'itemRefId' => 'Product or Service',
            'itemRefName' => 'Product or Service',
            'estimateLinesDescription' => 'Description',
            'estimateLinespack' => 'Days',
            'estimateLinesQty' => 'Qty',
            'estimateLinesUnitPrice' => 'Unit Price',
            'estimateLinesAmount' => 'Amount',
            'estimateLinesTaxCodeRef' => 'Tax',
            'estimateLinesTaxamount' => 'Tax Amount',
            'estimateLinesDiscount' => 'Discount(%)',
            'estimateLinesCreatedAt' => 'Created At',
            'estimateLinesUpdatedAt' => 'Updated At',
            'estimateLinesDeletedAt' => 'Deleted At',
            'estimateLinesStatus' => 'Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemRef()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemRefId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemRefName0()
    {
        return $this->hasOne(FinanceItems::className(), ['itemsName' => 'itemRefName']);
    }
}
