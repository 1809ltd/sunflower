<?php

namespace app\modules\event\eventInfo\models;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Event Categories*/
use app\modules\event\eventsetup\eventcaterogies\models\EventCaterogies;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "event_details".
 *
 * @property int $id
 * @property int $customerId
 * @property string $eventNumber
 * @property string $eventName
 * @property string $eventTheme
 * @property double $eventVistorsNumber
 * @property int $eventCategory
 * @property string $eventStartDate
 * @property string $eventEndDate
 * @property string $eventSetUpDateTime
 * @property string $eventSetDownDateTime
 * @property string $eventLocation
 * @property string $eventDescription
 * @property string $eventNotes
 * @property int $status
 * @property string $eventCreatedAt
 * @property string $eventLastUpdatedAt
 * @property string $eventDeletedAt
 * @property int $createdBy
 * @property int $userId
 *
 * @property AssetCheckOut[] $assetCheckOuts
 * @property AssetDeliveryform[] $assetDeliveryforms
 * @property CustomerDetails $customer
 * @property EventCaterogies $eventCategory0
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property EventOrderfrom[] $eventOrderfroms
 * @property EventRequirements[] $eventRequirements
 * @property EventTransport[] $eventTransports
 * @property FinanceBills[] $financeBills
 * @property FinanceCustomerPurchaseOrder[] $financeCustomerPurchaseOrders
 * @property FinanceEstimate[] $financeEstimates
 * @property FinanceEventRequstionForm[] $financeEventRequstionForms
 * @property FinanceInvoice[] $financeInvoices
 */
class EventDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

     /*Additional factor*/
     public $caterogiesNumber;

    public static function tableName()
    {
        return 'event_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerId', 'eventNumber', 'eventName', 'eventTheme', 'eventVistorsNumber', 'eventCategory', 'eventStartDate', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventLocation', 'eventDescription', 'eventNotes', 'status', 'eventCreatedAt', 'createdBy', 'userId'], 'required'],
            [['customerId', 'eventCategory', 'status', 'createdBy', 'userId'], 'integer'],
            [['eventVistorsNumber'], 'number'],
            [['eventStartDate', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventCreatedAt', 'eventLastUpdatedAt', 'eventDeletedAt'], 'safe'],
            [['eventDescription', 'eventNotes'], 'string'],
            [['eventNumber', 'eventName', 'eventTheme'], 'string', 'max' => 100],
            [['eventLocation'], 'string', 'max' => 50],
            [['eventNumber'], 'unique'],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['eventCategory'], 'exist', 'skipOnError' => true, 'targetClass' => EventCaterogies::className(), 'targetAttribute' => ['eventCategory' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customerId' => 'Customer',
            'eventNumber' => 'Event Number',
            'eventName' => 'Event Name',
            'eventTheme' => 'Event Theme',
            'eventVistorsNumber' => 'Guests Number',
            'eventCategory' => 'Event Category',
            'eventStartDate' => 'Event Start Date',
            'eventEndDate' => 'Event End Date',
            'eventSetUpDateTime' => 'Event Set Up Date Time',
            'eventSetDownDateTime' => 'Event Set Down Date Time',
            'eventLocation' => 'Event Location',
            'eventDescription' => 'Event Description',
            'eventNotes' => 'Event Notes',
            'status' => 'Status',
            'eventCreatedAt' => 'Event Created At',
            'eventLastUpdatedAt' => 'Event Last Updated At',
            'eventDeletedAt' => 'Event Deleted At',
            'createdBy' => 'Created By',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['activityId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetDeliveryforms()
    {
        return $this->hasMany(AssetDeliveryform::className(), ['eventId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventCategory0()
    {
        return $this->hasOne(EventCaterogies::className(), ['id' => 'eventCategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfroms()
    {
        return $this->hasMany(EventOrderfrom::className(), ['eventId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRequirements()
    {
        return $this->hasMany(EventRequirements::className(), ['eventNumber' => 'eventNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventTransports()
    {
        return $this->hasMany(EventTransport::className(), ['eventId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBills()
    {
        return $this->hasMany(FinanceBills::className(), ['projectId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerPurchaseOrders()
    {
        return $this->hasMany(FinanceCustomerPurchaseOrder::className(), ['purchaseOrderProjectId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEstimates()
    {
        return $this->hasMany(FinanceEstimate::className(), ['estimateProjectId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionForms()
    {
        return $this->hasMany(FinanceEventRequstionForm::className(), ['eventId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoices()
    {
        return $this->hasMany(FinanceInvoice::className(), ['invoiceProjectId' => 'id']);
    }
    /*Getting All the events from a customer*/
    public static function getCustomerEventList($customer_id)
    {
        $projects = self::find()
            ->select(['id','eventName as name'])
            ->where(['customerId' => $customer_id])
            ->asArray()
            ->all();
        return $projects;
    }
    /*Getting All the events from a customer*/
    public static function getEventCategory($project_id)
    {
        $connection = Yii::$app->db;
        $query= "SELECT event_caterogies.id AS id, event_caterogies.eventCatName AS name FROM event_details, event_caterogies WHERE event_details.id = $project_id AND event_caterogies.id = event_details.eventCategory";
        $eventcategory= $connection->createCommand($query)->queryAll();

        return $eventcategory;
    }


    /*Getting All the events from a Project Name*/
    public static function getEventName($project_id)
    {
        $eventJina = self::find()
            ->select(['id','eventName as name'])
            ->where(['id' => $project_id])
            ->asArray()
            ->all();
        return $eventJina;
        // return self::find()
        //       ->select(['id','eventName as name'])
        //       ->where(['id' => $project_id])->indexBy('id')->column();
    }

    public function getEnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(eventNumber) AS number from event_details where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number = "EVNT-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "EVNT-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "EVNT-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
