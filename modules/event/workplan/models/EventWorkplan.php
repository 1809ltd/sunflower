<?php

namespace app\modules\event\workplan\models;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Order form */
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
/*Order form List */
use app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromList;
/*Personnel details*/
use app\modules\personnel\personneldetails\models\PersonnelDetails;
/*Item Role*/
use app\modules\event\eventsetup\eventitemrole\models\EventItemRole;
/*Evevnt Details*/
use app\modules\event\eventInfo\models\EventDetails;

use app\modules\event\workplan\models\EventWorkplan;

use Yii;

/**
 * This is the model class for table "event_workplan".
 *
 * @property int $id
 * @property int $customerId
 * @property int $eventId
 * @property int $orderId
 * @property int $orderformId
 * @property int $personnelid
 * @property string $personnelPhone
 * @property string $category
 * @property int $taskAssign
 * @property string $detail
 * @property int $status
 * @property string $createdAt
 * @property string $updateAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $createdBy
 * @property int $approvedId
 *
 * @property EventOrderfromList $orderform
 * @property PersonnelDetails $personnel
 * @property UserDetails $user
 * @property UserDetails $approved
 * @property EventOrderfrom $order
 * @property EventItemRole $taskAssign0
 * @property UserDetails $createdBy0
 * @property CustomerDetails $customer
 * @property EventDetails $event
 * @property FinanceEventRequstionFormList[] $financeEventRequstionFormLists
 */
class EventWorkplan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_workplan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orderId', 'orderformId', 'personnelPhone', 'category', 'taskAssign', 'detail', 'status', 'createdAt', 'userId', 'createdBy'], 'required'],
            [['customerId', 'eventId', 'orderId', 'orderformId', 'personnelid', 'taskAssign', 'status', 'userId', 'createdBy', 'approvedId'], 'integer'],
            [['detail'], 'string'],
            [['customerId', 'eventId', 'createdAt', 'updateAt', 'deletedAt'], 'safe'],
            [['personnelPhone'], 'string', 'max' => 15],
            [['category'], 'string', 'max' => 100],
            [['orderformId'], 'exist', 'skipOnError' => true, 'targetClass' => EventOrderfromList::className(), 'targetAttribute' => ['orderformId' => 'id']],
            [['personnelid'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelDetails::className(), 'targetAttribute' => ['personnelid' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedId' => 'id']],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => EventOrderfrom::className(), 'targetAttribute' => ['orderId' => 'id']],
            [['taskAssign'], 'exist', 'skipOnError' => true, 'targetClass' => EventItemRole::className(), 'targetAttribute' => ['taskAssign' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['eventId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['eventId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customerId' => 'Customer',
            'eventId' => 'Event',
            'orderId' => 'Order',
            'orderformId' => 'Service',
            'personnelid' => 'Personnel',
            'personnelPhone' => 'Personnel Phone',
            'category' => 'Category',
            'taskAssign' => 'Task Assign',
            'detail' => 'Detail',
            'status' => 'Status',
            'createdAt' => 'Created',
            'updateAt' => 'Update',
            'deletedAt' => 'Deleted',
            'userId' => 'User',
            'createdBy' => 'Created',
            'approvedId' => 'Approved',
        ];
    }

    /*Get All workplan based on orderID*/
    public function geteventWorkOrderfromList($id,$orderStatus){
      //Getting all the event Order list with the same status
      return EventWorkplan::find()
                    ->where(['orderId'=>$id])
                    ->Andwhere(['status'=>$orderStatus])
                    ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderform()
    {
        return $this->hasOne(EventOrderfromList::className(), ['id' => 'orderformId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonnel()
    {
        return $this->hasOne(PersonnelDetails::className(), ['id' => 'personnelid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApproved()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(EventOrderfrom::className(), ['id' => 'orderId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskAssign0()
    {
        return $this->hasOne(EventItemRole::className(), ['id' => 'taskAssign']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'eventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionFormLists()
    {
        return $this->hasMany(FinanceEventRequstionFormList::className(), ['workplanId' => 'id']);
    }
}
