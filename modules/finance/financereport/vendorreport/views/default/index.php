<!-- <div class="vendorreport-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
// Vendor Details
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
?>
<!-- <div class="customerreport-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<!-- Main content -->
<!-- Search Date And the rest -->
<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label class="col-lg-4 control-label">Supplier: </label>
          <div class="col-lg-8">
            <?= $form->field($model, 'vendor')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(VendorCompanyDetails::find()->where('`vendorCompanyStatus` = 1')->asArray()->all(),'id','vendorCompanyName'),
                  'language' => 'en',
                  'options' => ['autocomplete'=>'off','id'=> 'customer-id', 'placeholder' => 'Select a Company ...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ])->label(false); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">

      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date From: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
              //'language' => 'ru',
              'options' => ['autocomplete'=>'off','class' => 'form-control'],
              'inline' => false,
              'dateFormat' => 'yyyy-MM-dd',
              ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date To: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
              //'language' => 'ru',
              'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <div class="col-lg-8 col-lg-offset-4">
            <div class="center-align">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
            </div>
          </div>
        </div>
      </div>
    </div>
      <?php ActiveForm::end(); ?>
  </div>
</div>
<?php
if (!empty($vendor_statement)) {
  // code...
// print_r($vendor);
// echo $vendor[0]["vendorCompanyName"];

// die();
  ?>
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title"><?= $vendor[0]["vendorCompanyName"] ?></h3>
      <h5 class="box-title">Reporting period:  <?= Yii::$app->formatter->asDate($startDate, 'long');?> to <?= Yii::$app->formatter->asDate($endDate, 'long');?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>

     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <?php

              ?>
             <th class="page-header" style="text-align: center;width: 85%;">STATEMENT OF ACCOUNT: <?php
             // Yii::$app->formatter->asDate($tarehe, 'long');?></th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>
     <!-- info row -->
     <div class="row invoice-info">
     </div>
     <!-- /.row -->
     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Date:</th>
             <th>Transaction No:</th>
             <th>Details:</th>
             <th class="text-right">Amount Invoiced: (Ksh)</th>
             <th class="text-right">Amount Paid: (Ksh)</th>
             <th class="text-right">Balance (Ksh)</th>
           </thead>
             <tbody>

               <?php

                ?>

               <tr>
                 <td> Balance brought forward as at<?= Yii::$app->formatter->asDate($startDate, 'long');?></td>
                 <td> </td>
                 <td>  </td>
                 <td> </td>
                 <td></td>
                <td class="text-right"><?= number_format($balbroughtforward,2) ;?>
                </td>
              </tr>
               <?php
               // Get all the specific Invoice

               $SubamountPaid=0;
               $SubamountInvoiced=0;
               $SubBal=$balbroughtforward;

               foreach ($vendor_statement as $vendor_statement) {
                 // code...
                 $SubamountPaid+=$vendor_statement->amountPaid;
                 $SubamountInvoiced+=$vendor_statement->amountinvoiced;
                 $SubBal = $SubBal+$vendor_statement->amountinvoiced-$vendor_statement->amountPaid;
                 ?>
                 <tr>
                   <td><?= Yii::$app->formatter->asDate($vendor_statement["date"], 'long');?></td>
                   <td><?= $vendor_statement->transactionNo;?></td>
                   <td><?= $vendor_statement->Details;?> </td>
                   <td class="text-right"><?= number_format($vendor_statement->amountinvoiced,2) ;?></td>
                   <td class="text-right"><?= number_format($vendor_statement->amountPaid,2) ;?></td>
                  <td class="text-right">
                    <?php
                    echo number_format($SubBal,2);
                    ?>
                  </td>
                </tr>
                <?php
              }
              ?>
            </tbody>
              <th>Total:</th>
              <th></th>
              <th></th>
              <th class="text-right"><?php
              $SubamountInvoiceamount = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($SubamountInvoiced,2,'.',',')
             );

              echo $SubamountInvoiceamount;

              ?></th>
              <th class="text-right"><?php
              $SubamountPaidamount = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($SubamountPaid,2,'.',',')
             );

              echo $SubamountPaidamount;

              ?></th>
              <th class="text-right"><?php
              $BalAoamount = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($SubBal,2,'.',',')
             );

              echo $BalAoamount;

              ?><!-- Sub Total --></th>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <br>
      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
            CHEQUES SHOULD BE MADE PAYABLE TO SUNFLOWER TENTS & DÉCOR LTD
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="" style="margin-top: 10px;">
            <?php
            // $model->invoiceFooter; ?>
          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->


      <!-- /.row -->
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
              class="btn btn-xs btn-success m-b-10"><i
                   class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
        </button> -->
        </div>
      </div>
    </section>
    <!-- /.content -->
    <?php
  } else {
    // code...

    echo "No Invoices Bills were found Here";
  }
  ?>
