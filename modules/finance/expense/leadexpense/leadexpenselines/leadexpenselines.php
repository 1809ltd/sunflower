<?php

namespace app\modules\finance\expense\leadexpense\leadexpenselines;

/**
 * leadexpenselines module definition class
 */
class leadexpenselines extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\leadexpense\leadexpenselines\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
