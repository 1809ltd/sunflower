"use strict";
var ass = function () {
    "use strict";

    $(document).ready(function () {
        var table = "";
        table = $('#table').DataTable({
            "scrollX"     : true,
            "ajax"        : "assets/php/Data/tariff",
            "initComplete": function (settings, json) {

            },
            "columnDefs"  : [
                {
                    "render" : function (data, type, row) {
                        return '<a href="javascript:void(0)" class="label label-primary">Edit</a>';
                    },
                    "targets": 8
                }
            ],
            "columns"     : [
                {"data": null},
                {"data": 'cat'},
                {"data": 'tt'},
                {"data": 'minkm'},
                {"data": 'minc'},
                {"data": 'exkm'},
                {"data": 'nc'},
                {"data": 'status'},
                {"data": null}
            ],
            "order"       : [ [ 0, 'asc' ] ]
        });

        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
};
var Assign = function () {
    "use strict";
    return {
        init: function () {
            ass();
        }
    }
}();
$(function () {
    Assign.init();
});