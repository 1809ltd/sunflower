// For Direction between two markers
var icons;
var interval;
var directionsDisplay = null;
var directionsService;
var bounds;
var infowindow;
var marker;
var map, markerCluster;
var locations = [];
var allMarkers = [];
var isLoaded = 0;

// Starting and ending point for routing between two markers
// var org = new google.maps.LatLng(11.084956, 76.998373);
// var dest = new google.maps.LatLng(11.020522, 76.966698);

var styles = [[{
    url: 'assets/img/culster.png',
    height: 98,
    width: 106,
    anchor: [35, 37],
    textColor: '#ff00ff',
    textSize: 11
}]];

function initialize() {
    //setTimeout(isMapLoaded, GLOBAL.MAPLOAD_INTERVAL * 100);
    setTimeout(isMapLoaded, 5000);

    if (typeof(google) === 'object' && typeof(google.maps) === 'object') {
        isLoaded = 1;
        loadJS = function (src) {
            var jsLink = $("<script type='text/javascript' src='" + src + "'>");
            $("head").append(jsLink);
        };
        loadJS("assets/js/map/map-icons.js");
        loadJS("assets/js/map/markerclusterer_compiled.js");
        loadJS("assets/js/map/vehiclelist.js");

        icons = {
            taxi: {
                path: MAP_PIN,
                fillColor: '#348fe2',
                fillOpacity: 1,
                strokeColor: '',
                strokeWeight: 0
            },
            start: {
                path: ROUTE,
                fillColor: '#1998F7',
                fillOpacity: 1,
                strokeColor: '',
                strokeWeight: 0
            },
            end: {
                path: ROUTE,
                fillColor: '#6331AE',
                fillOpacity: 1,
                strokeColor: '',
                strokeWeight: 0
            }
        };
    }

    function isMapLoaded() {
        /* if (isLoaded == "0") {
            var path = GLOBAL.BASE_URL + "/Custom/error502";
            $(".map").html('<iframe src="' + path + '" class="height-full" style="width:100%"></iframe>');
        } */
    }

    directionsDisplay = new google.maps.DirectionsRenderer();
    directionsService = new google.maps.DirectionsService();
    bounds = new google.maps.LatLngBounds();
    infowindow = new google.maps.InfoWindow({maxWidth: 200});
    marker = new google.maps.Marker();

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: new google.maps.LatLng(11.341036, 77.717164),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        fullscreenControl: true,
        minZoom: 5,
        mapTypeControlOption: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    });

    var zoomControlDiv = document.createElement('div');
    zoomControlDiv.id = 'zoomControl';
    var zoomControl = new ZoomControl(zoomControlDiv, map);
    zoomControlDiv.index = 1;
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(zoomControlDiv);

    var colors = document.getElementById('colors_display')
    map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(colors);

    var vech_select_for_map = document.getElementById('vech_select_for_map')
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(vech_select_for_map);

    directionsDisplay.setMap(map);
    directionsDisplay.setOptions({suppressMarkers: true});

    google.maps.event.addListenerOnce(map, 'idle', function () {
        if (interval)
            clearInterval(interval);
        interval = setInterval(updateMarkers, 30 * 1000);
    });

    if (typeof google === 'object' && typeof google.maps === 'object') {
        //setInterval(updateMarkers, GLOBAL.MAPLOAD_INTERVAL * 1000);
    }
    updateMarkers();
}

// Custom Controll icons related functions starts
function ZoomControl(controlDiv, map) {
    var control = this;
    controlDiv.style.clear = 'both';

    // Zoom-In Controll
    var zoomInUI = document.createElement('div');
    zoomInUI.id = 'zoomIn';
    zoomInUI.title = 'ZoomIn';
    zoomInUI.innerHTML = "<i class='fa fa-plus'></i>";
    controlDiv.appendChild(zoomInUI);

    zoomInUI.addEventListener('click', function () {
        map.setZoom(map.getZoom() + 1);
    });

    // Zoom-Out Controll
    var zoomOutUI = document.createElement('div');
    zoomOutUI.id = 'zoomOut';
    zoomOutUI.title = 'ZoomOut';
    zoomOutUI.innerHTML = "<i class='fa fa-minus'></i>";
    controlDiv.appendChild(zoomOutUI);

    zoomOutUI.addEventListener('click', function () {
        map.setZoom(map.getZoom() - 1);
    });

    // Map Scroll Controll
    var scrollUI = document.createElement('div');
    scrollUI.id = 'scrollLock';
    scrollUI.title = 'Scroll Lock';
    scrollUI.innerHTML = "<i class='fa fa-lock'></i>";
    controlDiv.appendChild(scrollUI);

    scrollUI.addEventListener('click', function () {
        if (map.get("draggable")) {
            map.set("draggable", false);
            scrollUI.innerHTML = "<i class='fa fa-unlock-alt'></i>";
        } else {
            map.set("draggable", true);
            scrollUI.innerHTML = "<i class='fa fa-lock'></i>";
        }
    });
}

function loadMap(locations) {
    var i, latlng, markPos, icon;
    //var locations = locations.assignable;
    for (i in locations) {
        if (!locations[i]['current_lat'] || !locations[i]['current_lng'] || locations[i]["running_status"] == 'leave') {
            continue;
        }
        var color_array = {
            "free": "#00DEAF",
            "assigned": "#CD11EE",
            "allotted": "#2B1AAB",
            "running": "#9999FF",
            "break_down": "#BE394F",
            "break": "#F0AD4E",
            "leave": "#E0690A"
        };

        icons.taxi.fillColor = color_array[locations[i]["running_status"]];
        markPos = new google.maps.LatLng(locations[i]['current_lat'], locations[i]['current_lng']);
        marker = makeMarker(markPos, icons.taxi, locations[i]["vehicle_no"], '<span class="map-icon m-b-8"><i class="fa fa-taxi f-s-16" aria-hidden="true"></i></span>');

        var geocoder = new google.maps.Geocoder();
        var LocationAddress;

        google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
            return function () {
                // Get the address of marker starts
                latlng = new google.maps.LatLng(locations[i]['current_lat'], locations[i]["current_lng"]);
                geocoder.geocode({
                    "latLng": latlng
                }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var placeName = results[0].address_components[0].long_name;
                        LocationAddress = results[0].formatted_address;

                        var content = LocationAddress.substring(0, 30) + "...<br /><b> Vehicle No: </b>" + locations[i]["vehicle_no"] + " <br><span class='hide'><b> Model: </b> " + locations[i]["model"] + "<br/><b> Seat Capacity: </b>" + locations[i]["seat_capacity"] + "<br/></span><b> Status: </b>" + show_as_label(locations[i]["running_status"]);

                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    }
                });
                google.maps.event.addListener(map, 'click', function () {
                    infowindow.close();
                });

                // Get the address of marker ends
            }
        })(marker, i));

        function show_as_label(text) {
            text = text.replace("_", " ");
            var opt = text.toLowerCase().replace(/\b[a-z]/g, function (letter) {
                return letter.toUpperCase();
            });

            return opt;
        }

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                if ($("#allot_now_div").is(":visible") == true) {
                    var veh_id = locations[i]["vehicle_id"];
                    pick_veh_forAllot("marker", veh_id);
                }
            }
        })(marker, i));
        allMarkers.push(marker);
    }

    // >>>>> Fit all markers within the visible map area
    for (var i = 0; i < allMarkers.length; i++) {
        bounds.extend(allMarkers[i].getPosition());
    }
    map.fitBounds(bounds);
    if (allMarkers.length == 1) {
        map.setCenter(allMarkers[0].getPosition());
        map.setZoom(15);
    }
   // console.log(allMarkers);
    // <<<<< Fit all markers within the visible map area

    markerCluster = new MarkerClusterer(map, allMarkers, {
        gridSize: 50,
        styles: styles[0]
    });
    // setTimeout(updateMarkers ,GLOBAL.MAPLOAD_INTERVAL * 1000);
}


function pick_veh_forAllot(call_for, veh_id) {
}

function calcRoute(orgin, destin) {
    org = orgin;
    dest = destin;

    bounds.extend(org);
    bounds.extend(dest);
    map.fitBounds(bounds);

    if (directionsDisplay != null) {
        directionsDisplay.setMap(null);
        directionsDisplay = null;
    }

    var request = {
        origin: org,
        destination: dest,
        travelMode: google.maps.TravelMode.DRIVING
    };

    // Create or Draw route between two marker positions
    directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay = new google.maps.DirectionsRenderer();
            directionsDisplay.setDirections(response);
            directionsDisplay.setOptions({
                suppressMarkers: true,
                map: map
            });

            var legs = response.routes[0].legs[0];
            var distanceCovered = legs.distance.text;
            var distanceCoveredMeters = legs.distance.value; // Distance Value in meters
            var distanceInkm = distanceCoveredMeters / 1000;
            var timeTaken = legs.duration.text;
            var timeTakenValue = legs.duration.value;

            //console.log(distanceCoveredMeters);
            //console.log(distanceCovered);
            //console.log(timeTaken);
            //console.log(timeTakenValue);

            //$("body").find(".route_info").html(distanceCovered + " - " + timeTaken);

            // <b>Estimated Time: </b>' + timeTaken + '<br>

            $("body").find(".route_info_dtls").html('<b>Estimated Distance: </b><span id="appx_distance">' + distanceCovered + '</span><br><span id="est_cost"></span>');
            $("#preff_mod_id").trigger("change");

            $("#route_distance_val").val(distanceInkm);

            $("body").find(".map-content").removeClass("hide");

        } else {
            alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
        }
    });
}



// For setting the marker image
function makeMarker(position, icon, vid, icon_label) {
    marker = new Marker({
        position: position,
        id: vid,
        icon: icon,
        map: map,
        //animation: google.maps.Animation.DROP,
        draggable: false,
        map_icon_label: icon_label
    });
    return marker;
}
var serach_veh = "";
function track_vehicle(sveh) {
    serach_veh = sveh;
    updateMarkers();
}

// Update all markers in the array by removing references to them and re-initialize the array.
function updateMarkers() {

    for (var i = 0; i < allMarkers.length; i++) {
        allMarkers[i].setMap(null);
    }
    allMarkers = [];

    if (typeof(markerCluster) != "undefined")
        markerCluster.clearMarkers();

    
	loadMap(vehicleList);
}

function reload_select_box(obj) {
    var other_veh = {};

    var opt = "<option value=''>Select</option><optgroup label='Free'>";
    $.each(obj.assignable, function (key, val) {
        if (val.status == "free") {
            pre_allot = typeof(obj.assigned[val.veh_id]) != "undefined" ? obj.assigned[val.veh_id] : "";
            opt += "<option data-subtext='" + pre_allot + "' value='" + val.veh_id + "' >" + val.veh_no + " - " + val.model + "</option>";
        }
        else {
            if (typeof(other_veh[val.status]) == "undefined") other_veh[val.status] = {};

            other_veh[val.status][val.veh_id] = val;
        }
    });
    opt += "</optgroup>";

    var order_val = assignable_order.split(",");

    $.each(order_val, function (ky, vl) {
        if (typeof(other_veh[vl]) != "undefined") {
            opt += "<optgroup label='" + vl + "'>";

            $.each(other_veh[vl], function (ik, iv) {
                pre_allot = typeof(obj.assigned[iv.veh_id]) != "undefined" ? obj.assigned[iv.veh_id] : "";
                opt += "<option data-subtext='" + pre_allot + "' value='" + iv.veh_id + "' >" + iv.veh_no + " - " + iv.model + "</option>";
            });

            opt += "</optgroup>";
        }
    });

    $("#allotted_vehicle_id").html(opt).selectpicker('refresh');
    //$("#allotted_vehicle_id").value('').selectpicker('refresh');
    //console.log(other_veh);
}

// For setting the marker image
function makePath(position, icon, icon_label) {
    marker = new Marker({
        position: position,
        icon: icon,
        map: map,
        //animation: google.maps.Animation.DROP,
        draggable: false,
        map_icon_label: icon_label
    });
    return marker;
}

$(function () {
    setTimeout(function () {
        $('#colors_display,#vech_select_for_map').show();
    }, 3000);
});