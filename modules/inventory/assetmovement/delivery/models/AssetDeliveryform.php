<?php

namespace app\modules\inventory\assetmovement\delivery\models;
use app\modules\event\eventInfo\models\EventDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Order form */
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;

/*Getting the Lines relationship*/
use app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut;

use Yii;

/**
 * This is the model class for table "asset_deliveryform".
 *
 * @property int $id
 * @property string $deliveryNumber
 * @property int $eventId
 * @property int $orderId
 * @property string $duedate
 * @property string $date
 * @property int $deliveryStatus
 * @property string $delivery
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $createdBy
 * @property int $approvedBy
 *
 * @property AssetCheckOut[] $assetCheckOuts
 * @property EventDetails $event
 * @property UserDetails $user
 * @property UserDetails $approvedBy0
 * @property EventOrderfrom $order
 * @property UserDetails $createdBy0
 */
class AssetDeliveryform extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

     /*Customer Details*/
     public $customerId;


    public static function tableName()
    {
        return 'asset_deliveryform';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deliveryNumber', 'eventId', 'orderId', 'duedate', 'date', 'deliveryStatus', 'userId', 'createdBy'], 'required'],
            [['eventId', 'orderId', 'deliveryStatus', 'userId', 'createdBy', 'approvedBy'], 'integer'],
            [['customerId','duedate', 'date', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['delivery'], 'string'],
            [['deliveryNumber'], 'string', 'max' => 100],
            [['deliveryNumber'], 'unique'],
            [['eventId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['eventId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => EventOrderfrom::className(), 'targetAttribute' => ['orderId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deliveryNumber' => 'Delivery Number',
            'customerId'=>'Customer',
            'eventId' => 'Event',
            'orderId' => 'Order',
            'duedate' => 'Return Date',
            'date' => 'Date',
            'deliveryStatus' => 'Delivery Status',
            'delivery' => 'Delivery',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'createdBy' => 'Created By',
            'approvedBy' => 'Approved By',
        ];
    }
    public function getAssetCheckoutList($id,$deliveryStatus){
      //Getting all the event Order list with the same status
      return AssetCheckOut::find()
                    ->where(['deliveryId'=>$id])
                    ->Andwhere(['status'=>$deliveryStatus])
                    ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['deliveryId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'eventId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(EventOrderfrom::className(), ['id' => 'orderId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /*Delivery number */
    public function getDevnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(deliveryNumber) AS number from asset_deliveryform where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "Deli-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "Deli-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "ORD-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }

    /*Get Hold Asset*/

    public function getallassetcheckoutsonhold($eventId)
    {
        $eventJina = AssetCheckOut::find()
            ->select(['*'])
            ->joinWith('asset')
            ->joinWith('location.loaction')
            ->joinWith('orderlist')
            ->joinWith('orderlist.item')
            ->joinWith('vehicle0')
            ->where(['asset_check_out.activityId' => $eventId])
            ->Andwhere(['asset_check_out.status'=>2])
            ->asArray()
            ->all();
        return $eventJina;

        // return self::find()
        //       ->select(['id','eventName as name'])
        //       ->where(['id' => $project_id])->indexBy('id')->column();
    }
}
