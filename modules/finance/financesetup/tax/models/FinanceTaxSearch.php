<?php

namespace app\modules\finance\financesetup\tax\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\financesetup\tax\models\FinanceTax;

/**
 * FinanceTaxSearch represents the model behind the search form of `app\modules\finance\financesetup\tax\models\FinanceTax`.
 */
class FinanceTaxSearch extends FinanceTax
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'taxStatus', 'userId'], 'integer'],
            [['taxCode', 'taxType', 'account', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['taxRate'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceTax::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'taxRate' => $this->taxRate,
            'taxStatus' => $this->taxStatus,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'taxCode', $this->taxCode])
            ->andFilterWhere(['like', 'taxType', $this->taxType])
            ->andFilterWhere(['like', 'account', $this->account]);

        return $dataProvider;
    }
}
