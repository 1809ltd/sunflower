<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventRequstionForm */

$this->title = 'Create Finance Event Requstion Form';
$this->params['breadcrumbs'][] = ['label' => 'Finance Event Requstion Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-requstion-form-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceEventRequstionFormList'=>$modelsFinanceEventRequstionFormList, //to enable Dynamic Form
    ]) ?>

</div>
