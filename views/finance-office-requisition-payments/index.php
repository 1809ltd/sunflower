<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Finance Office Requisition Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-office-requisition-payments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Finance Office Requisition Payments'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'officeReqPaymentPayType',
            'officeReqPaymentaccount',
            'reqrecepitNumber',
            'officeReqPaymentTotalAmt',
            //'unallocatedAmount',
            //'date',
            //'officeReqPaymentofficeReqRef',
            //'officeReqPaymentPrivateNote:ntext',
            //'officeReqPaymentCreatedAt',
            //'officeReqPaymentUpdatedAt',
            //'officeReqPaymentDeletedAt',
            //'userId',
            //'createdBy',
            //'approvedBy',
            //'paymentstatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
