<?php

namespace app\modules\customer\marketing\customerleadcommunicationtracker\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\customer\marketing\customerleadcommunicationtracker\models\CustomerLeadCommunicationTracker;

/**
 * CustomerLeadCommunicationTrackerSearch represents the model behind the search form of `app\modules\customer\marketing\customerleadcommunicationtracker\models\CustomerLeadCommunicationTracker`.
 */
class CustomerLeadCommunicationTrackerSearch extends CustomerLeadCommunicationTracker
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'leadAppointId', 'leadId', 'userId'], 'integer'],
            [['commLeadTxnDate', 'commLeadMeetingSubject', 'commLeadPartiesInvolved', 'commLeadFormOfCommunication', 'commLeadNoted', 'commLeadNextDate', 'commLeadCreatedAt', 'commLeadLastUpdatedAt', 'commLeadDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerLeadCommunicationTracker::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'leadAppointId' => $this->leadAppointId,
            'leadId' => $this->leadId,
            'commLeadTxnDate' => $this->commLeadTxnDate,
            'commLeadNextDate' => $this->commLeadNextDate,
            'commLeadCreatedAt' => $this->commLeadCreatedAt,
            'commLeadLastUpdatedAt' => $this->commLeadLastUpdatedAt,
            'commLeadDeletedAt' => $this->commLeadDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'commLeadMeetingSubject', $this->commLeadMeetingSubject])
            ->andFilterWhere(['like', 'commLeadPartiesInvolved', $this->commLeadPartiesInvolved])
            ->andFilterWhere(['like', 'commLeadFormOfCommunication', $this->commLeadFormOfCommunication])
            ->andFilterWhere(['like', 'commLeadNoted', $this->commLeadNoted]);

        return $dataProvider;
    }
}
