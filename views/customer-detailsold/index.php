<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\models\CustomerDetails;
use app\models\CustomerDetailsSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Details';
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Company</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="customer-details-index">

                <div class="table-responsive">

                    <p>
                        <?= Html::button('Create Customer Details', ['value'=>Url::to((['/customer-details/create'])),'class' => 'btn btn-success','id'=>'modalButton']) ?>
                    </p>
                    <?php
                    Modal::begin([
                      // 'header'=>'<h3>Add Customer</h3>',
                      'class'=>'modal-dialog',
                      'id'=>'modal',
                      'size'=>'modal-lg',
                    ]);

                    echo "<div class='modal-content' id='modalContent'></div>";

                    Modal::end();
                     ?>

                  <?php Pjax::begin(); ?>

                  <?=  GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
                          'rowOptions'=> function($model)
                          {
                            // code...
                            if ($model->customerstatus==1) {
                              // code...
                              return['class'=>'success'];
                            } else {
                              // code...
                              return['class'=>'danger'];
                            }

                          },
                          'columns' => [
                              ['class' => 'kartik\grid\ExpandRowColumn',
                                'value'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  return GridView::ROW_COLLAPSED;
                                },
                                'detail'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  $searchModel = new CustomerDetailsSearch();
                                  $searchModel->id = $model->id;
                                  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                                  return Yii::$app->controller->renderPartial('_viewcustomerdetails',[
                                    'searchModel' => $searchModel,
                                    'dataProvider' => $dataProvider,

                                  ]);
                                },
                            ],

                            // 'id',
                            'customerNumber',
                            'customerFullname',
                            'customerCompanyName',
                            'customerKraPin',
                            'customerDisplayName',
                            'customerPrimaryPhone',
                            'customerPrimaryEmail:email',
                            // 'customerstatus',
                            // 'customerBillWithParent',
                            // 'customerParentId',
                            // 'customercity',
                            // 'customerSuite',
                            // 'customerCounty',
                            // 'customerCountry',
                            // 'customerPostalCode',
                            // 'customerAddress',
                            // 'customerCreateTime',
                            // 'customerUpdatedAt',
                            // 'customerDeleteAt',
                            // 'userId',

                            ['class' => 'yii\grid\ActionColumn'],
                          ],
                      ]);
                      ?>

                </div>

                <?php Pjax::end(); ?>
             </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
<?php
/*Pop up modal Js*/
// $script = <<< JS
//     $(function() {
//       $('#modalButton').click(function() {
//         // $('#modal').modal('show').find('.modalContent')
//         // .load($(this).attr('value'));
//         $('#modal').modal('show')
//           .find('#modalContent')
//           .load($(this).attr('value'));
//       });
//     });
// JS;
// $this->registerJs($script);
?>
