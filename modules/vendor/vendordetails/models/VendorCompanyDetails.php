<?php

namespace app\modules\vendor\vendordetails\models;

use app\models\UserDetails;

use Yii;

/**
 * This is the model class for table "vendor_company_details".
 *
 * @property int $id
 * @property string $vendorCompanyName
 * @property string $vendorKraPin
 * @property string $vendorContactPersonFullName
 * @property string $contactMobilePhone
 * @property string $vendorPhone
 * @property string $contactEmail
 * @property string $vendoremail
 * @property string $vendorCompanyAddress
 * @property string $vendorCompanySuite
 * @property string $vendorCompanyCity
 * @property string $vendorCompanyCounty
 * @property string $vendorCompanyPostal
 * @property string $vendorCompanyCountry
 * @property string $vendorCompanyLogo
 * @property string $vendorTwitter
 * @property string $vendorFb
 * @property string $vendorIg
 * @property string $vendorType
 * @property int $vendorCompanyStatus
 * @property string $vendorCompanyTimestamp
 * @property string $vendorCompanyupdatedAt
 * @property string $vendorCompanyDeletedAt
 * @property int $companyUserId
 *
 * @property EventOrderfromList[] $eventOrderfromLists
 * @property EventTransport[] $eventTransports
 * @property FinanceBills[] $financeBills
 * @property FinancePurchaseOrder[] $financePurchaseOrders
 * @property FinanceSunflowerPurchaseOrder[] $financeSunflowerPurchaseOrders
 * @property FinanceVendorCreditMemo[] $financeVendorCreditMemos
 * @property FinanceVendorCreditMemoLines[] $financeVendorCreditMemoLines
 * @property UserDetails $companyUser
 */
class VendorCompanyDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendor_company_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorCompanyName', 'vendorType', 'vendorCompanyStatus', 'companyUserId'], 'required'],
            [['vendorCompanyLogo'], 'string'],
            [['vendorCompanyStatus', 'companyUserId'], 'integer'],
            [['vendorCompanyTimestamp', 'vendorCompanyupdatedAt', 'vendorCompanyDeletedAt'], 'safe'],
            [['vendorCompanyName', 'vendorContactPersonFullName', 'vendorCompanyAddress', 'vendorCompanySuite', 'vendorCompanyCity', 'vendorCompanyCounty', 'vendorCompanyPostal', 'vendorCompanyCountry', 'vendorType'], 'string', 'max' => 255],
            [['vendorKraPin', 'contactMobilePhone', 'vendorPhone'], 'string', 'max' => 20],
            [['contactEmail', 'vendoremail', 'vendorTwitter', 'vendorFb', 'vendorIg'], 'string', 'max' => 50],
            [['vendorCompanyName'], 'unique'],
            // [['vendorKraPin'], 'unique'],
            [['companyUserId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['companyUserId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'vendorCompanyName' => 'Supplier Name',
          'vendorKraPin' => 'Kra Pin',
          'vendorContactPersonFullName' => 'Contact Person',
          'contactMobilePhone' => 'Contact Phone',
          'vendorPhone' => 'Supplier Phone',
          'contactEmail' => 'Contact Email',
          'vendoremail' => 'Supplier email',
          'vendorCompanyAddress' => 'Address',
          'vendorCompanySuite' => 'Suite',
          'vendorCompanyCity' => 'City',
          'vendorCompanyCounty' => 'County',
          'vendorCompanyPostal' => 'Postal',
          'vendorCompanyCountry' => 'Country',
          'vendorCompanyLogo' => 'Logo',
          'vendorTwitter' => 'Twitter',
          'vendorFb' => 'Fb',
          'vendorIg' => 'Ig',
          'vendorType' => 'Type',
          'vendorCompanyStatus' => 'Status',
          'vendorCompanyTimestamp' => 'Timestamp',
          'vendorCompanyupdatedAt' => 'updated At',
          'vendorCompanyDeletedAt' => 'Deleted At',
          'companyUserId' => 'Company User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfromLists()
    {
        return $this->hasMany(EventOrderfromList::className(), ['vendorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventTransports()
    {
        return $this->hasMany(EventTransport::className(), ['vendorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBills()
    {
        return $this->hasMany(FinanceBills::className(), ['vendorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePurchaseOrders()
    {
        return $this->hasMany(FinancePurchaseOrder::className(), ['vendorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceSunflowerPurchaseOrders()
    {
        return $this->hasMany(FinanceSunflowerPurchaseOrder::className(), ['vendorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemos()
    {
        return $this->hasMany(FinanceVendorCreditMemo::className(), ['vendorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemoLines()
    {
        return $this->hasMany(FinanceVendorCreditMemoLines::className(), ['vendorId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'companyUserId']);
    }
}
