<?php
use yii\helpers\Url;

use app\models\AllTransactions;
// Event Details
use app\modules\event\eventInfo\models\EventDetails;

/* @var $this yii\web\View */

/*Asset*/
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;

// $this->title = 'Sunflower Management System';
$leo= date('Y-m-d H:i:s');
// Get RandomColours
function random_color()
{
  $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
  $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
  return $color;
}
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Event</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <!-- =========================================================== -->

      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Upcoming Orders Forms</span>
              <span class="info-box-number">..</span>

              <!-- <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Successful Events</span>
              <span class="info-box-number"><?php
              $doneEvent=EventDetails::find()->where(['<=', 'eventStartDate', $leo])
                                                    ->count('event_details.id');
              echo $doneEvent;
              ?></span>

              <!-- <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Upcoming Events</span>
              <span class="info-box-number">

                <?php
                $upcoimingEvent=EventDetails::find()->where(['>=', 'eventStartDate', $leo])
                                                      ->count('event_details.id');
                                                      echo $upcoimingEvent;

                 ?>
               </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pending Worklist</span>
              <span class="info-box-number">...</span>

              <!-- <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- =========================================================== -->


      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Event Calender</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12 col-sm-8">
                  <div class="pad">
                    <!-- Event Will be displayed here -->
                      <!-- THE CALENDAR -->
                      <div>
                        <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                            'events'=> $events,
                        ));
                       ?>
                      </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="row">


          </div>
          <!-- /.row -->

          <!-- TABLE: Upcoming Events ORDERS -->
          <div class="box box-info">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Upcoming Event ans Status</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div> -->
            <!-- /.box-header -->
            <!-- <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Event Number</th>
                    <th>Event Name</th>
                    <th>Status</th>
                    <th>Popularity</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div> -->
              <!-- /.table-responsive -->
            <!-- </div> -->
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div> -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->

          <!-- BAR CHART -->
        <!-- <div class="box box-success"> -->
          <!-- <div class="box-header with-border">
            <h3 class="box-title">Expense and Income</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart">
              <canvas id="barChart" style="height:230px"></canvas>
            </div>
          </div> -->
          <!-- /.box-body -->
        <!-- </div> -->
        <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4">
          <!-- Add Event -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <!-- <h3 class="box-title"> </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <button type="button" class="btn btn-block btn-success btn-lg">Add Event</button>
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
          <!-- /.info-box -->


            <!-- Upcoming Event  LIST -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Upcoming Event - <?= $upcoimingEvent;?></h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <?php
                $nextEvents=EventDetails::find()
                                        ->where(['>=', 'eventStartDate', $leo])
                                        ->all();

                // print_r($nextEvents);

                foreach ($nextEvents as $nextEvents) {
                  // code...
                  ?>
                  <a href="#">
                    <div class="box box-solid">
                    <div class="box-header with-border">
                      <i class="fa fa-text-width"></i>

                      <h3 class="box-title"><?= $nextEvents['eventName']?></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <blockquote>
                        <p><?= $nextEvents['eventCategory0']['eventCatName']?> - .</p>
                        <small>Customer -<cite title="Source Title"> <?= $nextEvents['customer']['customerDisplayName']?></cite></small>
                      </blockquote>
                    </div>
                    <!-- /.box-body -->
                  </a>
                  <!-- /.box -->
                </div>
              </a>

                  <?php
                }

                ?>

              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="javascript:void(0)" class="uppercase">View All Event</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Category Events</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="chart-responsive">
                      <canvas id="pieChart" height="250"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-4">
                    <ul class="chart-legend clearfix">
                      <?php
                      /*Event Category Chart*/
                      $piecharteventcategorynumbdata = '';
                      // Getting Categories
                      $numbereventCategory=EventDetails::find()
                                            ->select(['Count(event_details.eventCategory) as caterogiesNumber','event_details.eventCategory','event_caterogies.eventCatName'])
                                            ->where("event_details.`status` > 1")
                                            ->groupBy(['event_details.eventCategory'])
                                            ->innerJoinWith('eventCategory0')
                                            ->all();

                      $errors = array_filter($numbereventCategory);
                      // Checking if the array is empty
                      if (!empty($errors)) {
                        // code...
                        foreach ($numbereventCategory as $numbereventCategory) {
                          // code...

                          // Getting the Colour of event Categories
                          $eventcolour = random_color();

                          // Assigning Values of the to data of chart
                          $piecharteventcategorynumbdata .= "{ value:".$numbereventCategory['caterogiesNumber'].",color:'".$eventcolour."',highlight:'".$eventcolour."',label:'".$numbereventCategory['eventCategory0']['eventCatName']."'},";

                          ?>
                          <li><i class="fa fa-circle-o" style="color:<?=$eventcolour;?>"></i> <?= $numbereventCategory['eventCategory0']['eventCatName']."- ".number_format($numbereventCategory['caterogiesNumber'],2)?></li>
                          <?php
                        }
                      }
                      ?>
                    </ul>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer no-padding">
                <ul class="nav nav-pills nav-stacked">
                  <!-- <li><a href="#">United States of America
                    <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                  <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                  </li>
                  <li><a href="#">China
                    <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li> -->
                </ul>
              </div>
              <!-- /.footer -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <?php
    $script = <<<JS
    $(function () {
      /* ChartJS
       * -------
       * Here we will create a few charts using ChartJS
       */
      //-------------
      //- PIE CHART -
      //-------------
      // Get context with jQuery - using jQuery's .get() method.
      var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
      var pieChart       = new Chart(pieChartCanvas)
      var PieData        = [$piecharteventcategorynumbdata]
      var pieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions)
    })
JS;
$this->registerJs($script);
?>
