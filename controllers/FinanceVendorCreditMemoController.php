<?php

namespace app\controllers;

use Yii;
use app\models\FinanceVendorCreditMemo;
use app\models\FinanceVendorCreditMemoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\models\FinanceVendorCreditMemoLines;
use app\models\FinanceVendorCreditMemoLinesSearch;


/**
 * FinanceVendorCreditMemoController implements the CRUD actions for FinanceVendorCreditMemo model.
 */
class FinanceVendorCreditMemoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->vendorCreditMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->vendorCreditMemoUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceVendorCreditMemo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new FinanceVendorCreditMemoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceVendorCreditMemo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceVendorCreditMemo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new FinanceVendorCreditMemo();

        $modelsFinanceVendorCreditMemoLines = [new FinanceVendorCreditMemoLines];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsFinanceVendorCreditMemoLines = Model::createMultiple(FinanceVendorCreditMemoLines::classname());
          Model::loadMultiple($modelsFinanceVendorCreditMemoLines, Yii::$app->request->post());


          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFinanceVendorCreditMemoLines),
                    ActiveForm::validate($model)
                );
            }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceVendorCreditMemoLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceVendorCreditMemoLines as $modelFinanceVendorCreditMemoLines) {

                        $modelFinanceVendorCreditMemoLines->vendorCreditMemoId = $model->id;
                        $modelFinanceVendorCreditMemoLines->userId = $model->userId;
                        $modelFinanceVendorCreditMemoLines->vendorCreditMemoLineStatus = $model->vendorCreditMemoStatus;
                        $modelFinanceVendorCreditMemoLines->vendorId = $model->vendorId;

                          if (! ($flag = $modelFinanceVendorCreditMemoLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }


        }

        return $this->render('create', [
            'model' => $model,
              'modelsFinanceVendorCreditMemoLines' => (empty($modelsFinanceVendorCreditMemoLines)) ? [new FinanceVendorCreditMemoLines] : $modelsFinanceVendorCreditMemoLines
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Updates an existing FinanceVendorCreditMemo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        $modelsFinanceVendorCreditMemoLines = $this->getFinanceVendorCreditMemoLines($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $oldIDs = ArrayHelper::map($modelsFinanceVendorCreditMemoLines, 'id', 'id');
          $modelsFinanceVendorCreditMemoLines = Model::createMultiple(FinanceVendorCreditMemoLines::classname(), $modelsFinanceVendorCreditMemoLines);
          Model::loadMultiple($modelsFinanceVendorCreditMemoLines, Yii::$app->request->post());
          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceVendorCreditMemoLines, 'id', 'id')));

          // ajax validation
          if (Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ArrayHelper::merge(
                  ActiveForm::validateMultiple($modelsFinanceVendorCreditMemoLines),
                  ActiveForm::validate($model)
              );
          }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceVendorCreditMemoLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      if (! empty($deletedIDs)) {
                          FinanceVendorCreditMemoLines::deleteAll(['id' => $deletedIDs]);
                      }
                      foreach ($modelsFinanceVendorCreditMemoLines as $modelFinanceVendorCreditMemoLines) {
                          $modelFinanceVendorCreditMemoLines->vendorCreditMemoId = $model->id;
                          if (! ($flag = $modelFinanceVendorCreditMemoLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
              }
          }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsFinanceVendorCreditMemoLines' => (empty($modelsFinanceVendorCreditMemoLines)) ? [new FinanceVendorCreditMemoLines] : $modelsFinanceVendorCreditMemoLines
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('update', [
        //     'model' => $model,
        // ]);
    }
    public function getFinanceVendorCreditMemoLines($id)
    {
      $model = FinanceVendorCreditMemoLines::find()->where(['vendorCreditMemoId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing FinanceVendorCreditMemo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceVendorCreditMemo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceVendorCreditMemo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceVendorCreditMemo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
