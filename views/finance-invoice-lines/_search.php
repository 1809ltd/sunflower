<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoiceLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-invoice-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'invoiceId') ?>

    <?= $form->field($model, 'itemRefId') ?>

    <?= $form->field($model, 'itemRefName') ?>

    <?= $form->field($model, 'invoiceLinesDescription') ?>

    <?php // echo $form->field($model, 'invoiceLinespack') ?>

    <?php // echo $form->field($model, 'invoiceLinesQty') ?>

    <?php // echo $form->field($model, 'invoiceLinesUnitPrice') ?>

    <?php // echo $form->field($model, 'invoiceLinesAmount') ?>

    <?php // echo $form->field($model, 'invoiceLinesTaxCodeRef') ?>

    <?php // echo $form->field($model, 'invoiceLinesTaxamount') ?>

    <?php // echo $form->field($model, 'invoiceLinesDiscount') ?>

    <?php // echo $form->field($model, 'invoiceLinesCreatedAt') ?>

    <?php // echo $form->field($model, 'invoiceLinesUpdatedAt') ?>

    <?php // echo $form->field($model, 'invoiceLinesDeletedAt') ?>

    <?php // echo $form->field($model, 'invoiceLinesStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
