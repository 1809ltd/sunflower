<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AssetCategory */

$this->title = 'Create Asset Category';
$this->params['breadcrumbs'][] = ['label' => 'Asset Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
