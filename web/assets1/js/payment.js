"use strict";
var ass = function () {
    "use strict";
    $(document).ready(function () {
        var table = "";

        table = $('#table').DataTable({
            "fnDrawCallback": function () {

                $('.text_edit').editable({
                    success : function (response, newValue) {
                    },
                    validate: function (value) {
                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    }
                });

                $('[data-type="text"]').editable({
                    validate: function (value) {
                        if ($.trim(value) == '') {
                            return 'This field is required';
                        }
                    }
                });

                $('[data-collection-type]').editable({
                    source : collection_type,
                    success: function (response, newValue) {

                    }
                });

                $('[data-collection-based-on]').editable({
                    source: collection_based_on,
                });

                $('[data-payment-type]').editable({
                    source: payment_type,
                });

                $('[data-payment-by]').editable({
                    source: payment_by,
                });
            },
            "order"         : [ [ 0, 'asc' ] ]
        });
    });
};
var Assign = function () {
    "use strict";
    return {
        init: function () {
            ass();
        }
    }
}();
$(function () {
    Assign.init();
});