"use strict";
var pik = function () {
    "use strict";
    $(document).ready(function () {
        $('#cp2').colorpicker();
    });
};
var regular = function () {
    "use strict";
    return {
        init: function () {
            pik();
        }
    }
}();
$(function () {
    regular.init();
});