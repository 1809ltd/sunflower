<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerLead;

/**
 * CustomerLeadSearch represents the model behind the search form of `app\models\CustomerLead`.
 */
class CustomerLeadSearch extends CustomerLead
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customerId', 'userId'], 'integer'],
            [['leadprojectName', 'leadProjectclassification', 'leadGenerator', 'leadReferredby', 'leadReferredbyRelation', 'leadCreatedAt', 'leadLastUpdatedAt', 'leadDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerLead::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customerId' => $this->customerId,
            'leadCreatedAt' => $this->leadCreatedAt,
            'leadLastUpdatedAt' => $this->leadLastUpdatedAt,
            'leadDeletedAt' => $this->leadDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'leadprojectName', $this->leadprojectName])
            ->andFilterWhere(['like', 'leadProjectclassification', $this->leadProjectclassification])
            ->andFilterWhere(['like', 'leadGenerator', $this->leadGenerator])
            ->andFilterWhere(['like', 'leadReferredby', $this->leadReferredby])
            ->andFilterWhere(['like', 'leadReferredbyRelation', $this->leadReferredbyRelation]);

        return $dataProvider;
    }
}
