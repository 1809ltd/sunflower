<?php

namespace app\modules\inventory\assetsetup\assetcategorytype;

/**
 * assetcategorytype module definition class
 */
class assetcategorytype extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\assetsetup\assetcategorytype\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
