<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Company Reports';
$this->params['breadcrumbs'][] = ['label' => 'Finance Dashboard', 'url' => ['/finance']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Main content -->
    <section class="content">

      <!-- START pf Report Page -->
      <h2 class="page-header">Financial Related Report</h2>

      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['/finance/report/company/p&l']))?>">
            <!-- <a href="<?= Url::to((['income']))?>"> -->
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Income Statement</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>Income minus expenses; tells you if you brought in more than you spent this period.</p>
                <small>Profit<cite title="Source Title"> & Loss</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>
        <!-- ./col -->

          <div class="col-md-6">
            <a href="<?= Url::to((['/finance/report/company/balance-sheet']))?>">
              <div class="box box-solid">
              <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Balance Sheet</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <blockquote>
                  <p>Snapshot of what your business owns or is due to receive from others (assets), what it owes to others (liabilities), and what you've invested or retained in your company (equity).</p>
                  <small>Pending<cite title="Source Title"> Bills</cite></small>
                </blockquote>
              </div>
              <!-- /.box-body -->
            </a>
            <!-- /.box -->
          </div>
        </a>
        </div>
          <!-- ./col -->

      </div>
      <!-- /.row -->

            <div class="row">
              <div class="col-md-6">
                <a href="<?= Url::to((['/finance/report/company/tax-report']))?>">
                  <!-- <a href="<?= Url::to((['income']))?>"> -->
                  <div class="box box-solid">
                  <div class="box-header with-border">
                    <i class="fa fa-text-width"></i>

                    <h3 class="box-title">Tax Statement</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <blockquote>
                      <p>See the tax you Recived and collected.</p>
                      <small>from <cite title="Source Title"> Bill and Invoices</cite></small>
                    </blockquote>
                  </div>
                  <!-- /.box-body -->
                </a>
                <!-- /.box -->
              </div>
            </a>
            </div>
              <!-- ./col -->

                <!-- <div class="col-md-6">
                  <a href="<?= Url::to((['/finance/report/company/balance-sheet']))?>">
                    <div class="box box-solid">
                    <div class="box-header with-border">
                      <i class="fa fa-text-width"></i>
                      <h3 class="box-title">Balance Sheet</h3>
                    </div> -->
                    <!-- /.box-header -->
                    <!-- <div class="box-body">
                      <blockquote>
                        <p>Snapshot of what your business owns or is due to receive from others (assets), what it owes to others (liabilities), and what you've invested or retained in your company (equity).</p>
                        <small>Pending<cite title="Source Title"> Bills</cite></small>
                      </blockquote>
                    </div> -->
                    <!-- /.box-body -->
                  <!-- </a> -->
                  <!-- /.box -->
                <!-- </div>
              </a>
              </div> -->
                <!-- ./col -->

            </div>
            <!-- /.row -->
      <!-- START pf Report Page -->
      <h2 class="page-header">Customer Financial Related Report</h2>
      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['agedreciveables']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Aged Receivables</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>See how much money is expected to come in, and how long you've been waiting for it.</p>
                <small>Pending<cite title="Source Title"> Invoices</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>

      <div class="col-md-6">
          <a href="<?= Url::to((['/finance/report/customer']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Customer Statement</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>View Customer Statement all invoices and respective income.</p>
                <small>All<cite title="Source Title"> Transaction</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <a href="<?= Url::to((['incomebycustomer']))?>">
          <div class="box box-solid">
          <div class="box-header with-border">
            <i class="fa fa-text-width"></i>

            <h3 class="box-title">Income by Customer</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <blockquote>
              <p>See the income you received, broken down by source.</p>
              <small>All<cite title="Source Title"> Invoices</cite></small>
            </blockquote>
          </div>
          <!-- /.box-body -->
        </a>
        <!-- /.box -->
      </div>
    </a>
    </div>

    <div class="col-md-6">
        <!-- <a href="<?= Url::to((['incomebycustomer']))?>"> -->
          <!-- <div class="box box-solid"> -->
          <!-- <div class="box-header with-border">
            <i class="fa fa-text-width"></i>

            <h3 class="box-title">Income by Customer</h3>
          </div> -->
          <!-- /.box-header -->
          <!-- <div class="box-body">
            <blockquote>
              <p>See the income you received, broken down by source.</p>
              <small>All<cite title="Source Title"> Invoices</cite></small>
            </blockquote>
          </div> -->
          <!-- /.box-body -->
        </a>
        <!-- /.box -->
      <!-- </div> -->
    <!-- </a> -->
    </div>
  </div>
    <!-- /.row -->
    <!-- START pf Report Page -->
    <h2 class="page-header">Supplier Financial Related Report</h2>

    <div class="row">
      <div class="col-md-6">
          <a href="<?= Url::to((['agedpayables']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Aged Payables</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>See the expenses you haven't paid yet, and how long payment has been outstanding.</p>
                <small>Pending<cite title="Source Title"> Bills</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
        </a>
      </div>
        <!-- ./col -->
        <div class="col-md-6">
            <a href="<?= Url::to((['billsbyvendor']))?>">
              <div class="box box-solid">
              <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Expense by Vendor</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <blockquote>
                  <p>See what you paid in expenses, broken down by recipient.</p>
                  <small>All<cite title="Source Title"> Bills</cite></small>
                </blockquote>
              </div>
              <!-- /.box-body -->
            </a>
            <!-- /.box -->
          </div>
        </a>
        </div>
          <!-- ./col -->


      </div>
      <!-- /.row -->


      <div class="row">
        <!-- <div class="col-md-6"> -->
            <!-- <a href="<?= Url::to((['agedpayables']))?>"> -->
              <!-- <div class="box box-solid"> -->
              <!-- <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Aged Payables</h3>
              </div> -->
              <!-- /.box-header -->
              <!-- <div class="box-body">
                <blockquote>
                  <p>See the expenses you haven't paid yet, and how long payment has been outstanding.</p>
                  <small>Pending<cite title="Source Title"> Bills</cite></small>
                </blockquote>
              </div> -->
              <!-- /.box-body -->
            <!-- </a> -->
            <!-- /.box -->
          <!-- </div> -->
          <!-- </a> -->
        <!-- </div> -->
          <!-- ./col -->
          <div class="col-md-6">
              <a href="<?= Url::to((['/finance/report/vendor']))?>">
                <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-text-width"></i>
                  <h3 class="box-title">Supplier Statement</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <blockquote>
                    <p>See what you paid in expenses, broken down by recipient.</p>
                    <small>All<cite title="Source Title"> Bills</cite></small>
                  </blockquote>
                </div>
                <!-- /.box-body -->
              </a>
              <!-- /.box -->
            </div>
          </a>
          </div>
            <!-- ./col -->


        </div>
        <!-- /.row -->

      <h2 class="page-header">Other Report</h2>
      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['/finance/report/company/cash-flow']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Cash Flow</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>Is the net amount of cash and cash-equivalents being transferred into and out of a business.</p>
                <small>All<cite title="Source Title"> Income Payment and Expense Payment</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>

      <!-- ./col -->
      <div class="col-md-6">
        <!-- <a href="<?= Url::to((['']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Account Transactions</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- <div class="box-body">
              <blockquote>
                <p>See the transactions that occurred in each account.</p>
                <small>summary<cite title="Source Title"> details</cite></small>
              </blockquote>
            </div> -->
            <!-- /.box-body -->
          <!-- </a> -->
          <!-- /.box -->
        <!-- </div> -->
      <!-- </a> -->
    </div>
          <!-- ./col -->


      </div>
      <!-- /.row -->

      <!-- <h2 class="page-header">Event Financial Related Report</h2>
      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['event']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Event Financial Report</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- <div class="box-body">
              <blockquote>
                <p>Event Financial Statement.</p>
                <small>All<cite title="Source Title"> Event Financial Transaction</cite></small>
              </blockquote>
            </div> -->
            <!-- /.box-body -->
          <!-- </a> -->
          <!-- /.box -->
        <!-- </div>
      </a>
      </div> -->

      <!-- ./col -->
      <div class="col-md-6">
        <!-- <a href="<?= Url::to((['']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>
              <h3 class="box-title">Account Transactions</h3>
            </div> -->
            <!-- /.box-header -->
            <!-- <div class="box-body">
              <blockquote>
                <p>See the transactions that occurred in each account.</p>
                <small>summary<cite title="Source Title"> details</cite></small>
              </blockquote>
            </div> -->
            <!-- /.box-body -->
          <!-- </a> -->
          <!-- /.box -->
        <!-- </div> -->
      <!-- </a> -->
    </div>
          <!-- ./col -->


      </div>
      <!-- /.row -->




      <!-- END TYPOGRAPHY -->

    </section>
    <!-- /.content -->
