<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */

$this->title = 'Create Asset Check Out';
$this->params['breadcrumbs'][] = ['label' => 'Asset Check Outs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-check-out-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
