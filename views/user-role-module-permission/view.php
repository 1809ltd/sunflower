<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userrolemodulepermission\models\UserRoleModulePermission */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Role Module Permissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-role-module-permission-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'roleId',
            'moduleId',
            'new',
            'view',
            'save',
            'remove',
            'createdBy',
            'createdAt',
            'updatedAt',
            'deletedAt',
            'userId',
            'status',
        ],
    ]) ?>

</div>
