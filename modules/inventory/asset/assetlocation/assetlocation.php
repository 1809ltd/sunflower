<?php

namespace app\modules\inventory\asset\assetlocation;

/**
 * assetlocation module definition class
 */
class assetlocation extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\asset\assetlocation\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
