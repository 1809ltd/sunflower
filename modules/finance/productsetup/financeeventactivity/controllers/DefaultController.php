<?php

namespace app\modules\finance\productsetup\financeeventactivity\controllers;

use yii\web\Controller;

/**
 * Default controller for the `financeeventactivity` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
