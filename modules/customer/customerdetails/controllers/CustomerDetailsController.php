<?php

namespace app\modules\customer\customerdetails\controllers;

use Yii;
use app\modules\customer\customerdetails\models\CustomerDetails;
use app\modules\customer\customerdetails\models\CustomerDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;


/*Getting All the Customer Related Fields*/
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\finance\income\invoice\models\FinanceInvoice;
use app\modules\finance\income\estimate\models\FinanceEstimate;


/**
 * CustomerDetailsController implements the CRUD actions for CustomerDetails model.
 */
class CustomerDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->customerUpdatedAt = new \yii\db\Expression('NOW()');
        /**/

      }else {
        // code...
        $this->customerUpdatedAt = new \yii\db\Expression('NOW()');
      }
      return parent::beforeSave();
    }


    /**
     * Lists all CustomerDetails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new CustomerDetailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerDetails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $this->layout = '@app/views/layouts/addformdatatablelayout';

        /*Getting all Clients Events*/
        $queryEvent = EventDetails::find();
        $queryEvent->andFilterWhere(['customerId' => $id]);
        $dataProviderEvent = new ActiveDataProvider([
            'query' => $queryEvent,
            'pagination'=> ['defaultPageSize' => 50],
        ]);

        /*Getting All Invoices*/
        $queryInvoice = FinanceInvoice::find();
        $queryInvoice->andFilterWhere(['customerId' => $id]);
        $dataProviderInvoice = new ActiveDataProvider([
            'query' => $queryInvoice,
            'pagination'=> ['defaultPageSize' => 50],
        ]);

        /*Getting All Quotes*/
        $queryQuotes = FinanceEstimate::find();
        $queryQuotes->andFilterWhere(['customerId' => $id]);

        $dataProviderQuotes = new ActiveDataProvider([
            'query' => $queryQuotes,
            'pagination'=> ['defaultPageSize' => 50],
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelEvents' => $dataProviderEvent,
            'modelQuotes' => $dataProviderQuotes,
            'modelInvoice' => $dataProviderInvoice,
        ]);
    }

    /**
     * Creates a new CustomerDetails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerDetails();

        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new CustomerDetails();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
          // code...
          Yii::$app->response->format='json';
          return ActiveForm::validate($model);
        }

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Geting the time created Timestamp
          $model->customerCreateTime= new \yii\db\Expression('NOW()');

          //Generating Customer Number
          $customerNum=$model->getCustomerNumber();
          $model->customerNumber= $customerNum;
          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;

          $model->createdBy = Yii::$app->user->identity->id;

          // echo $model->userId."".$model->createdBy."".$model->customerNumber."".$model->customerCreateTime;

          $model->save(false);

          return $this->redirect(['index']);

        } else {
          // code...

          //load the create form
          return $this->renderAjax('create', [
              'model' => $model,
          ]);
        }

    }

    /**
     * Updates an existing CustomerDetails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;

          $model->save();

          //after Saving the customer Details

          return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->renderAjax('update', [
              'model' => $model,
          ]);
        }

    }

    /**
     * Deletes an existing CustomerDetails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        // $model = $this->findModel($id);

        $this->findModel($id)->updateAttributes(['customerstatus'=>0,'customerDeleteAt' => new \yii\db\Expression('NOW()')]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerDetails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerDetails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        if (($model = CustomerDetails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
