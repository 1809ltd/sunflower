<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventItemCategoryType */

$this->title = 'Create Event Service Category Type';
$this->params['breadcrumbs'][] = ['label' => 'Event Service Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-item-category-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
