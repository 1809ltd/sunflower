<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Event Categories*/
use app\modules\event\eventsetup\eventcaterogies\models\EventCaterogies;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyDepartment */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Customer Leads</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="customer-lead-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(CustomerDetails::find()->all(),'id','customerDisplayName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Customer ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                  </div>
                </div>

                <div class="col-sm-3">
                  <div class="form-group">

                    <?= $form->field($model, 'leadprojectName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'leadProjectclassification')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(EventCaterogies::find()->all(),'eventCatName','eventCatName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Category Name ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>

                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'leadGenerator')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                      <?= $form->field($model, 'leadReferredby')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= $form->field($model, 'leadReferredbyRelation')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>


                  </div>
                </div>
                <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>
                <div class="col-sm-3">
                  <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>

                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
