/*Customer Income Generated*/
CREATE VIEW `event_sherehe`.`customerIncome` AS SELECT
finance_invoice.customerId AS customerId,
customer_details.customerNumber AS customerNumber,
customer_details.customerKraPin,
customer_details.customerDisplayName,
finance_invoice_lines.invoiceLinesAmount,
finance_invoice_lines.invoiceLinesTaxamount,
finance_invoice_lines.invoiceLinesTaxCodeRef,
finance_invoice_lines.itemRefId,
finance_invoice_lines.itemRefName,
finance_invoice_lines.invoiceId,
finance_invoice_lines.id AS invoicelineId,
finance_invoice_lines.invoiceLinesDescription,
finance_invoice_lines.invoiceLinesQty,
finance_invoice_lines.invoiceLinesUnitPrice,
finance_invoice_lines.invoiceLinesCreatedAt,
finance_invoice_lines.invoiceLinesDiscount,
finance_invoice.invoiceTxnDate AS txnDate,
finance_invoice_lines.invoiceLinesStatus,
event_details.eventNumber,
event_caterogies.eventCatName,
event_caterogies.id AS catId
FROM
	finance_invoice_lines
	INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
	LEFT JOIN customer_details ON finance_invoice.customerId = customer_details.id
	INNER JOIN event_details ON event_details.customerId = customer_details.id
	AND finance_invoice.invoiceProjectId = event_details.id
	INNER JOIN event_caterogies ON event_details.eventCategory = event_caterogies.id
