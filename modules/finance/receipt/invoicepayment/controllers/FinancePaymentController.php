<?php
namespace app\modules\finance\receipt\invoicepayment\controllers;

use Yii;
use app\modules\finance\receipt\invoicepayment\models\FinancePayment;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentSearch;
// Invoices Paid
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLinesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinancePaymentController implements the CRUD actions for FinancePayment model.
 */
class FinancePaymentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinancePayment models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new FinancePaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinancePayment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinancePayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new FinancePayment();

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...
          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;
          $model->createdBy= Yii::$app->user->identity->id;
          $model->invoicePaymentCreatedAt= new \yii\db\Expression('NOW()');
          // $deliveryNumber=$model->getDevnumber();
          // $model->deliveryNumber= $deliveryNumber;
          // print_r($model->attributes);
          // die();
          $model->save();
          // code...
          if (!empty($model->id)) {
            // code...
            $invoicepaymentlines = FinancePaymentLines::find()
                                          ->Where('status = :status', [':status' => 3])
                                          ->all();

            foreach ($invoicepaymentlines as $invoicepaymentlines) {
              // code...
              // Update all the data based on the payment made and status
              FinancePaymentLines::findOne($invoicepaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus,'invoicepaymentId' =>$model->id]);
            }
            //after Saving the Payment Items Details
            /*After an Update to check out table*/
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['create']);
          }
        } else {
          // code...
          return $this->render('create', [
            'model' => $model,
          ]);
        }
    }

    /**
     * Updates an existing FinancePayment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = $this->findModel($id);

      //checking if its a post
      if ($model->load(Yii::$app->request->post())) {
        // code...
        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        $model->save();
        // code...

        if (!empty($model->id)) {
          // code...
          $invoicepaymentlines = FinancePaymentLines::find()
                                        ->Where('invoicepaymentId = :invoicepaymentId', [':invoicepaymentId' => $model->id])
                                        ->all();

         foreach ($invoicepaymentlines as $invoicepaymentlines) {
           // code...
           // Update all the data based on the payment made and status
           FinancePaymentLines::findOne($invoicepaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus]);
         }
         //after Saving the Payment Items Details
         /*After an Update to check out table*/
         return $this->redirect(['view', 'id' => $model->id]);
         // return $this->redirect(['create']);
       }
     } else {
       // code...
       return $this->render('update', [
         'model' => $model,
       ]);
     }
   }

    /**
     * Deletes an existing FinancePayment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $this->findModel($id)->updateAttributes(['paymentstatus'=>0,'invoicePaymentDeletedAt' => new \yii\db\Expression('NOW()')]);
      // Confriming that the post is not empty
      if (!empty($id)) {
        // code...
        $invoicepaymentlines = FinancePaymentLines::find()
              ->Where('invoicepaymentId = :invoicepaymentId', [':invoicepaymentId' => $id])
              // ->andWhere('status = :status', [':status' => 3])
              ->all();

        foreach ($invoicepaymentlines as $invoicepaymentlines) {
          // code...
          // Update all the data based on the payment made and status
          FinancePaymentLines::findOne($invoicepaymentlines["id"])->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);
        }
      }
      return $this->redirect(['index']);
    }

    /**
     * Finds the FinancePayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinancePayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinancePayment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
