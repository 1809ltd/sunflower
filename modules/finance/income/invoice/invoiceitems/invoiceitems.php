<?php

namespace app\modules\finance\income\invoice\invoiceitems;

/**
 * invoiceitems module definition class
 */
class invoiceitems extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\income\invoice\invoiceitems\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
