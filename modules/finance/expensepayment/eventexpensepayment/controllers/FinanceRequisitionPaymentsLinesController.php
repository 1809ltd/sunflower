<?php

namespace app\modules\finance\expensepayment\eventexpensepayment\controllers;

use Yii;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLinesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceRequisitionPaymentsLinesController implements the CRUD actions for FinanceRequisitionPaymentsLines model.
 */
class FinanceRequisitionPaymentsLinesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinanceRequisitionPaymentsLines models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinanceRequisitionPaymentsLinesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceRequisitionPaymentsLines model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceRequisitionPaymentsLines model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          $model = new FinanceRequisitionPaymentsLines();

          //Geting the time created Timestamp
          $model->created_at= new \yii\db\Expression('NOW()');
          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;

          if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);

            if (!empty($model->reqpaymentId)) {
              // code...
              return $this->redirect(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/update', 'id' => $model->reqpaymentId]);

            } else {
              // code...
              return $this->redirect(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/create']);
            }
          }
          return $this->renderAjax('create', [
            'model' => $model,
          ]);
    }

    /**
     * Updates an existing FinanceRequisitionPaymentsLines model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);

            if (!empty($model->reqpaymentId)) {
              // code...
              return $this->redirect(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/update', 'id' => $model->reqpaymentId]);

            } else {
              // code...
              return $this->redirect(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/create']);

            }

            // return $this->redirect(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/create']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing FinanceRequisitionPaymentsLines model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();

        $this->findModel($id)->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);

        $model= FinanceRequisitionPaymentsLines::find()
                      ->where('id = :id', [':id' => $id])
                      ->one();

        if (!empty($model->reqpaymentId)) {
          // code...
          return $this->redirect(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/update', 'id' => $model->reqpaymentId]);

        } else {
          // code...
          return $this->redirect(['/finance/expensepayment/eventexpensepayment/finance-requisition-payments/create']);

        }
        // return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceRequisitionPaymentsLines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceRequisitionPaymentsLines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceRequisitionPaymentsLines::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
