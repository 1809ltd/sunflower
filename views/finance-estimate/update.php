<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimate */

$this->title = 'Update Finance Quotation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-estimate-update">
    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceEstimateLines'=>$modelsFinanceEstimateLines, //to enable Dynamic Form
    ]) ?>

</div>
