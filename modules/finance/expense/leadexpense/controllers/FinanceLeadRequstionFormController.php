<?php

namespace app\modules\finance\expense\leadexpense\controllers;

use Yii;
use app\modules\finance\expense\leadexpense\models\FinanceLeadRequstionForm;
use app\modules\finance\expense\leadexpense\models\FinanceLeadRequstionFormSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*Getting the Lines relationship*/
use app\modules\finance\expense\leadexpense\leadexpenselines\models\FinanceLeadRequstionFormList;
use app\modules\finance\expense\leadexpense\leadexpenselines\models\FinanceLeadRequstionFormListSearch;

// /*Get Event Details*/
// use app\models\EventDetails;


/**
 * FinanceLeadRequstionFormController implements the CRUD actions for FinanceLeadRequstionForm model.
 */
class FinanceLeadRequstionFormController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...

            $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          }else {
            // code...
            $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
          }
          return parent::beforeSave();
        }


        /**
         * Lists all FinanceLeadRequstionForm models.
         * @return mixed
         */
        public function actionIndex()
        {

          $this->layout = '@app/views/layouts/addformdatatablelayout';
          $searchModel = new FinanceLeadRequstionFormSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single FinanceLeadRequstionForm model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {

          $this->layout = '@app/views/layouts/addformdatatablelayout';
          return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new FinanceLeadRequstionForm model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {

          $this->layout = '@app/views/layouts/addformdatatablelayout';
          $model = new FinanceLeadRequstionForm();

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;


            $modelsFinanceLeadRequstionFormList = [new FinanceLeadRequstionFormList];

            //Generate Office Number
            // $model->requstionNumber = $model->getRQnumber();
            // Validate if all the data is inserted
            if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
              Yii :: $app->response->format = 'json';
              return \yii\bootstrap\ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

              $modelsFinanceLeadRequstionFormList = Model::createMultiple(FinanceLeadRequstionFormList::classname());
              Model::loadMultiple($modelsFinanceLeadRequstionFormList, Yii::$app->request->post());

              // ajax validation
              /*
              if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ArrayHelper::merge(
                        ActiveForm::validateMultiple($modelsFinanceLeadRequstionFormList),
                        ActiveForm::validate($model)
                    );
                }*/

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceLeadRequstionFormList) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          foreach ($modelsFinanceLeadRequstionFormList as $modelFinanceLeadRequstionFormList) {

                            $modelFinanceLeadRequstionFormList->reqId = $model->id;
                            $modelFinanceLeadRequstionFormList->userId = $model->userId;
                            $modelFinanceLeadRequstionFormList->status = $model->requstionStatus;


                              if (! ($flag = $modelFinanceLeadRequstionFormList->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }

                  } catch (Exception $e) {

                      $transaction->rollBack();

                  }
              }


            }

            return $this->render('create', [
                'model' => $model,
                  'modelsFinanceLeadRequstionFormList' => (empty($modelsFinanceLeadRequstionFormList)) ? [new FinanceLeadRequstionFormList] : $modelsFinanceLeadRequstionFormList
            ]);
        }

        /**
         * Updates an existing FinanceLeadRequstionForm model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {

          $this->layout = '@app/views/layouts/addformdatatablelayout';
          $model = $this->findModel($id);
            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;


            $modelsFinanceLeadRequstionFormList = $this->getFinanceLeadRequstionFormList($model->id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {

              $oldIDs = ArrayHelper::map($modelsFinanceLeadRequstionFormList, 'id', 'id');
              $modelsFinanceLeadRequstionFormList = Model::createMultiple(FinanceLeadRequstionFormList::classname(), $modelsFinanceLeadRequstionFormList);
              Model::loadMultiple($modelsFinanceLeadRequstionFormList, Yii::$app->request->post());
              $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceLeadRequstionFormList, 'id', 'id')));

              // ajax validation
              if (Yii::$app->request->isAjax) {
                  Yii::$app->response->format = Response::FORMAT_JSON;
                  return ArrayHelper::merge(
                      ActiveForm::validateMultiple($modelsFinanceLeadRequstionFormList),
                      ActiveForm::validate($model)
                  );
              }

              // validate all models
              $valid = $model->validate();
              $valid = Model::validateMultiple($modelsFinanceLeadRequstionFormList) && $valid;

              if ($valid) {
                  $transaction = \Yii::$app->db->beginTransaction();
                  try {
                      if ($flag = $model->save(false)) {
                          if (! empty($deletedIDs)) {
                              FinanceLeadRequstionFormList::deleteAll(['id' => $deletedIDs]);
                          }
                          foreach ($modelsFinanceLeadRequstionFormList as $modelFinanceLeadRequstionFormList) {

                              $modelFinanceLeadRequstionFormList->reqId = $model->id;
                              $modelFinanceLeadRequstionFormList->userId = $model->userId;
                              $modelFinanceLeadRequstionFormList->status = $model->requstionStatus;


                              if (! ($flag = $modelFinanceLeadRequstionFormList->save(false))) {
                                  $transaction->rollBack();
                                  break;
                              }
                          }
                      }
                      if ($flag) {
                          $transaction->commit();
                          return $this->redirect(['view', 'id' => $model->id]);
                      }
                  } catch (Exception $e) {
                      $transaction->rollBack();
                  }
              }
            }

            return $this->render('update', [
                'model' => $model,
                'modelsFinanceLeadRequstionFormList' => (empty($modelsFinanceLeadRequstionFormList)) ? [new FinanceLeadRequstionFormList] : $modelsFinanceLeadRequstionFormList
            ]);

        }

        public function getFinanceLeadRequstionFormList($id)
        {
          $model = FinanceLeadRequstionFormList::find()->where(['reqId' => $id])->all();
          return $model;
        }


        /**
         * Deletes an existing FinanceLeadRequstionForm model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the FinanceLeadRequstionForm model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return FinanceLeadRequstionForm the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = FinanceLeadRequstionForm::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
