<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;


use dosamigos\datepicker\DatePicker;
use app\modules\finance\expense\officeexpense\models\FinanceOfficeRequstionForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines */
/* @var $form yii\widgets\ActiveForm */

// die();
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body finance-office-requisition-payments-lines-form">

  <?= $form->field($model, 'officeReqId')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(FinanceOfficeRequstionForm::find()->all(),'id','requstionNumber'),
      'language' => 'en',
      'options' => ['placeholder' => 'Select a Requstion Bill ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);?>

  <?= $form->field($model, 'amount')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
  <?php
  // necessary for update action.
  if ($model->isNewRecord&&((!empty($_GET)))) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput(['value'=> $_GET["status"]])->label(false);
    echo  $form->field($model, 'officeReqpaymentId')->hiddenInput(['value'=> $_GET["officeReqpaymentId"]])->label(false);

  }elseif (!$model->isNewRecord) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput()->label(false);

  }else {
    // code...
    echo  $form->field($model, 'status')->hiddenInput(['value'=>3])->label(false);

  }

  ?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>

<div class="finance-office-requisition-payments-lines-form">

    <?php
     // $form = ActiveForm::begin(); ?>

    <?php
    // $form->field($model, 'officeReqId')->textInput() ?>

    <?php
    // $form->field($model, 'amount')->textInput() ?>

    <?php
    // $form->field($model, 'status')->textInput() ?>


    <div class="form-group">
        <?php
        // Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php
    // ActiveForm::end(); ?>

</div>
<?php


// use kartik\time\TimePicker;


/*Array*/

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines */
/* @var $form yii\widgets\ActiveForm */
?>
