<?php

namespace app\modules\finance\expensepayment\officeexpensepayment\models;
// PAyment Model
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
// Office Requstion been paid for
use app\modules\finance\expense\officeexpense\models\FinanceOfficeRequstionForm;


use Yii;

/**
 * This is the model class for table "finance_office_requisition_payments_lines".
 *
 * @property int $id
 * @property int $officeReqpaymentId the main payment cheque
 * @property int $officeReqId Requsition number been paid for
 * @property double $amount
 * @property int $status
 * @property string $deletedAt
 * @property string $created_at
 * @property string $updated_at
 * @property int $userId
 *
 * @property FinanceOfficeRequisitionPayments $officeReqpayment
 * @property FinanceOfficeRequstionForm $officeReq
 * @property UserDetails $user
 */
class FinanceOfficeRequisitionPaymentsLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_office_requisition_payments_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['officeReqpaymentId', 'officeReqId', 'status', 'userId'], 'integer'],
            [['amount', 'status', 'userId'], 'required'],
            [['amount'], 'number'],
            [['deletedAt', 'created_at', 'updated_at'], 'safe'],
            [['officeReqpaymentId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceOfficeRequisitionPayments::className(), 'targetAttribute' => ['officeReqpaymentId' => 'id']],
            [['officeReqId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceOfficeRequstionForm::className(), 'targetAttribute' => ['officeReqId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'officeReqpaymentId' => 'Office Payement',
            'officeReqId' => 'Office Requstion Number',
            'amount' => 'Amount',
            'status' => 'Status',
            'deletedAt' => 'Deleted At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeReqpayment()
    {
        return $this->hasOne(FinanceOfficeRequisitionPayments::className(), ['id' => 'officeReqpaymentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeReq()
    {
        return $this->hasOne(FinanceOfficeRequstionForm::className(), ['id' => 'officeReqId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
