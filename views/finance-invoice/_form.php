<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;

/*Getting the product and service offered, customer, Tax*/
use app\models\FinanceItems;
use app\models\CustomerDetails;
use app\models\FinanceTax;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoice */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="row" >

      <div class="panel-body finance-invoice-form">

          <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

          <?php
              // necessary for update action.
              if (! $model->isNewRecord) {
                // code...

                $invoiceNum = $model->invoiceNumber;

                echo $form->field($model, 'invoiceNumber')->textInput(['readonly' => true, 'value' => $invoiceNum]);

              }
          ?>
          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(CustomerDetails::find()->where('`customerstatus` = 1')->asArray()->all(),'id','customerDisplayName'),
                  'language' => 'en',
                  'options' => ['id'=> 'customer-id', 'placeholder' => 'Select a Company ...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]); ?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">

              <?= $form->field($model, 'invoiceProjectId')->widget(DepDrop::classname(), [
                  'options'=>['id'=>'project-id'],
                  'data' => [$model->invoiceProjectId => $model->invoiceProjectId],
                  'type' => DepDrop::TYPE_SELECT2,
                  'pluginOptions'=>[
                      'depends'=>['customer-id'],
                      'initialize' => true,
                      // 'initDepends'=>['orderId'],
                      'placeholder'=>'Select Clients Event...',
                      'url'=>Url::to(['/finance-estimate/events'])
                  ]
              ]);?>

            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'localpurchaseOrder')->textInput(['maxlength' => true]) ?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'invoiceTxnDate')->widget(\yii\jui\DatePicker::class, [
                    //'language' => 'ru',
                    'options' => ['class' => 'form-control'],
                    'inline' => false,
                    'dateFormat' => 'yyyy-MM-dd',
                ]); ?>

            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'invoiceDeadlineDate')->widget(\yii\jui\DatePicker::class, [
                    //'language' => 'ru',
                    'options' => ['class' => 'form-control'],
                    'inline' => false,
                    'dateFormat' => 'yyyy-MM-dd',
                ]); ?>

            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
              <?= $form->field($model, 'invoiceTxnStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'invoiceEmailStatus')->textInput(['maxlength' => true]) ?>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <?= $form->field($model, 'invoiceBillEmail')->textInput(['maxlength' => true]) ?>

            </div>
          </div>


          <div class="col-sm-3">
            <div class="form-group">

            </div>
          </div>

          <div class="panel-body"></div>

          <div class="panel-body">

            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Add Invoice Item</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                </div>
              </div>


              <div class="panel-body">
                   <?php DynamicFormWidget::begin([
                      'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                      'widgetBody' => '.container-items', // required: css class selector
                      'widgetItem' => '.item', // required: css class
                      'limit' => 80, // the maximum times, an element can be cloned (default 999)
                      'min' => 1, // 0 or 1 (default 1)
                      'insertButton' => '.add-item', // css class
                      'deleteButton' => '.remove-item', // css class
                      'model' => $modelsFinanceInvoiceLines[0],
                      'formId' => 'dynamic-form',
                      'formFields' => [

                        'itemRefId',
                        'itemRefName',
                        'invoiceLinesDescription',
                        'invoiceLinespack',
                        'invoiceLinesQty',
                        'invoiceLinesUnitPrice',
                        'invoiceLinesAmount',
                        'invoiceLinesTaxCodeRef',
                        'invoiceLinesTaxamount',
                        'invoiceLinesDiscount',

                      ],
                  ]); ?>

                  <div class="container-items"><!-- widgetContainer -->
                    <!-- Loopping Items in the Lists -->
                  <?php foreach ($modelsFinanceInvoiceLines as $i => $modelFinanceInvoiceLines): ?>
                      <div class="item box-header with-border"><!-- widgetBody -->
                          <h3 class="box-title">Invoice Item</h3>

                          <div class="box-tools pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>

                          </div>
                          <div class="box-body">
                              <?php
                                  // necessary for update action.
                                  if (! $modelFinanceInvoiceLines->isNewRecord) {
                                      echo Html::activeHiddenInput($modelFinanceInvoiceLines, "[{$i}]id");
                                  }
                              ?>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?php $Prp=ArrayHelper::map(FinanceItems::find()->all(),'id','itemsName');  ?>

                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]itemRefId")->dropDownList($Prp, [
                                                  'prompt'=>'Select...',
                                                  'class' => 'prod form-control']); ?>
                                </div>
                              </div>

                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinesDescription")->textInput() ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinespack")->textInput([
                                  'maxlength' => true,
                                  'class' => 'packs form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinesQty")->textInput([
                                  'maxlength' => true,
                                  'class' => 'qnty form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinesUnitPrice")->textInput([
                                  'maxlength' => true,
                                  'class' => 'price form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinesDiscount")->textInput([
                                  'maxlength' => true,
                                  'class' => 'discountpart form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?php $TaxCodeRef =  ArrayHelper::map(FinanceTax::find()->andWhere(['taxStatus'=>1])->asArray()->all(),'taxRate','taxCode'); ?>
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinesTaxCodeRef")->dropDownList($TaxCodeRef, [
                                                  'prompt'=>'Select...',
                                                  'class' => 'taxcode form-control']); ?>
                                </div>
                              </div>

                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinesTaxamount")->textInput([
                                  'readonly' => true,
                                  'maxlength' => true,
                                  'class' => 'sumTaxPart form-control']) ?>
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                  <?= $form->field($modelFinanceInvoiceLines, "[{$i}]invoiceLinesAmount")->textInput([
                                  'readonly' => true,
                                  'maxlength' => true,
                                  'class' => 'amount sumPart  form-control']) ?>
                                </div>
                              </div>

                              <?= $form->field($modelFinanceInvoiceLines, "[{$i}]itemRefName")->hiddenInput([
                              'readonly' => true,
                              // 'maxlength' => true,
                              'class' => 'prodName form-control'])->label(false);?>


                              <div class="col-sm-3">
                                <div class="form-group">

                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">

                                </div>
                              </div>

                          </div>
                      </div>
                  <?php endforeach; ?>
                  </div>
                  <?php DynamicFormWidget::end(); ?>
              </div>
          </div>

          </div>

            <div class="panel-body"></div>

            <div class="col-sm-6">
              <div class="form-group">
                <?= $form->field($model, 'invoiceNote')->textarea(['rows' => 6]) ?>

              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <?= $form->field($model, 'invoiceFooter')->textarea(['rows' => 6]) ?>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <?= $form->field($model, 'invoiceDiscountAmount')->textInput(['readonly' => true,'class' => 'finaldiscount form-control']) ?>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <?= $form->field($model, 'invoiceTaxAmount')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
              </div>
            </div>

            <div class="col-sm-3">
              <div class="form-group">
                <?= $form->field($model, 'invoiceSubAmount')->textInput(['readonly' => true,'class' => 'total form-control']) ?>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <?= $form->field($model, 'invoiceAmount')->textInput(['readonly' => true,'class' => 'amounttotal form-control']) ?>
              </div>
            </div>



            <?= $form->field($model, 'invoicedCreatedby')->hiddenInput(['value'=> "1"])->label(false);?>

            <?= $form->field($model, 'approvedby')->hiddenInput(['value'=> ""])->label(false);?>

            <div class="col-sm-3">
              <div class="form-group">

              </div>
            </div>

            <div class="panel-body"></div>

      </div>

      </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <?= Html::submitButton($modelFinanceInvoiceLines->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
<!-- /.box -->


<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var amount = 0;
        var total = 0;
        var taxation=0;
        var tax=0
        var discount=0;
        var totaldiscount=0;
        var amounttotal= 0;

        items.each(function (index, elem) {

            //getting specific elements
            var qnty = $(elem).find(".qnty").val();
            var price = $(elem).find(".price").val();
            var packs = $(elem).find(".packs").val();
            var taxcode = $(elem).find(".taxcode").val();
            var discountpart = $(elem).find(".discountpart").val();

            //Getting the Value of the Product Selected.
            var prodN = $(elem).find(".prod option:selected").text();

            //Assign the Taxation value to the field
            $(elem).find(".prodName").val(prodN);



            //Check if qnty and price are numeric or something like that
            amount = parseFloat(qnty) * parseFloat(price) * parseFloat(packs);


            //getting disount value
            discount = (parseFloat(amount) * parseFloat(discountpart))/100;

            totaldiscount = parseFloat(totaldiscount) + parseFloat(discount);


            //assigning total discount at the end
            $(".finaldiscount").val(totaldiscount);


            var amountafterdiscount= parseFloat(amount) - parseFloat(discount);
            //Assign the amount value to the field
            $(elem).find(".amount").val(amountafterdiscount);

            var amountValue = $(elem).find(".amount").val();

            taxation= parseFloat(taxcode) * parseFloat(amountafterdiscount);

            //Assign the Taxation value to the field
            $(elem).find(".sumTaxPart").val(taxation);

            //getting the tax amount figures
            var taxValue = $(elem).find(".sumTaxPart").val();

            //getting the total of all figures
            total = parseFloat(total) + parseFloat(amountValue);

            //assing value to the Subtotal amount
            $(".total").val(total);

            //getting total tax amountValue
            tax = parseFloat(tax) + parseFloat(taxValue);

            //assing value to the total tax amount
            $(".tax").val(tax);

            amounttotal = tax+total;

            //assing value to the Amount total tax amount
            $(".amounttotal").val(amounttotal);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
