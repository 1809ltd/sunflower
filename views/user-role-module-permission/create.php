<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userrolemodulepermission\models\UserRoleModulePermission */

$this->title = 'Create User Role Module Permission';
$this->params['breadcrumbs'][] = ['label' => 'User Role Module Permissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-module-permission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
