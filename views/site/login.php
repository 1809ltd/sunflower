<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';
//

$fieldOptions1 = [
    'options' => ['class' => 'form-control has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-control has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>
<?php $form = ActiveForm::begin(['id' => 'login-form','enableClientValidation' => false,'options' => [
                'class' => 'styled_form client-form'
             ]]); ?>
<header>
    Sign in to your Account
</header>

<div>
</div>
<fieldset>
    <!-- <div>
      <?php
       // $form
       //    ->field($model, 'username', $fieldOptions1)
       //    ->label(false)
       //    ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>
    </div>

    <div>
      <?php
       // $form
       //    ->field($model, 'password', $fieldOptions2)
       //    ->label(false)
       //    ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

    </div> -->
    <div>
      <?= $form->field($model, 'username')->textInput(['options' => ['autocomplete'=>'off','class' => 'form-control']]) ?>

    </div>

    <div>
      <?= $form->field($model, 'password')->passwordInput(['options' => ['autocomplete'=>'off','class' => 'form-control']]) ?>

    </div>

    <div>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
    </div>

</fieldset>
<footer>
    <?= Html::submitButton('Sign in', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    <!-- <button type="submit" class="btn btn-primary">
        Sign in
    </button> -->
</footer>
<?php ActiveForm::end(); ?>
<!-- <form action="booking_v1.html" id="login-form" class="styled_form client-form">
                        <header>
                            Sign in to your Account
                        </header>

                        <fieldset>

                            <div>
                                <label class="label">Username</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" name="email" autocomplete="off">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i>
                                        Please enter username</b></label>
                            </div>

                            <div>
                                <label class="label">Password</label>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" name="password" autocomplete="off">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter
                                        your password</b> </label>
                                <div class="note">
                                    <a href="javascript:void(0);">Forgot password?</a>
                                </div>
                            </div>

                            <div>
                                <label class="checkbox">
                                    <input type="checkbox" name="remember" checked="">
                                    <i></i>Stay signed in</label>
                            </div>
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                                Sign in
                            </button>
                        </footer>
                    </form> -->
