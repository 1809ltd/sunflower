<?php

namespace app\modules\event\eventsetup\eventitemcategorytype;

/**
 * eventitemcategorytype module definition class
 */
class eventitemcategorytype extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventsetup\eventitemcategorytype\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
