<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceCreditMemoLines;

/**
 * FinanceCreditMemoLinesSearch represents the model behind the search form of `app\models\FinanceCreditMemoLines`.
 */
class FinanceCreditMemoLinesSearch extends FinanceCreditMemoLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'creditMemoLineStatus', 'userId'], 'integer'],
            [['invoiceNumber', 'creditMemoId', 'creditMemoLineDescription', 'creditMemoLineCreatedAt', 'creditMemoLineUpdatedAt', 'creidtDeletedAt'], 'safe'],
            [['creditMemoLineAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceCreditMemoLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'creditMemoId' => $this->creditMemoId,
            'creditMemoLineAmount' => $this->creditMemoLineAmount,
            'creditMemoLineStatus' => $this->creditMemoLineStatus,
            'creditMemoLineCreatedAt' => $this->creditMemoLineCreatedAt,
            'creditMemoLineUpdatedAt' => $this->creditMemoLineUpdatedAt,
            'creidtDeletedAt' => $this->creidtDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'invoiceNumber', $this->invoiceNumber])
            ->andFilterWhere(['like', 'creditMemoLineDescription', $this->creditMemoLineDescription]);

        return $dataProvider;
    }
}
