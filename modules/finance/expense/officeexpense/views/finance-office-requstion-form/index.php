<?php
use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\models\FinanceOfficeRequstionFormList;
use app\models\FinanceOfficeRequstionFormListSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceOfficeRequstionFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Office Requstion Forms';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>

            <div class="panel-body">
              <div class="finance-invoice-index">
                <div class="table-responsive">
                  <!-- <p>
                      <?= Html::a('Add expense', ['create'], ['class' => 'btn btn-success']) ?>
                  </p> -->
                  <?php
                  $gridColumns = [
                    ['class' => 'yii\grid\SerialColumn'],
                    'requstionNumber',
                    'date',
                    // 'requstionStatus',
                    // 'requstionNote:ntext',
                    'amount',
                    //'preparedBy',
                    //'createdAt',
                    //'updatedAt',
                    //'deletedAt',
                    //'userId',
                    //'approvedBy',
                    ['class' => 'yii\grid\ActionColumn'],
                  ];
                  // Renders a export dropdown menu
                  echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                      // 'filterModel' => $searchModel,
                    'columns' => $gridColumns
                  ]);
                  // You can choose to render your own GridView separately
                  // echo \kartik\grid\GridView::widget([
                  //   'dataProvider' => $dataProvider,
                  //   'filterModel' => $searchModel,
                  //   'columns' => $gridColumns
                  // ]);
                  ?>
                  <p class="pull-right">
                    <?= Html::a('Add expense', ['create'], ['class' => 'btn btn-success']) ?>
                  </p>

              <?php Pjax::begin(); ?>

                  <?=  GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
                          'rowOptions'=> function($model)
                          {
                            // code...
                            if ($model->requstionStatus ==1) {
                              // code...
                              return['class'=>'success'];

                            } elseif ($model->requstionStatus==2) {
                              // code...
                              return['class'=>'warning'];
                            } else {
                              // code...
                              return['class'=>'danger'];
                            }

                          },
                          'columns' => [
                            ['class' => 'kartik\grid\ExpandRowColumn',
                              'value'=>function ($model,$key,$index,$column)
                              {
                                // code...
                                return GridView::ROW_COLLAPSED;
                              },
                              'detail'=>function ($model,$key,$index,$column)
                              {
                                // code...
                                $searchModel = new FinanceOfficeRequstionFormListSearch();
                                $searchModel->reqId = $model->id;
                                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                                return Yii::$app->controller->renderPartial('_requisitionlines',[
                                  'searchModel' => $searchModel,
                                  'dataProvider' => $dataProvider,

                                ]);
                              },
                          ],

                            // 'id',
                            'requstionNumber',
                            'date',
                            // 'requstionStatus',
                            // 'requstionNote:ntext',
                            'amount',
                            //'preparedBy',
                            //'createdAt',
                            //'updatedAt',
                            //'deletedAt',
                            //'userId',
                            //'approvedBy',


                            // ['class' => 'yii\grid\ActionColumn'],
                            // ['class' => 'yii\grid\ActionColumn'],

                            [
                                'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                                'items' => [
                                    [
                                        'label' => 'View',
                                        'url'   => ['view'],
                                    ],
                                    [
                                        'label' => 'Update',
                                        'url'   => ['update'],
                                        // 'options'=>['class'=>'update-modal-click grid-action'],
                                        // 'update'=>function($url,$model,$key){
                                        //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                                        //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                                        //           'class'=>'update-modal-click grid-action',
                                        //           'data-toggle'=>'tooltip',
                                        //           'data-placement'=>'bottom',
                                        //           'title'=>'Update'
                                        //       ]);
                                        //       return $btn;
                                        // },
                                        // 'option' 'class'=>'update-modal-click grid-action',
                                        // 'data-toggle'=>'tooltip',
                                        // 'data-placement'=>'bottom',
                                    ],
                                    [
                                        'label'   => 'Disable',
                                        'url'     => ['delete'],
                                        'linkOptions' => [
                                            'data-method' => 'post'
                                        ],
                                    ],
                                ]
                            ],
                          ],
                      ]);
                      ?>

                </div>

                <?php Pjax::end(); ?>
             </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
