<?php

namespace app\modules\finance\income\invoice\invoiceitems\models;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
/*Invoice*/
use app\modules\finance\income\invoice\models\FinanceInvoice;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "finance_invoice_lines".
 *
 * @property int $id
 * @property int $invoiceId
 * @property int $itemRefId
 * @property string $itemRefName
 * @property string $invoiceLinesDescription
 * @property double $invoiceLinespack
 * @property double $invoiceLinesQty
 * @property double $invoiceLinesUnitPrice
 * @property double $invoiceLinesAmount
 * @property double $invoiceLinesTaxCodeRef
 * @property double $invoiceLinesTaxamount
 * @property double $invoiceLinesDiscount
 * @property string $invoiceLinesCreatedAt
 * @property string $invoiceLinesUpdatedAt
 * @property string $invoiceLinesDeletedAt
 * @property int $invoiceLinesStatus
 * @property int $userId
 *
 * @property FinanceItems $itemRef
 * @property FinanceItems $itemRefName0
 * @property FinanceInvoice $invoice
 * @property UserDetails $user
 */
class FinanceInvoiceLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_invoice_lines';
    }

    /**
     * {@inheritdoc}
     */
     public function rules()
     {
         return [
             [['itemRefId', 'invoiceLinespack', 'invoiceLinesQty', 'invoiceLinesUnitPrice', 'invoiceLinesAmount', 'invoiceLinesTaxCodeRef', 'invoiceLinesTaxamount', 'invoiceLinesDiscount'], 'required'],
             [['invoiceId', 'itemRefId', 'invoiceLinesStatus', 'userId'], 'integer'],
             [['invoiceLinesDescription'], 'string'],
             [['invoiceLinespack', 'invoiceLinesQty', 'invoiceLinesUnitPrice', 'invoiceLinesAmount', 'invoiceLinesTaxCodeRef', 'invoiceLinesTaxamount', 'invoiceLinesDiscount'], 'number'],
             [['invoiceId','invoiceLinesDescription', 'itemRefName','invoiceLinesStatus', 'userId','invoiceLinesCreatedAt', 'invoiceLinesUpdatedAt', 'invoiceLinesDeletedAt'], 'safe'],
             [['itemRefName'], 'string', 'max' => 255],
             [['itemRefId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemRefId' => 'id']],
             [['itemRefName'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemRefName' => 'itemsName']],
             [['invoiceId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceInvoice::className(), 'targetAttribute' => ['invoiceId' => 'id']],
             [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
         ];
     }

     /**
      * {@inheritdoc}
      */
     public function attributeLabels()
     {
         return [
             'id' => 'ID',
             'invoiceId' => 'Invoice',
             'itemRefId' => 'Service',
             'itemRefName' => 'Service Name',
             'invoiceLinesDescription' => 'Description',
             'invoiceLinespack' => 'Days',
             'invoiceLinesQty' => 'Qty',
             'invoiceLinesUnitPrice' => 'Unit Price',
             'invoiceLinesAmount' => 'Amount',
             'invoiceLinesTaxCodeRef' => 'Tax',
             'invoiceLinesTaxamount' => 'Taxamount',
             'invoiceLinesDiscount' => 'Discount(%)',
             'invoiceLinesCreatedAt' => 'Created At',
             'invoiceLinesUpdatedAt' => 'Updated At',
             'invoiceLinesDeletedAt' => 'Deleted At',
             'invoiceLinesStatus' => 'Status',
             'userId' => 'User ID',
         ];
     }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemRef()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemRefId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemRefName0()
    {
        return $this->hasOne(FinanceItems::className(), ['itemsName' => 'itemRefName']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(FinanceInvoice::className(), ['id' => 'invoiceId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
