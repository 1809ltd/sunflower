<?php

namespace app\modules\company;

/**
 * company module definition class
 */
class company extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\company\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Company details Sub module*/
          'companydetails' => [
                  'class' => 'app\modules\company\companydetails\companydetails',
              ],
        /*Company departments*/
          'companydepartment' => [
                  'class' => 'app\modules\company\companydepartment\companydepartment',
              ],
        /*Company Site*/
          'companysite' => [
                  'class' => 'app\modules\company\companysite\companysite',
              ],
        /*Compnay Site Location*/
        'companysitelocation' => [
              'class' => 'app\modules\company\companysitelocation\companysitelocation',
          ],

        ];

        // custom initialization code goes here
    }
}
