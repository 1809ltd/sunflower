<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body finance-requisition-payments-lines-form">

  <?= $form->field($model, 'eventReqId')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(FinanceEventRequstionForm::find()->all(),'id','requstionNumber'),
      'language' => 'en',
      'options' => ['placeholder' => 'Select a Requstion Bill ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);?>

  <?= $form->field($model, 'amount')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
  <?php
  // necessary for update action.
  if ($model->isNewRecord&&((!empty($_GET)))) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput(['value'=> $_GET["status"]])->label(false);
    echo  $form->field($model, 'reqpaymentId')->hiddenInput(['value'=> $_GET["reqpaymentId"]])->label(false);

  }elseif (!$model->isNewRecord) {
    // code...
    echo  $form->field($model, 'status')->hiddenInput()->label(false);

  }else {
    // code...
    echo  $form->field($model, 'status')->hiddenInput(['value'=>2])->label(false);

  }

  ?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
