<?php

namespace app\modules\personnel\personneldetails\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\personnel\personneldetails\models\PersonnelDetails;

/**
 * PersonnelDetailsSearch represents the model behind the search form of `app\modules\personnel\personneldetails\models\PersonnelDetails`.
 */
class PersonnelDetailsSearch extends PersonnelDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'printOnChequeName', 'status', 'billable'], 'integer'],
            [['nationalId', 'displayName', 'firstName', 'sName', 'lName', 'primaryPhone', 'secondaryPhone', 'primaryAddr', 'town', 'area', 'email', 'hiredDate', 'createdAt', 'deletedAt', 'updatedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PersonnelDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'printOnChequeName' => $this->printOnChequeName,
            'status' => $this->status,
            'hiredDate' => $this->hiredDate,
            'createdAt' => $this->createdAt,
            'deletedAt' => $this->deletedAt,
            'updatedAt' => $this->updatedAt,
            'billable' => $this->billable,
        ]);

        $query->andFilterWhere(['like', 'nationalId', $this->nationalId])
            ->andFilterWhere(['like', 'displayName', $this->displayName])
            ->andFilterWhere(['like', 'firstName', $this->firstName])
            ->andFilterWhere(['like', 'sName', $this->sName])
            ->andFilterWhere(['like', 'lName', $this->lName])
            ->andFilterWhere(['like', 'primaryPhone', $this->primaryPhone])
            ->andFilterWhere(['like', 'secondaryPhone', $this->secondaryPhone])
            ->andFilterWhere(['like', 'primaryAddr', $this->primaryAddr])
            ->andFilterWhere(['like', 'town', $this->town])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
