<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemo */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Credit Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-credit-memo-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vendorCreditMemoAccount',
            'refernumber',
            'vendorCreditMemoTxnDate',
            'vendorCreditMemoNote:ntext',
            'vendorId',
            'creditAmount',
            'vendorCreditMemoStatus',
            'vendorCreditMemoCreatedAt',
            'vendorCreditMemoUpdatedAt',
            'vendorCreditMemoDeletedAt',
            'userId',
        ],
    ]) ?>

</div>
