<!-- <div class="taxreport-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Tax Reports*/
use app\modules\finance\financereport\companyfinancials\taxreport\models\TaxReport;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Tax Statement';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
if (!empty($tafuta)) {
  // code...
  
  ?>

  <div class="box no-print">
    <div class="box-header with-border">
      <h3 class="box-title">Search</h3>
      <div class="box-tools pull-right">
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-4">
          <div class="form-group">
            <label class="col-lg-4 control-label">Date From: </label>
            <div class="col-lg-8">
              <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="col-lg-4 control-label">Date To: </label>
            <div class="col-lg-8">
                  <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['autocomplete'=>'off','class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ])->label(false) ?>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <div class="col-lg-8 col-lg-offset-4">
              <div class="center-align">
                <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
              </div>
            </div>
          </div>
        </div>
        <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="<?=Url::to(['/finance/report/company/tax-report/default/view','startDate'=>$startDate,'endDate'=>$endDate]) ?>"
      class="btn btn-xs btn-success m-b-10"><i
                    class="fa fa-print m-r-5"></i> Show Detail Audit Report</a>
    </div>
  </div>

  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Tax Statement</h3>
      <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>
    <div class="box">
      <div class="box-body">
        <h5 class="box-title">Tax Report</h5>
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th class="text-left">Tax</th>
              <th class="text-left">Taxable Amount</th>
              <th class="text-left">Dr Tax Amount</th>
              <th class="text-left">Dr Tax  Amount</th>
              <!-- <th class="text-right">Dr Tax  Amount</th> -->
            </tr>
          </thead>
          <tbody>
            <?php
            $taxTotal=0;
            foreach ($tax as $tax){
              // code...
              $taxTotal+= $tax["cr_amount"];
            ?>
            <tr>
              <td  class="text-left"><?= $tax->taxCode;?>  (<?= $tax->taxRate;?>)</td>
              <td  class="text-left"><?= $tax->taxableamount;?></td>
              <td  class="text-left"><?= $tax->dr_amount;?></td>
              <td  class="text-left"><?= $tax->cr_amount;?></span></td>
              </tr>
              <?php
            }
            ?>
            <tr>
              <th  class="text-left">Total Revenue:</th>
              <td  class="text-left"><?= number_format($taxTotal,2); ?></td>
            </tr>
          </tbody>
        </table>
      </br>

    </br>

    </div>
    </div>
    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="javascript:void(0)" onclick="window.print()"
                 class="btn btn-xs btn-success m-b-10"><i
                      class="fa fa-print m-r-5"></i> Print</a>
      </div>
    </div>
  </section>
  <?php

  } else {
    // code...
    $model->startDate= date('Y-01-01');
    $model->endDate= date('Y-m-d');
    $tax = TaxReport::find()->select(['taxCode','taxRate','COALESCE(SUM(taxableamount),0) as taxableamount','COALESCE(SUM(cr_amount),0) as cr_amount','COALESCE(SUM(dr_amount),0) as dr_amount'])
                                    ->Where(['between', 'date', $model->startDate, $model->endDate])
                                    ->groupBy(['taxCode', 'taxRate'])
                                    ->orderBy(['taxCode' => SORT_ASC,])
                                    ->all();
    ?>
    <div class="box no-print">
      <div class="box-header with-border">
        <h3 class="box-title">Search</h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <?php $form = ActiveForm::begin(); ?>
          <div class="col-md-4">
            <div class="form-group">
              <label class="col-lg-4 control-label">Date From: </label>
              <div class="col-lg-8">
                <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['autocomplete'=>'off','class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ])->label(false) ?>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="col-lg-4 control-label">Date To: </label>
              <div class="col-lg-8">
                    <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['autocomplete'=>'off','class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ])->label(false) ?>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <div class="col-lg-8 col-lg-offset-4">
                <div class="center-align">
                  <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
                </div>
              </div>
            </div>
          </div>
          <?php ActiveForm::end(); ?>
        </div>
      </div>
    </div>

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="<?=Url::to(['/finance/report/company/tax-report/default/view','startDate'=>$model->startDate,'endDate'=>$model->endDate]) ?>"
        class="btn btn-xs btn-success m-b-10"><i
                      class="fa fa-print m-r-5"></i> Show Detail Audit Report</a>
      </div>
    </div>
    <section class="invoice">
      <div class="text-center">
        <h6 class="box-title">
          <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
        </h6>
        <h3 class="box-title">Tax Statement</h3>
        <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
        <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
      </div>
      <div class="box">
        <div class="box-body">
          <h5 class="box-title">Tax Report</h5>
          <table class="table  table-striped table-condensed">
            <thead>
              <tr>
                <th class="text-left">Tax</th>
                <th class="text-left">Taxable Amount</th>
                <th class="text-left">Dr Tax Amount</th>
                <th class="text-left">Dr Tax  Amount</th>
                <!-- <th class="text-right">Dr Tax  Amount</th> -->
              </tr>
            </thead>
            <tbody>
              <?php
              $taxTotal=0;
              foreach ($tax as $tax){
                // code...
                $taxTotal+= $tax["cr_amount"];
              ?>
              <tr>
                <td  class="text-left"><?= $tax->taxCode;?>  (<?= $tax->taxRate;?>)</td>
                <td  class="text-left"><?= $tax->taxableamount;?></td>
                <td  class="text-left"><?= $tax->dr_amount;?></td>
                <td  class="text-left"><?= $tax->cr_amount;?></span></td>
                </tr>
                <?php
              }
              ?>
              <!-- <tr>
                <th  class="text-left">Total Revenue:</th>
                <td  class="text-left"><?= number_format($taxTotal,2); ?></td>
              </tr> -->
            </tbody>
          </table>
        </br>

      </br>

      </div>
      </div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
                   class="btn btn-xs btn-success m-b-10"><i
                        class="fa fa-print m-r-5"></i> Print</a>
        </div>
      </div>
    </section>
    <?php
  }
  ?>
