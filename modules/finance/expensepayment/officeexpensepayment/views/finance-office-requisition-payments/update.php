<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;


/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsSearch;

// Get the related payments based on the payemtn
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLinesSearch;
/*Dropdown action Bar*/
use yii\bootstrap\ButtonDropdown;
/* @var $this yii\web\View */
/* @var $model app\models\FinanceOfficeRequisitionPayments */

$this->title = 'Update Payments Voucher: ' . $model->reqrecepitNumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Office Requisition Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-office-requisition-payments-update">
  <div class="col-md-6">
    <?php
    // This is to get all the Bill Payments that are on hold
    $modelsFinanceOfficeRequisitionPaymentsLines1 = new FinanceOfficeRequisitionPayments();
    $output= $modelsFinanceOfficeRequisitionPaymentsLines1->getallpaymentslinesforApayment($model->id,$model->paymentstatus);

    // echo "This is the status been pull".$model->paymentstatus;
    // echo "This the id for the model".$model->id;
    // print_r($output);
    //
    // This is to get all the payments that only belong to this client and list them

    $searchModel = new FinanceOfficeRequisitionPaymentsSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    // Calling the forms with the list of payments on hold plus passing the variables for form
    echo $this->render('_holdbillpaymentsinesupdate', [
      // This is th model for the Bill Payments
      'model' => $model,
      // This is the result of the clients hold payments that are not yet assigned
      'output' => $output,
      ])
    ?>
    <!-- <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Payment On Hold</h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">

      </div>
    </div> -->
  </div>
  <!-- Payments on Hold -->
  <div class="col-md-6">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Office Requstion Payment Receipt</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <div class="box-body finance-office-requisition-payments-index">
          <?php Pjax::begin(); ?>
          <?php // echo $this->render('_search', ['model' => $searchModel]);

          ?>
          <?= GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'columns' => [
                  // ['class' => 'yii\grid\SerialColumn'],
                  ['class' => 'kartik\grid\ExpandRowColumn',
                    'value'=>function ($model,$key,$index,$column)
                    {
                      // code...
                      return GridView::ROW_COLLAPSED;
                    },
                    'detail'=>function ($model,$key,$index,$column)
                    {
                      // code...
                      $searchModel = new FinanceOfficeRequisitionPaymentsLinesSearch();
                      $searchModel->officeReqpaymentId = $model->id;
                      $searchModel->status = $model->paymentstatus;
                      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                      return Yii::$app->controller->renderPartial('_paymentlines',[
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,

                      ]);
                    },
                ],

                  // 'id',
                  'officeReqPaymentPayType',
                  // 'officeReqPaymentaccount',
                  [
                    'attribute'=>'officeReqPaymentaccount',
                    'value'=>'officeReqPaymentaccount0.fullyQualifiedName',
                  ],
                  'reqrecepitNumber',
                  'officeReqPaymentTotalAmt',
                  'unallocatedAmount',
                  'date',
                  'officeReqPaymentofficeReqRef',
                  //'officeReqPaymentPrivateNote:ntext',
                  //'officeReqPaymentCreatedAt',
                  //'officeReqPaymentUpdatedAt',
                  //'officeReqPaymentDeletedAt',
                  //'userId',
                  //'createdBy',
                  //'approvedBy',
                  //'paymentstatus',

                  /* ... GridView configuration ... */
                  [
                      'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                      'items' => [
                          [
                              'label' => 'View',
                              'url'   => ['view'],
                          ],
                          [
                              'label' => 'Update',
                              'url'   => ['update'],
                              'linkOptions' => [
                                  'data-method' => 'post'
                              ],
                              // 'options'=>['class'=>'update-modal-click grid-action'],
                              // 'update'=>function($url,$model,$key){
                              //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                              //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                              //           'class'=>'update-modal-click grid-action',
                              //           'data-toggle'=>'tooltip',
                              //           'data-placement'=>'bottom',
                              //           'title'=>'Update'
                              //       ]);
                              //       return $btn;
                              // },
                              // 'option' 'class'=>'update-modal-click grid-action',
                              // 'data-toggle'=>'tooltip',
                              // 'data-placement'=>'bottom',
                          ],
                          [
                              'label'   => 'Disable',
                              'url'     => ['delete'],
                              'linkOptions' => [
                                  'data-method' => 'post'
                              ],
                          ],
                      ]
                  ],

                  // ['class' => 'yii\grid\ActionColumn'],
              ],
          ]); ?>
          <?php Pjax::end(); ?>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</div>
