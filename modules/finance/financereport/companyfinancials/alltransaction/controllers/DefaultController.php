<?php

namespace app\modules\finance\financereport\companyfinancials\alltransaction\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// for db functions
use yii\db\Expression;
/*All transactions */
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
use app\modules\finance\financesetup\coa\models\FinanceAccounts;

/**
 * Default controller for the `alltransaction` module
 */

class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // Create a model to allow the search
      $model = new \yii\base\DynamicModel([
          'startDate','endDate','account'
        ]);

     // Adding Rules for validation
     $model->addRule(['startDate','endDate','account'], 'required')
          ->validate();

     // Checking if it a load
     if($model->load(Yii::$app->request->post())){
       // do somenthing with model
       // $tafuta="we are here";
       // Get all the revenue
       $accountName= FinanceAccounts::find()->Where('id = :id', [':id' => $model->account])->one();

       $transactionaccounts = AllTransactions::find()->Where('accountId = :accountId', [':accountId' => $model->account])
                                          ->andWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                          ->andWhere('status > :status', [':status' => 0])
                                          // ->groupBy(['accountId', 'transactionClassification'])
                                          ->orderBy(['transactionDate' => SORT_ASC,])
                                          ->all();
      // all transaction
      if (!empty($transactionaccounts)) {
        // code...
        return $this->render('index', [
                              'transactionaccounts' => $transactionaccounts,
                              'startDate' => $model->startDate,
                              'endDate' => $model->endDate,
                              'accountName' => $accountName,
                              'model' => $model,
                            ]);
       } else {
         // code...
         return $this->render('index', [
                    'model' => $model,
                  ]);

      }
    }else {
      // code...
      return $this->render('index', [
                  'model' => $model,
                ]);
    }
    // return $this->render('index');
  }
  // View
  public function actionView($account,$startDate,$endDate)
  {
    // $this->layout='addformdatatablelayout';
    $this->layout = '@app/views/layouts/addformdatatablelayout';
    // Create a model to allow the search
    $model = new \yii\base\DynamicModel([
        'startDate','endDate','account'
      ]);

   // Adding Rules for validation
   $model->addRule(['startDate','endDate','account'], 'required')
        ->validate();


    // Get all the revenue
    $accountName= FinanceAccounts::find()->Where('id = :id', [':id' => $model->account])->one();

    // Get all the revenue
    $transactionaccounts = AllTransactions::find()->Where('accountId = :accountId', [':accountId' => $account])
                                       ->andWhere(['between', 'transactionDate', $startDate, $endDate])
                                       ->andWhere('status > :status', [':status' => 0])
                                       // ->groupBy(['accountId', 'transactionClassification'])
                                       ->orderBy(['transactionDate' => SORT_ASC,])
                                       ->all();

      return $this->render('index', [
          'model' => $model,
                                'transactionaccounts' => $transactionaccounts,
                                'startDate' => $model->startDate,
                                'endDate' => $model->endDate,
                                'accountName' => $accountName,
      ]);


    // $this->layout = '@app/views/layouts/addformdatatablelayout';


   // Checking if it a load
  //  if($model->load(Yii::$app->request->post())){
  //    // do somenthing with model
  //    // $tafuta="we are here";
  //   // all transaction
  //   if (!empty($transactionaccounts)) {
  //     // code...
  //     return $this->render('index', [
  //                           'transactionaccounts' => $transactionaccounts,
  //                           'startDate' => $model->startDate,
  //                           'endDate' => $model->endDate,
  //                           'model' => $model,
  //                         ]);
  //    } else {
  //      // code...
  //      return $this->render('index', [
  //                 'model' => $model,
  //               ]);
  //
  //   }
  // }else {
  //   // code...
  //   return $this->render('index', [
  //               'model' => $model,
  //             ]);
  // }
  // return $this->render('index');
}
}
