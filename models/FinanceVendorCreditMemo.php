<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_vendor_credit_memo".
 *
 * @property int $id
 * @property int $vendorCreditMemoAccount
 * @property string $refernumber
 * @property string $vendorCreditMemoTxnDate
 * @property string $vendorCreditMemoNote
 * @property int $vendorId
 * @property double $creditAmount
 * @property int $vendorCreditMemoStatus
 * @property string $vendorCreditMemoCreatedAt
 * @property string $vendorCreditMemoUpdatedAt
 * @property string $vendorCreditMemoDeletedAt
 * @property int $userId
 *
 * @property VendorCompanyDetails $vendor
 * @property FinanceAccounts $vendorCreditMemoAccount0
 * @property FinanceVendorCreditMemoLines[] $financeVendorCreditMemoLines
 */
class FinanceVendorCreditMemo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_vendor_credit_memo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorCreditMemoAccount', 'refernumber', 'vendorCreditMemoTxnDate', 'vendorCreditMemoNote', 'vendorId', 'creditAmount', 'vendorCreditMemoStatus', 'userId'], 'required'],
            [['vendorCreditMemoAccount', 'vendorId', 'vendorCreditMemoStatus', 'userId'], 'integer'],
            [['vendorCreditMemoTxnDate', 'vendorCreditMemoCreatedAt', 'vendorCreditMemoUpdatedAt', 'vendorCreditMemoDeletedAt'], 'safe'],
            [['vendorCreditMemoNote'], 'string'],
            [['creditAmount'], 'number'],
            [['refernumber'], 'string', 'max' => 50],
            [['refernumber', 'vendorId'], 'unique', 'targetAttribute' => ['refernumber', 'vendorId']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['vendorCreditMemoAccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['vendorCreditMemoAccount' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorCreditMemoAccount' => 'Account',
            'refernumber' => 'Refernumber',
            'vendorCreditMemoTxnDate' => 'Date',
            'vendorCreditMemoNote' => 'Note',
            'vendorId' => 'Vendor',
            'creditAmount' => 'Credit Amount',
            'vendorCreditMemoStatus' => 'Status',
            'vendorCreditMemoCreatedAt' => 'Created At',
            'vendorCreditMemoUpdatedAt' => 'Updated At',
            'vendorCreditMemoDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorCreditMemoAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'vendorCreditMemoAccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemoLines()
    {
        return $this->hasMany(FinanceVendorCreditMemoLines::className(), ['vendorCreditMemoId' => 'id']);
    }
}
