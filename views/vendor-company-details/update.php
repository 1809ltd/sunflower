<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VendorCompanyDetails */

$this->title = 'Update Supplier Company Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Supplier Company Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vendor-company-details-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
