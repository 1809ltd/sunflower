<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventRequirements */

$this->title = 'Update Event Requirements: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Requirements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-requirements-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
