<?php

namespace app\modules\finance\expensepayment\billpayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments;

/**
 * FinanceBillsPaymentsSearch represents the model behind the search form of `app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments`.
 */
class FinanceBillsPaymentsSearch extends FinanceBillsPayments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'userId', 'paymentstatus'], 'integer'],
            [['billPaymentPayType',  'vendorId', 'billPaymentaccount','billPaymentBillRef', 'billPaymentPrivateNote', 'billPaymentCreatedAt', 'billPaymentUpdatedAt', 'billPaymentDeletedAt'], 'safe'],
            [['billPaymentTotalAmt'], 'number'],
            [['unallocatedAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBillsPayments::find();
        $query->joinWith(['vendor']);
        $query->joinWith(['billPaymentaccount0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'billPaymentaccount' => $this->billPaymentaccount,
            'billPaymentTotalAmt' => $this->billPaymentTotalAmt,
            // 'vendorId' => $this->vendorId,
            'billPaymentCreatedAt' => $this->billPaymentCreatedAt,
            'unallocatedAmount' => $this->unallocatedAmount,
            'billPaymentUpdatedAt' => $this->billPaymentUpdatedAt,
            'billPaymentDeletedAt' => $this->billPaymentDeletedAt,
            'userId' => $this->userId,
            'paymentstatus' => $this->paymentstatus,
        ]);

        $query->andFilterWhere(['like', 'billPaymentPayType', $this->billPaymentPayType])
            ->andFilterWhere(['like', 'vendorCompanyName', $this->vendorId])
                ->andFilterWhere(['like', 'fullyQualifiedName', $this->billPaymentaccount])
            ->andFilterWhere(['like', 'billPaymentBillRef', $this->billPaymentBillRef])
            ->andFilterWhere(['like', 'billPaymentPrivateNote', $this->billPaymentPrivateNote]);

        return $dataProvider;
    }
}
