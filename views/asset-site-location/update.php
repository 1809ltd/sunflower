<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetSiteLocation */

$this->title = 'Update Asset Site Location: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Site Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-site-location-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
