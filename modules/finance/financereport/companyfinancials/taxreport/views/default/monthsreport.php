<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Tax Reports*/
use app\modules\finance\financereport\companyfinancials\taxreport\models\TaxReport;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Tax Statement';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="invoice">
  <div class="text-center">
    <h6 class="box-title">
      <img class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      <!-- <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo"> -->
    </h6>
    <h3 class="box-title">Tax Statement</h3>
    <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
    <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
  </div>
  <div class="box">
    <div class="box-body">
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <h5 class="box-title">Tax Report Purchases</h5>
          <table class="table  table-striped table-condensed">
            <thead>
              <tr>
                <th class="text-left">Date</th>
                <th class="text-left">Supplier</th>
                <th class="text-left">Pin</th>
                <th style="text-align: center;">Invoice</th>
                <!-- <th class="text-left">ETR Reference Number</th> -->
                <!-- <th class="text-left">Type of Good/ Service</th> -->
                <th class="text-left">Taxable Amount</th>
                <th class="text-left">Tax Amount</th>
                <th class="text-left">Type Of Tax</th>
                <!-- <th class="text-right">Dr Tax  Amount</th> -->
              </tr>
            </thead>
            <tbody>
              <?php
              $taxPurchasedr_amountTotal=0;
              foreach ($taxPurchase as $taxPurchase){
                // code...
                $taxPurchasedr_amountTotal+= $taxPurchase["dr_amount"];
                ?>
                <tr>
                  <td class="text-left"><?= Yii::$app->formatter->asDate($taxPurchase->date, 'long');?></td>
                  <td class="text-left"><?= $taxPurchase->parties;?></td>
                  <td class="text-left"><?= $taxPurchase->kraPin;?></td>
                  <td style="text-align: center;"><?= $taxPurchase->transactionCode;?></span></td>
                  <!-- <td class="text-left"></span></td> -->
                  <!-- <td class="text-left"> </td> -->
                  <td class="text-right"><?= number_format($taxPurchase->taxableamount,2,'.',',');?></td>
                  <td class="text-right"><?= number_format($taxPurchase->dr_amount,2,'.',',');?></td>
                  <td class="text-left"><?= $taxPurchase->taxCode;?>  (<?= $taxPurchase->taxRate;?>)</td>
                </tr>
                <?php
              }
              ?>

              <tr>
                <th  class="text-left"></th>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <th class="text-left"></th>
                <!-- <td class="text-left"> </td>
                <td class="text-left"> </td> -->
                <td  class="text-left"></td>
              </tr>
              <tr>
                <th  class="text-left">Total Revenue:</th>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <th class="text-right"> <?= number_format($taxPurchasedr_amountTotal,2); ?></th>
                <!-- <td class="text-left"> </td>
                <td class="text-left"> </td> -->
                <td  class="text-left"></td>
              </tr>
            </tbody>
          </table>
        </br>
        </br>
        </div>
      </div>
    </div>
  </div>
<!-- Sales -->
  <div class="box">
    <div class="box-body">
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <h5 class="box-title">Tax Report Sales</h5>
          <table class="table  table-striped table-condensed">
            <thead>
              <tr>
                <th class="text-left">Date</th>
                <th class="text-left">Customer</th>
                <th class="text-left">Pin</th>
                <th style="text-align: center;">Invoice</th>
                <!-- <th class="text-left">ETR Reference Number</th> -->
                <!-- <th class="text-left">Type of Good/ Service</th> -->
                <th class="text-left">Taxable Amount</th>
                <th class="text-left">Tax Amount</th>
                <th class="text-left">Type Of Tax</th>
                <!-- <th class="text-right">Dr Tax  Amount</th> -->
              </tr>
            </thead>
            <tbody>
              <?php
              $taxSalescr_amountTotal=0;
              foreach ($taxSales as $taxSales){
                // code...
                $taxSalescr_amountTotal+= $taxSales["cr_amount"];
                ?>
                <tr>
                  <td class="text-left"><?= Yii::$app->formatter->asDate($taxSales->date, 'long');?></td>
                  <td class="text-left"><?= $taxSales->parties;?></td>
                  <td class="text-left"><?= $taxSales->kraPin;?></td>
                  <td style="text-align: center;"><?= $taxSales->transactionCode;?></span></td>
                  <!-- <td class="text-left"></span></td> -->
                  <!-- <td class="text-left"> </td> -->
                  <td class="text-right"><?= number_format($taxSales->taxableamount,2,'.',',');?></td>
                  <td class="text-right"><?= number_format($taxSales->cr_amount,2,'.',',');?></td>
                  <td class="text-left"><?= $taxSales->taxCode;?>  (<?= $taxSales->taxRate;?>)</td>
                </tr>
                <?php
              }
              ?>
              <tr>
                <th  class="text-left"></th>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <th class="text-left"></th>
                <!-- <td class="text-left"> </td>
                <td class="text-left"> </td> -->
                <td  class="text-left"></td>
              </tr>
              <tr>
                <th  class="text-left">Total Revenue:</th>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <td class="text-left"> </td>
                <th class="text-right"><?= number_format($taxSalescr_amountTotal,2); ?> </th>
                <!-- <td class="text-left"> </td>
                <td class="text-left"> </td> -->
                <td  class="text-left"></td>
              </tr>
            </tbody>
          </table>
          </br>
          </br>
        </div>
      </div>
    </div>
  </div>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="javascript:void(0)" onclick="window.print()"
                 class="btn btn-xs btn-success m-b-10"><i
                      class="fa fa-print m-r-5"></i> Print</a>
    </div>
  </div>
</section>
