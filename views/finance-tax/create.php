<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceTax */

$this->title = 'Create Finance Tax';
$this->params['breadcrumbs'][] = ['label' => 'Finance Taxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-tax-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
