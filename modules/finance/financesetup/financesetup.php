<?php

namespace app\modules\finance\financesetup;

/**
 * financesetup module definition class
 */
class financesetup extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financesetup\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->modules =[
          /*Fiance Category Type Sub Module*/
          'financecategorytype' => [
              'class' => 'app\modules\finance\financesetup\financecategorytype\financecategorytype',
          ],
          /*Chart of Accounts*/
          'coa' => [
              'class' => 'app\modules\finance\financesetup\coa\coa',
          ],
          /*Tax*/
          'tax' => [
              'class' => 'app\modules\finance\financesetup\tax\tax',
          ],
        ];

        // custom initialization code goes here
    }
}
