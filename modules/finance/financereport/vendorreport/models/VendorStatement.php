<?php

namespace app\modules\finance\financereport\vendorreport\models;

use Yii;

/**
 * This is the model class for table "vendor_statement".
 *
 * @property int $vendorId
 * @property string $date
 * @property string $transactionNo
 * @property string $Details
 * @property double $amountinvoiced
 * @property double $amountPaid
 */
class VendorStatement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
     public $balance_amount;
     
    public static function tableName()
    {
        return 'vendor_statement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendorId'], 'integer'],
            [['date'], 'safe'],
            [['amountinvoiced', 'amountPaid'], 'number'],
            [['transactionNo'], 'string', 'max' => 50],
            [['Details'], 'string', 'max' => 208],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vendorId' => 'Vendor',
            'date' => 'Date',
            'transactionNo' => 'Transaction No',
            'Details' => 'Details',
            'amountinvoiced' => 'Amountinvoiced',
            'amountPaid' => 'Amount Paid',
        ];
    }
}
