<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\modules\company\companysite\models\CompanySiteDetails;
use app\modules\company\companysite\models\CompanySiteDetailsSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;


$this->title = 'Company Site Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::button('Add Site', ['value'=>Url::to((['/company/companysite/company-site-details/create'])),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        </p>
      </div>
    </div>
  </div>

  <?php
  Modal::begin([

    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
   <?php
    Modal::begin([
        // 'header'=>'<h4>Update Model</h4>',
        'id'=>'update-modal',
        'size'=>'modal-lg'
    ]);

    echo "<div id='updateModalContent'></div>";

    Modal::end();
?>

  <div class="box-body company-department-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([

      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
      'rowOptions'=> function($model)
      {
        // code...
        if ($model->siteStatus==1) {
          // code...
          return['class'=>'success'];
        } else {
          // code...
          return['class'=>'danger'];
        }

      },
      'columns' => [
          ['class' => 'kartik\grid\ExpandRowColumn',
            'value'=>function ($model,$key,$index,$column)
            {
              // code...
              return GridView::ROW_COLLAPSED;
            },
            'detail'=>function ($model,$key,$index,$column)
            {
              // code...
              $searchModel = new CompanySiteDetailsSearch();
              $searchModel->id = $model->id;
              $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

              return Yii::$app->controller->renderPartial('_viewcompanysitedetails',[
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

              ]);
            },
        ],

        //'id',
        //'companyId',
        'company.companyName',
        'siteName',
        'siteAddress',
        'siteSuite',
        //'siteCity',
        //'siteCounty',
        //'sitePostal',
        //'siteCountry',
        //'siteStatus',
        //'siteTimestamp',
        //'siteUpdatedAt',
        //'siteDeletedAt',
        //'companyUserId',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => ['view' => function($url, $model) {
            	    return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
            	},
              'update' => function($url,$model,$key){
                    $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                        'value'=>Url::to((['/company/companysite/company-site-details/update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                        // 'value'=>Url::to((['/asset-site-location/update&id='.$key])), //<---- here is where you define the action that handles the ajax request
                        // 'value'=> Yii::$app->urlManager->createUrl('asset-site-location/update?id='.$key), //<---- here is where you define the action that handles the ajax request
                        'class'=>'update-modal-click grid-action',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'bottom',
                        'title'=>'Update'
                    ]);
                    return $btn;
              },
            	// 'update' => function($id, $model) {
              // return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update', 'class'=>'update-modal-click'/* grid-action'/*,'id' => 'modal-btn-view'*/]);
            	// },
            	'delete' => function($url, $model) {
            	    return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
            	}
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
