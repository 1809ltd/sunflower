<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companydetails\models\CompanyDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'companyName') ?>

    <?= $form->field($model, 'companyAddress') ?>

    <?= $form->field($model, 'companySuite') ?>

    <?= $form->field($model, 'companyCity') ?>

    <?php // echo $form->field($model, 'companyCounty') ?>

    <?php // echo $form->field($model, 'companyPostal') ?>

    <?php // echo $form->field($model, 'companyCountry') ?>

    <?php // echo $form->field($model, 'companyLogo') ?>

    <?php // echo $form->field($model, 'companyStatus') ?>

    <?php // echo $form->field($model, 'companyTimestamp') ?>

    <?php // echo $form->field($model, 'companyupdatedAt') ?>

    <?php // echo $form->field($model, 'companyDeletedAt') ?>

    <?php // echo $form->field($model, 'companyUserId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
