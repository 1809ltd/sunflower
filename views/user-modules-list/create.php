<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\moduleslist\models\UserModulesList */

$this->title = 'Create User Modules List';
$this->params['breadcrumbs'][] = ['label' => 'User Modules Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-modules-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
