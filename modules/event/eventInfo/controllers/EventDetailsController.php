<?php

namespace app\modules\event\eventInfo\controllers;

use Yii;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\event\eventInfo\models\EventDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventDetailsController implements the CRUD actions for EventDetails model.
 */
class EventDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...
            //$this->companyTimestamp = new \yii\db\Expression('NOW()');
            $this->eventLastUpdatedAt = new \yii\db\Expression('NOW()');

          }else {
            // code...
            $this->eventLastUpdatedAt = new \yii\db\Expression('NOW()');
          }
          return parent::beforeSave();
        }

        /**
         * Lists all EventDetails models.
         * @return mixed
         */
        public function actionIndex()
        {
            // $this->layout='addformdatatablelayout';

            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $events= EventDetails::find()->all();

            $shereheglobal=[];

            foreach ($events as $sherehe) {
              // code...

              // print_r($event);

              // echo $event->eventName;
              //
              // die();
              $event = new \yii2fullcalendar\models\Event();
              $event->id = $sherehe->id;
              // $event->title = 'Testing';
              $event->title =$sherehe->eventName;
              // $event->start = date('Y-m-d\Th:m:s\Z',strtotime('tomorrow 6am'));
              // date('Y-m-d\Th:i:s\Z',strtotime($time->date_start.' '.$time->time_start));
              // $event->start = $sherehe->eventStartDate;
              $event->start = date('Y-m-d\Th:i:s\Z',strtotime($sherehe->eventStartDate));
              $event->end = date('Y-m-d\Th:i:s\Z',strtotime($sherehe->eventEndDate));
              $shereheglobal[] = $event;
            }

            $searchModel = new EventDetailsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'events'=> $shereheglobal,
            ]);
        }

        /**
         * Displays a single EventDetails model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
            // $this->layout='addformdatatablelayout';

            $this->layout = '@app/views/layouts/addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new EventDetails model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
            // $this->layout='addformdatatablelayout';

            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = new EventDetails();
            //Getting user Log in details

            //checking if its a post
            if ($model->load(Yii::$app->request->post())) {
              // code...

              //Geting the time created Timestamp
              $model->eventCreatedAt= new \yii\db\Expression('NOW()');

              //Generating Event Number
              $eventNum=$model->getEnumber();
              $model->eventNumber= $eventNum;

              //Getting user Log in details

              $model->userId = Yii::$app->user->identity->id;

              $model->createdBy = Yii::$app->user->identity->id;

              $model->save();

              //after Saving the customer Details

              // return $this->redirect(['view', 'id' => $model->id]);
              return $this->redirect(['index']);

            } else {
              // code...

              //load the create form
              return $this->render('create', [
                  'model' => $model,
              ]);
            }




            // if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //     return $this->redirect(['view', 'id' => $model->id]);
            // }
            //
            // return $this->render('create', [
            //     'model' => $model,
            // ]);
        }

        /**
         * Updates an existing EventDetails model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
            // $this->layout='addformdatatablelayout';

            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = $this->findModel($id);
            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        /**
         * Deletes an existing EventDetails model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
            // $this->findModel($id)->delete();
            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = $this->findModel($id);

            // $model =YourModel::find()-where(['id'=>$id])->one();
            $model->status = '0';
            $model->eventDeletedAt =new \yii\db\Expression('NOW()');
            $model->save();

            return $this->redirect(['index']);
        }


        /**
         * Finds the EventDetails model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return EventDetails the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = EventDetails::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
