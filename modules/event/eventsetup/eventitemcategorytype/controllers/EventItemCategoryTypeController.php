<?php

namespace app\modules\event\eventsetup\eventitemcategorytype\controllers;

use Yii;
use app\modules\event\eventsetup\eventitemcategorytype\models\EventItemCategoryType;
use app\modules\event\eventsetup\eventitemcategorytype\models\EventItemCategoryTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventItemCategoryTypeController implements the CRUD actions for EventItemCategoryType model.
 */
class EventItemCategoryTypeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...

            $this->updatedAt =  new \yii\db\Expression('NOW()');

          }else {
            // code...
            $this->updatedAt =  new \yii\db\Expression('NOW()');
          }
          return parent::beforeSave();
        }

        /**
         * Lists all EventItemCategoryType models.
         * @return mixed
         */
        public function actionIndex()
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $searchModel = new EventItemCategoryTypeSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single EventItemCategoryType model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
            // return $this->redirect(['view', 'id' => $model->id]);
        }

        /**
         * Creates a new EventItemCategoryType model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $model = new EventItemCategoryType();
            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        /**
         * Updates an existing EventItemCategoryType model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $model = $this->findModel($id);

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        /**
         * Deletes an existing EventItemCategoryType model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the EventItemCategoryType model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return EventItemCategoryType the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            if (($model = EventItemCategoryType::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
