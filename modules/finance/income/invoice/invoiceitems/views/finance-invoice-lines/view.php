<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoiceLines */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Invoice Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-invoice-lines-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'invoiceId',
            'itemRefId',
            'itemRefName',
            'invoiceLinesDescription:ntext',
            'invoiceLinespack',
            'invoiceLinesQty',
            'invoiceLinesUnitPrice',
            'invoiceLinesAmount',
            'invoiceLinesTaxCodeRef',
            'invoiceLinesTaxamount',
            'invoiceLinesDiscount',
            'invoiceLinesCreatedAt',
            'invoiceLinesUpdatedAt',
            'invoiceLinesDeletedAt',
            'invoiceLinesStatus',
            'userId',
        ],
    ]) ?>

</div>
