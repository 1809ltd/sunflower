<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-requisition-payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reqpaymentPayType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentaccount')->textInput() ?>

    <?= $form->field($model, 'reqrecepitNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentTotalAmt')->textInput() ?>

    <?= $form->field($model, 'unallocatedAmount')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'reqpaymenteventReqRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reqpaymentPrivateNote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reqpaymentCreatedAt')->textInput() ?>

    <?= $form->field($model, 'reqpaymentUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'reqpaymentDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <?= $form->field($model, 'approvedBy')->textInput() ?>

    <?= $form->field($model, 'paymentstatus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
