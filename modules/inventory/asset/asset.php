<?php

namespace app\modules\inventory\asset;

/**
 * asset module definition class
 */
class asset extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\asset\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Asset Registration Sub Module*/
          'assetregistration' => [
              'class' => 'app\modules\inventory\asset\assetregistration\assetregistration',
          ],
          /*Valuation*/
          'assetvaluation' => [
              'class' => 'app\modules\inventory\asset\assetvaluation\assetvaluation',
          ],
          /*Asset amortization*/
          'assetamortization' => [
              'class' => 'app\modules\inventory\asset\assetamortization\assetamortization',
          ],
          /*Asset Location*/
          'assetlocation' => [
              'class' => 'app\modules\inventory\asset\assetlocation\assetlocation',
          ],
        ];

        // custom initialization code goes here
    }
}
