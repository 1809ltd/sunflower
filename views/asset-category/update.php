<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetCategory */

$this->title = 'Update Asset Category: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
