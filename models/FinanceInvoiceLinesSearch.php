<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceInvoiceLines;

/**
 * FinanceInvoiceLinesSearch represents the model behind the search form of `app\models\FinanceInvoiceLines`.
 */
class FinanceInvoiceLinesSearch extends FinanceInvoiceLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'invoiceId', 'itemRefId', 'invoiceLinesStatus', 'userId'], 'integer'],
            [['itemRefName', 'invoiceLinesDescription', 'invoiceLinesCreatedAt', 'invoiceLinesUpdatedAt', 'invoiceLinesDeletedAt'], 'safe'],
            [['invoiceLinespack', 'invoiceLinesQty', 'invoiceLinesUnitPrice', 'invoiceLinesAmount', 'invoiceLinesTaxCodeRef', 'invoiceLinesTaxamount', 'invoiceLinesDiscount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceInvoiceLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'invoiceId' => $this->invoiceId,
            'itemRefId' => $this->itemRefId,
            'invoiceLinespack' => $this->invoiceLinespack,
            'invoiceLinesQty' => $this->invoiceLinesQty,
            'invoiceLinesUnitPrice' => $this->invoiceLinesUnitPrice,
            'invoiceLinesAmount' => $this->invoiceLinesAmount,
            'invoiceLinesTaxCodeRef' => $this->invoiceLinesTaxCodeRef,
            'invoiceLinesTaxamount' => $this->invoiceLinesTaxamount,
            'invoiceLinesDiscount' => $this->invoiceLinesDiscount,
            'invoiceLinesCreatedAt' => $this->invoiceLinesCreatedAt,
            'invoiceLinesUpdatedAt' => $this->invoiceLinesUpdatedAt,
            'invoiceLinesDeletedAt' => $this->invoiceLinesDeletedAt,
            'invoiceLinesStatus' => $this->invoiceLinesStatus,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'itemRefName', $this->itemRefName])
            ->andFilterWhere(['like', 'invoiceLinesDescription', $this->invoiceLinesDescription]);

        return $dataProvider;
    }
}
