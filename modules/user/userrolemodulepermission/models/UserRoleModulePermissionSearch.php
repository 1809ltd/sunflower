<?php

namespace app\modules\user\userrolemodulepermission\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\user\userrolemodulepermission\models\UserRoleModulePermission;

/**
 * UserRoleModulePermissionSearch represents the model behind the search form of `app\modules\user\userrolemodulepermission\models\UserRoleModulePermission`.
 */
class UserRoleModulePermissionSearch extends UserRoleModulePermission
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'roleId', 'moduleId', 'new', 'view', 'save', 'remove', 'createdBy', 'userId', 'status'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserRoleModulePermission::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'roleId' => $this->roleId,
            'moduleId' => $this->moduleId,
            'new' => $this->new,
            'view' => $this->view,
            'save' => $this->save,
            'remove' => $this->remove,
            'createdBy' => $this->createdBy,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'status' => $this->status,
        ]);

        return $dataProvider;
    }
}
