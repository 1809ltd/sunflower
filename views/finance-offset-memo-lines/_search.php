<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceOffsetMemoLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-offset-memo-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'offsetMemoId') ?>

    <?= $form->field($model, 'invoiceNumber') ?>

    <?= $form->field($model, 'offsetMemoLineDescription') ?>

    <?= $form->field($model, 'offsetMemoLineAmount') ?>

    <?php // echo $form->field($model, 'offsetMemoLineStatus') ?>

    <?php // echo $form->field($model, 'offsetMemoLineCreatedAt') ?>

    <?php // echo $form->field($model, 'offsetMemoLineUpdatedAt') ?>

    <?php // echo $form->field($model, 'creidtDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
