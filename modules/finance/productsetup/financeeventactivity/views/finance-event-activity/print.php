<?php

use yii\helpers\Url;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
// Fiance Expense Acticty
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
?>
<section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-xs-12">
      <h2 class="page-header">
            <img style="height:25%;width:25%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
            <i class=""></i> Expense Activity.
            <small class="pull-right">As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">

        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">


        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Activity Name</th>
                <th>Description</th>
                <th>Account</th>
              </tr>
            </thead>
            <tbody>
              <?php
              // Getting the Parent Parent Account Name:

              $parentId= FinanceAccounts::find()
                              ->where('accountsStatus=1')
                              ->orderBy(
                                [
                                  new \yii\db\Expression('COALESCE(accountsClassification,accountParentId),accountsClassification,accountParentId,accountNumber')
                                ]
                                )
                                ->all();

              foreach ($parentId as $parentId) {
                // code...
                if ($parentId["accountParentId"]==0) {
                  // code...
                  // Get Expense Based on the account Id

                  $expenseactivity= FinanceEventActivity::find()->where(['activityStatus' => 1])->Andwhere(['account' => $parentId["id"]])
                                                ->orderBy(
                                                  [
                                                    new \yii\db\Expression('account')
                                                  ]
                                                  )
                                                  ->all();
                  // Check result set is empty
                  if (!empty($expenseactivity)) {
                    // code...
                    ?>
                    <tr>
                      <th style="background-color: gold;">Account: <span class="align-numbers"><?= $parentId["fullyQualifiedName"] ;?></span></th>
                      <th style="background-color: gold;"></th>
                      <th style="background-color: gold;"></th>
                    </tr>
                    <?php

                    foreach ($expenseactivity as $expenseactivity) {
                      // code...


                      ?>
                      <tr>
                        <td><?= $expenseactivity["activityName"] ?></td>
                        <td><span class="align-numbers"><?= $expenseactivity["activityDescription"] ?></span></td>
                        <td><?= $expenseactivity["account0"]["fullyQualifiedName"]?></td>
                      </tr>

                      <?php

                    }

                  } else {
                    // code...
                  }
                } else {
                  // code...
                  // Getting the Name of the Service Category if parent its not null


                  $expenseactivity= FinanceEventActivity::find()->where(['activityStatus' => 1])->Andwhere(['account' => $parentId["id"]])
                                                ->orderBy(
                                                  [
                                                    new \yii\db\Expression('account')
                                                  ]
                                                  )
                                                  ->all();
                  // Check result set is empty
                  if (!empty($expenseactivity)) {
                    // code...
                    ?>
                    <tr>
                      <th style="background-color: gold;">Account :<span class="align-numbers"><?= $parentId["fullyQualifiedName"] ;?></span></th>
                      <th style="background-color: gold;"></th>
                      <th style="background-color: gold;"></th>
                    </tr>
                    <?php

                    foreach ($expenseactivity as $expenseactivity) {
                      // code...


                      ?>
                      <tr>
                        <td><?= $expenseactivity["activityName"] ?></td>
                        <td><span class="align-numbers"><?= $expenseactivity["activityDescription"] ?></span></td>
                        <td><?= $expenseactivity["account0"]["fullyQualifiedName"]?></td>
                      </tr>

                      <?php

                    }

                  } else {
                    // code...
                  }

                }
              }
              ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>
        </div>
      </div>
    </section>
