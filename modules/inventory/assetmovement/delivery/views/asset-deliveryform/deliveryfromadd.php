<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
// use kartik\time\TimePicker;


/*Array*/
use yii\helpers\ArrayHelper;


use dosamigos\datepicker\DatePicker;

use app\modules\company\companysitelocation\models\CompanySiteLocation;
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
use app\modules\personnel\personneldetails\models\PersonnelDetails;
use app\modules\event\transportation\models\EventTransport;
use app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromList;

/* @var $this yii\web\View */
/* @var $checkoutitem app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body deliveryform-asset-check-out-form">

  <?= $form->field($checkoutitem, 'activityId')->hiddenInput()->label(false);?>
  <?= $form->field($checkoutitem, 'orderId')->hiddenInput()->label(false);?>
  <?= $form->field($checkoutitem, 'status')->hiddenInput(['value'=>2])->label(false);?>

  <?= $form->field($checkoutitem, 'activity')->hiddenInput()->label(false);?>

  <?= $form->field($checkoutitem, 'orderlistId')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(EventOrderfromList::find()
      ->joinWith('item')->all(),'id','item.itemsName'),
      'language' => 'en',
      'options' => ['placeholder' => 'Select a Order Details ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);

  ?>


    <?= $form->field($checkoutitem, 'locationId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CompanySiteLocation::find()->all(),'id','locationName'),
        'language' => 'en',
        'id'=>'location',
        'options' => ['placeholder' => 'Select a Asset Location ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?= $form->field($checkoutitem, 'assetId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AssetRegistration::find()->all(),'id','assetName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Asset ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>
    <?= $form->field($checkoutitem, 'vehicle')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(EventTransport::find()->all(),'id','numberPlate'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Transport ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>
    <?= $form->field($checkoutitem, 'assignto')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(PersonnelDetails::find()->all(),'id','displayName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Personnel ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>


</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($checkoutitem->isNewRecord ? 'Add' : 'Update', ['class' => $checkoutitem->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
