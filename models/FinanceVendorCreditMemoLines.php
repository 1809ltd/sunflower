<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_vendor_credit_memo_lines".
 *
 * @property int $id
 * @property int $vendorId
 * @property int $vendorCreditMemoId
 * @property int $billRef
 * @property string $vendorCreditMemoLineDescription
 * @property double $vendorCreditMemoLineAmount
 * @property int $vendorCreditMemoLineStatus
 * @property string $vendorCreditMemoLineCreatedAt
 * @property string $vendorCreditMemoLineUpdatedAt
 * @property string $creidtDeletedAt
 * @property int $userId
 *
 * @property FinanceVendorCreditMemo $vendorCreditMemo
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 * @property FinanceBills $billRef0
 */
class FinanceVendorCreditMemoLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_vendor_credit_memo_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billRef', 'vendorCreditMemoLineAmount'], 'required'],
            [['vendorId', 'vendorCreditMemoId', 'billRef', 'vendorCreditMemoLineStatus', 'userId'], 'integer'],
            [['vendorCreditMemoLineDescription'], 'string'],
            [['vendorCreditMemoLineAmount'], 'number'],
            [['vendorId', 'vendorCreditMemoId', 'vendorCreditMemoLineStatus', 'userId', 'vendorCreditMemoLineCreatedAt', 'vendorCreditMemoLineUpdatedAt', 'creidtDeletedAt'], 'safe'],
            [['vendorCreditMemoId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceVendorCreditMemo::className(), 'targetAttribute' => ['vendorCreditMemoId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['billRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBills::className(), 'targetAttribute' => ['billRef' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorId' => 'Vendor',
            'vendorCreditMemoId' => 'Credit Memo',
            'billRef' => 'Bill Ref',
            'vendorCreditMemoLineDescription' => 'Description',
            'vendorCreditMemoLineAmount' => 'Amount',
            'vendorCreditMemoLineStatus' => 'Status',
            'vendorCreditMemoLineCreatedAt' => 'Created At',
            'vendorCreditMemoLineUpdatedAt' => 'Updated At',
            'creidtDeletedAt' => 'Creidt Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorCreditMemo()
    {
        return $this->hasOne(FinanceVendorCreditMemo::className(), ['id' => 'vendorCreditMemoId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillRef0()
    {
        return $this->hasOne(FinanceBills::className(), ['id' => 'billRef']);
    }
}
