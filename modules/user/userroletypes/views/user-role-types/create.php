<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userroletypes\models\UserRoleTypes */

$this->title = 'Create User Role';
$this->params['breadcrumbs'][] = ['label' => 'User Role Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-types-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelUserRoleModulePermissions' => $modelUserRoleModulePermissions,
    ]) ?>


</div>
