<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventWorkplan */

$this->title = 'Add Event Workplan';
$this->params['breadcrumbs'][] = ['label' => 'Event Workplans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-workplan-create">
  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
