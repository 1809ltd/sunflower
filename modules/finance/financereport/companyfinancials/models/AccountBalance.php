<?php

namespace app\modules\finance\financereport\companyfinancials\models;

use Yii;

/**
 * This is the model class for table "account_balance".
 *
 * @property int $accountParentId
 * @property string $accountsclassfication
 * @property int $accountId
 * @property string $accountName
 * @property double $dr_amount
 * @property double $cr_amount
 * @property double $balance
 */
class AccountBalance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'account_balance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['accountParentId', 'accountId'], 'integer'],
            [['dr_amount', 'cr_amount', 'balance'], 'number'],
            [['accountsclassfication', 'accountName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'accountParentId' => 'Account Parent ID',
            'accountsclassfication' => 'Accountsclassfication',
            'accountId' => 'Account ID',
            'accountName' => 'Account Name',
            'dr_amount' => 'Dr Amount',
            'cr_amount' => 'Cr Amount',
            'balance' => 'Balance',
        ];
    }
}
