<?php

namespace app\modules\inventory\assetsetup;

/**
 * assetsetup module definition class
 */
class assetsetup extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\assetsetup\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Asset Category Sub Module*/
          'assetcategory' => [
              'class' => 'app\modules\inventory\assetsetup\assetcategory\assetcategory',
          ],
          /*Asset CAtegory Type */
          'assetcategorytype' => [
              'class' => 'app\modules\inventory\assetsetup\assetcategorytype\assetcategorytype',
          ],
        ];

        // custom initialization code goes here
    }
}
