<?php

namespace app\modules\personnel\personneldetails\controllers;

use Yii;
use app\modules\personnel\personneldetails\models\PersonnelDetails;
use app\modules\personnel\personneldetails\models\PersonnelDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PersonnelDetailsController implements the CRUD actions for PersonnelDetails model.
 */
class PersonnelDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...

            $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          }else {
            // code...
            $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
          }
          return parent::beforeSave();
        }


        /**
         * Lists all PersonnelDetails models.
         * @return mixed
         */
        public function actionIndex()
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $searchModel = new PersonnelDetailsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single PersonnelDetails model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new PersonnelDetails model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $model = new PersonnelDetails();

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;
            $model->createdBy= Yii::$app->user->identity->id;
            $model->createdAt= new \yii\db\Expression('NOW()');

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }

        /**
         * Updates an existing PersonnelDetails model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            $model = $this->findModel($id);

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }

        /**
         * Deletes an existing PersonnelDetails model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';

            // $this->findModel($id)->delete();
            $this->findModel($id)->updateAttributes(['status'=>0,'deletedAt' => new \yii\db\Expression('NOW()')]);
            // Confriming that the post is not empty

            return $this->redirect(['index']);
        }

        /**
         * Finds the PersonnelDetails model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return PersonnelDetails the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          // $this->layout='addformdatatablelayout';
            if (($model = PersonnelDetails::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
