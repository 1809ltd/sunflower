<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceItems */

$this->title = 'Create Product or Service';
$this->params['breadcrumbs'][] = ['label' => 'Product or Service', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-items-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
