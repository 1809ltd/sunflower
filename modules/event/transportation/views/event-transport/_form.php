<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;


/*AssetREgisrtartion*/
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;

use app\modules\event\eventInfo\models\EventDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Order form */
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
/*VEndor details*/
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;

/*Dependant Drop*/
use kartik\depdrop\DepDrop;

// use kartik\widgets\DateTimePicker;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\event\transportation\models\EventTransport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body event-transport-form">

  <div class="row">
    <div class="col-sm-4">
        <div class="form-group">
          <?= $form->field($model, 'customerId')->widget(Select2::classname(), [
              'data' => ArrayHelper::map(CustomerDetails::find()->where('`customerstatus` = 1')->asArray()->all(),'id','customerDisplayName'),
              'language' => 'en',
              'options' => ['id'=>'customerId','prompt'=>'Select...'],
              'pluginOptions' => [
                  'allowClear' => true
              ],
          ]);?>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
          <?= $form->field($model, 'eventId')->widget(DepDrop::classname(), [
              'options'=>['id'=>'eventId'],
              'data' => [$model->eventId => $model->eventId],
              'type' => DepDrop::TYPE_SELECT2,
              'pluginOptions'=>[
                  'depends'=>['customerId'],
                  'initialize' => true,
                  // 'initDepends'=>['customerId'],
                  'placeholder'=>'Select...',
                  'url'=>Url::to(['/event/eventdocs/orderfroms/orderformsitems/event-orderfrom-list/eventdetails'])
              ]
          ]);?>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="form-group">
          <?= $form->field($model, 'orderId')->widget(DepDrop::classname(), [
              'type' => DepDrop::TYPE_SELECT2,
              'data' => [$model->orderId => $model->orderId],
              'pluginOptions'=>[
                  'depends'=>['customerId', 'eventId'],
                  'initialize' => true,
                  'initDepends'=>['customerId', 'eventId'],
                  'placeholder'=>'Select...',
                  'url'=>Url::to(['/event/eventdocs/orderfroms/orderformsitems/event-orderfrom-list/ordernumber'])
              ]
          ]); ?>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
        <?php $source = ['1' => 'Yes', '2' => 'No']; ?>
        <?= $form->field($model, 'outsourced')->dropDownList($source, ['prompt'=>'is it external Supplier','class'=>'form-control']);?>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="form-group">
        <?= $form->field($model, 'vendorId')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(VendorCompanyDetails::find()->all(),'id','vendorCompanyName'),
            'language' => 'en',
            'options' => ['placeholder' => 'Select a Vendor ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>
      </div>
    </div>

  </div>

<div class="row">
  <div class="col-sm-3">
    <div class="form-group">
      <?= $form->field($model, 'assetId')->widget(Select2::classname(), [
          'data' => ArrayHelper::map(AssetRegistration::find()->all(),'id','assetName'),
          'language' => 'en',
          'options' => ['placeholder' => 'Select a Asset ...'],
          'pluginOptions' => [
              'allowClear' => true
          ],
      ]);

      ?>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
      <?= $form->field($model, 'numberPlate')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>
    </div>
  </div>

  <div class="col-sm-3">
    <div class="form-group">
      <?php $chois = ['1' => 'Yes', '2' => 'No']; ?>
    <?= $form->field($model, 'delivery')->dropDownList($chois, ['prompt'=>'Select Status','class'=>'form-control']);?>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
      <?php $chois = ['1' => 'Yes', '2' => 'No']; ?>
      <?= $form->field($model, 'collection')->dropDownList($chois, ['prompt'=>'Select Status','class'=>'form-control']);?>
    </div>
  </div>

</div>


<div class="row">

  <div class="col-sm-3">
    <div class="form-group">
      <?= $form->field($model, 'deliveryTime')->widget(DateTimePicker::classname(), [
              'options' => ['autocomplete'=>'off','placeholder' => 'Enter event Start time ...'],
              'pluginOptions' => [
                'autoclose' => true
              ]
            ]); ?>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
      <?= $form->field($model, 'collectionTime')->widget(DateTimePicker::classname(), [
              'options' => ['autocomplete'=>'off','placeholder' => 'Enter event Start time ...'],
              'pluginOptions' => [
                'autoclose' => true
              ]
            ]); ?>
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
      <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
      <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>

    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">

    </div>
  </div>
</div>


<div class="row">

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
