<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userdetails\models\UserDetails */

$this->title = 'Update User Details: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-details-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
