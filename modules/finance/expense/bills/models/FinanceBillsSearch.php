<?php

namespace app\modules\finance\expense\bills\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expense\bills\models\FinanceBills;

/**
 * FinanceBillsSearch represents the model behind the search form of `app\modules\finance\expense\bills\models\FinanceBills`.
 */
class FinanceBillsSearch extends FinanceBills
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','billStatus', 'userId'], 'integer'],
            [['billRef', 'billDueDate', 'projectId', 'vendorId', 'billCreatetime', 'billUpdatedTime', 'billDeletedAt'], 'safe'],
            [['billAmount', 'billsTaxAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBills::find();
        $query->joinWith(['vendor']);
        $query->joinWith(['project']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'projectId' => $this->projectId,
            // 'vendorId' => $this->vendorId,
            'billDueDate' => $this->billDueDate,
            'billAmount' => $this->billAmount,
            'billsTaxAmount' => $this->billsTaxAmount,
            'billCreatetime' => $this->billCreatetime,
            'billUpdatedTime' => $this->billUpdatedTime,
            'billDeletedAt' => $this->billDeletedAt,
            'billStatus' => $this->billStatus,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'billRef', $this->billRef]);
        $query->andFilterWhere(['like', 'vendorCompanyName', $this->vendorId]);
        $query->andFilterWhere(['like', 'eventName', $this->projectId]);

        return $dataProvider;
    }
}
