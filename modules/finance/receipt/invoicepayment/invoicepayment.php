<?php

namespace app\modules\finance\receipt\invoicepayment;

/**
 * invoicepayment module definition class
 */
class invoicepayment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\receipt\invoicepayment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
