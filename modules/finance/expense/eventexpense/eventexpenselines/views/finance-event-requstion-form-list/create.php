<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventRequstionFormList */

$this->title = 'Create Finance Event Requstion Form List';
$this->params['breadcrumbs'][] = ['label' => 'Finance Event Requstion Form Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-requstion-form-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
