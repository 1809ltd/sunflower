<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventRequstionForm */

$this->title = 'Update Office Requstion Form: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Office Requstion Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-event-requstion-form-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceOfficeRequstionFormList'=>$modelsFinanceOfficeRequstionFormList, //to enable Dynamic Form
    ]) ?>

</div>
