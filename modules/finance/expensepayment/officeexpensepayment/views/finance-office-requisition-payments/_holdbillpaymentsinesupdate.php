<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


	/*to enable modal pop up*/
	use yii\bootstrap\Modal;
	use yii\helpers\Url;

	// print_r($model);
// echo \Yii::$app->session->get('eventId');
?>
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title">Add an Office Requstion</h3>
		<div class="box-tools pull-right">
			<?= Html::button('Add Requstion', ['value'=>Url::to((['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments-lines/create','officeReqpaymentId'=>$model->id,'status'=>$model->paymentstatus])),'class' => 'btn btn-sm btn-success','id'=>'modalButton']) ?>
		</div>
	</div>
	<?php
	Modal::begin([
		// 'header'=>'<h3>Add Customer</h3>',
		'class'=>'modal-dialog',
		'id'=>'modal',
		'size'=>'modal-lg',
		'options' => [
				'tabindex' => false // important for Select2 to work properly
		],
	]);
	echo "<div class='modal-content' id='modalContent'></div>";
	Modal::end();
	?>
	<?php
	Modal::begin([
		'header'=>'<h4>Update Model</h4>',
		'id'=>'update-modal',
		'size'=>'modal-lg',
		'options' => [
				'tabindex' => false // important for Select2 to work properly
		],
	]);
	echo "<div id='updateModalContent'></div>";
	Modal::end();
	?>
	<div class="box-body">
		<?php Pjax::begin(['id' => 'holdbillpayment']); ?>
		<?php
		// Assign the output to dataProvider to be able to be view in grid
		$dataProvider = new \yii\data\ArrayDataProvider([
			'key'=>'id',
			'allModels' => $output,
			// 'sort' => [
			//     'attributes' => ['id', 'billRef', 'amount'],
			// ],
		]);

		echo GridView::widget([
			'dataProvider' => $dataProvider,
			// 'filterModel' => $searchModel,
			// 'dataProvider' => $output,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				// 'id',
				// 'officeReqpaymentId',
				[
					'label' => 'Requstion No:',
					'attribute'=>'officeReqId',
					'value'=>'officeReq.requstionNumber',
				],
				// 'officeReq.requstionNumber',
				// 'officeReqId',
				'amount',
				// 'status',
				// ['class' => 'yii\grid\ActionColumn'],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{view}{update}{delete}',
					'buttons' => ['view' => function($url, $model) {
						return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
					},
					'update' => function($url,$model,$key){
						$btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
							'value'=>Url::to((['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments-lines/update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
							'class'=>'update-modal-click grid-action',
							'data-toggle'=>'tooltip',
							'data-placement'=>'bottom',
							'title'=>'Update'
						]);
						return $btn;
					},
					'delete' => function($url, $model) {
						return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments-lines/delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
					}
				]
			],
		],
	]); ?>
	<?php Pjax::end(); ?>
	<br>

</div>
</div>
