<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companydepartment\models\CompanyDepartmentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-department-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'companyId') ?>

    <?= $form->field($model, 'departmentName') ?>

    <?= $form->field($model, 'departmentCode') ?>

    <?= $form->field($model, 'departmentTimestamp') ?>

    <?php // echo $form->field($model, 'departmentUpdatedAt') ?>

    <?php // echo $form->field($model, 'departmentDeleteAt') ?>

    <?php // echo $form->field($model, 'departmentStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
