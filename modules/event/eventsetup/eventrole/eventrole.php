<?php

namespace app\modules\event\eventsetup\eventrole;

/**
 * eventrole module definition class
 */
class eventrole extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventsetup\eventrole\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
