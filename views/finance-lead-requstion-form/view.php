<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use app\models\FinanceInvoiceLines;
use app\models\FinanceInvoiceLinesSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequstionForm */

$this->title = $model->requstionNumber;
$this->params['breadcrumbs'][] = ['label' => 'Lead Requisition Form', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?=$model->date;?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>
           <strong>SUNFLOWER EVENTS,</strong><br/>
            Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
           Nairobi, 00100 Kenya<br/>
           Phone: +254 (0) 722 790632<br/>
           Email: info@...com
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>
           <?php $eventinfo=$model->geteventDetailss($model->leadId); ?>
           <?php foreach ($eventinfo as $eventinfo): ?>
               <small>Event</small>
                   <strong><?= $eventinfo->leadprojectName;?></strong><br/>

                   <?php endforeach; ?>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Office Req:  #<?=$model->requstionNumber;?></b><br>
         <br>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
           <tr>
               <th>Office Expenses</th>
               <th>Quantity</th>
               <th>Price</th>
               <th>Amount(Ksh)</th>
           </tr>
           </thead>
           <tbody>
             <?php
             $FinanceLeadRequstionFormLists=$model->getFinanceLeadRequstionFormList($model->id);


             foreach ($FinanceLeadRequstionFormLists as $FinanceLeadRequstionFormList) {
               // code...


               $FinanceLeadRequstionFormList->activityId;
               $FinanceLeadRequstionFormList->personelId;
               $FinanceLeadRequstionFormList->personnelTel;
               $FinanceLeadRequstionFormList->description;
               $FinanceLeadRequstionFormList->qty;
               $FinanceLeadRequstionFormList->unitprice;
               $FinanceLeadRequstionFormList->amont;

               ?>
               <tr>
                   <td>
                     <?php
                       $FinanceLeadRequstionFormListsactivityName=$model->getActivity($FinanceLeadRequstionFormList->activityId);
                     ?>
                     <?php foreach ($FinanceLeadRequstionFormListsactivityName as $FinanceLeadRequstionFormListsactivityName){

                       echo $FinanceLeadRequstionFormListsactivityName->activityName;
                     }
                     ?><br/>
                       <small><?= $FinanceLeadRequstionFormList->description;?>.
                       </small>
                   </td>
                   <td><?= $FinanceLeadRequstionFormList->qty;;?></td>
                   <td><?= $FinanceLeadRequstionFormList->unitprice;?></td>
                   <td><?= $FinanceLeadRequstionFormList->amont;?></td>
               </tr>

               <?php

             }

              ?>

              <tr>
                  <td>
                       <br/>
                      <small>.
                      </small>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>

              </tr>


           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Office Requistion Note:</p>


         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           * <?= $model->requstionNote; ?><br/>
         </p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Prepared by:</th>
               <td>Ksh <?= $model->preparedBy; ?></td>
             </tr>
             <tr>
               <th>Requested by:</th>
               <td>Ksh <?= $model->amount; ?></td>
             </tr>
             <tr>
               <th>Approved By:</th>
               <td>Ksh <?= $model->approvedBy; ?></td>
             </tr>
           </table>
         </div>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="lead">Amount Due <?= $model->date; ?></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Total:</th>
               <td>Ksh <?= $model->amount; ?></td>
             </tr>
           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
         <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Make Payment
         </button>
         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->


<?php


/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequstionForm */

// $this->title = $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Finance Lead Requstion Forms', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
// \yii\web\YiiAsset::register($this);

?>
<div class="finance-lead-requstion-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'requstionNumber',
            'leadId',
            'date',
            'requstionStatus',
            'requstionNote:ntext',
            'amount',
            'preparedBy',
            'createdAt',
            'updatedAt',
            'deletedAt',
            'userId',
            'approvedBy',
        ],
    ]) ?>

</div>
