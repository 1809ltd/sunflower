<?php

namespace app\modules\finance\expensepayment\officeexpensepayment\controllers;

use Yii;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsSearch;

// Lines to update

use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLinesSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceOfficeRequisitionPaymentsController implements the CRUD actions for FinanceOfficeRequisitionPayments model.
 */
class FinanceOfficeRequisitionPaymentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinanceOfficeRequisitionPayments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';

        $searchModel = new FinanceOfficeRequisitionPaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceOfficeRequisitionPayments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceOfficeRequisitionPayments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $model = new FinanceOfficeRequisitionPayments();

        // Validate if all the data is inserted
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
          Yii :: $app->response->format = 'json';
          return \yii\bootstrap\ActiveForm::validate($model);
        }

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;
          $model->createdBy= Yii::$app->user->identity->id;

          $model->officeReqPaymentCreatedAt= new \yii\db\Expression('NOW()');;

          // $deliveryNumber=$model->getDevnumber();
          // $model->deliveryNumber= $deliveryNumber;

          // print_r($model->attributes);
          // die();

          $model->save();
          // code...

          if (!empty($model->id)) {
            // code...
            $officeReqpaymentlines = FinanceOfficeRequisitionPaymentsLines::find()
                                        ->Where('status = :status', [':status' => 3])
                                        ->Andwhere(['finance_office_requisition_payments_lines.userId'=>Yii::$app->user->identity->id])
                                        ->all();

            foreach ($officeReqpaymentlines as $officeReqpaymentlines) {
              // code...
              // Update all the data based on the payment made and status
              FinanceOfficeRequisitionPaymentsLines::findOne($officeReqpaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus,'officeReqpaymentId' =>$model->id]);

            }

            //after Saving the Payment Items Details
            /*After an Update to check out table*/
            return $this->redirect(['view', 'id' => $model->id]);

          }

        } else {
          // code...
          return $this->render('create', [
              'model' => $model,
          ]);
        }

    }

    /**
     * Updates an existing FinanceOfficeRequisitionPayments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $model = $this->findModel($id);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('update', [
        //     'model' => $model,
        // ]);
        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;
          $model->createdBy= Yii::$app->user->identity->id;

          $model->officeReqPaymentCreatedAt= new \yii\db\Expression('NOW()');;

          // $deliveryNumber=$model->getDevnumber();
          // $model->deliveryNumber= $deliveryNumber;

          // print_r($model->attributes);
          // die();

          $model->save();
          // code...

          if (!empty($model->id)) {
            // code...
            $officeReqpaymentlines = FinanceOfficeRequisitionPaymentsLines::find()
                                        ->Where('officeReqpaymentId = :officeReqpaymentId', [':officeReqpaymentId' => $model->id])
                                        ->all();

            foreach ($officeReqpaymentlines as $officeReqpaymentlines) {
              // code...
              // Update all the data based on the payment made and status
              FinanceOfficeRequisitionPaymentsLines::findOne($officeReqpaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus]);

            }

            //after Saving the Payment Items Details
            /*After an Update to check out table*/
            return $this->redirect(['view', 'id' => $model->id]);

          }

        } else {
          // code...
          return $this->render('update', [
              'model' => $model,
          ]);
        }
    }

    /**
     * Deletes an existing FinanceOfficeRequisitionPayments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        // $this->findModel($id)->delete();

        $this->findModel($id)->updateAttributes(['paymentstatus'=>0,'officeReqPaymentDeletedAt' => new \yii\db\Expression('NOW()')]);
        // Confriming that the post is not empty
        if (!empty($id)) {
              // code...
              $billpaymentlines = FinanceOfficeRequisitionPaymentsLines::find()
              ->Where('officeReqpaymentId = :officeReqpaymentId', [':officeReqpaymentId' => $id])
              // ->andWhere('status = :status', [':status' => 3])
              ->all();

              foreach ($billpaymentlines as $billpaymentlines) {
                // code...
                // Update all the data based on the payment made and status
                FinanceOfficeRequisitionPaymentsLines::findOne($billpaymentlines["id"])->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);

              }
            }

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceOfficeRequisitionPayments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceOfficeRequisitionPayments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceOfficeRequisitionPayments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
