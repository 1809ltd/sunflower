DROP VIEW
IF
	EXISTS `tax_report`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root` @`localhost` SQL SECURITY DEFINER VIEW `tax_report` AS -- Invoice Taxs
SELECT
	finance_invoice_lines.invoiceId AS transactionId,
	finance_invoice.id AS referenceId,
	finance_invoice.invoiceTxnDate AS date,
	finance_invoice.customerId AS recipientId,
	finance_invoice.invoiceNumber AS transactionCode,
	customer_details.customerCompanyName AS parties,
	CONCAT ( "Invoice Number->  ", finance_invoice.invoiceNumber, "  for Customer:  ", customer_details.customerCompanyName ) AS details,
	finance_invoice_lines.invoiceLinesAmount AS taxableamount,
	'0' AS dr_amount,
	finance_invoice_lines.invoiceLinesTaxamount AS cr_amount,
	finance_tax.taxCode AS taxCode,
	finance_tax.taxRate AS taxRate,
	customer_details.customerKraPin AS kraPin,
	"customer" AS typeofRecipeint,
	"invoice" AS typeofTransaction
FROM
	finance_invoice_lines
	INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
	INNER JOIN finance_tax ON finance_invoice_lines.invoiceLinesTaxCodeRef = finance_tax.taxRate
	INNER JOIN customer_details ON finance_invoice.customerId = customer_details.id
WHERE
	finance_invoice_lines.invoiceLinesStatus <> 0
	OR finance_invoice.invoiceTxnStatus <> 0 UNION ALL-- Inoice credit note tax
SELECT
	finance_credit_memo_lines.id AS transactionId,
	finance_credit_memo.id AS referenceId,
	finance_credit_memo.creditMemoTxnDate AS date,
	customer_details.id AS recipientId,
	finance_credit_memo.refernumber AS transactionCode,
	customer_details.customerCompanyName AS parties,
	CONCAT ( "CreditMemo Number-> ", finance_credit_memo.refernumber, "  for Customer: ", customer_details.customerCompanyName ) AS details,
	finance_credit_memo_lines.creditMemoLineAmount AS taxableamount,
	finance_credit_memo_lines.taxamount AS dr_amount,
	'0' AS cr_amount,
	finance_tax.taxCode AS taxCode,
	finance_tax.taxRate AS taxRate,
	customer_details.customerKraPin AS kraPin,
	"customer" AS typeofRecipeint,
	"Credit Note" AS typeofTransaction
FROM
	finance_credit_memo_lines
	INNER JOIN finance_credit_memo ON finance_credit_memo_lines.creditMemoId = finance_credit_memo.id
	INNER JOIN finance_tax ON finance_credit_memo_lines.creditTaxCodeRef = finance_tax.taxRate
	INNER JOIN customer_details ON finance_credit_memo.customerId = customer_details.id
	INNER JOIN finance_invoice ON finance_credit_memo_lines.invoiceNumber = finance_invoice.invoiceNumber
WHERE
	finance_credit_memo_lines.creditMemoLineStatus <> 0
	OR finance_credit_memo.creditMemoStatus <> 0
	OR finance_invoice.invoiceTxnStatus <> 0 UNION ALL-- Bills tax from Vendors
SELECT
	finance_bills_lines.biilsId AS transactionId,
	finance_bills.id AS referenceId,
	finance_bills.billDueDate AS DATE,
	finance_bills.vendorId AS recipientId,
	finance_bills.billRef AS transactionCode,
	vendor_company_details.vendorCompanyName AS parties,
	CONCAT ( "Bill: Invoice Number->  ", finance_bills.billRef, "  for Supplier:  ", vendor_company_details.vendorCompanyName ) AS details,
	finance_bills_lines.biilsLinesAmount AS taxableamount,
	finance_bills_lines.biilsLinesTaxAmount AS dr_amount,
	'0' AS cr_amount,
	finance_tax.taxCode AS taxCode,
	finance_tax.taxRate AS taxRate,
	vendor_company_details.vendorKraPin AS kraPin,
	"vendor" AS typeofRecipeint,
	"Bill" AS typeofTransaction
FROM
	finance_bills_lines
	INNER JOIN finance_bills ON finance_bills_lines.biilsId = finance_bills.id
	INNER JOIN vendor_company_details ON finance_bills.vendorId = vendor_company_details.id
	INNER JOIN finance_tax ON finance_bills_lines.billLinesTaxCodeRef = finance_tax.taxRate
WHERE
	finance_bills_lines.billsLinesBillableStatus <> 0
	OR finance_bills.billStatus <> 0 UNION ALL-- Vendor Credit note
SELECT
	finance_vendor_credit_memo_lines.id AS transactionId,
	finance_vendor_credit_memo.id AS referenceId,
	finance_vendor_credit_memo.vendorCreditMemoTxnDate AS DATE,
	finance_vendor_credit_memo.vendorId AS recipientId,
	finance_vendor_credit_memo.refernumber AS transactionCode,
	vendor_company_details.vendorCompanyName AS parties,
	CONCAT ( "Vendor Credit Memo: Reference Number->   ", finance_vendor_credit_memo.refernumber, "  from Supplier: ", vendor_company_details.vendorCompanyName ) AS details,
	finance_vendor_credit_memo_lines.vendorCreditMemoLineAmount AS taxableamount,
	'0' AS dr_amount,
	finance_vendor_credit_memo_lines.taxamount AS cr_amount,
	finance_tax.taxCode AS taxCode,
	finance_tax.taxRate AS taxRate,
	vendor_company_details.vendorKraPin AS kraPin,
	"vendor" AS typeofRecipeint,
	"Vendor Credit Note" AS typeofTransaction
FROM
	finance_vendor_credit_memo_lines
	INNER JOIN finance_vendor_credit_memo ON finance_vendor_credit_memo.id = finance_vendor_credit_memo_lines.vendorCreditMemoId
	INNER JOIN finance_bills ON finance_vendor_credit_memo_lines.billRef = finance_bills.id
	INNER JOIN finance_tax ON finance_vendor_credit_memo_lines.creditTaxCodeRef = finance_tax.taxRate
	INNER JOIN vendor_company_details ON finance_vendor_credit_memo.vendorId = vendor_company_details.id
WHERE
	finance_vendor_credit_memo_lines.vendorCreditMemoLineStatus <> 0
	OR finance_vendor_credit_memo.vendorCreditMemoTxnDate <> 0
	OR finance_bills.billStatus <> 0 -- Arrage based on the date

ORDER BY
	date ASC
