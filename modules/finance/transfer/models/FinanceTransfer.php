<?php

namespace app\modules\finance\transfer\models;
/*Chart of Accounts*/
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*User Details*/
use app\models\UserDetails;


use Yii;

/**
 * This is the model class for table "finance_transfer".
 *
 * @property int $id
 * @property string $tranferRefernce
 * @property int $tranferFromAccountRef
 * @property int $tranferToAccountRef
 * @property double $tranferAmount
 * @property double $transferCharges
 * @property string $tranferTxnDate
 * @property string $tranferCreatedAt
 * @property string $tranferUpdatedAt
 * @property string $tranferDeletedAt
 * @property int $userId
 * @property int $status
 * @property int $createdBy
 *
 * @property FinanceAccounts $tranferFromAccountRef0
 * @property FinanceAccounts $tranferToAccountRef0
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property FinanceTransferee[] $financeTransferees
 */
class FinanceTransfer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_transfer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tranferRefernce', 'tranferFromAccountRef', 'tranferToAccountRef', 'tranferAmount', 'tranferTxnDate','status'], 'required'],
            [['tranferFromAccountRef', 'tranferToAccountRef', 'userId', 'status', 'createdBy'], 'integer'],
            [['tranferAmount', 'transferCharges'], 'number'],
            [['userId', 'tranferCreatedAt', 'tranferDeletedAt', 'tranferTxnDate', 'tranferCreatedAt', 'tranferUpdatedAt', 'tranferDeletedAt', 'createdBy'], 'safe'],
            [['tranferRefernce'], 'string', 'max' => 100],
            [['tranferFromAccountRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['tranferFromAccountRef' => 'id']],
            [['tranferToAccountRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['tranferToAccountRef' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tranferRefernce' => 'Tranfer Refernce',
            'tranferFromAccountRef' => 'From Account',
            'tranferToAccountRef' => 'To Account',
            'tranferAmount' => 'Amount',
            'transferCharges' => 'Transfer Charges',
            'tranferTxnDate' => 'Date',
            'tranferCreatedAt' => 'Created At',
            'tranferUpdatedAt' => 'Updated At',
            'tranferDeletedAt' => 'Deleted At',
            'userId' => 'User',
            'status' => 'Status',
            'createdBy' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranferFromAccountRef0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'tranferFromAccountRef']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranferToAccountRef0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'tranferToAccountRef']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceTransferees()
    {
        return $this->hasMany(FinanceTransferee::className(), ['transferid' => 'id']);
    }
    /*Drep Drop Dropdown*/
    /*Step Two AFter Selection of Tranfer From acount Details*/
    /*Get Event List For a Specic Client*/
    public static function getAccountstranferee($id)
    {
      // code...
      $out = [];

      $data = FinanceAccounts::find()
                ->where(['NOT IN', 'id', [$id]])
                // (['id' => $id])
                ->andWhere(['like', 'accountsPays', 'yes'])
                // ->joinWith('item')
                ->andWhere(['=', 'accountsStatus', '1'])
                ->asArray()
                ->all();

        foreach ($data as $dat) {

            $out[] = ['id' => $dat['id'], 'name' => $dat['fullyQualifiedName']];
        }

        return $output = [
            'output' => $out,
            'selected' => ''
        ];

    }
}
