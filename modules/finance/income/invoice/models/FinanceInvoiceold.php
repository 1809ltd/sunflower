<?php

namespace app\modules\finance\income\invoice\models;
/*All Payments*/
use app\modules\finance\receipt\invoicepayment\models\FinancePayment;
use app\modules\finance\offset\inhouse\inoffsetinvoice\models\FinanceOffsetMemoLines;
use app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\models\FinanceCreditMemoLines;

/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Event*/
use app\modules\event\eventInfo\models\EventDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Customer Purchase order*/
use app\modules\finance\purchaseorder\purchaseordercustomer\models\FinanceCustomerPurchaseOrder;
/*Invoice Items*/
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLines;
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLinesSearch;


use Yii;

/**
 * This is the model class for table "finance_invoice".
 *
 * @property int $id
 * @property string $invoiceNumber
 * @property int $customerId
 * @property int $localpurchaseOrder
 * @property int $invoiceProjectId
 * @property string $invoiceTxnDate
 * @property string $invoiceDeadlineDate
 * @property string $invoiceNote
 * @property string $invoiceFooter
 * @property string $invoiceTxnStatus
 * @property int $invoiceEmailStatus
 * @property string $invoiceBillEmail
 * @property double $invoiceTaxAmount
 * @property double $invoiceDiscountAmount
 * @property double $invoiceSubAmount
 * @property double $invoiceAmount
 * @property int $invoicedCreatedby
 * @property string $invoicedCreatedAt
 * @property string $invoicedUpdatedAt
 * @property string $invoicedDeletedAt
 * @property int $approvedby
 * @property int $userId
 *
 * @property FinanceCreditMemoLines[] $financeCreditMemoLines
 * @property UserDetails $invoicedCreatedby0
 * @property FinanceCustomerPurchaseOrder $localpurchaseOrder0
 * @property EventDetails $invoiceProject
 * @property CustomerDetails $customer
 * @property UserDetails $user
 * @property UserDetails $approvedby0
 * @property FinanceInvoiceLines[] $financeInvoiceLines
 * @property FinanceOffsetMemoLines[] $financeOffsetMemoLines
 */
class FinanceInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoiceNumber', 'customerId', 'invoiceTxnDate', 'invoiceDeadlineDate', 'invoiceTxnStatus',  'invoiceTaxAmount', 'invoiceDiscountAmount', 'invoiceSubAmount', 'invoiceAmount', 'invoicedCreatedby', 'invoicedCreatedAt', 'userId'], 'required'],
            [['customerId', 'localpurchaseOrder', 'invoiceProjectId', 'invoiceEmailStatus', 'invoicedCreatedby', 'approvedby', 'userId'], 'integer'],
            [['invoiceProjectId','invoiceBillEmail','invoiceTxnDate', 'invoiceDeadlineDate', 'invoicedCreatedAt', 'invoicedUpdatedAt', 'invoicedDeletedAt'], 'safe'],
            [['invoiceNote', 'invoiceFooter', 'invoiceTxnStatus'], 'string'],
            [['invoiceTaxAmount', 'invoiceDiscountAmount', 'invoiceSubAmount', 'invoiceAmount'], 'number'],
            [['invoiceNumber'], 'string', 'max' => 50],
            [['invoiceBillEmail'], 'string', 'max' => 100],
            [['invoiceNumber'], 'unique'],
            [['invoicedCreatedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['invoicedCreatedby' => 'id']],
            [['localpurchaseOrder'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceCustomerPurchaseOrder::className(), 'targetAttribute' => ['localpurchaseOrder' => 'id']],
            [['invoiceProjectId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['invoiceProjectId' => 'id']],
            [['customerId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerDetails::className(), 'targetAttribute' => ['customerId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['approvedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedby' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoiceNumber' => 'Invoice Number',
            'customerId' => 'Customer',
            'localpurchaseOrder' => 'LPO',
            'invoiceProjectId' => 'Event',
            'invoiceTxnDate' => 'Date',
            'invoiceDeadlineDate' => 'Due Date',
            'invoiceNote' => 'Note To Operations',
            'invoiceFooter' => 'Footer',
            'invoiceTxnStatus' => 'Status',
            'invoiceEmailStatus' => 'Email to Client',
            'invoiceBillEmail' => 'Bill Email',
            'invoiceTaxAmount' => 'Total Tax Amount',
            'invoiceDiscountAmount' => 'Total Discount Amount',
            'invoiceSubAmount' => 'Sub Amount',
            'invoiceAmount' => 'Total Amount',
            'invoicedCreatedby' => 'Createdby',
            'invoicedCreatedAt' => 'Created At',
            'invoicedUpdatedAt' => 'Updated At',
            'invoicedDeletedAt' => 'Deleted At',
            'approvedby' => 'Approvedby',
            'userId' => 'User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemoLines()
    {
        return $this->hasMany(FinanceCreditMemoLines::className(), ['invoiceNumber' => 'invoiceNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoicedCreatedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'invoicedCreatedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalpurchaseOrder0()
    {
        return $this->hasOne(FinanceCustomerPurchaseOrder::className(), ['id' => 'localpurchaseOrder']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceProject()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'invoiceProjectId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerDetails::className(), ['id' => 'customerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoiceLines()
    {
        return $this->hasMany(FinanceInvoiceLines::className(), ['invoiceId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOffsetMemoLines()
    {
        return $this->hasMany(FinanceOffsetMemoLines::className(), ['invoiceNumber' => 'invoiceNumber']);
    }
    public function getCustomerDetails($id)
    {
      return CustomerDetails::find()
        ->where(['and', "id=$id"])->all();
        // ->indexBy('id')->column();
    }

    public function getInvnumber()
    {
        //select Invoice code

        $connection = Yii::$app->db;
        $query= "Select MAX(invoiceNumber) AS number from finance_invoice where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "INV-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "INV-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "INV-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
    public function allPayments($id){
      $inoicepayment= $this->find()->joinWith('financePayments')->where(['finance_payment.paymentLinkedTxn' => $id])
          ->Andwhere('finance_payment.paymentStatus >0')->sum('paymentTotalAmt');
      $offsetPayments=$this->find()->joinWith('financeOffsetMemoLines')->where(['finance_offset_memo_lines.invoiceNumber' => $id])
          ->Andwhere('finance_offset_memo_lines.offsetMemoLineStatus >0')->sum('offsetMemoLineAmount');

      $creditPayments=$this->find()->joinWith('financeCreditMemoLines')->where(['finance_credit_memo_lines.invoiceNumber' => $id])
          ->Andwhere('finance_credit_memo_lines.creditMemoLineStatus >0')->sum('creditMemoLineAmount');

      $totalpaymentmade=$inoicepayment+$offsetPayments+$creditPayments;

          return $totalpaymentmade;
    }
}
