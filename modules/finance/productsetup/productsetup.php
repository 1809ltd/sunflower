<?php

namespace app\modules\finance\productsetup;

/**
 * productsetup module definition class
 */
class productsetup extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\productsetup\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Service CategorySub Module*/
          'financeitemscategory' => [
              'class' => 'app\modules\finance\productsetup\financeitemscategory\financeitemscategory',
          ],
          /*CAtegiry type*/
          'financeitemscategorytype' => [
              'class' => 'app\modules\finance\productsetup\financeitemscategorytype\financeitemscategorytype',
          ],
          /*Product and service*/
          'financeitems' => [
              'class' => 'app\modules\finance\productsetup\financeitems\financeitems',
          ],
          /*Compnay Activyty*/
          'financeeventactivity' => [
              'class' => 'app\modules\finance\productsetup\financeeventactivity\financeeventactivity',
          ],
        ];

        // custom initialization code goes here
    }
}
