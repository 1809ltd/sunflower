<?php

namespace app\modules\finance\offset\outhouse;

/**
 * outhouse module definition class
 */
class outhouse extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\offset\outhouse\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance OutoffsetBills Sub Module*/
          'outoffsetbiils' => [
              'class' => 'app\modules\finance\offset\outhouse\outoffsetbiils\outoffsetbiils',
          ],
        ];

        // custom initialization code goes here
    }
}
