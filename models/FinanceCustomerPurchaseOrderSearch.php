<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceCustomerPurchaseOrder;

/**
 * FinanceCustomerPurchaseOrderSearch represents the model behind the search form of `app\models\FinanceCustomerPurchaseOrder`.
 */
class FinanceCustomerPurchaseOrderSearch extends FinanceCustomerPurchaseOrder
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customerId', 'purchaseOrderProjectId', 'purchaseOrderdCreatedby'], 'integer'],
            [['purchaseOrderNumber', 'purchaseOrderProjectClassification', 'purchaseOrderProjectName', 'purchaseOrderNote', 'purchaseOrderTxnDate', 'purchaseOrderBillEmail', 'purchaseOrderTxnStatus', 'purchaseOrderdCreatedAt', 'purchaseOrderdUpdatedAt', 'purchaseOrderdDeletedAt'], 'safe'],
            [['purchaseOrderDiscountAmount', 'purchaseOrderTaxAmount', 'purchaseOrderSubAmount', 'purchaseOrderAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceCustomerPurchaseOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customerId' => $this->customerId,
            'purchaseOrderProjectId' => $this->purchaseOrderProjectId,
            'purchaseOrderTxnDate' => $this->purchaseOrderTxnDate,
            'purchaseOrderDiscountAmount' => $this->purchaseOrderDiscountAmount,
            'purchaseOrderTaxAmount' => $this->purchaseOrderTaxAmount,
            'purchaseOrderSubAmount' => $this->purchaseOrderSubAmount,
            'purchaseOrderAmount' => $this->purchaseOrderAmount,
            'purchaseOrderdCreatedby' => $this->purchaseOrderdCreatedby,
            'purchaseOrderdCreatedAt' => $this->purchaseOrderdCreatedAt,
            'purchaseOrderdUpdatedAt' => $this->purchaseOrderdUpdatedAt,
            'purchaseOrderdDeletedAt' => $this->purchaseOrderdDeletedAt,
        ]);

        $query->andFilterWhere(['like', 'purchaseOrderNumber', $this->purchaseOrderNumber])
            ->andFilterWhere(['like', 'purchaseOrderProjectClassification', $this->purchaseOrderProjectClassification])
            ->andFilterWhere(['like', 'purchaseOrderProjectName', $this->purchaseOrderProjectName])
            ->andFilterWhere(['like', 'purchaseOrderNote', $this->purchaseOrderNote])
            ->andFilterWhere(['like', 'purchaseOrderBillEmail', $this->purchaseOrderBillEmail])
            ->andFilterWhere(['like', 'purchaseOrderTxnStatus', $this->purchaseOrderTxnStatus]);

        return $dataProvider;
    }
}
