<?php

namespace app\modules\customer\customerdetails\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\customer\customerdetails\models\CustomerDetails;

/**
 * CustomerDetailsSearch represents the model behind the search form of `app\modules\customer\customerdetails\models\CustomerDetails`.
 */
class CustomerDetailsSearch extends CustomerDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customerstatus', 'createdBy', 'userId'], 'integer'],
            [['customerNumber', 'customerFullname', 'customerCompanyName', 'customerKraPin', 'customerDisplayName', 'customerPrimaryPhone', 'customerPrimaryEmail', 'customercity', 'customerPhyscialAdress', 'customerCounty', 'customerCountry', 'customerAddress', 'customerCreateTime', 'customerUpdatedAt', 'customerDeleteAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customerstatus' => $this->customerstatus,
            'customerCreateTime' => $this->customerCreateTime,
            'customerUpdatedAt' => $this->customerUpdatedAt,
            'customerDeleteAt' => $this->customerDeleteAt,
            'createdBy' => $this->createdBy,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'customerNumber', $this->customerNumber])
            ->andFilterWhere(['like', 'customerFullname', $this->customerFullname])
            ->andFilterWhere(['like', 'customerCompanyName', $this->customerCompanyName])
            ->andFilterWhere(['like', 'customerKraPin', $this->customerKraPin])
            ->andFilterWhere(['like', 'customerDisplayName', $this->customerDisplayName])
            ->andFilterWhere(['like', 'customerPrimaryPhone', $this->customerPrimaryPhone])
            ->andFilterWhere(['like', 'customerPrimaryEmail', $this->customerPrimaryEmail])
            ->andFilterWhere(['like', 'customercity', $this->customercity])
            ->andFilterWhere(['like', 'customerPhyscialAdress', $this->customerPhyscialAdress])
            ->andFilterWhere(['like', 'customerCounty', $this->customerCounty])
            ->andFilterWhere(['like', 'customerCountry', $this->customerCountry])
            ->andFilterWhere(['like', 'customerAddress', $this->customerAddress]);

        return $dataProvider;
    }
}
