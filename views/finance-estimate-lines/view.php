<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimateLines */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Estimate Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-estimate-lines-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'estimateId',
            'itemRefId',
            'itemRefName',
            'estimateLinesDescription:ntext',
            'estimateLinespack',
            'estimateLinesQty',
            'estimateLinesUnitPrice',
            'estimateLinesAmount',
            'estimateLinesTaxCodeRef',
            'estimateLinesTaxamount',
            'estimateLinesDiscount',
            'estimateLinesCreatedAt',
            'estimateLinesUpdatedAt',
            'estimateLinesDeletedAt',
            'estimateLinesStatus',
            'userId',
        ],
    ]) ?>

</div>
