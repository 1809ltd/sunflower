<?php

namespace app\modules\vendor\vendordetails\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;

/**
 * VendorCompanyDetailsSearch represents the model behind the search form of `app\modules\vendor\vendordetails\models\VendorCompanyDetails`.
 */
class VendorCompanyDetailsSearch extends VendorCompanyDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vendorCompanyStatus', 'companyUserId'], 'integer'],
            [['vendorCompanyName', 'vendorKraPin', 'vendorContactPersonFullName', 'contactMobilePhone', 'vendorPhone', 'contactEmail', 'vendoremail', 'vendorCompanyAddress', 'vendorCompanySuite', 'vendorCompanyCity', 'vendorCompanyCounty', 'vendorCompanyPostal', 'vendorCompanyCountry', 'vendorCompanyLogo', 'vendorTwitter', 'vendorFb', 'vendorIg', 'vendorType', 'vendorCompanyTimestamp', 'vendorCompanyupdatedAt', 'vendorCompanyDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VendorCompanyDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vendorCompanyStatus' => $this->vendorCompanyStatus,
            'vendorCompanyTimestamp' => $this->vendorCompanyTimestamp,
            'vendorCompanyupdatedAt' => $this->vendorCompanyupdatedAt,
            'vendorCompanyDeletedAt' => $this->vendorCompanyDeletedAt,
            'companyUserId' => $this->companyUserId,
        ]);

        $query->andFilterWhere(['like', 'vendorCompanyName', $this->vendorCompanyName])
            ->andFilterWhere(['like', 'vendorKraPin', $this->vendorKraPin])
            ->andFilterWhere(['like', 'vendorContactPersonFullName', $this->vendorContactPersonFullName])
            ->andFilterWhere(['like', 'contactMobilePhone', $this->contactMobilePhone])
            ->andFilterWhere(['like', 'vendorPhone', $this->vendorPhone])
            ->andFilterWhere(['like', 'contactEmail', $this->contactEmail])
            ->andFilterWhere(['like', 'vendoremail', $this->vendoremail])
            ->andFilterWhere(['like', 'vendorCompanyAddress', $this->vendorCompanyAddress])
            ->andFilterWhere(['like', 'vendorCompanySuite', $this->vendorCompanySuite])
            ->andFilterWhere(['like', 'vendorCompanyCity', $this->vendorCompanyCity])
            ->andFilterWhere(['like', 'vendorCompanyCounty', $this->vendorCompanyCounty])
            ->andFilterWhere(['like', 'vendorCompanyPostal', $this->vendorCompanyPostal])
            ->andFilterWhere(['like', 'vendorCompanyCountry', $this->vendorCompanyCountry])
            ->andFilterWhere(['like', 'vendorCompanyLogo', $this->vendorCompanyLogo])
            ->andFilterWhere(['like', 'vendorTwitter', $this->vendorTwitter])
            ->andFilterWhere(['like', 'vendorFb', $this->vendorFb])
            ->andFilterWhere(['like', 'vendorIg', $this->vendorIg])
            ->andFilterWhere(['like', 'vendorType', $this->vendorType]);

        return $dataProvider;
    }
}
