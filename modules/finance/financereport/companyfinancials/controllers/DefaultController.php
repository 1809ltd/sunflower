<?php

namespace app\modules\finance\financereport\companyfinancials\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*All transactions */
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
/*All Aged Receivables */
use app\modules\finance\financereport\companyfinancials\models\AgedReceivables;

/*All the Events*/
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\event\eventInfo\models\EventDetailsSearch;

/*Active Record*/
use yii\data\ActiveDataProvider;


/**
 * Default controller for the `companyfinancials` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $this->layout = '@app/views/layouts/addformdatatablelayout';

        return $this->render('index');
    }
    /*Income Statement*/
    public function actionIncome()
    {

        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new AllTransactions();

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          \Yii::$app->session->set('startDate',$model->startDate);
          \Yii::$app->session->set('endDate',$model->endDate);

          return $this->redirect(['income']);

        } else {
          // code...

          //load the Income Statement Search form
            return $this->render('incomestst', [
                // 'searchModel' => $searchModel,
                // 'dataProvider' => $dataProvider,
                'model'=>$model,
            ]);

        }
    }
    /*All transaction*/
    public function actionAlltransacation($accountId)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = new AllTransactions();

      //checking if its a post
      if ($model->load(Yii::$app->request->post())) {
        // code...

        \Yii::$app->session->set('startDate',$model->startDate);
        \Yii::$app->session->set('endDate',$model->endDate);
        \Yii::$app->session->set('transactionaccountId',$model->accountId);

        return $this->redirect(['search']);

      } else {
        // code...

        //load the Income Statement Search form
        \Yii::$app->session->set('transactionaccountId',$accountId);
          return $this->render('search', [
                'model'=>$model,
          ]);

      }
    }
    /*Aged Recievables */
    public function actionAgedreciveables()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';

        return $this->render('aged_recivables');
    }
    /*Aged Payables*/
    public function actionAgedpayables()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('aged_payables');
    }
    public function actionBalancesheet()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new AllTransactions();
        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...
          \Yii::$app->session->set('mwishoDate',$model->mwishoDate);
          return $this->redirect(['balancesheet']);

        } else {
          // code...

          //load the Income Statement Search form
            return $this->render('balance_sheet', [
                // 'searchModel' => $searchModel,
                // 'dataProvider' => $dataProvider,
                'model'=>$model,
            ]);

        }
        // return $this->render('balance_sheet');
    }
    /*Income By Customer*/
    public function actionIncomebycustomer()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('incomebycustomer');
    }
    /*Vendor Bills*/
    public function actionBillsbyvendor()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('billsbyvendor');
    }
    /*All the Events*/
    public function actionEvent(){

      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $events= EventDetails::find()->all();

      /*Getting all Event With invoices and Expenses*/
      $queryEventWithActivty = EventDetails::find();
      $dataProviderEventWithActivty = new ActiveDataProvider([
          'query' => $queryEventWithActivty,
          'pagination'=> ['defaultPageSize' => 50],
      ]);
      // $eventStatus="1";
      // $queryEventWithActivty->andFilterWhere(['status' => $eventStatus]);
      // $queryEventWithActivty->andFilterWhere(['status' => $eventStatus]);

      $searchModel = new EventDetailsSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('eventlist', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProviderEventWithActivty
      ]);
    }
    public function actionView($id)
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';

        return $this->render('view', [
            'eventId' => $id,
        ]);
    }



}
