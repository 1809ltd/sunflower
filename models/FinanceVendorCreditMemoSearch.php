<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceVendorCreditMemo;

/**
 * FinanceVendorCreditMemoSearch represents the model behind the search form of `app\models\FinanceVendorCreditMemo`.
 */
class FinanceVendorCreditMemoSearch extends FinanceVendorCreditMemo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vendorCreditMemoAccount', 'vendorId', 'vendorCreditMemoStatus', 'userId'], 'integer'],
            [['refernumber', 'vendorCreditMemoTxnDate', 'vendorCreditMemoNote', 'vendorCreditMemoCreatedAt', 'vendorCreditMemoUpdatedAt', 'vendorCreditMemoDeletedAt'], 'safe'],
            [['creditAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceVendorCreditMemo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vendorCreditMemoAccount' => $this->vendorCreditMemoAccount,
            'vendorCreditMemoTxnDate' => $this->vendorCreditMemoTxnDate,
            'vendorId' => $this->vendorId,
            'creditAmount' => $this->creditAmount,
            'vendorCreditMemoStatus' => $this->vendorCreditMemoStatus,
            'vendorCreditMemoCreatedAt' => $this->vendorCreditMemoCreatedAt,
            'vendorCreditMemoUpdatedAt' => $this->vendorCreditMemoUpdatedAt,
            'vendorCreditMemoDeletedAt' => $this->vendorCreditMemoDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'refernumber', $this->refernumber])
            ->andFilterWhere(['like', 'vendorCreditMemoNote', $this->vendorCreditMemoNote]);

        return $dataProvider;
    }
}
