-- All invoice with respective Payments
-- Aged Receivables-- ----------------------------
-- View structure for `aged_receivables`
-- ----------------------------
DROP VIEW IF EXISTS `aged_receivables`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `aged_receivables` AS
SELECT
`finance_invoice`.`customerId` AS `customerId`,
`customer_details`.`customerCompanyName` AS `receivables`,
(
  CASE
  WHEN (
    sum(
      IF(
         ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) = 0 ),`finance_invoice_lines`.`invoiceLinesAmount`,0
				)
      ) = 0
    )
    THEN
    0
    WHEN (
      sum(
          IF(
              ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) = 0 ),
  							`finance_invoice_lines`.`invoiceLinesAmount`, 0
            )
          ) > 0
      ) -- if the value is greater than 0 means there is data
			THEN
			(
        --  adding Invoices Sub Amount
        sum(
								IF
                 (
                   ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) = 0 ),
    								`finance_invoice_lines`.`invoiceLinesAmount`, 0
								 )
							)
        +
        -- adding the tax of the Invoices
        sum(
								IF
                 (
                   ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) = 0 ),
    								`finance_invoice_lines`.`invoiceLinesTaxamount`, 0
								 )
							)
        -
        -- Reduce payment made via normal payments
            ( SELECT COALESCE ( sum( `finance_payment_lines`.`amount` ), 0 )
                FROM
                `finance_payment_lines`
                WHERE
                 `finance_payment_lines`.`invoiceId` = `finance_invoice`.`id`
							)
         -
         -- Reduce payments made via credit Memo
						 ( SELECT COALESCE ( sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 )
								FROM
                `finance_credit_memo_lines`
                WHERE
                 `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
						 )
        -
        -- Reduce payments made from offest accounts
            ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 )
                FROM
                `finance_offset_memo_lines`
                WHERE
                 `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
             )
			)
		END
	) AS `Coming Due`,
  (
    CASE
    WHEN (
      sum(
        IF(
           ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 1 AND 30 ),
           `finance_invoice_lines`.`invoiceLinesAmount`,0
  				)
        ) = 0
      )
      THEN
      0
      WHEN (
        sum(
            IF(
                ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 1 AND 30 ),
    							`finance_invoice_lines`.`invoiceLinesAmount`, 0
              )
            ) > 0
        ) -- if the value is greater than 0 means there is data
  			THEN
  			(
          --  adding Invoices Sub Amount
          sum(
  								IF
                   (
                     ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 1 AND 30 ),
      								`finance_invoice_lines`.`invoiceLinesAmount`, 0
  								 )
  							)
          +
          -- adding the tax of the Invoices
          sum(
  								IF
                   (
                     ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 1 AND 30 ),
      								`finance_invoice_lines`.`invoiceLinesTaxamount`, 0
  								 )
  							)
          -
          -- Reduce payment made via normal payments
              ( SELECT COALESCE ( sum( `finance_payment_lines`.`amount` ), 0 )
                  FROM
                  `finance_payment_lines`
                  WHERE
                   `finance_payment_lines`.`invoiceId` = `finance_invoice`.`id`
  							)
           -
           -- Reduce payments made via credit Memo
  						 ( SELECT COALESCE ( sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 )
  								FROM
                  `finance_credit_memo_lines`
                  WHERE
                   `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
  						 )
          -
          -- Reduce payments made from offest accounts
              ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 )
                  FROM
                  `finance_offset_memo_lines`
                  WHERE
                   `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
               )
  			)
  		END
  	) AS `1-30 Days`,
    (
      CASE
      WHEN (
        sum(
          IF(
             ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 31 AND 60 ),
             `finance_invoice_lines`.`invoiceLinesAmount`,0
    				)
          ) = 0
        )
        THEN
        0
        WHEN (
          sum(
              IF(
                  ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 31 AND 60 ),
      							`finance_invoice_lines`.`invoiceLinesAmount`, 0
                )
              ) > 0
          ) -- if the value is greater than 0 means there is data
    			THEN
    			(
            --  adding Invoices Sub Amount
            sum(
    								IF
                     (
                       ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 31 AND 60 ),
        								`finance_invoice_lines`.`invoiceLinesAmount`, 0
    								 )
    							)
            +
            -- adding the tax of the Invoices
            sum(
    								IF
                     (
                       ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 31 AND 60 ),
        								`finance_invoice_lines`.`invoiceLinesTaxamount`, 0
    								 )
    							)
            -
            -- Reduce payment made via normal payments
                ( SELECT COALESCE ( sum( `finance_payment_lines`.`amount` ), 0 )
                    FROM
                    `finance_payment_lines`
                    WHERE
                     `finance_payment_lines`.`invoiceId` = `finance_invoice`.`id`
    							)
             -
             -- Reduce payments made via credit Memo
    						 ( SELECT COALESCE ( sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 )
    								FROM
                    `finance_credit_memo_lines`
                    WHERE
                     `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
    						 )
            -
            -- Reduce payments made from offest accounts
                ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 )
                    FROM
                    `finance_offset_memo_lines`
                    WHERE
                     `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
                 )
    			)
    		END
    	) AS `31-60 Days`,
      (
        CASE
        WHEN (
          sum(
            IF(
               ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 61 AND 90 ),
               `finance_invoice_lines`.`invoiceLinesAmount`,0
      				)
            ) = 0
          )
          THEN
          0
          WHEN (
            sum(
                IF(
                    ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 61 AND 90 ),
        							`finance_invoice_lines`.`invoiceLinesAmount`, 0
                  )
                ) > 0
            ) -- if the value is greater than 0 means there is data
      			THEN
      			(
              --  adding Invoices Sub Amount
              sum(
      								IF
                       (
                         ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 61
                         AND 90 ),
          								`finance_invoice_lines`.`invoiceLinesAmount`, 0
      								 )
      							)
              +
              -- adding the tax of the Invoices
              sum(
      								IF
                       (
                         ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) BETWEEN 61 AND 90),
          								`finance_invoice_lines`.`invoiceLinesTaxamount`, 0
      								 )
      							)
              -
              -- Reduce payment made via normal payments
                  ( SELECT COALESCE ( sum( `finance_payment_lines`.`amount` ), 0 )
                      FROM
                      `finance_payment_lines`
                      WHERE
                       `finance_payment_lines`.`invoiceId` = `finance_invoice`.`id`
      							)
               -
               -- Reduce payments made via credit Memo
      						 ( SELECT COALESCE ( sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 )
      								FROM
                      `finance_credit_memo_lines`
                      WHERE
                       `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
      						 )
              -
              -- Reduce payments made from offest accounts
                  ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 )
                      FROM
                      `finance_offset_memo_lines`
                      WHERE
                       `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
                   )
      			)
      		END
      	) AS `61-90 Days`,
        (
          CASE
          WHEN (
            sum(
              IF(
                 ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) >90 ),`finance_invoice_lines`.`invoiceLinesAmount`,0
        				)
              ) = 0
            )
            THEN
            0
            WHEN (
              sum(
                  IF(
                      ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) >90 ),
          							`finance_invoice_lines`.`invoiceLinesAmount`, 0
                    )
                  ) > 0
              ) -- if the value is greater than 0 means there is data
        			THEN
        			(
                --  adding Invoices Sub Amount
                sum(
        								IF
                         (
                           ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) >90 ),
            								`finance_invoice_lines`.`invoiceLinesAmount`, 0
        								 )
        							)
                +
                -- adding the tax of the Invoices
                sum(
        								IF
                         (
                           ( ( to_days( curdate( ) ) - to_days( cast( `finance_invoice`.`invoiceTxnDate` AS date ) ) ) >90 ),
            								`finance_invoice_lines`.`invoiceLinesTaxamount`, 0
        								 )
        							)
                -
                -- Reduce payment made via normal payments
                    ( SELECT COALESCE ( sum( `finance_payment_lines`.`amount` ), 0 )
                        FROM
                        `finance_payment_lines`
                        WHERE
                         `finance_payment_lines`.`invoiceId` = `finance_invoice`.`id`
        							)
                 -
                 -- Reduce payments made via credit Memo
        						 ( SELECT COALESCE ( sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 )
        								FROM
                        `finance_credit_memo_lines`
                        WHERE
                         `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
        						 )
                -
                -- Reduce payments made from offest accounts
                    ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 )
                        FROM
                        `finance_offset_memo_lines`
                        WHERE
                         `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber`
                     )
        			)
        		END
        	) AS `>90 Days`,
          (
            SELECT COALESCE ( Sum(finance_payment.unallocatedAmount), 0 )
            FROM finance_payment
            WHERE
             finance_payment.customerId = `finance_invoice`.`customerId`
          ) AS `unlocated`
  FROM
  `finance_invoice`
    LEFT JOIN `finance_invoice_lines` ON `finance_invoice_lines`.`invoiceId` = `finance_invoice`.`id`
    LEFT JOIN `customer_details` ON `customer_details`.`id` = `finance_invoice`.`customerId`
    WHERE
    	finance_invoice.invoiceTxnStatus > 0
    	AND finance_invoice_lines.invoiceLinesStatus > 0
  GROUP BY  `finance_invoice`.`customerId`
ORDER BY `receivables` ASC
