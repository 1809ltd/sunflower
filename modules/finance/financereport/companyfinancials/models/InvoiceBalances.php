<?php

namespace app\modules\finance\financereport\companyfinancials\models;

use Yii;

/**
 * This is the model class for table "invoice_balances".
 *
 * @property int $transactionId
 * @property int $referenceId
 * @property string $referenceCode
 * @property string $recipientId
 * @property string $recipientName
 * @property double $Sum( all_transactions.dr_amount )
 * @property double $Sum( all_transactions.cr_amount )
 * @property double $owed
 * @property string $invoiceTxnDate
 */
class InvoiceBalances extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice_balances';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transactionId', 'referenceId'], 'integer'],
            [['Sum( all_transactions.dr_amount )', 'Sum( all_transactions.cr_amount )', 'owed'], 'number'],
            [['invoiceTxnDate'], 'safe'],
            [['referenceCode'], 'string', 'max' => 100],
            [['recipientId'], 'string', 'max' => 11],
            [['recipientName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'transactionId' => 'Transaction ID',
            'referenceId' => 'Reference ID',
            'referenceCode' => 'Reference Code',
            'recipientId' => 'Recipient ID',
            'recipientName' => 'Recipient Name',
            'Sum( all_transactions.dr_amount )' => 'Sum( All Transactions Dr Amount )',
            'Sum( all_transactions.cr_amount )' => 'Sum( All Transactions Cr Amount )',
            'owed' => 'Owed',
            'invoiceTxnDate' => 'Invoice Txn Date',
        ];
    }
}
