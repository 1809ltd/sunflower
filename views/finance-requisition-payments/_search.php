<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-requisition-payments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reqpaymentPayType') ?>

    <?= $form->field($model, 'reqpaymentaccount') ?>

    <?= $form->field($model, 'reqrecepitNumber') ?>

    <?= $form->field($model, 'reqpaymentTotalAmt') ?>

    <?php // echo $form->field($model, 'unallocatedAmount') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'reqpaymenteventReqRef') ?>

    <?php // echo $form->field($model, 'reqpaymentPrivateNote') ?>

    <?php // echo $form->field($model, 'reqpaymentCreatedAt') ?>

    <?php // echo $form->field($model, 'reqpaymentUpdatedAt') ?>

    <?php // echo $form->field($model, 'reqpaymentDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <?php // echo $form->field($model, 'approvedBy') ?>

    <?php // echo $form->field($model, 'paymentstatus') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
