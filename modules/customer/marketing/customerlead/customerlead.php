<?php

namespace app\modules\customer\marketing\customerlead;

/**
 * customerlead module definition class
 */
class customerlead extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\customer\marketing\customerlead\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
