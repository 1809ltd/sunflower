<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-header">
  <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button> -->
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body search-form">


  <?= $form->field($model, 'nameCustomer')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(CustomerDetails::find()->all(),'id','customerDisplayName'),
      'language' => 'en',
      'options' => ['placeholder' => 'Select a Customer ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);
  ?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton('Search for Customer', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
