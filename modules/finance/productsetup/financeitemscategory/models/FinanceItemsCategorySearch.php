<?php

namespace app\modules\finance\productsetup\financeitemscategory\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory;

/**
 * FinanceItemsCategorySearch represents the model behind the search form of `app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory`.
 */
class FinanceItemsCategorySearch extends FinanceItemsCategory
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'financeItemCategoryParentId', 'financeItemCategoryStatus', 'userId'], 'integer'],
            [['financeItemCategoryName', 'financeItemCategoryDescription', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceItemsCategory::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'financeItemCategoryParentId' => $this->financeItemCategoryParentId,
            'financeItemCategoryStatus' => $this->financeItemCategoryStatus,
            'userId' => $this->userId,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'financeItemCategoryName', $this->financeItemCategoryName])
            ->andFilterWhere(['like', 'financeItemCategoryDescription', $this->financeItemCategoryDescription]);

        return $dataProvider;
    }
}
