<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;

use app\models\CompanyDetails;
use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory;
use app\modules\finance\productsetup\financeitemscategorytype\models\FinanceItemsCategoryType;
// use app\models\AssetRegistration;
use kartik\select2\Select2;

use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItems */
/* @var $form yii\widgets\ActiveForm */

?>
<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="finance-items-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'itemsName')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                  <?= $form->field($model, 'itemsTaxable')->checkbox() ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'itemsDescription')->textarea(['rows' => 6]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'itemCategory')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(FinanceItemsCategory::find()->where('`financeItemCategoryStatus` = 1')->asArray()->all(),'id','financeItemCategoryName'),
                        'language' => 'en',
                        'options' => ['id'=> 'itemCategory-id', 'placeholder' => 'Select a Category ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>

                    <?php
                    // $itemCategory=ArrayHelper::map(FinanceItemsCategory::find()->where('`financeItemCategoryStatus` = 1')->asArray()->all(), 'id', 'financeItemCategoryName');?>
                    <?php
                    //  $form->field($model, 'itemCategory')->dropDownList($itemCategory, [
                    //   'prompt'=>'Select...',
                    //   'onChange'=>'$.post();'
                    //   // Url::to((['/finance/productsetup/financeitemscategorytype/finance-items-category-type/lists','id'=>$key]))
                    //   // 'onChange'=>'$.post("index.php?r=finance-items-category-type/lists&id='.'"+$(this).val(),function(data){
                    //   //   $("select#financeitems-itemcategorytype").html(data);
                    //   // });'
                    // ]); ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'itemCategoryType')->widget(DepDrop::classname(), [
                        'options'=>['id'=>'itemCategoryType-id'],
                        'data' => [$model->itemCategoryType => $model->itemCategoryType],
                        'type' => DepDrop::TYPE_SELECT2,
                        'pluginOptions'=>[
                            'depends'=>['itemCategory-id'],
                            'initialize' => true,
                            // 'initDepends'=>['orderId'],
                            'placeholder'=>'Select sub Category Type...',
                            'url'=>Url::to(['/finance/productsetup/financeitemscategorytype/finance-items-category-type/lists'])
                        ]
                    ]);?>


                    <?php
                     // $itemCategoryType = ArrayHelper::map(FinanceItemsCategoryType::find()->all(),'id','financeItemCategoryTypeName'); ?>
                    <?php
                     // $form->field($model, 'itemCategoryType')->dropDownList($itemCategoryType, ['prompt'=>'Select...']); ?>

                  </div>
                </div>
                <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($model, 'itemsIncomeAccountRef')->widget(Select2::classname(), [
                            'data' =>ArrayHelper::map(FinanceAccounts::find()
                                ->where(['accountsClassification' => "Revenue"])
                                ->Andwhere(['accountsStatus'=>1])
                                // ->asArray()
                                ->all(),'id','fullyQualifiedName'),
                            // ArrayHelper::map(FinanceAccounts::find()->all(),'id','fullyQualifiedName'),
                            'language' => 'en',
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                      </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                  <?= $form->field($model, 'itemsCustomerUnitPrice')->textInput() ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'itemsSupplierUnitPrice')->textInput() ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $itemsType = ['Inventory' => 'Inventory','Service' => 'Service','NonInventory' => 'NonInventory']; ?>
                    <?= $form->field($model, 'itemsClassification')->dropDownList($itemsType, ['prompt'=>'Select Classification ']);  ?>
                  </div>
                </div>
                <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($model, 'itemsExpenseAccountRef')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(FinanceAccounts::find()
                                ->where(['accountsClassification' => "Expenses"])
                                ->Andwhere(['accountsStatus'=>1])
                                // ->asArray()
                                ->all(),'id','fullyQualifiedName'),
                                // ArrayHelper::map(FinanceAccounts::find()->all(),'id','fullyQualifiedName'),
                            'language' => 'en',
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                      </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'itemsPurchaseDesc')->textarea(['rows' => 6]) ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'itemsPurchaseCost')->textInput() ?>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'itemsStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                  </div>
                </div>


                <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>

                <div class="col-sm-3">
                  <div class="form-group">
                      <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                  </div>
                </div>

                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
