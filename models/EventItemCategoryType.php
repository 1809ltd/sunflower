<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_item_category_type".
 *
 * @property int $id
 * @property int $itemId
 * @property int $categorytypeId
 * @property double $qty
 * @property int $status
 * @property int $userid
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property FinanceItems $item
 * @property AssetCategoryType $categorytype
 */
class EventItemCategoryType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_item_category_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemId', 'categorytypeId', 'qty', 'status', 'userid'], 'required'],
            [['itemId', 'categorytypeId', 'status', 'userid'], 'integer'],
            [['qty'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['itemId', 'categorytypeId'], 'unique', 'targetAttribute' => ['itemId', 'categorytypeId']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['categorytypeId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetCategoryType::className(), 'targetAttribute' => ['categorytypeId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'itemId' => 'Service',
            'categorytypeId' => 'Category Type',
            'qty' => 'Qty',
            'status' => 'Status',
            'userid' => 'Userid',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorytype()
    {
        return $this->hasOne(AssetCategoryType::className(), ['id' => 'categorytypeId']);
    }
}
