<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimateLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-estimate-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'estimateId') ?>

    <?= $form->field($model, 'itemRefId') ?>

    <?= $form->field($model, 'itemRefName') ?>

    <?= $form->field($model, 'estimateLinesDescription') ?>

    <?php // echo $form->field($model, 'estimateLinespack') ?>

    <?php // echo $form->field($model, 'estimateLinesQty') ?>

    <?php // echo $form->field($model, 'estimateLinesUnitPrice') ?>

    <?php // echo $form->field($model, 'estimateLinesAmount') ?>

    <?php // echo $form->field($model, 'estimateLinesTaxCodeRef') ?>

    <?php // echo $form->field($model, 'estimateLinesTaxamount') ?>

    <?php // echo $form->field($model, 'estimateLinesDiscount') ?>

    <?php // echo $form->field($model, 'estimateLinesCreatedAt') ?>

    <?php // echo $form->field($model, 'estimateLinesUpdatedAt') ?>

    <?php // echo $form->field($model, 'estimateLinesDeletedAt') ?>

    <?php // echo $form->field($model, 'estimateLinesStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
