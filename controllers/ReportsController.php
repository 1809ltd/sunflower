<?php


namespace app\controllers;

use Yii;
use app\models\EventDetails;
use app\models\EventDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class ReportsController extends \yii\web\Controller
{
    public function actionIndex()
    {
      $this->layout='addformdatatablelayout';
        return $this->render('index');
    }

    public function actionAgedreciveables()
    {
      $this->layout='addformdatatablelayout';
        return $this->render('aged_receivables');
    }
    public function actionAgedpayables()
    {
      $this->layout='addformdatatablelayout';
        return $this->render('aged_payables');
    }
    public function actionIncome()
    {
      $this->layout='addformdatatablelayout';
        return $this->render('incomestst');
    }
    public function actionBalancesheet()
    {
      $this->layout='addformdatatablelayout';
        return $this->render('balance_sheet');
    }public function actionIncomebycustomer()
    {
      $this->layout='addformdatatablelayout';
        return $this->render('income_by_customer');
    }
    public function actionBillsbyvendor()
    {
      $this->layout='addformdatatablelayout';
        return $this->render('bills_by_vendor');
    }
    public function actionEvent(){
      $this->layout='addformdatatablelayout';
      $events= EventDetails::find()->all();

      $searchModel = new EventDetailsSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('eventlist', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider
      ]);
    }
    public function actionView($id)
    {
      $this->layout='addformdatatablelayout';

        return $this->render('view', [
            'eventId' => $id,
        ]);
    }

}
