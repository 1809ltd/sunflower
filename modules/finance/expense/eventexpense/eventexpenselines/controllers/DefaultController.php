<?php

namespace app\modules\finance\expense\eventexpense\eventexpenselines\controllers;

use yii\web\Controller;

/**
 * Default controller for the `eventexpenselines` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
