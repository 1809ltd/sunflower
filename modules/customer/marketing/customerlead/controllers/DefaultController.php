<?php

namespace app\modules\customer\marketing\customerlead\controllers;

use yii\web\Controller;

/**
 * Default controller for the `customerlead` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
