<?php

namespace app\modules\event\eventInfo\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\event\eventInfo\models\EventDetails;

/**
 * EventDetailsSearch represents the model behind the search form of `app\modules\event\eventInfo\models\EventDetails`.
 */
class EventDetailsSearch extends EventDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'eventCategory', 'userId'], 'integer'],
            [[ 'customerId','eventNumber', 'eventName', 'eventTheme', 'eventStartDate', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventLocation', 'eventDescription', 'eventNotes', 'eventCreatedAt', 'eventLastUpdatedAt', 'eventDeletedAt'], 'safe'],
            [['eventVistorsNumber'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventDetails::find();
        $query->joinWith(['customer']);
        $query->joinWith(['eventCategory0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'customerId' => $this->customerId,
            'eventVistorsNumber' => $this->eventVistorsNumber,
            // 'eventCategory' => $this->eventCategory,
            'eventStartDate' => $this->eventStartDate,
            'eventEndDate' => $this->eventEndDate,
            'eventSetUpDateTime' => $this->eventSetUpDateTime,
            'eventSetDownDateTime' => $this->eventSetDownDateTime,
            'eventCreatedAt' => $this->eventCreatedAt,
            'eventLastUpdatedAt' => $this->eventLastUpdatedAt,
            'eventDeletedAt' => $this->eventDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'eventNumber', $this->eventNumber])
            ->andFilterWhere(['like', 'eventName', $this->eventName])
            ->andFilterWhere(['like', 'eventTheme', $this->eventTheme])
            ->andFilterWhere(['like', 'eventLocation', $this->eventLocation])
            ->andFilterWhere(['like', 'eventDescription', $this->eventDescription])
            ->andFilterWhere(['like', 'customerDisplayName', $this->customerId])
            ->andFilterWhere(['like', 'eventCatName', $this->eventCategory])
            ->andFilterWhere(['like', 'eventNotes', $this->eventNotes]);

        return $dataProvider;
    }
}
