<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\modules\finance\financereport\companyfinancials\models\AgePayables;



/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

?>
<section class="invoice">
  <div class="text-center">
    <h6 class="box-title">
      <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
    </h6>
    <h3 class="box-title">Customer</h3>
    <h5 class="box-title">As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h5>
    <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
  </div>

  <div class="box">
    <div class="box-body">
      <h5 class="box-title">REVENUE</h5>
      <table class="table  table-striped table-condensed">
        <thead>
          <tr>
            <th class="text-left">Customer</th>
            <th class="text-right">Income</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $connection = Yii::$app->getDb();
          $command1 = $connection->createCommand("SELECT
                                                  	customerincome.customerId,
                                                  	customerincome.customerNumber,
                                                  	customerincome.customerKraPin,
                                                  	customerincome.customerDisplayName,
                                                  	Sum( customerincome.invoiceLinesAmount ),
                                                  	customerincome.invoiceLinesTaxamount,
                                                  	customerincome.invoiceLinesTaxCodeRef,
                                                  	customerincome.itemRefId,
                                                  	customerincome.itemRefName,
                                                  	customerincome.invoiceId,
                                                  	customerincome.invoicelineId,
                                                  	customerincome.invoiceLinesDescription,
                                                  	customerincome.invoiceLinesQty,
                                                  	customerincome.invoiceLinesUnitPrice,
                                                  	customerincome.invoiceLinesCreatedAt,
                                                  	customerincome.invoiceLinesDiscount,
                                                  	customerincome.txnDate,
                                                  	customerincome.invoiceLinesStatus,
                                                  	customerincome.eventNumber,
                                                  	customerincome.eventCatName,
                                                  	customerincome.catId
                                                  FROM
                                                  	customerincome
                                                  GROUP BY
                                                  	customerincome.customerId");

          $customerincomes = $command1->query();

          foreach ($customerincomes as $customerincomes) {
            // code...

            // $customerincomes->customerId;
            //
            // print_r($customerincomes);
            //
            // die();


            ?>
            <tr>

              <td class="text-left"><?= $customerincomes["customerDisplayName"];?></td>
              <td class="text-right"><?= number_format($customerincomes["Sum( customerincome.invoiceLinesAmount )"],2);?></td>

            </tr>


            <?php


          }



           ?>

           <tr>

               <td></td>
               <td></td>
           </tr>
           <tr>

               <!-- <th class="text-left">Total Receivable</th>

               <th class="text-right"><?php
                 // number_format($SumTT,2);?></th> -->

           </tr>

         </tbody>
     </table>
   </br>

  </div>
</div>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <div class="col-xs-12">
    <a href="javascript:void(0)" onclick="window.print()"
       class="btn btn-xs btn-success m-b-10"><i
            class="fa fa-print m-r-5"></i> Print</a>

      <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
      <i class="fa fa-download"></i> Generate PDF -->
    <!-- </button> -->
  </div>
</div>
</section>
