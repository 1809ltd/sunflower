<?php

namespace app\modules\finance\expensepayment\eventexpensepayment\models;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
// Event Requstion been paid for
use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionForm;
// Getting relation tables and Model
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLinesSearch;

use Yii;

/**
 * This is the model class for table "finance_requisition_payments".
 *
 * @property int $id
 * @property string $reqpaymentPayType
 * @property int $reqpaymentaccount Account Paid from
 * @property string $reqrecepitNumber
 * @property double $reqpaymentTotalAmt
 * @property double $unallocatedAmount
 * @property string $date
 * @property string $reqpaymenteventReqRef
 * @property string $reqpaymentPrivateNote
 * @property string $reqpaymentCreatedAt
 * @property string $reqpaymentUpdatedAt
 * @property string $reqpaymentDeletedAt
 * @property int $userId
 * @property int $createdBy
 * @property int $approvedBy
 * @property int $paymentstatus
 *
 * @property FinanceAccounts $reqpaymentaccount0
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property FinanceAccounts $approvedBy0
 * @property FinanceRequisitionPaymentsLines[] $financeRequisitionPaymentsLines
 */
class FinanceRequisitionPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_requisition_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reqpaymentaccount', 'reqrecepitNumber', 'reqpaymentTotalAmt', 'unallocatedAmount', 'date', 'reqpaymenteventReqRef', 'userId', 'createdBy', 'paymentstatus'], 'required'],
            [['reqpaymentaccount', 'userId', 'createdBy', 'approvedBy', 'paymentstatus'], 'integer'],
            [['reqpaymentTotalAmt', 'unallocatedAmount'], 'number'],
            ['unallocatedAmount', 'match', 'pattern' => '/^[0-9]*$/'],
            ['unallocatedAmount', 'integer', 'min' => 0, 'max' => 1000000],
            [['date', 'reqpaymentCreatedAt', 'reqpaymentUpdatedAt', 'reqpaymentDeletedAt'], 'safe'],
            [['reqpaymentPrivateNote'], 'string'],
            [['reqpaymentPayType', 'reqrecepitNumber', 'reqpaymenteventReqRef'], 'string', 'max' => 50],
            [['reqrecepitNumber'], 'unique'],
            [['reqpaymentaccount', 'reqpaymenteventReqRef'], 'unique', 'message'=>'The account Transaction already exist for this Account. Please try another one.','targetAttribute' => ['reqpaymentaccount', 'reqpaymenteventReqRef']],
            [['reqpaymentaccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['reqpaymentaccount' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reqpaymentPayType' => 'Payment Method',
            'reqpaymentaccount' => 'Payment Account',
            'reqrecepitNumber' => 'Recepit Number',
            'reqpaymentTotalAmt' => 'Value',
            'unallocatedAmount' => 'Unallocated Amount',
            'date' => 'Date',
            'reqpaymenteventReqRef' => 'Transaction Code',
            'reqpaymentPrivateNote' => 'Private Note',
            'reqpaymentCreatedAt' => 'Created At',
            'reqpaymentUpdatedAt' => 'Updated At',
            'reqpaymentDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'createdBy' => 'Created By',
            'approvedBy' => 'Approved By',
            'paymentstatus' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReqpaymentaccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'reqpaymentaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceRequisitionPaymentsLines()
    {
        return $this->hasMany(FinanceRequisitionPaymentsLines::className(), ['reqpaymentId' => 'id']);
    }// Generate Receipt Number
    public function getRcpnumber()
    {
        //select product code

        $connection = Yii::$app->db;
        $query= "Select MAX(reqrecepitNumber) AS number from finance_requisition_payments where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "REQRCP-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "REQRCP-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "REQRCP-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
    // Get all the payments on Hold
    public function getallpaymentslinesonhold()
    {
        $paymentsonHold = FinanceRequisitionPaymentsLines::find()
            ->joinWith('eventReq')
            // ->where(['finance_requisition_payments_lines.vendorId' => $vendorId])
            ->where(['finance_requisition_payments_lines.status'=>3])
            ->Andwhere(['finance_requisition_payments_lines.userId'=>Yii::$app->user->identity->id])
            ->asArray()
            ->all();
        // print_r($paymentsonHold);

        return $paymentsonHold;

    }

    // Get payment based on the payment Id and status
    // Get all the payments on Hold
    public function getallpaymentslinesforApayment($id,$status)
    {
        $paymentsonHold = FinanceRequisitionPaymentsLines::find()
            ->joinWith('eventReq')
            ->where(['finance_requisition_payments_lines.reqpaymentId' => $id])
            ->Andwhere(['finance_requisition_payments_lines.status'=>$status])
            ->asArray()
            ->all();
        // print_r($paymentsonHold);

        return $paymentsonHold;
    }
}
