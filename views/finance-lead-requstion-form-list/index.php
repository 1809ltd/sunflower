<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceLeadRequstionFormListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Lead Requstion Form Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-lead-requstion-form-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Lead Requstion Form List', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'reqId',
            'activityId',
            'personelId',
            'personnelTel',
            //'description:ntext',
            //'qty',
            //'unitprice',
            //'amont',
            //'status',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
