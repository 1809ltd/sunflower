<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\transfer\models\FinanceTransferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Transfers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-transfer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Transfer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tranferRefernce',
            'tranferFromAccountRef',
            'tranferToAccountRef',
            'tranferAmount',
            //'transferCharges',
            //'tranferTxnDate',
            //'tranferCreatedAt',
            //'tranferUpdatedAt',
            //'tranferDeletedAt',
            //'userId',
            //'status',
            //'createdBy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
