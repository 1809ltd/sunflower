<?php

namespace app\modules\personnel;

/**
 * personnel module definition class
 */
class personnel extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\personnel\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->modules =[
          /*Personnel Details Sub module*/
          'personneldetails' => [
                'class' => 'app\modules\personnel\personneldetails\personneldetails',
            ],

        ];

        // custom initialization code goes here
    }
}
