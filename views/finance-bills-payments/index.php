<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Finance Bills Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-bills-payments-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Finance Bills Payments'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'billPaymentPayType',
            'billPaymentaccount',
            'billPaymentTotalAmt',
            'billPaymentBillRef',
            //'vendorId',
            //'billPaymentPrivateNote:ntext',
            //'billPaymentCreatedAt',
            //'billPaymentUpdatedAt',
            //'billPaymentDeletedAt',
            //'userId',
            //'paymentstatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
