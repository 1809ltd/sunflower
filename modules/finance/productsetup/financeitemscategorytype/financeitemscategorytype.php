<?php

namespace app\modules\finance\productsetup\financeitemscategorytype;

/**
 * financeitemscategorytype module definition class
 */
class financeitemscategorytype extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\productsetup\financeitemscategorytype\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
