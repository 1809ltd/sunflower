<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AssetCategory */

$this->title = 'Search Deliveryform';
$this->params['breadcrumbs'][] = ['label' => 'Search Results', 'url' => ['deliverysearch']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="asset-deliveryform-dlivery-search">

    <?= $this->render('searchformdelivery', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider
      ]) ?>

</div>
