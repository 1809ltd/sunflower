<?php

namespace app\modules\finance\income\invoice\invoiceitems\models;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
/*Invoice*/
use app\modules\finance\income\invoice\models\FinanceInvoice;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "finance_invoice_lines".
 *
 * @property int $id
 * @property int $invoiceId
 * @property int $itemRefId
 * @property string $itemRefName
 * @property string $invoiceLinesDescription
 * @property double $invoiceLinespack
 * @property double $invoiceLinesQty
 * @property double $invoiceLinesUnitPrice
 * @property double $invoiceLinesAmount
 * @property double $invoiceLinesTaxCodeRef
 * @property double $invoiceLinesTaxamount
 * @property double $invoiceLinesDiscount
 * @property string $invoiceLinesCreatedAt
 * @property string $invoiceLinesUpdatedAt
 * @property string $invoiceLinesDeletedAt
 * @property int $invoiceLinesStatus
 * @property int $userId
 * @property int $createdBy
 * @property int $approvedBy
 *
 * @property FinanceItems $itemRef
 * @property FinanceInvoice $invoice
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property UserDetails $approvedBy0
 */
class FinanceInvoiceLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_invoice_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemRefId', 'itemRefName', 'invoiceLinespack', 'invoiceLinesQty', 'invoiceLinesUnitPrice', 'invoiceLinesAmount', 'invoiceLinesTaxCodeRef', 'invoiceLinesTaxamount', 'invoiceLinesDiscount'], 'required'],
            [['invoiceId', 'itemRefId', 'invoiceLinesStatus', 'userId', 'createdBy', 'approvedBy'], 'integer'],
            [['invoiceLinesDescription'], 'string'],
            [['invoiceLinespack', 'invoiceLinesQty', 'invoiceLinesUnitPrice', 'invoiceLinesAmount', 'invoiceLinesTaxCodeRef', 'invoiceLinesTaxamount', 'invoiceLinesDiscount'], 'number'],
            [['invoiceId', 'invoiceLinesDescription', 'invoiceLinesCreatedAt', 'invoiceLinesUpdatedAt', 'invoiceLinesDeletedAt', 'invoiceLinesCreatedAt', 'invoiceLinesStatus', 'userId', 'createdBy'], 'safe'],
            [['itemRefName'], 'string', 'max' => 255],
            [['itemRefId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemRefId' => 'id']],
            [['invoiceId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceInvoice::className(), 'targetAttribute' => ['invoiceId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoiceId' => 'Invoice',
            'itemRefId' => 'Service',
            'itemRefName' => 'Service Name',
            'invoiceLinesDescription' => 'Description',
            'invoiceLinespack' => 'Days',
            'invoiceLinesQty' => 'Qty',
            'invoiceLinesUnitPrice' => 'Unit Price',
            'invoiceLinesAmount' => 'Sub Amount',
            'invoiceLinesTaxCodeRef' => 'Tax Code',
            'invoiceLinesTaxamount' => 'Taxamount',
            'invoiceLinesDiscount' => 'Discount',
            'invoiceLinesCreatedAt' => 'Created At',
            'invoiceLinesUpdatedAt' => 'Updated At',
            'invoiceLinesDeletedAt' => 'Deleted At',
            'invoiceLinesStatus' => 'Status',
            'userId' => 'User',
            'createdBy' => 'Created By',
            'approvedBy' => 'Approved By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemRef()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemRefId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(FinanceInvoice::className(), ['id' => 'invoiceId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }
}
