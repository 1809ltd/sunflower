<?php

namespace app\modules\finance\expense\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// for db functions
use yii\db\Expression;
/*All Expense summary  */
use app\modules\finance\expense\models\ExpenseSummary;
// PDf Maneno
use Mpdf\Mpdf;
/**
 * Default controller for the `expense` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

      // Create a model to allow the search

      $model = new \yii\base\DynamicModel([
        'startDate','endDate'
      ]);
      // Adding Rules for validation
      $model->addRule(['startDate','endDate'], 'required')
            ->validate();

    // Checking if it a load
     if($model->load(Yii::$app->request->post())){
       // do somenthing with model

       $tafuta="we are here";

       $allExpense = ExpenseSummary::find()->andFilterWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                       ->orderBy(['transactionDate' => SORT_ASC,])
                                       ->all();

       if (!empty($tafuta)) {
         // code...
         return $this->render('index', [
           'startDate' => $model->startDate,
           'endDate' => $model->endDate,
           'allExpense' => $allExpense,
           'model' => $model,
           'tafuta' => $tafuta,
         ]);
       } else {
         // code...
         return $this->render('index', [
           'model' => $model,
         ]);
       }
     }else {
       // code...
       return $this->render('index', [
         'model' => $model,
       ]);
     }
   }
   public function actionPdf($startDate,$endDate)
   {
     // $this->layout = '@app/views/layouts/addformdatatablelayout';

     if (empty($startDate)) {
       // code...

     } elseif ($endDate>$startDate) {
       // code...
       $allExpense = ExpenseSummary::find()->andFilterWhere(['between', 'transactionDate', $startDate, $endDate])
                                       ->orderBy(['transactionDate' => SORT_ASC,])
                                       ->all();
       // $this->layout='addformdatatablelayout';
       $this->layout = '@app/views/layouts/pdflayout';
       $mpdf = new mPDF( array(
         'mode' => '',
         'format' => 'A4',
         'default_font_size' => 0,
         'default_font' => '',
         'margin_left' => 15,
         'margin_right' => 15,
         'margin_top' => 16,
         'margin_bottom' => 16,
         'margin_header' => 9,
         'margin_footer' => 9,
         'orientation' => 'P'
       ));
       $pdf_info= $this->renderPartial('pdf', [
         'startDate' => $startDate,
         'endDate' => $endDate,
         'allExpense' => $allExpense,
       ]);
       $leo = date('M j, Y', strtotime(date('Y-m-d')));
       // $mpdf->cssInline= '@app/views/layouts/pdflayout' ;
       $mpdf->SetHeader('As of : '.$leo);
       $mpdf->shrink_tables_to_fit = 1;
       $mpdf->WriteHTML($pdf_info);
       $mpdf->Output();
       exit;

     } else {
       // code...


     }



   }

}
