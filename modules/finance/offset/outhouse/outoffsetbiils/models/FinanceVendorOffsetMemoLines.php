<?php

namespace app\modules\finance\offset\outhouse\outoffsetbiils\models;

/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Bills*/
use app\modules\finance\expense\bills\models\FinanceBills;

use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Accounts */
use app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemo;
use Yii;

/**
 * This is the model class for table "finance_vendor_offset_memo_lines".
 *
 * @property int $id
 * @property int $vendorId
 * @property int $vendorOffsetMemoId
 * @property int $billRef
 * @property string $vendorOffsetMemoLineDescription
 * @property double $vendorOffsetMemoLineAmount
 * @property int $vendorOffsetMemoLineStatus
 * @property string $vendorOffsetMemoLineCreatedAt
 * @property string $vendorOffsetMemoLineUpdatedAt
 * @property string $creidtDeletedAt
 * @property int $userId
 *
 * @property FinanceVendorOffsetMemo $vendorOffsetMemo
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $user
 * @property FinanceBills $billRef0
 */
class FinanceVendorOffsetMemoLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_vendor_offset_memo_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billRef', 'vendorOffsetMemoLineAmount'], 'required'],
            [['vendorId', 'vendorOffsetMemoId', 'billRef', 'vendorOffsetMemoLineStatus', 'userId'], 'integer'],
            [['vendorOffsetMemoLineDescription'], 'string'],
            [['vendorOffsetMemoLineAmount'], 'number'],
            [['vendorId', 'vendorOffsetMemoId', 'vendorOffsetMemoLineCreatedAt', 'vendorOffsetMemoLineUpdatedAt', 'creidtDeletedAt', 'vendorOffsetMemoLineStatus', 'userId'], 'safe'],
            [['vendorOffsetMemoId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceVendorOffsetMemo::className(), 'targetAttribute' => ['vendorOffsetMemoId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['billRef'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBills::className(), 'targetAttribute' => ['billRef' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorId' => 'Vendor',
            'vendorOffsetMemoId' => 'Vendor Offset Memo',
            'billRef' => 'Invoice',
            'vendorOffsetMemoLineDescription' => 'Description',
            'vendorOffsetMemoLineAmount' => 'Amount',
            'vendorOffsetMemoLineStatus' => 'Status',
            'vendorOffsetMemoLineCreatedAt' => 'Created At',
            'vendorOffsetMemoLineUpdatedAt' => 'Updated At',
            'creidtDeletedAt' => 'Creidt Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorOffsetMemo()
    {
        return $this->hasOne(FinanceVendorOffsetMemo::className(), ['id' => 'vendorOffsetMemoId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillRef0()
    {
        return $this->hasOne(FinanceBills::className(), ['id' => 'billRef']);
    }
}
