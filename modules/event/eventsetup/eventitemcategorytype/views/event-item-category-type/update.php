<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventItemCategoryType */

$this->title = 'Update Event Service Category Type: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Service Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-item-category-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
