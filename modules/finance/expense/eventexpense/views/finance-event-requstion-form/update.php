<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventRequstionForm */

$this->title = 'Update Finance Event Requstion Form: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Event Requstion Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-event-requstion-form-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceEventRequstionFormList'=>$modelsFinanceEventRequstionFormList, //to enable Dynamic Form
    ]) ?>

</div>
