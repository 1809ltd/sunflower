<?php
// use yii\helpers\Html;
// use yii\grid\GridView;
// use yii\widgets\Pjax;
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsSearch;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLinesSearch;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceRequisitionPaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Requisition Payments';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
    <div class="form-group">
      <?php
      $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        // 'id',
        'reqpaymentPayType',
        // 'reqpaymentaccount',
        [
          'attribute'=>'reqpaymentaccount',
          'value'=>'reqpaymentaccount0.fullyQualifiedName',
        ],
        'reqrecepitNumber',
        'reqpaymentTotalAmt',
        'unallocatedAmount',
        //'date',
        //'reqpaymenteventReqRef',
        //'reqpaymentPrivateNote:ntext',
        //'reqpaymentCreatedAt',
        //'reqpaymentUpdatedAt',
        //'reqpaymentDeletedAt',
        //'userId',
        //'createdBy',
        //'approvedBy',
        //'paymentstatus',
        ['class' => 'yii\grid\ActionColumn'],
      ];
      // Renders a export dropdown menu
      echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
          // 'filterModel' => $searchModel,
        'columns' => $gridColumns
      ]);
      // You can choose to render your own GridView separately
      // echo \kartik\grid\GridView::widget([
      //   'dataProvider' => $dataProvider,
      //   'filterModel' => $searchModel,
      //   'columns' => $gridColumns
      // ]);
      ?>

      <p class="pull-right">
          <?= Html::a('Record Requisition Payments', ['create'], ['class' => 'btn btn-success']) ?>
      </p>
      <!-- <?= Html::a('Record Requisition Payments', ['create'], ['class' => 'btn btn-success']) ?> -->
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="finance-office-requisition-payments-index">

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                ['class' => 'kartik\grid\ExpandRowColumn',
                  'value'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    return GridView::ROW_COLLAPSED;
                  },
                  'detail'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    $searchModel = new FinanceRequisitionPaymentsLinesSearch();
                    $searchModel->reqpaymentId = $model->id;
                    $searchModel->status = $model->paymentstatus;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return Yii::$app->controller->renderPartial('_paymentlines',[
                      'searchModel' => $searchModel,
                      'dataProvider' => $dataProvider,

                    ]);
                  },
              ],

                // 'id',
                'reqpaymentPayType',
                // 'reqpaymentaccount',
                [
                  'attribute'=>'reqpaymentaccount',
                  'value'=>'reqpaymentaccount0.fullyQualifiedName',
                ],
                'reqrecepitNumber',
                'reqpaymentTotalAmt',
                'unallocatedAmount',
                //'date',
                //'reqpaymenteventReqRef',
                //'reqpaymentPrivateNote:ntext',
                //'reqpaymentCreatedAt',
                //'reqpaymentUpdatedAt',
                //'reqpaymentDeletedAt',
                //'userId',
                //'createdBy',
                //'approvedBy',
                //'paymentstatus',

                /* ... GridView configuration ... */
                [
                    'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                    'items' => [
                        [
                            'label' => 'View',
                            'url'   => ['view'],
                        ],
                        [
                            'label' => 'Update',
                            'url'   => ['update'],
                            'linkOptions' => [
                                'data-method' => 'post'
                            ],
                            // 'options'=>['class'=>'update-modal-click grid-action'],
                            // 'update'=>function($url,$model,$key){
                            //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                            //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                            //           'class'=>'update-modal-click grid-action',
                            //           'data-toggle'=>'tooltip',
                            //           'data-placement'=>'bottom',
                            //           'title'=>'Update'
                            //       ]);
                            //       return $btn;
                            // },
                            // 'option' 'class'=>'update-modal-click grid-action',
                            // 'data-toggle'=>'tooltip',
                            // 'data-placement'=>'bottom',
                        ],
                        [
                            'label'   => 'Disable',
                            'url'     => ['delete'],
                            'linkOptions' => [
                                'data-method' => 'post'
                            ],
                        ],
                    ]
                ],

                // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>


  </div>
  <!-- /.box-body -->
  <div class="box-footer">

  </div>

</div>
<!-- /.box -->
