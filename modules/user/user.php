<?php

namespace app\modules\user;

/**
 * user module definition class
 */
class user extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\user\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*User Details Sub module*/
          'userdetails' => [
              'class' => 'app\modules\user\userdetails\userdetails',
          ],
          /*Module List sub module*/
          'moduleslist' => [
            'class' => 'app\modules\user\moduleslist\moduleslist',
          ],
          /*Role Tpe Sub Module*/
          'userroletypes' => [
            'class' => 'app\modules\user\userroletypes\userroletypes',
          ],
          /*User Role Permission Syb Module*/
          'userrolemodulepermission' => [
            'class' => 'app\modules\user\userrolemodulepermission\userrolemodulepermission',
          ],
          /*Assign Roles to Users*/
          'userassignment' => [
            'class' => 'app\modules\user\userassignment\userassignment',
          ],
        ];

        // custom initialization code goes here
    }
}
