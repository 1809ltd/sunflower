<?php

namespace app\modules\user\userassignment\controllers;

use yii\web\Controller;

/**
 * Default controller for the `userassignment` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
