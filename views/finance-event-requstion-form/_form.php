<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;
use kartik\select2\Select2;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;

/*Getting the product and service offered, customer, Tax*/
use app\models\FinanceEventRequstionFormList;
use app\models\PersonnelDetails;
use app\models\EventWorkplan;
use app\models\FinanceEventActivity;

use app\models\EventDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventRequstionForm */
/* @var $form yii\widgets\ActiveForm */

?>



<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="panel-body finance-event-requstion-form-form">

                  <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>


                  <div class="col-sm-3">
                    <div class="form-group">
                      <?php
                          // necessary for update action.
                          if (! $model->isNewRecord) {
                            // code...

                            $requstion = $model->requstionNumber;

                          } else {
                            // code...
                            $requstion=$model->getRQnumber();
                          }

                      ?>
                      <?= $form->field($model, 'requstionNumber')->textInput(['readonly' => true, 'value' => $requstion])?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'eventId')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(EventDetails::find()->all(),'id','eventNumber'),
                          'language' => 'en',
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                      <?= $form->field($model, 'requstionStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">

                    </div>
                  </div>

                  <div class="panel-body"></div>

                  <div class="panel-body">

                    <div class="panel panel-pvr panel--style--1">
                      <div class="panel-heading">
                          <div class="panel-heading-btn">
                              <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                              <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                              <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                              <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                          </div>
                          <h4 class="panel-title">Add Requisition Item</h4>
                      </div>
                      <div class="panel-body">
                           <?php DynamicFormWidget::begin([
                              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                              'widgetBody' => '.container-items', // required: css class selector
                              'widgetItem' => '.item', // required: css class
                              'limit' => 80, // the maximum times, an element can be cloned (default 999)
                              'min' => 1, // 0 or 1 (default 1)
                              'insertButton' => '.add-item', // css class
                              'deleteButton' => '.remove-item', // css class
                              'model' => $modelsFinanceEventRequstionFormList[0],
                              'formId' => 'dynamic-form',
                              'formFields' => [

                                // 'id',
                                // 'reqId',
                                'activityId',
                                'personelId',
                                'personnelTel',
                                'description',
                                'qty',
                                'unitprice',
                                'amont',
                                // 'status',
                                // 'createdAt',
                                // 'updatedAt',
                                // 'deletedAt',
                                // 'userId',


                              ],
                          ]); ?>

                          <div class="container-items"><!-- widgetContainer -->
                            <!-- Loopping Items in the Lists -->
                          <?php foreach ($modelsFinanceEventRequstionFormList as $i => $modelFinanceEventRequstionFormList): ?>
                              <div class="item panel-pvr panel--style--2 "><!-- widgetBody -->


                                  <div class="panel-heading">
                                      <h3 class="panel-title pull-left">Requisition Item</h3>
                                      <div class="pull-right">
                                          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>


                                  <div class="panel-body">
                                      <?php
                                          // necessary for update action.
                                          if (! $modelFinanceEventRequstionFormList->isNewRecord) {
                                              echo Html::activeHiddenInput($modelFinanceEventRequstionFormList, "[{$i}]id");
                                          }
                                      ?>

                                      <div class="col-sm-3">
                                        <div class="form-group">
                                        <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]activityId")->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(FinanceEventActivity::find()->all(),'id','activityName'),
                                            'language' => 'en',
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                        <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]workplanId")->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(EventWorkplan::find()->all(),'id','personnelPhone'),
                                            'language' => 'en',
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                        <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]personelId")->widget(Select2::classname(), [
                                            'data' => ArrayHelper::map(PersonnelDetails::find()->all(),'id','displayName'),
                                            'language' => 'en',
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]); ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]personnelTel")->textInput([
                                          'maxlength' => true,
                                          'class' => 'form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]description")->textInput() ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]qty")->textInput([
                                          'maxlength' => true,
                                          'class' => 'qnty form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]unitprice")->textInput([
                                          'maxlength' => true,
                                          'class' => 'price form-control']) ?>
                                        </div>
                                      </div>
                                     <div class="col-sm-3">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceEventRequstionFormList, "[{$i}]amont")->textInput([
                                          'readonly' => true,
                                          'maxlength' => true,
                                          'class' => 'sumPart  form-control']) ?>
                                        </div>
                                      </div>


                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>

                                  </div>
                              </div>
                          <?php endforeach; ?>
                          </div>
                          <?php DynamicFormWidget::end(); ?>
                      </div>
                  </div>

                  </div>

                    <div class="panel-body"></div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($model, 'requstionNote')->textarea(['rows' => 6]) ?>

                      </div>
                    </div>

                    <div class="col-sm-3">
                      <div class="form-group">
                        <?= $form->field($model, 'amount')->textInput(['readonly' => true,'class' => 'amounttotal form-control']) ?>
                      </div>
                    </div>



                    <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>
                    <?= $form->field($model, 'preparedBy')->hiddenInput(['value'=> "1"])->label(false);?>


                    <?= $form->field($model, 'approvedBy')->hiddenInput(['value'=> ""])->label(false);?>

                    <div class="col-sm-3">
                      <div class="form-group">

                      </div>
                    </div>

                    <div class="panel-body"></div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= Html::submitButton($modelFinanceEventRequstionFormList->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                    </div>
                  </div>

                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->

<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var amount = 0;
        var total = 0;
        var amounttotal= 0;

        items.each(function (index, elem) {

            //getting specific elements
            var qnty = $(elem).find(".qnty").val();
            var price = $(elem).find(".price").val();

            //Check if qnty and price are numeric or something like that
            amount = parseFloat(qnty) * parseFloat(price);

            //Assign the Taxation value to the field
            $(elem).find(".sumPart").val(amount);

            amounttotal = amounttotal+amount;

            //assing value to the Amount total tax amount
            $(".amounttotal").val(amounttotal);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>
