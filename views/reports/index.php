<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Sunflower Management System';
?>
  <!-- Main content -->
    <section class="content">

      <!-- START pf Report Page -->
      <h2 class="page-header">Financial Related Report</h2>

      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['/reports/income']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Income Statement</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>Income minus expenses; tells you if you brought in more than you spent this period.</p>
                <small>Profit<cite title="Source Title"> & Loss</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>
        <!-- ./col -->


          <div class="col-md-6">
            <a href="<?= Url::to((['/reports/balancesheet']))?>">
              <div class="box box-solid">
              <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Balance Sheet</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <blockquote>
                  <p>Snapshot of what your business owns or is due to receive from others (assets), what it owes to others (liabilities), and what you've invested or retained in your company (equity).</p>
                  <small>Pending<cite title="Source Title"> Bills</cite></small>
                </blockquote>
              </div>
              <!-- /.box-body -->
            </a>
            <!-- /.box -->
          </div>
        </a>
        </div>
          <!-- ./col -->


      </div>
      <!-- /.row -->


      <!-- START pf Report Page -->
      <h2 class="page-header">Financial Related Report</h2>

      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['/reports/agedreciveables']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Aged Receivables</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>See how much money is expected to come in, and how long you've been waiting for it.</p>
                <small>Pending<cite title="Source Title"> Invoices</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>
        <!-- ./col -->


          <div class="col-md-6">
            <a href="<?= Url::to((['/reports/agedpayables']))?>">
              <div class="box box-solid">
              <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Aged Payables</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <blockquote>
                  <p>See the expenses you haven't paid yet, and how long payment has been outstanding.</p>
                  <small>Pending<cite title="Source Title"> Bills</cite></small>
                </blockquote>
              </div>
              <!-- /.box-body -->
            </a>
            <!-- /.box -->
          </div>
        </a>
        </div>
          <!-- ./col -->


      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['/reports/incomebycustomer']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Income by Customer</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>See the income you received, broken down by source.</p>
                <small>All<cite title="Source Title"> Invoices</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>
        <!-- ./col -->
        <div class="col-md-6">
            <a href="<?= Url::to((['/reports/billsbyvendor']))?>">
              <div class="box box-solid">
              <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Expense by Vendor</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <blockquote>
                  <p>See what you paid in expenses, broken down by recipient.</p>
                  <small>All<cite title="Source Title"> Bills</cite></small>
                </blockquote>
              </div>
              <!-- /.box-body -->
            </a>
            <!-- /.box -->
          </div>
        </a>
        </div>
          <!-- ./col -->


      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-6">
          <a href="<?= Url::to((['/reports/event']))?>">
            <div class="box box-solid">
            <div class="box-header with-border">
              <i class="fa fa-text-width"></i>

              <h3 class="box-title">Event Financial Report</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <blockquote>
                <p>Event Financial Statement.</p>
                <small>All<cite title="Source Title"> Event Financial Transaction</cite></small>
              </blockquote>
            </div>
            <!-- /.box-body -->
          </a>
          <!-- /.box -->
        </div>
      </a>
      </div>
        <!-- ./col -->


          <div class="col-md-6">
            <a href="<?= Url::to((['/reports/agedpayables']))?>">
              <div class="box box-solid">
              <div class="box-header with-border">
                <i class="fa fa-text-width"></i>
                <h3 class="box-title">Account Transactions</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <blockquote>
                  <p>See the transactions that occurred in each account.</p>
                  <small>summary<cite title="Source Title"> details</cite></small>
                </blockquote>
              </div>
              <!-- /.box-body -->
            </a>
            <!-- /.box -->
          </div>
        </a>
        </div>
          <!-- ./col -->


      </div>
      <!-- /.row -->




      <!-- END TYPOGRAPHY -->

    </section>
    <!-- /.content -->
