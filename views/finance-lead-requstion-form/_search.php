<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceLeadRequstionFormSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-lead-requstion-form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'requstionNumber') ?>

    <?= $form->field($model, 'leadId') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'requstionStatus') ?>

    <?php // echo $form->field($model, 'requstionNote') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'preparedBy') ?>

    <?php // echo $form->field($model, 'createdAt') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'approvedBy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
