<?php

namespace app\modules\finance\offset\inhouse\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\offset\inhouse\models\FinanceOffsetMemo;

/**
 * FinanceOffsetMemoSearch represents the model behind the search form of `app\modules\finance\offset\inhouse\models\FinanceOffsetMemo`.
 */
class FinanceOffsetMemoSearch extends FinanceOffsetMemo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'offsetMemoAccount', 'customerId', 'offsetMemoStatus', 'userId'], 'integer'],
            [['refernumber', 'offsetMemoTxnDate', 'offsetMemoNote', 'offsetMemoCreatedAt', 'offsetMemoUpdatedAt', 'offsetMemoDeletedAt'], 'safe'],
            [['offsetAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceOffsetMemo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'offsetMemoAccount' => $this->offsetMemoAccount,
            'offsetMemoTxnDate' => $this->offsetMemoTxnDate,
            'customerId' => $this->customerId,
            'offsetAmount' => $this->offsetAmount,
            'offsetMemoStatus' => $this->offsetMemoStatus,
            'offsetMemoCreatedAt' => $this->offsetMemoCreatedAt,
            'offsetMemoUpdatedAt' => $this->offsetMemoUpdatedAt,
            'offsetMemoDeletedAt' => $this->offsetMemoDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'refernumber', $this->refernumber])
            ->andFilterWhere(['like', 'offsetMemoNote', $this->offsetMemoNote]);

        return $dataProvider;
    }
}
