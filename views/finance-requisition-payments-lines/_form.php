<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-requisition-payments-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reqpaymentId')->textInput() ?>

    <?= $form->field($model, 'eventReqId')->textInput() ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
