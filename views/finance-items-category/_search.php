<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-items-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'financeItemCategoryName') ?>

    <?= $form->field($model, 'financeItemCategoryDescription') ?>

    <?= $form->field($model, 'financeItemCategoryParentId') ?>

    <?= $form->field($model, 'financeItemCategoryStatus') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'createdAt') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
