<?php

namespace app\controllers;

use Yii;
use app\models\FinanceOfficeRequstionForm;
use app\models\FinanceOfficeRequstionFormSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\models\FinanceOfficeRequstionFormList;
use app\models\FinanceOfficeRequstionFormListSearch;

/*Get Event Details*/
use app\models\EventDetails;


/**
 * FinanceOfficeRequstionFormController implements the CRUD actions for FinanceOfficeRequstionForm model.
 */
class FinanceOfficeRequstionFormController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }



    /**
     * Lists all FinanceOfficeRequstionForm models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinanceOfficeRequstionFormSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceOfficeRequstionForm model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceOfficeRequstionForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FinanceOfficeRequstionForm();

        $modelsFinanceOfficeRequstionFormList = [new FinanceOfficeRequstionFormList];

        //Generate Office Number
        $model->requstionNumber = $model->getRQnumber();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsFinanceOfficeRequstionFormList = Model::createMultiple(FinanceOfficeRequstionFormList::classname());
          Model::loadMultiple($modelsFinanceOfficeRequstionFormList, Yii::$app->request->post());

          // ajax validation
          /*
          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFinanceOfficeRequstionFormList),
                    ActiveForm::validate($model)
                );
            }*/

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceOfficeRequstionFormList) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceOfficeRequstionFormList as $modelFinanceOfficeRequstionFormList) {

                        $modelFinanceOfficeRequstionFormList->reqId = $model->id;
                        $modelFinanceOfficeRequstionFormList->userId = $model->userId;
                        $modelFinanceOfficeRequstionFormList->status = $model->requstionStatus;


                          if (! ($flag = $modelFinanceOfficeRequstionFormList->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }


        }

        return $this->render('create', [
            'model' => $model,
              'modelsFinanceOfficeRequstionFormList' => (empty($modelsFinanceOfficeRequstionFormList)) ? [new FinanceOfficeRequstionFormList] : $modelsFinanceOfficeRequstionFormList
        ]);
    }

    /**
     * Updates an existing FinanceOfficeRequstionForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelsFinanceOfficeRequstionFormList = $this->getFinanceOfficeRequstionFormList($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $oldIDs = ArrayHelper::map($modelsFinanceOfficeRequstionFormList, 'id', 'id');
          $modelsFinanceOfficeRequstionFormList = Model::createMultiple(FinanceOfficeRequstionFormList::classname(), $modelsFinanceOfficeRequstionFormList);
          Model::loadMultiple($modelsFinanceOfficeRequstionFormList, Yii::$app->request->post());
          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceOfficeRequstionFormList, 'id', 'id')));

          // ajax validation
          if (Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ArrayHelper::merge(
                  ActiveForm::validateMultiple($modelsFinanceOfficeRequstionFormList),
                  ActiveForm::validate($model)
              );
          }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceOfficeRequstionFormList) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      if (! empty($deletedIDs)) {
                          FinanceOfficeRequstionFormList::deleteAll(['id' => $deletedIDs]);
                      }
                      foreach ($modelsFinanceOfficeRequstionFormList as $modelFinanceOfficeRequstionFormList) {

                          $modelFinanceOfficeRequstionFormList->reqId = $model->id;
                          $modelFinanceOfficeRequstionFormList->userId = $model->userId;
                          $modelFinanceOfficeRequstionFormList->status = $model->requstionStatus;


                          if (! ($flag = $modelFinanceOfficeRequstionFormList->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
              }
          }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsFinanceOfficeRequstionFormList' => (empty($modelsFinanceOfficeRequstionFormList)) ? [new FinanceOfficeRequstionFormList] : $modelsFinanceOfficeRequstionFormList
        ]);

    }

    public function getFinanceOfficeRequstionFormList($id)
    {
      $model = FinanceOfficeRequstionFormList::find()->where(['reqId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing FinanceOfficeRequstionForm model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceOfficeRequstionForm model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceOfficeRequstionForm the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceOfficeRequstionForm::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
