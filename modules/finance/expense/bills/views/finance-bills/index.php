<?php

use yii\helpers\Html;

//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;

/*Adding an Expanding Row */

use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/

use app\modules\finance\expense\bills\models\FinanceBills;
use app\modules\finance\expense\bills\models\FinanceBillsSearch;
use app\modules\finance\expense\bills\billslines\models\FinanceBillsLines;
use app\modules\finance\expense\bills\billslines\models\FinanceBillsLinesSearch;

/*Pop up menu*/

use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceBillsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Bills';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- begin row -->
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <!-- <div class="form-group">
      <p class="pull-right">
          <?= Html::a('Create Bill', ['create'], ['class' => 'btn btn-success']) ?>
      </p>
    </div> -->
    </div>
    <!-- /.box-header -->
    <!-- <div class="pull-right"> -->
    <!-- <div class="col-sm-3"> -->
    <!-- <div class="form-group">
        <p class="pull-right">
            <?php
    // Html::a('Create Quotation', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div> -->
    <!-- </div> -->
    <!-- </div> -->

    <div class="box-body finance-bills-index">

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]);

        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'vendorId',
                'value' => 'vendor.vendorCompanyName',
            ],
            [
                'attribute' => 'projectId',
                'value' => 'project.eventName',
            ],
            'billRef',
            'billDueDate',
            'billsTaxAmount',
            'billAmount',
            [
                'label' => 'Amount',
                'attribute' => 'billTotal',
                'value' => function ($model) {
                    return $model->Balance();
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ];
        //    // Renders a export dropdown menu
        //    echo ExportMenu::widget([
        //      'dataProvider' => $dataProvider,
        //        // 'filterModel' => $searchModel,
        //      'columns' => $gridColumns
        //    ]);
        // You can choose to render your own GridView separately
        // echo \kartik\grid\GridView::widget([
        //   'dataProvider' => $dataProvider,
        //   'filterModel' => $searchModel,
        //   'columns' => $gridColumns
        // ]);
        ?>
        <p class="pull-right">
            <?= Html::a('Create Bill', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($model) {
                // code...
                if ($model->billStatus == 1) {
                    // code...
                    return ['class' => 'success'];

                } elseif ($model->billStatus == 2) {
                    // code...
                    return ['class' => 'warning'];
                } else {
                    // code...
                    return ['class' => 'danger'];
                }

            },
            'columns' => [
                // ['class' => 'yii\grid\SerialColumn'],
                ['class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        // code...
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail' => function ($model, $key, $index, $column) {
                        // code...
                        $searchModel = new FinanceBillsLinesSearch();
                        $searchModel->biilsId = $model->id;
                        $searchModel->billsLinesBillableStatus = $model->billStatus;
                        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                        return Yii::$app->controller->renderPartial('billdetails', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,

                        ]);
                    },
                ],

                // 'id',
                // 'vendorId',
                [
                    'attribute' => 'vendorId',
                    'value' => 'vendor.vendorCompanyName',
                ],
                [
                    'attribute' => 'projectId',
                    'value' => 'project.eventName',
                ],
                'billRef',
                'billDueDate',
                'billsTaxAmount',
                'billAmount',
                [
                    'label' => 'Amount',
                    'attribute' => 'billTotal',
                    'value' => function ($model) {
                        return $model->Balance();
                    }
                ],
                //'billCreatetime',
                //'billUpdatedTime',
                //'billDeletedAt',
                //'billStatus',
                //'userId',

                // ['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                    'items' => [
                        [
                            'label' => 'View',
                            'url' => ['view'],
                        ],
                        [
                            'label' => 'Update',
                            'url' => ['update'],
                            // 'options'=>['class'=>'update-modal-click grid-action'],
                            // 'update'=>function($url,$model,$key){
                            //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                            //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                            //           'class'=>'update-modal-click grid-action',
                            //           'data-toggle'=>'tooltip',
                            //           'data-placement'=>'bottom',
                            //           'title'=>'Update'
                            //       ]);
                            //       return $btn;
                            // },
                            // 'option' 'class'=>'update-modal-click grid-action',
                            // 'data-toggle'=>'tooltip',
                            // 'data-placement'=>'bottom',
                        ],
                        [
                            'label' => 'Disable',
                            'url' => ['delete'],
                            'linkOptions' => [
                                'data-method' => 'post'
                            ],
                        ],
                    ]
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
