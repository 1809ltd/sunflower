<!-- <div class="cashflow-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Cash Flow Statement';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = ['label' => 'Cash Flow Statement', 'url' => ['index']];;
?>
<!-- Search Date And the rest -->

<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <?php $form = ActiveForm::begin(); ?>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date From: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                      //'language' => 'ru',
                      'options' => ['autocomplete'=>'off','class' => 'form-control'],
                      'inline' => false,
                      'dateFormat' => 'yyyy-MM-dd',
                  ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date To: </label>
          <div class="col-lg-8">
                <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <div class="col-lg-8 col-lg-offset-4">
            <div class="center-align">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
            </div>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
<?php
if (!empty($tafuta)) {
  // code...
  ?>
  <!-- Main content -->

  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Income Statement</h3>
      <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>
    <div class="box">
      <div class="box-body">
        <h5 class="box-title">REVENUE</h5>
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th class="text-left">Account</th>
              <th class="text-right">Balance</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $revenueTotal=0;

            foreach ($revenue as $revenue){
              // code...
              $revenueTotal+= $revenue["dr_amount"];
              $transactionAccountId= $revenue["accountId"];

              // Brackets if value is negative
              $replaceamountrevenue = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($revenue["dr_amount"],2,'.',',')
             );
              ?>
              <tr>
                <td  class="text-left"><?= $revenue["accountName"]." : ".$revenue->transactionCode." Payment for ".$revenue->referenceCode;;?></td>
                <td  class="text-right"><span class="align-numbers"><?= $replaceamountrevenue ;?></span></td>
              </tr>
              <?php
            }
            ?>
            <tr>
              <th  class="text-left">Total Revenue:</th>
              <td  class="text-right"><?= number_format($revenueTotal,2); ?></td>
            </tr>
          </tbody>
        </table>
      </br>
      <h5 class="box-title">COST OF GOODS SOLD</h5>
      <table class="table  table-striped table-condensed">
        <thead>
          <tr>
            <th class="text-left">Account</th>
            <th class="text-right">Balance</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $COGSTotal=0;
          foreach ($COGS as $COGS){
            // code...
            $COGSTotal+= $COGS["cr_amount"];
            $cogsAccountId= $COGS["accountId"];
            ?>
            <tr>
              <td  class="text-left"><?= $COGS["accountName"]." : ".$COGS->transactionCode." Payment for ".$COGS->referenceCode;?></td>
              <td  class="text-right"><span class="align-numbers"><?= number_format($COGS["cr_amount"],2) ;?></span></td>
            </tr>
            <?php
          }
          ?>
          <tr>
            <th class="text-left">Total Cost of Goods Sold (COGS):</th>
            <th class="text-right"><?= number_format($COGSTotal,2); ?></td>
          </tr>
          <tr>
            <th class="text-left">Gross Profit (Loss):</th>
            <?php
            // Brackets if value is negative

            $replacegrossprofLoss = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($revenueTotal-$COGSTotal,2,'.',',')
           );

            ?>
            <td class="text-right"><?= $replacegrossprofLoss ?></td>
          </tr>
        </tbody>
      </table>
    </br>
    <h5 class="box-title">OPERATING EXPENSE</h5>
    <table class="table  table-striped table-condensed">
      <thead>
        <tr>
          <th class="text-left">Account</th>
          <th class="text-right">Balance</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $operatingTotal=0;
        foreach ($operating as $operating){
          // code...
          $operatingTotal+= $operating["cr_amount"];
          $operatingAccountId= $operating["accountId"];
          ?>
          <tr>
            <td  class="text-left"><?= $operating["accountName"]." : ".$operating->transactionCode." Payment for ".$operating->referenceCode;?></td>
            <td  class="text-right"><span class="align-numbers"><?= number_format($operating["cr_amount"],2) ;?></span></td>
          </tr>
          <?php
        }
        ?>
        <tr>
          <th class="text-left">Total Operating Expenses:</th>
          <th class="text-right"><?= number_format($operatingTotal,2); ?></th>
        </tr>
        <tr>
          <th class="text-left">Operating Profit (Loss)</th>
          <?php
          $operatingLossorProfit = preg_replace(
             '/(-)([\d\.\,]+)/ui',
             '($2)',
             number_format($revenueTotal-$COGSTotal-$operatingTotal,2,'.',',')
         );

          ?>
          <th class="text-right"><?= $operatingLossorProfit ?></th>
        </tr>
      </table>
    </tbody>
  </table>
    </br>
    <!-- <h5 class="box-title">INTEREST (INCOME), EXPENSE & TAXES</h5> -->
    <table class="table  table-striped table-condensed">
      <thead>
        <tr>
          <th class="text-left"></th>
          <th class="text-right"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th class="text-left"><strong>NET Profit(Loss)</strong></th>
          <th class="text-right"><?php  echo $operatingLossorProfit;?></th>
        </tr>
      </tbody>
    </table>
    </div>
    </div>
    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="javascript:void(0)" onclick="window.print()"
                 class="btn btn-xs btn-success m-b-10"><i
                      class="fa fa-print m-r-5"></i> Print
      </div>
    </div>
  </section>
  <!-- /.content -->
    <?php
  } else {
    // code...

    echo "No Activities found Here";
  }
  ?>
