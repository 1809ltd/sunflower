<?php
/*Getting all transactions*/
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
/*Account Receivable*/
use app\modules\finance\financereport\companyfinancials\models\AgedReceivables;
/*Accounts Payables*/
use app\modules\finance\financereport\companyfinancials\models\AgePayables;
/*Accounts Balances*/
use app\modules\finance\financereport\companyfinancials\models\AccountBalance;
/*Invoices Balance*/
use app\modules\finance\financereport\companyfinancials\models\InvoiceBalances;


use yii\helpers\Url;
$leo= date('Y-m-d');
// Get RandomColours
function random_color()
{
  $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
  $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
  return $color;
}
?>
<section class="content-header">
  <h1>Finance <small>Dashboard</small> </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">

        <div class="row">

          <!-- LINE CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Past 6 Months Accrual Accounting </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart" style="height:300px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
  <!-- Info boxes -->
  <div class="row">

    <!-- Info Details  -->


        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-files-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pending Invoices</span>
              <?php $PendingInvoices = InvoiceBalances::find()->where('owed >0')->count();?>
              <span class="info-box-number"><?= number_format($PendingInvoices,2);?></span>

              <!-- <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pending Bills</span>
              <span class="info-box-number"><?= number_format($PendingInvoices,2);?></span>

              <!-- <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Receivables  (>90 days)</span>
              <?php
              $more913= AgedReceivables::find()->sum("`>90 Days`");
              ?>
              <span class="info-box-number">SH <?= number_format($more913,2);?></span>
              <!-- <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Payables (>90 days)</span>
              <?php

              $more911= AgePayables::find()->sum("`>90 Days`");

               ?>
              <span class="info-box-number">SH <?= number_format($more911,2);?></span>

              <!-- <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->





      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Past 6 Month Cashflow</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">

                <div class="col-md-12 col-sm-12">

                  <div class="box-body">
                    <div class="chart">
                      <canvas id="lineChart" style="height:300px"></canvas>
                    </div>
                  </div>
                  <!-- <div class="chart">
                    <canvas id="areaChart" style="height:250px"></canvas>
                  </div> -->
                </div>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <div class="row">
            <div class="col-md-6">
              <!-- PRODUCT LIST -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Top 5 Income</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                    <?php
                    $connection = Yii::$app->getDb();
                    $command2 = $connection->createCommand("SELECT
                                                          all_transactions.accountParentId,
                                                          all_transactions.accountsclassfication,
                                                          all_transactions.accountId,
                                                          all_transactions.accountName,
                                                          Sum( all_transactions.cr_amount )
                                                          FROM
                                                          all_transactions
                                                          WHERE
                                                          all_transactions.transactionCategory = 'Revenue'
                                                          AND Date(`all_transactions`.`transactionDate`) <= Date('$leo')
                                                          GROUP BY
                                                          all_transactions.accountId
                                                          ORDER BY
                                                          Sum( all_transactions.cr_amount ) ASC LIMIT 5
                                                          ");
                    $inacccount = $command2->query();

                    foreach ($inacccount as $inacccount){
                      // code...
                      $transactionAccountId= $inacccount["accountId"];
                      ?>
                      <li class="item">
                        <div class="product-info">
                          <a href="javascript:void(0)" class="product-title"><?= $inacccount["accountName"];?>
                            <span class="label label-success pull-right">Ksh <?= number_format($inacccount["Sum( all_transactions.cr_amount )"],2) ;?></span></a>
                          <!-- <span class="product-description">
                                Samsung 32" 1080p 60Hz LED Smart HDTV.
                              </span> -->
                        </div>
                      </li>
                     <?php
                   }
                   ?>

                    <!-- /.item -->
                  </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <!-- <a href="javascript:void(0)" class="uppercase">View All Products</a> -->
                </div>
                <!-- /.box-footer -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
              <!-- Expense Accounts -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Top 5 Expense Account</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <ul class="products-list product-list-in-box">
                    <?php
                    $connection = Yii::$app->getDb();
                    $command3 = $connection->createCommand("SELECT
                                                          all_transactions.accountParentId,
                                                          all_transactions.accountsclassfication,
                                                          all_transactions.accountId,
                                                          all_transactions.accountName,
                                                          Sum( all_transactions.dr_amount )
                                                          FROM
                                                          all_transactions
                                                          WHERE
                                                          all_transactions.transactionCategory = 'Expense'
                                                          AND Date(`all_transactions`.`transactionDate`) <= Date('$leo')
                                                          GROUP BY
                                                          all_transactions.accountId
                                                          ORDER BY
                                                          Sum( all_transactions.dr_amount ) ASC LIMIT 5
                                                          ");
                    $expacccount = $command3->query();

                    foreach ($expacccount as $expacccount){
                      // code...

                      // $bal= $expacccount["Sum( all_transactions.dr_amount )"]-$expacccount["Sum( all_transactions.dr_amount )"];

                      // $expacccountTotal+=$bal;
                      $transactionAccountId= $expacccount["accountId"];
                      ?>
                      <li class="item">
                        <div class="product-info">
                          <a href="javascript:void(0)" class="product-title"><?= $expacccount["accountName"];?>
                            <span class="label label-danger pull-right">Ksh <?= number_format($expacccount["Sum( all_transactions.dr_amount )"],2) ;?></span></a>
                          <!-- <span class="product-description">
                                Samsung 32" 1080p 60Hz LED Smart HDTV.
                              </span> -->
                        </div>
                      </li>

                     <?php
                   }
                   ?>

                    <!-- /.item -->
                  </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <!-- <a href="javascript:void(0)" class="uppercase">View All Products</a> -->
                </div>
                <!-- /.box-footer -->
              </div>
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Receivable and Payables</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#receivable" data-toggle="tab">Receivable</a></li>
                  <li><a href="#payables" data-toggle="tab">Payables</a></li>
                </ul>
                <div class="tab-content">
                  <!-- receivable -->
                  <div class="tab-pane active" id="receivable">
                    <div class="table-responsive">
                          <table class="table no-margin">
                            <thead>
                            <tr>
                              <th colspan="2" >Invoices Not Paid</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php

                              $comingdue= AgedReceivables::find()->sum("`Coming Due`");
                              // array
                              $thirty = AgedReceivables::find()->sum("`1-30 Days`");
                              $sixtydays= AgedReceivables::find()->sum("`31-60 Days`");
                              $ninety= AgedReceivables::find()->sum("`61-90 Days`");
                              $more90= AgedReceivables::find()->sum("`>90 Days`");
                              ?>
                              <tr>
                                <td>Coming Due</td> <td>SH <?= number_format($comingdue,2);?></td>
                              </tr>
                              <tr>
                                  <td>1-30 days overdue</td><td>SH <?= number_format($thirty,2);?></td>
                              </tr>
                              <tr>
                                  <td>31-60 days overdue</td><td>SH <?= number_format($sixtydays,2);?></td>
                              </tr>
                              <tr>
                                  <td>61-90 days overdue</td><td>SH <?= number_format($ninety,2);?></td>
                              </tr>
                              <tr>
                                  <td>&gt; 90 days overdue</td><td>SH <?= number_format($more90,2);?></td>
                              </tr>

                            </tbody>
                          </table>
                        </div>
                        <!-- /.table-responsive -->

                        <div class="box-footer clearfix">
                          <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
                          <a href="<?= Url::to((['/finance/financereport/companyfinancials/default/agedreciveables']))?>" class="btn btn-sm btn-default btn-flat pull-right">View All Receivable </a>
                        </div>

                  </div>
                  <!-- /#receivable -->
                  <!-- payables-->
                  <div class="tab-pane" id="payables">
                    <div class="table-responsive">
                          <table class="table no-margin">
                            <thead>
                            <tr>
                              <th colspan="2" >Bills Not Paid</th>
                            </tr>
                            </thead>
                            <tbody>
                              <?php

                              $comingdu1e= AgePayables::find()->sum("`Coming Due`");
                              // array
                              $thirty1 = AgePayables::find()->sum("`1-30 Days`");
                              $sixtyday1s= AgePayables::find()->sum("`31-60 Days`");
                              $ninet1y= AgePayables::find()->sum("`61-90 Days`");
                              $more910= AgePayables::find()->sum("`>90 Days`");
                              ?>
                              <tr>
                                <td>Coming Due</td> <td>SH <?= number_format($comingdu1e,2);?></td>
                              </tr>
                              <tr>
                                  <td>1-30 days overdue</td><td>SH <?= number_format($thirty1,2);?></td>
                              </tr>
                              <tr>
                                  <td>31-60 days overdue</td><td>SH <?= number_format($sixtyday1s,2);?></td>
                              </tr>
                              <tr>
                                  <td>61-90 days overdue</td><td>SH <?= number_format($ninet1y,2);?></td>
                              </tr>
                              <tr>
                                  <td>&gt; 90 days overdue</td><td>SH <?= number_format($more910,2);?></td>
                              </tr>

                            </tbody>
                          </table>
                        </div>
                        <!-- /.table-responsive -->

                        <div class="box-footer clearfix">
                          <!-- <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a> -->
                          <a href="<?= Url::to((['/finance/financereport/companyfinancials/default/agedpayables']))?>" class="btn btn-sm btn-default btn-flat pull-right">View All Payables</a>
                        </div>

                  </div>
                  <!-- /#payables -->

                </div>
                <!-- /.tab-content -->
              </div>
              <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <div class="col-md-4">
          <!-- Expense Accounts -->
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Top 5 Overdue Invoices</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <?php
                $Invoices = InvoiceBalances::find()->limit(5)->all();

                foreach ($Invoices as $Invoices){

                  ?>

                 <li class="item">
                   <!-- <div class="product-img">
                     <img src="dist/img/default-50x50.gif" alt="Product Image">
                   </div> -->
                   <div class="product-info">
                     <a href="javascript:void(0)" class="product-title"><?= $Invoices["referenceCode"];?>
                       <span class="label label-danger pull-right">Ksh <?= number_format($Invoices["owed"],2) ;?></span></a>
                     <span class="product-description">
                           For <?= $Invoices["recipientName"];?>
                         </span>
                   </div>
                 </li>
                 <!-- /.item -->

                 <?php
               }

                 ?>

              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <!-- <a href="javascript:void(0)" class="uppercase">View All Products</a> -->
            </div>
            <!-- /.box-footer -->
          </div>

          <!-- /.info-box -->

          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Expense last 30 days</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <?php
                /*Expense Chart*/

               $piechartexpensedata = '';

               // Getting the data for the array;
               $allexpnesalast30days=AllTransactions::find()
                                     ->select(['sum(all_transactions.dr_amount) as dr_sum','all_transactions.accountParentId','all_transactions.accountId',
                                     'all_transactions.transactionCategory', 'all_transactions.transactionClassification', 'all_transactions.accountName'])
                                     ->where("all_transactions.transactionCategory = 'Expense' and ( DATEDIFF( CURDATE( ), all_transactions.transactionDate ) BETWEEN 1 AND 30 )")
                                     ->groupBy(['all_transactions.accountId'])
                                     // ->sum('all_transactions.dr_amount')
                                     ->all();


               $errors = array_filter($allexpnesalast30days);
               // Checking if the array is empty
                if (!empty($errors)) {
                  // code...
                  ?>

                  <div class="col-md-8">

                    <div class="chart-responsive">
                      <canvas id="pieChart" height="250"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-4">
                    <ul class="chart-legend clearfix">
                      <?php

                        foreach ($allexpnesalast30days as $allexpnesalast30days) {
                          // code...

                          // Getting the Colour of event Categories
                          $allexpnesalast30dayscolour = random_color();
                          // Assigning Values of the to data of chart

                          $piechartexpensedata .= "{ value:".$allexpnesalast30days['dr_sum'].",color:'".$allexpnesalast30dayscolour."',highlight:'".$allexpnesalast30dayscolour."',label:'".$allexpnesalast30days['accountName']."'},";
                          // array('value:' => $allexpnesalast30days['dr_sum'],'color:' =>"".$allexpnesalast30dayscolour."",'highlight:' => "".$allexpnesalast30dayscolour."",'label:' => $allexpnesalast30days['accountName'] );
                          ?>
                          <li><i class="fa fa-circle-o" style="color:<?=$allexpnesalast30dayscolour;?>"></i> <?= $allexpnesalast30days['accountName']."-Ksh ".number_format($allexpnesalast30days['dr_sum'],2)?></li>
                          <?php

                        }

                      ?>
                    </ul>
                  </div>
                  <!-- /.col -->
                  <?php
                }else {
                  // code...
                  ?>
                  <div class="col-md-12">
                    <img style="height:100%;width:100%" src="<?=Url::to('@web/img/placeholder-expenses-category.png'); ?>">

                    Expense Sample diagram
                  </div>

                  <?php
                }
                ?>
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <!-- <li><a href="#">United States of America
                  <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                </li>
                <li><a href="#">China
                  <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li> -->
              </ul>
            </div>
            <!-- /.footer -->
          </div>
          <!-- /.box -->

          <!-- PRODUCT LIST -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Accounts and Balance</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <?php

                $balancesas = AccountBalance::find()->all();

                foreach ($balancesas as $balancesas){
                  $accountId= $balancesas["accountId"];
                  ?>
                  <li class="item">
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title"><?= $balancesas["accountName"];?>
                        <span class="label label-warning pull-right">Ksh <?= number_format($balancesas["balance"],2) ;?></span></a>
                      <!-- <span class="product-description">
                            Samsung 32" 1080p 60Hz LED Smart HDTV.
                          </span> -->
                    </div>
                  </li>

                 <?php
               }

                 ?>

                <!-- /.item -->
              </ul>

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <!-- <a href="javascript:void(0)" class="uppercase">View All Products</a> -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->



      <!-- /.row -->
    </section>
    <!-- /.content -->
    <?php

/*getting the Chart Values*/
/*Getting the Income and Expense for the Past 6 months from today*/
$chart_array = array();
$chartExpenses = '';
$chartIncomes = '';
$total_labels= '';


for ($i = 6; $i >= 0; $i--) {
  // code...
  $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
  $months_explode = explode('-', $months);
  $year = $months_explode[0];
  $month = $months_explode[1];
  $last_visit = date('M Y',strtotime($months));

  /*Incomes*/
  $chartIncome=AllTransactions::find()->where(['all_transactions.transactionCategory'=>'Revenue','YEAR ( all_transactions.transactionDate )'=>$year, 'MONTH ( all_transactions.transactionDate )'=>$month])
                                      ->sum('cr_amount');

  if (empty($chartIncome)) {
    // code...
    $chartIncome = 0;
  }
  /*Getting Expesnse*/
  $chartExpense=AllTransactions::find()->where(['all_transactions.transactionCategory'=>'Expense','YEAR ( all_transactions.transactionDate )'=>$year, 'MONTH ( all_transactions.transactionDate )'=>$month])
                                            ->sum('dr_amount');

  if (empty($chartExpense)) {
      // code...
      $chartExpense = 0;
    }
      $total_labels .= "'".$last_visit."',";
      $chartIncomes .= $chartIncome.',';
      $chartExpenses .= $chartExpense.',';
    }

    /*Getting the Cashflow for the Past 6 months from today*/
    $chart_arrayflow = array();

    $chartExpensesflow = '';
    $chartIncomesflow = '';
    $total_labelsflow= '';

    for ($j = 6; $j >= 0; $j--) {
      // code...
      $monthsflow = date("Y-m", strtotime( date( 'Y-m-d' )." -$j months"));
      $months_explodeflow = explode('-', $monthsflow);
      $yearflow = $months_explodeflow[0];
      $monthflow = $months_explodeflow[1];
      $last_visitflow = date('M Y',strtotime($monthsflow));

      /*Incomes Flow*/
      $chartIncomeflow=AllTransactions::find()->where(['all_transactions.transactionCategory'=>'Revenue Payment','YEAR ( all_transactions.transactionDate )'=>$yearflow, 'MONTH ( all_transactions.transactionDate )'=>$monthflow])
                                            ->sum('dr_amount');

      if (empty($chartIncomeflow)) {
        // code...
        $chartIncomeflow = 0;
      }

    /*Getting Expesnse*/
      $chartExpenseflow=AllTransactions::find()->where(['all_transactions.transactionCategory'=>'Expense Payment','YEAR ( all_transactions.transactionDate )'=>$yearflow, 'MONTH ( all_transactions.transactionDate )'=>$monthflow])
                                            ->sum('cr_amount');

    if (empty($chartExpenseflow)) {
      // code...
      $chartExpenseflow = 0;
    }
      $total_labelsflow .= "'".$last_visitflow."',";
      $chartIncomesflow .= $chartIncomeflow.',';
      $chartExpensesflow .= $chartExpenseflow.',';
    }

    $script = <<<EOD
    $(function () {
      /* ChartJS
       * -------
       * Here we will create a few charts using ChartJS
       */
       //--------------
      //- AREA CHART -
      //--------------
      // Get context with jQuery - using jQuery's .get() method.
      var areaChartCanvas = $('#areaChart').get(0).getContext('2d')
      // This will get the first returned node in the jQuery collection.
      var areaChart       = new Chart(areaChartCanvas)
      var areaChartData = {
        labels  : [$total_labelsflow],
        datasets: [
          {
            label               : 'Income',
            fillColor           : 'rgba(13, 241, 88, 0.1)',
            strokeColor         : 'rgba(13, 241, 88)',
            pointColor          : 'rgba(13, 241, 88)',
            pointStrokeColor    : '#0DF158',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : [$chartIncomes]
          // data                : [65, 59, 80, 81, 56, 55, 40]
          },
          {
            label               : 'Expense',
            fillColor           : 'rgba(242, 17, 12, 0.1)',
            strokeColor         : 'rgba(242, 17, 11)',
            pointColor          : '#F2110C',
            pointStrokeColor    : 'rgba(242, 17, 12)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(242, 17, 12)',
            data                : [$chartExpenses]
          // data                : [28, 48, 40, 19, 86, 27, 90]
          }
        ]
      }
      var areaChartOptions = {
        //Boolean - If we should show the scale at all
        showScale               : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : true,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - Whether the line is curved between points
        bezierCurve             : true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension      : 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot                : true,
        //Number - Radius of each point dot in pixels
        pointDotRadius          : 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth     : 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius : 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke           : true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth      : 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill             : true,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio     : true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive              : true
      }
      //Create the line chart
      areaChart.Line(areaChartData, areaChartOptions)
      //-------------
      //- LINE CHART -
      //--------------
      var lineChartCanvas          = $('#lineChart').get(0).getContext('2d')
      var lineChart                = new Chart(lineChartCanvas)
      var lineChartDatainfo = {
        labels  : [$total_labelsflow],
        datasets: [
          {
            label               : 'Income',
            fillColor           : 'rgba(13, 241, 88, 0.1)',
            strokeColor         : 'rgba(13, 241, 88)',
            pointColor          : 'rgba(13, 241, 88)',
            pointStrokeColor    : '#0DF158',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(220,220,220,1)',
            data                : [$chartIncomesflow]
          // data                : [65, 59, 80, 81, 56, 55, 40]
          },
          {
            label               : 'Expense',
            fillColor           : 'rgba(242, 17, 12, 0.1)',
            strokeColor         : 'rgba(242, 17, 11)',
            pointColor          : '#F2110C',
            pointStrokeColor    : 'rgba(242, 17, 12)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgba(242, 17, 12)',
            data                : [$chartExpensesflow]
          // data                : [28, 48, 40, 19, 86, 27, 90]
          }
        ]
      }
      var lineChartOptions         = areaChartOptions
      lineChartOptions.datasetFill = false
      // create line chart graph
      lineChart.Line(lineChartDatainfo, lineChartOptions)
      //-------------
      //- PIE CHART -
      //-------------
      // Get context with jQuery - using jQuery's .get() method.
      var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
      var pieChart       = new Chart(pieChartCanvas)
      var PieData        = [$piechartexpensedata]
      var pieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions)
      //-------------
      //- BAR CHART -
      //-------------
      var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
      var barChart                         = new Chart(barChartCanvas)
      var barChartData                     = areaChartData
      barChartData.datasets[1].fillColor   = '#00a65a'
      barChartData.datasets[1].strokeColor = '#00a65a'
      barChartData.datasets[1].pointColor  = '#00a65a'
      var barChartOptions                  = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero        : true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines      : true,
        //String - Colour of the grid lines
        scaleGridLineColor      : 'rgba(0,0,0,.05)',
        //Number - Width of the grid lines
        scaleGridLineWidth      : 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines  : true,
        //Boolean - If there is a stroke on each bar
        barShowStroke           : true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth          : 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing         : 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing       : 1,
        //String - A legend template
        legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
        //Boolean - whether to make the chart responsive
        responsive              : true,
        maintainAspectRatio     : true
      }
      barChartOptions.datasetFill = false
      barChart.Bar(barChartData, barChartOptions)
    })
EOD;
$this->registerJs($script);
?>
