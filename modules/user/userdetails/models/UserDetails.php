<?php

namespace app\modules\user\userdetails\models;

use Yii;
use yii\db\ActiveRecord;

use yii\base\NotSupportedException;
use yii\helpers\Security;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "user_details".
 *
 * @property int $id
 * @property string $userStaffId
 * @property string $userFName
 * @property string $userLName
 * @property string $userPhone
 * @property string $userEmail
 * @property string $username
 * @property string $password
 * @property string $authKey
 * @property string $password_reset_token
 * @property string $userImage
 * @property int $userStatus
 * @property string $userLastLogin
 * @property int $userCreatedBy
 * @property string $userDeleteAt
 *
 * @property AssetCategory[] $assetCategories
 * @property AssetCategoryType[] $assetCategoryTypes
 * @property AssetCheckIn[] $assetCheckIns
 * @property AssetCheckIn[] $assetCheckIns0
 * @property AssetCheckOut[] $assetCheckOuts
 * @property CompanyDepartment[] $companyDepartments
 * @property CompanyDetails[] $companyDetails
 * @property CompanySiteDetails[] $companySiteDetails
 * @property CompanySiteLocation[] $companySiteLocations
 * @property CustomerLead[] $customerLeads
 * @property CustomerLeadAppointment[] $customerLeadAppointments
 * @property EventDetails[] $eventDetails
 * @property EventOrderfrom[] $eventOrderfroms
 * @property EventOrderfrom[] $eventOrderfroms0
 * @property EventOrderfromList[] $eventOrderfromLists
 * @property EventOrderfromList[] $eventOrderfromLists0
 * @property EventRequirements[] $eventRequirements
 * @property EventRequirements[] $eventRequirements0
 * @property EventTransport[] $eventTransports
 * @property EventTransport[] $eventTransports0
 * @property EventWorkplan[] $eventWorkplans
 * @property EventWorkplan[] $eventWorkplans0
 * @property FinanceBills[] $financeBills
 * @property FinanceBillsLines[] $financeBillsLines
 * @property FinanceBillsPayments[] $financeBillsPayments
 * @property FinanceCreditMemo[] $financeCreditMemos
 * @property FinanceCreditMemoLines[] $financeCreditMemoLines
 * @property FinanceCustomerLeadRequstionForm[] $financeCustomerLeadRequstionForms
 * @property FinanceCustomerLeadRequstionForm[] $financeCustomerLeadRequstionForms0
 * @property FinanceCustomerLeadRequstionForm[] $financeCustomerLeadRequstionForms1
 * @property FinanceCustomerLeadRequstionFormList[] $financeCustomerLeadRequstionFormLists
 * @property FinanceCustomerPurchaseOrder[] $financeCustomerPurchaseOrders
 * @property FinanceCustomerPurchaseOrderLines[] $financeCustomerPurchaseOrderLines
 * @property FinanceEstimate[] $financeEstimates
 * @property FinanceEventActivity[] $financeEventActivities
 * @property FinanceEventRequstionForm[] $financeEventRequstionForms
 * @property FinanceEventRequstionForm[] $financeEventRequstionForms0
 * @property FinanceEventRequstionForm[] $financeEventRequstionForms1
 * @property FinanceEventRequstionFormList[] $financeEventRequstionFormLists
 * @property FinanceInvoice[] $financeInvoices
 * @property FinanceInvoiceLines[] $financeInvoiceLines
 * @property FinanceItems[] $financeItems
 * @property FinanceItemsCategory[] $financeItemsCategories
 * @property FinanceItemsCategoryType[] $financeItemsCategoryTypes
 * @property FinanceJournalEntryLines[] $financeJournalEntryLines
 * @property FinanceLeadRequisitionPayments[] $financeLeadRequisitionPayments
 * @property FinanceLeadRequstionForm[] $financeLeadRequstionForms
 * @property FinanceLeadRequstionForm[] $financeLeadRequstionForms0
 * @property FinanceLeadRequstionForm[] $financeLeadRequstionForms1
 * @property FinanceOfficeRequisitionPayments[] $financeOfficeRequisitionPayments
 * @property FinanceOfficeRequstionForm[] $financeOfficeRequstionForms
 * @property FinanceOfficeRequstionForm[] $financeOfficeRequstionForms0
 * @property FinanceOfficeRequstionFormList[] $financeOfficeRequstionFormLists
 * @property FinanceOffsetMemo[] $financeOffsetMemos
 * @property FinanceOffsetMemoLines[] $financeOffsetMemoLines
 * @property FinancePurchaseOrder[] $financePurchaseOrders
 * @property FinanceRequisitionPayments[] $financeRequisitionPayments
 * @property FinanceSunflowerPurchaseOrder[] $financeSunflowerPurchaseOrders
 * @property FinanceSunflowerPurchaseOrderLines[] $financeSunflowerPurchaseOrderLines
 * @property FinanceTax[] $financeTaxes
 * @property FinanceVendorCreditMemo[] $financeVendorCreditMemos
 * @property FinanceVendorCreditMemoLines[] $financeVendorCreditMemoLines
 * @property FinanceVendorOffsetMemoLines[] $financeVendorOffsetMemoLines
 * @property VendorCompanyDetails[] $vendorCompanyDetails
 */
class UserDetails extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userFName', 'userLName', 'userPhone', 'userEmail', 'username', 'password', 'userStatus'], 'required'],
            [['userStatus', 'userCreatedBy'], 'integer'],
            [['userStaffId', 'userLastLogin', 'authKey', 'password_reset_token', 'userImage', 'userDeleteAt', 'userCreatedBy'], 'safe'],
            [['userStaffId'], 'string', 'max' => 50],
            [['userFName', 'userLName', 'userEmail'], 'string', 'max' => 255],
            [['userPhone'], 'string', 'max' => 15],
            [['username', 'password', 'authKey', 'password_reset_token'], 'string', 'max' => 250],
            [['userImage'], 'string', 'max' => 500],
            [['username'], 'unique'],
            [['userPhone'], 'unique'],
            [['userEmail'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userStaffId' => 'Staff Number',
            'userFName' => 'First Name',
            'userLName' => 'Last Name',
            'userPhone' => 'Phone',
            'userEmail' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'userImage' => 'User Image',
            'userStatus' => 'User Status',
            'userLastLogin' => 'User Last Login',
            'userCreatedBy' => 'User Created By',
            'userDeleteAt' => 'User Delete At',
        ];
    }
    /*Get Identity*/
    public static function findIdentity($id) {
    $user = self::find()
            ->where([
                "id" => $id
            ])
            ->one();
    // if (!count($user)) {
    //     return null;
    // }
    return new static($user);
    }

    /**
    * @inheritdoc
    */
    public static function findIdentityByAccessToken($token, $userType = null) {

        $user = self::find()
                ->where(["accessToken" => $token])
                ->one();
        if (!count($user)) {
            return null;
        }
        return new static($user);
    }

    /**
    * Finds user by username
    *
    * @param  string      $username
    * @return static|null
    */
    public static function findByUsername($username) {
        $user = self::find()
                ->where([
                    "username" => $username
                ])
                ->one();
        // if (!count($user)) {
        //     return null;
        // }
        return new static($user);
    }

    public static function findByUser($username) {
        $user = self::find()
                ->where([
                    "username" => $username
                ])
                ->one();
        if (!count($user)) {
            return null;
        }
        return $user;
    }

    /**
    * @inheritdoc
    */
    public function getId() {
        return $this->id;
    }

    /**
    * @inheritdoc
    */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
    * @inheritdoc
    */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
    * Validates password
    *
    * @param  string  $password password to validate
    * @return boolean if password provided is valid for current user
    */
    public function validatePassword($password) {
        return $this->password ===  md5($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCategories()
    {
        return $this->hasMany(AssetCategory::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCategoryTypes()
    {
        return $this->hasMany(AssetCategoryType::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckIns()
    {
        return $this->hasMany(AssetCheckIn::className(), ['athurizedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckIns0()
    {
        return $this->hasMany(AssetCheckIn::className(), ['checkinBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDepartments()
    {
        return $this->hasMany(CompanyDepartment::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDetails()
    {
        return $this->hasMany(CompanyDetails::className(), ['companyUserId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySiteDetails()
    {
        return $this->hasMany(CompanySiteDetails::className(), ['companyUserId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanySiteLocations()
    {
        return $this->hasMany(CompanySiteLocation::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLeads()
    {
        return $this->hasMany(CustomerLead::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLeadAppointments()
    {
        return $this->hasMany(CustomerLeadAppointment::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDetails()
    {
        return $this->hasMany(EventDetails::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfroms()
    {
        return $this->hasMany(EventOrderfrom::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfroms0()
    {
        return $this->hasMany(EventOrderfrom::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfromLists()
    {
        return $this->hasMany(EventOrderfromList::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventOrderfromLists0()
    {
        return $this->hasMany(EventOrderfromList::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRequirements()
    {
        return $this->hasMany(EventRequirements::className(), ['preparedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventRequirements0()
    {
        return $this->hasMany(EventRequirements::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventTransports()
    {
        return $this->hasMany(EventTransport::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventTransports0()
    {
        return $this->hasMany(EventTransport::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventWorkplans()
    {
        return $this->hasMany(EventWorkplan::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventWorkplans0()
    {
        return $this->hasMany(EventWorkplan::className(), ['approvedId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBills()
    {
        return $this->hasMany(FinanceBills::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsLines()
    {
        return $this->hasMany(FinanceBillsLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsPayments()
    {
        return $this->hasMany(FinanceBillsPayments::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemos()
    {
        return $this->hasMany(FinanceCreditMemo::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemoLines()
    {
        return $this->hasMany(FinanceCreditMemoLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerLeadRequstionForms()
    {
        return $this->hasMany(FinanceCustomerLeadRequstionForm::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerLeadRequstionForms0()
    {
        return $this->hasMany(FinanceCustomerLeadRequstionForm::className(), ['preparedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerLeadRequstionForms1()
    {
        return $this->hasMany(FinanceCustomerLeadRequstionForm::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerLeadRequstionFormLists()
    {
        return $this->hasMany(FinanceCustomerLeadRequstionFormList::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerPurchaseOrders()
    {
        return $this->hasMany(FinanceCustomerPurchaseOrder::className(), ['purchaseOrderdCreatedby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerPurchaseOrderLines()
    {
        return $this->hasMany(FinanceCustomerPurchaseOrderLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEstimates()
    {
        return $this->hasMany(FinanceEstimate::className(), ['estimatedCreatedby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventActivities()
    {
        return $this->hasMany(FinanceEventActivity::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionForms()
    {
        return $this->hasMany(FinanceEventRequstionForm::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionForms0()
    {
        return $this->hasMany(FinanceEventRequstionForm::className(), ['preparedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionForms1()
    {
        return $this->hasMany(FinanceEventRequstionForm::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionFormLists()
    {
        return $this->hasMany(FinanceEventRequstionFormList::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoices()
    {
        return $this->hasMany(FinanceInvoice::className(), ['invoicedCreatedby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceInvoiceLines()
    {
        return $this->hasMany(FinanceInvoiceLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItems()
    {
        return $this->hasMany(FinanceItems::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItemsCategories()
    {
        return $this->hasMany(FinanceItemsCategory::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItemsCategoryTypes()
    {
        return $this->hasMany(FinanceItemsCategoryType::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceJournalEntryLines()
    {
        return $this->hasMany(FinanceJournalEntryLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceLeadRequisitionPayments()
    {
        return $this->hasMany(FinanceLeadRequisitionPayments::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceLeadRequstionForms()
    {
        return $this->hasMany(FinanceLeadRequstionForm::className(), ['preparedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceLeadRequstionForms0()
    {
        return $this->hasMany(FinanceLeadRequstionForm::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceLeadRequstionForms1()
    {
        return $this->hasMany(FinanceLeadRequstionForm::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequisitionPayments()
    {
        return $this->hasMany(FinanceOfficeRequisitionPayments::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequstionForms()
    {
        return $this->hasMany(FinanceOfficeRequstionForm::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequstionForms0()
    {
        return $this->hasMany(FinanceOfficeRequstionForm::className(), ['approvedBy' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequstionFormLists()
    {
        return $this->hasMany(FinanceOfficeRequstionFormList::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOffsetMemos()
    {
        return $this->hasMany(FinanceOffsetMemo::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOffsetMemoLines()
    {
        return $this->hasMany(FinanceOffsetMemoLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePurchaseOrders()
    {
        return $this->hasMany(FinancePurchaseOrder::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceRequisitionPayments()
    {
        return $this->hasMany(FinanceRequisitionPayments::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceSunflowerPurchaseOrders()
    {
        return $this->hasMany(FinanceSunflowerPurchaseOrder::className(), ['purchaseOrderdCreatedby' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceSunflowerPurchaseOrderLines()
    {
        return $this->hasMany(FinanceSunflowerPurchaseOrderLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceTaxes()
    {
        return $this->hasMany(FinanceTax::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemos()
    {
        return $this->hasMany(FinanceVendorCreditMemo::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemoLines()
    {
        return $this->hasMany(FinanceVendorCreditMemoLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorOffsetMemoLines()
    {
        return $this->hasMany(FinanceVendorOffsetMemoLines::className(), ['userId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorCompanyDetails()
    {
        return $this->hasMany(VendorCompanyDetails::className(), ['companyUserId' => 'id']);
    }
}
