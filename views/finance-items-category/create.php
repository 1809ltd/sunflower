<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory */

$this->title = 'Create Finance Items Category';
$this->params['breadcrumbs'][] = ['label' => 'Finance Items Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-items-category-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
