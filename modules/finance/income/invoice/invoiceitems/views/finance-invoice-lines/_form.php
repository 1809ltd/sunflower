<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoiceLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-invoice-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'invoiceId')->textInput() ?>

    <?= $form->field($model, 'itemRefId')->textInput() ?>

    <?= $form->field($model, 'itemRefName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoiceLinesDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'invoiceLinespack')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesQty')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesUnitPrice')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesAmount')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesTaxCodeRef')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesTaxamount')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesDiscount')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesCreatedAt')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesDeletedAt')->textInput() ?>

    <?= $form->field($model, 'invoiceLinesStatus')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
