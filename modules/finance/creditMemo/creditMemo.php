<?php

namespace app\modules\finance\creditMemo;

/**
 * creditMemo module definition class
 */
class creditMemo extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\creditMemo\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->modules =[
          /*Customer Credit Sub Module*/
          'customercreditMemo' => [
              'class' => 'app\modules\finance\creditMemo\customercreditMemo\customercreditMemo',
          ],
          /*Vendor Credit Note */
          'vendorcreditMemo' => [
              'class' => 'app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemo',
          ],
        ];

        // custom initialization code goes here
    }
}
