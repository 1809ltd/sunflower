<?php

namespace app\modules\user\userroletypes\controllers;

use Yii;
use app\modules\user\userroletypes\models\UserRoleTypes;
use app\modules\user\userroletypes\models\UserRoleTypesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\modules\user\userrolemodulepermission\models\UserRoleModulePermissionSearch;
use app\modules\user\userrolemodulepermission\models\UserRoleModulePermission;

/**
 * UserRoleTypesController implements the CRUD actions for UserRoleTypes model.
 */
class UserRoleTypesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }


    /**
     * Lists all UserRoleTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new UserRoleTypesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserRoleTypes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
        $query = UserRoleModulePermission::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> ['defaultPageSize' => 50],
        ]);
        $query->andFilterWhere(['roleId' => $id]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'modelRolePermissions' => $dataProvider,
        ]);

    }

    /**
     * Creates a new UserRoleTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new UserRoleTypes();
        // $modelUserRoleModulePermissions = [new UserRoleModulePermission()];
        $modelUserRoleModulePermissions = [new UserRoleModulePermission];

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Geting the time created Timestamp
          $model->createdAt = date('Y-m-d H:i:s');

          //Getting user Log in details
          $model->createdBy = Yii::$app->user->identity->id;
          $model->userId = Yii::$app->user->identity->id;

          $model->save();

          //after Saving the Modules roles Details

          $modelUserRoleModulePermissions = Model::createMultiple(UserRoleModulePermission::classname());
          Model::loadMultiple($modelUserRoleModulePermissions, Yii::$app->request->post());


          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelUserRoleModulePermissions) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelUserRoleModulePermissions as $modelUserRoleModulePermission) {

                        $modelUserRoleModulePermission->roleId = $model->id;
                        $modelUserRoleModulePermission->createdAt = $model->createdAt;
                        $modelUserRoleModulePermission->createdBy = $model->createdBy;
                        $modelUserRoleModulePermission->userId = $model->userId;
                        $modelUserRoleModulePermission->status = $model->status;


                          if (! ($flag = $modelUserRoleModulePermission->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }

          // return $this->redirect(['view', 'id' => $model->id]);

        } else {
          // code...

          //load the create form
          return $this->render('create', [
              'model' => $model,
              'modelUserRoleModulePermissions' => (empty($modelUserRoleModulePermissions)) ? [new UserRoleModulePermission] : $modelUserRoleModulePermissions
          ]);
        }

    }

    /**
     * Updates an existing UserRoleTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);


         $modelUserRoleModulePermissions = $this->getusersRoleModulePermissions($model->id);

         if ($model->load(Yii::$app->request->post())) {
           // code...

           //Getting user Log in details
           $model->userId = Yii::$app->user->identity->id;
           $model->save();

           //after Saving the customer Details

           $oldIDs = ArrayHelper::map($modelUserRoleModulePermissions, 'id', 'id');
           $modelUserRoleModulePermissions = Model::createMultiple(UserRoleModulePermission::classname(), $modelUserRoleModulePermissions);
           Model::loadMultiple($modelUserRoleModulePermissions, Yii::$app->request->post());
           $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelUserRoleModulePermissions, 'id', 'id')));

           // ajax validation
           if (Yii::$app->request->isAjax) {
               Yii::$app->response->format = Response::FORMAT_JSON;
               return ArrayHelper::merge(
                   ActiveForm::validateMultiple($modelUserRoleModulePermissions),
                   ActiveForm::validate($model)
               );
           }

           // validate all models
           $valid = $model->validate();
           $valid = Model::validateMultiple($modelUserRoleModulePermissions) && $valid;

           if ($valid) {
               $transaction = \Yii::$app->db->beginTransaction();
               try {
                   if ($flag = $model->save(false)) {
                       if (! empty($deletedIDs)) {
                           UserRoleModulePermission::deleteAll(['id' => $deletedIDs]);
                       }
                       foreach ($modelUserRoleModulePermissions as $modelUserRoleModulePermission) {

                           $modelUserRoleModulePermission->roleId = $model->id;
                           $modelUserRoleModulePermission->userId = $model->userId;
                           $modelUserRoleModulePermission->status = $model->status;
                           if (empty($modelUserRoleModulePermission->createdBy)) {
                             // code...
                             $modelUserRoleModulePermission->createdBy= $model->userId;
                           }


                           if (! ($flag = $modelUserRoleModulePermission->save(false))) {
                               $transaction->rollBack();
                               break;
                           }
                       }
                   }
                   if ($flag) {
                       $transaction->commit();
                       return $this->redirect(['view', 'id' => $model->id]);
                   }
               } catch (Exception $e) {
                   $transaction->rollBack();
               }
           }

           // return $this->redirect(['view', 'id' => $model->id]);

         } else {
           // code...

           //load the create form
           return $this->render('update', [
               'model' => $model,
               'modelUserRoleModulePermissions' => (empty($modelUserRoleModulePermissions)) ? [new UserRoleModulePermission] : $modelUserRoleModulePermissions
           ]);
         }

        // $modelUserRoleModulePermissions = $model->userRoleModulePermissions;
        //
        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //
        //         $oldIDs = ArrayHelper::map($modelUserRoleModulePermissions, 'id', 'id');
        //         $modelUserRoleModulePermissions = Role::createMultiple(UserRoleModulePermission::classname(), $modelUserRoleModulePermissions);
        //
        //         UserRoleTypes::loadMultiple($modelUserRoleModulePermissions, Yii::$app->request->post());
        //         $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelUserRoleModulePermissions, 'id', 'id')));
        //
        //         $transaction = \Yii::$app->db->beginTransaction();
        //         try {
        //             if ($flag = $model->save(false)) {
        //                 if (! empty($deletedIDs)) {
        //                     UserRoleModulePermission::deleteAll(['id' => $deletedIDs]);
        //                 }
        //                 foreach ($modelUserRoleModulePermissions as $modelUserRoleModulePermission) {
        //                     $modelUserRoleModulePermission->role_id = $model->role_id;
        //                     $modelUserRoleModulePermission->added_at = date('Y-m-d H:i:s');
        //                     $modelUserRoleModulePermission->added_by = Yii::$app->user->identity->id;
        //                     if (! ($flag = $modelUserRoleModulePermission->save(false))) {
        //                         $transaction->rollBack();
        //                         break;
        //                     }
        //                 }
        //             }
        //             if ($flag) {
        //                 $transaction->commit();
        //                 return $this->redirect(['view', 'id' => $model->id]);
        //             }
        //         } catch (Exception $e) {
        //             $transaction->rollBack();
        //         }
        //         return $this->redirect(['index']);
        // } else {
        //
        //     return $this->render('create', [
        //         'model' => $model,
        //         'modelUserRoleModulePermissions' => (empty($modelUserRoleModulePermissions)) ? [new UserRoleModulePermission()] : $modelUserRoleModulePermissions,
        //     ]);
        // }

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('update', [
        //     'model' => $model,
        // ]);
    }
    public function getusersRoleModulePermissions($id)
    {
      $model = UserRoleModulePermission::find()->where(['roleId' => $id])->all();
      return $model;
    }

    /**
     * Deletes an existing UserRoleTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();

      $this->findModel($id)->updateAttributes(['status'=>0,'deletedAt' => new \yii\db\Expression('NOW()')]);


        return $this->redirect(['index']);
    }

    /**
     * Finds the UserRoleTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserRoleTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserRoleTypes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
