<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-bills-payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'billPaymentPayType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'billPaymentaccount')->textInput() ?>

    <?= $form->field($model, 'billPaymentTotalAmt')->textInput() ?>

    <?= $form->field($model, 'billPaymentBillRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendorId')->textInput() ?>

    <?= $form->field($model, 'billPaymentPrivateNote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'billPaymentCreatedAt')->textInput() ?>

    <?= $form->field($model, 'billPaymentUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'billPaymentDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'paymentstatus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
