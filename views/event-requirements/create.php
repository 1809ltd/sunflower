<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventRequirements */

$this->title = 'Create Event Requirements';
$this->params['breadcrumbs'][] = ['label' => 'Event Requirements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-requirements-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
