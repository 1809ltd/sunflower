<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\user\moduleslist\models\UserModulesList;
/* @var $this yii\web\View */
/* @var $model app\modules\user\moduleslist\models\UserModulesList */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body user-modules-list-form">

  <?= $form->field($model, 'moduleName')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'controller')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'route')->textInput(['maxlength' => true]) ?>
  <i class="fa fa-fw fa-cc-discover"></i> fa-cc-discover
<span class="glyphicon glyphicon-cog pull-right"></span>
  <?php
   // $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
  <?php
  // $icon=['fa fa-fw fa-black-tie' => '<i class='fa fa-fw fa-black-tie'></i> Sign Up', 'fa fa-fw fa-cc-discover' => "<i class=fa fa-fw fa-cc-discover></i> fa-cc-discover"];
  $icon=['1'=>"mama",'2'=>'<span class="glyphicon glyphicon-cog pull-right"></span>'];

  // echo  $form->field($model, 'icon')->dropDownList($icon, ['prompt'=>'Select Icon']);

  // ->widget(Select2::classname(), [
  //     'data' =>  $icon,
  //     // ArrayHelper::map(FinanceItems::find()->where('`itemsStatus` = 1')->asArray()->all(),'id','itemsName'),
  //     'language' => 'en',
  //     'options' => ['prompt'=>'Select...'],
  //     'pluginOptions' => [
  //         'allowClear' => true
  //     ],
  // ]);?>

  <?= $form->field($model, 'icon')->widget(Select2::classname(), [
      'data' => $icon,
      'language' => 'en',
      'options' => ['prompt'=>'Select...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);?>

  <?= $form->field($model, 'parentId')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(UserModulesList::find()->where('`status` = 1')->asArray()->all(),'id','moduleName'),
      'language' => 'en',
      'options' => ['id'=> 'parentId', 'placeholder' => 'Select a Module Parent ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]); ?>

  <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
  <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']); ?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
