<?php

namespace app\modules\event\eventsetup\eventcaterogies\models;

use Yii;

/**
 * This is the model class for table "event_caterogies".
 *
 * @property int $id
 * @property string $eventCatName
 * @property string $eventCatDesc
 * @property int $eventCatStatus
 * @property string $eventCatCreatedAt
 * @property string $eventCatLastUpdatedAt
 * @property string $eventCatDeletedAt
 * @property int $userId
 *
 * @property EventDetails[] $eventDetails
 */
class EventCaterogies extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_caterogies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eventCatName', 'eventCatDesc', 'eventCatStatus', 'userId'], 'required'],
            [['eventCatDesc'], 'string'],
            [['eventCatStatus', 'userId'], 'integer'],
            [['eventCatCreatedAt', 'eventCatLastUpdatedAt', 'eventCatDeletedAt'], 'safe'],
            [['eventCatName'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'eventCatName' => 'Event Cat Name',
            'eventCatDesc' => 'Event Cat Desc',
            'eventCatStatus' => 'Event Cat Status',
            'eventCatCreatedAt' => 'Event Cat Created At',
            'eventCatLastUpdatedAt' => 'Event Cat Last Updated At',
            'eventCatDeletedAt' => 'Event Cat Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventDetails()
    {
        return $this->hasMany(EventDetails::className(), ['eventCategory' => 'id']);
    }
}
