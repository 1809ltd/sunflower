<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\journal\models\FinanceJournalEntryLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-journal-entry-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'journalId') ?>

    <?= $form->field($model, 'accountId') ?>

    <?= $form->field($model, 'transactionId') ?>

    <?= $form->field($model, 'postingType') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'drAmount') ?>

    <?php // echo $form->field($model, 'crAmount') ?>

    <?php // echo $form->field($model, 'createAt') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'deletedAt') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
