<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_lead_communication_tracker".
 *
 * @property int $id
 * @property int $leadAppointId
 * @property int $leadId
 * @property string $commLeadTxnDate
 * @property string $commLeadMeetingSubject
 * @property string $commLeadPartiesInvolved
 * @property string $commLeadFormOfCommunication
 * @property string $commLeadNoted
 * @property string $commLeadNextDate
 * @property string $commLeadCreatedAt
 * @property string $commLeadLastUpdatedAt
 * @property string $commLeadDeletedAt
 * @property int $userId
 *
 * @property CustomerLeadAppointment $leadAppoint
 * @property CustomerLead $lead
 * @property UserDetails $user
 */
class CustomerLeadCommunicationTracker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_lead_communication_tracker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['leadAppointId', 'leadId', 'commLeadTxnDate', 'commLeadMeetingSubject', 'commLeadPartiesInvolved', 'commLeadFormOfCommunication', 'commLeadNoted', 'commLeadNextDate'], 'required'],
            [['leadAppointId', 'leadId', 'userId'], 'integer'],
            [['commLeadTxnDate', 'commLeadNextDate', 'commLeadCreatedAt', 'commLeadLastUpdatedAt', 'commLeadDeletedAt', 'userId'], 'safe'],
            [['commLeadMeetingSubject', 'commLeadPartiesInvolved', 'commLeadFormOfCommunication', 'commLeadNoted'], 'string'],
            [['leadAppointId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerLeadAppointment::className(), 'targetAttribute' => ['leadAppointId' => 'id']],
            [['leadId'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerLead::className(), 'targetAttribute' => ['leadId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Comm Lead',
            'leadAppointId' => 'Appointment',
            'leadId' => 'Project',
            'commLeadTxnDate' => 'Date',
            'commLeadMeetingSubject' => 'Meeting Subject',
            'commLeadPartiesInvolved' => 'Parties Involved',
            'commLeadFormOfCommunication' => 'Form Of Communication',
            'commLeadNoted' => 'Noted',
            'commLeadNextDate' => 'Next Meeting',
            'commLeadCreatedAt' => 'Comm Lead Created At',
            'commLeadLastUpdatedAt' => 'Comm Lead Last Updated At',
            'commLeadDeletedAt' => 'Comm Lead Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadAppoint()
    {
        return $this->hasOne(CustomerLeadAppointment::className(), ['id' => 'leadAppointId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLead()
    {
        return $this->hasOne(CustomerLead::className(), ['id' => 'leadId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
