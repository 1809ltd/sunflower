<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use yii\helpers\Url;
use yii\base\Model;
// Customer Details
use app\modules\customer\customerdetails\models\CustomerDetails;
// Event Details
// Personnel Details
use app\modules\personnel\personneldetails\models\PersonnelDetails;

use app\modules\event\eventInfo\models\EventDetails;
// Get RandomColours
function random_color()
{
  $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
  $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
  return $color;
}
?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Personnel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->

      <div class="row">

        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
                    <div class="box box-default">
                      <div class="box-header with-border">
                        <h3 class="box-title">Customer Events</h3>

                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <div class="row">
                          <div class="col-md-8">
                            <div class="chart-responsive">
                              <canvas id="pieChart" height="250"></canvas>
                            </div>
                            <!-- ./chart-responsive -->
                          </div>
                          <!-- /.col -->
                          <div class="col-md-4">
                            <ul class="chart-legend clearfix">
                              <?php
                              /*Event Category Chart*/
                              $piecharteventcategorynumbdata = '';
                              // Getting Categories
                              $numbereventCategory=EventDetails::find()
                                                    ->select(['Count(event_details.eventCategory) as caterogiesNumber','event_details.eventCategory','event_caterogies.eventCatName'])
                                                    ->where("event_details.`status` > 1")
                                                    ->groupBy(['event_details.eventCategory'])
                                                    ->innerJoinWith('eventCategory0')
                                                    ->all();

                              $errors = array_filter($numbereventCategory);
                              // Checking if the array is empty
                              if (!empty($errors)) {
                                // code...
                                foreach ($numbereventCategory as $numbereventCategory) {
                                  // code...
                                  // Getting the Colour of event Categories
                                  $eventcolour = random_color();
                                  // Assigning Values of the to data of chart
                                  $piecharteventcategorynumbdata .= "{ value:".$numbereventCategory['caterogiesNumber'].",color:'".$eventcolour."',highlight:'".$eventcolour."',label:'".$numbereventCategory['eventCategory0']['eventCatName']."'},";

                                  ?>
                                  <li><i class="fa fa-circle-o" style="color:<?=$eventcolour;?>"></i> <?= $numbereventCategory['eventCategory0']['eventCatName']."- ".number_format($numbereventCategory['caterogiesNumber'],2)?></li>
                                  <?php
                                }
                              }
                              ?>
                            </ul>
                          </div>
                          <!-- /.col -->
                        </div>
                        <!-- /.row -->
                      </div>
                      <!-- /.box-body -->
                      <div class="box-footer no-padding">
                        <ul class="nav nav-pills nav-stacked">
                          <!-- <li><a href="#">United States of America
                            <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                          <li><a href="#">India <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 4%</span></a>
                          </li>
                          <li><a href="#">China
                            <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 0%</span></a></li> -->
                        </ul>
                      </div>
                      <!-- /.footer -->
                    </div>
                    <!-- /.box -->

        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">

          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-person-add"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">New Staff</span>
              <?php
              $Newcustomer= PersonnelDetails::find()
                                            ->where('status>0')
                                            ->count();
              ?>
              <span class="info-box-number"><?= number_format($Newcustomer,2) ;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>

          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Staff</span>
              <?php
              $allcustomer= PersonnelDetails::find()
                                            ->where('status>0')
                                            ->count();

              ?>
              <span class="info-box-number"><?= number_format($allcustomer,2) ;?></span>
            </div>
            <!-- /.info-box-content -->
          </div>

        </div>
        <!-- ./col -->


        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <?php
              $activecustomer= PersonnelDetails::find()
                                            ->where('status>0')
                                            ->count();
              ?>
              <h3><?= number_format($activecustomer,2) ;?></h3>

              <p>Active Personnel</p>
            </div>
            <div class="icon">
              <i class="fa fa-fw fa-street-view"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
          <!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>

              <h3 class="box-title">Personnel List</h3>

              <div class="box-tools pull-right">
                <!-- <ul class="pagination pagination-sm inline">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul> -->
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- See @web/adminlte/dist/js/pages/dashboard.js to activate the todoList plugin -->
              <?php Pjax::begin(); ?>
              <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

              <?= GridView::widget([
                  'dataProvider' => $dataProvider,
                  'filterModel' => $searchModel,
                  'rowOptions'=> function($model)
                  {
                    // code...
                    if ($model->status==1) {
                      // code...
                      return['class'=>'success'];
                    } else {
                      // code...
                      return['class'=>'danger'];
                    }

                  },
                  'columns' => [
                      ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        'nationalId',
                        'displayName',
                        // 'firstName',
                        // 'sName',
                        // 'lName',
                        'primaryPhone',
                        //'secondaryPhone',
                        //'printOnChequeName',
                        //'primaryAddr:ntext',
                        //'town',
                        //'area',
                        //'email:email',
                        //'hiredDate',
                        //'createdAt',
                        //'deletedAt',
                        //'updatedAt',
                        //'billable',

                      // ['class' => 'yii\grid\ActionColumn'],
                  ],
              ]); ?>
              <?php Pjax::end(); ?>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add Personnel</button>
            </div>
          </div>
          <!-- /.box -->
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
              <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
              <li class="pull-left header"><i class="fa fa-inbox"></i> Sample Chart 1</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
              <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
            </div>
          </div>
          <!-- /.nav-tabs-custom -->
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">

          <!-- Calendar -->
          <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-calendar"></i>

              <h3 class="box-title">Calendar</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                <div class="btn-group">
                  <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bars"></i></button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="#">Add new event</a></li>
                    <li class="divider"></li>
                    <li><a href="#">View calendar</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
              <!-- Event Will be displayed here -->
                <!-- THE CALENDAR -->

                  <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                      'events'=> $events,
                  ));
                 ?>

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-6">
                  <!-- Progress bars -->
                  <!-- <div class="clearfix">
                    <span class="pull-left">Task #1</span>
                    <small class="pull-right">90%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 90%;"></div>
                  </div>

                  <div class="clearfix">
                    <span class="pull-left">Task #2</span>
                    <small class="pull-right">70%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 70%;"></div>
                  </div> -->
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                  <!-- <div class="clearfix">
                    <span class="pull-left">Task #3</span>
                    <small class="pull-right">60%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 60%;"></div>
                  </div>

                  <div class="clearfix">
                    <span class="pull-left">Task #4</span>
                    <small class="pull-right">40%</small>
                  </div>
                  <div class="progress xs">
                    <div class="progress-bar progress-bar-green" style="width: 40%;"></div>
                  </div> -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.box -->

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>

    <?php
    $script = <<<JS
    $(function () {
      /* ChartJS
       * -------
       * Here we will create a few charts using ChartJS
       */
      //-------------
      //- PIE CHART -
      //-------------
      // Get context with jQuery - using jQuery's .get() method.
      var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
      var pieChart       = new Chart(pieChartCanvas)
      var PieData        = [$piecharteventcategorynumbdata]
      var pieOptions     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
      }
      //Create pie or douhnut chart
      // You can switch between pie and douhnut using the method below.
      pieChart.Doughnut(PieData, pieOptions)
    })
JS;
$this->registerJs($script);
?>
