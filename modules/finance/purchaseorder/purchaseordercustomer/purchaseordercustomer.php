<?php

namespace app\modules\finance\purchaseorder\purchaseordercustomer;

/**
 * purchaseordercustomer module definition class
 */
class purchaseordercustomer extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\purchaseorder\purchaseordercustomer\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
