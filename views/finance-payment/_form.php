<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\receipt\invoicepayment\models\FinancePayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'invoicePaymentPayType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoicePaymentaccount')->textInput() ?>

    <?= $form->field($model, 'recepitNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'invoicePaymentTotalAmt')->textInput() ?>

    <?= $form->field($model, 'unallocatedAmount')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'invoicePaymentinvoiceRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerId')->textInput() ?>

    <?= $form->field($model, 'invoicePaymentPrivateNote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'invoicePaymentCreatedAt')->textInput() ?>

    <?= $form->field($model, 'invoicePaymentUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'invoicePaymentDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <?= $form->field($model, 'approvedBy')->textInput() ?>

    <?= $form->field($model, 'paymentstatus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
