<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceAccounts;

/**
 * FinanceAccountsSearch represents the model behind the search form of `app\models\FinanceAccounts`.
 */
class FinanceAccountsSearch extends FinanceAccounts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'companyId', 'accountParentId', 'accountsStatus', 'userId'], 'integer'],
            [['subAccount', 'accountName', 'accountNumber', 'accountsDescription', 'fullyQualifiedName', 'accountsClassification', 'accountsType', 'accountsDetailType', 'accountsPays', 'accountsCreateTime', 'accountsUpdatedAt', 'accountsDeleteAt'], 'safe'],
            [['accountsCurrentBalance'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceAccounts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'companyId' => $this->companyId,
            'accountParentId' => $this->accountParentId,
            'accountsStatus' => $this->accountsStatus,
            'accountsCurrentBalance' => $this->accountsCurrentBalance,
            'accountsCreateTime' => $this->accountsCreateTime,
            'accountsUpdatedAt' => $this->accountsUpdatedAt,
            'accountsDeleteAt' => $this->accountsDeleteAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'subAccount', $this->subAccount])
            ->andFilterWhere(['like', 'accountName', $this->accountName])
            ->andFilterWhere(['like', 'accountNumber', $this->accountNumber])
            ->andFilterWhere(['like', 'accountsDescription', $this->accountsDescription])
            ->andFilterWhere(['like', 'fullyQualifiedName', $this->fullyQualifiedName])
            ->andFilterWhere(['like', 'accountsClassification', $this->accountsClassification])
            ->andFilterWhere(['like', 'accountsType', $this->accountsType])
            ->andFilterWhere(['like', 'accountsDetailType', $this->accountsDetailType])
            ->andFilterWhere(['like', 'accountsPays', $this->accountsPays]);

        return $dataProvider;
    }
}
