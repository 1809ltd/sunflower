<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AssetCheckOutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asset Check Outs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-check-out-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Asset Check Out', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'assetId',
            'locationId',
            'checkOutDate',
            'dueDate',
            //'activity',
            //'deliveryId',
            //'activityId',
            //'orderId',
            //'orderlistId',
            //'vehicle',
            //'assignto',
            //'timestamp',
            //'updatedAt',
            //'deletedAt',
            //'status',
            //'athurizedBy',
            //'userId',
            //'createdBy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
