<?php

namespace app\modules\personnel\personneldetails\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use Yii;

/**
 * This is the model class for table "personnel_details".
 *
 * @property int $id
 * @property string $nationalId
 * @property string $displayName
 * @property string $firstName
 * @property string $sName
 * @property string $lName
 * @property string $primaryPhone
 * @property string $secondaryPhone
 * @property string $printOnChequeName
 * @property string $primaryAddr
 * @property string $town
 * @property string $area
 * @property string $email
 * @property int $status
 * @property int $createdBy
 * @property int $userId
 * @property string $hiredDate
 * @property string $createdAt
 * @property string $deletedAt
 * @property string $updatedAt
 * @property int $billable
 *
 * @property AssetCheckOut[] $assetCheckOuts
 * @property EventWorkplan[] $eventWorkplans
 * @property FinanceCustomerLeadRequstionFormList[] $financeCustomerLeadRequstionFormLists
 * @property FinanceEventRequstionFormList[] $financeEventRequstionFormLists
 * @property FinanceOfficeRequstionFormList[] $financeOfficeRequstionFormLists
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 */
class PersonnelDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personnel_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['displayName', 'firstName', 'status', 'createdBy', 'userId', 'billable'], 'required'],
            [['primaryAddr'], 'string'],
            [['status', 'createdBy', 'userId', 'billable'], 'integer'],
            [['hiredDate', 'createdAt', 'deletedAt', 'updatedAt'], 'safe'],
            [['nationalId', 'town'], 'string', 'max' => 50],
            [['displayName', 'firstName', 'sName', 'lName', 'printOnChequeName', 'area', 'email'], 'string', 'max' => 100],
            [['primaryPhone'], 'string', 'max' => 25],
            [['secondaryPhone'], 'string', 'max' => 15],
            [['nationalId'], 'unique'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nationalId' => 'National ID',
            'displayName' => 'Display Name',
            'firstName' => 'First Name',
            'sName' => 'S Name',
            'lName' => 'L Name',
            'primaryPhone' => 'Primary Phone',
            'secondaryPhone' => 'Secondary Phone',
            'printOnChequeName' => 'Print On Cheque Name',
            'primaryAddr' => 'Primary Addr',
            'town' => 'Town',
            'area' => 'Area',
            'email' => 'Email',
            'status' => 'Status',
            'createdBy' => 'Created By',
            'userId' => 'User ID',
            'hiredDate' => 'Hired Date',
            'createdAt' => 'Created At',
            'deletedAt' => 'Deleted At',
            'updatedAt' => 'Updated At',
            'billable' => 'Billable',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['assignto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventWorkplans()
    {
        return $this->hasMany(EventWorkplan::className(), ['personnelid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCustomerLeadRequstionFormLists()
    {
        return $this->hasMany(FinanceCustomerLeadRequstionFormList::className(), ['personelId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventRequstionFormLists()
    {
        return $this->hasMany(FinanceEventRequstionFormList::className(), ['personelId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequstionFormLists()
    {
        return $this->hasMany(FinanceOfficeRequstionFormList::className(), ['personelId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }
}
