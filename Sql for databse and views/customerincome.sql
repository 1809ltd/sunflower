DROP VIEW IF EXISTS `customerincome`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `customerincome` AS
SELECT
	finance_invoice.customerId AS customerId,
	customer_details.customerNumber AS customerNumber,
	customer_details.customerKraPin AS customerKraPin,
	customer_details.customerDisplayName AS customerDisplayName,
	finance_invoice_lines.invoiceLinesAmount AS invoiceLinesAmount,
	finance_invoice_lines.invoiceLinesTaxamount AS invoiceLinesTaxamount,
	finance_invoice_lines.invoiceLinesTaxCodeRef AS invoiceLinesTaxCodeRef,
	finance_invoice_lines.itemRefId AS itemRefId,
	finance_invoice_lines.itemRefName AS itemRefName,
	finance_invoice_lines.invoiceId AS invoiceId,
	finance_invoice_lines.id AS invoicelineId,
	finance_invoice_lines.invoiceLinesDescription AS invoiceLinesDescription,
	finance_invoice_lines.invoiceLinesQty AS invoiceLinesQty,
	finance_invoice_lines.invoiceLinesUnitPrice AS invoiceLinesUnitPrice,
	finance_invoice_lines.invoiceLinesCreatedAt AS invoiceLinesCreatedAt,
	finance_invoice_lines.invoiceLinesDiscount AS invoiceLinesDiscount,
	finance_invoice.invoiceTxnDate AS txnDate,
	finance_invoice_lines.invoiceLinesStatus AS invoiceLinesStatus,
	event_details.eventNumber AS eventNumber,
	event_caterogies.eventCatName AS eventCatName,
	event_caterogies.id AS catId
FROM
	finance_invoice_lines
	LEFT JOIN finance_invoice ON finance_invoice.id = finance_invoice_lines.invoiceId
	LEFT JOIN customer_details ON customer_details.id = finance_invoice.customerId
	LEFT JOIN event_details ON event_details.id = finance_invoice.invoiceProjectId
	LEFT JOIN event_caterogies ON event_caterogies.id = event_details.eventCategory
WHERE
	finance_invoice_lines.invoiceLinesStatus > 0
