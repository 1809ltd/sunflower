<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrderLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-customer-purchase-order-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'purchaseOrderId') ?>

    <?= $form->field($model, 'itemRefName') ?>

    <?= $form->field($model, 'purchaseOrderLinesDescription') ?>

    <?= $form->field($model, 'purchaseOrderLinespack') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesQty') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesUnitPrice') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesAmount') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesTaxCodeRef') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesTaxamount') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesDiscount') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesCreatedAt') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesUpdatedAt') ?>

    <?php // echo $form->field($model, 'purchaseOrderLinesDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
