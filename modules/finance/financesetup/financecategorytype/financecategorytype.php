<?php

namespace app\modules\finance\financesetup\financecategorytype;

/**
 * financecategorytype module definition class
 */
class financecategorytype extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financesetup\financecategorytype\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
