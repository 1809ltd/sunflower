<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\customerdetails\models\CustomerDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-details-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Customer Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customerNumber',
            'customerFullname',
            'customerCompanyName',
            'customerKraPin',
            //'customerDisplayName',
            //'customerPrimaryPhone',
            //'customerPrimaryEmail:email',
            //'customerstatus',
            //'customercity',
            //'customerPhyscialAdress',
            //'customerCounty',
            //'customerCountry',
            //'customerAddress',
            //'customerCreateTime',
            //'customerUpdatedAt',
            //'customerDeleteAt',
            //'createdBy',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
