<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\moduleslist\models\UserModulesList */

// $this->title = 'Update User Modules List: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Modules Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-modules-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
