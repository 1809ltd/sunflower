<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceTax */
/* @var $form yii\widgets\ActiveForm */

/*Getting the Chart of Accounts details  */
use app\models\FinanceAccounts;
use app\models\FinanceAccountsSearch;
/*Getting the Select2 to work */
use kartik\select2\Select2;


?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="finance-tax-form">

                <?php $form = ActiveForm::begin(); ?>



                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'taxCode')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'taxRate')->textInput() ?>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'account')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(FinanceAccounts::find()->all(),'accountName','fullyQualifiedName'),
                        'language' => 'en',
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'taxType')->dropDownList($status, ['prompt'=>'Select Status']);?>

                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $taxStatus = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'taxStatus')->dropDownList($taxStatus, ['prompt'=>'Select Status']);?>

                  </div>
                </div>



                <div class="col-sm-3">
                  <div class="form-group">
                  <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                  </div>
                </div>

              </div>

              <?php ActiveForm::end(); ?>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
