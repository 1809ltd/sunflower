<?php
namespace app\modules\finance\expensepayment\officeexpensepayment\models;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
// Office Requstion been paid for
use app\modules\finance\expense\officeexpense\models\FinanceOfficeRequstionForm;
// Getting relation tables and Model
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLinesSearch;

use Yii;

/**
 * This is the model class for table "finance_office_requisition_payments".
 *
 * @property int $id
 * @property string $officeReqPaymentPayType
 * @property int $officeReqPaymentaccount Account Paid from
 * @property string $reqrecepitNumber
 * @property double $officeReqPaymentTotalAmt
 * @property double $unallocatedAmount
 * @property string $date
 * @property string $officeReqPaymentofficeReqRef
 * @property string $officeReqPaymentPrivateNote
 * @property string $officeReqPaymentCreatedAt
 * @property string $officeReqPaymentUpdatedAt
 * @property string $officeReqPaymentDeletedAt
 * @property int $userId
 * @property int $createdBy
 * @property int $approvedBy
 * @property int $paymentstatus
 *
 * @property FinanceAccounts $officeReqPaymentaccount0
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property FinanceAccounts $approvedBy0
 * @property FinanceOfficeRequisitionPaymentsLines[] $financeOfficeRequisitionPaymentsLines
 */
class FinanceOfficeRequisitionPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_office_requisition_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['officeReqPaymentaccount', 'reqrecepitNumber', 'officeReqPaymentTotalAmt', 'unallocatedAmount', 'date', 'officeReqPaymentofficeReqRef', 'userId', 'createdBy', 'paymentstatus'], 'required'],
            [['officeReqPaymentaccount', 'userId', 'createdBy', 'approvedBy', 'paymentstatus'], 'integer'],
            [['officeReqPaymentTotalAmt', 'unallocatedAmount'], 'number'],
            ['unallocatedAmount', 'match', 'pattern' => '/^[0-9]*$/'],
            ['unallocatedAmount', 'integer', 'min' => 0, 'max' => 1000000],
            [['date', 'officeReqPaymentCreatedAt', 'officeReqPaymentUpdatedAt', 'officeReqPaymentDeletedAt'], 'safe'],
            [['officeReqPaymentPrivateNote'], 'string'],
            [['officeReqPaymentPayType', 'reqrecepitNumber', 'officeReqPaymentofficeReqRef'], 'string', 'max' => 50],
            [['reqrecepitNumber'], 'unique'],
            [['officeReqPaymentaccount', 'officeReqPaymentofficeReqRef'], 'unique', 'message'=>'The account Transaction already exist for this Account. Please try another one.', 'targetAttribute' => ['officeReqPaymentaccount', 'officeReqPaymentofficeReqRef']],
            [['officeReqPaymentaccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['officeReqPaymentaccount' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }
// ,'message' => 'Sorry that Transaction code in that account has already been taken.']
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'officeReqPaymentPayType' => 'Payment Method',
            'officeReqPaymentaccount' => 'Payment Account',
            'reqrecepitNumber' => 'Recepit Number',
            'officeReqPaymentTotalAmt' => 'Value',
            'unallocatedAmount' => 'Unallocated Amount',
            'date' => 'Date',
            'officeReqPaymentofficeReqRef' => 'Transaction Code',
            'officeReqPaymentPrivateNote' => 'Private Note',
            'officeReqPaymentCreatedAt' => 'Created At',
            'officeReqPaymentUpdatedAt' => 'Updated At',
            'officeReqPaymentDeletedAt' => 'Deleted At',
            'userId' => 'User',
            'createdBy' => 'Created By',
            'approvedBy' => 'Approved By',
            'paymentstatus' => 'status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfficeReqPaymentaccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'officeReqPaymentaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequisitionPaymentsLines()
    {
        return $this->hasMany(FinanceOfficeRequisitionPaymentsLines::className(), ['officeReqpaymentId' => 'id']);
    }// generating the Recipet number for the payment
    public function getRcpnumber()
    {
        //select product code
        $connection = Yii::$app->db;
        $query= "Select MAX(reqrecepitNumber) AS number from finance_office_requisition_payments where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...
          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "OREQRCP-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "OREQRCP-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "OREQRCP-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
    // Get all the payments on Hold
    public function getallpaymentslinesonhold()
    {
        $paymentsonHold = FinanceOfficeRequisitionPaymentsLines::find()
            ->joinWith('officeReq')
            // ->where(['finance_office_requisition_payments_lines.vendorId' => $vendorId])
            ->where(['finance_office_requisition_payments_lines.status'=>3])
            ->Andwhere(['finance_office_requisition_payments_lines.userId'=>Yii::$app->user->identity->id])
            ->asArray()
            ->all();
        // print_r($paymentsonHold);
        // Yii::$app->user->identity->id

        return $paymentsonHold;

        // return self::find()
        //       ->select(['id','eventName as name'])
        //       ->where(['id' => $project_id])->indexBy('id')->column();
    }

    // Get payment based on the payment Id and status
    // Get all the payments on Hold
    public function getallpaymentslinesforApayment($id,$status)
    {
      echo "this is the moule";
        $paymentsonHold = FinanceOfficeRequisitionPaymentsLines::find()
            ->joinWith('officeReq')
            ->where(['finance_office_requisition_payments_lines.officeReqpaymentId' => $id])
            ->Andwhere(['finance_office_requisition_payments_lines.status'=>$status])
            ->asArray()
            ->all();
        // print_r($paymentsonHold);
        //
        // die();

        return $paymentsonHold;

    }
}
