<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CompanySiteDetails;

/**
 * CompanySiteDetailsSearch represents the model behind the search form of `app\models\CompanySiteDetails`.
 */
class CompanySiteDetailsSearch extends CompanySiteDetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'companyId', 'siteStatus', 'companyUserId'], 'integer'],
            [['siteName', 'siteAddress', 'siteSuite', 'siteCity', 'siteCounty', 'sitePostal', 'siteCountry', 'siteTimestamp', 'siteUpdatedAt', 'siteDeletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CompanySiteDetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'companyId' => $this->companyId,
            'siteStatus' => $this->siteStatus,
            'siteTimestamp' => $this->siteTimestamp,
            'siteUpdatedAt' => $this->siteUpdatedAt,
            'siteDeletedAt' => $this->siteDeletedAt,
            'companyUserId' => $this->companyUserId,
        ]);

        $query->andFilterWhere(['like', 'siteName', $this->siteName])
            ->andFilterWhere(['like', 'siteAddress', $this->siteAddress])
            ->andFilterWhere(['like', 'siteSuite', $this->siteSuite])
            ->andFilterWhere(['like', 'siteCity', $this->siteCity])
            ->andFilterWhere(['like', 'siteCounty', $this->siteCounty])
            ->andFilterWhere(['like', 'sitePostal', $this->sitePostal])
            ->andFilterWhere(['like', 'siteCountry', $this->siteCountry]);

        return $dataProvider;
    }
}
