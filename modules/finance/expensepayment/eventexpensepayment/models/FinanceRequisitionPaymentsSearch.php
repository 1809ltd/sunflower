<?php

namespace app\modules\finance\expensepayment\eventexpensepayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments;

/**
 * FinanceRequisitionPaymentsSearch represents the model behind the search form of `app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPayments`.
 */
class FinanceRequisitionPaymentsSearch extends FinanceRequisitionPayments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'userId', 'createdBy', 'approvedBy', 'paymentstatus'], 'integer'],
            [['reqpaymentPayType',  'reqpaymentaccount','reqrecepitNumber', 'date', 'reqpaymenteventReqRef', 'reqpaymentPrivateNote', 'reqpaymentCreatedAt', 'reqpaymentUpdatedAt', 'reqpaymentDeletedAt'], 'safe'],
            [['reqpaymentTotalAmt', 'unallocatedAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceRequisitionPayments::find();
        $query->joinWith(['reqpaymentaccount0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'reqpaymentaccount' => $this->reqpaymentaccount,
            'reqpaymentTotalAmt' => $this->reqpaymentTotalAmt,
            'unallocatedAmount' => $this->unallocatedAmount,
            'date' => $this->date,
            'reqpaymentCreatedAt' => $this->reqpaymentCreatedAt,
            'reqpaymentUpdatedAt' => $this->reqpaymentUpdatedAt,
            'reqpaymentDeletedAt' => $this->reqpaymentDeletedAt,
            'userId' => $this->userId,
            'createdBy' => $this->createdBy,
            'approvedBy' => $this->approvedBy,
            'paymentstatus' => $this->paymentstatus,
        ]);

        $query->andFilterWhere(['like', 'reqpaymentPayType', $this->reqpaymentPayType])
            ->andFilterWhere(['like', 'reqrecepitNumber', $this->reqrecepitNumber])
                ->andFilterWhere(['like', 'fullyQualifiedName', $this->reqpaymentaccount])
            ->andFilterWhere(['like', 'reqpaymenteventReqRef', $this->reqpaymenteventReqRef])
            ->andFilterWhere(['like', 'reqpaymentPrivateNote', $this->reqpaymentPrivateNote]);

        return $dataProvider;
    }
}
