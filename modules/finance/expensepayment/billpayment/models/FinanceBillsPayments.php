<?php

namespace app\modules\finance\expensepayment\billpayment\models;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\finance\expense\bills\models\FinanceBills;
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;

use Yii;

/**
 * This is the model class for table "finance_bills_payments".
 *
 * @property int $id
 * @property string $billPaymentPayType
 * @property int $billPaymentaccount
 * @property double $billPaymentTotalAmt
 * @property double $unallocatedAmount
 * @property string $date
 * @property string $billPaymentBillRef
 * @property int $vendorId
 * @property string $billPaymentPrivateNote
 * @property string $billPaymentCreatedAt
 * @property string $billPaymentUpdatedAt
 * @property string $billPaymentDeletedAt
 * @property int $userId
 * @property int $createdBy
 * @property int $approvedBy
 * @property int $paymentstatus
 *
 * @property FinanceAccounts $billPaymentaccount0
 * @property UserDetails $user
 * @property VendorCompanyDetails $vendor
 * @property UserDetails $createdBy0
 * @property UserDetails $approvedBy0
 * @property FinanceBillsPaymentsLines[] $financeBillsPaymentsLines
 */
class FinanceBillsPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_bills_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billPaymentaccount', 'billPaymentTotalAmt', 'unallocatedAmount', 'date', 'billPaymentBillRef', 'userId', 'createdBy', 'paymentstatus'], 'required'],
            [['billPaymentaccount', 'vendorId', 'userId', 'createdBy', 'approvedBy', 'paymentstatus'], 'integer'],
            [['billPaymentTotalAmt', 'unallocatedAmount'], 'number'],
            ['unallocatedAmount', 'match', 'pattern' => '/^[0-9]*$/'],
            ['unallocatedAmount', 'integer', 'min' => 0, 'max' => 1000000],
            [['date', 'billPaymentCreatedAt', 'billPaymentUpdatedAt', 'billPaymentDeletedAt'], 'safe'],
            [['billPaymentPrivateNote'], 'string'],
            [['billPaymentPayType', 'billPaymentBillRef'], 'string', 'max' => 50],
            [['billPaymentaccount', 'billPaymentBillRef'], 'unique', 'message'=>'The account Transaction already exist for this Account. Please try another one.', 'targetAttribute' => ['billPaymentaccount', 'billPaymentBillRef']],
            [['billPaymentaccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['billPaymentaccount' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['vendorId'], 'exist', 'skipOnError' => true, 'targetClass' => VendorCompanyDetails::className(), 'targetAttribute' => ['vendorId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'billPaymentPayType' => 'Payment Method',
          'billPaymentaccount' => 'Payment Account',
          'billPaymentTotalAmt' => 'Total Amount',
          'unallocatedAmount' => 'Unallocated Amount',
          'date' => 'Date',
          'billPaymentBillRef' => 'Cheque No/Transacrion Code',
          'vendorId' => 'Vendor',
          'billPaymentPrivateNote' => 'Private Note',
          'billPaymentCreatedAt' => 'Bill Payment Created At',
          'billPaymentUpdatedAt' => 'Bill Payment Updated At',
          'billPaymentDeletedAt' => 'Bill Payment Deleted At',
          'userId' => 'User ID',
          'createdBy' => 'Created By',
          'approvedBy' => 'Approved By',
          'paymentstatus' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillPaymentaccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'billPaymentaccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(VendorCompanyDetails::className(), ['id' => 'vendorId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsPaymentsLines()
    {
        return $this->hasMany(FinanceBillsPaymentsLines::className(), ['billpaymentId' => 'id']);
    }
    /*Get Hold Asset*/

    public function getallpaymentslinesonhold($vendorId)
    {
        $paymentsonHold = FinanceBillsPaymentsLines::find()
            ->joinWith('bill')
            ->where(['finance_bills_payments_lines.vendorId' => $vendorId])
            ->Andwhere(['finance_bills_payments_lines.status'=>3])
            ->Andwhere(['finance_bills_payments_lines.userId'=>Yii::$app->user->identity->id])
            ->asArray()
            ->all();
        // print_r($paymentsonHold);

        return $paymentsonHold;

        // return self::find()
        //       ->select(['id','eventName as name'])
        //       ->where(['id' => $project_id])->indexBy('id')->column();
    }

    public function getbillpaymentItems($id,$paymentstatus)
    {
      return FinanceBillsPaymentsLines::find()
          ->joinWith('bill')
          ->where(['finance_bills_payments_lines.billpaymentId' => $id])
          ->Andwhere(['finance_bills_payments_lines.status'=>$paymentstatus])
          ->all();
    }
}
