<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\models\FinanceCustomerPurchaseOrder;
use app\models\FinanceCustomerPurchaseOrderSearch;
use app\models\FinanceCustomerPurchaseOrderLines;
use app\models\FinanceCustomerPurchaseOrderLinesSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceCustomerPurchaseOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Purchase Orders';
$this->params['breadcrumbs'][] = $this->title;

?>


<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="finance-customer-purchase-order-index">
                <div class="table-responsive">
                  <p>
                      <?= Html::a('Add Customer Purchase Order', ['create'], ['class' => 'btn btn-success']) ?>
                  </p>
              <?php Pjax::begin(); ?>

                  <?=  GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
                          'rowOptions'=> function($model)
                          {
                            // code...
                            if ($model->purchaseOrderTxnStatus==1) {
                              // code...
                              return['class'=>'success'];
                            } else {
                              // code...
                              return['class'=>'danger'];
                            }

                          },
                          'columns' => [
                              ['class' => 'kartik\grid\ExpandRowColumn',
                                'value'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  return GridView::ROW_COLLAPSED;
                                },
                                'detail'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  $searchModel = new FinanceCustomerPurchaseOrderLinesSearch();
                                  $searchModel->id = $model->id;
                                  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                                  return Yii::$app->controller->renderPartial('customerpodetails',[
                                    'searchModel' => $searchModel,
                                    'dataProvider' => $dataProvider,

                                  ]);
                                },
                            ],

                            // 'id',
                            'purchaseOrderNumber',
                            'customerId',
                            'purchaseOrderProjectClassification',
                            // 'purchaseOrderProjectId',
                            'purchaseOrderProjectName',
                            'purchaseOrderNote:ntext',
                            'purchaseOrderTxnDate',
                            //'purchaseOrderBillEmail:ntext',
                            'purchaseOrderDiscountAmount',
                            'purchaseOrderTaxAmount',
                            'purchaseOrderSubAmount',
                            'purchaseOrderAmount',
                            //'purchaseOrderTxnStatus:ntext',
                            //'purchaseOrderdCreatedby',
                            //'purchaseOrderdCreatedAt',
                            //'purchaseOrderdUpdatedAt',
                            //'purchaseOrderdDeletedAt',


                            ['class' => 'yii\grid\ActionColumn'],
                          ],
                      ]);
                      ?>

                </div>

                <?php Pjax::end(); ?>
             </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
