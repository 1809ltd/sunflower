<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\transfer\models\FinanceTransfereeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-transferee-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'transferid') ?>

    <?= $form->field($model, 'transfereeRefernce') ?>

    <?= $form->field($model, 'transfereeFromAccountRef') ?>

    <?= $form->field($model, 'transfereeToAccountRef') ?>

    <?php // echo $form->field($model, 'transfereeAmount') ?>

    <?php // echo $form->field($model, 'transferCharges') ?>

    <?php // echo $form->field($model, 'transfereeTxnDate') ?>

    <?php // echo $form->field($model, 'transfereeCreatedAt') ?>

    <?php // echo $form->field($model, 'transfereeUpdatedAt') ?>

    <?php // echo $form->field($model, 'transfereeDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
