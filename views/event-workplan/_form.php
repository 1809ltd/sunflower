<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\EventOrderfromList;
use app\models\EventOrderfrom;
use app\models\PersonnelDetails;
use kartik\select2\Select2;


/*Dependant Drop*/
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\EventWorkplan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<div class="event-workplan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php

    // Top most parent
    // $orderId= ArrayHelper::map(EventOrderfrom::find()->where('`orderStatus` = 1')->asArray()->all(),'id','orderNumber');

    ?>
    <?= $form->field($model, 'orderId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(EventOrderfrom::find()->where('`orderStatus` = 1')->asArray()->all(),'id','orderNumber'),
        'language' => 'en',
        'options' => ['id'=>'orderId','prompt'=>'Select...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

      <?= $form->field($model, 'orderformId')->widget(DepDrop::classname(), [
          'options'=>['id'=>'orderformId'],
          'data' => [$model->orderformId => $model->orderformId],
          'type' => DepDrop::TYPE_SELECT2,
          'pluginOptions'=>[
              'depends'=>['orderId'],
              'initialize' => true,
              // 'initDepends'=>['orderId'],
              'placeholder'=>'Select...',
              'url'=>Url::to(['/event-orderfrom-list/orderdetails'])
          ]
      ]);?>

      <?= $form->field($model, 'taskAssign')->widget(DepDrop::classname(), [
          'type' => DepDrop::TYPE_SELECT2,
          'data' => [$model->taskAssign => $model->taskAssign],
          'pluginOptions'=>[
              'depends'=>['orderId', 'orderformId'],
              'initialize' => true,
              'initDepends'=>['orderId', 'orderformId'],
              'placeholder'=>'Select...',
              'url'=>Url::to(['/event-orderfrom-list/jobname'])
          ]
      ]); ?>

      <?= $form->field($model, 'personnelid')->widget(Select2::classname(), [
          'data' => ArrayHelper::map(PersonnelDetails::find()->all(),'id','displayName'),
          'language' => 'en',
          'options' => ['placeholder' => 'Select a Personnel ...'],
          'pluginOptions' => [
              'allowClear' => true
          ],
      ]);?>

      <?= $form->field($model, 'personnelPhone')->textInput(['maxlength' => true]) ?>

      <?php $category = ['Permanent' => 'Permanent','On Site Casual' => 'On Site Casual','off Site Casual' => 'off Site Casual',]; ?>
      <?= $form->field($model, 'category')->dropDownList($category, ['prompt'=>'Select Category','class'=>'form-control']);?>

      <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

      <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
      <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status','class'=>'form-control']);?>


      <div class="box-footer">
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
      </div>


    <?php ActiveForm::end(); ?>

</div>
