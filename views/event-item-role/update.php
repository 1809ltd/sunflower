<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventItemRole */

$this->title = 'Update Event Item Role: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Item Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-item-role-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
