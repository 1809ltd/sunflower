<?php

namespace app\modules\finance\transfer\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\transfer\models\FinanceTransfer;

/**
 * FinanceTransferSearch represents the model behind the search form of `app\modules\finance\transfer\models\FinanceTransfer`.
 */
class FinanceTransferSearch extends FinanceTransfer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','userId', 'status', 'createdBy'], 'integer'],
            [[ 'tranferFromAccountRef', 'tranferToAccountRef', 'tranferRefernce', 'tranferTxnDate', 'tranferCreatedAt', 'tranferUpdatedAt', 'tranferDeletedAt'], 'safe'],
            [['tranferAmount', 'transferCharges'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceTransfer::find();
        $query->joinWith(['tranferFromAccountRef0']);
        // $query->joinWith(['tranferToAccountRef0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'tranferFromAccountRef' => $this->tranferFromAccountRef,
            // 'tranferToAccountRef' => $this->tranferToAccountRef,
            'tranferAmount' => $this->tranferAmount,
            'transferCharges' => $this->transferCharges,
            'tranferTxnDate' => $this->tranferTxnDate,
            'tranferCreatedAt' => $this->tranferCreatedAt,
            'tranferUpdatedAt' => $this->tranferUpdatedAt,
            'tranferDeletedAt' => $this->tranferDeletedAt,
            'userId' => $this->userId,
            'status' => $this->status,
            'createdBy' => $this->createdBy,
        ]);

        $query->andFilterWhere(['like', 'tranferRefernce', $this->tranferRefernce])
        ->andFilterWhere(['like', 'fullyQualifiedName', $this->tranferFromAccountRef])
        ->andFilterWhere(['like', 'fullyQualifiedName', $this->tranferToAccountRef]);

        return $dataProvider;
    }
}
