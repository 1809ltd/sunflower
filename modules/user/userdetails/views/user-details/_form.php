<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userdetails\models\UserDetails */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body user-details-form">
          <div class="row">
            <!-- <div class="col-md-6"> -->
            <?php $form = ActiveForm::begin(); ?>

            


              <div class="col-md-6 form-group">
                <?= $form->field($model, 'userFName')->textInput(['maxlength' => true]) ?>
              </div>
              <!-- /.form-group -->
              <div class="col-md-6 form-group">
                <?= $form->field($model, 'userLName')->textInput(['maxlength' => true]) ?>
              </div>
              <!-- /.form-group -->
            <!-- </div> -->
            <!-- /.col -->
            <div class="col-md-6 form-group">
              <?= $form->field($model, 'userPhone')->textInput(['maxlength' => true]) ?>
            </div>
            <!-- /.form-group -->
            <div class="col-md-6 form-group">
              <?= $form->field($model, 'userEmail')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6 form-group">
              <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <!-- /.form-group -->
            <div class="col-md-6 form-group">
              <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-6 form-group">

              <?= $form->field($model, 'userStaffId')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6 form-group">

                <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                <?= $form->field($model, 'userStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>
            </div>
            <!-- /.form-group -->
            <div class="col-md-6 form-group">
              <?= $form->field($model, 'userImage')->textInput(['maxlength' => true]) ?>
            </div>

            <!-- /.col -->

            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

            <?php ActiveForm::end(); ?>

      </div>
