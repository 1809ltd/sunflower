<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetDispose */

$this->title = 'Update Asset Dispose: ' . $model->disposeId;
$this->params['breadcrumbs'][] = ['label' => 'Asset Disposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->disposeId, 'url' => ['view', 'id' => $model->disposeId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-dispose-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
