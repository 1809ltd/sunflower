<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceLeadRequisitionPaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lead Requisition Payments';
$this->params['breadcrumbs'][] = $this->title;
?>



<!-- SELECT2 EXAMPLE -->
<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="finance-lead-requisition-payments-index">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p class="pull-right">
            <?= Html::a('Record Requisition Payments', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'reqrecepitNumber',
                'reqpaymentName',
                'reqpaymentAccount',
                'reqpaymentTxnDate',
                //'reqpaymentType',
                //'reqpaymentRefence',
                //'reqpaymentTotalAmt',
                //'reqpaymentProcessduration',
                //'reqpaymentLinkedTxn',
                //'reqpaymentLinkedTxnType',
                //'reqpaymentStatus',
                //'reqpaymentCreateAt',
                //'reqpaymentUpdatedAt',
                //'reqpaymentDeletedAt',
                //'userId',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>


  </div>
  <!-- /.box-body -->
  <div class="box-footer">

  </div>

</div>
    <!-- /.box -->
