<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orderformdetails;

/**
 * OrderformdetailsSearch represents the model behind the search form of `app\models\Orderformdetails`.
 */
class OrderformdetailsSearch extends Orderformdetails
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
          [['id', 'customerId', 'eventCategory', 'userId', 'orderformid', 'eventId', 'orderStatus', 'personIncharge', 'orderformuserid', 'orderId', 'orderformlistid', 'itemid', 'outsourced', 'vendorId', 'status', 'orderformlistuser', 'approvedBy'], 'integer'],
          [['eventVistorsNumber', 'quantity'], 'number'],
          [['eventStartDate', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventCreatedAt', 'eventLastUpdatedAt', 'eventDeletedAt', 'date', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
          [['eventDescription', 'eventNotes', 'orderNote', 'details'], 'string'],
          [['eventNumber', 'eventName', 'eventTheme', 'orderNumber'], 'string', 'max' => 100],
          [['eventLocation'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orderformdetails::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //
        //
        //     'id'=> $this->id,
        //     'customerId'=> $this->customerId,
        //     'eventNumber'=> $this->eventNumber,
        //     'eventName'=> $this->eventName,
        //     'eventTheme'=> $this->eventTheme,
        //     'eventVistorsNumber'=> $this->,
        //     'eventCategory'=> $this->,
        //     'eventStartDate'=> $this->,
        //     'eventEndDate'=> $this->,
        //     'eventSetUpDateTime'=> $this->,
        //     'eventSetDownDateTime'=> $this->,
        //     'eventLocation'=> $this->,
        //     'eventDescription'=> $this->,
        //     'eventNotes'=> $this->,
        //     'eventCreatedAt'=> $this->,
        //     'eventLastUpdatedAt'=> $this->,
        //     'eventDeletedAt'=> $this->,
        //     'userId'=> $this->,
        //     'orderformid'=> $this->,
        //     'orderNumber'=> $this->,
        //     'eventId'=> $this->,
        //     'date'=> $this->date,
        //     'orderNote'=> $this->,
        //     'personIncharge'=> $this->,
        //     'orderformuserid'=> $this->,
        //     'orderId'=> $this->,
        //     'orderformlistid'=> $this->,
        //     'itemid'=> $this->,
        //     'details'=> $this->,
        //     'outsourced'=> $this->,
        //     'vendorId'=> $this->,
        //     'quantity'=> $this->,
        //     'status'=> $this->status,
        //     'orderformlistuser'=> $this->orderformlistuser,
        //
        // ]);

        // $query->andFilterWhere(['like', 'nationalId', $this->nationalId])
        //     ->andFilterWhere(['like', 'displayName', $this->displayName])
        //     ->andFilterWhere(['like', 'firstName', $this->firstName])
        //     ->andFilterWhere(['like', 'sName', $this->sName])
        //     ->andFilterWhere(['like', 'lName', $this->lName])
        //     ->andFilterWhere(['like', 'primaryPhone', $this->primaryPhone])
        //     ->andFilterWhere(['like', 'secondaryPhone', $this->secondaryPhone])
        //     ->andFilterWhere(['like', 'primaryAddr', $this->primaryAddr])
        //     ->andFilterWhere(['like', 'town', $this->town])
        //     ->andFilterWhere(['like', 'area', $this->area])
        //     ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
