<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItems */

$this->title = 'Update Product or Service: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product or Service', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-items-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
