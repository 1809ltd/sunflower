<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AssetDispose */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-dispose-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'assetId')->textInput() ?>

    <?= $form->field($model, 'dateDisposed')->textInput() ?>

    <?= $form->field($model, 'disposeTo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'disposal Reason')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'disposeTimeStamp')->textInput() ?>

    <?= $form->field($model, 'disposeUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'disposeDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
