<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\modules\company\companydetails\models\CompanyDetails */

$this->title = $model->companyName;
$this->params['breadcrumbs'][] = ['label' => 'Company Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
// \yii\web\YiiAsset::register($this);
?>

<p>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
    ]) ?>
</p>

<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">

      </div>
    </div>
  </div>

  <div class="box-body company-details-view">
    <div class="row mix-grid">
        <div class="col-md-3" id="main_image">
            <div>
                <div>
                    <div class="mix-inner">
                      <img class="img-responsive pad" src="<?=Url::to('@web/'.$model->companyLogo); ?>" alt="Photo">
                    </div>
                </div>
            </div>
        </div>
    <div class="col-md-4">
        <table class="table table-bordered table-asset" style="margin-bottom:0 !important;">
            <tbody>
                <tr><td>Company Address</td><td><?=$model->companyAddress;?></td></tr>
                    <tr>
                        <td>Purchase Date</td>
                        <td>2018-12-12</td>
                    </tr>
                    <tr><td>Suite</td><td><?= $model->companySuite?></td></tr>
                    <tr><td>County</td><td><?= $model->companyCounty?></td></tr>

                    <tr><td>Recorded </td><td><?= $model->companyTimestamp?></td></tr>
                    <tr><td>Updated</td><td><?= $model->companyupdatedAt?></td></tr>
                    <tr><td>Created By</td><td>1</td></tr>

            </tbody>
        </table>
    </div>
    <div class="col-md-5">
        <table class="table table-bordered table-asset" style="margin-bottom:0 !important;">
            <tbody>
                <tr>
                    <td>Postal</td>
                    <td><?=$model->companyPostal?></td>
                </tr>
                <tr><td>City</td><td><?=$model->companyCity?></td></tr>
                <tr><td>Country</td><td><?=$model->companyCountry?></td></tr>
                <tr><td>Created By</td><td><?= $model->companyUserId?></td></tr>
                <tr>
                    <td>Status</td>

                                                                        <td class="success">
                          Active
                      </td>


                </tr>
            </tbody>
        </table>
    </div>
    </div>


    <?php
    //  DetailView::widget([
    //     'model' => $model,
    //     'attributes' => [
    //         'id',
    //         'companyName',
    //         'companyAddress',
    //         'companySuite',
    //         'companyCity',
    //         'companyCounty',
    //         'companyPostal',
    //         'companyCountry',
    //         'companyLogo:ntext',
    //         'companyStatus',
    //         'companyTimestamp',
    //         'companyupdatedAt',
    //         'companyDeletedAt',
    //         'companyUserId',
    //         // 'companyuser.userFName'
    //     ],
    // ]) ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
