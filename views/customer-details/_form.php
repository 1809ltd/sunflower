<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\customer\customerdetails\models\CustomerDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customerNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerFullname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerCompanyName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerKraPin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerDisplayName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerPrimaryPhone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerPrimaryEmail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerstatus')->textInput() ?>

    <?= $form->field($model, 'customercity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerPhyscialAdress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerCounty')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerCountry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerAddress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerCreateTime')->textInput() ?>

    <?= $form->field($model, 'customerUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'customerDeleteAt')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
