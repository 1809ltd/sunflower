<?php

namespace app\modules\event\transportation\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\event\transportation\models\EventTransport;

/**
 * EventTransportSearch represents the model behind the search form of `app\modules\event\transportation\models\EventTransport`.
 */
class EventTransportSearch extends EventTransport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'outsourced', 'assetId', 'vendorId', 'delivery', 'collection', 'status', 'userId', 'approvedBy', 'createdBy'], 'integer'],
            [['customerId', 'eventId', 'orderId', 'numberPlate', 'deliveryTime', 'collectionTime', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventTransport::find();
        $query->joinWith(['customer']);
        $query->joinWith(['event']);
        $query->joinWith(['order']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'customerId' => $this->customerId,
            // 'eventId' => $this->eventId,
            // 'orderId' => $this->orderId,
            'outsourced' => $this->outsourced,
            'assetId' => $this->assetId,
            'vendorId' => $this->vendorId,
            'delivery' => $this->delivery,
            'collection' => $this->collection,
            'deliveryTime' => $this->deliveryTime,
            'collectionTime' => $this->collectionTime,
            'status' => $this->status,
            'userId' => $this->userId,
            'approvedBy' => $this->approvedBy,
            'createdBy' => $this->createdBy,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'numberPlate', $this->numberPlate])
        ->andFilterWhere(['like', 'eventName', $this->eventId])
        ->andFilterWhere(['like', 'orderNumber', $this->orderId])
        ->andFilterWhere(['like', 'customerDisplayName', $this->customerId]);

        return $dataProvider;
    }
}
