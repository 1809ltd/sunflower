<?php
namespace app\modules\finance\expensepayment\billpayment\controllers;

use Yii;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*Getting the Lines relationship*/

/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Payment Lines Items*/
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLinesSearch;
/**
 * FinanceBillsPaymentsController implements the CRUD actions for FinanceBillsPayments model.
 */
class FinanceBillsPaymentsController extends Controller
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }
  /*Getting the time updated*/
  public function beforeSave() {
    if ($this->isNewRecord) {
      // code...
      $this->billPaymentUpdatedAt = new \yii\db\Expression('NOW()');
    }else {
      // code...
      $this->billPaymentUpdatedAt = new \yii\db\Expression('NOW()');
    }
    return parent::beforeSave();
  }
  /**
  * Lists all FinanceBillsPayments models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->layout = '@app/views/layouts/addformdatatablelayout';
      $searchModel = new FinanceBillsPaymentsSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

  /**
     * Displays a single FinanceBillsPayments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceBillsPayments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
     {
         $this->layout = '@app/views/layouts/addformdatatablelayout';
         $model = new FinanceBillsPayments();
          // Validate if all the data is inserted
         if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
           Yii :: $app->response->format = 'json';
           return \yii\bootstrap\ActiveForm::validate($model);
         }

         //checking if its a post
         if ($model->load(Yii::$app->request->post())) {
           // code...
           //Getting user Log in details
           $model->userId = Yii::$app->user->identity->id;
           $model->createdBy= Yii::$app->user->identity->id;
           $model->billPaymentCreatedAt= new \yii\db\Expression('NOW()');
           // $deliveryNumber=$model->getDevnumber();
           // $model->deliveryNumber= $deliveryNumber;
           // print_r($model->attributes);
           // die();
           $model->save();
           // code...
           if (!empty($model->id)) {
             // code...
             $billpaymentlines = FinanceBillsPaymentsLines::find()
                                           ->Where('status = :status', [':status' => 3])
                                           ->Andwhere(['finance_bills_payments_lines.userId'=>Yii::$app->user->identity->id])
                                           ->all();

             foreach ($billpaymentlines as $billpaymentlines) {
               // code...
               // Update all the data based on the payment made and status
               FinanceBillsPaymentsLines::findOne($billpaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus,'billpaymentId' =>$model->id]);
             }
             //after Saving the Payment Items Details
             /*After an Update to check out table*/
             // return $this->redirect(['view', 'id' => $model->id]);
             return $this->redirect(['create']);
           }
         } else {
           // code...
           return $this->render('create', [
             'model' => $model,
           ]);
         }
     }

     /**
      * Updates an existing FinanceBillsPayments model.
      * If update is successful, the browser will be redirected to the 'view' page.
      * @param integer $id
      * @return mixed
      * @throws NotFoundHttpException if the model cannot be found
      */
     public function actionUpdate($id)
     {
       $this->layout = '@app/views/layouts/addformdatatablelayout';
       $model = $this->findModel($id);

       // Validate if all the data is inserted
       if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
         Yii :: $app->response->format = 'json';
         return \yii\bootstrap\ActiveForm::validate($model);
       }

       //checking if its a post
       if ($model->load(Yii::$app->request->post())) {
         // code...
         //Getting user Log in details
         $model->userId = Yii::$app->user->identity->id;

         $model->save();
         // code...

         if (!empty($model->id)) {
           // code...
           $billpaymentlines = FinanceBillsPaymentsLines::find()
                                         ->Where('billpaymentId = :billpaymentId', [':billpaymentId' => $model->id])
                                         ->all();

          foreach ($billpaymentlines as $billpaymentlines) {
            // code...
            // Update all the data based on the payment made and status
            FinanceBillsPaymentsLines::findOne($billpaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus]);
          }
          //after Saving the Payment Items Details
          /*After an Update to check out table*/
          return $this->redirect(['view', 'id' => $model->id]);
          // return $this->redirect(['create']);
        }
      } else {
        // code...
        return $this->render('update', [
          'model' => $model,
        ]);
      }
    }
    // public function actionCreate()
    // {
    //   $this->layout = '@app/views/layouts/addformdatatablelayout';
    //   $model = new FinanceBillsPayments();
    //
    //   //checking if its a post
    //   if ($model->load(Yii::$app->request->post())) {
    //     // code...
    //
    //     //Getting user Log in details
    //     $model->userId = Yii::$app->user->identity->id;
    //     $model->createdBy= Yii::$app->user->identity->id;
    //
    //     $model->billPaymentCreatedAt= date('Y-m-d H:i:s');
    //
    //     // $deliveryNumber=$model->getDevnumber();
    //     // $model->deliveryNumber= $deliveryNumber;
    //
    //     // print_r($model->attributes);
    //     // die();
    //
    //     $model->save();
    //     // code...
    //
    //     if (!empty($model->id)) {
    //       // code...
    //       $billpaymentlines = FinanceBillsPaymentsLines::find()
    //                                   ->Where('vendorId = :vendorId', [':vendorId' => $model->vendorId])
    //                                   ->andWhere('status = :status', [':status' => 3])
    //                                   ->all();
    //
    //       foreach ($billpaymentlines as $billpaymentlines) {
    //         // code...
    //         // Update all the data based on the payment made and status
    //         FinanceBillsPaymentsLines::findOne($billpaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus,'billpaymentId' =>$model->id]);
    //       }
    //     } else {
    //       // code...
    //
    //     }
    //
    //     //after Saving the Payment Items Details
    //     /*After an Update to check out table*/
    //     return $this->redirect(['index']);
    //
    //   } else {
    //     // code...
    //
    //     return $this->redirect(['index']);
    //     //load the create form
    //     // return $this->renderAjax('create', [
    //     //   'model' => $model,
    //     // ]);
    //   }
    // }
    //
    // /**
    //  * Updates an existing FinanceBillsPayments model.
    //  * If update is successful, the browser will be redirected to the 'view' page.
    //  * @param integer $id
    //  * @return mixed
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    //  public function actionUpdate($id)
    //  {
    //    // $this->layout='addformdatatablelayout';
    //    $this->layout = '@app/views/layouts/addformdatatablelayout';
    //    $model = $this->findModel($id);
    //
    //    $searchModel = new FinanceBillsPaymentsSearch();
    //    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    //
    //    //Getting user Log in details
    //    $model->userId = Yii::$app->user->identity->id;
    //
    //    if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //      // Confriming that the post is not empty
    //      if (!empty($model->id)) {
    //        // code...
    //        $billpaymentlines = FinanceBillsPaymentsLines::find()
    //        ->Where('billpaymentId = :billpaymentId', [':billpaymentId' => $model->id])
    //        // ->andWhere('status = :status', [':status' => 3])
    //        ->all();
    //
    //        foreach ($billpaymentlines as $billpaymentlines) {
    //          // code...
    //          // Update all the data based on the payment made and status
    //          FinanceBillsPaymentsLines::findOne($billpaymentlines["id"])->updateAttributes(['status'=>$model->paymentstatus,'billpaymentId' =>$model->id]);
    //        }
    //      }
    //
    //      return $this->redirect(['view', 'id' => $model->id]);
    //    }
    //    return $this->render('update', [
    //      'searchModel' => $searchModel,
    //      'dataProvider' => $dataProvider,
    //      'model' => $model,
    //    ]);
    //  }

    /**
     * Deletes an existing FinanceBillsPayments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
     public function actionDelete($id)
     {

       $this->findModel($id)->updateAttributes(['paymentstatus'=>0,'billPaymentDeletedAt' => new \yii\db\Expression('NOW()')]);
       // Confriming that the post is not empty
       if (!empty($id)) {
             // code...
             $billpaymentlines = FinanceBillsPaymentsLines::find()
             ->Where('billpaymentId = :billpaymentId', [':billpaymentId' => $id])
             // ->andWhere('status = :status', [':status' => 3])
             ->all();

             foreach ($billpaymentlines as $billpaymentlines) {
               // code...
               // Update all the data based on the payment made and status
               FinanceBillsPaymentsLines::findOne($billpaymentlines["id"])->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);

             }
           }

       return $this->redirect(['index']);
     }

    /**
     * Finds the FinanceBillsPayments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceBillsPayments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceBillsPayments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
