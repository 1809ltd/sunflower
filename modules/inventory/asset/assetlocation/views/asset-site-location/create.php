<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AssetSiteLocation */

$this->title = 'Create Asset Site Location';
$this->params['breadcrumbs'][] = ['label' => 'Asset Site Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-site-location-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
