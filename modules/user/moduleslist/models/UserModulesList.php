<?php

namespace app\modules\user\moduleslist\models;
use app\modules\user\userdetails\models\UserDetails;


use Yii;

/**
 * This is the model class for table "user_modules_list".
 *
 * @property int $id
 * @property string $moduleName
 * @property string $controller
 * @property string $route
 * @property string $icon
 * @property int $parentId
 * @property int $createdBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $status
 *
 * @property UserDetails $createdBy0
 * @property UserDetails $user
 * @property UserModulesList $parent
 * @property UserModulesList[] $userModulesLists
 * @property UserRoleModulePermission[] $userRoleModulePermissions
 */
class UserModulesList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_modules_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['controller', 'route', 'icon'], 'required'],
            [['parentId', 'createdBy', 'userId', 'status'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['moduleName'], 'string', 'max' => 100],
            [['controller'], 'string', 'max' => 50],
            [['route'], 'string', 'max' => 255],
            [['icon'], 'string', 'max' => 25],
            [['moduleName', 'controller'], 'unique', 'targetAttribute' => ['moduleName', 'controller']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['parentId'], 'exist', 'skipOnError' => true, 'targetClass' => UserModulesList::className(), 'targetAttribute' => ['parentId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'moduleName' => 'Module Name',
            'controller' => 'Controller',
            'route' => 'Route',
            'icon' => 'Icon',
            'parentId' => 'Parent ID',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(UserModulesList::className(), ['id' => 'parentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModulesLists()
    {
        return $this->hasMany(UserModulesList::className(), ['parentId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoleModulePermissions()
    {
        return $this->hasMany(UserRoleModulePermission::className(), ['moduleId' => 'id']);
    }
}
