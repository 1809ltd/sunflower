<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orderformdetails".
 *
 * @property int $id
 * @property int $customerId
 * @property string $eventNumber
 * @property string $eventName
 * @property string $eventTheme
 * @property double $eventVistorsNumber
 * @property int $eventCategory
 * @property string $eventStartDate
 * @property string $eventEndDate
 * @property string $eventSetUpDateTime
 * @property string $eventSetDownDateTime
 * @property string $eventLocation
 * @property string $eventDescription
 * @property string $eventNotes
 * @property string $eventCreatedAt
 * @property string $eventLastUpdatedAt
 * @property string $eventDeletedAt
 * @property int $userId
 * @property int $orderformid
 * @property string $orderNumber
 * @property int $eventId
 * @property string $date
 * @property int $orderStatus
 * @property string $orderNote
 * @property int $personIncharge
 * @property int $orderformuserid
 * @property int $orderId
 * @property int $orderformlistid
 * @property int $itemid
 * @property string $details
 * @property int $outsourced
 * @property int $vendorId
 * @property double $quantity
 * @property int $status
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $orderformlistuser
 * @property int $approvedBy
 */
class Orderformdetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orderformdetails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'customerId', 'eventCategory', 'userId', 'orderformid', 'eventId', 'orderStatus', 'personIncharge', 'orderformuserid', 'orderId', 'orderformlistid', 'itemid', 'outsourced', 'vendorId', 'status', 'orderformlistuser', 'approvedBy'], 'integer'],
            [['customerId', 'eventNumber', 'eventName', 'eventTheme', 'eventVistorsNumber', 'eventCategory', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventLocation', 'eventDescription', 'eventNotes', 'userId', 'orderId', 'itemid', 'details', 'outsourced', 'quantity', 'status', 'orderformlistuser', 'approvedBy'], 'required'],
            [['eventVistorsNumber', 'quantity'], 'number'],
            [['eventStartDate', 'eventEndDate', 'eventSetUpDateTime', 'eventSetDownDateTime', 'eventCreatedAt', 'eventLastUpdatedAt', 'eventDeletedAt', 'date', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['eventDescription', 'eventNotes', 'orderNote', 'details'], 'string'],
            [['eventNumber', 'eventName', 'eventTheme', 'orderNumber'], 'string', 'max' => 100],
            [['eventLocation'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customerId' => 'Customer ID',
            'eventNumber' => 'Event Number',
            'eventName' => 'Event Name',
            'eventTheme' => 'Event Theme',
            'eventVistorsNumber' => 'Event Vistors Number',
            'eventCategory' => 'Event Category',
            'eventStartDate' => 'Event Start Date',
            'eventEndDate' => 'Event End Date',
            'eventSetUpDateTime' => 'Event Set Up Date Time',
            'eventSetDownDateTime' => 'Event Set Down Date Time',
            'eventLocation' => 'Event Location',
            'eventDescription' => 'Event Description',
            'eventNotes' => 'Event Notes',
            'eventCreatedAt' => 'Event Created At',
            'eventLastUpdatedAt' => 'Event Last Updated At',
            'eventDeletedAt' => 'Event Deleted At',
            'userId' => 'User ID',
            'orderformid' => 'Orderformid',
            'orderNumber' => 'Order Number',
            'eventId' => 'Event ID',
            'date' => 'Date',
            'orderStatus' => 'Order Status',
            'orderNote' => 'Order Note',
            'personIncharge' => 'Person Incharge',
            'orderformuserid' => 'Orderformuserid',
            'orderId' => 'Order ID',
            'orderformlistid' => 'Orderformlistid',
            'itemid' => 'Itemid',
            'details' => 'Details',
            'outsourced' => 'Outsourced',
            'vendorId' => 'Vendor ID',
            'quantity' => 'Quantity',
            'status' => 'Status',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'orderformlistuser' => 'Orderformlistuser',
            'approvedBy' => 'Approved By',
        ];
    }
}
