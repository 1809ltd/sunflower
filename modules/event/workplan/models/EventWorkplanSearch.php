<?php

namespace app\modules\event\workplan\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\event\workplan\models\EventWorkplan;

/**
 * EventWorkplanSearch represents the model behind the search form of `app\modules\event\workplan\models\EventWorkplan`.
 */
class EventWorkplanSearch extends EventWorkplan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'userId', 'createdBy', 'approvedId'], 'integer'],
            [['customerId', 'taskAssign', 'eventId', 'orderId', 'orderformId', 'personnelid', 'personnelPhone', 'category', 'detail', 'createdAt', 'updateAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventWorkplan::find();
        $query->joinWith(['orderform.item']);
        $query->joinWith(['personnel']);
        $query->joinWith(['order']);
        $query->joinWith(['event']);
        $query->joinWith(['customer']);
        $query->joinWith(['taskAssign0.role']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'customerId' => $this->customerId,
            // 'eventId' => $this->eventId,
            // 'orderId' => $this->orderId,
            'orderformId' => $this->orderformId,
            // 'personnelid' => $this->personnelid,
            // 'taskAssign' => $this->taskAssign,
            'status' => $this->status,
            'createdAt' => $this->createdAt,
            'updateAt' => $this->updateAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'createdBy' => $this->createdBy,
            'approvedId' => $this->approvedId,
        ]);

        $query->andFilterWhere(['like', 'personnelPhone', $this->personnelPhone])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'customerDisplayName', $this->customerId])
            ->andFilterWhere(['like', 'eventName', $this->eventId])
            ->andFilterWhere(['like', 'orderNumber', $this->orderId])
            ->andFilterWhere(['like', 'itemsName', $this->orderformId])
            ->andFilterWhere(['like', 'roleName', $this->taskAssign])
            ->andFilterWhere(['like', 'displayName', $this->personnelid]);

        return $dataProvider;
    }
}
