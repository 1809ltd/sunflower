<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customerincome".
 *
 * @property int $customerId
 * @property string $customerNumber
 * @property string $customerKraPin
 * @property string $customerDisplayName
 * @property double $invoiceLinesAmount
 * @property double $invoiceLinesTaxamount
 * @property double $invoiceLinesTaxCodeRef
 * @property int $itemRefId
 * @property string $itemRefName
 * @property int $invoiceId
 * @property int $invoicelineId
 * @property string $invoiceLinesDescription
 * @property double $invoiceLinesQty
 * @property double $invoiceLinesUnitPrice
 * @property string $invoiceLinesCreatedAt
 * @property double $invoiceLinesDiscount
 * @property string $txnDate
 * @property int $invoiceLinesStatus
 * @property string $eventNumber
 * @property string $eventCatName
 * @property int $catId
 */
class Customerincome extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'customerincome';
    }

    public $income;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customerId', 'invoiceLinesAmount', 'invoiceLinesTaxamount', 'invoiceLinesTaxCodeRef', 'itemRefId', 'itemRefName', 'invoiceId', 'invoiceLinesDescription', 'invoiceLinesQty', 'invoiceLinesUnitPrice', 'invoiceLinesDiscount', 'txnDate', 'invoiceLinesStatus', 'eventNumber', 'eventCatName'], 'required'],
            [['customerId', 'itemRefId', 'invoiceId', 'invoicelineId', 'invoiceLinesStatus', 'catId'], 'integer'],
            [['invoiceLinesAmount', 'invoiceLinesTaxamount', 'invoiceLinesTaxCodeRef', 'invoiceLinesQty', 'invoiceLinesUnitPrice', 'invoiceLinesDiscount'], 'number'],
            [['invoiceLinesDescription'], 'string'],
            [['invoiceLinesCreatedAt', 'txnDate'], 'safe'],
            [['customerNumber', 'customerKraPin', 'customerDisplayName'], 'string', 'max' => 50],
            [['itemRefName'], 'string', 'max' => 255],
            [['eventNumber', 'eventCatName'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'customerId' => 'Customer ID',
            'customerNumber' => 'Customer Number',
            'customerKraPin' => 'Customer Kra Pin',
            'customerDisplayName' => 'Customer Display Name',
            'invoiceLinesAmount' => 'Invoice Lines Amount',
            'invoiceLinesTaxamount' => 'Invoice Lines Taxamount',
            'invoiceLinesTaxCodeRef' => 'Invoice Lines Tax Code Ref',
            'itemRefId' => 'Item Ref ID',
            'itemRefName' => 'Item Ref Name',
            'invoiceId' => 'Invoice ID',
            'invoicelineId' => 'Invoiceline ID',
            'invoiceLinesDescription' => 'Invoice Lines Description',
            'invoiceLinesQty' => 'Invoice Lines Qty',
            'invoiceLinesUnitPrice' => 'Invoice Lines Unit Price',
            'invoiceLinesCreatedAt' => 'Invoice Lines Created At',
            'invoiceLinesDiscount' => 'Invoice Lines Discount',
            'txnDate' => 'Txn Date',
            'invoiceLinesStatus' => 'Invoice Lines Status',
            'eventNumber' => 'Event Number',
            'eventCatName' => 'Event Cat Name',
            'catId' => 'Cat ID',
        ];
    }
}
