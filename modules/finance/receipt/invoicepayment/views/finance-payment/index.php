<?php
use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;

use kartik\select2\Select2;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;
// use  yii\bootstrap\Modal;
// Payment Model
use app\modules\finance\receipt\invoicepayment\models\FinancePayment;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentSearch;
// Invoices Paid
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLines;
use app\modules\finance\receipt\invoicepayment\models\FinancePaymentLinesSearch;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\receipt\invoicepayment\models\FinancePaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invoice Payments');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

if (isset($_SESSION['customerInvoiceId'])) {
  // code...
  Yii::$app->response->redirect(Url::to(['create'], true));
  // return redirect(['create']);
} else {
  // code...
  ?>
  <!-- SELECT2 EXAMPLE -->
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

      <div class="form-group">
        <?php
        $gridColumns = [
          ['class' => 'yii\grid\SerialColumn'],
          // 'id',
          // 'customerId',
          [
            'attribute'=>'customerId',
            'value'=>'customer.customerDisplayName',
          ],
          'invoicePaymentinvoiceRef',
          [
            'attribute'=>'invoicePaymentaccount',
            'value'=>'invoicePaymentaccount0.fullyQualifiedName',
          ],
          // 'invoicePaymentaccount',
          'invoicePaymentPayType',
          'recepitNumber',
          'invoicePaymentTotalAmt',
          'unallocatedAmount',
          //'date',
          //'invoicePaymentPrivateNote:ntext',
          //'invoicePaymentCreatedAt',
          //'invoicePaymentUpdatedAt',
          //'invoicePaymentDeletedAt',
          //'userId',
          //'createdBy',
          //'approvedBy',
          //'paymentstatus',
          ['class' => 'yii\grid\ActionColumn'],
        ];
        // Renders a export dropdown menu
        echo ExportMenu::widget([
          'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
          'columns' => $gridColumns
        ]);
        // You can choose to render your own GridView separately
        // echo \kartik\grid\GridView::widget([
        //   'dataProvider' => $dataProvider,
        //   'filterModel' => $searchModel,
        //   'columns' => $gridColumns
        // ]);
        ?>
        <p class="pull-right">
          <?= Html::button('Record Payment', ['value'=>Url::to((['/finance/receipt/invoicepayment/default/search'])),'class' => 'btn btn-sm btn-success','id'=>'modalButton']) ?>

          <?php
        	Modal::begin([
        		'header'=>'<h4>Search Customer</h4>',
        		'class'=>'modal-dialog',
        		'id'=>'modal',
        		'size'=>'modal-lg',
        		'options' => [
        				'tabindex' => false // important for Select2 to work properly
        		],
        	]);
        	echo "<div class='modal-content' id='modalContent'></div>";
        	Modal::end();
        	?>

            <?php
             // Html::a('Record Invoice Payments', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div>

      <!-- <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
      </div> -->

        <!-- <p class="pull-right"> -->
          <!-- <?= Html::button('Record Payment', ['value'=>Url::to((['/finance/receipt/invoicepayment/default/search'])),'class' => 'btn btn-sm btn-success','id'=>'modalButton']) ?> -->

          <?php
        	// Modal::begin([
        	// 	'header'=>'<h4>Search Customer</h4>',
        	// 	'class'=>'modal-dialog',
        	// 	'id'=>'modal',
        	// 	'size'=>'modal-lg',
        	// 	'options' => [
        	// 			'tabindex' => false // important for Select2 to work properly
        	// 	],
        	// ]);
        	// echo "<div class='modal-content' id='modalContent'></div>";
        	// Modal::end();
        	?>

            <?php
             // Html::a('Record Invoice Payments', ['create'], ['class' => 'btn btn-success']) ?>
        <!-- </p> -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="finance-payment-index">
          <?php Pjax::begin(); ?>
          <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

          <?= GridView::widget([
              'dataProvider' => $dataProvider,
              'filterModel' => $searchModel,
              'columns' => [
                  // ['class' => 'yii\grid\SerialColumn'],
                  ['class' => 'kartik\grid\ExpandRowColumn',
                    'value'=>function ($model,$key,$index,$column)
                    {
                      // code...
                      return GridView::ROW_COLLAPSED;
                    },
                    'detail'=>function ($model,$key,$index,$column)
                    {
                      // code...
                      $searchModel = new FinancePaymentLinesSearch();
                      $searchModel->invoiceId = $model->id;
                      $searchModel->status = $model->paymentstatus;
                      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                      return Yii::$app->controller->renderPartial('_paymentlines',[
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,

                      ]);
                    },
                ],

                  // 'id',
                  // 'customerId',
                  [
                    'attribute'=>'customerId',
                    'value'=>'customer.customerDisplayName',
                  ],
                  'invoicePaymentinvoiceRef',
                  [
                    'attribute'=>'invoicePaymentaccount',
                    'value'=>'invoicePaymentaccount0.fullyQualifiedName',
                  ],
                  // 'invoicePaymentaccount',
                  'invoicePaymentPayType',
                  'recepitNumber',
                  'invoicePaymentTotalAmt',
                  'unallocatedAmount',
                  //'date',
                  //'invoicePaymentPrivateNote:ntext',
                  //'invoicePaymentCreatedAt',
                  //'invoicePaymentUpdatedAt',
                  //'invoicePaymentDeletedAt',
                  //'userId',
                  //'createdBy',
                  //'approvedBy',
                  //'paymentstatus',

                  /* ... GridView configuration ... */
                  [
                      'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                      'items' => [
                          [
                              'label' => 'View',
                              'url'   => ['view'],
                          ],
                          [
                              'label' => 'Update',
                              'url'   => ['update'],
                              'linkOptions' => [
                                  'data-method' => 'post'
                              ],
                              // 'options'=>['class'=>'update-modal-click grid-action'],
                              // 'update'=>function($url,$model,$key){
                              //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                              //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                              //           'class'=>'update-modal-click grid-action',
                              //           'data-toggle'=>'tooltip',
                              //           'data-placement'=>'bottom',
                              //           'title'=>'Update'
                              //       ]);
                              //       return $btn;
                              // },
                              // 'option' 'class'=>'update-modal-click grid-action',
                              // 'data-toggle'=>'tooltip',
                              // 'data-placement'=>'bottom',
                          ],
                          [
                              'label'   => 'Disable',
                              'url'     => ['delete'],
                              'linkOptions' => [
                                  'data-method' => 'post'
                              ],
                          ],
                      ]
                  ],

                  // ['class' => 'yii\grid\ActionColumn'],
              ],
          ]); ?>
          <?php Pjax::end(); ?>
      </div>


    </div>
    <!-- /.box-body -->
    <div class="box-footer">

    </div>

  </div>
      <!-- /.box -->

  <?php
}
?>
