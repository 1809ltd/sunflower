<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceBillsPayments;

/**
 * FinanceBillsPaymentsSearch represents the model behind the search form of `app\models\FinanceBillsPayments`.
 */
class FinanceBillsPaymentsSearch extends FinanceBillsPayments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'billId', 'userId'], 'integer'],
            [['billPaymentPayType', 'billPaymentaccount', 'billPaymentBillRef', 'billPaymentPrivateNote', 'billPaymentCreatedAt', 'billPaymentUpdatedAt', 'billPaymentDeletedAt'], 'safe'],
            [['billPaymentTotalAmt'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceBillsPayments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'billId' => $this->billId,
            'billPaymentTotalAmt' => $this->billPaymentTotalAmt,
            'billPaymentCreatedAt' => $this->billPaymentCreatedAt,
            'billPaymentUpdatedAt' => $this->billPaymentUpdatedAt,
            'billPaymentDeletedAt' => $this->billPaymentDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'billPaymentPayType', $this->billPaymentPayType])
            ->andFilterWhere(['like', 'billPaymentaccount', $this->billPaymentaccount])
            ->andFilterWhere(['like', 'billPaymentBillRef', $this->billPaymentBillRef])
            ->andFilterWhere(['like', 'billPaymentPrivateNote', $this->billPaymentPrivateNote]);

        return $dataProvider;
    }
}
