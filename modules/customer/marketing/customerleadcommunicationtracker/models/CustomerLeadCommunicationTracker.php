<?php

namespace app\modules\customer\marketing\customerleadcommunicationtracker\models;

use Yii;

/**
 * This is the model class for table "customer_lead_communication_tracker".
 *
 * @property int $id
 * @property int $leadAppointId
 * @property int $leadId
 * @property string $commLeadTxnDate
 * @property string $commLeadMeetingSubject
 * @property string $commLeadPartiesInvolved
 * @property string $commLeadFormOfCommunication
 * @property string $commLeadNoted
 * @property string $commLeadNextDate
 * @property string $commLeadCreatedAt
 * @property string $commLeadLastUpdatedAt
 * @property string $commLeadDeletedAt
 * @property int $userId
 */
class CustomerLeadCommunicationTracker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer_lead_communication_tracker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['leadAppointId', 'leadId', 'commLeadTxnDate', 'commLeadMeetingSubject', 'commLeadPartiesInvolved', 'commLeadFormOfCommunication', 'commLeadNoted', 'commLeadNextDate', 'userId'], 'required'],
            [['leadAppointId', 'leadId', 'userId'], 'integer'],
            [['commLeadTxnDate', 'commLeadNextDate', 'commLeadCreatedAt', 'commLeadLastUpdatedAt', 'commLeadDeletedAt'], 'safe'],
            [['commLeadMeetingSubject', 'commLeadPartiesInvolved', 'commLeadFormOfCommunication', 'commLeadNoted'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Comm Lead',
            'leadAppointId' => 'Appointment',
            'leadId' => 'Project',
            'commLeadTxnDate' => 'Date',
            'commLeadMeetingSubject' => 'Meeting Subject',
            'commLeadPartiesInvolved' => 'Parties Involved',
            'commLeadFormOfCommunication' => 'Form Of Communication',
            'commLeadNoted' => 'Noted',
            'commLeadNextDate' => 'Next Meeting',
            'commLeadCreatedAt' => 'Comm Lead Created At',
            'commLeadLastUpdatedAt' => 'Comm Lead Last Updated At',
            'commLeadDeletedAt' => 'Comm Lead Deleted At',
            'userId' => 'User ID',
        ];
    }
}
