<?php

namespace app\modules\finance\expense\eventexpense;

/**
 * eventexpense module definition class
 */
class eventexpense extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\eventexpense\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->modules =[
          /*Fiance Event expense Lines Sub Module*/
          'eventexpenselines' => [
               'class' => 'app\modules\finance\expense\eventexpense\eventexpenselines\eventexpenselines',
           ],
        ];

        // custom initialization code goes here
    }
}
