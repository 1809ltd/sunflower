<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AssetRegistration */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-registration-view">

</div>


<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Department</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Assets View<small> Asset</small></h4>
            </div>
            <div class="panel-body">

              <div class="page-container">
                  <div class="page-sidebar-wrapper readyPanel2" id="ajax-sidebar-wrapper">

                  </div>
                  <div class="page-content-wrapper">
                      <div class="page-content">

              <div class="FixWidth">
                  <div class="clearfix">
                    <div class="pull-left">
                      <p >
                          <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                          <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                              'class' => 'btn btn-danger',
                              'data' => [
                                  'confirm' => 'Are you sure you want to delete this item?',
                                  'method' => 'post',
                              ],
                          ]) ?>
                      </p>
                    </div>
                      <div class="pull-right">
                              <a href=" " class="btn btn-circle default btn-sm" onclick="$('#spinner').show(0);"> <i class="fa fa-arrow-left"></i> Prev. </a>
                              <form method="post" id="formAssetPdf" name="formAssetPdf" action="">
                                  <input name="__RequestVerificationToken" type="hidden" value="4-HrYPcqABbGFJ2Uon7UCgmFqIR6pxMtqsaGEkrw0Y2o6lmFJwe3kP2vcUBm01ioJWo2xT0PyxglLLgATII9jO4FgzE2Sv-LqK8NegBtVe4uRuE5EcYctssV4hbAsMQGBRVCFw2" />
                                  <button type="submit" class="btn btn-circle btn-sm btn-default"><i class="glyphicon glyphicon-print font-blue"></i>&nbsp; Print</button>
                              </form>
                      </div>
                  </div>
              </div>

                          <div class="mat-filter">

              <div class="FixWidth">
                  <div class="clearfix">

                      <div class="portlet light bg-inverse asset-registration-view">
                          <div class="portlet-title">
                              <div class="caption font-red-sunglo"><div class="form-title"><span><?= $model->assetName; ?></span></div></div>
                              <div class="actions">
                                <div class="title-action-btn">

                                </div>
                              </div>
                          </div>
                          <div class="portlet-body">
                              <div class="row mix-grid">
                                      <div class="col-md-3" id="main_image">
                                          <div>
                                              <div>
                                                  <div class="mix-inner">
                                                      <img class="img-responsive" style="background: #FFF; padding: 10px;" src="<?= $model->assetphoto;?>" alt="" />
                                                  </div>
                                                  <div class="mix-inner">
                                                      <img class="img-responsive" style="background: #FFF; padding: 10px;" src="<?= $model->assetQrCode;?>" alt="" />
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  <div class="col-md-4">
                                      <table class="table table-bordered table-asset" style="margin-bottom:0 !important;">
                                          <tbody>
                                              <tr><td>Asset Tag</td><td><?=$model->assetTag;?></td></tr>
                                                  <tr>
                                                      <td>Purchase Date</td>
                                                      <td><?= $model->assetpurchaseDate;?></td>
                                                  </tr>
                                                  <tr><td>Cost</td><td><?=$model->assetCost;?></td></tr>
                                                  <tr><td>Brand</td><td><?=$model->assetBrand;?></td></tr>
                                                  <tr><td>Model</td><td><?=$model->assetModel;?></td></tr>
                                                  <tr><td>Recorded </td><td><?=$model->assetTimestamp;?></td></tr>
                                                  <tr><td>Updated</td><td><?=$model->assetUpdatedAt;?></td></tr>
                                                  <tr><td>Created By</td><td><?=$model->userId;?></td></tr>

                                          </tbody>
                                      </table>
                                  </div>
                                  <div class="col-md-5">
                                      <table class="table table-bordered table-asset" style="margin-bottom:0 !important;">
                                          <tbody>
                                              <tr>
                                                  <td>Serial Number</td>
                                                  <td>
                                                        <?=$model->assetSerialNumber;?>
                                                  </td>
                                              </tr>
                                              <tr><td>Vendor</td><td><?=$model->vendor;?></td></tr>
                                              <tr><td>Category</td><td><?=$model->assetCategory;?></td></tr>
                                              <tr><td>Category Type</td><td><?=$model->assetCategoryType;?></td></tr>
                                              <tr><td>Created By</td><td><?=$model->assetCategoryType;?></td></tr>
                                              <tr>
                                                  <td>Status</td>

                                                  <?php

                                                  if ($model->assetStatus==1) {
                                                    // code...
                                                    ?>
                                                    <td class="success">
                                                        Active
                                                    </td>

                                                    <?php
                                                  } else {
                                                    // code...
                                                    ?>
                                                    <td class="danger">
                                                        Suspended
                                                    </td>

                                                    <?php
                                                  }


                                                   ?>

                                              </tr>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          <form action="/assettiger/assets/eventCheckOut" class="form-horizontal" id="formEventCheckOut" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="4Iuy0HpqHZqYsHEIihfd1cLtmDW7hYxMPkAVMu4UNHV8Cmewa1I9oQDCSlXj4TMX_2t9n3iezrCVD7j77OA_FbID73pmOe_rEaT8oG9dZ-ZAzo-DVuP--HEEQLHyRHZg6RzLrA2" />                <div class="modal fade " id="event-checkout" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="modal-dialog ">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                          <h4 class="modal-title">Check out</h4>
                                      </div>
                                      <div class="modal-body">
                                          <div class="form-body">
                                              <div class="form-row">
                                                  <div class="col-md-12">
                                                      <div class="form-group">
                                                          <label class="col-md-3 control-label" for="eventCheckOutModel_Date1">Check-out Date </label>
                                                          <div class="col-md-7">
                                                              <div class="input-group date date-picker">
                                                                  <input class="form-control form-control-inline" data-val="true" data-val-date="Check-out date must be a date." placeholder="MM/dd/yyyy" id="eventCheckOutModel_Date1" name="eventCheckOutModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                                  <span class="input-group-btn">
                                                                      <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                  </span>
                                                              </div>
                                                              <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel.Date1" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-md-3 control-label" for="eventCheckOutModel_CheckoutTo">Check-out to</label>
                                                          <div class="col-md-7">
                                                              <div class="radio-list">
                                                                  <label class="radio-inline"><input id="eventCheckOutModel_CheckoutToPerson" type="radio" name="eventCheckOutModel.CheckoutTo" data-val="true" onchange="$('#EventcheckoutToPerson,.checkoutToPersonOption').slideDown(300); $('#eventcheckoutToSiteLocation,.checkoutToSiteLocationOption').slideUp(300);" checked='checked' value="Person"> Person</label>
                                                                  <label class="radio-inline"><input id="eventCheckOutModel_CheckoutToSiteLocation" type="radio" name="eventCheckOutModel.CheckoutTo" data-val="true" onchange="$('#EventcheckoutToPerson,.checkoutToPersonOption').slideUp(300); $('#eventcheckoutToSiteLocation,.checkoutToSiteLocationOption').slideDown(300);" value="SiteLocation"> Site / Location</label>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="form-group" id="EventcheckoutToPerson">
                                                          <label class="col-md-3 control-label" for="eventCheckOutModel_PersonId">Assign to <span class="font-red">*</span></label>
                                                          <div class="col-md-9" id="personsList_ajax_wrapper">
                                                              <div id="personsList_ajax_wrapper" class="input-group">
                                                                  <input class="form-control select2person" data-val="true" data-val-number="The field Assign to must be a number." data-val-range="Person is required." data-val-required="Person is required." id="eventCheckOutModel_PersonId" name="eventCheckOutModel.PersonId" onchange="javascript: fungetCheckOutPersonSiteLocation();funupdateEmail(&#39;eventCheckOutModel_PersonId&#39;, &#39;eventCheckOutModel_PersonEmail&#39;,&#39;formEventCheckOut&#39;);" placeholder="Select Person" type="text" value="" />
                                                                  <div class="input-group-addon" style="padding:0; border:0; background:transparent; padding-left:18px;">
                                                                          <a class="btn btn-default btn-circle" onclick="funPopupAjaxForm('persons/add', 'popup_ajax_wrapper', '', true, '')"><i class="icon-plus"></i> New</a>
                                                                  </div>
                                                              </div>
                                                              <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel.PersonId" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div id="eventcheckoutToSiteLocation" style="display:none">
                                                          <div class="form-group">
                                                              <label class="col-md-3 control-label" for="eventCheckOutModel_SiteId">Site <span class="font-red">*</span></label>
                                                              <div class="col-md-7">
                                                                  <select class="form-control" data-val="true" data-val-number="The field Site must be a number." data-val-range="Site is required." data-val-range-max="9999999" data-val-range-min="1" data-val-required="Site is required." id="eventCheckOutModel_SiteId" name="eventCheckOutModel.SiteId" onchange="funPopupAjaxForm(&#39;locations/list?type=9&#39;, &#39;EventCheckoutToLocationListWrapper&#39;, $(this).val(), false, &#39;&#39;);"><option value="">Select Site</option>
          <option value="199458">Sunning Hills Apartments  (Nairobi, Nairobi, Kenya)</option>
          <option value="199422">Uchumi hse (Kenya)</option>
          </select>
                                                                  <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel.SiteId" data-valmsg-replace="true"></span>
                                                              </div>
                                                          </div>
                                                          <div class="form-group" id="EventCheckoutToLocationListWrapper">
                  <label class="col-md-3 control-label" for="eventCheckOutModel_LocationId">Location</label>
                  <div class="col-md-7">
                      <select class="form-control" data-val-number="Location reference must be a number." id="eventCheckOutModel_LocationId" name="eventCheckOutModel.LocationId">
                          <option value="">Select Location</option>
                      </select>
                      <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel_LocationId" data-valmsg-replace="true"></span>
                  </div>

                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <div class="col-md-7 col-md-offset-3">
                                                              <div class="checkbox-list">
                                                                  <label><input checked="checked" data-val="true" data-val-required="No Due Date is required." id="eventCheckOutModel_hasNoDueDate" name="eventCheckOutModel.hasNoDueDate" onchange="javascript: $(&#39;#checkout_due_date_wrapper&#39;).toggle(0);" type="checkbox" value="true" /><input name="eventCheckOutModel.hasNoDueDate" type="hidden" value="false" /> No due date </label>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="form-group" id="checkout_due_date_wrapper" style="display:none;">
                                                          <label class="col-md-3 control-label" for="eventCheckOutModel_Date2">Due Date</label>
                                                          <div class="col-md-7">
                                                              <div class="input-group date date-picker">
                                                                  <input class="form-control form-control-inline" data-val="true" data-val-date="Due date must be a date." data-val-required="Due date is required." placeholder="MM/dd/yyyy" id="eventCheckOutModel_Date2" name="eventCheckOutModel.Date2" type="text" value="" autocomplete="off">
                                                                  <span class="input-group-btn">
                                                                      <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                  </span>
                                                              </div>
                                                              <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel.Date2" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                          <div class="form-group checkoutToPersonOption"><div class="col-md-9 col-md-offset-3">Optionally change site, location and department of assets to:</div></div>
                                                          <div class="form-group checkoutToSiteLocationOption" style="display:none;"><div class="col-md-9 col-md-offset-3">Optionally change department of assets to:</div></div>
                                                      <div class="form-group checkoutToPersonOption">
                                                          <label class="col-md-3 control-label" for="eventCheckOutModel_newSiteId">Site</label>
                                                          <div class="col-md-7">
                                                              <select class="form-control" data-val="true" data-val-number="The field Site must be a number." id="eventCheckOutModel_newSiteId" name="eventCheckOutModel.newSiteId" onchange="funPopupAjaxForm(&#39;locations/list?type=15&#39;, &#39;eventCheckOutModel_newLocationListWrapper&#39;, $(this).val(), false, &#39;&#39;);"><option value="">Select Site</option>
          <option value="199458">Sunning Hills Apartments  (Nairobi, Nairobi, Kenya)</option>
          <option value="199422">Uchumi hse (Kenya)</option>
          </select>
                                                          </div>
                                                      </div>
                                                      <div class="form-group checkoutToPersonOption">
                                                          <label class="col-md-3 control-label" for="eventCheckOutModel_newLocationId">Location</label>
                                                          <div class="col-md-7" id="eventCheckOutModel_newLocationListWrapper">
                  <select class="form-control" data-val-number="Location reference must be a number." id="eventCheckOutModel_newLocationId" name="eventCheckOutModel.newLocationId">
                      <option value="">Select Location</option>
                  </select>
                  <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel_newLocationId" data-valmsg-replace="true"></span>

                                                          </div>
                                                      </div>
                                                          <div class="form-group">
                                                              <label class="col-md-3 control-label" for="eventCheckOutModel_newDepartmentId">Department</label>
                                                              <div class="col-md-7">
                                                                  <select class="form-control" data-val="true" data-val-number="The field Department must be a number." id="eventCheckOutModel_newDepartmentId" name="eventCheckOutModel.newDepartmentId"><option value="">Select Department</option>
          <option value="3642785">IT</option>
          </select>
                                                                  <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel.newDepartmentId" data-valmsg-replace="true"></span>
                                                              </div>
                                                          </div>
                                                      <div class="form-group checkoutToPersonOption">
                                                          <div class="col-md-7 col-md-offset-3"><button type="button" class="btn btn-default btn-xs pull-right btn-circle" onclick="$('#eventCheckOutModel_newSiteId').val(''); $('#eventCheckOutModel_newSiteId').trigger('change'); $('#eventCheckOutModel_newDepartmentId').val('');">&nbsp; Reset &nbsp;</button></div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-md-3 control-label" for="eventCheckOutModel_Notes">Notes</label>
                                                          <div class="col-md-8">
                                                              <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventCheckOutModel_Notes" maxlength="1000" name="eventCheckOutModel.Notes" rows="4">
          </textarea>
                                                              <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel.Notes" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div class="form-group checkoutToPersonOption">
                                                          <div class="checkbox-list">
                                                              <label class="col-md-3 control-label"><input data-val="true" data-val-required="The Send Email field is required." id="eventCheckOutModel_IsEmailSend" name="eventCheckOutModel.IsEmailSend" onchange="javascript:funChangeEmailText(&#39;eventCheckOutModel_IsEmailSend&#39;,&#39;eventCheckOutModel_PersonId&#39;,&#39;eventCheckOutModel_PersonEmail&#39;,true,&#39;formEventCheckOut&#39;); $(&#39;#eventCheckOutModel_PersonEmail&#39;).prop(&#39;required&#39;, function(i, v) { return !v; });" type="checkbox" value="true" /><input name="eventCheckOutModel.IsEmailSend" type="hidden" value="false" /> Send Email </label>
                                                          </div>
                                                          <div class="col-md-6">
                                                              <input class="form-control sendemail" data-Val-required="Person email is required." data-val="true" data-val-length="Maximum length is 100." data-val-length-max="100" disabled="disabled" id="eventCheckOutModel_PersonEmail" maxlength="50" name="eventCheckOutModel.PersonEmail" onkeyup="javascript:funChangeEmailText(&#39;eventCheckOutModel_IsEmailSend&#39;,&#39;eventCheckOutModel_PersonId&#39;,&#39;eventCheckOutModel_PersonEmail&#39;,false,&#39;formEventCheckOut&#39;);" placeholder="Enter Email Address" type="email" value="" />
                                                              <span class="field-validation-valid" data-valmsg-for="eventCheckOutModel.PersonEmail" data-valmsg-replace="true"></span>
                                                          </div>
                                                          <div class="col-md-3">
                                                              <input type="button" class="btn btn-sm blue circle sendemailbtn hide" value="Save Email" name="sendemail" id="updateEmail" onclick="funChangeEmail('eventCheckOutModel_PersonId', 'eventCheckOutModel_PersonEmail', 'formEventCheckOut', '/persons/UpdateEmail')" />
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div Class="modal-footer">
                                          <button type="button" class="btn blue circle" onclick="funValidateReserve3('formEventCheckOut', 'eventCheckOutModel_Date1', 'Checkout','eventCheckOutModel_Date2')">Check-out</button>

                                          <Button type="button" Class="btn default circle" data-dismiss="modal">Cancel</Button>
                                      </div>
                                  </div>
                              </div>
                          </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-range="Asset reference is required." data-val-range-max="9999999" data-val-range-min="1" data-val-required="Asset reference is required." id="eventCheckOutModel_assetId" name="eventCheckOutModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventCheckOutModel_eventId" name="eventCheckOutModel.eventId" type="hidden" value="0" /></form><form action="/assettiger/assets/eventLease" class="form-horizontal" id="formEventLease" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="_79WdnqzrHIinf7z59id_OwFp2iac1miwAkbj27wzcpF_poXv-pPdsKDjFC-hHy_kTbyKJE0QyRFW5EO2DH_oXeC-Lk_Od5obweab3GNw_HZON6Kqf2_ObKpEUi83K8xJrdZHA2" />                <div class="modal fade " id="event-lease" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="modal-dialog ">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                          <h4 class="modal-title">Lease</h4>
                                      </div>
                                      <div class="modal-body">
                                          <div class="form-body">
                                              <div class="form-row">
                                                  <div class="col-md-12">
                                                      <div class="form-group">
                                                          <label class="col-md-4 control-label" for="eventLeaseModel_Date1">Lease Begins <span class="font-red">*</span></label>
                                                          <div class="col-md-6">
                                                              <div class="input-group date date-picker">
                                                                  <input class="form-control form-control-inline" data-val="true" data-val-date="Lease Begins must be a date." data-val-required="Lease begins is required." placeholder="MM/dd/yyyy" id="eventLeaseModel_Date1" name="eventLeaseModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                                  <span class="input-group-btn">
                                                                      <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                  </span>
                                                              </div>
                                                              <span class="field-validation-valid" data-valmsg-for="eventLeaseModel.Date1" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-md-4 control-label" for="eventLeaseModel_PersonId">Leasing Customer <span class="font-red">*</span></label>
                                                          <div class="col-md-8">
                                                              <div id="customersList_ajax_wrapper" class="input-group">

                                                                  <input class="form-control select2customer" data-val="true" data-val-number="The field Leasing Customer must be a number." data-val-range="Assign to is required." data-val-range-max="9999999" data-val-range-min="1" data-val-required="Leasing customer is required." id="eventLeaseModel_PersonId" name="eventLeaseModel.PersonId" onchange="javascript: funupdateEmail('eventLeaseModel_PersonId', 'eventLeaseModel_PersonEmail', 'formEventLease');" placeholder="Select Customer" type="text" value="" itle="Leasing Customer *">
                                                                  <div class="input-group-addon" style="padding:0; border:0; background:transparent; padding-left:18px;">
                                                                          <a class="btn btn-default btn-circle" onclick="funPopupAjaxForm('customers/add', 'popup_ajax_wrapper', '', true, '')"><i class="icon-plus"></i> New</a>
                                                                  </div>
                                                              </div>
                                                              <span class="field-validation-valid" data-valmsg-for="eventLeaseModel.PersonId" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-md-4 control-label" for="eventLeaseModel_Date2">Lease Expires</label>
                                                          <div class="col-md-6">
                                                              <div class="input-group date date-picker">
                                                                  <input class="form-control form-control-inline" data-val="true" data-val-date="Lease expires must be a date." placeholder="MM/dd/yyyy" id="eventLeaseModel_Date2" name="eventLeaseModel.Date2" type="text" value="" autocomplete="off">
                                                                  <span class="input-group-btn">
                                                                      <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                  </span>
                                                              </div>
                                                              <span class="field-validation-valid" data-valmsg-for="eventLeaseModel.Date2" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-md-4 control-label" for="eventLeaseModel_Notes">Notes</label>
                                                          <div class="col-md-8">
                                                              <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventLeaseModel_Notes" maxlength="1000" name="eventLeaseModel.Notes" rows="4">
          </textarea>
                                                              <span class="field-validation-valid" data-valmsg-for="eventLeaseModel.Notes" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <div class="checkbox-list">
                                                              <label class="col-md-4 control-label"><input data-val="true" data-val-required="The Send Email field is required." id="eventLeaseModel_IsEmailSend" name="eventLeaseModel.IsEmailSend" onchange="javascript:funChangeEmailText(&#39;eventLeaseModel_IsEmailSend&#39;,&#39;eventLeaseModel_PersonId&#39;,&#39;eventLeaseModel_PersonEmail&#39;,true,&#39;formEventLease&#39;); $(&#39;#eventLeaseModel_PersonEmail&#39;).prop(&#39;required&#39;, function(i, v) { return !v; });" type="checkbox" value="true" /><input name="eventLeaseModel.IsEmailSend" type="hidden" value="false" /> Send Email </label>
                                                          </div>
                                                          <div class="col-md-6">
                                                              <input class="form-control sendemail" data-Val-required="Customer email is required." data-val="true" data-val-length="Maximum length is 100." data-val-length-max="100" disabled="disabled" id="eventLeaseModel_PersonEmail" maxlength="50" name="eventLeaseModel.PersonEmail" onkeyup="javascript:funChangeEmailText(&#39;eventLeaseModel_IsEmailSend&#39;,&#39;eventLeaseModel_PersonId&#39;,&#39;eventLeaseModel_PersonEmail&#39;,false,&#39;formEventLease&#39;);" placeholder="Enter Email Address" type="email" value="" />
                                                              <span class="field-validation-valid" data-valmsg-for="eventLeaseModel.PersonEmail" data-valmsg-replace="true"></span>
                                                          </div>
                                                          <div class="col-md-2">
                                                              <input type="button" class="btn btn-sm blue circle sendemailbtn hide" value="Save Email" name="sendemail" id="updateEmail" onclick="funChangeEmail('eventLeaseModel_PersonId', 'eventLeaseModel_PersonEmail', 'formEventLease', '/customers/UpdateEmail')" />
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn blue circle" onclick="funValidateReserve3('formEventLease', 'eventLeaseModel_Date1','Lease','eventLeaseModel_Date2')">Lease</button>

                                          <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Asset reference is required." id="eventLeaseModel_assetId" name="eventLeaseModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventLeaseModel_eventId" name="eventLeaseModel.eventId" type="hidden" value="0" /></form><form action="/assettiger/assets/eventLost" class="form-horizontal" id="formEventLost" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="RX8jsDb4pKwVq-2_E70ckTH3a-G_hplVh0la0S4UtQAEbRKcT8tRFnp0ArcE_rm92pORgACqVTZATvS8iwEX4n-VKduzt759Z69N3rzqVVgv5_HPVpY53-Etjs80tanhMZp2Eg2" />                <div class="modal fade " id="event-lost" tabindex="-1" role="dialog" aria-hidden="true">
                              <div class="modal-dialog ">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                          <h4 class="modal-title">Lost / Missing Asset</h4>
                                      </div>
                                      <div class="modal-body">
                                          <div class="form-body">
                                              <div class="form-row">
                                                  <div class="col-md-12">
                                                      <div class="form-group">
                                                          <label class="col-md-3 control-label" for="eventLostModel_Date1">Date Lost <span class="font-red">*</span></label>
                                                          <div class="col-md-7">
                                                              <div class="input-group date date-picker">
                                                                  <input class="form-control form-control-inline" data-val="true" data-val-date="Date lost must be a date." data-val-required="Date lost is required." placeholder="MM/dd/yyyy" id="eventLostModel_Date1" name="eventLostModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                                  <span class="input-group-btn">
                                                                      <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                  </span>
                                                              </div>
                                                              <span class="field-validation-valid" data-valmsg-for="eventLostModel.Date1" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                      <div class="form-group">
                                                          <label class="col-md-3 control-label" for="eventLostModel_Notes">Notes</label>
                                                          <div class="col-md-8">
                                                              <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventLostModel_Notes" maxlength="1000" name="eventLostModel.Notes" rows="4">
          </textarea>
                                                              <span class="field-validation-valid" data-valmsg-for="eventLostModel.Notes" data-valmsg-replace="true"></span>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="modal-footer">
                                          <input type="submit" class="btn red circle" value="Lost" name="EventType" />
                                          <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Asset reference is required." id="eventLostModel_assetId" name="eventLostModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventLostModel_eventId" name="eventLostModel.eventId" type="hidden" value="0" /></form><form action="/assettiger/assets/eventRepair" class="form-horizontal" id="formEventRepair" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="dA56yhzwnOOOta9fQL1G9CVC6t5Lm3yLHiQjJ5fyOfN8H7rhtAhBORlUatBKNSF1wOPTT-q6SddRG_sCRK8scr0w0rsjMiS_DFbpJAnWD5i6ZPjxQ3TAJByXnKo5jGuVPCIruw2" />            <div class="modal fade " id="event-repair" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog ">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Repair Asset</h4>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-body">
                                          <div class="form-row">
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventRepairModel_Date1">Schedule Date <span class="font-red">*</span></label>
                                                      <div class="col-md-6">
                                                          <div class="input-group date date-picker">
                                                              <input class="form-control form-control-inline" data-val="true" data-val-date="Schedule date must be a date." data-val-required="Schedule date is required." placeholder="MM/dd/yyyy" id="eventRepairModel_Date1" name="eventRepairModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                              <span class="input-group-btn">
                                                                  <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                              </span>
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventRepairModel.Date1" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventRepairModel_PersonName">Assigned to</label>
                                                      <div class="col-md-6">
                                                          <input class="form-control" data-val="true" data-val-length="Maximum length is 100." data-val-length-max="100" id="eventRepairModel_PersonName" maxlength="100" name="eventRepairModel.PersonName" type="text" value="" />
                                                          <span class="field-validation-valid" data-valmsg-for="eventRepairModel.PersonName" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventRepairModel_Date2">Date Completed</label>
                                                      <div class="col-md-6">
                                                          <div class="input-group date date-picker">
                                                              <input class="form-control form-control-inline" data-val="true" data-val-date="Schedule date must be a date." placeholder="MM/dd/yyyy" id="eventRepairModel_Date2" name="eventRepairModel.Date2" type="text" value="" autocomplete="off">
                                                              <span class="input-group-btn">
                                                                  <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                              </span>
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventRepairModel.Date2" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventRepairModel_Amount">Repair Cost</label>
                                                      <div class="col-md-7">
                                                          <div class="input-group">
                                                              <span class="input-group-addon">KSh</span>
                                                              <input class="form-control" data-val="true" data-val-number="The field Repair Cost must be a number." data-val-range="Repair Cost must be within 0 to 999999999999999." data-val-range-max="999999999999999" data-val-range-min="0" id="eventRepairModel_Amount" name="eventRepairModel.Amount" placeholder="Kenya Shilling" type="text" value="" />
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventRepairModel.Amount" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventRepairModel_Notes">Repair Notes</label>
                                                      <div class="col-md-8">
                                                          <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventRepairModel_Notes" maxlength="1000" name="eventRepairModel.Notes" rows="4">
          </textarea>
                                                          <span class="field-validation-valid" data-valmsg-for="eventRepairModel.Notes" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <input type="submit" class="btn blue circle" value="Submit" name="EventType" />
                                      <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                                  </div>
                              </div>
                          </div>
                      </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Asset reference is required." id="eventRepairModel_assetId" name="eventRepairModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventRepairModel_eventId" name="eventRepairModel.eventId" type="hidden" value="0" /></form><form action="/assettiger/assets/eventBroken" class="form-horizontal" id="formEventBroken" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="HnjdiXx_3O55QXDdch2VSuYBCCG2KPLUtUXg4v7Q4QeVeTPpMGWStBTKaZ5ek7npwK1lZQE1ksxFB6ZCDIEZ5-xEHmpoco1XfKUQgE3XYK_jcUelbbSTYqV1hGEjb0Jz7xRINQ2" />            <div class="modal fade " id="event-broken" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog ">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Broken Asset</h4>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-body">
                                          <div Class="form-row" style="display:block">
                                              <div Class="col-md-12">
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventBrokenModel_Date1">Date Broken <span class="font-red">*</span></label>
                                                      <div class="col-md-7">
                                                          <div class="input-group date date-picker">
                                                              <input class="form-control form-control-inline" data-val="true" data-val-date="Date broken must be a date." data-val-required="Date broken is required." placeholder="MM/dd/yyyy" id="eventBrokenModel_Date1" name="eventBrokenModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                              <span class="input-group-btn">
                                                                  <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                              </span>
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventBrokenModel.Date1" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div Class="form-group">
                                                      <label class="col-md-3 control-label" for="eventBrokenModel_Notes">Notes</label>
                                                      <div class="col-md-8">
                                                          <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventBrokenModel_Notes" maxlength="1000" name="eventBrokenModel.Notes" rows="4">
          </textarea>
                                                          <span class="field-validation-valid" data-valmsg-for="eventBrokenModel.Notes" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <input type="submit" class="btn blue circle" value="Broken" name="EventType" />
                                      <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                                  </div>
                              </div>
                          </div>
                      </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Asset reference is required." id="eventBrokenModel_assetId" name="eventBrokenModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventBrokenModel_eventId" name="eventBrokenModel.eventId" type="hidden" value="0" /></form><form action="/assettiger/assets/eventDispose" class="form-horizontal" id="formEventDispose" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="WNqCX0dy7oYLLeB8D33S9Mis9TT6iTEIMVJ68v4woxvtmQ8OSg7td6tKn8OsgSMb20-klmR5POia355Fl9kbCCKMA6JBFmlbwb0boYHW0z88IOulbndnFryo_OmYxdwwhHGoDA2" />            <div class="modal fade " id="event-dispose" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog ">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Dispose Asset</h4>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-body">
                                          <div class="form-row" style="display:block">
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDisposeModel_Date1">Date Disposed <span class="font-red">*</span></label>
                                                      <div class="col-md-7">
                                                          <div class="input-group date date-picker">
                                                              <input class="form-control form-control-inline" data-val="true" data-val-date="Date disposed must be a date." data-val-required="Date disposed is required." placeholder="MM/dd/yyyy" id="eventDisposeModel_Date1" name="eventDisposeModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                              <span class="input-group-btn">
                                                                  <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                              </span>
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventDisposeModel.Date1" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDisposeModel_PersonName">Dispose to</label>
                                                      <div class="col-md-7">
                                                          <input class="form-control" data-val="true" data-val-length="Maximum length is 100." data-val-length-max="100" id="eventDisposeModel_PersonName" maxlength="100" name="eventDisposeModel.PersonName" type="text" value="" />
                                                          <span class="field-validation-valid" data-valmsg-for="eventDisposeModel.PersonName" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDisposeModel_Notes">Disposal Reason</label>
                                                      <div class="col-md-8">
                                                          <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventDisposeModel_Notes" maxlength="1000" name="eventDisposeModel.Notes" rows="4">
          </textarea>
                                                          <span class="field-validation-valid" data-valmsg-for="eventDisposeModel.Notes" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <input type="submit" class="btn blue circle" value="Dispose" name="EventType" />
                                      <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                                  </div>
                              </div>
                          </div>
                      </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Asset reference is required." id="eventDisposeModel_assetId" name="eventDisposeModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventDisposeModel_eventId" name="eventDisposeModel.eventId" type="hidden" value="0" /></form><form action="/assettiger/assets/eventDonate" class="form-horizontal" id="formEventDonate" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="_yklaedcgdFkQPprLskKp-MXXO7329s_SyBvhbk6lo6FIo69l2ey8NKSl-xdsgkjdH7Whfiu8u3_swL7gkAwCpMCj_YEXAczX3UPo7fEU4g1PiHOKcB0xNc4rMwYpWfmrZdd4Q2" />            <div class="modal fade " id="event-donate" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog ">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Donate Asset</h4>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-body">
                                          <div class="form-row" style="display:block">
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDonateModel_Date1">Date Donated <span class="font-red">*</span></label>
                                                      <div class="col-md-6">
                                                          <div class="input-group date date-picker">
                                                              <input class="form-control form-control-inline" data-val="true" data-val-date="Date donated must be a date." data-val-required="Date donated is required." placeholder="MM/dd/yyyy" id="eventDonateModel_Date1" name="eventDonateModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                              <span class="input-group-btn">
                                                                  <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                              </span>
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventDonateModel.Date1" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDonateModel_PersonName">Donate to</label>
                                                      <div class="col-md-6">
                                                          <input class="form-control" data-val="true" data-val-length="Maximum length is 100." data-val-length-max="100" id="eventDonateModel_PersonName" maxlength="100" name="eventDonateModel.PersonName" type="text" value="" />
                                                          <span class="field-validation-valid" data-valmsg-for="eventDonateModel.PersonName" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDonateModel_Amount">Donate Value <span class="font-red">*</span></label>
                                                      <div class="col-md-6">
                                                          <div class="input-group">
                                                              <span class="input-group-addon">KSh</span>
                                                              <input class="form-control" data-val="true" data-val-number="The field Donate Value must be a number." data-val-range="Donate Value must be within 0 to 999999999999999." data-val-range-max="999999999999999" data-val-range-min="0" data-val-required="Amount is required." id="eventDonateModel_Amount" name="eventDonateModel.Amount" placeholder="Kenya Shilling" type="text" value="0" />
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventDonateModel.Amount" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDonateModel_Deductible">Deductible</label>
                                                      <div class="col-md-6">
                                                          <div class="radio-list">
                                                              <label class="radio-inline"><input type="radio" name="radDonateModelIsDeductible" onchange="javascript: $('#formEventDonate').find('#eventDonateModel_Deductible').val('True')" > Yes</label>
                                                              <label class="radio-inline"><input type="radio" name="radDonateModelIsDeductible" onchange="javascript: $('#formEventDonate').find('#eventDonateModel_Deductible').val('False')" checked=&quot;checked&quot;> No</label>
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventDonateModel.Deductible" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventDonateModel_Notes">Notes</label>
                                                      <div class="col-md-8">
                                                          <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventDonateModel_Notes" maxlength="1000" name="eventDonateModel.Notes" rows="4">
          </textarea>
                                                          <span class="field-validation-valid" data-valmsg-for="eventDonateModel.Notes" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <input type="submit" class="btn blue circle" value="Donate" name="EventType" />
                                      <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                                  </div>
                              </div>
                          </div>
                      </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Asset reference is required." id="eventDonateModel_assetId" name="eventDonateModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-required="Is deductible?" id="eventDonateModel_Deductible" name="eventDonateModel.Deductible" type="hidden" value="False" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventDonateModel_eventId" name="eventDonateModel.eventId" type="hidden" value="0" /></form><form action="/assettiger/assets/eventSell" class="form-horizontal" id="formEventSell" method="post" onsubmit="funShowLoaderOnValidate(this);" role="form"><input name="__RequestVerificationToken" type="hidden" value="oVosYSMLLepp3Qtx55_-W57-fxJY_Q1nmk_m993s5FArXKfSJeMdQD9xiNEET9-1MgYuKGT909BA0tpRtDysh4YB04Er8e1nJnTupJUjndG4jyBSBnEDyq3An-KTxG45B8mqlQ2" />            <div class="modal fade " id="event-sell" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog ">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                      <h4 class="modal-title">Sell AssetSell</h4>
                                  </div>
                                  <div class="modal-body">
                                      <div class="form-body">
                                          <div class="form-row" style="display:block">
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventSellModel_Date1">Sale Date <span class="font-red">*</span></label>
                                                      <div class="col-md-6">
                                                          <div class="input-group date date-picker">
                                                              <input class="form-control form-control-inline" data-val="true" data-val-date="Sell date must be a date." data-val-required="Sell date is required." placeholder="MM/dd/yyyy" id="eventSellModel_Date1" name="eventSellModel.Date1" type="text" value="12/12/2018" autocomplete="off">
                                                              <span class="input-group-btn">
                                                                  <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                              </span>
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventSellModel.Date1" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventSellModel_PersonName">Sold to</label>
                                                      <div class="col-md-7">
                                                          <input class="form-control" data-val="true" data-val-length="Maximum length is 100." data-val-length-max="100" id="eventSellModel_PersonName" maxlength="100" name="eventSellModel.PersonName" type="text" value="" />
                                                          <span class="field-validation-valid" data-valmsg-for="eventSellModel.PersonName" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventSellModel_Amount">Sale amount</label>
                                                      <div class="col-md-6">
                                                          <div class="input-group">
                                                              <span class="input-group-addon">KSh</span>
                                                              <input class="form-control" data-val="true" data-val-number="The field Sale amount must be a number." data-val-range="Sale amount must be within 0 to 999999999999999." data-val-range-max="999999999999999" data-val-range-min="0" id="eventSellModel_Amount" name="eventSellModel.Amount" placeholder="Kenya Shilling" type="text" value="" />
                                                          </div>
                                                          <span class="field-validation-valid" data-valmsg-for="eventSellModel.Amount" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-md-3 control-label" for="eventSellModel_Notes">Notes</label>
                                                      <div class="col-md-8">
                                                          <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="eventSellModel_Notes" maxlength="1000" name="eventSellModel.Notes" rows="4">
          </textarea>
                                                          <span class="field-validation-valid" data-valmsg-for="eventSellModel.Notes" data-valmsg-replace="true"></span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <input type="submit" class="btn blue  circle" value="Sell" name="EventType" />
                                      <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                                  </div>
                              </div>
                          </div>
                      </div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Asset reference is required." id="eventSellModel_assetId" name="eventSellModel.assetId" type="hidden" value="6956696" /><input data-val="true" data-val-number="The field eventId must be a number." data-val-required="The eventId field is required." id="eventSellModel_eventId" name="eventSellModel.eventId" type="hidden" value="0" /></form>
          <div id="mat_asset_detail_wrapper" style="display:none; ">
                                          <div class="form-row static mat-asset-detail-event" style="display:none; ">
                  <div class="col-md-2"><h4><span> Creation</span></h4></div>
                  <div class="col-md-5">
                      <table class="table table-bordered table-asset">
                          <tbody>
                              <tr>
                                  <td>Date Created</td>
                                  <td>10/10/2018 03:04 PM</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
                  <div class="col-md-5">
                      <table class="table table-bordered table-asset">
                          <tbody>
                              <tr>
                                  <td>Created by</td>
                                  <td>Kennedy Maikuma</td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>

                          </div>
                          <div class="FixWidth">
                              <div class="clearfix">



          <div class="tabbable-custom tabbable-full-width readyPanel2">
              <ul class="nav nav-tabs asset_sticky">
                  <li class="active"><a href="#PanalDetail" data-toggle="tab">Details</a></li>
                  <li><a href="#PanalEvent" data-toggle="tab" onclick="funBeginEvent(); $(this).removeAttr('onclick');">Events</a></li>
                      <li><a href="#PanalImages" data-toggle="tab" onclick="funfetchAssetImages(); $(this).removeAttr('onclick');">Photos</a></li>
                              <li><a href="#PanalDocuments" data-toggle="tab" onclick="funfetchAssetDocs(); $(this).removeAttr('onclick');" title="Documents">Docs.</a></li>
                              <li><a href="#PanalDepr" data-toggle="tab" onclick="funBeginDepr(); $(this).removeAttr('onclick');">Depreciation</a></li>
                              <li><a href="#PanalWarranty" data-toggle="tab" onclick="funBeginWarranty(); $(this).removeAttr('onclick');">Warranty</a></li>
                  <li><a href="#PanalLinks" data-toggle="tab" onclick="funBeginLinks(); $(this).removeAttr('onclick');">Linking</a></li>
                      <li><a href="#PanalMaintenance" data-toggle="tab" onclick="funBeginMaintenance(); $(this).removeAttr('onclick');" title="Maintenance">Maint.</a></li>
                                      <li><a href="#PanalInsurance" data-toggle="tab" onclick="funBeginInsurance(); $(this).removeAttr('onclick');">Insurances</a></li>
                              <li><a href="#PanalReserve" data-toggle="tab" onclick="funBeginReserve(); $(this).removeAttr('onclick');">Reserve</a></li>
                              <li><a href="#PanalAudit" data-toggle="tab" onclick="funBeginAudit(); $(this).removeAttr('onclick');">Audit</a></li>
                              <li><a href="#PanalHistory" data-toggle="tab" onclick="funBeginHistory(); $(this).removeAttr('onclick');">History</a></li>
              </ul>
              <div class="tab-content">
                  <div class="tab-pane active" id="PanalDetail">
                      <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Description</h5></div>
                      <?=$model->assetDescription;?>

                      </div>
                      <div class="FixWidth">
                          <div class="clearfix">
                              <div class="portlet light bg-inverse">
                                  <div class="portlet-body form">
                                      <div class="form-body">
                                              <div class="form-row static" style="display:none; ">
                                                  <div class="col-md-2"><h4><span> Miscellaneous</span></h4></div>
                                                  <div class="col-md-5">
                                                          <table class="table table-bordered table-asset"><tbody><tr><td>Serial No</td><td></td></tr></tbody></table>
                                                  </div>
                                                  <div class="col-md-5">
                                                          <table class="table table-bordered table-asset"><tbody><tr><td>Purchased from</td><td>Amazon</td></tr></tbody></table>
                                                  </div>
                                              </div>
                                                                              <div class="form-row static" style="display:none; ">
                                                  <div class="col-md-2"><h4><span> Custom fields</span></h4></div>
                                                  <div class="col-md-5">
                                                      <table class="table table-bordered table-asset">
                                                          <tbody>
                                                              <tr>
                                                                  <td>qr code</td>
                                                                  <td>
                                                                  </td>
                                                              </tr>
                                                          </tbody>
                                                      </table>
                                                  </div>
                                                  <div class="col-md-5">
                                                      <table class="table table-bordered table-asset">
                                                          <tbody>
                                                          </tbody>
                                                      </table>
                                                  </div>
                                              </div>
                                                                              <div class="form-row static" style="display:none; ">
                                                  <div class="col-md-2"><h4><span> Funding</span></h4></div>
                                                  <div class="col-md-5">
                                                      <table class="table table-bordered table-asset"><tbody><tr><td>Fund</td><td><a onclick="javascript: quickView('/assettiger/funds/quickview/')"></a></td></tr></tbody></table>
                                                  </div>
                                                  <div class="col-md-5">
                                                      <table class="table table-bordered table-asset"><tbody><tr><td>Amount Debited</td><td></td></tr></tbody></table>
                                                  </div>
                                              </div>
                                                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="tab-pane" id="PanalEvent">
                      <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Events</h5></div></div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="portlet light bg-inverse">
                                  <div class="progress progress-striped active">
                                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                          <span class="">JUST A MOMENT</span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                      <div class="tab-pane" id="PanalImages">
                          <div class="row"><div class="col-md-12"><h5 class="page-title pull-left">Photos</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                              <div class="tab-pane" id="PanalDocuments">
                          <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Documents</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                              <div class="tab-pane" id="PanalDepr">
                          <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Depreciation</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                              <div class="tab-pane" id="PanalWarranty">
                          <div class="row FixWidth"><div class="col-md-12"><h5 class="page-title pull-left">Warranty</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  <div class="tab-pane" id="PanalLinks">
                      <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Asset Linking</h5></div></div>
                      <div class="row">
                          <div class="col-md-12">
                              <div class="portlet light bg-inverse">
                                  <div class="progress progress-striped active">
                                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                          <span class="">JUST A MOMENT</span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                      <div class="tab-pane" id="PanalMaintenance">
                          <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Maintenance</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                                      <div class="tab-pane" id="PanalInsurance">
                          <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Insurances</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                              <div class="tab-pane" id="PanalReserve">
                          <div class="row"><div class="col-md-12"><h5 class="page-title pull-left">Reserve</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="portlet-body form">
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <div class="portlet light calendar bordered" style="margin-bottom:0px;">
                                                      <div id="calendar"></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div id="ReserveFormWrapper"></div>
                      </div>
                              <div class="tab-pane" id="PanalAudit">
                          <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">Audit</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                              <div class="tab-pane" id="PanalHistory">
                          <div class="FixWidth"><div class="clearfix"><h5 class="page-title pull-left">History</h5></div></div>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bg-inverse">
                                      <div class="progress progress-striped active">
                                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                              <span class="">JUST A MOMENT</span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
              </div>
          </div>
          <div class="modal fade" id="confirmDetachImage" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Detach Image</h4>
                      </div>
                      <div class="modal-body">
                          Are you sure you want to detach this Image?
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn red btn-circle" data-dismiss="modal" onclick="funDetachImageMain()">Confirm Delete</button>
                          <button type="button" class="btn default btn-circle" data-dismiss="modal">Cancel</button>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal fade" id="confirmDeleteAssetBox" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Delete Asset</h4>
                      </div>
                      <div class="modal-body">
                          Are you sure you want to delete this asset?
                      </div>
                      <div class="modal-footer">
                          <a class="btn red btn-circle" href="/assettiger/assets/delete/6956696" onclick="$('#spinner').show(0);">Confirm Delete</a>
                          <button type="button" class="btn default btn-circle" data-dismiss="modal">Cancel</button>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal fade" id="geomap" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">Map</h4>
                      </div>
                      <div class="modal-body">
                          <div id="map" class="gmaps margin-bottom-40" style="height:400px;"></div>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn default btn-circle" data-dismiss="modal">Close</button>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal fade" id="AuditGeomap" tabindex="-1" role="basic" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title pull-left" style="margin-right:20px;">Map</h4>
                          <form class="form-inline" method="post" id="geocoding_form">
                              <div class="form-group">
                                  <div class="input-group">
                                      <div class="input-group-control">
                                          <input type="text" class="form-control" data-val="true" data-val-required="value is required." id="address" name="address" placeholder="Address" style="min-width:250px;" />
                                      </div>
                                      <span class="input-group-btn btn-right">
                                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                      </span>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <div class="modal-body">
                          <div id="Auditmap" class="gmaps margin-bottom-40" style="height:400px;"></div>
                          <form>
                              <div class="form-row" style="margin-top:15px;">
                                  <div class="col-md-5">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" for="LatitudeSelect">Latitude</label>
                                          <div class="col-md-9">
                                              <input class="form-control" data-val="true" data-val-number="The field Latitude must be a number." data-val-range="Latitude must be within -90 to 90." data-val-range-max="90" data-val-range-min="-90" data-val-required="GPS location latitude is required." id="LatitudeSelect" maxlength="50" name="LatitudeSelect" type="text" value="40.6983401817878" disabled>
                                              <span class="field-validation-valid" data-valmsg-for="LatitudeSelect" data-valmsg-replace="true"></span>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-5">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" for="LongitudeSelect">Latitude</label>
                                          <div class="col-md-9">
                                              <input class="form-control" data-val="true" data-val-number="The field Longitude must be a number." data-val-range="Longitude must be within -180 to 180." data-val-range-max="180" data-val-range-min="-180" data-val-required="GPS location longitude is required." id="LongitudeSelect" maxlength="50" name="LongitudeSelect" type="text" value="-74.0055370330811" disabled>
                                              <span class="field-validation-valid" data-valmsg-for="LongitudeSelect" data-valmsg-replace="true"></span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-circle blue" data-dismiss="modal" onclick="javascript: $('#Latitude').val($('#LatitudeSelect').val()); $('#Longitude').val($('#LongitudeSelect').val());">Apply</button>
                          <button type="button" class="btn default btn-circle" data-dismiss="modal">Cancel</button>
                      </div>
                  </div>
              </div>
          </div>
          <div Class="modal fade" id="asset-copy" tabindex="-1" role="basic" aria-hidden="true"></div>
          <form action="/assettiger/assets/email" class="form-horizontal" id="formEmail" method="post" role="form"><input name="__RequestVerificationToken" type="hidden" value="0-s1Q5hPBQUTEWXT7LEr4JP-7yaiHjE2GHC9Vq8Gumnd6Mc3deYAXcZZ68IaaAYesPCjahX9ENlvjD2hIJnAwTz5-u7K_pReUpZ7TqmH7ePKuFwHm42BlW_rxSYHA2N_qBETOA2" /><input data-val="true" data-val-number="The field assetId must be a number." data-val-required="Invalid form." id="emailModel_assetId" name="emailModel.assetId" type="hidden" value="6956696" />    <div class="modal fade " id="asset-email" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog ">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                              <h4 class="modal-title">Email Asset</h4>
                          </div>
                          <div class="modal-body">
                              <div class="form-body">
                                  <div class="form-row">
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" for="emailModel_EmailTo">Email to*</label>
                                              <div class="col-md-8">
                                                  <input class="form-control tags matTextTags" data-val="true" data-val-required="Email to required." id="emailModel_EmailTo" name="emailModel.EmailTo" type="email" value="" />
                                                  <span class="help-block">* separate each email with a comma.</span>
                                                  <span class="field-validation-valid" data-valmsg-for="emailModel.EmailTo" data-valmsg-replace="true"></span>
                                                  <span class="field-validation-valid" data-valmsg-for="emailModel_EmailTo_tag" data-valmsg-replace="true"></span>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" for="emailModel_Notes">Notes</label>
                                              <div class="col-md-8">
                                                  <textarea class="form-control" cols="20" data-val="true" data-val-length="Maximum length is 1000." data-val-length-max="1000" id="emailModel_Notes" maxlength="1000" name="emailModel.Notes" rows="4">
          </textarea>
                                                  <span class="field-validation-valid" data-valmsg-for="emailModel.Notes" data-valmsg-replace="true"></span>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-md-3 control-label">Attachments</label>
                                              <div class="col-md-7">
                                                  <div class="checkbox-list">
                                                      <label><input data-val="true" data-val-required="The Photos field is required." id="emailModel_hasPhotos" name="emailModel.hasPhotos" type="checkbox" value="true" /><input name="emailModel.hasPhotos" type="hidden" value="false" /> Photos</label>
                                                      <label><input data-val="true" data-val-required="The Documents field is required." id="emailModel_hasDocuments" name="emailModel.hasDocuments" type="checkbox" value="true" /><input name="emailModel.hasDocuments" type="hidden" value="false" /> Documents</label>
                                                  </div>
                                                  <span class="field-validation-valid" data-valmsg-for="emailModel.hasPhotos" data-valmsg-replace="true"></span>
                                                  <span class="field-validation-valid" data-valmsg-for="emailModel.hasDocuments" data-valmsg-replace="true"></span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn blue circle" id="formEmailSubmitBtn">Send Email</button>
                              <button type="button" class="btn default circle" data-dismiss="modal">Cancel</button>
                          </div>
                      </div>
                  </div>
              </div>
          </form><div id="popup_ajax_wrapper"></div>
          <input data-val="true" data-val-number="The field assetId must be a number." data-val-required="The assetId field is required." id="assetId" name="assetId" type="hidden" value="6956696" />


                              </div>
                          </div>

                      </div>
                  </div>
              </div>



            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
