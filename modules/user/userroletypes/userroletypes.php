<?php

namespace app\modules\user\userroletypes;

/**
 * userroletypes module definition class
 */
class userroletypes extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\user\userroletypes\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
