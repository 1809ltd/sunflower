<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_office_requisition_payments".
 *
 * @property int $id
 * @property string $reqrecepitNumber
 * @property string $reqpaymentName
 * @property int $reqpaymentAccount
 * @property string $reqpaymentTxnDate
 * @property string $reqpaymentType
 * @property string $reqpaymentRefence
 * @property double $reqpaymentTotalAmt
 * @property int $reqpaymentProcessduration
 * @property string $reqpaymentLinkedTxn Number of office Requstion
 * @property string $reqpaymentLinkedTxnType Office Expense
 * @property int $reqpaymentStatus
 * @property string $reqpaymentCreateAt
 * @property string $reqpaymentUpdatedAt
 * @property string $reqpaymentDeletedAt
 * @property int $userId
 *
 * @property FinanceAccounts $reqpaymentAccount0
 * @property FinanceOfficeRequstionForm $reqpaymentLinkedTxn0
 * @property UserDetails $user
 */
class FinanceOfficeRequisitionPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_office_requisition_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reqrecepitNumber', 'reqpaymentName', 'reqpaymentAccount', 'reqpaymentTxnDate', 'reqpaymentType', 'reqpaymentRefence', 'reqpaymentTotalAmt', 'reqpaymentProcessduration', 'reqpaymentLinkedTxn', 'reqpaymentLinkedTxnType', 'reqpaymentStatus', 'userId'], 'required'],
            [['reqpaymentAccount', 'reqpaymentProcessduration', 'reqpaymentStatus', 'userId'], 'integer'],
            [['reqpaymentTxnDate', 'reqpaymentCreateAt', 'reqpaymentUpdatedAt', 'reqpaymentDeletedAt'], 'safe'],
            [['reqpaymentTotalAmt'], 'number'],
            [['reqrecepitNumber', 'reqpaymentName', 'reqpaymentLinkedTxn'], 'string', 'max' => 50],
            [['reqpaymentType', 'reqpaymentRefence', 'reqpaymentLinkedTxnType'], 'string', 'max' => 100],
            [['reqrecepitNumber'], 'unique'],
            [['reqpaymentAccount'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['reqpaymentAccount' => 'id']],
            [['reqpaymentLinkedTxn'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceOfficeRequstionForm::className(), 'targetAttribute' => ['reqpaymentLinkedTxn' => 'requstionNumber']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'reqrecepitNumber' => 'Number',
          'reqpaymentName' => 'Name',
          'reqpaymentAccount' => 'Payment Account',
          'reqpaymentTxnDate' => 'Date',
          'reqpaymentType' => 'Type',
          'reqpaymentRefence' => 'Payment Reference',
          'reqpaymentTotalAmt' => 'Amount',
          'reqpaymentProcessduration' => 'Process Duration',
          'reqpaymentLinkedTxn' => 'Requstion number',
          'reqpaymentLinkedTxnType' => 'Type',
          'reqpaymentStatus' => 'Status',
          'reqpaymentCreateAt' => 'Reqpayment Create At',
          'reqpaymentUpdatedAt' => 'Reqpayment Updated At',
          'reqpaymentDeletedAt' => 'Reqpayment Deleted At',
          'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReqpaymentAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'reqpaymentAccount']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReqpaymentLinkedTxn0()
    {
        return $this->hasOne(FinanceOfficeRequstionForm::className(), ['requstionNumber' => 'reqpaymentLinkedTxn']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
    public function getRcpnumber()
    {
        //select product code
        $connection = Yii::$app->db;
        $query= "Select MAX(reqrecepitNumber) AS number from finance_office_requisition_payments where id>0";
        $rows= $connection->createCommand($query)->queryAll();

        if ($rows) {
          // code...

          $number=$rows[0]['number'];
          $number++;
          if ($number == 1) {
            // code...
            $number =   "OREQRCP-".date('Y')."-".date('m')."-0001";
          }
          if ($number == 1) {
            // code...
            $number = "OREQRCP-".date('Y')."-".date('m')."-0001";
          }
        } else {
          // code...
          //generating numbers
          $number = "OREQRCP-".date('Y')."-".date('m')."-0001";
        }

      return $number;
    }
}
