<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;


/*Array*/
use yii\helpers\ArrayHelper;

use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
use app\modules\company\companysitelocation\models\CompanySiteLocation;
use app\modules\company\companydepartment\models\CompanyDepartment;
use app\modules\company\companydetails\models\CompanyDetails;
use app\modules\company\companysite\models\CompanySiteDetails;

use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\AssetSiteLocation */
/* @var $form yii\widgets\ActiveForm */
?>

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
  </div>
  <?php $form = ActiveForm::begin(); ?>

  <div class="modal-body asset-site-location-form">

    <?= $form->field($model, 'assetId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(AssetRegistration::find()->all(),'id','assetName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Asset ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>
    <?php
    $siteId=ArrayHelper::map(CompanySiteDetails::find()->where('`siteStatus` = 1')->asArray()->all(), 'id','siteName');?>
    <?= $form->field($model, 'siteId')->dropDownList($siteId, [
      'prompt'=>'Select...',
      'onChange'=>'$.post("index.php?r=company-site-location/lists&id='.'"+$(this).val(),function(data){
        $("select#assetsitelocation-loactionid").html(data);
      });'
    ]); ?>

    <?php
    // = $form->field($model, 'siteId')->widget(Select2::classname(), [
    //     'data' => ArrayHelper::map(CompanySiteDetails::find()->where('`siteStatus` = 1')->asArray()->all(), 'id','siteName'),
    //     'language' => 'en',
    //     'options' => ['placeholder' => 'Select a Site ...'],
    //     'pluginOptions' => [
    //         'allowClear' => true,
    //         'onChange' => '$.post("index.php?r=company-site-location/lists&id='.'"+$(this).val(),function(data){
    //             $("select#assetsitelocation-loactionid").html(data);
    //           });'
    //     ],
    // ]);

    ?>

    <?= $form->field($model, 'loactionId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CompanySiteLocation::find()->all(),'id','locationName'),
        'language' => 'en',
        'id'=>'location',
        'options' => ['placeholder' => 'Select a Customer Location ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <?= $form->field($model, 'departmentId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CompanyDepartment::find()->all(),'id','departmentName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Department ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>
    <?php $status = ['1' => 'Available', '2' => 'Moved', '3' => 'Event', '4' => 'Suspended']; ?>
    <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']); ?>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

  </div>
  <?php ActiveForm::end(); ?>
