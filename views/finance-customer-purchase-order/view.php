<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Customer Purchase Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-customer-purchase-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'purchaseOrderNumber',
            'customerId',
            'purchaseOrderProjectClassification',
            'purchaseOrderProjectId',
            'purchaseOrderProjectName',
            'purchaseOrderNote:ntext',
            'purchaseOrderTxnDate',
            'purchaseOrderBillEmail:ntext',
            'purchaseOrderDiscountAmount',
            'purchaseOrderTaxAmount',
            'purchaseOrderSubAmount',
            'purchaseOrderAmount',
            'purchaseOrderTxnStatus:ntext',
            'purchaseOrderdCreatedby',
            'purchaseOrderdCreatedAt',
            'purchaseOrderdUpdatedAt',
            'purchaseOrderdDeletedAt',
        ],
    ]) ?>

</div>
