<?php

namespace app\modules\finance\financereport\companyfinancials\alltransaction;

/**
 * alltransaction module definition class
 */
class alltransaction extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\companyfinancials\alltransaction\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
