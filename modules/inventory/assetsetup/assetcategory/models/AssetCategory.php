<?php

namespace app\modules\inventory\assetsetup\assetcategory\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "asset_category".
 *
 * @property int $id
 * @property string $assetCategoryName
 * @property string $assetCategoryDescription
 * @property int $assetCategoryParentId
 * @property int $assetCategoryStatus
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property UserDetails $user
 * @property AssetCategoryType[] $assetCategoryTypes
 * @property AssetRegistration[] $assetRegistrations
 */
class AssetCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetCategoryName', 'assetCategoryStatus', 'userId'], 'required'],
            [['assetCategoryDescription'], 'string'],
            [['assetCategoryParentId', 'assetCategoryStatus', 'userId'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['assetCategoryName'], 'string', 'max' => 50],
            [['assetCategoryName'], 'unique'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetCategoryName' => 'Asset Category Name',
            'assetCategoryDescription' => 'Asset Category Description',
            'assetCategoryParentId' => 'Asset Category Parent',
            'assetCategoryStatus' => 'Status',
            'userId' => 'User ID',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCategoryTypes()
    {
        return $this->hasMany(AssetCategoryType::className(), ['assetCategoryId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetRegistrations()
    {
        return $this->hasMany(AssetRegistration::className(), ['assetCategory' => 'id']);
    }
}
