<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
// Fiance Expense Acticty
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEventActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Expense Activities';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- begin row -->
<!-- this row will not appear when printing -->
<div class="box no-print">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::button('Add Expense', ['value'=>Url::to((['create'])),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        </p>
      </div>
    </div>
  </div>

  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
    'options' => [
        'tabindex' => false // important for Select2 to work properly
    ],
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
   <?php
    Modal::begin([
        // 'header'=>'<h4>Update Model</h4>',
        'id'=>'update-modal',
        'size'=>'modal-lg',
        'options' => [
            'tabindex' => false // important for Select2 to work properly
        ],
    ]);

    echo "<div id='updateModalContent'></div>";

    Modal::end();
?>

  <div class="box-body finance-event-activity-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->activityStatus==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'activityName',
            'activityDescription:ntext',
            // 'account',
            [
              'attribute'=>'account',
              'value'=>'account0.fullyQualifiedName',
            ],
            // 'activityStatus',
            //'updatedAt',
            //'deletedAt',
            //'createdAt',
            //'userId',



            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => ['view' => function($url, $model) {
            	    return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
            	},
              'update' => function($url,$model,$key){
                    $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                        'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                        'class'=>'update-modal-click grid-action',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'bottom',
                        'title'=>'Update'
                    ]);
                    return $btn;
              },
            	'delete' => function($url, $model) {
            	    return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
            	}
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
<?= $this->render('print.php') ?>

<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   // $('li.treeview').addClass('active');
   $('li.treeview').addClass('treeview menu-open');
   $('#productsetup').addClass('active');
   $('#finance-event-activity').addClass('active');
", View::POS_READY);
?>
