<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\userdetails\models\UserDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-details-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User Details', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'userStaffId',
            'userFName',
            'userLName',
            'userPhone',
            //'userEmail:email',
            'username',
            //'password',
            //'authKey',
            //'password_reset_token',
            //'userImage',
            //'userStatus',
            //'userLastLogin',
            //'userCreatedBy',
            //'userDeleteAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
