<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Office Requisition Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="finance-office-requisition-payments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'officeReqPaymentPayType',
            'officeReqPaymentaccount',
            'reqrecepitNumber',
            'officeReqPaymentTotalAmt',
            'unallocatedAmount',
            'date',
            'officeReqPaymentofficeReqRef',
            'officeReqPaymentPrivateNote:ntext',
            'officeReqPaymentCreatedAt',
            'officeReqPaymentUpdatedAt',
            'officeReqPaymentDeletedAt',
            'userId',
            'createdBy',
            'approvedBy',
            'paymentstatus',
        ],
    ]) ?>

</div>
