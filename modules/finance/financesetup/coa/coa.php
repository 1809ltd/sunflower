<?php

namespace app\modules\finance\financesetup\coa;

/**
 * coa module definition class
 */
class coa extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financesetup\coa\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
