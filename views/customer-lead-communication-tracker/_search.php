<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerLeadCommunicationTrackerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-lead-communication-tracker-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'leadAppointId') ?>

    <?= $form->field($model, 'leadId') ?>

    <?= $form->field($model, 'commLeadTxnDate') ?>

    <?= $form->field($model, 'commLeadMeetingSubject') ?>

    <?php // echo $form->field($model, 'commLeadPartiesInvolved') ?>

    <?php // echo $form->field($model, 'commLeadFormOfCommunication') ?>

    <?php // echo $form->field($model, 'commLeadNoted') ?>

    <?php // echo $form->field($model, 'commLeadNextDate') ?>

    <?php // echo $form->field($model, 'commLeadCreatedAt') ?>

    <?php // echo $form->field($model, 'commLeadLastUpdatedAt') ?>

    <?php // echo $form->field($model, 'commLeadDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
