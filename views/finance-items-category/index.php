<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Items Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-items-category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Items Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'financeItemCategoryName',
            'financeItemCategoryDescription:ntext',
            'financeItemCategoryParentId',
            'financeItemCategoryStatus',
            //'userId',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
