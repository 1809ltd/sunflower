<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_department".
 *
 * @property int $id
 * @property int $companyId
 * @property string $departmentName
 * @property string $departmentCode
 * @property string $departmentTimestamp
 * @property string $departmentUpdatedAt
 * @property string $departmentDeleteAt
 * @property int $departmentStatus
 * @property int $userId
 */
class CompanyDepartment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyId', 'departmentName', 'departmentCode'], 'required'],
            [['companyId', 'departmentStatus', 'userId'], 'integer'],
            [['departmentTimestamp', 'departmentUpdatedAt', 'departmentDeleteAt', 'userId'], 'safe'],
            [['departmentName'], 'string', 'max' => 255],
            [['departmentCode'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'companyId' => 'Company Name',
            'departmentName' => 'Department Name',
            'departmentCode' => 'Department Code',
            'departmentTimestamp' => 'Department Timestamp',
            'departmentUpdatedAt' => 'Department Updated At',
            'departmentDeleteAt' => 'Department Delete At',
            'departmentStatus' => 'Department Status',
            'userId' => 'User ID',
        ];
    }

    /**
    * @return \yii\db\ActiveQuery
    */
   public function getAssetSiteLocations()
   {
       return $this->hasMany(AssetSiteLocation::className(), ['id' => 'departmentId']);
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getCompany()
   {
       return $this->hasOne(CompanyDetails::className(), ['id' => 'companyId']);
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getUser()
   {
       return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
   }
}
