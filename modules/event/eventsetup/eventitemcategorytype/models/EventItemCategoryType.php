<?php

namespace app\modules\event\eventsetup\eventitemcategorytype\models;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
/*AssetCategoryType*/
use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType;
/*User details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "event_item_category_type".
 *
 * @property int $id
 * @property int $itemId
 * @property int $categorytypeId
 * @property double $qty
 * @property int $status
 * @property int $userId
 * @property int $createdBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property FinanceItems $item
 * @property AssetCategoryType $categorytype
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 */
class EventItemCategoryType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_item_category_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemId', 'categorytypeId', 'qty', 'status', 'userId', 'createdBy', 'createdAt'], 'required'],
            [['itemId', 'categorytypeId', 'status', 'userId', 'createdBy'], 'integer'],
            [['qty'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['itemId', 'categorytypeId'], 'unique', 'targetAttribute' => ['itemId', 'categorytypeId']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['categorytypeId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetCategoryType::className(), 'targetAttribute' => ['categorytypeId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'itemId' => 'Service',
            'categorytypeId' => 'Asset Category',
            'qty' => 'Qty',
            'status' => 'Status',
            'userId' => 'User',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorytype()
    {
        return $this->hasOne(AssetCategoryType::className(), ['id' => 'categorytypeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }
}
