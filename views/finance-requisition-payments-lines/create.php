<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines */

$this->title = Yii::t('app', 'Create Finance Requisition Payments Lines');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Requisition Payments Lines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-requisition-payments-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
