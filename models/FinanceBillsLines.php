<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_bills_lines".
 *
 * @property int $id
 * @property int $biilsId
 * @property string $biilsLinesVendorsRef
 * @property string $biilsLinesDescription
 * @property double $biilsLinespacks
 * @property double $biilsLinesQty
 * @property double $billlinePriceperunit
 * @property double $biilsLinesAmount
 * @property double $billLinesTaxCodeRef
 * @property double $biilsLinesTaxAmount
 * @property int $accountId
 * @property int $billsLinesBillableStatus
 * @property string $billsLinesCreatedAt
 * @property string $billsLinesDeleteAt
 * @property string $billsLinesUpdatedAt
 * @property int $userId
 *
 * @property FinanceBills $biils
 * @property UserDetails $user
 * @property FinanceAccounts $account
 */
class FinanceBillsLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_bills_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['biilsLinespacks', 'biilsLinesQty', 'billlinePriceperunit', 'biilsLinesAmount', 'billLinesTaxCodeRef', 'biilsLinesTaxAmount', 'accountId'], 'required'],
            [['biilsId', 'accountId', 'billsLinesBillableStatus', 'userId'], 'integer'],
            [['biilsLinesDescription'], 'string'],
            [['biilsLinespacks', 'biilsLinesQty', 'billlinePriceperunit', 'biilsLinesAmount', 'billLinesTaxCodeRef', 'biilsLinesTaxAmount'], 'number'],
            [['biilsId', 'billsLinesBillableStatus', 'userId', 'billsLinesCreatedAt', 'billsLinesDeleteAt', 'billsLinesUpdatedAt'], 'safe'],
            [['biilsLinesVendorsRef'], 'string', 'max' => 50],
            [['biilsId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceBills::className(), 'targetAttribute' => ['biilsId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['accountId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['accountId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'biilsId' => 'Biils',
            'biilsLinesVendorsRef' => 'Service',
            'biilsLinesDescription' => 'Description',
            'biilsLinespacks' => 'Days',
            'biilsLinesQty' => 'Qty',
            'billlinePriceperunit' => 'Priceperunit',
            'biilsLinesAmount' => 'Amount',
            'billLinesTaxCodeRef' => 'Tax Code',
            'biilsLinesTaxAmount' => 'Tax Amount',
            'accountId' => 'Account',
            'billsLinesBillableStatus' => 'Status',
            'billsLinesCreatedAt' => 'Created At',
            'billsLinesDeleteAt' => 'Delete At',
            'billsLinesUpdatedAt' => 'Updated At',
            'userId' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBiils()
    {
        return $this->hasOne(FinanceBills::className(), ['id' => 'biilsId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'accountId']);
    }
}
