<?php

namespace app\modules\finance\receipt;

/**
 * receipt module definition class
 */
class receipt extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\receipt\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Invoice Payment Sub Module*/
          'invoicepayment' => [
              'class' => 'app\modules\finance\receipt\invoicepayment\invoicepayment',
          ],
        ];

        // custom initialization code goes here
    }
}
