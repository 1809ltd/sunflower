<?php

namespace app\modules\company\companydepartment\models;
use app\modules\company\companydetails\models\CompanyDetails;
use app\models\UserDetails;
use Yii;

/**
 * This is the model class for table "company_department".
 *
 * @property int $id
 * @property int $companyId
 * @property string $departmentName
 * @property string $departmentCode
 * @property string $departmentTimestamp
 * @property string $departmentUpdatedAt
 * @property string $departmentDeleteAt
 * @property int $departmentStatus
 * @property int $userId
 *
 * @property AssetSiteLocation[] $assetSiteLocations
 * @property CompanyDetails $company
 * @property UserDetails $user
 */
class CompanyDepartment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_department';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyId', 'departmentName', 'userId'], 'required'],
            [['companyId', 'departmentStatus', 'userId'], 'integer'],
            [['departmentTimestamp', 'departmentUpdatedAt', 'departmentDeleteAt'], 'safe'],
            [['departmentName'], 'string', 'max' => 255],
            [['departmentCode'], 'string', 'max' => 50],
            [['companyId'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyDetails::className(), 'targetAttribute' => ['companyId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'companyId' => 'Company',
            'departmentName' => 'Department Name',
            'departmentCode' => 'Department Code',
            'departmentTimestamp' => 'Department Timestamp',
            'departmentUpdatedAt' => 'Department Updated At',
            'departmentDeleteAt' => 'Department Delete At',
            'departmentStatus' => 'Department Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetSiteLocations()
    {
        return $this->hasMany(AssetSiteLocation::className(), ['departmentId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(CompanyDetails::className(), ['id' => 'companyId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
