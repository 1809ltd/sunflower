"use strict";
var initz = function () {
    "use strict";
    $(document).ready(function () {
        var table = "";
        table = $('#table').DataTable({
            "order"       : [ [ 0, 'desc' ] ],
        });
    });
};
var table = function () {
    "use strict";
    return {
        init: function () {
            initz();
        }
    }
}();
$(function () {
    table.init();
});