<?php

namespace app\modules\inventory\asset\assetamortization;

/**
 * assetamortization module definition class
 */
class assetamortization extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\asset\assetamortization\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
