<?php

namespace app\modules\inventory\asset\assetregistration;

/**
 * assetregistration module definition class
 */
class assetregistration extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\asset\assetregistration\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
