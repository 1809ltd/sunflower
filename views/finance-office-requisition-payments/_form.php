<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-office-requisition-payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'officeReqPaymentPayType')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'officeReqPaymentaccount')->textInput() ?>

    <?= $form->field($model, 'reqrecepitNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'officeReqPaymentTotalAmt')->textInput() ?>

    <?= $form->field($model, 'unallocatedAmount')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'officeReqPaymentofficeReqRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'officeReqPaymentPrivateNote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'officeReqPaymentCreatedAt')->textInput() ?>

    <?= $form->field($model, 'officeReqPaymentUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'officeReqPaymentDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <?= $form->field($model, 'approvedBy')->textInput() ?>

    <?= $form->field($model, 'paymentstatus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
