<?php

namespace app\modules\finance\creditMemo\vendorcreditMemo\controllers;

use Yii;
use app\modules\finance\creditMemo\vendorcreditMemo\models\FinanceVendorCreditMemo;
use app\modules\finance\creditMemo\vendorcreditMemo\models\FinanceVendorCreditMemoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models\FinanceVendorCreditMemoLines;
use app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models\FinanceVendorCreditMemoLinesSearch;

/**
 * FinanceVendorCreditMemoController implements the CRUD actions for FinanceVendorCreditMemo model.
 */
class FinanceVendorCreditMemoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


                /*Getting the time updated*/
                public function beforeSave() {
                  if ($this->isNewRecord) {
                    // code...

                    $this->vendorCreditMemoStatus = new \yii\db\Expression('NOW()');

                  }else {
                    // code...
                    $this->vendorCreditMemoStatus = new \yii\db\Expression('NOW()');
                  }
                  return parent::beforeSave();
                }

                /**
                 * Lists all FinanceVendorCreditMemo models.
                 * @return mixed
                 */
                public function actionIndex()
                {
                  $this->layout = '@app/views/layouts/addformdatatablelayout';
                  // $this->layout='addformdatatablelayout';
                    $searchModel = new FinanceVendorCreditMemoSearch();
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return $this->render('index', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                    ]);
                }

                /**
                 * Displays a single FinanceVendorCreditMemo model.
                 * @param integer $id
                 * @return mixed
                 * @throws NotFoundHttpException if the model cannot be found
                 */
                public function actionView($id)
                {
                  $this->layout = '@app/views/layouts/addformdatatablelayout';
                  // $this->layout='addformdatatablelayout';
                    return $this->render('view', [
                        'model' => $this->findModel($id),
                    ]);
                }

                /**
                 * Creates a new FinanceVendorCreditMemo model.
                 * If creation is successful, the browser will be redirected to the 'view' page.
                 * @return mixed
                 */
                public function actionCreate()
                {
                  $this->layout = '@app/views/layouts/addformdatatablelayout';
                  // $this->layout='addformdatatablelayout';
                    $model = new FinanceVendorCreditMemo();
                    //Getting user Log in details
                    $model->userId = Yii::$app->user->identity->id;


                    $modelsFinanceVendorCreditMemoLines = [new FinanceVendorCreditMemoLines];

                    if ($model->load(Yii::$app->request->post()) && $model->save()) {

                      $modelsFinanceVendorCreditMemoLines = Model::createMultiple(FinanceVendorCreditMemoLines::classname());
                      Model::loadMultiple($modelsFinanceVendorCreditMemoLines, Yii::$app->request->post());

                      // ajax validation
                      /*
                      if (Yii::$app->request->isAjax) {
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return ArrayHelper::merge(
                                ActiveForm::validateMultiple($modelsFinanceVendorCreditMemoLines),
                                ActiveForm::validate($model)
                            );
                        }*/

                      // validate all models
                      $valid = $model->validate();
                      $valid = Model::validateMultiple($modelsFinanceVendorCreditMemoLines) && $valid;

                      if ($valid) {
                          $transaction = \Yii::$app->db->beginTransaction();
                          try {
                              if ($flag = $model->save(false)) {
                                  foreach ($modelsFinanceVendorCreditMemoLines as $modelFinanceVendorCreditMemoLines) {

                                    $modelFinanceVendorCreditMemoLines->vendorCreditMemoId = $model->id;
                                    $modelFinanceVendorCreditMemoLines->userId = $model->userId;
                                    $modelFinanceVendorCreditMemoLines->vendorCreditMemoLineStatus = $model->vendorCreditMemoStatus;

                                      if (! ($flag = $modelFinanceVendorCreditMemoLines->save(false))) {
                                          $transaction->rollBack();
                                          break;
                                      }
                                  }
                              }
                              if ($flag) {
                                  $transaction->commit();
                                  return $this->redirect(['view', 'id' => $model->id]);
                              }

                          } catch (Exception $e) {

                              $transaction->rollBack();

                          }
                      }


                    }

                    return $this->render('create', [
                        'model' => $model,
                          'modelsFinanceVendorCreditMemoLines' => (empty($modelsFinanceVendorCreditMemoLines)) ? [new FinanceVendorCreditMemoLines] : $modelsFinanceVendorCreditMemoLines
                    ]);


                    // if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    //     return $this->redirect(['view', 'id' => $model->id]);
                    // }
                    //
                    // return $this->render('create', [
                    //     'model' => $model,
                    // ]);
                }

                /**
                 * Updates an existing FinanceVendorCreditMemo model.
                 * If update is successful, the browser will be redirected to the 'view' page.
                 * @param integer $id
                 * @return mixed
                 * @throws NotFoundHttpException if the model cannot be found
                 */

                public function actionUpdate($id)
                {

                  // $this->layout='addformdatatablelayout';
                  $this->layout = '@app/views/layouts/addformdatatablelayout';
                    $model = $this->findModel($id);
                    //Getting user Log in details
                    $model->userId = Yii::$app->user->identity->id;



                    $modelsFinanceVendorCreditMemoLines = $this->getFinanceVendorCreditMemoLines($model->id);

                    if ($model->load(Yii::$app->request->post())) {

                          // code...

                          //Geting the time created Timestamp
                          // $model->customerCreateTime= new \yii\db\Expression('NOW()');

                          //Getting user Log in details
                          // $model->userId= "1";

                          //Generating Estimate Number

                          // $estimateNum=$model->getEstnumber();
                          // $model->estimateNumber= $estimateNum;
                          $model->save();

                          //after Saving the customer Details

                          $oldIDs = ArrayHelper::map($modelsFinanceVendorCreditMemoLines, 'id', 'id');
                          $modelsFinanceVendorCreditMemoLines = Model::createMultiple(FinanceVendorCreditMemoLines::classname(), $modelsFinanceVendorCreditMemoLines);
                          Model::loadMultiple($modelsFinanceVendorCreditMemoLines, Yii::$app->request->post());
                          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceVendorCreditMemoLines, 'id', 'id')));

                          // ajax validation
                          if (Yii::$app->request->isAjax) {
                              Yii::$app->response->format = Response::FORMAT_JSON;
                              return ArrayHelper::merge(
                                  ActiveForm::validateMultiple($modelsFinanceVendorCreditMemoLines),
                                  ActiveForm::validate($model)
                              );
                          }

                          // validate all models
                          $valid = $model->validate();
                          $valid = Model::validateMultiple($modelsFinanceVendorCreditMemoLines) && $valid;

                          if ($valid) {
                              $transaction = \Yii::$app->db->beginTransaction();
                              try {
                                  if ($flag = $model->save(false)) {
                                      if (! empty($deletedIDs)) {
                                          FinanceVendorCreditMemoLines::deleteAll(['id' => $deletedIDs]);
                                      }
                                      foreach ($modelsFinanceVendorCreditMemoLines as $modelFinanceVendorCreditMemoLines) {

                                        $modelFinanceVendorCreditMemoLines->vendorCreditMemoId = $model->id;
                                        $modelFinanceVendorCreditMemoLines->userId = $model->userId;
                                        $modelFinanceVendorCreditMemoLines->vendorCreditMemoLineStatus = $model->vendorCreditMemoStatus;


                                          if (! ($flag = $modelFinanceVendorCreditMemoLines->save(false))) {
                                              $transaction->rollBack();
                                              break;
                                          }
                                      }
                                  }
                                  if ($flag) {
                                      $transaction->commit();
                                      return $this->redirect(['view', 'id' => $model->id]);
                                  }
                              } catch (Exception $e) {
                                  $transaction->rollBack();
                              }
                          }

                          // return $this->redirect(['view', 'id' => $model->id]);

                    } else {
                      // code...

                      //load the create form
                      return $this->render('update', [
                          'model' => $model,
                          'modelsFinanceVendorCreditMemoLines' => (empty($modelsFinanceVendorCreditMemoLines)) ? [new FinanceVendorCreditMemoLines] : $modelsFinanceVendorCreditMemoLines
                      ]);
                    }

                }




                public function getFinanceVendorCreditMemoLines($id)
                {
                  $model = FinanceVendorCreditMemoLines::find()->where(['vendorCreditMemoId' => $id])->all();
                  return $model;
                }

                /**
                 * Deletes an existing FinanceVendorCreditMemo model.
                 * If deletion is successful, the browser will be redirected to the 'index' page.
                 * @param integer $id
                 * @return mixed
                 * @throws NotFoundHttpException if the model cannot be found
                 */
                public function actionDelete($id)
                {
                  $this->layout = '@app/views/layouts/addformdatatablelayout';
                  // $this->layout='addformdatatablelayout';
                    $this->findModel($id)->delete();

                    return $this->redirect(['index']);
                }

                /**
                 * Finds the FinanceVendorCreditMemo model based on its primary key value.
                 * If the model is not found, a 404 HTTP exception will be thrown.
                 * @param integer $id
                 * @return FinanceVendorCreditMemo the loaded model
                 * @throws NotFoundHttpException if the model cannot be found
                 */

                protected function findModel($id)
                {
                    if (($model = FinanceVendorCreditMemo::findOne($id)) !== null) {
                        return $model;
                    }

                    throw new NotFoundHttpException('The requested page does not exist.');
                }
            }
