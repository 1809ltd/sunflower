<?php

namespace app\modules\event\eventdocs\orderfroms\orderformsitems\controllers;

use yii\web\Controller;

/**
 * Default controller for the `orderformsitems` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
