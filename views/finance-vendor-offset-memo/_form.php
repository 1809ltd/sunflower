<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use dosamigos\datepicker\DatePicker;

use kartik\select2\Select2;
use kartik\depdrop\DepDrop;

use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*Bills*/
use app\modules\finance\expense\bills\models\FinanceBills;


/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemo */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="panel-body finance-vendor-offset-memo-form">

                  <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>


                  <div class="col-sm-3">
                    <div class="form-group">

                      <?php $customer = ArrayHelper::map(VendorCompanyDetails::find()->all(),'id','vendorCompanyName'); ?>
                      <?= $form->field($model, 'vendorId')->widget(Select2::classname(), [
                          'data' => $customer,
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Customer ...',
                                        'onChange'=>'$.post("index.php?r=finance-invoice/lists&customerId='.'"+$(this).val(),function(data){
                                          $("select#billRef").html(data);
                                        });'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'refernumber')->textInput()?>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'vendorOffsetMemoAccount')->widget(Select2::classname(), [
                          'data' => ArrayHelper::map(FinanceAccounts::find()->all(),'id','fullyQualifiedName'),
                          'language' => 'en',
                          'options' => ['placeholder' => 'Select a Account ...'],
                          'pluginOptions' => [
                              'allowClear' => true
                          ],
                      ]); ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'vendorOffsetMemoTxnDate')->widget(\yii\jui\DatePicker::class, [
                            //'language' => 'ru',
                            'options' => ['class' => 'form-control'],
                            'inline' => false,
                            'dateFormat' => 'yyyy-MM-dd',
                        ]); ?>

                    </div>
                  </div>



                  <div class="col-sm-3">
                    <div class="form-group">


                    </div>
                  </div>

                  <div class="panel-body"></div>

                  <div class="panel-body">

                    <div class="panel panel-pvr panel--style--1">
                      <div class="panel-heading">
                          <div class="panel-heading-btn">
                              <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                              <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                              <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                              <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                          </div>
                          <h4 class="panel-title">Add offset Memo Item</h4>
                      </div>
                      <div class="panel-body">
                           <?php DynamicFormWidget::begin([
                              'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                              'widgetBody' => '.container-items', // required: css class selector
                              'widgetItem' => '.item', // required: css class
                              'limit' => 10, // the maximum times, an element can be cloned (default 999)
                              'min' => 1, // 0 or 1 (default 1)
                              'insertButton' => '.add-item', // css class
                              'deleteButton' => '.remove-item', // css class
                              'model' => $modelsFinanceVendorOffsetMemoLines[0],
                              'formId' => 'dynamic-form',
                              'formFields' => [

                                'billRef',
                                'vendorOffsetMemoLineDescription',
                                'vendorOffsetMemoLineAmount',
                                //'vendorOffsetMemoLineStatus',

                              ],
                          ]); ?>

                          <div class="container-items"><!-- widgetContainer -->
                            <!-- Loopping Items in the Lists -->
                          <?php foreach ($modelsFinanceVendorOffsetMemoLines as $i => $modelFinanceVendorOffsetMemoLines): ?>
                              <div class="item panel-pvr panel--style--2 "><!-- widgetBody -->


                                  <div class="panel-heading">
                                      <h3 class="panel-title pull-left">offset Memo Item</h3>
                                      <div class="pull-right">
                                          <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                          <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                  </div>


                                  <div class="panel-body">
                                      <?php
                                          // necessary for update action.
                                          if (! $modelFinanceVendorOffsetMemoLines->isNewRecord) {
                                              echo Html::activeHiddenInput($modelFinanceVendorOffsetMemoLines, "[{$i}]id");
                                          }
                                      ?>
                                      <div class="col-sm-4">
                                        <div class="form-group">

                                          <?= $form->field($modelFinanceVendorOffsetMemoLines, "[{$i}]billRef")->widget(Select2::classname(), [
                                              'data' => ArrayHelper::map(FinanceBills::find()->all(),'id','billRef'),
                                              'language' => 'en',
                                              'class' => 'billRef form-control',
                                              'options' => ['placeholder' => 'Select a Invoice ...'],
                                              'pluginOptions' => [
                                                  'allowClear' => true
                                              ],
                                          ]); ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceVendorOffsetMemoLines, "[{$i}]vendorOffsetMemoLineDescription")->textarea(['rows' => 6]) ?>
                                        </div>
                                      </div>

                                      <div class="col-sm-4">
                                        <div class="form-group">
                                          <?= $form->field($modelFinanceVendorOffsetMemoLines, "[{$i}]vendorOffsetMemoLineAmount")->textInput([
                                          'maxlength' => true,
                                          'class' => 'sumTaxPart form-control']) ?>
                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>
                                      <div class="col-sm-3">
                                        <div class="form-group">

                                        </div>
                                      </div>

                                  </div>
                              </div>
                          <?php endforeach; ?>
                          </div>
                          <?php DynamicFormWidget::end(); ?>
                      </div>
                  </div>

                  </div>

                    <div class="panel-body"></div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <?= $form->field($model, 'vendorOffsetMemoNote')->textarea(['rows' => 6]) ?>

                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <?= $form->field($model, 'offsetAmount')->textInput(['readonly' => true,'class' => 'tax form-control']) ?>
                      </div>
                    </div>

                    <div class="col-sm-3">
                      <div class="form-group">
                        <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                        <?= $form->field($model, 'vendorOffsetMemoStatus')->dropDownList($status, ['prompt'=>'Select Status']);?>
                      </div>
                    </div>


                    <?= $form->field($model, 'userId')->hiddenInput(['value'=> "1"])->label(false);?>


                    <div class="panel-body"></div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= Html::submitButton($modelFinanceVendorOffsetMemoLines->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success']) ?>
                    </div>
                  </div>

                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->

<?php

/*getting the totalamount and the total tax amount */
$script = <<<EOD
    var getAmount = function() {

        //getting the elemetent ID
        var items = $(".item");

        //intialization on amount figure and the total figure
        var tax=0

        items.each(function (index, elem) {

            //getting specific elements

            //getting the tax amount figures
            var taxValue = $(elem).find(".sumTaxPart").val();

            //getting total tax amountValue
            tax = parseFloat(tax) + parseFloat(taxValue);

            //assing value to the total tax amount
            $(".tax").val(tax);

        });
    };

    //Bind new elements to support the function too
    $(".container-items").on("change", function() {
        getAmount();
    });
EOD;
$this->registerJs($script);
/*end getting the totalamount */
?>

<div class="finance-vendor-offset-memo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vendorOffsetMemoAccount')->textInput() ?>

    <?= $form->field($model, 'refernumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendorOffsetMemoTxnDate')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoNote')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'vendorId')->textInput() ?>

    <?= $form->field($model, 'offsetAmount')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoStatus')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoCreatedAt')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'vendorOffsetMemoDeletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
