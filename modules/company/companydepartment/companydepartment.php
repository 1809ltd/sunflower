<?php

namespace app\modules\company\companydepartment;

/**
 * companydepartment module definition class
 */
class companydepartment extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\company\companydepartment\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
