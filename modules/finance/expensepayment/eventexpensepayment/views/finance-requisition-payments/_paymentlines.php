<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="finance-bills-payments-lines-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
    				// 'reqpaymentId',
    				// 'eventReqId',
    				[
    					'label' => 'Requstion No:',
    					'attribute'=>'eventReqId',
    					'value'=>'eventReq.requstionNumber',
    				],
    				'amount',
    				'status',
    				//'deletedAt',
    				//'created_at',
    				//'updated_at',
    				//'userId',
        ],
    ]); ?>

</div>
