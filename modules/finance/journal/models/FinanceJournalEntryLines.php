<?php

namespace app\modules\finance\journal\models;

/*Chart of Accounts*/
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*User Details*/
use app\modules\user\userdetails\models\UserDetails;


use Yii;

/**
 * This is the model class for table "finance_journal_entry_lines".
 *
 * @property int $id
 * @property int $journalId
 * @property int $accountId
 * @property int $transactionId
 * @property string $postingType
 * @property string $description
 * @property double $drAmount
 * @property double $crAmount
 * @property string $createAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $status
 * @property int $userId
 *
 * @property FinanceJournalEntry $journal
 * @property UserDetails $user
 * @property FinanceAccounts $account
 */
class FinanceJournalEntryLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_journal_entry_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['accountId',  'description', 'drAmount', 'crAmount'], 'required'],
            [['journalId', 'accountId', 'transactionId', 'status', 'userId'], 'integer'],
            [['description'], 'string'],
            [['drAmount', 'crAmount'], 'number'],
            [['journalId', 'postingType','transactionId', 'createAt', 'updatedAt', 'deletedAt', 'status', 'userId'], 'safe'],
            [['postingType'], 'string', 'max' => 50],
            [['journalId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceJournalEntry::className(), 'targetAttribute' => ['journalId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['accountId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['accountId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'journalId' => 'Journal',
            'accountId' => 'Account',
            'transactionId' => 'Transaction ID',
            'postingType' => 'Posting Type',
            'description' => 'Description',
            'drAmount' => 'Dr Amount',
            'crAmount' => 'Cr Amount',
            'createAt' => 'Create At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJournal()
    {
        return $this->hasOne(FinanceJournalEntry::className(), ['id' => 'journalId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(FinanceAccounts::className(), ['id' => 'accountId']);
    }
}
