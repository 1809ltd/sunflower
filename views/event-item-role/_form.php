<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

use app\models\EventRole;
use  app\models\FinanceItems;

/* @var $this yii\web\View */
/* @var $model app\models\EventItemRole */
/* @var $form yii\widgets\ActiveForm */
?>

<!-- begin row -->
<div class="row" id="show_multiple_filter_div" style="display: none;">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title">Register Department</h4>
            </div>
            <div class="panel-body">
                <div class="row" id="append_col">

                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->
</div>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="event-item-role-form">

                <?php $form = ActiveForm::begin(); ?>



                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'itemId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(FinanceItems::find()->all(),'id','itemsName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Service ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">

                    <?= $form->field($model, 'roleId')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(EventRole::find()->all(),'id','roleName'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select a Select role ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?= $form->field($model, 'qty')->textInput(['maxlength' => true]) ?>

                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

                    <?= $form->field($model, 'status')->dropDownList($status, ['prompt'=>'Select Status']);?>

                  </div>
                </div>

                <?= $form->field($model, 'userid')->hiddenInput(['value'=> "1"])->label(false);?>

                <div class="col-sm-3">
                  <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                  </div>
                </div>



                <div class="col-sm-3">
                  <div class="form-group">

                  </div>
                </div>

                  <div class="form-group">

                  </div>

                  <?php ActiveForm::end(); ?>

              </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
