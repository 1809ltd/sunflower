<?php

namespace app\modules\personnel\controllers;

use Yii;
use yii\web\Controller;

/*Get Event Details*/
use app\models\EventDetails;
use app\models\EventDetailsSearch;

// Personnel details
use app\modules\personnel\personneldetails\models\PersonnelDetails;
use app\modules\personnel\personneldetails\models\PersonnelDetailsSearch;

/**
 * Default controller for the `personnel` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

      $searchModel = new PersonnelDetailsSearch();
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      $events= EventDetails::find()->all();
      $shereheglobal=[];

      foreach ($events as $sherehe) {
        // code...
        $event = new \yii2fullcalendar\models\Event();
        $event->id = $sherehe->id;
        // $event->title = 'Testing';
        $event->title =$sherehe->eventName;
        // $event->start = date('Y-m-d\Th:m:s\Z',strtotime('tomorrow 6am'));
        // date('Y-m-d\Th:i:s\Z',strtotime($time->date_start.' '.$time->time_start));
        // $event->start = $sherehe->eventStartDate;
        $event->start = date('Y-m-d\Th:i:s\Z',strtotime($sherehe->eventStartDate));
        $event->end = date('Y-m-d\Th:i:s\Z',strtotime($sherehe->eventEndDate));
        $shereheglobal[] = $event;
      }

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
          'events'=> $shereheglobal,
      ]);
      // return $this->render('index');
    }
}
