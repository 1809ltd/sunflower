<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-vendor-offset-memo-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vendorId') ?>

    <?= $form->field($model, 'vendorOffsetMemoId') ?>

    <?= $form->field($model, 'billRef') ?>

    <?= $form->field($model, 'vendorOffsetMemoLineDescription') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoLineAmount') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoLineStatus') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoLineCreatedAt') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoLineUpdatedAt') ?>

    <?php // echo $form->field($model, 'creidtDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
