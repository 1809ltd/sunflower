<?php

namespace app\controllers;

use Yii;
use app\models\FinanceInvoice;
use app\models\FinanceInvoiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*GEtting the Lines relationship*/
use app\models\FinanceInvoiceLines;
use app\models\FinanceInvoiceLinesSearch;

/**
 * FinanceInvoiceController implements the CRUD actions for FinanceInvoice model.
 */
class FinanceInvoiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->invoicedUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->invoicedUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceInvoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='addformdatatablelayout';
        $searchModel = new FinanceInvoiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceInvoice model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceInvoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='addformdatatablelayout';
        $model = new FinanceInvoice();
        $modelsFinanceInvoiceLines = [new FinanceInvoiceLines];

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $modelsFinanceInvoiceLines = Model::createMultiple(FinanceInvoiceLines::classname());
          Model::loadMultiple($modelsFinanceInvoiceLines, Yii::$app->request->post());

          // ajax validation
          /*
          if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsFinanceInvoiceLines),
                    ActiveForm::validate($model)
                );
            }*/

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceInvoiceLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      foreach ($modelsFinanceInvoiceLines as $modelFinanceInvoiceLines) {

                        $modelFinanceInvoiceLines->invoiceId = $model->id;
                        $modelFinanceInvoiceLines->userId = $model->invoicedCreatedby;


                          if (! ($flag = $modelFinanceInvoiceLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }

              } catch (Exception $e) {

                  $transaction->rollBack();

              }
          }


        }

        return $this->render('create', [
            'model' => $model,
              'modelsFinanceInvoiceLines' => (empty($modelsFinanceInvoiceLines)) ? [new FinanceInvoiceLines] : $modelsFinanceInvoiceLines
        ]);

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Updates an existing FinanceInvoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);


        $modelsFinanceInvoiceLines = $this->getFinanceInvoiceLines($model->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

          $oldIDs = ArrayHelper::map($modelsFinanceInvoiceLines, 'id', 'id');
          $modelsFinanceInvoiceLines = Model::createMultiple(FinanceInvoiceLines::classname(), $modelsFinanceInvoiceLines);
          Model::loadMultiple($modelsFinanceInvoiceLines, Yii::$app->request->post());
          $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsFinanceInvoiceLines, 'id', 'id')));

          // ajax validation
          if (Yii::$app->request->isAjax) {
              Yii::$app->response->format = Response::FORMAT_JSON;
              return ArrayHelper::merge(
                  ActiveForm::validateMultiple($modelsFinanceInvoiceLines),
                  ActiveForm::validate($model)
              );
          }

          // validate all models
          $valid = $model->validate();
          $valid = Model::validateMultiple($modelsFinanceInvoiceLines) && $valid;

          if ($valid) {
              $transaction = \Yii::$app->db->beginTransaction();
              try {
                  if ($flag = $model->save(false)) {
                      if (! empty($deletedIDs)) {
                          FinanceInvoiceLines::deleteAll(['id' => $deletedIDs]);
                      }
                      foreach ($modelsFinanceInvoiceLines as $modelFinanceInvoiceLines) {
                          $modelFinanceInvoiceLines->invoiceId = $model->id;
                          if (! ($flag = $modelFinanceInvoiceLines->save(false))) {
                              $transaction->rollBack();
                              break;
                          }
                      }
                  }
                  if ($flag) {
                      $transaction->commit();
                      return $this->redirect(['view', 'id' => $model->id]);
                  }
              } catch (Exception $e) {
                  $transaction->rollBack();
              }
          }
        }

        return $this->render('update', [
            'model' => $model,
            'modelsFinanceInvoiceLines' => (empty($modelsFinanceInvoiceLines)) ? [new FinanceInvoiceLines] : $modelsFinanceInvoiceLines
        ]);

        //
        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }
        //
        // return $this->render('update', [
        //     'model' => $model,
        // ]);
    }

    /**
     * Deletes an existing FinanceInvoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout='addformdatatablelayout';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function getFinanceInvoiceLines($id)
    {
      $model = FinanceInvoiceLines::find()->where(['invoiceId' => $id])->all();
      return $model;
    }
    public function actionLists($customerId)
    {
      // code...
      $this->layout='addformdatatablelayout';

      $invoiceNumber = FinanceInvoice::find()
                    ->where(['customerId'=>$customerId])
                    ->all();

      $countinvoiceNumber = FinanceInvoice::find()
                            ->where(['customerId'=>$customerId])
                            ->count();


      // print_r($invoiceNumber);
      // die();

      if ($countinvoiceNumber>0) {
        // code...

        foreach ($invoiceNumber as $invoice) {
          // code...
          echo "<option value= '".$invoice->invoiceNumber."'>".$invoice->invoiceNumber."</option>";

        }

      } else {
        // code...

        echo "<option> No Details $customerId</option>";

      }



    }

    /**
     * Finds the FinanceInvoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceInvoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceInvoice::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
