<?php

namespace app\modules\finance\productsetup\financeeventactivity\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;

/**
 * FinanceEventActivitySearch represents the model behind the search form of `app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity`.
 */
class FinanceEventActivitySearch extends FinanceEventActivity
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','activityStatus', 'userId'], 'integer'],
            [[ 'account', 'activityName', 'activityDescription', 'updatedAt', 'deletedAt', 'createdAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceEventActivity::find();
        $query->joinWith(['account0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'account' => $this->account,
            'activityStatus' => $this->activityStatus,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'createdAt' => $this->createdAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'activityName', $this->activityName])
            ->andFilterWhere(['like', 'fullyQualifiedName', $this->account])
            ->andFilterWhere(['like', 'activityDescription', $this->activityDescription]);

        return $dataProvider;
    }
}
