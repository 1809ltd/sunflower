<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-deliveryform-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'deliveryNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eventId')->textInput() ?>

    <?= $form->field($model, 'orderId')->textInput() ?>

    <?= $form->field($model, 'duedate')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'deliveryStatus')->textInput() ?>

    <?= $form->field($model, 'delivery')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'updatedAt')->textInput() ?>

    <?= $form->field($model, 'deletedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <?= $form->field($model, 'approvedBy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
