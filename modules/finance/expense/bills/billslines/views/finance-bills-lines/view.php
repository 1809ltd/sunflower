<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBillsLines */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Bills Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-bills-lines-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'biilsId',
            'biilsLinesVendorsRef',
            'biilsLinesDescription:ntext',
            'biilsLinespacks',
            'biilsLinesQty',
            'billlinePriceperunit',
            'biilsLinesAmount',
            'billLinesTaxCodeRef',
            'biilsLinesTaxAmount',
            'eventActivityId',
            'billsLinesBillableStatus',
            'billsLinesCreatedAt',
            'billsLinesDeleteAt',
            'billsLinesUpdatedAt',
            'userId',
        ],
    ]) ?>

</div>
