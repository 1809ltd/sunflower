<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'eventId') ?>

    <?= $form->field($model, 'customerId') ?>

    <?= $form->field($model, 'eventNumber') ?>

    <?= $form->field($model, 'eventName') ?>

    <?= $form->field($model, 'eventTheme') ?>

    <?php // echo $form->field($model, 'eventVistorsNumber') ?>

    <?php // echo $form->field($model, 'eventCategory') ?>

    <?php // echo $form->field($model, 'eventStartDate') ?>

    <?php // echo $form->field($model, 'eventEndDate') ?>

    <?php // echo $form->field($model, 'eventStartTime') ?>

    <?php // echo $form->field($model, 'eventEndTime') ?>

    <?php // echo $form->field($model, 'eventSetUpDateTime') ?>

    <?php // echo $form->field($model, 'eventSetDownDateTime') ?>

    <?php // echo $form->field($model, 'eventLocation') ?>

    <?php // echo $form->field($model, 'eventDescription') ?>

    <?php // echo $form->field($model, 'eventNotes') ?>

    <?php // echo $form->field($model, 'eventCreatedAt') ?>

    <?php // echo $form->field($model, 'eventLastUpdatedAt') ?>

    <?php // echo $form->field($model, 'eventDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
