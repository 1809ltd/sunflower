<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="asset-preciation-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

          //'id',
          //'asset.assetName',
          //'company.companyName',
          //'companyId',
          //'preciableCost',
          //'preciationSalvageValue',
          'assetLifeMonths',
          'preciationDateAcquired',
          'preciationDateAcquiredDepreciationMethod',
          //'preciationStatus',
          //'preciationTimestamp',
          //'preciationUpdatedAt',
          //'preciationDeletedAt',
          //'userId',
        ],
    ]); ?>

</div>
