<?php

namespace app\modules\inventory\asset\controllers;

use yii\web\Controller;

/**
 * Default controller for the `asset` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('index');
    }
}
