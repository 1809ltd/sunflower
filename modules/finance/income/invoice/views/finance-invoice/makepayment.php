<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut */

$this->title = 'Create Finance Payment';
$this->params['breadcrumbs'][] = ['label' => 'Finance Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-payment-create">
    <?= $this->render('recordpayment', [
      'payments' => $payments,
      'model' => $model,
    ]) ?>

</div>
