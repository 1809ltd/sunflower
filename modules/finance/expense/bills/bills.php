<?php

namespace app\modules\finance\expense\bills;

/**
 * bills module definition class
 */
class bills extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\bills\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Bills Lines Sub Module*/
          'billslines' => [
              'class' => 'app\modules\finance\expense\bills\billslines\billslines',
          ],
        ];

        // custom initialization code goes here
    }
}
