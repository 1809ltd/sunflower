<?php

namespace app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models\FinanceVendorCreditMemoLines;

/**
 * FinanceVendorCreditMemoLinesSearch represents the model behind the search form of `app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models\FinanceVendorCreditMemoLines`.
 */
class FinanceVendorCreditMemoLinesSearch extends FinanceVendorCreditMemoLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vendorCreditMemoId', 'vendorCreditMemoLineStatus', 'userId'], 'integer'],
            [['billRef', 'vendorCreditMemoLineDescription', 'vendorCreditMemoLineCreatedAt', 'vendorCreditMemoLineUpdatedAt', 'creidtDeletedAt'], 'safe'],
            [['vendorCreditMemoLineAmount', 'creditTaxCodeRef', 'taxamount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceVendorCreditMemoLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vendorCreditMemoId' => $this->vendorCreditMemoId,
            'vendorCreditMemoLineAmount' => $this->vendorCreditMemoLineAmount,
            'creditTaxCodeRef' => $this->creditTaxCodeRef,
            'taxamount' => $this->taxamount,
            'vendorCreditMemoLineStatus' => $this->vendorCreditMemoLineStatus,
            'vendorCreditMemoLineCreatedAt' => $this->vendorCreditMemoLineCreatedAt,
            'vendorCreditMemoLineUpdatedAt' => $this->vendorCreditMemoLineUpdatedAt,
            'creidtDeletedAt' => $this->creidtDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'billRef', $this->billRef])
            ->andFilterWhere(['like', 'vendorCreditMemoLineDescription', $this->vendorCreditMemoLineDescription]);

        return $dataProvider;
    }
}
