<?php

namespace app\modules\inventory;

/**
 * inventory module definition class
 */
class inventory extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Asset Set up Sub Module*/
          'assetsetup' => [
                'class' => 'app\modules\inventory\assetsetup\assetsetup',
            ],
            /*Asset*/
            'asset' => [
                 'class' => 'app\modules\inventory\asset\asset',
             ],
            /*Asset Movement*/
            'assetmovement' => [
                'class' => 'app\modules\inventory\assetmovement\assetmovement',
            ],
        ];

        // custom initialization code goes here
    }
}
