<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="company-department-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

          // 'id',
          // 'assetName',
          // 'assetDescription',
          // 'assetCategory',
          // 'assetCategoryType',
          //'assetTag:ntext',
          'assetpurchaseDate',
          'vendor',
          'assetCost',
          'assetBrand',
          'assetModel',
          'assetSerialNumber',
          //'assetQrCode:ntext',
          //'assetphoto:ntext',
          //'assetTimestamp',
          //'assetUpdatedAt',
          //'assetDeletedAt',
          //'assetStatus',
          //'userId',

        ],
    ]); ?>

</div>
