<?php

namespace app\modules\finance\offset\outhouse\outoffsetbiils\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines;

/**
 * FinanceVendorOffsetMemoLinesSearch represents the model behind the search form of `app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines`.
 */
class FinanceVendorOffsetMemoLinesSearch extends FinanceVendorOffsetMemoLines
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vendorId', 'vendorOffsetMemoId', 'billRef', 'vendorOffsetMemoLineStatus', 'userId'], 'integer'],
            [['vendorOffsetMemoLineDescription', 'vendorOffsetMemoLineCreatedAt', 'vendorOffsetMemoLineUpdatedAt', 'creidtDeletedAt'], 'safe'],
            [['vendorOffsetMemoLineAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceVendorOffsetMemoLines::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vendorId' => $this->vendorId,
            'vendorOffsetMemoId' => $this->vendorOffsetMemoId,
            'billRef' => $this->billRef,
            'vendorOffsetMemoLineAmount' => $this->vendorOffsetMemoLineAmount,
            'vendorOffsetMemoLineStatus' => $this->vendorOffsetMemoLineStatus,
            'vendorOffsetMemoLineCreatedAt' => $this->vendorOffsetMemoLineCreatedAt,
            'vendorOffsetMemoLineUpdatedAt' => $this->vendorOffsetMemoLineUpdatedAt,
            'creidtDeletedAt' => $this->creidtDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'vendorOffsetMemoLineDescription', $this->vendorOffsetMemoLineDescription]);

        return $dataProvider;
    }
}
