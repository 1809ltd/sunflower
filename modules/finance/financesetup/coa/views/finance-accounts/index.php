<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/*Dropdown action Bar*/
use yii\bootstrap\ButtonDropdown;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceAccountsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();


$this->title = 'Finance Accounts';
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- this row will not appear when printing -->

<div class="box no-print">

    <div class="box-header">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
      <div class="form-group">
        <?php
        $gridColumns = [
          ['class' => 'yii\grid\SerialColumn'],
          //'id',
          //'companyId',
          // 'subAccount',
          //'accountParentId',
          'accountName',
          'accountNumber',
          'accountsDescription:ntext',
          'fullyQualifiedName',
          // 'accountsClassification',
          //'accountsType',
          //'accountsDetailType',
          //'accountsStatus',
          //'accountsCurrentBalance',
          //'accountsCreateTime',
          //'accountsUpdatedAt',
          //'accountsDeleteAt',
          //'userId',
          ['class' => 'yii\grid\ActionColumn'],
        ];
        // Renders a export dropdown menu
//        echo ExportMenu::widget([
//          'dataProvider' => $dataProvider,
//            // 'filterModel' => $searchModel,
//          'columns' => $gridColumns
//        ]);
        // You can choose to render your own GridView separately
        // echo \kartik\grid\GridView::widget([
        //   'dataProvider' => $dataProvider,
        //   'filterModel' => $searchModel,
        //   'columns' => $gridColumns
        // ]);
        ?>
        <p class="pull-right"><?= Html::a('Create COA', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body personnel-details-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?=  GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
            'rowOptions'=> function($model)
            {
              // code...
              if ($model->accountsStatus==1) {
                // code...
                return['class'=>'success'];
              } elseif ($model->accountsStatus==2) {
                // code...

                return['class'=>'warning'];
              } else {
                // code...
                return['class'=>'danger'];
              }

            },
            'columns' => [
                ['class' => 'kartik\grid\ExpandRowColumn',
                  'value'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    return GridView::ROW_COLLAPSED;
                  },
                  'detail'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    $searchModel = new FinanceAccountsSearch();
                    $searchModel->id = $model->id;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return Yii::$app->controller->renderPartial('_viewaccountdetails',[
                      'searchModel' => $searchModel,
                      'dataProvider' => $dataProvider,

                    ]);
                  },
              ],

              //'id',
              //'companyId',
              // 'subAccount',
              //'accountParentId',
              'accountName',
              'accountNumber',
              'accountsDescription:ntext',
              'fullyQualifiedName',
              'accountsClassification',
              //'accountsType',
              //'accountsDetailType',
              //'accountsStatus',
              //'accountsCurrentBalance',
              //'accountsCreateTime',
              //'accountsUpdatedAt',
              //'accountsDeleteAt',
              //'userId',

              /* ... GridView configuration ... */
              [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{all}',
                'buttons' => [
                  'all' => function ($url, $model, $key) {
                    return ButtonDropdown::widget([
                      'encodeLabel' => false, // if you're going to use html on the button label
                      'label' => 'Options',
                      'dropdown' => [
                        'encodeLabels' => false, // if you're going to use html on the items' labels
                        'items' => [
                          [
                            'label' => \Yii::t('yii', 'View'),
                            'url' => ['view', 'id' => $key],
                          ],
                          [
                            'label' => \Yii::t('yii', 'Update'),
                            'url' => ['update', 'id' => $key],
                            // 'visible' => Yii::$app->Permission->getSpecificPermission('save'),  // if you want to hide an item based on a condition, use this
                          ],
                          [
                            'label' => \Yii::t('yii', 'Aprrove'),
                            'url' => ['update', 'id' => $key],
                            // 'visible' => Yii::$app->Permission->getSpecificPermission('other'),  // if you want to hide an item based on a condition, use this
                          ],
                          [
                            'label' => \Yii::t('yii', 'Delete'),
                            'linkOptions' => [
                              'data' => [
                                  'method' => 'post',
                                  'confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'),
                              ],
                            ],
                            'url' => ['delete', 'id' => $key],
                            'visible' => Yii::$app->Permission->getSpecificPermission('remove'),   // same as above
                          ],
                        ],
                        'options' => [
                          'class' => 'dropdown-menu-right', // right dropdown
                        ],
                      ],
                      'options' => [
                        'class' => 'btn-default',   // btn-success, btn-info, et cetera
                      ],
                      'split' => true,    // if you want a split button
                    ]);
                  },
                ],
              ],


              // ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
<?= $this->render('printAllaccounts.php') ?>
<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   // $('li.treeview').addClass('active');
   $('li.treeview').addClass('treeview menu-open');
   $('#productsetup').addClass('active');
   $('#finance-items').addClass('active');
", View::POS_READY);
?>
