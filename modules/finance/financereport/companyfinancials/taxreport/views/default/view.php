<!-- <div class="taxreport-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <p>
        This is the view content for action "<?= $this->context->action->id ?>".
        The action belongs to the controller "<?= get_class($this->context) ?>"
        in the "<?= $this->context->module->id ?>" module.
    </p>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div> -->
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/*Tax Reports*/
use app\modules\finance\financereport\companyfinancials\taxreport\models\TaxReport;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$this->title = 'Tax Statement Report';
$this->params['breadcrumbs'][] = ['label' => 'Company Reports', 'url' => ['/finance/report/company']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Search Date And the rest -->


<div class="box no-print">
  <div class="box-header with-border">
    <h3 class="box-title">Search</h3>
    <div class="box-tools pull-right">
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <?php $form = ActiveForm::begin(); ?>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date From: </label>
          <div class="col-lg-8">
            <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                      //'language' => 'ru',
                      'options' => ['autocomplete'=>'off','class' => 'form-control'],
                      'inline' => false,
                      'dateFormat' => 'yyyy-MM-dd',
                  ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label class="col-lg-4 control-label">Date To: </label>
          <div class="col-lg-8">
                <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <div class="col-lg-8 col-lg-offset-4">
            <div class="center-align">
              <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
            </div>
          </div>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
<!-- this row will not appear when printing -->
<?php
if (!empty($tafuta)) {
  // code...
  // if the array is not empty
  ?>

  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Tax Statement</h3>
      <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>
    <div class="box">
      <div class="box-body">
        <h5 class="box-title">Tax Report</h5>
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th class="text-left">Date</th>
              <th class="text-left">Transaction Code</th>
              <th class="text-left">Tax</th>
              <th class="text-left">Description</th>
              <th class="text-left">Taxable Amount</th>
              <th class="text-left">Dr Tax Amount</th>
              <th class="text-left">Cr Tax  Amount</th>
              <!-- <th class="text-right">Dr Tax  Amount</th> -->
            </tr>
          </thead>
          <tbody>
            <?php
            $taxTotal=0;
            $url="";

            foreach ($tax as $tax){
              // code...
              $taxTotal+= $tax["cr_amount"];

              $replacetaxdramount = preg_replace(
                 '/(-)([\d\.\,]+)/ui',
                 '($2)',
                 number_format($tax->dr_amount,2,'.',',')
             );
             $replacetaxcramount = preg_replace(
                '/(-)([\d\.\,]+)/ui',
                '($2)',
                number_format($tax->cr_amount,2,'.',',')
            );
            $replacetaxableamount = preg_replace(
               '/(-)([\d\.\,]+)/ui',
               '($2)',
               number_format($tax->taxableamount,2,'.',',')
           );
           // Assign Url based on the transaction
              // Office Expense
              if ($tax->typeofTransaction=="Bill") {
                // code...
                $url= Url::to(['/finance/expense/bills/finance-bills/view','id'=>$tax->referenceId]);

              } elseif ($tax->typeofTransaction=="Vendor Credit Note") {
                // code...
                $url= Url::to(['/finance/creditMemo/vendorcreditMemo/finance-vendor-credit-memo/view','id'=>$tax->referenceId]);

              }elseif ($tax->typeofTransaction=="invoice") {
                // code...

                $url= Url::to(['/finance/income/invoice/finance-invoice/view','id'=>$tax->referenceId]);

              } elseif ($tax->typeofTransaction=="Credit Note") {
                // code...
                $url= Url::to(['/finance/creditMemo/customercreditMemo/finance-credit-memo/view','id'=>$tax->referenceId]);

              } else {
                // code...

              }
            ?>
            <tr>
              <td  class="text-left"><?= Yii::$app->formatter->asDate($tax->date, 'long');;?>)</td>
              <td  class=""><a href="<?php echo $url; ?>"><?= $tax->transactionCode;?></a></td>
              <td  class="text-left"><?= $tax->taxCode;?>  (<?= $tax->taxRate;?>)</td>
              <td  class="text-left"><?= $tax->details;?></td>
              <td  class="text-left"><?= $replacetaxableamount;?></td>
              <td  class="text-left"><?= $replacetaxdramount;?></td>
              <td  class="text-left"><?= $replacetaxcramount;?></span></td>
              </tr>
              <?php
            }
            ?>

          </tbody>
        </table>
      </br>

    </br>

    </div>
    </div>
    <!-- this row will not appear when printing -->
    <!-- <div class="row no-print">
      <div class="col-xs-12">
        <a href="javascript:void(0)" onclick="window.print()"
                 class="btn btn-xs btn-success m-b-10"><i
                      class="fa fa-print m-r-5"></i> Print</a>
      </div>
    </div> -->
  </section>

  <?php

} else {
  // code...


}




?>
