"use strict";
var v2 = function () {
    $(document).ready(function() {
        var interval = setInterval(function() {
            var momentNow = moment();
            $('#date').html(momentNow.format('DD MMMM YYYY') + ', ' + momentNow.format('dddd').toUpperCase());
            $('#time').html(momentNow.format('hh:mm:ss A'));
        }, 100);
    });
};
var Login = function () {
    "use strict";
    return {
        init: function () {
            v2();
        }
    }
}();
$(function () {
    Login.init();
});