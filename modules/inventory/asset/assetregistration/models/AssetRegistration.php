<?php

namespace app\modules\inventory\asset\assetregistration\models;
/*Asset Category*/
use app\modules\inventory\assetsetup\assetcategory\models\AssetCategory;
use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType;

use Yii;

/**
 * This is the model class for table "asset_registration".
 *
 * @property int $id
 * @property string $assetName
 * @property string $assetDescription
 * @property int $assetCategory
 * @property int $assetCategoryType
 * @property string $assetTag
 * @property string $assetpurchaseDate
 * @property int $vendor
 * @property double $assetCost
 * @property string $assetBrand
 * @property string $assetModel
 * @property string $assetSerialNumber
 * @property string $assetQrCode
 * @property string $assetphoto
 * @property string $assetTimestamp
 * @property string $assetUpdatedAt
 * @property string $assetDeletedAt
 * @property int $assetStatus
 * @property int $userId
 *
 * @property AssetCheckIn[] $assetCheckIns
 * @property AssetCheckOut[] $assetCheckOuts
 * @property AssetCategory $assetCategory0
 * @property AssetCategoryType $assetCategoryType0
 * @property AssetSiteLocation[] $assetSiteLocations
 * @property EventTransport[] $eventTransports
 */
class AssetRegistration extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_registration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetName', 'assetDescription', 'assetCategory', 'assetTag', 'assetpurchaseDate', 'assetCost', 'assetSerialNumber', 'assetStatus', 'userId'], 'required'],
            [['assetCategory', 'assetCategoryType', 'vendor', 'assetStatus', 'userId'], 'integer'],
            [['assetTag', 'assetQrCode', 'assetphoto'], 'string'],
            [['assetpurchaseDate', 'assetTimestamp', 'assetUpdatedAt', 'assetDeletedAt'], 'safe'],
            [['assetCost'], 'number'],
            [['assetName', 'assetDescription', 'assetBrand', 'assetModel', 'assetSerialNumber'], 'string', 'max' => 255],
            [['assetCategory'], 'exist', 'skipOnError' => true, 'targetClass' => AssetCategory::className(), 'targetAttribute' => ['assetCategory' => 'id']],
            [['assetCategoryType'], 'exist', 'skipOnError' => true, 'targetClass' => AssetCategoryType::className(), 'targetAttribute' => ['assetCategoryType' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
          'id' => 'ID',
          'assetName' => 'Asset Name*',
          'assetDescription' => 'Asset Description*',
          'assetCategory' => 'Asset Category*',
          'assetCategoryType' => 'Asset Category Type',
          'assetTag' => 'Asset Tag*',
          'assetpurchaseDate' => 'Assetpurchase Date*',
          'vendor' => 'Vendor*',
          'assetCost' => 'Asset Cost*',
          'assetBrand' => 'Asset Brand',
          'assetModel' => 'Asset Model',
          'assetSerialNumber' => 'Asset Serial Number*',
          'assetQrCode' => 'Asset Qr Code',
          'assetphoto' => 'Assetphoto',
          'assetTimestamp' => 'Asset Timestamp',
          'assetUpdatedAt' => 'Asset Updated At',
          'assetDeletedAt' => 'Asset Deleted At',
          'assetStatus' => 'Asset Status',
          'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckIns()
    {
        return $this->hasMany(AssetCheckIn::className(), ['assetId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckOuts()
    {
        return $this->hasMany(AssetCheckOut::className(), ['assetId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCategory0()
    {
        return $this->hasOne(AssetCategory::className(), ['id' => 'assetCategory']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCategoryType0()
    {
        return $this->hasOne(AssetCategoryType::className(), ['id' => 'assetCategoryType']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetSiteLocations()
    {
        return $this->hasMany(AssetSiteLocation::className(), ['assetId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventTransports()
    {
        return $this->hasMany(EventTransport::className(), ['assetId' => 'id']);
    }
}
