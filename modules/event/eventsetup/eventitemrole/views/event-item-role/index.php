<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventItemRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Service Roles';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::button('Assign Role To service', ['value'=>Url::to((['create'])),'class' => 'btn btn-success','id'=>'modalButton']) ?>
        </p>
      </div>
    </div>
  </div>

  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
   <?php
    Modal::begin([
        'header'=>'<h4>Update Model</h4>',
        'id'=>'update-modal',
        'size'=>'modal-lg'
    ]);

    echo "<div id='updateModalContent'></div>";

    Modal::end();
?>

  <div class="box-body event-item-role-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->status==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            // 'itemId',
            [
              'attribute'=>'itemId',
              'value'=>'item.itemsName',
            ],
            // 'roleId',
            [
              'attribute'=>'roleId',
              'value'=>'role.roleName',
            ],
            'qty',
            // 'status',
            // 'userId',
            // 'createdBy',
            // 'createdAt',
            // 'updatedAt',
            // 'deletedAt',

            // ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
                'buttons' => ['view' => function($url, $model) {
                  return Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-search-plus"></b></span>', ['view', 'id' => $model['id']], ['title' => 'View', 'id' => 'modal-btn-view']);
              },
              'update' => function($url,$model,$key){
                    $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                        'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                        'class'=>'update-modal-click grid-action',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'bottom',
                        'title'=>'Update'
                    ]);
                    return $btn;
              },
              'delete' => function($url, $model) {
                  return Html::a('<span class="btn btn-sm btn-danger"><b class="fa fa-trash"></b></span>', ['delete', 'id' => $model['id']], ['title' => 'Delete', 'class' => '', 'data' => ['confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.', 'method' => 'post', 'data-pjax' => false],]);
              }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
