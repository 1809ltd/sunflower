<?php

namespace app\modules\inventory\assetmovement\assetcheckout;

/**
 * assetcheckout module definition class
 */
class assetcheckout extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\inventory\assetmovement\assetcheckout\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
