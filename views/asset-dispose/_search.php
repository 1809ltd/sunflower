<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AssetDisposeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-dispose-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'disposeId') ?>

    <?= $form->field($model, 'assetId') ?>

    <?= $form->field($model, 'dateDisposed') ?>

    <?= $form->field($model, 'disposeTo') ?>

    <?= $form->field($model, 'disposal Reason') ?>

    <?php // echo $form->field($model, 'disposeTimeStamp') ?>

    <?php // echo $form->field($model, 'disposeUpdatedAt') ?>

    <?php // echo $form->field($model, 'disposeDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
