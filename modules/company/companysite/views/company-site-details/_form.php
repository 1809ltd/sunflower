<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\CompanyDetails;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-md-12">
    <!-- begin panel -->
    <div class="panel panel-pvr panel--style--1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
            </div>
            <h4 class="panel-title">Register Company Storage Site</h4>
        </div>
        <div class="panel-body">
            <div class="row" >

              <div class="company-site-details-form">

                  <?php $form = ActiveForm::begin(); ?>


                  <div class="col-sm-3">
                    <div class="form-group">

                      <?php $comp = ArrayHelper::map(CompanyDetails::find()->all(),'id','companyName'); ?>

                      <?= $form->field($model, 'companyId')->dropDownList($comp, ['prompt'=>'Please Select Company']);?>

                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">

                      <?= $form->field($model, 'siteName')->textInput(['maxlength' => true]) ?>


                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'siteAddress')->textInput(['maxlength' => true]) ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                        <?= $form->field($model, 'siteSuite')->textInput(['maxlength' => true]) ?>
                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'siteCity')->textInput(['maxlength' => true]) ?>

                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'siteCounty')->textInput(['maxlength' => true]) ?>

                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'sitePostal')->textInput(['maxlength' => true]) ?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?= $form->field($model, 'siteCountry')->textInput(['maxlength' => true]) ?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
                      <?= $form->field($model, 'siteStatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

                    </div>
                  </div>

                  <div class="col-sm-3">
                    <div class="form-group">

                      <?= $form->field($model, 'companyUserId')->hiddenInput(['value'=> "1"])->label(false);?>

                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

                    </div>
                  </div>


                  <?php ActiveForm::end(); ?>

              </div>


            </div>
        </div>
    </div>
</div>
