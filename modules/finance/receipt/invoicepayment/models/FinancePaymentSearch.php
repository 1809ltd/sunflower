<?php

namespace app\modules\finance\receipt\invoicepayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\receipt\invoicepayment\models\FinancePayment;

/**
 * FinancePaymentSearch represents the model behind the search form of `app\modules\finance\receipt\invoicepayment\models\FinancePayment`.
 */
class FinancePaymentSearch extends FinancePayment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id',  'userId', 'createdBy', 'approvedBy', 'paymentstatus'], 'integer'],
            [['invoicePaymentPayType', 'invoicePaymentaccount', 'customerId','recepitNumber', 'date', 'invoicePaymentinvoiceRef', 'invoicePaymentPrivateNote', 'invoicePaymentCreatedAt', 'invoicePaymentUpdatedAt', 'invoicePaymentDeletedAt'], 'safe'],
            [['invoicePaymentTotalAmt', 'unallocatedAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinancePayment::find();
        $query->joinWith(['customer']);
        $query->joinWith(['invoicePaymentaccount0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'invoicePaymentaccount' => $this->invoicePaymentaccount,
            'invoicePaymentTotalAmt' => $this->invoicePaymentTotalAmt,
            'unallocatedAmount' => $this->unallocatedAmount,
            'date' => $this->date,
            // 'customerId' => $this->customerId,
            'invoicePaymentCreatedAt' => $this->invoicePaymentCreatedAt,
            'invoicePaymentUpdatedAt' => $this->invoicePaymentUpdatedAt,
            'invoicePaymentDeletedAt' => $this->invoicePaymentDeletedAt,
            'userId' => $this->userId,
            'createdBy' => $this->createdBy,
            'approvedBy' => $this->approvedBy,
            'paymentstatus' => $this->paymentstatus,
        ]);

        $query->andFilterWhere(['like', 'invoicePaymentPayType', $this->invoicePaymentPayType])
            ->andFilterWhere(['like', 'recepitNumber', $this->recepitNumber])
            ->andFilterWhere(['like', 'customerDisplayName', $this->customerId])
            ->andFilterWhere(['like', 'fullyQualifiedName', $this->invoicePaymentaccount])
            ->andFilterWhere(['like', 'invoicePaymentinvoiceRef', $this->invoicePaymentinvoiceRef])
            ->andFilterWhere(['like', 'invoicePaymentPrivateNote', $this->invoicePaymentPrivateNote]);

        return $dataProvider;
    }
}
