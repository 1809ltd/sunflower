<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\user\userroletypes\models\UserRoleTypesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Role Types';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
          <?= Html::a('Add User Roles', ['create'], ['class' => 'btn btn-success']) ?>

        </p>
      </div>
    </div>
  </div>

  <div class="box-body user-role-types-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->status==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'roleName',
            [
                'attribute' => 'status',
                'value' =>  function($data) { return $data->status == 1 ? 'Yes' : 'No'; }
            ],
            // 'createdBy',
            // 'createdAt',
            // 'updatedAt',
            //'deletedAt',
            //'userId',
            //'status',

            ['class' => 'yii\grid\ActionColumn'],

        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
