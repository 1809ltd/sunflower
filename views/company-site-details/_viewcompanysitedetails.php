<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceEstimateLinesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="company-site-details-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [

          //'companyId',
          //'company.companyName',
          //'siteName',
          //'siteAddress',
          //'siteSuite',
          'siteCity',
          'siteCounty',
          'sitePostal',
          'siteCountry',
          //'siteStatus',
          //'siteTimestamp',
          //'siteUpdatedAt',
          //'siteDeletedAt',
          //'companyUserId',

        ],
    ]); ?>

</div>
