<?php

namespace app\modules\finance\expensepayment\officeexpensepayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments;

/**
 * FinanceOfficeRequisitionPaymentsSearch represents the model behind the search form of `app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPayments`.
 */
class FinanceOfficeRequisitionPaymentsSearch extends FinanceOfficeRequisitionPayments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','userId', 'createdBy', 'approvedBy', 'paymentstatus'], 'integer'],
            [['officeReqPaymentPayType',  'officeReqPaymentaccount', 'reqrecepitNumber', 'date', 'officeReqPaymentofficeReqRef', 'officeReqPaymentPrivateNote', 'officeReqPaymentCreatedAt', 'officeReqPaymentUpdatedAt', 'officeReqPaymentDeletedAt'], 'safe'],
            [['officeReqPaymentTotalAmt', 'unallocatedAmount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceOfficeRequisitionPayments::find();
        $query->joinWith(['officeReqPaymentaccount0']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'officeReqPaymentaccount' => $this->officeReqPaymentaccount,
            'officeReqPaymentTotalAmt' => $this->officeReqPaymentTotalAmt,
            'unallocatedAmount' => $this->unallocatedAmount,
            'date' => $this->date,
            'officeReqPaymentCreatedAt' => $this->officeReqPaymentCreatedAt,
            'officeReqPaymentUpdatedAt' => $this->officeReqPaymentUpdatedAt,
            'officeReqPaymentDeletedAt' => $this->officeReqPaymentDeletedAt,
            'userId' => $this->userId,
            'createdBy' => $this->createdBy,
            'approvedBy' => $this->approvedBy,
            'paymentstatus' => $this->paymentstatus,
        ]);

        $query->andFilterWhere(['like', 'officeReqPaymentPayType', $this->officeReqPaymentPayType])
            ->andFilterWhere(['like', 'reqrecepitNumber', $this->reqrecepitNumber])
                ->andFilterWhere(['like', 'fullyQualifiedName', $this->officeReqPaymentaccount])
            ->andFilterWhere(['like', 'officeReqPaymentofficeReqRef', $this->officeReqPaymentofficeReqRef])
            ->andFilterWhere(['like', 'officeReqPaymentPrivateNote', $this->officeReqPaymentPrivateNote]);

        return $dataProvider;
    }
}
