<?php

namespace app\modules\finance\expense\bills\billslines\controllers;

use yii\web\Controller;

/**
 * Default controller for the `billslines` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
