<?php

namespace app\modules\finance\financereport\companyfinancials\balancesheet\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// for db functions
use yii\db\Expression;
// Geetng related models;
use app\modules\finance\financereport\companyfinancials\balancesheet\models\BalanceSheet;

/**
 * Default controller for the `balancesheet` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        // Create a model to allow the search
        $modelBal = new BalanceSheet();

        // Create a model to allow the search
        $model = new \yii\base\DynamicModel([
          'endDate'
        ]);
        // Adding Rules for validation
        $model->addRule(['endDate'], 'required')
              ->validate();



        if($model->load(Yii::$app->request->post())){
          // do somenthing with model
          $getserach="The search is here";

          // Get the current asset based on the date
          $cureentAsstCash = $modelBal->getallBlancecashtime($model->endDate);
          // Get the Reviceables  based on the date
          $recviableTT = $modelBal->getreceivableAmount($model->endDate);
          // Get Prepaid bills
          $prepaidbills=$modelBal->getPrepaidAmount($model->endDate);
          // GetFixed ASSETS
          $fixedAssets = $modelBal->getfixedAssetAmount($model->endDate);
          // Get the Payables Figure
          $payableTT = $modelBal->getpayablesAmount($model->endDate);
          // Get Event Prepayemnt
          $eventPrepayemnt = $modelBal->getEventPrepayments($model->endDate);
          // Get Tax amount
          $taxpayables = $modelBal->getTaxpayablesAmount($model->endDate);
          // Get Owners Equity
          $ownerEquity = $modelBal->getOwnersinputAmount($model->endDate);
          // Get Previous Year Earnings
          $lastyearEarning = $modelBal->getpreviousyearAmount($model->endDate);
          // Get This years Earning
          $thisyearEarning = $modelBal->getcurrentyearAmount($model->endDate);

          // See is ther search is not empty
         if (!empty($getserach)) {
           // code...
           return $this->render('index', [
                                     'getserach' => $getserach,
                                     'cureentAsstCash' => $cureentAsstCash,
                                     'recviableTT' => $recviableTT,
                                     'prepaidbills' => $prepaidbills,
                                     'fixedAssets' => $fixedAssets,
                                     'payableTT' => $payableTT,
                                     'eventPrepayemnt' => $eventPrepayemnt,
                                     'taxpayables' => $taxpayables,
                                     'ownerEquity' => $ownerEquity,
                                     'lastyearEarning' => $lastyearEarning,
                                     'thisyearEarning' => $thisyearEarning,
                                     'model' => $model,
                                   ]);
          } else {
            // code...
            return $this->render('index', [
                           'model' => $model,
                           'modelBal'=>$modelBal,
                         ]);

          }
        }else {
          // code...
          return $this->render('index', [
                         'model' => $model,
                         'modelBal'=>$modelBal,
                       ]);
        }
    }
}
