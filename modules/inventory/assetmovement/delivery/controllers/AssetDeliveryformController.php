<?php

namespace app\modules\inventory\assetmovement\delivery\controllers;

use Yii;
use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform;
use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryformSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/*To enable dynamicform_wrapper*/

use app\models\Model;
use yii\helpers\ArrayHelper;

/*Getting the Lines relationship*/
use app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOut;
use app\modules\inventory\assetmovement\assetcheckout\models\AssetCheckOutSearch;

/**
 * AssetDeliveryformController implements the CRUD actions for AssetDeliveryform model.
 */
class AssetDeliveryformController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all AssetDeliveryform models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $searchModel = new AssetDeliveryformSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AssetDeliveryform model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AssetDeliveryform model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new AssetDeliveryform();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AssetDeliveryform model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /*Searchfrom for DElivery list*/
    public function actionDeliverysearch($array=null)
    {

      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $searchModel = new AssetDeliveryformSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new AssetDeliveryform();

        /*If we loading a new page*/
        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          \Yii::$app->session->set('eventId',$model->eventId);
          \Yii::$app->session->set('orderId',$model->orderId);

          // $_SESSION['eventId']=$model->eventId;
          // $_SESSION['orderId']=$model->orderId;

          return $this->redirect(['deliverysearch']);
          //

        } else {
          // code...

          //load the Delvery Search form
            return $this->render('deliverysearch', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model'=>$model,

            ]);
        }

    }

    public function actionClosesearch(){

        unset($_SESSION['eventId']);
        unset($_SESSION['orderId']);

        return $this->redirect(['deliverysearch']);
    }
    public function actionDeliveryformcreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';

      /*$activityId,$orderId*/

      $model = new AssetDeliveryform();

        $checkoutitem = new AssetCheckOut();
        $checkoutitem->activityId=$_SESSION['eventId'];
        $checkoutitem->orderId= $_SESSION['orderId'];
        $checkoutitem->activity="1";

        //checking if its a post
        // $checkoutitem->load(Yii::$app->request->post())
        // Yii::$app->request->isAjax
        if ($checkoutitem->load(Yii::$app->request->post())) {
          // code...

          //Geting the time created Timestamp
          $checkoutitem->timestamp= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          //Getting user Log in details
          $checkoutitem->userId = Yii::$app->user->identity->id;
          $checkoutitem->createdBy= Yii::$app->user->identity->id;

          $checkoutitem->save();

          return $this->redirect(['deliverysearch']);

        } else {
          // code...

          //load the create form
          return $this->renderAjax('deliverycreate', [
              'checkoutitem' => $checkoutitem,
              'model' => $model,
          ]);
        }


    }
    /**
     * Updates an existing AssetCheckOut model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeliveryformupdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $checkoutitem = $this->findModel($id);

        //checking if its a post
        if ($checkoutitem->load(Yii::$app->request->post())) {
          // code...

          //Getting user Log in details
          $checkoutitem->userId = Yii::$app->user->identity->id;

          $checkoutitem->save();

          /*After an Update*/

          return $this->redirect(['deliverysearch']);

        } else {
          // code...

          //load the create form
          return $this->renderAjax('deliveryupdate', [
              'checkoutitem' => $checkoutitem,
          ]);
        }

    }
    public function actionCheckout()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new AssetDeliveryform();
        //

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Geting the time created Timestamp
          // $model->timestamp= Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));


          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;
          $model->createdBy= Yii::$app->user->identity->id;

          $deliveryNumber=$model->getDevnumber();
          $model->deliveryNumber= $deliveryNumber;

          // var_dump($model->duedate);
          // die();

          $model->save();
            // code...
            $connection = Yii::$app->db;

            $query= "UPDATE `asset_check_out` SET `status` = ".$model->deliveryStatus.",`deliveryId` = ".$model->id.",
             `activityId` = ".$model->eventId.",`dueDate` = '".$model->duedate."',
             `userId` = ".$model->userId.",
             `checkOutDate` = '".$model->date."' WHERE activityId= ".$model->eventId." and orderId=".$model->eventId;
          //
            $connection->createCommand($query)->execute();
          //

        //after Saving the Checkout Details
          /*After an Update to check out table*/

          return $this->redirect(['deliverysearch']);

        } else {
          // code...

          //load the create form
          return $this->renderAjax('create', [
              'model' => $model,
          ]);
        }
    }

    public function getCheckoutItems($eventId,$orderId)
    {
      $model = AssetCheckOut::find()->where(['activityId' => $id])
      ->Andwhere(['status'=>2])
      ->Andwhere(['orderId'=>$orderId])->all();
      return $model;
    }

    /**
     * Deletes an existing AssetDeliveryform model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AssetDeliveryform model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AssetDeliveryform the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AssetDeliveryform::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
