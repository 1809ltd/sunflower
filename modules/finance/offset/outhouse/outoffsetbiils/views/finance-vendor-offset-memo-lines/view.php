<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Offset Memo Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="finance-vendor-offset-memo-lines-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'vendorId',
            'vendorOffsetMemoId',
            'billRef',
            'vendorOffsetMemoLineDescription:ntext',
            'vendorOffsetMemoLineAmount',
            'vendorOffsetMemoLineStatus',
            'vendorOffsetMemoLineCreatedAt',
            'vendorOffsetMemoLineUpdatedAt',
            'creidtDeletedAt',
            'userId',
        ],
    ]) ?>

</div>
