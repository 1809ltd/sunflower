<?php

namespace app\modules\event\eventsetup\eventitemrole\models;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;
/*Evevnet Role*/
use app\modules\event\eventsetup\eventrole\models\EventRole;
/*User details*/
use app\modules\user\userdetails\models\UserDetails;


use Yii;

/**
 * This is the model class for table "event_item_role".
 *
 * @property int $id
 * @property int $itemId
 * @property int $roleId
 * @property double $qty
 * @property int $status
 * @property int $userId
 * @property int $createdBy
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property FinanceItems $item
 * @property EventRole $role
 * @property UserDetails $user
 * @property UserDetails $createdBy0
 * @property EventWorkplan[] $eventWorkplans
 */
class EventItemRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_item_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemId', 'roleId', 'qty', 'status'], 'required'],
            [['itemId', 'roleId', 'status', 'userId', 'createdBy'], 'integer'],
            [['qty'], 'number'],
            [['createdAt', 'updatedAt', 'deletedAt', 'userId', 'createdBy', 'createdAt'], 'safe'],
            [['itemId', 'roleId'], 'unique', 'targetAttribute' => ['itemId', 'roleId']],
            [['itemId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceItems::className(), 'targetAttribute' => ['itemId' => 'id']],
            [['roleId'], 'exist', 'skipOnError' => true, 'targetClass' => EventRole::className(), 'targetAttribute' => ['roleId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'itemId' => 'Service',
            'roleId' => 'Role',
            'qty' => 'Qty',
            'status' => 'Status',
            'userId' => 'User',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(FinanceItems::className(), ['id' => 'itemId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(EventRole::className(), ['id' => 'roleId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventWorkplans()
    {
        return $this->hasMany(EventWorkplan::className(), ['taskAssign' => 'id']);
    }
}
