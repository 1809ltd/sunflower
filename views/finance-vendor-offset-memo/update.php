<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemo */

$this->title = 'Update Finance Vendor Offset Memo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Offset Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-vendor-offset-memo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceVendorOffsetMemoLines'=>$modelsFinanceVendorOffsetMemoLines,
    ]) ?>

</div>
