<?php
/*Pop up menu*/
use yii\helpers\Url;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
?>

<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <img style="height:25%;width:25%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
            <i class=""></i> Chart of Accounts.
            <small class="pull-right">As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">

        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">


        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Account Number</th>
              <th>Name</th>
              <th>Description</th>
              <th>A/C Classification</th>
              <th>A/C Type</th>
              <th>A/C Detail Type</th>
            </tr>
            </thead>
            <tbody>
              <?php
              $connection = Yii::$app->getDb();
              $command1 = $connection->createCommand("SELECT * FROM `finance_accounts` WHERE accountsStatus = 1 ORDER by COALESCE(accountsClassification,accountParentId),accountsClassification,accountParentId,accountNumber");
              $coa = $command1->query();

              foreach ($coa as $coa){
                // code...
                ?>
                <?php

                if ($coa["accountParentId"]==0) {
                  // code...
                  ?>
                  <tr>
                    <th style="background-color: violet;"></th>
                    <th style="background-color: violet;"><span class="align-numbers"><?= $coa["fullyQualifiedName"] ;?></span></th>
                    <th style="background-color: violet;"></th>
                    <th style="background-color: violet;"></span></th>
                    <th style="background-color: violet;"></th>
                    <th style="background-color: violet;"></th>
                  </tr>

                  <?php

                } else {
                  // code...
                  ?>
                  <tr>
                  <td><span class="align-numbers"><?= $coa["accountNumber"] ;?></span></td>
                    <td><span class="align-numbers"><?= $coa["fullyQualifiedName"] ;?></span></td>
                    <td><span class="align-numbers"><?= $coa["accountsDescription"] ;?></span></td>
                    <td><span class="align-numbers"><?= $coa["accountsClassification"] ;?></span></td>
                    <td><span class="align-numbers"><?= $coa["accountsType"] ;?></span></td>
                    <td><span class="align-numbers"><?= $coa["accountsDetailType"] ;?></span></td>
                  </tr>
                 <?php

                }

             }


             ?>

            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>
        </div>
      </div>
    </section>
