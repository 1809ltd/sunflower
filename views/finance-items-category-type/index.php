<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceItemsCategoryTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Service Category Type';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::a('Create Service Category Type', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div>
    </div>
  </div>



  <div class="box-body finance-items-category-type-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'financeItemCategoryId',
            'financeItemCategoryTypeName',
            'financeItemCategoryTypeDescription:ntext',
            'financeItemCategoryTypeStatus',
            //'userId',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
