<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EventOrderfromList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-orderfrom-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'orderId')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'itemid')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'details')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'outsourced')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'vendorId')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'quantity')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'userId')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'approvedBy')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
