<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCreditMemo */

$this->title = 'Update Finance Credit Memo: ' . $model->refernumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Credit Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->refernumber, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-credit-memo-update">


    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceCreditMemoLines'=>$modelsFinanceCreditMemoLines, //to enable Dynamic Form
    ]) ?>

</div>
