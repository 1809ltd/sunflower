<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companysitelocation\models\CompanySiteLocationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-site-location-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'siteId') ?>

    <?= $form->field($model, 'locationName') ?>

    <?= $form->field($model, 'locationtimestamp') ?>

    <?= $form->field($model, 'locationUpdatedAt') ?>

    <?php // echo $form->field($model, 'locationDeleteAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
