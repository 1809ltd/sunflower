<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventActivity */
/* @var $form yii\widgets\ActiveForm */
/*Chart of Accounts*/
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
/*All trasnaction  */
use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

/*Getting the Select2 to work */
use kartik\select2\Select2;

/* @var $this yii\web\View */
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

$model->accountId= \Yii::$app->session->get('transactionaccountId');
$model->startDate= \Yii::$app->session->get('startDate');
$model->endDate= \Yii::$app->session->get('endDate');

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Account Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<!-- Search Date And the rest -->
  <div class="box no-print">
      <div class="box-header with-border">
        <h3 class="box-title">Search</h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <?php $form = ActiveForm::begin([
                          'action' => ['alltransacation'],
                          'options' => ['method' => 'post'],
                      ]);
                      // begin(['action' => ['builder/saveform'],'options' => ['method' => 'post']]) ?>
          <div class="col-md-4">
            <div class="form-group">
              <label class="col-lg-4 control-label">Account: </label>
              <div class="col-lg-8">
                <?= $form->field($model,'accountId')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(FinanceAccounts::find()
                                              // ->where(['accountsClassification' => "Expenses"])
                                              ->where(['accountsStatus'=>1])
                                              // ->asArray()
                                              ->all(),'id','fullyQualifiedName'),
                    'language' => 'en',
                    'options' => ['prompt'=>'Select...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false) ?>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="col-lg-4 control-label">From: </label>
              <div class="col-lg-8">
                <?= $form->field($model,'startDate')->widget(\yii\jui\DatePicker::class, [
                        //'language' => 'ru',
                        'options' => ['autocomplete'=>'off','class' => 'form-control'],
                        'inline' => false,
                        'dateFormat' => 'yyyy-MM-dd',
                    ])->label(false) ?>
              </div>
            </div>
          </div>
            <div class="col-md-3">
              <div class="form-group">
                <label class="col-lg-4 control-label">To: </label>
                <div class="col-lg-8">
                  <?= $form->field($model,'endDate')->widget(\yii\jui\DatePicker::class, [
                          //'language' => 'ru',
                          'options' => ['autocomplete'=>'off','class' => 'form-control'],
                          'inline' => false,
                          'dateFormat' => 'yyyy-MM-dd',
                      ])->label(false) ?>
                </div>
              </div>
            </div>
          <div class="col-md-2">
            <div class="form-group">
              <div class="col-lg-8 col-lg-offset-4">
                <div class="center-align">
                  <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
                </div>
              </div>
            </div>
          </div>
          <?php ActiveForm::end(); ?>
        </div>
    </div>
  </div>

  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Income Statement</h3>
      <h5 class="box-title">Reporting period:<?= Yii::$app->formatter->asDate($model->startDate, 'long');?> to <?= Yii::$app->formatter->asDate($model->endDate, 'long');?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>

    <div class="box">
      <div class="box-body">
        <h5 class="box-title">REVENUE</h5>
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th class="text-left">Date</th>
              <th class="">Account</th>
              <th class="text-right">Dr</th>
              <th class="text-right">Cr</th>
            </tr>
          </thead>
          <tbody>
            <?php

            $transactionCreditTotal=0;
            $transactionDrTotal=0;

            $transactionaccounts = AllTransactions::find()
                          ->where('accountId = :accountId', [':accountId' => $model->accountId])
                          ->andWhere('all_transactions.`transactionDate` BETWEEN :startDate and :endDate', [':startDate' => $model->startDate,':endDate' => $model->endDate])
                          ->all();

            foreach ($transactionaccounts as $transactionaccount){
              // code...
              // echo "this is the Credit Amount".$transactionaccount->cr_amount;
              // die();
              $transactionCreditTotal+= $transactionaccount->cr_amount;
              $transactionDrTotal+= $transactionaccount->dr_amount;
              ?>
              <tr>
                <td class="text-left"><?= Yii::$app->formatter->asDate($transactionaccount->transactionDate, 'long');?></td>
                <td  class=""><a href="<?=Url::to(['']) ?>"><?= $transactionaccount->tranctionDescription;?></a></td>
                <td  class="text-right"><span class="align-numbers"><?= number_format($transactionaccount->dr_amount,2) ;?></span></td>
                <td  class="text-right"><span class="align-numbers"><?= number_format($transactionaccount->cr_amount,2) ;?></span></td>
              </tr>
             <?php
           }
           ?>
           <tr>
             <th class="text-left">Total</th>
             <th class=""></th>
             <th class="text-right"><?= number_format($transactionDrTotal,2); ?></th>
             <th class="text-right"><?= number_format($transactionCreditTotal,2); ?></th>
           </tr>
         </tbody>
       </table>
     </br>

  </div>
  </div>
  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
      <a href="javascript:void(0)" onclick="window.print()"
         class="btn btn-xs btn-success m-b-10"><i
              class="fa fa-print m-r-5"></i> Print</a>

        <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
        <i class="fa fa-download"></i> Generate PDF -->
      <!-- </button> -->
    </div>
  </div>
  </section>
