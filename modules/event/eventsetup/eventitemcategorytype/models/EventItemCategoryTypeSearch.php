<?php

namespace app\modules\event\eventsetup\eventitemcategorytype\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\event\eventsetup\eventitemcategorytype\models\EventItemCategoryType;

/**
 * EventItemCategoryTypeSearch represents the model behind the search form of `app\modules\event\eventsetup\eventitemcategorytype\models\EventItemCategoryType`.
 */
class EventItemCategoryTypeSearch extends EventItemCategoryType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'userId'], 'integer'],
            [['qty'], 'number'],
            [['itemId', 'categorytypeId', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventItemCategoryType::find();
        $query->joinWith(['item']);
        $query->joinWith(['categorytype']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'itemId' => $this->itemId,
            // 'categorytypeId' => $this->categorytypeId,
            'qty' => $this->qty,
            'status' => $this->status,
            'userId' => $this->userId,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);
        $query->andFilterWhere(['like', 'itemsName', $this->itemId])
            ->andFilterWhere(['like', 'assetCategoryTypeName', $this->categorytypeId]);

        return $dataProvider;
    }
}
