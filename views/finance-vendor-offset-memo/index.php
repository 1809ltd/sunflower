<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Vendor Offset Memos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-offset-memo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Finance Vendor Offset Memo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'vendorOffsetMemoAccount',
            'refernumber',
            'vendorOffsetMemoTxnDate',
            'vendorOffsetMemoNote:ntext',
            //'vendorId',
            //'offsetAmount',
            //'vendorOffsetMemoStatus',
            //'vendorOffsetMemoCreatedAt',
            //'vendorOffsetMemoUpdatedAt',
            //'vendorOffsetMemoDeletedAt',
            //'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
