<?php

namespace app\modules\inventory\assetmovement\assetcheckout\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Order form */
use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
/*Order form List */
use app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromList;
/*Personnel details*/
use app\modules\personnel\personneldetails\models\PersonnelDetails;
/*Asset Location*/
use app\modules\inventory\asset\assetlocation\models\AssetSiteLocation;
/*AssetREgisrtartion*/
use app\modules\inventory\asset\assetregistration\models\AssetRegistration;
/*Transportation*/
use app\modules\event\transportation\models\EventTransport;
/*Delivery Form*/
use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform;
/*Event Details*/
use app\modules\event\eventInfo\models\EventDetails;

use Yii;

/**
 * This is the model class for table "asset_check_out".
 *
 * @property int $id
 * @property int $assetId
 * @property int $locationId
 * @property string $checkOutDate
 * @property string $dueDate
 * @property int $activity
 * @property int $deliveryId
 * @property int $activityId
 * @property int $orderId
 * @property int $orderlistId
 * @property int $vehicle
 * @property int $assignto
 * @property string $timestamp
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $status
 * @property int $athurizedBy
 * @property int $userId
 * @property int $createdBy
 *
 * @property AssetCheckIn[] $assetCheckIns
 * @property PersonnelDetails $assignto0
 * @property EventOrderfromList $orderlist
 * @property UserDetails $createdBy0
 * @property UserDetails $user
 * @property EventDetails $activity0
 * @property AssetRegistration $asset
 * @property AssetSiteLocation $location
 * @property EventTransport $vehicle0
 * @property AssetDeliveryform $delivery
 * @property EventOrderfrom $order
 */
class AssetCheckOut extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_check_out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'status', 'userId', 'createdBy'], 'required'],
            [['assetId', 'locationId', 'activity', 'deliveryId', 'activityId', 'orderId', 'orderlistId', 'vehicle', 'assignto', 'status', 'athurizedBy', 'userId', 'createdBy'], 'integer'],
            [['checkOutDate', 'dueDate', 'timestamp', 'updatedAt', 'deletedAt'], 'safe'],
            [['assignto'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelDetails::className(), 'targetAttribute' => ['assignto' => 'id']],
            [['orderlistId'], 'exist', 'skipOnError' => true, 'targetClass' => EventOrderfromList::className(), 'targetAttribute' => ['orderlistId' => 'id']],
            [['createdBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['createdBy' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['activityId' => 'id']],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['locationId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetSiteLocation::className(), 'targetAttribute' => ['locationId' => 'id']],
            [['vehicle'], 'exist', 'skipOnError' => true, 'targetClass' => EventTransport::className(), 'targetAttribute' => ['vehicle' => 'id']],
            [['deliveryId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetDeliveryform::className(), 'targetAttribute' => ['deliveryId' => 'id']],
            [['orderId'], 'exist', 'skipOnError' => true, 'targetClass' => EventOrderfrom::className(), 'targetAttribute' => ['orderId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetId' => 'Asset',
            'locationId' => 'Location',
            'checkOutDate' => 'Check Out Date',
            'dueDate' => 'Due Date',
            'activity' => 'Activity',
            'deliveryId' => 'Delivery',
            'activityId' => 'Activity',
            'orderId' => 'Order',
            'orderlistId' => 'Service',
            'vehicle' => 'Vehicle',
            'assignto' => 'Assignto',
            'timestamp' => 'Timestamp',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'status' => 'Status',
            'athurizedBy' => 'Athurized By',
            'userId' => 'User',
            'createdBy' => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCheckIns()
    {
        return $this->hasMany(AssetCheckIn::className(), ['checkOutId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssignto0()
    {
        return $this->hasOne(PersonnelDetails::className(), ['id' => 'assignto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderlist()
    {
        return $this->hasOne(EventOrderfromList::className(), ['id' => 'orderlistId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'createdBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity0()
    {
        return $this->hasOne(EventDetails::className(), ['id' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(AssetSiteLocation::className(), ['id' => 'locationId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle0()
    {
        return $this->hasOne(EventTransport::className(), ['id' => 'vehicle']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(AssetDeliveryform::className(), ['id' => 'deliveryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(EventOrderfrom::className(), ['id' => 'orderId']);
    }
}
