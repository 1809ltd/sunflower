<?php

namespace app\modules\event\eventdocs\orderfroms\models;

use app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromList;
/*Finance Items*/
use app\modules\finance\productsetup\financeitems\models\FinanceItems;

use app\modules\event\eventInfo\models\EventDetails;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;


/*Getting the REquire Asset*/
/*AssetCategoryType*/
use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType;
use app\modules\event\eventsetup\eventitemcategorytype\models\EventItemCategoryType;

/*Getting the REquire Roles*/
/*Event roles*/
use app\modules\event\eventsetup\eventrole\models\EventRole;
use app\modules\event\eventsetup\eventitemrole\models\EventItemRole;

use Yii;

/**
 * This is the model class for table "event_orderfrom".
 *
 * @property int $id
 * @property string $orderNumber
 * @property int $eventId
 * @property string $date
 * @property int $orderStatus
 * @property string $orderNote
 * @property int $personIncharge
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 * @property int $approvedBy
 *
 * @property EventDetails $event
 * @property UserDetails $approvedBy0
 * @property UserDetails $user
 * @property EventOrderfromList[] $eventOrderfromLists
 * @property EventWorkplan[] $eventWorkplans
 */
class EventOrderfrom extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_orderfrom';
    }

    /**
     * {@inheritdoc}
     */
     public function rules()
     {
         return [
             [['orderNumber', 'eventId', 'date', 'orderStatus', 'userId', 'approvedBy'], 'required'],
             [['eventId', 'orderStatus', 'personIncharge', 'userId', 'approvedBy'], 'integer'],
             [['date', 'createdAt', 'updatedAt', 'personIncharge','deletedAt'], 'safe'],
             [['orderNote'], 'string'],
             [['orderNumber'], 'string', 'max' => 100],
             [['orderNumber'], 'unique'],
             [['eventId'], 'exist', 'skipOnError' => true, 'targetClass' => EventDetails::className(), 'targetAttribute' => ['eventId' => 'id']],
             [['approvedBy'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedBy' => 'id']],
             [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
         ];
     }

     /**
      * {@inheritdoc}
      */
     public function attributeLabels()
     {
         return [
             'id' => 'ID',
             'orderNumber' => 'Order Number',
             'eventId' => 'Event',
             'date' => 'Date',
             'orderStatus' => 'Order Status',
             'orderNote' => 'Order Note',
             'personIncharge' => 'Person Incharge',
             'createdAt' => 'Created At',
             'updatedAt' => 'Updated At',
             'deletedAt' => 'Deleted At',
             'userId' => 'User ID',
             'approvedBy' => 'Approved By',
         ];
     }
     /**
      * @return \yii\db\ActiveQuery
      */
     public function getEvent()
     {
         return $this->hasOne(EventDetails::className(), ['id' => 'eventId']);
     }

     /**
      * @return \yii\db\ActiveQuery
      */
     public function getApprovedBy0()
     {
         return $this->hasOne(UserDetails::className(), ['id' => 'approvedBy']);
     }

     /**
      * @return \yii\db\ActiveQuery
      */
     public function getUser()
     {
         return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
     }

     /**
      * @return \yii\db\ActiveQuery
      */
     public function getEventOrderfromLists()
     {
         return $this->hasMany(EventOrderfromList::className(), ['orderId' => 'id']);
     }
     /*Get Event order lst with the same status*/
     public function geteventOrderfromList($id,$orderStatus){
       //Getting all the event Order list with the same status
       return EventOrderfromList::find()
                     ->where(['id'=>$id])
                     ->Andwhere(['status'=>$orderStatus])
                     ->all();
     }
     /*GEt Asset List*/
     public function getAssetrequired($id){
       return EventItemCategoryType::find()
         ->where(['and', "status=1", "itemId=$id"])->all();
         // ->indexBy('id')->column();

     }
     /*Get Work force Details*/
     public function getRolerequired($id){
       return EventItemRole::find()
         ->where(['and', "status=1", "itemId=$id"])->all();
         // ->indexBy('id')->column();

     }

     /*GEt Role Name*/

     public function getRoleName($id)
     {
       return EventRole::find()
         ->where(['and', "id=$id"])->all();
         // ->indexBy('id')->column();

     }

     public function getOrdnumber()
     {
         //select product code

         $connection = Yii::$app->db;
         $query= "Select MAX(orderNumber) AS number from event_orderfrom where id>0";
         $rows= $connection->createCommand($query)->queryAll();

         if ($rows) {
           // code...

           $number=$rows[0]['number'];
           $number++;
           if ($number == 1) {
             // code...
             $number =   "ORD-".date('Y')."-".date('m')."-0001";
           }
           if ($number == 1) {
             // code...
             $number = "ORD-".date('Y')."-".date('m')."-0001";
           }
         } else {
           // code...
           //generating numbers
           $number = "ORD-".date('Y')."-".date('m')."-0001";
         }

       return $number;
     }
 }
