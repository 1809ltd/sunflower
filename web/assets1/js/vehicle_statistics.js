"use strict";
var initz = function () {
    "use strict";
    $(document).ready(function () {
        var table = "";
        table = $('#loggedout').DataTable({
            "dom":"mf",
            "ajax"   : {
                "url"    : "assets/php_data/logged_out.txt",
                "dataSrc": ""
            },
            "columns": [
                {"data": 'vehicle_no'},
                {"data": "current_place"}
            ],
            "order"  : [ [ 0, 'asc' ] ],
        });

        table = $('#loggedin').DataTable({
            "dom":"mf",
            "ajax"   : {
                "url"    : "assets/php_data/logged_in.txt",
                "dataSrc": ""
            },
            "columns": [
                {"data": 'vehicle_no'},
                {"data": "current_place"}
            ],
            "order"  : [ [ 1, 'asc' ] ],
        });
    });
};
var table = function () {
    "use strict";
    return {
        init: function () {
            initz();
        }
    }
}();
$(function () {
    table.init();
});