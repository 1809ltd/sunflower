<?php

use yii\helpers\Html;
use app\models\AllTransactions;

/* @var $this yii\web\View */

?>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?= date('Y-m-d'); ?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>
           <strong>SUNFLOWER EVENTS, Income Statement</strong><br/>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">


       </div>
       <!-- /.col -->
     </div>
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>

         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>
           <b>As of: <?= date('Y-m-d H:i:s'); ?></b><br>


         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

         <br>

       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-hover">
           <thead>


           </thead>
           <tbody>

             <!--  Revenue-->
             <?php

                $connection = Yii::$app->getDb();
                $command1 = $connection->createCommand("SELECT
                                                  	all_transactions.accountParentId,
                                                  	all_transactions.accountsclassfication,
                                                  	all_transactions.accountId,
                                                  	all_transactions.accountName,
                                                  	Sum( all_transactions.dr_amount ),
                                                  	Sum( all_transactions.cr_amount ),
                                                  	all_transactions.transactionClassification,
                                                  	all_transactions.transactionCategory
                                                  FROM
                                                  	all_transactions
                                                  WHERE
                                                  	all_transactions.accountsclassfication = 'Revenue'
                                                  GROUP BY
                                                  	all_transactions.accountId,
                                                  	all_transactions.transactionClassification
                                                  ORDER BY all_transactions.accountParentId ASC");

                $revenue = $command1->query();

              ?>
                <table class="table table table-hover">
                  <tr>Revenue</tr>
                  <tr>
                    <th style="width:75%">Accounts:</th>
                    <th>Balance (Ksh)</th>
                  </tr>
                  <?php

                  $revenueTotal=0;

                  foreach ($revenue as $revenue){
                    // code...

                     $revenueTotal+= $revenue["Sum( all_transactions.cr_amount )"];

                    ?>

                    <tr>

                      <td><?= $revenue["accountName"];?></td>
                      <td><span class="align-numbers"><?= number_format($revenue["Sum( all_transactions.cr_amount )"],2) ;?></span></td>
                    </tr>

                    <?php

                  }

                   ?>

                   <tr></tr>


                  <tr>
                    <th>Total Revenue:</th>
                      <td><?= number_format($revenueTotal,2); ?></td>
                  </tr>
                </table>

                <!--  Cost of Goods Sold (COGS)-->
                <?php

                   $command2 = $connection->createCommand("SELECT
                                                    	all_transactions.accountParentId,
                                                    	all_transactions.accountsclassfication,
                                                    	all_transactions.accountId,
                                                    	all_transactions.accountName,
                                                    	Sum( all_transactions.dr_amount ),
                                                    	Sum( all_transactions.cr_amount ),
                                                    	all_transactions.transactionClassification,
                                                    	all_transactions.transactionCategory
                                                    FROM
                                                    	all_transactions
                                                    WHERE
                                                    	all_transactions.transactionClassification LIKE 'Event expense'
                                                    	OR all_transactions.transactionClassification LIKE 'Bill expense'
                                                    GROUP BY
                                                    	all_transactions.accountId,
                                                    	all_transactions.transactionClassification
                                                    ORDER BY
                                                    	all_transactions.accountParentId ASC");

                   $COGS = $command2->query();

                 ?>
                   <table class="table table table-hover">
                     <tr>Cost of Goods Sold (COGS)</tr>
                     <tr>
                       <th style="width:75%">Accounts:</th>
                       <th>Balance (Ksh)</th>
                     </tr>
                     <?php

                     $COGSTotal=0;

                     foreach ($COGS as $COGS){
                       // code...

                       $COGSTotal+= $COGS["Sum( all_transactions.dr_amount )"];

                       ?>

                       <tr>

                         <td><?= $COGS["accountName"];?></td>
                         <td><span class="align-numbers"><?= number_format($COGS["Sum( all_transactions.dr_amount )"],2) ;?></span></td>
                       </tr>

                       <?php

                     }

                      ?>

                      <tr></tr>


                     <tr>
                       <th>Total Cost of Goods Sold (COGS):</th>
                         <td><?= number_format($COGSTotal,2); ?></td>
                     </tr>

                     <tr></tr>

                     <tr>
                       <th>Gross Profit:</th>
                         <td><?= number_format($revenueTotal-$COGSTotal,2); ?></td>
                     </tr>
                   </table>

                   <!--  Operating Expenses-->
                   <?php


                      $command3 = $connection->createCommand("SELECT
                                                       	all_transactions.accountParentId,
                                                       	all_transactions.accountsclassfication,
                                                       	all_transactions.accountId,
                                                       	all_transactions.accountName,
                                                       	Sum( all_transactions.dr_amount ),
                                                       	Sum( all_transactions.cr_amount ),
                                                       	all_transactions.transactionClassification,
                                                       	all_transactions.transactionCategory
                                                       FROM
                                                       	all_transactions
                                                       WHERE
                                                       	all_transactions.transactionClassification LIKE 'office expense'
                                                       	OR all_transactions.transactionClassification LIKE 'Lead expense'
                                                       GROUP BY
                                                       	all_transactions.accountId,
                                                       	all_transactions.transactionClassification
                                                       ORDER BY
                                                       	all_transactions.accountParentId ASC");

                      $operating = $command3->query();

                    ?>
                      <table class="table table table-hover">
                        <tr>Operating Expenses</tr>
                        <tr>
                          <th style="width:75%">Accounts:</th>
                          <th>Balance (Ksh)</th>
                        </tr>
                        <?php

                        $operatingTotal=0;

                        foreach ($operating as $operating){
                          // code...
                          $operatingTotal+= $operating["Sum( all_transactions.dr_amount )"];

                          ?>

                          <tr>

                            <td><?= $operating["accountName"];?></td>
                            <td><span class="align-numbers"><?= number_format($operating["Sum( all_transactions.dr_amount )"],2) ;?></span></td>
                          </tr>
                          <?php

                        }

                         ?>
                         <tr></tr>


                        <tr>
                          <th>Total Operating Expenses:</th>
                            <td><?= number_format($operatingTotal,2); ?></td>
                        </tr>
                        <tr>
                          <th>Net:</th>
                            <td><?= number_format($revenueTotal-$COGSTotal-$operatingTotal,2); ?></td>
                        </tr>
                      </table>



            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">


          </p>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->




      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="javascript:void(0)" onclick="window.print()"
             class="btn btn-xs btn-success m-b-10"><i
                  class="fa fa-print m-r-5"></i> Print</a>

            <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
