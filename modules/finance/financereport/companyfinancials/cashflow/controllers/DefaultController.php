<?php

namespace app\modules\finance\financereport\companyfinancials\cashflow\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
// for db functions
use yii\db\Expression;

use app\modules\finance\financereport\companyfinancials\models\AllTransactions;


/**
 * Default controller for the `cashflow` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new \yii\base\DynamicModel([
          'startDate','endDate'
        ]);
        $model->addRule(['startDate','endDate'], 'required')
              ->validate();

        if($model->load(Yii::$app->request->post())){
          // do somenthing with model

          $tafuta="we are here";
           // Get all the revenue
           $revenue = AllTransactions::find()->select(['referenceCode','SUM(all_transactions.dr_amount) as dr_amount','transactionCode','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                         ->Where('transactionCategory = :transactionCategory', [':transactionCategory' => "Revenue Payment"])
                                         ->andWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                         ->andWhere('status > :status', [':status' => 0])
                                         ->groupBy(['transactionCode','referenceCode'])
                                         ->orderBy(['accountParentId' => SORT_ASC,])
                                         ->all();
          // Get the $COGS
          $COGS = AllTransactions::find()->select(['referenceCode','SUM(all_transactions.cr_amount) as cr_amount','transactionCode','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                        ->Where("transactionClassification = 'Event expense Payment' OR ( transactionClassification = 'Bill Payment expense' AND projectId IS NOT NULL )
OR transactionClassification = 'Lead Expense Payment'")
          // ->orWhere('transactionClassification = :transactionClassification1', [':transactionClassification1' => "Event expense Payment"])
          //                                ->andWhere('transactionClassification = :transactionClassification and projectId IS NOT NULL', [':transactionClassification' => "Bill Payment expense"])
                                         ->andWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                         ->andWhere('status > :status', [':status' => 0])
                                         ->groupBy(['transactionCode','referenceCode'])
                                         ->orderBy(['accountParentId' => SORT_ASC,])
                                         ->all();
        // Get the Operating Cost
        $operating = AllTransactions::find()->select(['referenceCode','SUM(all_transactions.cr_amount) as cr_amount','transactionCode','accountParentId','accountsclassfication','accountId','accountName','transactionClassification','transactionCategory'])
                                       ->Where("transactionClassification = 'Office Expense Payment' OR ( transactionClassification = 'Bill Payment expense' AND projectId IS NULL )
	OR transactionClassification = 'Lead Expense Payment'")
                                       ->andWhere(['between', 'transactionDate', $model->startDate, $model->endDate])
                                       ->andWhere('status > :status', [':status' => 0])
                                       ->groupBy(['transactionCode','referenceCode'])
                                       ->orderBy(['accountParentId' => SORT_ASC,])
                                       ->all();

        if (!empty($tafuta)) {
          // code...
          return $this->render('index', [
            'revenue' => $revenue,
            'COGS' => $COGS,
            'startDate' => $model->startDate,
            'endDate' => $model->endDate,
            'operating' => $operating,
            'model' => $model,
            'tafuta' => $tafuta,
          ]);
        } else {
          // code...
          return $this->render('index', [
            'model' => $model,
          ]);
        }

      }else{
        // code...
        return $this->render('index', [
          'model' => $model,
        ]);
      }
      // return $this->render('index');
    }
}
