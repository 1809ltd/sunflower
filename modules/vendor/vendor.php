<?php

namespace app\modules\vendor;

/**
 * vendor module definition class
 */
class vendor extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\vendor\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->modules =[
          /*Vendor Sub Module*/
          'vendordetails' => [
            'class' => 'app\modules\vendor\vendordetails\vendordetails',
          ],
        ];

        // custom initialization code goes here
    }
}
