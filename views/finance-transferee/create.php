<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\transfer\models\FinanceTransferee */

$this->title = 'Create Finance Transferee';
$this->params['breadcrumbs'][] = ['label' => 'Finance Transferees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-transferee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
