<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventCaterogies */

$this->title = 'Create Event Caterogies';
$this->params['breadcrumbs'][] = ['label' => 'Event Caterogies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-caterogies-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
