<?php
use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
// invoices
use app\modules\finance\income\invoice\models\FinanceInvoice;
use app\modules\finance\income\invoice\models\FinanceInvoiceSearch;
/*GEtting the Lines relationship*/
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLines;
use app\modules\finance\income\invoice\invoiceitems\models\FinanceInvoiceLinesSearch;
/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FinanceInvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/*Invoices Balance*/
use app\modules\finance\financereport\companyfinancials\models\InvoiceBalances;
/*Account Receivable*/
use app\modules\finance\financereport\companyfinancials\models\AgedReceivables;

$piechartinvoicesummarydata = '';

$comingdue= AgedReceivables::find()->sum("`Coming Due`");
// // array
$thirty = AgedReceivables::find()->sum("`1-30 Days`");
$sixtydays= AgedReceivables::find()->sum("`31-60 Days`");
$ninety= AgedReceivables::find()->sum("`61-90 Days`");
$more90= AgedReceivables::find()->sum("`>90 Days`");
//
// //Comming due
$piechartinvoicesummarydata .= "{ value:".$comingdue.",color:'#4CAF50',highlight:'#4CAF50',label:'Coming Due'},";
// //  1-30 days
$piechartinvoicesummarydata .= "{ value:".$thirty.",color:'#F57C00',highlight:'#F57C00',label:'1-30 Days'},";
// // more than 30 days
$piechartinvoicesummarydata .= "{ value:".$sixtydays.",color:'#CCCCCC',highlight:'#CCCCCC',label:'31-60 Days'},";
// // more than sixty days
$piechartinvoicesummarydata .= "{ value:".$ninety.",color:'#2196F3',highlight:'#2196F3',label:'61-90 Days'},";
// // more that 90 days
$piechartinvoicesummarydata .= "{ value:".$more90.",color:'#E74C3C',highlight:'#E74C3C',label:'>90 Days'},";
//

$this->title = 'Finance Invoices';
$this->params['breadcrumbs'][] = $this->title;

// echo Yii::$app->controller->id;
?>
<!-- =========================================================== -->
      <div class="row">
        <div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Summary</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-8">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="250"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <ul class="chart-legend clearfix">
                    <li><i class="fa fa-circle-o" style="color:#4CAF50"></i> <?= "Coming Due" ?></li>
                    <li><i class="fa fa-circle-o" style="color:#F57C00"></i> <?= "31-60 Days" ?></li>
                    <li><i class="fa fa-circle-o" style="color:#CCCCCC"></i> <?= "31-60 Days" ?></li>
                    <li><i class="fa fa-circle-o" style="color:#2196F3"></i> <?= "61-90 Days" ?></li>
                    <li><i class="fa fa-circle-o" style="color:#E74C3C"></i> <?= ">90 Days" ?></li>

                  </ul>
                </div>
                <!-- /.col -->
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          <!-- LINE CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <i class="fa fa-bullhorn"></i>
              <h3 class="box-title">Line Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="lineChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col (RIGHT) -->
      </div>
      <!-- /.row -->
<!-- begin row -->
<!-- begin row -->
<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
    <div class="form-group">
      <?php
      $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        // 'id',
        'invoiceNumber',
        // 'customerId',
        [
          'attribute'=>'customerId',
          'value'=>'customer.customerDisplayName',
        ],
        [
          'attribute'=>'invoiceProjectId',
          'value'=>'invoiceProject.eventName',
        ],
        // 'localpurchaseOrder',
        //'invoiceProjectId',
        'invoiceTxnDate',
        // 'invoiceDeadlineDate',
        //'invoiceNote:ntext',
        //'invoiceFooter:ntext',
        //'invoiceTxnStatus:ntext',
        //'invoiceEmailStatus:email',
        //'invoiceBillEmail:email',
        'invoiceTaxAmount',
        'invoiceDiscountAmount',
        'invoiceSubAmount',
        'invoiceAmount',
        //'invoicedCreatedby',
        //'invoicedCreatedAt',
        //'invoicedUpdatedAt',
        //'invoicedDeletedAt',
        //'approvedby',
        ['class' => 'yii\grid\ActionColumn'],
      ];
//      // Renders a export dropdown menu
//      echo ExportMenu::widget([
//        'dataProvider' => $dataProvider,
//          // 'filterModel' => $searchModel,
//        'columns' => $gridColumns
//      ]);
      // You can choose to render your own GridView separately
      // echo \kartik\grid\GridView::widget([
      //   'dataProvider' => $dataProvider,
      //   'filterModel' => $searchModel,
      //   'columns' => $gridColumns
      // ]);
      ?>
      <p class="pull-right">
          <?= Html::a('Create Invoice', ['create'], ['class' => 'btn btn-success']) ?>
      </p>
    </div>
  </div>

  <div class="box-body finance-invoice-index">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?=  GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
            'rowOptions'=> function($model)
            {
              // code...
              if ($model->invoiceTxnStatus==1) {
                // code...
                return['class'=>'success'];
              } elseif ($model->invoiceTxnStatus==2) {
                // code...

                return['class'=>'warning'];
              }  else {
                // code...
                return['class'=>'danger'];
              }

            },
            'columns' => [
                ['class' => 'kartik\grid\ExpandRowColumn',
                  'value'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    return GridView::ROW_COLLAPSED;
                  },
                  'detail'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    $searchModel = new FinanceInvoiceLinesSearch();
                    $searchModel->invoiceId = $model->id;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                    return Yii::$app->controller->renderPartial('invoicedetails',[
                      'searchModel' => $searchModel,
                      'dataProvider' => $dataProvider,

                    ]);
                  },
              ],

              // 'id',
              'invoiceNumber',
              // 'customerId',
              [
                'attribute'=>'customerId',
                'value'=>'customer.customerDisplayName',
              ],
              [
                'attribute'=>'invoiceProjectId',
                'value'=>'invoiceProject.eventName',
              ],
              // 'localpurchaseOrder',
              //'invoiceProjectId',
              'invoiceTxnDate',
              // 'invoiceDeadlineDate',
              //'invoiceNote:ntext',
              //'invoiceFooter:ntext',
              //'invoiceTxnStatus:ntext',
              //'invoiceEmailStatus:email',
              //'invoiceBillEmail:email',
              'invoiceTaxAmount',
              'invoiceDiscountAmount',
              'invoiceSubAmount',
              'invoiceAmount',
              //'invoicedCreatedby',
              //'invoicedCreatedAt',
              //'invoicedUpdatedAt',
              //'invoicedDeletedAt',
              //'approvedby',


              // ['class' => 'yii\grid\ActionColumn'],
              // ['class' => 'yii\grid\ActionColumn'],
              [
                  'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                  'items' => [
                      [
                          'label' => 'View',
                          'url'   => ['view'],
                      ],
                      [
                          'label' => 'Update',
                          'url'   => ['update'],
                          // 'options'=>['class'=>'update-modal-click grid-action'],
                          // 'update'=>function($url,$model,$key){
                          //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                          //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                          //           'class'=>'update-modal-click grid-action',
                          //           'data-toggle'=>'tooltip',
                          //           'data-placement'=>'bottom',
                          //           'title'=>'Update'
                          //       ]);
                          //       return $btn;
                          // },
                          // 'option' 'class'=>'update-modal-click grid-action',
                          // 'data-toggle'=>'tooltip',
                          // 'data-placement'=>'bottom',
                      ],
                      [
                          'label'   => 'Disable',
                          'url'     => ['delete'],
                          'linkOptions' => [
                              'data-method' => 'post'
                          ],
                      ],
                  ]
              ],
            ],
        ]);
        ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->

<?php
$script = <<<JS
$(function () {
  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
  var pieChart       = new Chart(pieChartCanvas)
  var PieData        = [$piechartinvoicesummarydata]
  var pieOptions     = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    //String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    //Number - The width of each segment stroke
    segmentStrokeWidth   : 2,
    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    //Number - Amount of animation steps
    animationSteps       : 100,
    //String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : true,
    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    //Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : true,
    //String - A legend template
    legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
  }
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions)
})
JS;
$this->registerJs($script);
?>
<?php
// use yii\web\View;
// $this->registerJs("
//    // $('li.treeview').removeClass('active open');
//    $('li.treeview').addClass('active');
//    // $('#finance-invoice').addClass('active');
//    $('#finance-invoice').addClass('active');
// ", View::POS_READY);
?>
