<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-bills-payments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'billPaymentPayType') ?>

    <?= $form->field($model, 'billPaymentaccount') ?>

    <?= $form->field($model, 'billPaymentTotalAmt') ?>

    <?= $form->field($model, 'billPaymentBillRef') ?>

    <?php // echo $form->field($model, 'vendorId') ?>

    <?php // echo $form->field($model, 'billPaymentPrivateNote') ?>

    <?php // echo $form->field($model, 'billPaymentCreatedAt') ?>

    <?php // echo $form->field($model, 'billPaymentUpdatedAt') ?>

    <?php // echo $form->field($model, 'billPaymentDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'paymentstatus') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
