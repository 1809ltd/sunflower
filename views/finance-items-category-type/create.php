<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceItemsCategoryType */

$this->title = 'Create Service Category Type';
$this->params['breadcrumbs'][] = ['label' => 'Finance Items Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-items-category-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
