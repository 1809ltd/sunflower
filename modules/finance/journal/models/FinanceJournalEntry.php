<?php

namespace app\modules\finance\journal\models;

/*User details*/
use app\modules\user\userdetails\models\UserDetails;

/*GEtting the Lines relationship*/
use app\modules\finance\journal\models\FinanceJournalEntryLines;
use app\modules\finance\journal\models\FinanceJournalEntryLinesSearch;

use Yii;

/**
 * This is the model class for table "finance_journal_entry".
 *
 * @property int $id
 * @property string $journalEntryNo
 * @property string $journalEntrydate
 * @property int $preparedby
 * @property int $approvedby
 * @property string $createdate
 * @property string $deletedAt
 * @property string $updatedAt
 * @property int $status
 *
 * @property UserDetails $preparedby0
 * @property UserDetails $approvedby0
 * @property FinanceJournalEntryLines[] $financeJournalEntryLines
 *
 */
class FinanceJournalEntry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_journal_entry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['journalEntryNo', 'total','journalEntrydate', 'status'], 'required'],
            [['journalEntrydate', 'createdate', 'deletedAt', 'updatedAt'], 'safe'],
            [['preparedby', 'approvedby','total', 'status'], 'integer'],
            [['journalEntryNo'], 'string', 'max' => 50],
            [['journalEntryNo'], 'unique'],
            [['preparedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['preparedby' => 'id']],
            [['approvedby'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['approvedby' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'journalEntryNo' => 'Journal Entry No',
            'journalEntrydate' => 'Date',
            'preparedby' => 'Preparedby',
            'total' => 'Amount',
            'approvedby' => 'Approvedby',
            'createdate' => 'Createdate',
            'deletedAt' => 'Deleted At',
            'updatedAt' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreparedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'preparedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedby0()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'approvedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceJournalEntryLines()
    {
        return $this->hasMany(FinanceJournalEntryLines::className(), ['journalId' => 'id']);
    }
    // Generating Jounral Entry
    public function getJouEntynumber()
    {
      //select product code

      $connection = Yii::$app->db;
      $query= "Select MAX(journalEntryNo) AS number from finance_journal_entry where id>0";
      $rows= $connection->createCommand($query)->queryAll();
      if ($rows) {
        // code...
        $number=$rows[0]['number'];
        $number++;
        if ($number == 1) {
          // code...
          $number =   "JourNo:-".date('Y')."-".date('m')."-0001";
        }
        if ($number == 1) {
          // code...
          $number = "JourNo:-".date('Y')."-".date('m')."-0001";
        }
      }else{
        // code...
        //generating numbers
        $number = "JourNo:-".date('Y')."-".date('m')."-0001";
      }
      return $number;
    }

    public function getJournalItems($id,$status)
    {
      return FinanceJournalEntryLines::find()
        ->where(['and', "journalId=$id"])
        ->where(['and', "status=$status"])
        ->all();
    }
}
