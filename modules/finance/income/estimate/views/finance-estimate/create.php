<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceEstimate */

$this->title = 'Create Finance Quotation';
$this->params['breadcrumbs'][] = ['label' => 'Finance Quotation', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-estimate-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceEstimateLines'=>$modelsFinanceEstimateLines, //to enable Dynamic Form
    ]) ?>

</div>
