<?php

namespace app\models;


use app\models\EventDetails;


use Yii;

/**
 * This is the model class for table "event_transaction".
 *
 * @property int $transactionId
 * @property int $referenceId
 * @property int $eventId
 * @property int $customerId
 * @property string $eventNumber
 * @property string $eventName
 * @property string $eventCatName
 * @property string $transactionNumber
 * @property double $qty
 * @property double $unitprice
 * @property string $dr_amount
 * @property string $cr_amount
 * @property string $transName
 * @property string $transDesc
 * @property int $accountId
 * @property string $fullyQualifiedName
 * @property string $createdAt
 * @property int $status
 * @property string $txnDate
 */
class EventTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transactionId', 'referenceId', 'eventId', 'customerId', 'accountId', 'status'], 'integer'],
            [['qty', 'unitprice'], 'number'],
            [['transDesc'], 'string'],
            [['createdAt', 'txnDate'], 'safe'],
            [['eventNumber', 'eventName', 'eventCatName', 'transactionNumber'], 'string', 'max' => 100],
            [['dr_amount', 'cr_amount'], 'string', 'max' => 22],
            [['transName', 'fullyQualifiedName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'transactionId' => 'Transaction ID',
            'referenceId' => 'Reference ID',
            'eventId' => 'Event ID',
            'customerId' => 'Customer ID',
            'eventNumber' => 'Event Number',
            'eventName' => 'Event Name',
            'eventCatName' => 'Event Cat Name',
            'transactionNumber' => 'Transaction Number',
            'qty' => 'Qty',
            'unitprice' => 'Unitprice',
            'dr_amount' => 'Dr Amount',
            'cr_amount' => 'Cr Amount',
            'transName' => 'Trans Name',
            'transDesc' => 'Trans Desc',
            'accountId' => 'Account ID',
            'fullyQualifiedName' => 'Fully Qualified Name',
            'createdAt' => 'Created At',
            'status' => 'Status',
            'txnDate' => 'Txn Date',
        ];
    }
}
