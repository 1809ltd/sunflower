<?php

namespace app\modules\finance\expensepayment\leadexpensepayment\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\expensepayment\leadexpensepayment\models\FinanceLeadRequisitionPayments;

/**
 * FinanceLeadRequisitionPaymentsSearch represents the model behind the search form of `app\modules\finance\expensepayment\leadexpensepayment\models\FinanceLeadRequisitionPayments`.
 */
class FinanceLeadRequisitionPaymentsSearch extends FinanceLeadRequisitionPayments
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'reqpaymentAccount', 'reqpaymentProcessduration', 'reqpaymentStatus', 'userId'], 'integer'],
            [['reqrecepitNumber', 'reqpaymentName', 'reqpaymentTxnDate', 'reqpaymentType', 'reqpaymentRefence', 'reqpaymentLinkedTxn', 'reqpaymentLinkedTxnType', 'reqpaymentCreateAt', 'reqpaymentUpdatedAt', 'reqpaymentDeletedAt'], 'safe'],
            [['reqpaymentTotalAmt'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceLeadRequisitionPayments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reqpaymentAccount' => $this->reqpaymentAccount,
            'reqpaymentTxnDate' => $this->reqpaymentTxnDate,
            'reqpaymentTotalAmt' => $this->reqpaymentTotalAmt,
            'reqpaymentProcessduration' => $this->reqpaymentProcessduration,
            'reqpaymentStatus' => $this->reqpaymentStatus,
            'reqpaymentCreateAt' => $this->reqpaymentCreateAt,
            'reqpaymentUpdatedAt' => $this->reqpaymentUpdatedAt,
            'reqpaymentDeletedAt' => $this->reqpaymentDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'reqrecepitNumber', $this->reqrecepitNumber])
            ->andFilterWhere(['like', 'reqpaymentName', $this->reqpaymentName])
            ->andFilterWhere(['like', 'reqpaymentType', $this->reqpaymentType])
            ->andFilterWhere(['like', 'reqpaymentRefence', $this->reqpaymentRefence])
            ->andFilterWhere(['like', 'reqpaymentLinkedTxn', $this->reqpaymentLinkedTxn])
            ->andFilterWhere(['like', 'reqpaymentLinkedTxnType', $this->reqpaymentLinkedTxnType]);

        return $dataProvider;
    }
}
