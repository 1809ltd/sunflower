<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EventItemRole */

$this->title = 'Create Event Item Role';
$this->params['breadcrumbs'][] = ['label' => 'Event Item Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-item-role-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
