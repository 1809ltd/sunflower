<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EventItemCategoryType */

$this->title = 'Update Event Item Category Type: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Item Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="event-item-category-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
