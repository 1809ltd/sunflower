<?php

namespace app\modules\finance\productsetup\financeitemscategory\controllers;

use Yii;
use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategory;
use app\modules\finance\productsetup\financeitemscategory\models\FinanceItemsCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceItemsCategoryController implements the CRUD actions for FinanceItemsCategory model.
 */
class FinanceItemsCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceItemsCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';

      $searchModel = new FinanceItemsCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceItemsCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
      return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceItemsCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
      $model = new FinanceItemsCategory();
      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FinanceItemsCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
      $model = $this->findModel($id);
      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FinanceItemsCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
      // $this->findModel($id)->delete();

      $this->findModel($id)->updateAttributes(['financeItemCategoryStatus'=>0,'deletedAt' => new \yii\db\Expression('NOW()')]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceItemsCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceItemsCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceItemsCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
