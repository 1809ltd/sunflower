<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\inventory\assetmovement\delivery\models\AssetDeliveryformSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asset Deliveryforms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-deliveryform-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Asset Deliveryform', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'deliveryNumber',
            'eventId',
            'orderId',
            'duedate',
            //'date',
            //'deliveryStatus',
            //'delivery:ntext',
            //'createdAt',
            //'updatedAt',
            //'deletedAt',
            //'userId',
            //'createdBy',
            //'approvedBy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
