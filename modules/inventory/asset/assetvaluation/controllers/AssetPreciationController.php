<?php

namespace app\modules\inventory\asset\assetvaluation\controllers;

use Yii;
use app\modules\inventory\asset\assetvaluation\models\AssetPreciation;
use app\modules\inventory\asset\assetvaluation\models\AssetPreciationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

//
use app\models\AssetAmortization;
use app\models\AssetAmortizationSearch;

use app\modules\inventory\asset\assetregistration\models\AssetRegistration;

/**
 * AssetPreciationController implements the CRUD actions for AssetPreciation model.
 */
class AssetPreciationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
        $this->preciationUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->preciationUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all AssetPreciation models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $searchModel = new AssetPreciationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AssetPreciation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AssetPreciation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $model = new AssetPreciation();
        
        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveAmortization($model);
            return $this->redirect(['view', 'id' => $model->id]);

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function saveAmortization($model)
    {
         $amortization = new AssetAmortization();

        $assetId = $model->assetId;
        $preciableCost = $model->preciableCost;
        $preciationSalvageValue = $model->preciationSalvageValue;
        $assetLifeMonths = $model->assetLifeMonths;
        $preciationDateAcquired = $model->preciationDateAcquired;
        $preciationDateAcquiredDepreciationMethod = $model->preciationDateAcquiredDepreciationMethod;

         $model = AssetRegistration::find()
           ->select('assetCost')
           ->where(['id' => $assetId])
           ->one();

        // asset value is the asset value on purchase
        $asset_value = $model->assetCost;

        // no of repayments is the period that you can allow to have the product in your castody
        $no_of_repayments = $assetLifeMonths;

        // first date is the purchase date of the product
        $first_date = $preciationDateAcquired;

        //interest id:  1 for straight line and 2 for redusing balance
        $interest_id = $preciationDateAcquiredDepreciationMethod;

        // interest rate could mean the rate at which the product is appretiating in percentage
        $interest_rate = $preciableCost;

        // installment type duration is the period interval the product is appretiating or depreciating
        $installment_type_duration = 1;



        $result = '';

        if($asset_value > 0)
        {

            $cummulative_interest = 0;
            $cummulative_principal = 0;
            $start_balance = $asset_value;
            $total_days = 0;

            $data = array();
            //display all payment dates
            for($r = 0; $r < $no_of_repayments; $r++)
            {
                $total_days += $installment_type_duration;
                $count = $r+1;
                $payment_date = date('jS M Y', strtotime($first_date. ' + '.$total_days.' months'));
                $payment_date2 = date('Y-m-d', strtotime($first_date. ' + '.$total_days.' months'));
                // var_dump($payment_date2); die();
                //straight line
                if($interest_id == 1)
                {
                    //$interest_payment = ($asset_value * ($interest_rate/100)) / $no_of_repayments;
                    $interest_payment = ($asset_value * ($interest_rate/100));
                }

                //reducing balance
                else
                {
                    //$interest_payment = ($start_balance * ($interest_rate/100)) / $no_of_repayments;
                    $interest_payment = ($start_balance * ($interest_rate/100));
                }
                $principal_payment = round(($asset_value / $no_of_repayments),-3);
                $end_balance = $start_balance - $principal_payment;
                $cummulative_interest += $interest_payment;
                $cummulative_principal += $principal_payment;

                if ($count == $no_of_repayments)
                {
                    $principal_payment = $start_balance;
                    $end_balance = $start_balance - $principal_payment;
                    $cummulative_principal = $asset_value;
                }

        // $result .= '
        // <tr>
        //     <td>'.$count.'</td>
        //     <td>'.$payment_date.'</td>
        //     <td>'.number_format($start_balance, 2).'</td>
        //     <td>'.number_format($interest_payment, 2).'</td>
        //     <td>'.number_format($principal_payment, 2).'</td>
        //     <td>'.number_format($end_balance, 2).'</td>
        //     <td>'.number_format($cummulative_interest, 2).'</td>
        //     <td>'.number_format($cummulative_principal, 2).'</td>
        // </tr>';

                $data[] = [$count,$start_balance,$interest_payment, $principal_payment, $end_balance,$cummulative_interest,$cummulative_principal,$assetId, $payment_date2,1,1,];


                // $amortization->save();


                $start_balance -= $principal_payment;
            }
            // var_dump($data); die();
             Yii::$app->db
                ->createCommand()
                ->batchInsert('asset_amortization', ['repayment','startBalance','interestAmount', 'principalAmount','endBalance','cummulativeInterest','cummulativePrincipal','assetId','amortizationDate','status','userId'],$data)
                ->execute();



        }
    }

    /**
     * Updates an existing AssetPreciation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      // $this->layout='addformdatatablelayout';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AssetPreciation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AssetPreciation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AssetPreciation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AssetPreciation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
