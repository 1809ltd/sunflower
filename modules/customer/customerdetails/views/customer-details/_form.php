<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\customer\customerdetails\models\CustomerDetails */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body customer-details-form">

    <?= $form->field($model, 'customerFullname')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerDisplayName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerCompanyName')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerKraPin')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerPrimaryPhone')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerPrimaryEmail')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customercity')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerCounty')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerCountry')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerPhyscialAdress')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?= $form->field($model, 'customerAddress')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

    <?php
        // necessary for update action.
        if (!$model->isNewRecord) {
          // code...
            $status = ['1' => 'Active', '2' => 'Suspended'];
            echo $form->field($model, 'customerstatus')->dropDownList($status, ['prompt'=>'Select Status']);


        } else {
          // code...
            echo $form->field($model, 'customerstatus')->hiddenInput(['value'=> "1"])->label(false);

        }
    ?>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
