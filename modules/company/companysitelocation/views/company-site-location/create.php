<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\company\companysitelocation\models\CompanySiteLocation */

$this->title = 'Create Company Site Location';
$this->params['breadcrumbs'][] = ['label' => 'Company Site Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-site-location-create">
  
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
