<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;



?>
<div class="finance-estimate-lines-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'biilsId',
            'biilsLinesVendorsRef',
            'biilsLinesDescription:ntext',
            'biilsLinespacks',
            'biilsLinesQty',
            'billlinePriceperunit',
            'biilsLinesAmount',
            'billLinesTaxCodeRef',
            'biilsLinesTaxAmount',
            //'accountId',
            //'billsLinesBillableStatus',
            //'billsLinesCreatedAt',
            //'billsLinesDeleteAt',
            //'billsLinesUpdatedAt',
            //'userId',

        ],
    ]); ?>

</div>
