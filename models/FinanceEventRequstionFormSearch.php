<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FinanceEventRequstionForm;

/**
 * FinanceEventRequstionFormSearch represents the model behind the search form of `app\models\FinanceEventRequstionForm`.
 */
class FinanceEventRequstionFormSearch extends FinanceEventRequstionForm
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'eventId', 'requstionStatus', 'preparedBy', 'userId', 'approvedBy'], 'integer'],
            [['requstionNumber', 'date', 'requstionNote', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceEventRequstionForm::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'eventId' => $this->eventId,
            'date' => $this->date,
            'requstionStatus' => $this->requstionStatus,
            'preparedBy' => $this->preparedBy,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'approvedBy' => $this->approvedBy,
        ]);

        $query->andFilterWhere(['like', 'requstionNumber', $this->requstionNumber])
            ->andFilterWhere(['like', 'requstionNote', $this->requstionNote]);

        return $dataProvider;
    }
}
