<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoiceLines */

$this->title = 'Update Finance Invoice Lines: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Invoice Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-invoice-lines-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
