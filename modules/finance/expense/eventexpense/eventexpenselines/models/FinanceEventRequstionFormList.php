<?php

namespace app\modules\finance\expense\eventexpense\eventexpenselines\models;

/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
use app\modules\event\eventInfo\models\EventDetails;
use app\modules\finance\productsetup\financeeventactivity\models\FinanceEventActivity;
use app\modules\personnel\personneldetails\models\PersonnelDetails;
use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionForm;
use app\modules\event\workplan\models\EventWorkplan;



use Yii;

/**
 * This is the model class for table "finance_event_requstion_form_list".
 *
 * @property int $id
 * @property int $reqId
 * @property int $activityId
 * @property int $personelId
 * @property int $workplanId
 * @property string $personnelTel
 * @property string $description
 * @property double $qty
 * @property double $unitprice
 * @property double $amont
 * @property int $status
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 *
 * @property FinanceEventActivity $activity
 * @property PersonnelDetails $personel
 * @property FinanceEventRequstionForm $req
 * @property EventWorkplan $workplan
 * @property UserDetails $user
 */
class FinanceEventRequstionFormList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_event_requstion_form_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activityId', 'qty', 'unitprice', 'amont'], 'required'],
            [['reqId', 'activityId', 'personelId', 'workplanId', 'status', 'userId'], 'integer'],
            [['description'], 'string'],
            [['qty', 'unitprice', 'amont'], 'number'],
            [['reqId', 'status', 'personnelTel', 'userId', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['personnelTel'], 'string', 'max' => 15],
            [['activityId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEventActivity::className(), 'targetAttribute' => ['activityId' => 'id']],
            [['personelId'], 'exist', 'skipOnError' => true, 'targetClass' => PersonnelDetails::className(), 'targetAttribute' => ['personelId' => 'id']],
            [['reqId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceEventRequstionForm::className(), 'targetAttribute' => ['reqId' => 'id']],
            [['workplanId'], 'exist', 'skipOnError' => true, 'targetClass' => EventWorkplan::className(), 'targetAttribute' => ['workplanId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reqId' => 'Req',
            'activityId' => 'Expense Activity',
            'personelId' => 'Personel',
            'workplanId' => 'Workplan',
            'personnelTel' => 'Personnel Phone',
            'description' => 'Description',
            'qty' => 'Qty',
            'unitprice' => 'Unit Price',
            'amont' => 'Amount',
            'status' => 'Status',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivity()
    {
        return $this->hasOne(FinanceEventActivity::className(), ['id' => 'activityId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonel()
    {
        return $this->hasOne(PersonnelDetails::className(), ['id' => 'personelId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReq()
    {
        return $this->hasOne(FinanceEventRequstionForm::className(), ['id' => 'reqId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkplan()
    {
        return $this->hasOne(EventWorkplan::className(), ['id' => 'workplanId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
