<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceCustomerPurchaseOrder */

$this->title = 'Create Finance Customer Purchase Order';
$this->params['breadcrumbs'][] = ['label' => 'Finance Customer Purchase Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-customer-purchase-order-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceCustomerPurchaseOrderLines'=>$modelsFinanceCustomerPurchaseOrderLines, //to enable Dynamic Form
    ]) ?>

</div>
