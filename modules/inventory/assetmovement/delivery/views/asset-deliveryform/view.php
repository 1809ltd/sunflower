<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
// use yii\helpers\Url;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/*Asset Items*/
use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform;
use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryformSearch;

/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoice */

$this->title = $model->deliveryNumber;
$this->params['breadcrumbs'][] = ['label' => 'Delivery Form', 'url' => ['deliverysearch']];
$this->params['breadcrumbs'][] = $this->title;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();

?>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <p>
      <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
  </p>
  <!-- Pop up Form -->
  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h6>
           <img style="height:85%;width:85%" class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h6>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Delivery Form</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">

         <address>
           <strong>Client: </strong> <?= $model['event']['customer']['customerCompanyName']?><br/>
           <strong> No of guests:</strong> <?= $model->event['eventVistorsNumber']?> Pax<br/>
             <strong>Date of Function: </strong> <?= $model->event['eventStartDate']?><br/>
             <strong>Date of Set Up: </strong> <?= $model->event['eventSetUpDateTime']?><br/>

         Attn: <?= $model['event']['customer']['customerFullname']?>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

         <strong>Colour Scheme: </strong> <?= $model->event['eventTheme']?><br/>
         <strong>Event: </strong> <?= $model->event['eventName']?> <br/>
         <strong>Location: </strong> <?= $model->event['eventLocation']?><br/>
     </address>

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Delivery No: #<?=$model->deliveryNumber;?></b><br>
         <b>Date: </b> <?= Yii::$app->formatter->asDate($model->date, 'long');?><br>
         <b>Due Date: </b> <?= Yii::$app->formatter->asDate($model->duedate, 'long');?><br>
         <b>Account:</b> <?= $model['event']['customer']['customerNumber']?>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <?php
      $modelsAssetCheckOut = new AssetDeliveryform();
      ?>

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th>Assest Name</th>
             <th>Assest Tag</th>
             <th>Service & Description</th>
           </thead>
           <tbody>
             <?php

             // die();
             $id = $model->id;
             $deliveryStatus=$model->deliveryStatus;
             $CheckOutItems=$model->getAssetCheckoutList($id,$deliveryStatus);

             foreach ($CheckOutItems as $CheckOutItem) {
               // code...
               // print_r($CheckOutItem);


               // die();
               ?>

               <tr>
                   <td><?=$CheckOutItem->asset['assetName'];?></td>
                   <td><?=$CheckOutItem->asset['assetTag'];?></td>
                   <td><?= $CheckOutItem->orderlist['item']['itemsName']?>:</td>

               </tr>

               <?php

             }

              ?>

           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           <?php
           // $model->invoiceNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          <?php
          // $model->invoiceFooter; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <!-- /.row -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->

         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
