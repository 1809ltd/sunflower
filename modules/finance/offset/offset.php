<?php

namespace app\modules\finance\offset;

/**
 * offset module definition class
 */
class offset extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\offset\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Inhouse Sub Module*/
          'inhouse' => [
                'class' => 'app\modules\finance\offset\inhouse\inhouse',
            ],
            /*Outhouse*/
          'outhouse' => [
              'class' => 'app\modules\finance\offset\outhouse\outhouse',
          ],
        ];

        // custom initialization code goes here
    }
}
