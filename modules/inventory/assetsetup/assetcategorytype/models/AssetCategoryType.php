<?php

namespace app\modules\inventory\assetsetup\assetcategorytype\models;
/*Asset Category*/
use app\modules\inventory\assetsetup\assetcategory\models\AssetCategory;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;

use Yii;

/**
 * This is the model class for table "asset_category_type".
 *
 * @property int $id
 * @property int $assetCategoryId
 * @property string $assetCategoryTypeName
 * @property string $assetCategoryTypeDescription
 * @property int $assetCategoryTypeStatus
 * @property int $userId
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 *
 * @property AssetCategory $assetCategory
 * @property UserDetails $user
 * @property AssetRegistration[] $assetRegistrations
 * @property EventItemCategoryType[] $eventItemCategoryTypes
 * @property FinanceItems[] $items
 */
class AssetCategoryType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_category_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetCategoryId', 'assetCategoryTypeName', 'assetCategoryTypeStatus', 'userId'], 'required'],
            [['assetCategoryId', 'assetCategoryTypeStatus', 'userId'], 'integer'],
            [['assetCategoryTypeDescription'], 'string'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['assetCategoryTypeName'], 'string', 'max' => 50],
            [['assetCategoryTypeName'], 'unique'],
            [['assetCategoryId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetCategory::className(), 'targetAttribute' => ['assetCategoryId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assetCategoryId' => 'Asset Category',
            'assetCategoryTypeName' => 'Asset Category Type Name',
            'assetCategoryTypeDescription' => 'Description',
            'assetCategoryTypeStatus' => 'Status',
            'userId' => 'User ID',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetCategory()
    {
        return $this->hasOne(AssetCategory::className(), ['id' => 'assetCategoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAssetRegistrations()
    {
        return $this->hasMany(AssetRegistration::className(), ['assetCategoryType' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventItemCategoryTypes()
    {
        return $this->hasMany(EventItemCategoryType::className(), ['categorytypeId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(FinanceItems::className(), ['id' => 'itemId'])->viaTable('event_item_category_type', ['categorytypeId' => 'id']);
    }
}
