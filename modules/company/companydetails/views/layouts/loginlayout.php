<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;

use yii\widgets\Breadcrumbs;
use app\assets\LoginEventAsset;

LoginEventAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="animated fadeInLeft" id="page">
  <?php $this->beginBody() ?>
<header id="header">

    <div id="logo-group">
        <a href="#" class="navbar-brand f-w-500 m-t-5">
            <img src="http://sunflowertents.com/wp-content/uploads/2017/11/unnamed-1.png" alt="logo" style="display: inline-block">
            <span class="m-l-10">

            </span>
        </a>
    </div>

    <span id="page-header-space">
        <a href="javascript:void(0);" class="hidden-mobile hidden-xs btn btn-primary" id="date"></a>
		<a href="javascript:void(0);" class="hidden-mobile hidden-xs btn btn-primary" id="time"
           style="height: 32px;margin-top: 5px;"></a>
    </span>

</header>

<div id="main" role="main">

    <!-- MAIN CONTENT -->
    <div id="content" class="container">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
                <h1 class="txt-color-red login-header-big">Sunflower Event Management Solution</h1>
                <div class="hero">

                    <div class="pull-left login-desc-box-l">
                        <h4 class="paragraph-header text-justify">
                            Sunflower Events is your complete event solution. We own most of the sound equipment that is needed for any event. Most event companies lease equipment from other companies, but having our own equipment means we are able to fully cater to you without compromising on standards, and this, in turn, will translate to be more affordable. When we tell you that we are a one-stop-shop, we mean it in every sense.
                        </h4>
                    </div>

                  <!--  <img src="http://via.placeholder.com/913x1730" class="pull-right display-image" alt="" style="width:210px">

                  -->

                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h5 class="about-heading">...</h5>
                        <p>
                            ..
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h5 class="about-heading">!</h5>
                        <p>
                            ...
                        </p>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                <div class="well p-0">
                  <?= Alert::widget() ?>
                  <?= $content ?>
                </div>

                <h5 class="text-center"> - Or sign in using -</h5>

                <ul class="list-inline text-center">
                    <li>
                        <a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
                    </li>
                </ul>

            </div>
        </div>
    </div>

</div>

</body>b

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
