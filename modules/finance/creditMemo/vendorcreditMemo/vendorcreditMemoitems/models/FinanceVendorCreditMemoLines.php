<?php

namespace app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models;
/*USer details*/
use app\modules\user\userdetails\models\UserDetails;
/*Bills*/
use app\modules\finance\expense\bills\models\FinanceBills;

use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Accounts */
use app\modules\finance\creditMemo\vendorcreditMemo\models\FinanceVendorCreditMemo;

use Yii;

/**
 * This is the model class for table "finance_vendor_credit_memo_lines".
 *
 * @property int $id
 * @property int $vendorCreditMemoId
 * @property string $billRef
 * @property string $vendorCreditMemoLineDescription
 * @property double $vendorCreditMemoLineAmount
 * @property double $creditTaxCodeRef
 * @property double $taxamount
 * @property int $vendorCreditMemoLineStatus
 * @property string $vendorCreditMemoLineCreatedAt
 * @property string $vendorCreditMemoLineUpdatedAt
 * @property string $creidtDeletedAt
 * @property int $userId
 *
 * @property FinanceVendorCreditMemo $vendorCreditMemo
 * @property UserDetails $user
 */
class FinanceVendorCreditMemoLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_vendor_credit_memo_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['billRef', 'vendorCreditMemoLineAmount', 'creditTaxCodeRef', 'taxamount'], 'required'],
            [['vendorCreditMemoId', 'vendorCreditMemoLineStatus', 'userId'], 'integer'],
            [['vendorCreditMemoLineDescription'], 'string'],
            [['vendorCreditMemoLineAmount', 'creditTaxCodeRef', 'taxamount'], 'number'],
            [['vendorCreditMemoId', 'vendorCreditMemoLineCreatedAt', 'vendorCreditMemoLineUpdatedAt', 'creidtDeletedAt', 'vendorCreditMemoLineStatus', 'userId'], 'safe'],
            [['billRef'], 'string', 'max' => 50],
            [['vendorCreditMemoId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceVendorCreditMemo::className(), 'targetAttribute' => ['vendorCreditMemoId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendorCreditMemoId' => 'Vendor Credit Memo',
            'billRef' => 'Invoice',
            'vendorCreditMemoLineDescription' => 'Description',
            'vendorCreditMemoLineAmount' => 'Amount',
            'creditTaxCodeRef' => 'Tax Code Ref',
            'taxamount' => 'Taxamount',
            'vendorCreditMemoLineStatus' => 'Status',
            'vendorCreditMemoLineCreatedAt' => 'Created At',
            'vendorCreditMemoLineUpdatedAt' => 'Updated At',
            'creidtDeletedAt' => 'Creidt Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorCreditMemo()
    {
        return $this->hasOne(FinanceVendorCreditMemo::className(), ['id' => 'vendorCreditMemoId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
