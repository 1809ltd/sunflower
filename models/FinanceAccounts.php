<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_accounts".
 *
 * @property int $id
 * @property int $companyId
 * @property string $subAccount
 * @property int $accountParentId
 * @property string $accountName
 * @property string $accountNumber
 * @property string $accountsDescription
 * @property string $fullyQualifiedName
 * @property string $accountsClassification
 * @property string $accountsType
 * @property string $accountsDetailType
 * @property string $accountsPays
 * @property int $accountsStatus
 * @property double $accountsCurrentBalance
 * @property string $accountsCreateTime
 * @property string $accountsUpdatedAt
 * @property string $accountsDeleteAt
 * @property int $userId
 *
 * @property FinanceBillsLines[] $financeBillsLines
 * @property FinanceBillsPayments[] $financeBillsPayments
 * @property FinanceCreditMemo[] $financeCreditMemos
 * @property FinanceEventActivity[] $financeEventActivities
 * @property FinanceItems[] $financeItems
 * @property FinanceItems[] $financeItems0
 * @property FinanceLeadRequisitionPayments[] $financeLeadRequisitionPayments
 * @property FinanceOfficeRequisitionPayments[] $financeOfficeRequisitionPayments
 * @property FinancePayment[] $financePayments
 * @property FinanceRequisitionPayments[] $financeRequisitionPayments
 * @property FinanceTax[] $financeTaxes
 * @property FinanceVendorCreditMemo[] $financeVendorCreditMemos
 */
class FinanceAccounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['companyId', 'subAccount', 'accountName', 'accountsDescription', 'fullyQualifiedName', 'accountsClassification', 'accountsType', 'accountsDetailType', 'accountsPays', 'accountsStatus', 'accountsCurrentBalance', 'userId'], 'required'],
            [['companyId', 'accountParentId', 'accountsStatus', 'userId'], 'integer'],
            [['accountsDescription'], 'string'],
            [['accountsCurrentBalance'], 'number'],
            [['accountsCreateTime', 'accountsUpdatedAt', 'accountsDeleteAt'], 'safe'],
            [['subAccount'], 'string', 'max' => 50],
            [['accountName', 'fullyQualifiedName', 'accountsClassification', 'accountsType', 'accountsDetailType'], 'string', 'max' => 255],
            [['accountNumber'], 'string', 'max' => 100],
            [['accountsPays'], 'string', 'max' => 5],
            [['accountName'], 'unique'],
            [['accountNumber'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'companyId' => 'Company',
            'subAccount' => 'Sub Account',
            'accountParentId' => 'Account Parent',
            'accountName' => 'Account Name',
            'accountNumber' => 'Account Number',
            'accountsDescription' => 'Accounts Description',
            'fullyQualifiedName' => 'Fully Qualified Name',
            'accountsClassification' => 'Accounts Classification',
            'accountsType' => 'Accounts Type',
            'accountsDetailType' => 'Accounts Detail Type',
            'accountsPays' => 'Accounts Pays',
            'accountsStatus' => 'Accounts Status',
            'accountsCurrentBalance' => 'Accounts Current Balance',
            'accountsCreateTime' => 'Accounts Create Time',
            'accountsUpdatedAt' => 'Accounts Updated At',
            'accountsDeleteAt' => 'Accounts Delete At',
            'userId' => 'User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsLines()
    {
        return $this->hasMany(FinanceBillsLines::className(), ['accountId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceBillsPayments()
    {
        return $this->hasMany(FinanceBillsPayments::className(), ['billPaymentaccount' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceCreditMemos()
    {
        return $this->hasMany(FinanceCreditMemo::className(), ['creditMemoAccount' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceEventActivities()
    {
        return $this->hasMany(FinanceEventActivity::className(), ['account' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItems()
    {
        return $this->hasMany(FinanceItems::className(), ['itemsIncomeAccountRef' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceItems0()
    {
        return $this->hasMany(FinanceItems::className(), ['itemsExpenseAccountRef' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceLeadRequisitionPayments()
    {
        return $this->hasMany(FinanceLeadRequisitionPayments::className(), ['reqpaymentAccount' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOfficeRequisitionPayments()
    {
        return $this->hasMany(FinanceOfficeRequisitionPayments::className(), ['reqpaymentAccount' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinancePayments()
    {
        return $this->hasMany(FinancePayment::className(), ['paymentDepositToAccount' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceRequisitionPayments()
    {
        return $this->hasMany(FinanceRequisitionPayments::className(), ['reqpaymentAccount' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceTaxes()
    {
        return $this->hasMany(FinanceTax::className(), ['account' => 'accountName']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceVendorCreditMemos()
    {
        return $this->hasMany(FinanceVendorCreditMemo::className(), ['vendorCreditMemoAccount' => 'id']);
    }
}
