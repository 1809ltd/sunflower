<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\modules\finance\financereport\companyfinancials\models\AgePayables;

/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'home', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();

?>
<section class="invoice">
  <div class="text-center">
    <h6 class="box-title">
      <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
    </h6>
    <h3 class="box-title">Aged Payables</h3>
    <h5 class="box-title">As of:<?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h5>
    <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
  </div>

  <div class="box">
    <div class="box-body">
      <h5 class="box-title">REVENUE</h5>
      <table class="table  table-striped table-condensed">
        <thead>
          <tr>
            <th class="text-left">Payables</th>
            <th class="text-right">Coming Due	</th>
            <th class="text-right">	1- 30 days</th>
            <th class="text-right">31- 60 days</th>
            <th class="text-right">61- 90 days</th>
            <th class="text-right">Over 90 days</th>
            <th class="text-right">Unllocated</th>
            <th class="text-right">Total</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $agedpayables= AgePayables::find()->all();

          $Sumcoming=0;
          $Sum30=0;
          $Sum60=0;
          $Sum90=0;
          $Sumc90=0;
          $SumTT=0;
          $SumunTT=0;



          //

          foreach ($agedpayables as $agedpayable) {
            // code...

            $agedpayable->vendorId;

            $Sumcoming+= $agedpayable["Coming Due"];
            $Sum30+= $agedpayable["1-30 Days"];
            $Sum60+= $agedpayable["31-60 Days"];
            $Sum90+= $agedpayable["61-90 Days"];
            $Sumc90+= $agedpayable[">90 Days"];
            $SumunTT+=$agedpayable->unlocated;
            $SumTT+= $agedpayable->Total;


            ?>
            <tr>

              <td class="text-left"><?= $agedpayable->payables;?></td>
              <td class="text-right"><?= number_format($agedpayable["Coming Due"],2);?></td>
              <td class="text-right"><?= number_format($agedpayable["1-30 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedpayable["31-60 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedpayable["61-90 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedpayable[">90 Days"],2);?></td>
              <td class="text-right"><?= number_format($agedpayable->unlocated,2);?></td>
              <td class="text-right"><?= number_format($agedpayable->Total,2);?></td>

            </tr>


            <?php


          }



           ?>

           <tr>

               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>

           </tr>
           <tr>

               <th class="text-left">Total Receivable</th>
               <td class="text-right"><?= number_format($Sumcoming,2);?></td>
               <td class="text-right"><?= number_format($Sum30,2);?></td>
               <td class="text-right"><?= number_format($Sum60,2);?></td>
               <td class="text-right"><?= number_format($Sum90,2);?></td>
               <td class="text-right"><?= number_format($Sumc90,2);?></td>
               <th class="text-right"><?= number_format($SumunTT,2);?></th>
               <th class="text-right"><?= number_format($SumTT,2);?></th>

           </tr>

         </tbody>
     </table>
   </br>

  </div>
</div>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <div class="col-xs-12">
    <a href="javascript:void(0)" onclick="window.print()"
       class="btn btn-xs btn-success m-b-10"><i
            class="fa fa-print m-r-5"></i> Print</a>

      <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
      <i class="fa fa-download"></i> Generate PDF -->
    <!-- </button> -->
  </div>
</div>
</section>
