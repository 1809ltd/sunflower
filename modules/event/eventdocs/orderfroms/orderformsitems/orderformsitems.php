<?php

namespace app\modules\event\eventdocs\orderfroms\orderformsitems;

/**
 * orderformsitems module definition class
 */
class orderformsitems extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventdocs\orderfroms\orderformsitems\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
