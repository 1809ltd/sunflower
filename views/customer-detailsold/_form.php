<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/*Array*/
use yii\helpers\ArrayHelper;
use app\models\CustomerDetails;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerDetails */
/* @var $form yii\widgets\ActiveForm */
?>

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
  </div>
  <?php $form = ActiveForm::begin([
    'id'=>$model->formName(),
    'enableAjaxValidation'=>true,
    'validationUrl'=>Url::toRoute('/customer-details/validte')
  ]); ?>

  <div class="modal-body customer-details-form">

    <?php
        // necessary for update action.
        if (! $model->isNewRecord) {
          // code...
          $customerNum = $model->customerNumber;?>
          <?= $form->field($model, 'customerNumber')->textInput(['readonly' => true, 'value' => $customerNum])?>
      <?php
        } else {
          // code...
        }
    ?>

    <?= $form->field($model, 'customerCompanyName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerFullname')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'customerKraPin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerDisplayName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerPrimaryPhone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerPrimaryEmail')->textInput(['maxlength' => true]) ?>

    <?php $billwithparent = ['1' => 'Yes', '2' => 'No']; ?>
    <?= $form->field($model, 'customerBillWithParent')->dropDownList($billwithparent, ['prompt'=>'Select Parent']); ?>

    <?= $form->field($model, 'customerParentId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CustomerDetails::find()->all(),'id','customerCompanyName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Customer Parent ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'customercity')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerSuite')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerCounty')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerCountry')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerPostalCode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customerAddress')->textInput(['maxlength' => true]) ?>

    <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
    <?= $form->field($model, 'customerstatus')->dropDownList($status, ['prompt'=>'Select Status']); ?>

  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
  </div>
  <?php ActiveForm::end(); ?>
