<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_credit_memo_lines".
 *
 * @property int $id
 * @property int $creditMemoId
 * @property string $invoiceNumber
 * @property string $creditMemoLineDescription
 * @property double $creditMemoLineAmount
 * @property int $creditMemoLineStatus
 * @property string $creditMemoLineCreatedAt
 * @property string $creditMemoLineUpdatedAt
 * @property string $creidtDeletedAt
 * @property int $userId
 *
 * @property FinanceCreditMemo $creditMemo
 * @property FinanceInvoice $invoiceNumber0
 * @property UserDetails $user
 */
class FinanceCreditMemoLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_credit_memo_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoiceNumber', 'creditMemoLineAmount'], 'required'],
            [['creditMemoId', 'creditMemoLineStatus', 'userId'], 'integer'],
            [['creditMemoLineDescription'], 'string'],
            [['creditMemoLineAmount'], 'number'],
            [['creditMemoId', 'creditMemoLineCreatedAt', 'creditMemoLineUpdatedAt', 'creidtDeletedAt', 'userId', 'creditMemoLineStatus'], 'safe'],
            [['invoiceNumber'], 'string', 'max' => 50],
            [['creditMemoId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceCreditMemo::className(), 'targetAttribute' => ['creditMemoId' => 'id']],
            [['invoiceNumber'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceInvoice::className(), 'targetAttribute' => ['invoiceNumber' => 'invoiceNumber']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creditMemoId' => 'Credit Memo ID',
            'invoiceNumber' => 'Invoice Number',
            'creditMemoLineDescription' => 'Description',
            'creditMemoLineAmount' => 'Amount',
            'creditMemoLineStatus' => 'Status',
            'creditMemoLineCreatedAt' => 'Credit Memo Line Created At',
            'creditMemoLineUpdatedAt' => 'Credit Memo Line Updated At',
            'creidtDeletedAt' => 'Creidt Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreditMemo()
    {
        return $this->hasOne(FinanceCreditMemo::className(), ['id' => 'creditMemoId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceNumber0()
    {
        return $this->hasOne(FinanceInvoice::className(), ['invoiceNumber' => 'invoiceNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
