<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceCreditMemo */

$this->title = 'Create Finance Credit Memo';
$this->params['breadcrumbs'][] = ['label' => 'Finance Credit Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-credit-memo-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceCreditMemoLines'=>$modelsFinanceCreditMemoLines, //to enable Dynamic Form
    ]) ?>

</div>
