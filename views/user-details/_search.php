<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userdetails\models\UserDetailsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-details-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'userStaffId') ?>

    <?= $form->field($model, 'userFName') ?>

    <?= $form->field($model, 'userLName') ?>

    <?= $form->field($model, 'userPhone') ?>

    <?php // echo $form->field($model, 'userEmail') ?>

    <?php // echo $form->field($model, 'username') ?>

    <?php // echo $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'authKey') ?>

    <?php // echo $form->field($model, 'password_reset_token') ?>

    <?php // echo $form->field($model, 'userImage') ?>

    <?php // echo $form->field($model, 'userStatus') ?>

    <?php // echo $form->field($model, 'userLastLogin') ?>

    <?php // echo $form->field($model, 'userCreatedBy') ?>

    <?php // echo $form->field($model, 'userDeleteAt') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
