<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceCategoryType */

$this->title = 'Create Finance Category Type';
$this->params['breadcrumbs'][] = ['label' => 'Finance Category Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-category-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
