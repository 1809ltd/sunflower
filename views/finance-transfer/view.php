<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\transfer\models\FinanceTransfer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Transfers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="finance-transfer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tranferRefernce',
            'tranferFromAccountRef',
            'tranferToAccountRef',
            'tranferAmount',
            'transferCharges',
            'tranferTxnDate',
            'tranferCreatedAt',
            'tranferUpdatedAt',
            'tranferDeletedAt',
            'userId',
            'status',
            'createdBy',
        ],
    ]) ?>

</div>
