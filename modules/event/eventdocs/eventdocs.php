<?php

namespace app\modules\event\eventdocs;

/**
 * eventdocs module definition class
 */
class eventdocs extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\event\eventdocs\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[

          /*Event Order form Sub Module*/
          'orderfroms' => [
                'class' => 'app\modules\event\eventdocs\orderfroms\orderfroms',
            ],
        ];

        // custom initialization code goes here
    }
}
