<?php

namespace app\modules\finance\financereport\companyfinancials\cashflow;

/**
 * cashflow module definition class
 */
class cashflow extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\companyfinancials\cashflow\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
