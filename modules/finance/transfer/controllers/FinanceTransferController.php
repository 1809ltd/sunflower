<?php

namespace app\modules\finance\transfer\controllers;

use Yii;
use app\modules\finance\transfer\models\FinanceTransfer;
use app\modules\finance\transfer\models\FinanceTransferSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/*Relation */

use app\modules\finance\transfer\models\FinanceTransferee;
use app\modules\finance\transfer\models\FinanceTransfereeSearch;

/**
 * FinanceTransferController implements the CRUD actions for FinanceTransfer model.
 */
class FinanceTransferController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...
        $this->tranferUpdatedAt = date('Y-d-m h:i:s');
        /**/
      }else {
        // code...
        $this->tranferUpdatedAt = date('Y-d-m h:i:s');
      }
      return parent::beforeSave();
    }


    /**
     * Lists all FinanceTransfer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new FinanceTransferSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceTransfer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceTransfer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $this->layout = '@app/views/layouts/addformdatatablelayout';
        // $this->layout='addformdatatablelayout';
        $model = new FinanceTransfer();
        $modeltransferee = new FinanceTransferee();

        //checking if its a post
        if ($model->load(Yii::$app->request->post())) {
          // code...

          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;
          $model->createdBy = Yii::$app->user->identity->id;
          $model->tranferCreatedAt = date('Y-d-m h:i:s');

          $model->save();

          // $modeltransferee->transferid=$model->id;
          //
          //
          // echo $modeltransferee->transferid;
          //
          // die();
          //
          $modeltransferee->transferid=$model->id;
          $modeltransferee->transfereeRefernce=$model->tranferRefernce;
          $modeltransferee->transfereeFromAccountRef=$model->tranferFromAccountRef;
          $modeltransferee->transfereeToAccountRef=$model->tranferToAccountRef;
          $modeltransferee->transfereeAmount=$model->tranferAmount;
          $modeltransferee->transferCharges=$model->transferCharges;
          $modeltransferee->transfereeTxnDate=$model->tranferTxnDate;
          $modeltransferee->transfereeCreatedAt=$model->tranferCreatedAt;
          $modeltransferee->userId= $model->userId;
          $modeltransferee->status= $model->status;
          $modeltransferee->createdBy=$model->createdBy;


          $modeltransferee->save(false);

          // return $this->redirect(['index']);

          // echo $model->id;
          // echo $modeltransferee->transfereeFromAccountRef;
          //
          // die();


          //after Saving the transfer Details Details

          // return $this->redirect(['view', 'id' => $model->id]);
          return $this->redirect(['index']);

        } else {
          // code...

          //load the create form
          return $this->renderAjax('create', [
            'model' => $model,
            'modeltransferee' => $modeltransferee,
          ]);
        }
        
    }

    /**
     * Updates an existing FinanceTransfer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
          // $model = $this->findModel($id);
          //
          // if ($model->load(Yii::$app->request->post()) && $model->save()) {
          //     return $this->redirect(['view', 'id' => $model->id]);
          // }
          //
          // return $this->render('update', [
          //     'model' => $model,
          // ]);
          $this->layout = '@app/views/layouts/addformdatatablelayout';
          $model = $this->findModel($id);

          //Getting user Log in details
          $model->userId = Yii::$app->user->identity->id;
          if ($model->load(Yii::$app->request->post()) && $model->save()) {
              // return $this->redirect(['view', 'id' => $model->id]);
              return $this->redirect(['index']);
          }

          return $this->renderAjax('update', [
              'model' => $model,
          ]);
    }

    /**
     * Deletes an existing FinanceTransfer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceTransfer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceTransfer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceTransfer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    /*Dep Drop For Account Transfer*/
    /*Step One*/
    public function actionTransfereeaccount() {
      $out = [];
      if (isset($_POST['depdrop_parents'])) {
          $parents = $_POST['depdrop_parents'];
          if ($parents != null) {
              $id = $parents[0];
              $out = FinanceTransfer::getAccountstranferee($id);
              echo Json::encode($out);
              return;
          }
      }
      echo Json::encode(['output'=>'', 'selected'=>'']);
    }

}
