<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_offset_memo_lines".
 *
 * @property int $id
 * @property int $offsetMemoId
 * @property string $invoiceNumber
 * @property string $offsetMemoLineDescription
 * @property double $offsetMemoLineAmount
 * @property int $offsetMemoLineStatus
 * @property string $offsetMemoLineCreatedAt
 * @property string $offsetMemoLineUpdatedAt
 * @property string $creidtDeletedAt
 * @property int $userId
 *
 * @property FinanceOffsetMemo $offsetMemo
 * @property FinanceInvoice $invoiceNumber0
 * @property UserDetails $user
 */
class FinanceOffsetMemoLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_offset_memo_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoiceNumber', 'offsetMemoLineAmount'], 'required'],
            [['offsetMemoId', 'offsetMemoLineStatus', 'userId'], 'integer'],
            [['offsetMemoLineDescription'], 'string'],
            [['offsetMemoLineAmount'], 'number'],
            [['offsetMemoId','offsetMemoLineStatus', 'userId','offsetMemoLineCreatedAt', 'offsetMemoLineUpdatedAt', 'creidtDeletedAt'], 'safe'],
            [['invoiceNumber'], 'string', 'max' => 50],
            [['offsetMemoId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceOffsetMemo::className(), 'targetAttribute' => ['offsetMemoId' => 'id']],
            [['invoiceNumber'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceInvoice::className(), 'targetAttribute' => ['invoiceNumber' => 'invoiceNumber']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'offsetMemoId' => 'Offset Memo ID',
            'invoiceNumber' => 'Invoice Number',
            'offsetMemoLineDescription' => 'Offset Memo Line Description',
            'offsetMemoLineAmount' => 'Offset Memo Line Amount',
            'offsetMemoLineStatus' => 'Offset Memo Line Status',
            'offsetMemoLineCreatedAt' => 'Offset Memo Line Created At',
            'offsetMemoLineUpdatedAt' => 'Offset Memo Line Updated At',
            'creidtDeletedAt' => 'Creidt Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffsetMemo()
    {
        return $this->hasOne(FinanceOffsetMemo::className(), ['id' => 'offsetMemoId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceNumber0()
    {
        return $this->hasOne(FinanceInvoice::className(), ['invoiceNumber' => 'invoiceNumber']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
