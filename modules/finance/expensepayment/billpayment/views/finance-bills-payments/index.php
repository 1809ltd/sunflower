<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;
// Payment Line Items
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLinesSearch;
use kartik\select2\Select2;
/*Getting the Vendor Details*/
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;

$this->title = Yii::t('app', 'Bill Payments');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php

if (isset($_SESSION['vendorInvoiceId'])) {
  // code...
  Yii::$app->response->redirect(Url::to(['create'], true));
  // return redirect(['create']);
} else {
  // code...
  ?>
  <!-- SELECT2 EXAMPLE -->
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

      <div class="form-group">
        <?php
        $gridColumns = [
          ['class' => 'yii\grid\SerialColumn'],
          [
            'attribute'=>'vendorId',
            'value'=>'vendor.vendorCompanyName',
          ],
          // 'vendor.vendorCompanyName',
          'billPaymentBillRef',
          // 'id',
          'billPaymentPayType',
          [
            'attribute'=>'billPaymentaccount',
            'value'=>'billPaymentaccount0.fullyQualifiedName',
          ],
          'billPaymentTotalAmt',
          'unallocatedAmount',
          //'vendorId',
          //'billPaymentPrivateNote:ntext',
          //'billPaymentCreatedAt',
          //'billPaymentUpdatedAt',
          //'billPaymentDeletedAt',
          //'userId',
          // 'paymentstatus',
          ['class' => 'yii\grid\ActionColumn'],
        ];
        // Renders a export dropdown menu
        echo ExportMenu::widget([
          'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
          'columns' => $gridColumns
        ]);
        // You can choose to render your own GridView separately
        // echo \kartik\grid\GridView::widget([
        //   'dataProvider' => $dataProvider,
        //   'filterModel' => $searchModel,
        //   'columns' => $gridColumns
        // ]);
        ?>
        <p class="pull-right">
          <?= Html::button('Record Payment', ['value'=>Url::to((['/finance/expensepayment/bill-payment/default/search'])),'class' => 'btn btn-sm btn-success','id'=>'modalButton']) ?>

          <?php
        	Modal::begin([
        		'header'=>'<h4>Search Supplier</h4>',
        		'class'=>'modal-dialog',
        		'id'=>'modal',
        		'size'=>'modal-lg',
        		'options' => [
        				'tabindex' => false // important for Select2 to work properly
        		],
        	]);
        	echo "<div class='modal-content' id='modalContent'></div>";
        	Modal::end();
        	?>

            <?php
             // Html::a('Record Invoice Payments', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="box-body finance-bills-payments-index">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]);

        ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions'=> function($model)
            {
              // code...
              if ($model->paymentstatus ==1) {
                // code...
                return['class'=>'success'];
              } elseif ($model->paymentstatus==2) {
                // code...

                return['class'=>'warning'];
              } else {
                // code...
                return['class'=>'danger'];
              }

            },
            'columns' => [
                ['class' => 'kartik\grid\ExpandRowColumn',
                  'value'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    return GridView::ROW_COLLAPSED;
                  },
                  'detail'=>function ($model,$key,$index,$column)
                  {
                    // code...
                    $searchModel = new FinanceBillsPaymentsLinesSearch();
                    $searchModel->billId = $model->id;
                    $searchModel->status = $model->paymentstatus;
                    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
                    return Yii::$app->controller->renderPartial('_paymentlines',[
                      'searchModel' => $searchModel,
                      'dataProvider' => $dataProvider,

                    ]);
                  },
              ],
                [
                  'attribute'=>'vendorId',
                  'value'=>'vendor.vendorCompanyName',
                ],
                // 'vendor.vendorCompanyName',
                'billPaymentBillRef',
                // 'id',
                'billPaymentPayType',
                [
                  'attribute'=>'billPaymentaccount',
                  'value'=>'billPaymentaccount0.fullyQualifiedName',
                ],
                'billPaymentTotalAmt',
                'unallocatedAmount',
                //'vendorId',
                //'billPaymentPrivateNote:ntext',
                //'billPaymentCreatedAt',
                //'billPaymentUpdatedAt',
                //'billPaymentDeletedAt',
                //'userId',
                // 'paymentstatus',
                // 'id',
                // ['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                    'items' => [
                        [
                            'label' => 'View',
                            'url'   => ['view'],
                        ],
                        [
                            'label' => 'Update',
                            'url'   => ['update'],
                            'linkOptions' => [
                                'data-method' => 'post'
                            ],
                            // 'options'=>['class'=>'update-modal-click grid-action'],
                            // 'update'=>function($url,$model,$key){
                            //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                            //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                            //           'class'=>'update-modal-click grid-action',
                            //           'data-toggle'=>'tooltip',
                            //           'data-placement'=>'bottom',
                            //           'title'=>'Update'
                            //       ]);
                            //       return $btn;
                            // },
                            // 'option' 'class'=>'update-modal-click grid-action',
                            // 'data-toggle'=>'tooltip',
                            // 'data-placement'=>'bottom',
                        ],
                        [
                            'label'   => 'Disable',
                            'url'     => ['delete'],
                            'linkOptions' => [
                                'data-method' => 'post'
                            ],
                        ],
                    ]
                ],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
      </div>

    </div>
    <!-- /.box-body -->
    <div class="box-footer">

    </div>

  </div>
      <!-- /.box -->

  <?php
}
?>
