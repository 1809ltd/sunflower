<?php

namespace app\modules\finance\financereport\customerreport;

/**
 * customerreport module definition class
 */
class customerreport extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\financereport\customerreport\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
