<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCreditMemoLinesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-credit-memo-lines-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'creditMemoId') ?>

    <?= $form->field($model, 'invoiceNumber') ?>

    <?= $form->field($model, 'creditMemoLineDescription') ?>

    <?= $form->field($model, 'creditMemoLineAmount') ?>

    <?php // echo $form->field($model, 'creditMemoLineStatus') ?>

    <?php // echo $form->field($model, 'creditMemoLineCreatedAt') ?>

    <?php // echo $form->field($model, 'creditMemoLineUpdatedAt') ?>

    <?php // echo $form->field($model, 'creidtDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
