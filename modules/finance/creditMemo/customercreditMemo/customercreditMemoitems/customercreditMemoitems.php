<?php

namespace app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems;

/**
 * customercreditMemoitems module definition class
 */
class customercreditMemoitems extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\creditMemo\customercreditMemo\customercreditMemoitems\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
