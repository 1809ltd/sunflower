<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_customer_purchase_order_lines".
 *
 * @property int $id
 * @property int $purchaseOrderId
 * @property string $itemRefName
 * @property string $purchaseOrderLinesDescription
 * @property double $purchaseOrderLinespack
 * @property double $purchaseOrderLinesQty
 * @property double $purchaseOrderLinesUnitPrice
 * @property double $purchaseOrderLinesAmount
 * @property string $purchaseOrderLinesTaxCodeRef
 * @property double $purchaseOrderLinesTaxamount
 * @property double $purchaseOrderLinesDiscount
 * @property string $purchaseOrderLinesCreatedAt
 * @property string $purchaseOrderLinesUpdatedAt
 * @property string $purchaseOrderLinesDeletedAt
 * @property int $userId
 *
 * @property FinanceCustomerPurchaseOrder $purchaseOrder
 * @property UserDetails $user
 */
class FinanceCustomerPurchaseOrderLines extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_customer_purchase_order_lines';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['itemRefName', 'purchaseOrderLinesDescription', 'purchaseOrderLinespack', 'purchaseOrderLinesQty', 'purchaseOrderLinesUnitPrice', 'purchaseOrderLinesAmount', 'purchaseOrderLinesTaxCodeRef', 'purchaseOrderLinesTaxamount', 'purchaseOrderLinesDiscount'], 'required'],
            [['purchaseOrderId', 'userId'], 'integer'],
            [['purchaseOrderLinesDescription'], 'string'],
            [['purchaseOrderLinespack', 'purchaseOrderLinesQty', 'purchaseOrderLinesUnitPrice', 'purchaseOrderLinesAmount', 'purchaseOrderLinesTaxamount', 'purchaseOrderLinesDiscount'], 'number'],
            [['purchaseOrderId', 'purchaseOrderLinesCreatedAt', 'purchaseOrderLinesUpdatedAt', 'purchaseOrderLinesDeletedAt', 'userId'], 'safe'],
            [['itemRefName'], 'string', 'max' => 255],
            [['purchaseOrderLinesTaxCodeRef'], 'string', 'max' => 50],
            [['purchaseOrderId'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceCustomerPurchaseOrder::className(), 'targetAttribute' => ['purchaseOrderId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchaseOrderId' => 'Purchase Order',
            'itemRefName' => 'Product or Service',
            'purchaseOrderLinesDescription' => 'Description',
            'purchaseOrderLinespack' => 'Pack',
            'purchaseOrderLinesQty' => 'Qty',
            'purchaseOrderLinesUnitPrice' => 'Unit Price',
            'purchaseOrderLinesAmount' => 'Amount',
            'purchaseOrderLinesTaxCodeRef' => 'Tax Code Ref',
            'purchaseOrderLinesTaxamount' => 'Taxamount',
            'purchaseOrderLinesDiscount' => 'Discount',
            'purchaseOrderLinesCreatedAt' => 'Created At',
            'purchaseOrderLinesUpdatedAt' => 'Updated At',
            'purchaseOrderLinesDeletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrder()
    {
        return $this->hasOne(FinanceCustomerPurchaseOrder::className(), ['id' => 'purchaseOrderId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
