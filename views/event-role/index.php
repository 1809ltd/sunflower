<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;


$this->title = 'Event Roles';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="event-caterogies-index">


                  <?php Pjax::begin(); ?>
                  <?php // echo $this->render('_search', ['model' => $searchModel]);  event-role/create ?>

                  <p>

                    <?php
                    // echo Html::button('Create A Role', ['value' => Url::to((['event-role/popup'])), 'title' => 'Title', 'class' => 'showModalButton btn btn-success']);
                    ?>
                      <?=  Html::a('Create A Role',  ['create'], ['value' => Url::to(['url/to']), 'class' => 'showModalButton btn btn-success']); ?>

                  </p>

                  <?php
                        Modal::begin([
                            'id' => 'modal',
                            'header' => '<h4>Add Role</h4>',
                            'size'=>'modal-lg'
                        ]);

                        echo "";

                        // echo '<div class="modalContent"></div>';

                        Modal::end();
                  ?>

                  <?= GridView::widget([
                      'dataProvider' => $dataProvider,
                      'filterModel' => $searchModel,
                      'columns' => [
                          ['class' => 'yii\grid\SerialColumn'],

                          // 'id',
                          'roleName',
                          'roleDescription:ntext',
                          'status',
                          'userId',
                          //'createdAt',
                          //'updatedAt',
                          //'deletedAt',

                          ['class' => 'yii\grid\ActionColumn'],
                      ],
                  ]); ?>
                  <?php Pjax::end(); ?>
              </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
</div>
<!-- end row -->
<?php
$script = <<< JS
//javascript code
    $('button').click(function(){

    $('#modal').modal('show')
    .find('#modalContent')
    .load($(this).attr('value'));
    })
JS;
$this->registerJs($script);
?>
