<?php

namespace app\modules\finance\expense\leadexpense;

/**
 * leadexpense module definition class
 */
class leadexpense extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\expense\leadexpense\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Lead expense Lines Sub Module*/
          'leadexpenselines' => [
              'class' => 'app\modules\finance\expense\leadexpense\leadexpenselines\leadexpenselines',
          ],
        ];

        // custom initialization code goes here
    }
}
