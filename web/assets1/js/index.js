"use strict";
var v1 = function () {
    "use strict";
    $(function () {
        $(".select_mobile").select2({
            data: select_mobile
        });
    })
};
var Dashboard = function () {
    "use strict";
    return {
        init: function () {
            v1();
        }
    }
}();
$(function () {
    Dashboard.init();
});