<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines */

$this->title = Yii::t('app', 'Add Office Requisition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Add Office Requisition'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-office-requisition-payments-lines-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
