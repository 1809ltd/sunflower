<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AssetSiteLocation;

/**
 * AssetSiteLocationSearch represents the model behind the search form of `app\models\AssetSiteLocation`.
 */
class AssetSiteLocationSearch extends AssetSiteLocation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assetId', 'siteId', 'loactionId', 'departmentId', 'userId', 'status'], 'integer'],
            [['createdAt', 'updateAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetSiteLocation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinwith('asset');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'assetId' => $this->assetId,
            'siteId' => $this->siteId,
            'loactionId' => $this->loactionId,
            'departmentId' => $this->departmentId,
            'createdAt' => $this->createdAt,
            'updateAt' => $this->updateAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'status' => $this->status,
        ]);
        $query->andFilterWhere(['like','asset_registration.assetName',$this->assetId]);

        return $dataProvider;
    }
}
