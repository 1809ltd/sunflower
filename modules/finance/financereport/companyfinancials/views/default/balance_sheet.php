<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\modules\finance\financereport\companyfinancials\models\AllTransactions;
use app\modules\inventory\asset\assetamortization\models\AssetAmortization;

// AgePayables
use app\modules\finance\financereport\companyfinancials\models\AgePayables;


/*Company details*/
use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();
?>
<?php
// $startDate= \Yii::$app->session->get('startDate');
//
// echo "Where it all started".$startDate;
if (isset($_SESSION['mwishoDate'])) {
  // code...
  // $model->eventId = $_SESSION['startDate'];
  $model->mwishoDate= \Yii::$app->session->get('mwishoDate');

  ?>
  <!-- Main content -->
  <!-- Search Date And the rest -->
  <div class="box no-print">
    <div class="box-header with-border">
      <h3 class="box-title">Search</h3>
      <div class="box-tools pull-right">
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-lg-4 control-label">As Of: </label>
            <div class="col-lg-8">
              <?= $form->field($model,'mwishoDate')->widget(\yii\jui\DatePicker::class, [
                      //'language' => 'ru',
                      'options' => ['autocomplete'=>'off','class' => 'form-control'],
                      'inline' => false,
                      'dateFormat' => 'yyyy-MM-dd',
                  ])->label(false) ?>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <div class="col-lg-8 col-lg-offset-4">
              <div class="center-align">
                <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
              </div>
            </div>
          </div>
        </div>
        <?php ActiveForm::end(); ?>
      </div>
  </div>
</div>

  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Balance Sheet</h3>
      <h5 class="box-title">Reporting period: As of  <?php echo date('M j, Y', strtotime($model->mwishoDate));?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>

    <div class="box">
      <div class="box-body">
        <h5 class="box-title">ASSETS</h5>
        <h6 class="box-title">Current Assets</h6>
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th class="text-left">Account</th>
              <th class="text-right">Balance</th>
            </tr>
          </thead>
          <tbody>
            <?php

            $currentAssetTotal=0;

            $connection = Yii::$app->getDb();
            $command1 = $connection->createCommand("SELECT
                                  all_transactions.accountParentId,
                                  all_transactions.accountsclassfication,
                                  all_transactions.accountId,
                                  all_transactions.accountName,
                                  Sum( all_transactions.dr_amount ),
                                  Sum( all_transactions.cr_amount )
                                FROM
                                  all_transactions
                                WHERE
                                  all_transactions.`accountParentId` = 6
                                  AND Date(`all_transactions`.`transactionDate`) <= Date('$model->mwishoDate')
                                GROUP BY
                                  all_transactions.accountId

                                ORDER BY
                                  all_transactions.accountParentId ASC
                                  ");
            $currentAsset = $command1->query();

            foreach ($currentAsset as $currentAsset){
              // code...

              $bal= $currentAsset["Sum( all_transactions.dr_amount )"]-$currentAsset["Sum( all_transactions.cr_amount )"];

              $currentAssetTotal+=$bal;
              $transactionAccountId= $currentAsset["accountId"];
              ?>
              <tr>
                <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $transactionAccountId]) ?>"><?= $currentAsset["accountName"];?></a></td>
                <td  class="text-right"><span class="align-numbers"><?= number_format($bal,2) ;?></span></td>
              </tr>
             <?php
           }

           $connectionreceivables = Yii::$app->getDb();
           $commandreceivables = $connectionreceivables->createCommand(
             "SELECT COALESCE ( Sum( finance_invoice_lines.invoiceLinesAmount ), 0 ) + COALESCE ( Sum( finance_invoice_lines.invoiceLinesTaxamount ), 0 )
             - ( SELECT COALESCE ( sum( finance_payment_lines.amount ), 0 )
             FROM
	finance_payment_lines
WHERE
	( finance_payment_lines.invoiceId = finance_invoice.id )
	AND DATE( finance_payment.date ) <= : start_date
	) - (
SELECT COALESCE
	( sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 )
FROM
	`finance_credit_memo_lines`
WHERE
	( `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` )
	AND DATE( finance_credit_memo_lines.creditMemoLineCreatedAt ) <= : start_date
	) - (
SELECT COALESCE
	( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 )
FROM
	`finance_offset_memo_lines`
WHERE
	( `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` )
	AND DATE( finance_offset_memo_lines.offsetMemoLineCreatedAt ) <= : start_date
	) AS receivables
FROM
	finance_invoice_lines
	INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
WHERE
	DATE( finance_invoice.invoiceTxnDate ) <= : start_date ", [':start_date' => $model->mwishoDate]

);

           $resultreceivables = $commandreceivables->queryAll();
           $recviableTT = $resultreceivables[0]['receivables'];

           $totlalcureentasset=$currentAssetTotal+$recviableTT;

           ?>

           <tr>
             <td  class="text-left"><a href="<?=Url::to(['agedreciveables']) ?>">Accounts Receivables</a></td>
             <td  class="text-right"><span class="align-numbers"><?= number_format($recviableTT,2) ;?></span></td>
           </tr>
           <tr>
             <th  class="text-left">Total Current Assets:</th>
             <td  class="text-right"><?= number_format($totlalcureentasset,2); ?></td>
           </tr>
         </tbody>
       </table>

     <h6 class="box-title">Fixed Assets</h6>
     <table class="table  table-striped table-condensed">
       <thead>
         <tr>
           <th class="text-left">Asset</th>
           <th class="text-right">Value</th>
         </tr>
       </thead>
       <tbody>

         <?php

           $currentFixedAssetTotal=0;
           $connectionfixed = Yii::$app->getDb();
           $connectionfixed = $connectionfixed->createCommand("SELECT
                                                                asset_amortization.amortizationDate,
                                                                Sum( asset_amortization.endBalance ),
                                                                asset_registration.assetDescription,
                                                                asset_registration.assetCategory,
                                                                asset_category.assetCategoryName
                                                              FROM
                                                                asset_amortization
                                                                LEFT JOIN asset_registration ON asset_amortization.assetId = asset_registration.id
                                                                LEFT JOIN asset_category ON asset_registration.assetCategory = asset_category.id
                                                              WHERE
                                                                MONTH (asset_amortization.amortizationDate) = MONTH ('$model->mwishoDate')
                                                                AND YEAR (asset_amortization.amortizationDate) = YEAR ('$model->mwishoDate')
                                                              GROUP BY
                                                                asset_registration.assetCategory
                                                              ORDER BY
                                                                asset_category.assetCategoryName ASC
                                                                ");
           $fixedAsset = $connectionfixed->query();

           foreach ($fixedAsset as $fixedAsset){
             // code...
             $currentFixedAssetTotal+=$fixedAsset["Sum( asset_amortization.endBalance )"];
             // $transactionAccountId= $fixedAsset["accountId"];
             ?>
             <tr>
               <td  class="text-left"><a href="#"><?= $fixedAsset["assetCategoryName"];?></a></td>
               <td  class="text-right"><span class="align-numbers"><?= number_format($fixedAsset["Sum( asset_amortization.endBalance )"],2) ;?></span></td>
             </tr>
            <?php
          }
          ?>

          <tr>
            <th  class="text-left">Total Fixed Assets:</th>
            <td  class="text-right"><?= number_format($currentFixedAssetTotal,2); ?></td>
          </tr>

      </tbody>
    </table>
    <h6 class="box-title">Other Assets</h6>
    <table class="table  table-striped table-condensed">
      <thead>
        <tr>
          <th class="text-left">Asset</th>
          <th class="text-right">Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
         <th  class="text-left">Total Other Assets:</th>
         <th  class="text-right"><?= number_format(0,2); ?></th>
       </tr>
       <tr>
           <th class="text-left">TOTAL ASSETS</th>
           <th class="text-right"><?= number_format($currentFixedAssetTotal+$totlalcureentasset,2); ?></th>
         </tr>
     </tbody>
   </table>
    </br>

    <h5 class="box-title">LIABILITIES</h5>
    <h6 class="box-title">Current Liabilities</h6>
    <table class="table  table-striped table-condensed">
      <thead>
        <tr>
          <th class="text-left">Account</th>
          <th class="text-right">Balance</th>
        </tr>
      </thead>
      <tbody>
        <?php

        $connectionpayables = Yii::$app->getDb();
        $commandpayables = $connectionpayables->createCommand("SELECT COALESCE(Sum( finance_invoice_lines.invoiceLinesAmount ), 0)
                 -
                 (SELECT COALESCE(sum( finance_payment_lines.amount ), 0 ) FROM finance_payment_lines LEFT JOIN finance_payment ON finance_payment_lines.invoicepaymentId = finance_payment.id WHERE ( finance_payment_lines.invoiceId = finance_invoice.id ) and DATE(finance_payment.date ) <= :start_date)
                 -
                 (SELECT COALESCE(sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 ) FROM `finance_credit_memo_lines` WHERE ( `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_credit_memo_lines.creditMemoLineCreatedAt ) <= :start_date)
                 -
                 ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 ) FROM `finance_offset_memo_lines` WHERE ( `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_offset_memo_lines.offsetMemoLineCreatedAt ) <= :start_date)
                 as payables FROM
                 finance_invoice_lines
                 INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
                 WHERE

                 DATE( finance_invoice.invoiceTxnDate ) <= :start_date", [':start_date' => $model->mwishoDate]);

       $resultpayables = $commandpayables->queryAll();

       // print_r($resultpayables);

       $payableTT = $resultpayables[0]['payables'];

       $totlalcureentasset=$currentAssetTotal+$payableTT;

       ?>

       <tr>
         <td  class="text-left"><a href="<?=Url::to(['agedpayables']) ?>">Accounts Payables</a></td>
         <td  class="text-right"><span class="align-numbers"><?= number_format($payableTT,2) ;?></span></td>
       </tr>
       <tr>
         <th  class="text-left">Total Current Liabilities:</th>
         <td  class="text-right"><?= number_format($totlalcureentasset,2); ?></td>
       </tr>
     </tbody>
    </table>

    <h6 class="box-title">Long-Term Liabilities</h6>
    <table class="table  table-striped table-condensed">
    <thead>
     <tr>
       <th class="text-left">Asset</th>
       <th class="text-right">Value</th>
     </tr>
    </thead>
    <tbody>
     <?php
     ?>
     <tr>
      <td  class="text-left"><a href="#"> ...</a></td>
      <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
    </tr>
    <tr>
      <th  class="text-left"> Total Long-Term Liabilities:</th>
      <td  class="text-right"><?= number_format(0,2); ?></td>
    </tr>
    </tbody>
    </table>
    </br>

    <h5 class="box-title">EQUITY</h5>
    <!-- <h6 class="box-title">Current Liabilities</h6> -->
    <table class="table  table-striped table-condensed">
    <thead>
      <tr>
        <th class="text-left">Account</th>
        <th class="text-right">Balance</th>
      </tr>
    </thead>
    <tbody>
      <tr>
       <td  class="text-left"><a href="#">..</a></td>
       <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
     </tr>
     <tr>
        <td  class="text-left"><a href="#">Net Income (Loss)</a></td>
        <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
      </tr>
     <tr>
       <th  class="text-left"> Total Equity:</th>
       <td  class="text-right"><?= number_format($totlalcureentasset,2); ?></td>
     </tr>
    </tbody>
    </table>

    </br>

    </div>
    </div>
    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="javascript:void(0)" onclick="window.print()"
           class="btn btn-xs btn-success m-b-10"><i
                class="fa fa-print m-r-5"></i> Print</a>

          <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
          <i class="fa fa-download"></i> Generate PDF -->
        <!-- </button> -->
      </div>
  </div>
  </section>



  <?php

} else {
  // code...

  // $model->mwishoDate= date('Y-01-01');
  $model->mwishoDate= date('Y-m-d');
  ?>


  <!-- Main content -->
  <!-- Search Date And the rest -->
  <div class="box no-print">
    <div class="box-header with-border">
      <h3 class="box-title">Search</h3>
      <div class="box-tools pull-right">
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-6">
          <div class="form-group">
            <label class="col-lg-4 control-label">As Of: </label>
            <div class="col-lg-8">
              <?= $form->field($model,'mwishoDate')->widget(\yii\jui\DatePicker::class, [
                      //'language' => 'ru',
                      'options' => ['autocomplete'=>'off','class' => 'form-control'],
                      'inline' => false,
                      'dateFormat' => 'yyyy-MM-dd',
                  ])->label(false) ?>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="form-group">
            <div class="col-lg-8 col-lg-offset-4">
              <div class="center-align">
                <?= Html::submitButton('submit',['class' => 'btn btn-sm btn-success']) ?>
              </div>
            </div>
          </div>
        </div>
        <?php ActiveForm::end(); ?>
      </div>
  </div>
</div>

  <section class="invoice">
    <div class="text-center">
      <h6 class="box-title">
        <img style="height:20%;width:20%" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
      </h6>
      <h3 class="box-title">Balance Sheet</h3>
      <h5 class="box-title">Reporting period: As of  <?php echo date('M j, Y', strtotime($model->mwishoDate));?></h5>
      <h6 class="box-title">Created <?php echo date('M j, Y', strtotime(date('Y-m-d')));?></h6>
    </div>

    <div class="box">
      <div class="box-body">
        <h5 class="box-title">ASSETS</h5>
        <h6 class="box-title">Current Assets</h6>
        <table class="table  table-striped table-condensed">
          <thead>
            <tr>
              <th class="text-left">Account</th>
              <th class="text-right">Balance</th>
            </tr>
          </thead>
          <tbody>
            <?php

            $currentAssetTotal=0;

            $connection = Yii::$app->getDb();
            $command1 = $connection->createCommand("SELECT
                                  all_transactions.accountParentId,
                                  all_transactions.accountsclassfication,
                                  all_transactions.accountId,
                                  all_transactions.accountName,
                                  Sum( all_transactions.dr_amount ),
                                  Sum( all_transactions.cr_amount )
                                FROM
                                  all_transactions
                                WHERE
                                  all_transactions.`accountParentId` = 6
                                  AND Date(`all_transactions`.`transactionDate`) <= Date('$model->mwishoDate')
                                GROUP BY
                                  all_transactions.accountId

                                ORDER BY
                                  all_transactions.accountParentId ASC
                                  ");
            $currentAsset = $command1->query();

            foreach ($currentAsset as $currentAsset){
              // code...

              $bal= $currentAsset["Sum( all_transactions.dr_amount )"]-$currentAsset["Sum( all_transactions.cr_amount )"];

              $currentAssetTotal+=$bal;
              $transactionAccountId= $currentAsset["accountId"];
              ?>
              <tr>
                <td  class="text-left"><a href="<?=Url::to(['alltransacation','accountId' => $transactionAccountId]) ?>"><?= $currentAsset["accountName"];?></a></td>
                <td  class="text-right"><span class="align-numbers"><?= number_format($bal,2) ;?></span></td>
              </tr>
             <?php
           }

           $connectionreceivables = Yii::$app->getDb();
           $commandreceivables = $connectionreceivables->createCommand("SELECT COALESCE(Sum( finance_invoice_lines.invoiceLinesAmount ), 0)
                     -
                     (SELECT COALESCE(sum( finance_payment_lines.amount ), 0 ) FROM
   finance_payment_lines WHERE ( finance_payment_lines.invoiceId = finance_invoice.id ) and DATE(finance_payment.date ) <= :start_date)
                     -
                     (SELECT COALESCE(sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 ) FROM `finance_credit_memo_lines` WHERE ( `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_credit_memo_lines.creditMemoLineCreatedAt ) <= :start_date)
                     -
                     ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 ) FROM `finance_offset_memo_lines` WHERE ( `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_offset_memo_lines.offsetMemoLineCreatedAt ) <= :start_date)
                     as receivables FROM
                     finance_invoice_lines
                     INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
                     WHERE

                     DATE( finance_invoice.invoiceTxnDate ) <= :start_date", [':start_date' => $model->mwishoDate]);

           $resultreceivables = $commandreceivables->queryAll();
           $recviableTT = $resultreceivables[0]['receivables'];

           $totlalcureentasset=$currentAssetTotal+$recviableTT;

           ?>

           <tr>
             <td  class="text-left"><a href="<?=Url::to(['agedreciveables']) ?>">Accounts Receivables</a></td>
             <td  class="text-right"><span class="align-numbers"><?= number_format($recviableTT,2) ;?></span></td>
           </tr>
           <tr>
             <th  class="text-left">Total Current Assets:</th>
             <td  class="text-right"><?= number_format($totlalcureentasset,2); ?></td>
           </tr>
         </tbody>
       </table>

     <h6 class="box-title">Fixed Assets</h6>
     <table class="table  table-striped table-condensed">
       <thead>
         <tr>
           <th class="text-left">Asset</th>
           <th class="text-right">Value</th>
         </tr>
       </thead>
       <tbody>

         <?php

           $currentFixedAssetTotal=0;
           $connectionfixed = Yii::$app->getDb();
           $connectionfixed = $connectionfixed->createCommand("SELECT
                                                                asset_amortization.amortizationDate,
                                                                Sum( asset_amortization.endBalance ),
                                                                asset_registration.assetDescription,
                                                                asset_registration.assetCategory,
                                                                asset_category.assetCategoryName
                                                              FROM
                                                                asset_amortization
                                                                LEFT JOIN asset_registration ON asset_amortization.assetId = asset_registration.id
                                                                LEFT JOIN asset_category ON asset_registration.assetCategory = asset_category.id
                                                              WHERE
                                                                MONTH (asset_amortization.amortizationDate) = MONTH ('$model->mwishoDate')
                                                                AND YEAR (asset_amortization.amortizationDate) = YEAR ('$model->mwishoDate')
                                                              GROUP BY
                                                                asset_registration.assetCategory
                                                              ORDER BY
                                                                asset_category.assetCategoryName ASC
                                                                ");
           $fixedAsset = $connectionfixed->query();

           foreach ($fixedAsset as $fixedAsset){
             // code...
             $currentFixedAssetTotal+=$fixedAsset["Sum( asset_amortization.endBalance )"];
             // $transactionAccountId= $fixedAsset["accountId"];
             ?>
             <tr>
               <td  class="text-left"><a href="#"><?= $fixedAsset["assetCategoryName"];?></a></td>
               <td  class="text-right"><span class="align-numbers"><?= number_format($fixedAsset["Sum( asset_amortization.endBalance )"],2) ;?></span></td>
             </tr>
            <?php
          }
          ?>

          <tr>
            <th  class="text-left">Total Fixed Assets:</th>
            <td  class="text-right"><?= number_format($currentFixedAssetTotal,2); ?></td>
          </tr>

      </tbody>
    </table>
    <h6 class="box-title">Other Assets</h6>
    <table class="table  table-striped table-condensed">
      <thead>
        <tr>
          <th class="text-left">Asset</th>
          <th class="text-right">Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
         <th  class="text-left">Total Other Assets:</th>
         <th  class="text-right"><?= number_format(0,2); ?></th>
       </tr>
       <tr>
           <th class="text-left">TOTAL ASSETS</th>
           <th class="text-right"><?= number_format($currentFixedAssetTotal+$totlalcureentasset,2); ?></th>
         </tr>
     </tbody>
   </table>
    </br>

    <h5 class="box-title">LIABILITIES</h5>
    <h6 class="box-title">Current Liabilities</h6>
    <table class="table  table-striped table-condensed">
      <thead>
        <tr>
          <th class="text-left">Account</th>
          <th class="text-right">Balance</th>
        </tr>
      </thead>
      <tbody>
        <?php

        $connectionpayables = Yii::$app->getDb();
        $commandpayables = $connectionpayables->createCommand("SELECT COALESCE(Sum( finance_invoice_lines.invoiceLinesAmount ), 0)
                 -
                 (SELECT COALESCE(sum( finance_payment_lines.amount ), 0 ) FROM finance_payment_lines LEFT JOIN finance_payment ON finance_payment_lines.invoicepaymentId = finance_payment.id WHERE ( finance_payment_lines.invoiceId = finance_invoice.id ) and DATE(finance_payment.date ) <= :start_date)
                 -
                 (SELECT COALESCE(sum( `finance_credit_memo_lines`.`creditMemoLineAmount` ), 0 ) FROM `finance_credit_memo_lines` WHERE ( `finance_credit_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_credit_memo_lines.creditMemoLineCreatedAt ) <= :start_date)
                 -
                 ( SELECT COALESCE ( sum( `finance_offset_memo_lines`.`offsetMemoLineAmount` ), 0 ) FROM `finance_offset_memo_lines` WHERE ( `finance_offset_memo_lines`.`invoiceNumber` = `finance_invoice`.`invoiceNumber` ) and DATE( finance_offset_memo_lines.offsetMemoLineCreatedAt ) <= :start_date)
                 as payables FROM
                 finance_invoice_lines
                 INNER JOIN finance_invoice ON finance_invoice_lines.invoiceId = finance_invoice.id
                 WHERE

                 DATE( finance_invoice.invoiceTxnDate ) <= :start_date", [':start_date' => $model->mwishoDate]);

       $resultpayables = $commandpayables->queryAll();

       // print_r($resultpayables);

       $payableTT = $resultpayables[0]['payables'];

       $totlalcureentasset=$currentAssetTotal+$payableTT;

       ?>

       <tr>
         <td  class="text-left"><a href="<?=Url::to(['agedpayables']) ?>">Accounts Payables</a></td>
         <td  class="text-right"><span class="align-numbers"><?= number_format($payableTT,2) ;?></span></td>
       </tr>
       <tr>
         <th  class="text-left">Total Current Liabilities:</th>
         <td  class="text-right"><?= number_format($totlalcureentasset,2); ?></td>
       </tr>
     </tbody>
    </table>

    <h6 class="box-title">Long-Term Liabilities</h6>
    <table class="table  table-striped table-condensed">
    <thead>
     <tr>
       <th class="text-left">Asset</th>
       <th class="text-right">Value</th>
     </tr>
    </thead>
    <tbody>
     <?php
     ?>
     <tr>
      <td  class="text-left"><a href="#"> ...</a></td>
      <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
    </tr>
    <tr>
      <th  class="text-left"> Total Long-Term Liabilities:</th>
      <td  class="text-right"><?= number_format(0,2); ?></td>
    </tr>
    </tbody>
    </table>
    </br>

    <h5 class="box-title">EQUITY</h5>
    <!-- <h6 class="box-title">Current Liabilities</h6> -->
    <table class="table  table-striped table-condensed">
    <thead>
      <tr>
        <th class="text-left">Account</th>
        <th class="text-right">Balance</th>
      </tr>
    </thead>
    <tbody>
      <tr>
       <td  class="text-left"><a href="#">..</a></td>
       <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
     </tr>
     <tr>
        <td  class="text-left"><a href="#">Net Income (Loss)</a></td>
        <td  class="text-right"><span class="align-numbers"><?= number_format(0,2) ;?></span></td>
      </tr>
     <tr>
       <th  class="text-left"> Total Equity:</th>
       <td  class="text-right"><?= number_format($totlalcureentasset,2); ?></td>
     </tr>
    </tbody>
    </table>

    </br>

    </div>
    </div>
    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <a href="javascript:void(0)" onclick="window.print()"
           class="btn btn-xs btn-success m-b-10"><i
                class="fa fa-print m-r-5"></i> Print</a>

          <!-- <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
          <i class="fa fa-download"></i> Generate PDF -->
        <!-- </button> -->
      </div>
  </div>
  </section>

  <?php

}


 ?>
