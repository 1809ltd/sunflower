<?php

namespace app\modules\finance\income;

/**
 * income module definition class
 */
class income extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\income\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules =[
          /*Fiance Invoice Sub Module*/
          'invoice' => [
              'class' => 'app\modules\finance\income\invoice\invoice',
          ],
          /*Fiance Estimate Sub Module*/
          'estimate' => [
              'class' => 'app\modules\finance\income\estimate\estimate',
          ],
        ];

        // custom initialization code goes here
    }
}
