<?php

namespace app\modules\event\eventdocs\orderfroms\orderformsitems\controllers;

use Yii;
use app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromList;
use app\modules\event\eventdocs\orderfroms\orderformsitems\models\EventOrderfromListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


use yii\helpers\Json;
/**
 * EventOrderfromListController implements the CRUD actions for EventOrderfromList model.
 */
class EventOrderfromListController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->updatedAt =  new \yii\db\Expression('NOW()');

      }else {
        // code...
        $this->updatedAt =  new \yii\db\Expression('NOW()');
      }
      return parent::beforeSave();
    }


    /**
     * Lists all EventOrderfromList models.
     * @return mixed
     */
    public function actionIndex()
    {
      // $this->layout='addformdatatablelayout';

      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $searchModel = new EventOrderfromListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EventOrderfromList model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      // $this->layout='addformdatatablelayout';

      $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventOrderfromList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      // $this->layout='addformdatatablelayout';

      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = new EventOrderfromList();

        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EventOrderfromList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      // $this->layout='addformdatatablelayout';

      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        //Getting user Log in details
        $model->userId = Yii::$app->user->identity->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EventOrderfromList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      // $this->layout='addformdatatablelayout';

      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EventOrderfromList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EventOrderfromList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
      // $this->layout='addformdatatablelayout';

      $this->layout = '@app/views/layouts/addformdatatablelayout';
        if (($model = EventOrderfromList::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionLists($orderId)
    {
      // code...
      // $this->layout='addformdatatablelayout';

      $this->layout = '@app/views/layouts/addformdatatablelayout';

        $orders = EventOrderfromList::find()
                      ->where(['orderId' => $orderId])
                      ->all();

        $countorders = EventOrderfromList::find()
                              ->where(['orderId' => $orderId])
                              ->count();


        // print_r($orders);

        if ($countorders>0) {
          // code...
          echo "<option> Select Order Item</option>";

          foreach ($orders as $order) {
            // code...
            echo "<option value= '".$order->id."'>".$order->details."</option>";

          }

        } else {
          // code...

          echo "<option value='0'> No Order Item</option>";

        }


      // $connection = Yii::$app->db;
      // $query= "SELECT * FROM `finance_order` WHERE `type` Like '$type'";
      // $orders = $connection->createCommand($query)->queryAll();


    }

    // Getting OrderList Items
    public function actionOrderdetails() {
      $out = [];
      if (isset($_POST['depdrop_parents'])) {
          $parents = $_POST['depdrop_parents'];
          if ($parents != null) {
              $orderId = $parents[0];
              $out = EventOrderfromList::getOderformList($orderId);
              echo Json::encode($out);
              return;
          }
      }
      echo Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionJobname()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $orderId = empty($ids[0]) ? null : $ids[0];
            $orderformId = empty($ids[1]) ? null : $ids[1];
            if ($orderId != null) {
               $out = EventOrderfromList::getJob($orderId, $orderformId);
               // $data = EventOrderfromList::getJob($orderId, $orderformId);
                /**
                 * the getProdList function will query the database based on the
                 * cat_id and sub_cat_id and return an array like below:
                 *  [
                 *      'out'=>[
                 *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
                 *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
                 *       ],
                 *       'selected'=>'<prod-id-1>'
                 *  ]
                 */

               echo Json::encode($out);
               // echo Json::encode(['output'=>$data['out'], 'selected'=>$data['selected']]);
            // return;
               return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }

/*Dep Drop For Delivery Form Plus order Form */
    /*Step One*/
    //Getting Event Based on the Client Details

    public function actionEventdetails() {
      $out = [];

      if (isset($_POST['depdrop_parents'])) {
          $parents = $_POST['depdrop_parents'];
          if ($parents != null) {
              $customerId = $parents[0];
              $out = EventOrderfromList::getEventCustomer($customerId);
              echo Json::encode($out);
              return;
          }
      }
      echo Json::encode(['output'=>'', 'selected'=>'']);
    }
    /*Step Two*/
    //Getting all Order Based on the Event Details
    public function actionOrdernumber()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $customerId = empty($ids[0]) ? null : $ids[0];
            $eventId = empty($ids[1]) ? null : $ids[1];
            if ($customerId != null) {
               $out = EventOrderfromList::getOderformNo($eventId);
               // $data = EventOrderfromList::getJob($orderId, $orderformId);

                echo Json::encode($out);
               // echo Json::encode(['output'=>$data['out'], 'selected'=>$data['selected']]);
            // return;
               return;
            }
        }
        echo Json::encode(['output'=>'', 'selected'=>'']);
    }


}
