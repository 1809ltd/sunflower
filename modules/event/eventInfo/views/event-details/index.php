<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;

/*Models Used to help with the movement*/
use app\models\EventDetails;
use app\models\EventDetailsSearch;

/*Pop up menu*/
use yii\helpers\Url;
use  yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EventDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Details';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- begin row -->
<div class="row">
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-pvr panel--style--1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <i class="ion ion-ios-browsers-outline" data-click="panel-expand"></i>
                    <i class="ion ion-ios-reload" data-click="panel-reload"></i>
                    <i class="ion ion-ios-arrow-thin-up" data-click="panel-collapse"></i>
                    <i class="ion ion-ios-close-outline" data-click="panel-remove"></i>
                </div>
                <h4 class="panel-title"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="panel-body">
              <div class="event-details-index">

                <div class="table-responsive">
                  <p>
                      <?= Html::a('Create Event Details', ['create'], ['class' => 'btn btn-success']) ?>
                  </p>
                  <?php
                  /*Modal::begin([
                    'header'=>'<h4>Register Company</h4>',
                    'id'=>'modal',
                    'size'=> 'modal-lg',
                  ]);

                  echo "<div id='modalContent'></div>";

                  Modal::end();*/

                  ?>

                  <?php Pjax::begin(); ?>

                  <?=  GridView::widget([
                          'dataProvider' => $dataProvider,
                          'filterModel' => $searchModel,
                          //'class'=>'dataTables_wrapper form-inline dt-bootstrap no-footer',
                          // 'rowOptions'=> function($model)
                          // {
                          //   // code...
                          //   if ($model->companyStatus==1) {
                          //     // code...
                          //     return['class'=>'success'];
                          //   } else {
                          //     // code...
                          //     return['class'=>'danger'];
                          //   }
                          //
                          // },
                          'columns' => [
                              ['class' => 'kartik\grid\ExpandRowColumn',
                                'value'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  return GridView::ROW_COLLAPSED;
                                },
                                'detail'=>function ($model,$key,$index,$column)
                                {
                                  // code...
                                  $searchModel = new EventDetailsSearch();
                                  $searchModel->id = $model->id;
                                  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                                  return Yii::$app->controller->renderPartial('_vieweventDetails',[
                                    'searchModel' => $searchModel,
                                    'dataProvider' => $dataProvider,

                                  ]);
                                },
                            ],

                            //'id',
                            //'customerId',
                            [
                              'attribute'=>'customerId',
                              'value'=>'customer.customerDisplayName',
                            ],
                            'eventNumber',
                            'eventName',
                            'eventTheme',
                            'eventVistorsNumber',
                            [
                              'attribute'=>'eventCategory',
                              'value'=>'eventCategory0.eventCatName',
                            ],
                            // 'eventCategory',
                            //'eventStartDate',
                            //'eventEndDate',
                            //'eventStartTime',
                            //'eventEndTime',
                            //'eventSetUpDateTime',
                            //'eventSetDownDateTime',
                            'eventLocation',
                            //'eventDescription:ntext',
                            //'eventNotes:ntext',
                            //'eventCreatedAt',
                            //'eventLastUpdatedAt',
                            //'eventDeletedAt',
                            //'userId',

                            ['class' => 'yii\grid\ActionColumn'],
                          ],
                      ]);
                      ?>

                </div>

                <?php Pjax::end(); ?>
             </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <div class="col-md-12">
      <div class="box box-primary">
          <div class="box-body no-padding">
            <!-- THE CALENDAR -->
            <div>
              <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                  'events'=> $events,
              ));
             ?>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /. box -->
      </div>
      <!-- /.col -->
</div>
<!-- end row -->
