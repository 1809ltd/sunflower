<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinancePaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'paymentName') ?>

    <?= $form->field($model, 'paymentDepositToAccount') ?>

    <?= $form->field($model, 'paymentTxnDate') ?>

    <?= $form->field($model, 'paymentType') ?>

    <?php // echo $form->field($model, 'paymentRefence') ?>

    <?php // echo $form->field($model, 'paymentTotalAmt') ?>

    <?php // echo $form->field($model, 'paymentProcessduration') ?>

    <?php // echo $form->field($model, 'paymentLinkedTxn') ?>

    <?php // echo $form->field($model, 'paymentLinkedTxnType') ?>

    <?php // echo $form->field($model, 'paymentStatus') ?>

    <?php // echo $form->field($model, 'paymentCreateAt') ?>

    <?php // echo $form->field($model, 'paymentUpdatedAt') ?>

    <?php // echo $form->field($model, 'paymentDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
