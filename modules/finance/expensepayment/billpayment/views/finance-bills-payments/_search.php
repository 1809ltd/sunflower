<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBillsPaymentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-bills-payments-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'billId') ?>

    <?= $form->field($model, 'billPaymentPayType') ?>

    <?= $form->field($model, 'billPaymentaccount') ?>

    <?= $form->field($model, 'billPaymentTotalAmt') ?>

    <?php // echo $form->field($model, 'billPaymentBillRef') ?>

    <?php // echo $form->field($model, 'billPaymentPrivateNote') ?>

    <?php // echo $form->field($model, 'billPaymentCreatedAt') ?>

    <?php // echo $form->field($model, 'billPaymentUpdatedAt') ?>

    <?php // echo $form->field($model, 'billPaymentDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
