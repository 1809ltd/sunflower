<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemo */

$this->title = 'Create Finance Vendor Offset Memo';
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Offset Memos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-offset-memo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceVendorOffsetMemoLines'=>$modelsFinanceVendorOffsetMemoLines, //to enable Dynamic Form
    ]) ?>

</div>
