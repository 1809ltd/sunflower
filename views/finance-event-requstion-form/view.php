<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use app\models\FinanceInvoiceLines;
use app\models\FinanceInvoiceLinesSearch;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventRequstionForm */

$this->title = $model->requstionNumber;
$this->params['breadcrumbs'][] = ['label' => 'Event Requisition Form', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-12">
         <h2 class="page-header">
           <i class="fa fa-globe"></i> SUNFLOWER EVENTS | Complete Event Solution.
           <small class="pull-right">Date: <?=$model->date;?></small>
         </h2>
       </div>
       <!-- /.col -->
     </div>
     <!-- info row -->
     <?php $eventinfo=$model->geteventDetailss($model->eventId); ?>
     <?php foreach ($eventinfo as $eventinfo): ?>
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">
         <!-- From -->
         <address>
           <strong>SUNFLOWER EVENTS,</strong><br/>
            Lavington 85 Mbambane Rd, off. James Gichuru Rd<br/>
           Nairobi, 00100 Kenya<br/>
           Phone: +254 (0) 722 790632<br/>
           Email: info@...com
         </address>
       </div>
       <!-- /.col -->

       <div class="col-sm-4 invoice-col">
         <!-- To -->
         <address>
             <small>Event</small>
               <strong><?= $eventinfo->eventName;?></strong><br/>
               Location: <?= $eventinfo->eventLocation;?><br/>
               Type: <?= $eventinfo->eventCategory;?><br/>
               Theme: <?= $eventinfo->eventTheme;?><br/>
               Event Date : <?= $eventinfo->eventStartDate;?><br/>
         </address>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Office Req:  #<?=$model->requstionNumber;?></b><br>
         Set Up: <?= $eventinfo->eventSetUpDateTime;?><br/>
         Set Down: <?= $eventinfo->eventSetDownDateTime;?><br/>

       </div>
       <!-- /.col -->
     </div>
   <?php endforeach; ?>


     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
           <tr>
               <th>Details</th>
               <th>Quantity</th>
               <th>Price</th>
               <th>Amount(Ksh)</th>
           </tr>
           </thead>
           <tbody>
             <?php
             $FinanceEventRequstionFormLists=$model->getFinanceEventRequstionFormList($model->id);


             foreach ($FinanceEventRequstionFormLists as $FinanceEventRequstionFormList) {
               // code...


               $FinanceEventRequstionFormList->activityId;
               $FinanceEventRequstionFormList->personelId;
               $FinanceEventRequstionFormList->personnelTel;
               $FinanceEventRequstionFormList->description;
               $FinanceEventRequstionFormList->qty;
               $FinanceEventRequstionFormList->unitprice;
               $FinanceEventRequstionFormList->amont;

               ?>
               <tr>
                   <td>
                     <?php
                       $FinanceEventRequstionFormListsactivityName=$model->getActivity($FinanceEventRequstionFormList->activityId);
                     ?>
                     <?php foreach ($FinanceEventRequstionFormListsactivityName as $FinanceEventRequstionFormListsactivityName){

                       echo $FinanceEventRequstionFormListsactivityName->activityName;
                     }
                     ?><br/>
                       <small><?= $FinanceEventRequstionFormList->description;?>.
                       </small>
                   </td>
                   <td><?= $FinanceEventRequstionFormList->qty;;?></td>
                   <td><?= $FinanceEventRequstionFormList->unitprice;?></td>
                   <td><?= $FinanceEventRequstionFormList->amont;?></td>
               </tr>

               <?php

             }

              ?>

              <tr>
                  <td>
                       <br/>
                      <small>.
                      </small>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>

              </tr>

           </tbody>

         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->


     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Event Requistion Note:</p>


         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           * <?= $model->requstionNote; ?><br/>
         </p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Prepared by:</th>
               <td>Ksh <?= $model->preparedBy; ?></td>
             </tr>
             <tr>
               <th>Requested by:</th>
               <td>Ksh <?= $model->amount; ?></td>
             </tr>
             <tr>
               <th>Approved By:</th>
               <td>Ksh <?= $model->approvedBy; ?></td>
             </tr>
           </table>
         </div>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="lead">Amount Due <?= $model->date; ?></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th>Total:</th>
               <td>Ksh <?= $model->amount; ?></td>
             </tr>
           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
         <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Make Payment
         </button>
         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
