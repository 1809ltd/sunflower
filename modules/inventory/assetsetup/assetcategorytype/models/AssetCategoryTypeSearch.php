<?php

namespace app\modules\inventory\assetsetup\assetcategorytype\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType;

/**
 * AssetCategoryTypeSearch represents the model behind the search form of `app\modules\inventory\assetsetup\assetcategorytype\models\AssetCategoryType`.
 */
class AssetCategoryTypeSearch extends AssetCategoryType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'assetCategoryId', 'assetCategoryTypeStatus', 'userId'], 'integer'],
            [['assetCategoryTypeName', 'assetCategoryTypeDescription', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetCategoryType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'assetCategoryId' => $this->assetCategoryId,
            'assetCategoryTypeStatus' => $this->assetCategoryTypeStatus,
            'userId' => $this->userId,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
        ]);

        $query->andFilterWhere(['like', 'assetCategoryTypeName', $this->assetCategoryTypeName])
            ->andFilterWhere(['like', 'assetCategoryTypeDescription', $this->assetCategoryTypeDescription]);

        return $dataProvider;
    }
}
