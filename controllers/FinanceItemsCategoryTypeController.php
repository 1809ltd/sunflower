<?php

namespace app\controllers;

use Yii;
use app\models\FinanceItemsCategoryType;
use app\models\FinanceItemsCategoryTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceItemsCategoryTypeController implements the CRUD actions for FinanceItemsCategoryType model.
 */
class FinanceItemsCategoryTypeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /*Getting the time updated*/
    public function beforeSave() {
      if ($this->isNewRecord) {
        // code...

        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

      }else {
        // code...
        $this->updatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
      }
      return parent::beforeSave();
    }

    /**
     * Lists all FinanceItemsCategoryType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinanceItemsCategoryTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceItemsCategoryType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceItemsCategoryType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FinanceItemsCategoryType();

        //Getting user Log in details
        $model->userId= "1";

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FinanceItemsCategoryType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FinanceItemsCategoryType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceItemsCategoryType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceItemsCategoryType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceItemsCategoryType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLists($id)
    {
      // code...
      $this->layout='addformdatatablelayout';

      $financeItemCategoryTypeNames = FinanceItemsCategoryType::find()
                    ->where(['financeItemCategoryId'=>$id])
                    ->all();

      $countfinanceItemCategoryTypeNames = FinanceItemsCategoryType::find()
                    ->where(['financeItemCategoryId'=>$id])
                    ->count();
       // print_r($financeItemCategoryTypeNames);
       // die();

      if ($countfinanceItemCategoryTypeNames>0) {
        // code...

        foreach ($financeItemCategoryTypeNames as $financeItemCategoryTypeName) {
          // code...
          echo "<option value= '".$financeItemCategoryTypeName->id."'>".$financeItemCategoryTypeName->financeItemCategoryTypeName."</option>";

        }

      } else {
        // code...

        echo "<option> No Details </option>";

      }



    }
}
