<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceTax */
/* @var $form yii\widgets\ActiveForm */

/*Getting the Chart of Accounts details  */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;
/*Getting the Select2 to work */
use kartik\select2\Select2;


?>



<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?= Html::encode($this->title) ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>

<div class="modal-body company-site-location-form">

  <?= $form->field($model, 'taxCode')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

  <?= $form->field($model, 'taxRate')->textInput(['maxlength' => true,'autocomplete'=>"off"]) ?>

  <?= $form->field($model, 'account')->widget(Select2::classname(), [
      'data' => ArrayHelper::map(FinanceAccounts::find()->all(),'accountName','fullyQualifiedName'),
      'language' => 'en',
      'options' => ['placeholder' => 'Select a Account ...'],
      'pluginOptions' => [
          'allowClear' => true
      ],
  ]);
  ?>

  <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>

  <?= $form->field($model, 'taxType')->dropDownList($status, ['prompt'=>'Select Status']);?>

  <?php $taxStatus = ['1' => 'Active', '2' => 'Suspended']; ?>

  <?= $form->field($model, 'taxStatus')->dropDownList($taxStatus, ['prompt'=>'Select Status']);?>


</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
  <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

</div>
<?php ActiveForm::end(); ?>
