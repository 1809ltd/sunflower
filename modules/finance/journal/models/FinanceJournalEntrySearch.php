<?php

namespace app\modules\finance\journal\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\journal\models\FinanceJournalEntry;

/**
 * FinanceJournalEntrySearch represents the model behind the search form of `app\modules\finance\journal\models\FinanceJournalEntry`.
 */
class FinanceJournalEntrySearch extends FinanceJournalEntry
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'preparedby', 'approvedby', 'status'], 'integer'],
            [['journalEntryNo', 'journalEntrydate', 'createdate', 'deletedAt', 'updatedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceJournalEntry::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'journalEntrydate' => $this->journalEntrydate,
            'preparedby' => $this->preparedby,
            'approvedby' => $this->approvedby,
            'createdate' => $this->createdate,
            'deletedAt' => $this->deletedAt,
            'updatedAt' => $this->updatedAt,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'journalEntryNo', $this->journalEntryNo]);

        return $dataProvider;
    }
}
