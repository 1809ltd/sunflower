<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*Adding an Expanding Row */

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use dosamigos\datepicker\DatePicker;

use app\modules\event\eventdocs\orderfroms\models\EventOrderfrom;
/*Pop up menu*/
use yii\helpers\Url;
// use  yii\bootstrap\Modal;


use kartik\select2\Select2;

/*Dependant Drop*/
use kartik\depdrop\DepDrop;
/*Getting the Customer Details*/
use app\modules\customer\customerdetails\models\CustomerDetails;

$this->title = 'Delivery Form Results';
$this->params['breadcrumbs'][] = ['label' => 'Asset Deliveryforms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


if(isset($_SESSION['eventId']))
{
  $event_id = $_SESSION['eventId'];
  $modelsAssetCheckOut = new AssetDeliveryform();
  $output= $modelsAssetCheckOut->getallassetcheckoutsonhold($eventId);

  $searchModel = new AssetDeliveryformSearch();
  $searchModel->eventId = $model->eventId;
  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
}

?>

<div class="col-md-4">
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Event Details</h3>

        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-striped table-condensed">
          <thead>
            <tr>
              <th style="width: 40%;">Title</th>
              <th style="width: 60%;">Detail</th>
            </tr>
          </thead>
            <tbody>

                             <?php $eventinfo=EventOrderfrom::geteventDetailss($model->eventId); ?>
                             <?php foreach ($eventinfo as $eventinfo): ?>

                             <?php $customerinfo=EventOrderfrom::getCustomerDetails($eventinfo->customerId);

                                 // print_r($customerinfo);

                                 foreach ($customerinfo as $customerinfo) {
                                   // code...

                                 ?>
                                 <tr><td><span>Event Name :</span></td><td> <?= $eventinfo->eventName;?> </td></tr>
                                 <tr><td><span>Event Number :</span></td><td>1</td></tr>
                                 <tr><td><span>Customer :</span></td><td><?= $customerinfo->customerDisplayName;?></td></tr>
                                 <tr><td><span>Event date :</span></td><td><?= $eventinfo->eventStartDate;?></td></tr>

                                 <?php

                                 }

                                 ?>



                              <?php endforeach; ?>

              <?php


              ?>


              <tr><td><span>Contract Status :</span></td><td><span class="label label-success">Active</span></td></tr>
              >
              <!-- <tr><td><span>Rental Unit :</span></td><td>C6</td></tr> -->

              <tr><td><span>Set up date :</span></td>
                <td>
                                </td>
              </tr>
              <tr><td colspan="2"><div class="col-md-2">
                <div class="form-actions">
                  <a href="" class="btn btn-warning btn-sm pull-left text-center" >Close Search</a>
                </div>
              </div></td></tr>
            </tbody>
        </table>
      </div>
  </div>
</div>

<div class="col-md-8">

  <?php

  // $this->render('@app/inventory/assetmovement/assetcheckout/asset-check-out/index', [
  //       'output' => $output,
      // 'model'=>$model,
      // // 'output'=>$output,
  // ]) ?>
  <?=
  $this->render('_holdassestcheckout', [
      'model' => $model,
      // 'searchModel' => $searchModel,
      'output' => $output,
      // 'model'=>$model,
      // 'output'=>$output,
  ]) ?>

</div>

<div class="row">
  <div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Deilvry Note</h3>

          <div class="box-tools pull-right">

          </div>
        </div>
        <div class="box-body">


          <?= GridView::widget([
              'dataProvider' => $dataProvider,
              // 'filterModel' => $searchModel,
              'columns' => [
                  ['class' => 'yii\grid\SerialColumn'],

                  // 'id',
                  'deliveryNumber',
                  'event.eventName',
                  // 'eventId',
                  // 'orderId',
                  'duedate',
                  'date',
                  // 'deliveryStatus',
                  'delivery',
                  // 'createdAt',
                  // 'updatedAt',
                  // 'deletedAt',
                  // 'userId',
                  'approvedBy',


                ['class' => 'yii\grid\ActionColumn'],
              ],
          ]); ?>


        </div>
      </div>
    </div>
  </div>
