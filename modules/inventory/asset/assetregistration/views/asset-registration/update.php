<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AssetRegistration */

$this->title = 'Update Asset Registration: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asset Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="asset-registration-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
