<?php

namespace app\modules\company\companysite\controllers;

use Yii;
use app\modules\company\companysite\models\CompanySiteDetails;
use app\modules\company\companysite\models\CompanySiteDetailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompanySiteDetailsController implements the CRUD actions for CompanySiteDetails model.
 */
class CompanySiteDetailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


        /*Getting the time updated*/
        public function beforeSave() {
          if ($this->isNewRecord) {
            // code...
            //$this->companyTimestamp = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
            $this->siteUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));

          }else {
            // code...
            $this->siteUpdatedAt = Yii::$app->formatter->asDatetime(date('Y-d-m h:i:s'));
          }
          return parent::beforeSave();
        }


        /**
         * Lists all CompanySiteDetails models.
         * @return mixed
         */
        public function actionIndex()
        {
            // $this->layout='addformdatatablelayout';
            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $searchModel = new CompanySiteDetailsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        /**
         * Displays a single CompanySiteDetails model.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionView($id)
        {
            // $this->layout='addformdatatablelayout';
            $this->layout = '@app/views/layouts/addformdatatablelayout';
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        /**
         * Creates a new CompanySiteDetails model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */
        public function actionCreate()
        {
            // $this->layout='addformdatatablelayout';
            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = new CompanySiteDetails();

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        /**
         * Updates an existing CompanySiteDetails model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionUpdate($id)
        {
            // $this->layout='addformdatatablelayout';
            $this->layout = '@app/views/layouts/addformdatatablelayout';
            $model = $this->findModel($id);

            //Getting user Log in details
            $model->userId = Yii::$app->user->identity->id;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                // return $this->redirect(['view', 'id' => $model->id]);
                return $this->redirect(['index']);
            }

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        /**
         * Deletes an existing CompanySiteDetails model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         * @throws NotFoundHttpException if the model cannot be found
         */
        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        /**
         * Finds the CompanySiteDetails model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return CompanySiteDetails the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
        {
            if (($model = CompanySiteDetails::findOne($id)) !== null) {
                return $model;
            }

            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
