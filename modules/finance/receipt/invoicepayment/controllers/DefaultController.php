<?php

namespace app\modules\finance\receipt\invoicepayment\controllers;

use Yii;

use yii\web\Controller;

/**
 * Default controller for the `invoicepayment` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionSearch()
    {
      $model = new \yii\base\DynamicModel([
        'nameCustomer'
      ]);
      $model->addRule(['nameCustomer'], 'required')
          ->validate();

      if($model->load(Yii::$app->request->post())){
        // do somenthing with model
        \Yii::$app->session->set('customerInvoiceId',$model->nameCustomer);
        // return $this->redirect(['view']);
        return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/create']);
      }
      return $this->renderAjax('searchCustomer', ['model'=>$model]);
    }
    // Close session
    public function actionClosesearch(){
      unset($_SESSION['customerInvoiceId']);
      return $this->redirect(['/finance/receipt/invoicepayment/finance-payment/']);
    }
}
