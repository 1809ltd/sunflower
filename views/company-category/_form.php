<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\CompanyDetails;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\CompanyCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="company-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'companyId')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(CompanyDetails::find()->all(),'companyId','companyName'),
        'language' => 'en',
        'options' => ['placeholder' => 'Select a Company ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'categoryName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categoryCode')->textInput(['maxlength' => true]) ?>

    <!--= $form->field($model, 'categoryTimestamp')->textInput() ?>

    $form->field($model, 'categoryUpdatedAt')->textInput() ?>

    $form->field($model, 'categoryDeleteAt')->textInput() ?>

  -->

  <?php $status = ['1' => 'Active', '2' => 'Suspended']; ?>
    <?= $form->field($model, 'categoryStatus')->dropDownList($status, ['prompt'=>'Select Status']);?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
