<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceVendorCreditMemoLines */

$this->title = 'Create Finance Vendor Credit Memo Lines';
$this->params['breadcrumbs'][] = ['label' => 'Finance Vendor Credit Memo Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-vendor-credit-memo-lines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
