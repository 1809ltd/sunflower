<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines */

$this->title = Yii::t('app', 'Add Bill');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Finance Bills Payments Lines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-bills-payments-lines-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
