<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventRequstionForm */

$this->title = 'Add Office Requstion Form';
$this->params['breadcrumbs'][] = ['label' => 'Add Requstion Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-requstion-form-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelsFinanceOfficeRequstionFormList'=>$modelsFinanceOfficeRequstionFormList, //to enable Dynamic Form
    ]) ?>

</div>
