<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asset_dispose".
 *
 * @property int $id
 * @property int $assetId
 * @property string $dateDisposed
 * @property string $disposeTo
 * @property string $disposal Reason
 * @property string $disposeTimeStamp
 * @property string $disposeUpdatedAt
 * @property string $disposeDeletedAt
 * @property int $userId
 *
 * @property AssetRegistration $asset
 * @property UserDetails $user
 */
class AssetDispose extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_dispose';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['assetId', 'dateDisposed', 'disposeTo', 'disposal Reason', 'userId'], 'required'],
            [['assetId', 'userId'], 'integer'],
            [['dateDisposed', 'disposeTimeStamp', 'disposeUpdatedAt', 'disposeDeletedAt'], 'safe'],
            [['disposal Reason'], 'string'],
            [['disposeTo'], 'string', 'max' => 50],
            [['assetId'], 'exist', 'skipOnError' => true, 'targetClass' => AssetRegistration::className(), 'targetAttribute' => ['assetId' => 'id']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Dispose',
            'assetId' => 'Asset Name',
            'dateDisposed' => 'Date Disposed',
            'disposeTo' => 'Dispose To',
            'disposal Reason' => 'Disposal  Reason',
            'disposeTimeStamp' => 'Dispose Time Stamp',
            'disposeUpdatedAt' => 'Dispose Updated At',
            'disposeDeletedAt' => 'Dispose Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsset()
    {
        return $this->hasOne(AssetRegistration::className(), ['id' => 'assetId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
