<?php

namespace app\modules\inventory\assetmovement\delivery\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform;

/**
 * AssetDeliveryformSearch represents the model behind the search form of `app\modules\inventory\assetmovement\delivery\models\AssetDeliveryform`.
 */
class AssetDeliveryformSearch extends AssetDeliveryform
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'eventId', 'orderId', 'deliveryStatus', 'userId', 'createdBy', 'approvedBy'], 'integer'],
            [['deliveryNumber', 'duedate', 'date', 'delivery', 'createdAt', 'updatedAt', 'deletedAt'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AssetDeliveryform::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'eventId' => $this->eventId,
            'orderId' => $this->orderId,
            'duedate' => $this->duedate,
            'date' => $this->date,
            'deliveryStatus' => $this->deliveryStatus,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
            'deletedAt' => $this->deletedAt,
            'userId' => $this->userId,
            'createdBy' => $this->createdBy,
            'approvedBy' => $this->approvedBy,
        ]);

        $query->andFilterWhere(['like', 'deliveryNumber', $this->deliveryNumber])
            ->andFilterWhere(['like', 'delivery', $this->delivery]);

        return $dataProvider;
    }
}
