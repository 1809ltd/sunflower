<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonnelDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event List';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>

        </p>
      </div>
    </div>
  </div>

  <div class="box-body personnel-details-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // // code...
          // if ($model->status==1) {
          //   // code...
          //   return['class'=>'success'];
          // } else {
          //   // code...
          //   return['class'=>'danger'];
          // }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'customerId',
            'customer.customerDisplayName',
            'eventNumber',
            'eventName',
            'eventTheme',
            'eventVistorsNumber',
            // 'eventCategory',
            //'eventStartDate',
            //'eventEndDate',
            //'eventStartTime',
            //'eventEndTime',
            //'eventSetUpDateTime',
            //'eventSetDownDateTime',
            // 'eventLocation',
            //'eventDescription:ntext',
            //'eventNotes:ntext',
            //'eventCreatedAt',
            //'eventLastUpdatedAt',
            //'eventDeletedAt',
            //'userId',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
