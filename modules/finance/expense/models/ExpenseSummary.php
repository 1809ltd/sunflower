<?php

namespace app\modules\finance\expense\models;
// Bills
use app\modules\finance\expense\bills\models\FinanceBills;
use app\modules\finance\expense\bills\billslines\models\FinanceBillsLines;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\offset\outhouse\outoffsetbiils\models\FinanceVendorOffsetMemoLines;
use app\modules\finance\creditMemo\vendorcreditMemo\vendorcreditMemoitems\models\FinanceVendorCreditMemoLines;
// Event
use app\modules\finance\expense\eventexpense\models\FinanceEventRequstionForm;
use app\modules\finance\expense\eventexpense\eventexpenselines\models\FinanceEventRequstionFormList;
use app\modules\finance\expensepayment\eventexpensepayment\models\FinanceRequisitionPaymentsLines;
// Office
use app\modules\finance\expense\officeexpense\models\FinanceOfficeRequstionForm;
use app\modules\finance\expense\officeexpense\officeexpenselines\models\FinanceOfficeRequstionFormList;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;
// Lead
use app\modules\finance\expense\leadexpense\models\FinanceLeadRequstionForm;
use app\modules\finance\expense\leadexpense\leadexpenselines\models\FinanceLeadRequstionFormList;
use app\modules\finance\expensepayment\leadexpensepayment\models\FinanceLeadRequisitionPayments;

use Yii;

/**
 * This is the model class for table "expense_summary".
 *
 * @property int $transactionId
 * @property string $referenceCode
 * @property string $projectId
 * @property string $recipientId
 * @property string $recipientName
 * @property string $tranctionDescription
 * @property resource $subAmount
 * @property resource $taxAmount
 * @property string $transactionDate
 * @property int $status
 * @property string $transactionCategory
 * @property string $transactionClassification
 * @property string $referenceTable
 */
class ExpenseSummary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'expense_summary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transactionId', 'status'], 'integer'],
            [['transactionDate'], 'safe'],
            [['referenceCode'], 'string', 'max' => 100],
            [['projectId', 'recipientId'], 'string', 'max' => 11],
            [['recipientName'], 'string', 'max' => 255],
            [['tranctionDescription'], 'string', 'max' => 128],
            [['subAmount', 'taxAmount'], 'string', 'max' => 24],
            [['transactionCategory'], 'string', 'max' => 7],
            [['transactionClassification'], 'string', 'max' => 14],
            [['referenceTable'], 'string', 'max' => 29],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'transactionId' => 'Transaction ID',
            'referenceCode' => 'Reference Code',
            'projectId' => 'Project ID',
            'recipientId' => 'Recipient ID',
            'recipientName' => 'Recipient Name',
            'tranctionDescription' => 'Tranction Description',
            'subAmount' => 'Sub Amount',
            'taxAmount' => 'Tax Amount',
            'transactionDate' => 'Transaction Date',
            'status' => 'Status',
            'transactionCategory' => 'Transaction Category',
            'transactionClassification' => 'Transaction Classification',
            'referenceTable' => 'Reference Table',
        ];
    }

    // Get all the invoice amount
    public function getexpenseItems($id,$status,$referenceTable)
    {
      if ($referenceTable=="finance_office_requstion_form") {
        // code...
        return FinanceOfficeRequstionFormList::find()
                ->where('reqId = :reqId', [':reqId' => $id])
                ->Andwhere('status = :status', [':status' => $status])
                ->all();

      } elseif ($referenceTable=="finance_lead_requstion_form") {
        // code...
        return FinanceLeadRequstionFormList::find()
                ->where('reqId = :reqId', [':reqId' => $id])
                ->Andwhere('status = :status', [':status' => $status])
                ->all();
      } elseif ($referenceTable=="finance_event_requstion_form") {
        // code...
        return FinanceEventRequstionFormList::find()
                ->where('reqId = :reqId', [':reqId' => $id])
                ->Andwhere('status = :status', [':status' => $status])
                ->all();
      }elseif ($referenceTable=="finance_bills") {
        // code...
        return FinanceBillsLines::find()
                ->where('biilsId = :biilsId', [':biilsId' => $id])
                ->Andwhere('billsLinesBillableStatus = :billsLinesBillableStatus', [':billsLinesBillableStatus' => $status])
                ->all();
      }else {
        // code...
      }


    }
    // Getting All Payments
    public function allPayments($id,$referenceTable){
      // Get the actual Payments based on the Expense Type
      if ($referenceTable=="finance_office_requstion_form") {
        // code...
         $totalpaymentmade= FinanceOfficeRequisitionPaymentsLines::find()
                              ->where(['officeReqId' => $id])
                              ->Andwhere('status >0')
                              ->sum('amount');


      } elseif ($referenceTable=="finance_lead_requstion_form") {
        // code...
         $totalpaymentmade= FinanceLeadRequisitionPayments::find()
                              ->where(['id' => $id])
                              ->Andwhere('status >0')
                              ->sum('amount');

      } elseif ($referenceTable=="finance_event_requstion_form") {
        // code...
         $totalpaymentmade= FinanceRequisitionPaymentsLines::find()
                              ->where(['eventReqId' => $id])
                              ->Andwhere('status >0')
                              ->sum('amount');

        // $totalpaymentmade=$inoicepayment+$offsetPayments+$creditPayments;
      }elseif ($referenceTable=="finance_bills") {
        // code...
         $billpayment= FinanceBillsPaymentsLines::find()
                              ->where(['billId' => $id])
                              ->Andwhere('status >0')
                              ->sum('amount');

         // Get all the Offest cancel payments
         $offsetPayments=FinanceVendorOffsetMemoLines::find()
                              ->where(['billRef' => $id])
                              ->Andwhere('vendorOffsetMemoLineStatus>0')
                              ->sum('vendorOffsetMemoLineAmount');

         // Get all the Credit Payments
         $creditPayments=FinanceVendorCreditMemoLines::find()
                              ->where(['billRef' => $id])
                              ->Andwhere('vendorCreditMemoLineStatus >0')
                              ->sum('vendorCreditMemoLineAmount');

        // // This is the total payments of all the amount
        $totalpaymentmade=$billpayment+$offsetPayments+$creditPayments;
        
      }else {
        // code...
        $totalpaymentmade=0;
      }
      return $totalpaymentmade;
    }
}
