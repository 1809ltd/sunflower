<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
// use yii\helpers\Url;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;

/*Estimate Items*/
use app\modules\finance\income\invoice\invoiceitems\models\FinanceEstimateLines;
use app\modules\finance\income\invoice\invoiceitems\models\FinanceEstimateLinesSearch;

/*Company details*/

use app\modules\company\companydetails\models\CompanyDetails;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceInvoice */

$this->title = $model->invoiceNumber;
$this->params['breadcrumbs'][] = ['label' => 'Finance Invoice', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$companyId=1;
$companydetails = CompanyDetails::find()
        ->where('id = :id', [':id' => $companyId])
        ->one();
// print_r($companydetails);
// die();

/*All Payment for this invoice, i.e offset,creditnote,actualpayment*/
$payments=$model->allPayments($model->invoiceNumber);

?>
<!-- this row will not appear when printing -->
<div class="row no-print">
  <p>
      <?= Html::a('<span class="btn btn-sm btn-default"><b class="fa fa-pencil"></b></span>', ['update', 'id' => $model['id']], ['title' => 'Update']);?>
  </p>
  <!-- Pop up Form -->
  <?php
  Modal::begin([
    // 'header'=>'<h3>Add Customer</h3>',
    'class'=>'modal-dialog',
    'id'=>'modal',
    'size'=>'modal-lg',
  ]);

  echo "<div class='modal-content' id='modalContent'></div>";

  Modal::end();
   ?>
</div>


<!-- Main content -->
   <section class="invoice">
     <!-- title row -->
     <div class="row">
       <div class="col-xs-8">
         <!-- <h3 class="page-header"> -->
         <h5>
           <img class="img-responsive pad" src="<?=Url::to('@web/'.$companydetails->companyLogo); ?>" alt="Photo">
         </h5>
       </div>
       <div class="col-xs-4">
         <!-- <h3 class="page-header"> -->
           <address>
             <strong><?= $companydetails->companyName?>,</strong><br/>
              <?= $companydetails->companyAddress?><br/>
            <?= $companydetails->companyPostal?><br/>
             Phone: +254 (0) 722 790632<br/>
             Email: info@sunflowertents.com
           </address>
         <!-- </h3> -->
       </div>
       <!-- /.col -->
     </div>
     <!-- <div class="row">

       <div class="col-xs-12">
         <h4 class="page-header">

         </h4>
       </div>

     </div> -->
     <div class="row">
       <!-- <h5 class="page-header"></h5> -->
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>
             <th class="page-header" style="text-align: center;width: 100%;">Invoice</th>
           </thead>
           <tbody>
           </tbody>
         </table>
       </div>
     </div>


     <!-- info row -->
     <div class="row invoice-info">
       <div class="col-sm-4 invoice-col">

         <address>
           <strong>Client:- </strong> <?= $model->customer['customerCompanyName']?><br/>
           <strong>Kra Pin:- </strong><?= $model->customer['customerKraPin']?><br/>
           <strong> No of guests:-</strong> <?= $model->invoiceProject['eventVistorsNumber']?> Pax<br/>
             <strong>Date of Function:- </strong> <?= $model->invoiceProject['eventStartDate']?><br/>
             <strong>Date of Set Up:- </strong> <?= $model->invoiceProject['eventSetUpDateTime']?><br/>
             <strong>Colour Scheme:- </strong> <?= $model->invoiceProject['eventVistorsNumber']?><br/>
             <strong>Event:- </strong> <?= $model->invoiceProject['eventName']?> <br/>
             <strong>Location:- </strong> <?= $model->invoiceProject['eventLocation']?><br/>
         </address>

         Attn: <?= $model->customer['customerFullname']?>
       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">

       </div>
       <!-- /.col -->
       <div class="col-sm-4 invoice-col">
         <b>Invoice No: #<?=$model->invoiceNumber;?></b><br>
         <b>Date:- </b> <?= Yii::$app->formatter->asDate($model->invoiceTxnDate, 'long');?><br>
         <b>Due Date:- </b> <?= Yii::$app->formatter->asDate($model->invoiceDeadlineDate, 'long');?><br>
         <b>Account:</b> <?= $model->customer['customerNumber']?>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <!-- Table row -->
     <div class="row">
       <div class="col-xs-12 table-responsive">
         <table class="table table-striped">
           <thead>

             <th>Qty</th>
             <th>Service & Description</th>
             <th>Days</th>
             <th>Price</th>
             <th>Discount(%)</th>
             <th>Tax Amount</th>
             <th>Sub Amount</th>
           </thead>
           <tbody>
             <?php
             $invoicesItems=$model->getinvoiceItems($model->id);

             // print_r($invoicesItems);
             //
             // die();

             foreach ($invoicesItems as $invoicesItem) {
               // code...
               // $invoicesItem->invoiceId;
               // $invoicesItem->itemRefId;
               // $invoicesItem->itemRefName;
               // $invoicesItem->invoiceLinesDescription;
               // $invoicesItem->invoiceLinespack;
               // $invoicesItem->invoiceLinesQty;
               // $invoicesItem->invoiceLinesUnitPrice;
               // $invoicesItem->invoiceLinesAmount;
               // $invoicesItem->invoiceLinesTaxCodeRef;
               // $invoicesItem->invoiceLinesTaxamount;
               // $invoicesItem->invoiceLinesDiscount;
               // $invoicesItem->invoiceLinesCreatedAt;
               // $invoicesItem->invoiceLinesUpdatedAt;
               // $invoicesItem->invoiceLinesDeletedAt;
               // $invoicesItem->invoiceLinesStatus;
               ?>
               <tr>
                   <td><?=$invoicesItem->invoiceLinesQty;?></td>
                   <td><?= $invoicesItem->itemRefName;?>:<?= $invoicesItem->invoiceLinesDescription;?></td>
                   <td><?=$invoicesItem->invoiceLinespack;?></td>
                   <td>Ksh <?= number_format($invoicesItem->invoiceLinesUnitPrice,2) ;?></td>
                   <td><?= $invoicesItem->invoiceLinesDiscount;?></td>
                   <td>Ksh <?= number_format($invoicesItem->invoiceLinesTaxamount,2) ;?></td>
                   <td>Ksh <?= number_format($invoicesItem->invoiceLinesAmount,2) ;?></td>
               </tr>

               <?php

             }

              ?>

           </tbody>
         </table>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           <?= $model->invoiceNote; ?>
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
          <?= $model->invoiceFooter; ?>
         </p>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->

     <div class="row">
       <!-- accepted payments column -->
       <div class="col-xs-6">
         <p class="lead">Payment Methods:</p>

         <img src="<?=Url::to('@web/adminlte/dist/img/credit/visa.png');?>" alt="Visa">
         <img src="<?=Url::to('@web/adminlte/dist/img/credit/mastercard.png');?>" alt="Mastercard">
         <img src="<?=Url::to('@web/adminlte/dist/img/credit/american-express.png');?>" alt="American Express">
         <img src="<?=Url::to('@web/adminlte/dist/img/credit/paypal2.png');?>" alt="Paypal">
         <table class="table table-borderless table-sm">
           <tbody>
             <tr>
               <td>Bank name:</td>
               <td class="text-right">NIC Bank, Kenya</td>
             </tr>
             <tr>
               <td>Acc name:</td>
               <td class="text-right">Sunflower Limited</td>
             </tr>
             <tr>
               <td>Account Number:</td>
               <td class="text-right">12313231231321</td>
             </tr>
             <tr>
               <td>SWIFT code:</td>
               <td class="text-right">...</td>
             </tr>
             <tr>
               <td>KRA Company Name:</td>
               <td class="text-right">...</td>
             </tr>

             <tr>
               <td>KRA PIN:</td>
               <td class="text-right">...</td>
             </tr>
           </tbody>
         </table>

         <div class="col-md-8">
         </div>

         <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           * Invoice is confirmed upon receipt of full  payment in cash or 60% with LSO and balance on delivery<br/>
           * Invoice prices are valid for 30 days from original Invoiced<br/>
           * Sunflower Tents requires that client provide 24- hour security of equipment during setup & set down<br/>
           * All pricing information, discounts and equipment packaging contained in this document is confidential<br/>
           * Cancellation charge; 50% charge with less than 7 days notice<br/>
           * Late payment on the final invoice may result in the forfeiture of discounts included in this quote<br/>
           * If you have any questions concerning this invoice,<br/>
            contact [<?= $model->invoicedCreatedby0['userFName'].'  '.$model->invoicedCreatedby0['userLName']?>, <br/>
           Phone Number:    <?= $model->invoicedCreatedby0['userPhone']?><br/>
           Email:  <?= $model->invoicedCreatedby0['userEmail']?>]
         </p>
       </div>
       <!-- /.col -->
       <div class="col-xs-6">
         <?php $amountdue= $model->invoiceAmount-$payments;?>
         <p class="lead">Amount Due <strong>Ksh <?= number_format($amountdue,2) ;?></strong></p>

         <div class="table-responsive">
           <table class="table">
             <tr>
               <th style="width:50%">Subtotal:</th>
               <td>Ksh
                 Ksh <?= number_format($model->invoiceSubAmount,2) ;?></td>
             </tr>
             <tr>
               <th>Total Tax</th>
               <td>Ksh <?= number_format($model->invoiceTaxAmount,2) ;?></td>
             </tr>
             <tr>
               <th>Discount:</th>
               <td>Ksh <?= number_format($model->invoiceDiscountAmount,2) ;?></td>
             </tr>
             <tr>
               <th>Total:</th>
               <td>Ksh <?= number_format($model->invoiceAmount,2) ;?></td>
             </tr>

             <tr>
               <th>Total Payment:</th>
               <td>Ksh <?= number_format($payments,2) ;?></td>
             </tr>

           </table>
         </div>
       </div>
       <!-- /.col -->
     </div>
     <!-- /.row -->
     <!-- this row will not appear when printing -->
     <div class="row no-print">
       <div class="col-xs-12">
         <a href="javascript:void(0)" onclick="window.print()"
            class="btn btn-xs btn-success m-b-10"><i
                 class="fa fa-print m-r-5"></i> Print</a>
        <!-- <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
         </button> -->
         <?= Html::button('Submit Payment', ['value'=>Url::to((['makepayments', 'id' => $model['id']])),'class' => 'btn btn-success pull-right','id'=>'modalButton']) ?>
         <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
           <i class="fa fa-download"></i> Generate PDF
         </button>
       </div>
     </div>
   </section>
   <!-- /.content -->
