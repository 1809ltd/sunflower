<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceCreditMemoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-credit-memo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'creditMemoAccount') ?>

    <?= $form->field($model, 'refernumber') ?>

    <?= $form->field($model, 'creditMemoTxnDate') ?>

    <?= $form->field($model, 'creditMemoNote') ?>

    <?php // echo $form->field($model, 'customerId') ?>

    <?php // echo $form->field($model, 'creditAmount') ?>

    <?php // echo $form->field($model, 'creditMemoCreatedAt') ?>

    <?php // echo $form->field($model, 'creditMemoUpdatedAt') ?>

    <?php // echo $form->field($model, 'creditMemoDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
