<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventActivity */

$this->title = 'Create Finance Event Activity';
$this->params['breadcrumbs'][] = ['label' => 'Finance Event Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finance-event-activity-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
