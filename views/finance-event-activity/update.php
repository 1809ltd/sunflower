<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceEventActivity */

$this->title = 'Update Finance Event Activity: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Finance Event Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-event-activity-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
