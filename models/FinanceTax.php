<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "finance_tax".
 *
 * @property int $id
 * @property string $taxCode
 * @property double $taxRate
 * @property int $taxStatus
 * @property string $taxType
 * @property string $account
 * @property string $createdAt
 * @property string $updatedAt
 * @property string $deletedAt
 * @property int $userId
 *
 * @property FinanceAccounts $account0
 * @property UserDetails $user
 */
class FinanceTax extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'finance_tax';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['taxCode', 'taxRate', 'taxStatus', 'taxType', 'account', 'userId'], 'required'],
            [['taxRate'], 'number'],
            [['taxStatus', 'userId'], 'integer'],
            [['createdAt', 'updatedAt', 'deletedAt'], 'safe'],
            [['taxCode'], 'string', 'max' => 50],
            [['taxType'], 'string', 'max' => 100],
            [['account'], 'string', 'max' => 150],
            [['taxCode'], 'unique'],
            [['account'], 'exist', 'skipOnError' => true, 'targetClass' => FinanceAccounts::className(), 'targetAttribute' => ['account' => 'accountName']],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['userId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'taxCode' => 'Tax Code',
            'taxRate' => 'Tax Rate',
            'taxStatus' => 'Tax Status',
            'taxType' => 'Tax Type',
            'account' => 'Account',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'deletedAt' => 'Deleted At',
            'userId' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount0()
    {
        return $this->hasOne(FinanceAccounts::className(), ['accountName' => 'account']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['id' => 'userId']);
    }
}
