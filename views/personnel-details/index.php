<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonnelDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Personnel Details';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="box">
  <div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
  </div>
  <!-- /.box-header -->
  <div class="pull-right">
    <div class="col-sm-3">
      <div class="form-group">
        <p>
            <?= Html::a('Create Personnel Details', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
      </div>
    </div>
  </div>

    <?php
          Modal::begin([
              'id' => 'modal',
              'header' => '<h4>Add Event Workplan</h4>',
              'size'=>'modal-lg'
          ]);

          echo '<div class="modalContent"></div>';

          Modal::end();
    ?>

  <div class="box-body personnel-details-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=> function($model)
        {
          // code...
          if ($model->status==1) {
            // code...
            return['class'=>'success'];
          } else {
            // code...
            return['class'=>'danger'];
          }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

              // 'id',
              'nationalId',
              'displayName',
              'firstName',
              'sName',
              'lName',
              'primaryPhone',
              //'secondaryPhone',
              //'printOnChequeName',
              //'primaryAddr:ntext',
              //'town',
              //'area',
              //'email:email',
              //'hiredDate',
              //'createdAt',
              //'deletedAt',
              //'updatedAt',
              //'billable',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
