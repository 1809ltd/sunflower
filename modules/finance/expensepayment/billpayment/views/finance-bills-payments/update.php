<?php
use yii\helpers\Html;
use yii\widgets\Pjax;

/*Adding an Expanding Row */
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use kartik\select2\Select2;

/*to enable modal pop up*/
use yii\bootstrap\Modal;
use yii\helpers\Url;
// Payment Model
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPayments;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsSearch;
// Payment Line Items
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLines;
use app\modules\finance\expensepayment\billpayment\models\FinanceBillsPaymentsLinesSearch;
/*Getting the Vendor Details*/
use app\modules\vendor\vendordetails\models\VendorCompanyDetails;
/*Accounts */
use app\modules\finance\financesetup\coa\models\FinanceAccounts;
use app\modules\finance\financesetup\coa\models\FinanceAccountsSearch;

$this->title = 'Update Finance Bills Payments: ' . $model->billPaymentBillRef;
$this->params['breadcrumbs'][] = ['label' => 'Finance Bills Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->billPaymentBillRef, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="finance-bills-payments-update">
    <?php
     // $this->render('_billpaymentupdate', [
        // 'model' => $model,
        // 'searchModel' => $searchModel,
        // 'dataProvider' => $dataProvider
    // ]) ?>
</div>

<?php

/* @var $this yii\web\View */
/* @var $model app\modules\finance\receipt\invoicepayment\models\FinanceBillsPayments */

// $this->title = 'Record Bills Payments';
// $this->params['breadcrumbs'][] = ['label' => 'Bills Payments', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>

<div class="finance-bills-payments-create">

    <?php
    //  $this->render('_form', [
    //     'model' => $model,
    // ]) ?>

</div>
<?php

if (isset($_SESSION['vendorInvoiceId'])) {
  // code...
  // If Session of the customer is set based on the search
  $vendorInvoiceId = $_SESSION['vendorInvoiceId'];
  $modelVendor = VendorCompanyDetails::find()
          ->where('id = :vendorId', [':vendorId' => $vendorInvoiceId])
          ->one();
  ?>
  <div class="col-md-4">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Vendor Details</h3>
        <div class="box-tools pull-right">
        </div>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-striped table-condensed">
          <thead>
            <tr>
              <th style="width: 40%;">Title</th>
              <th style="width: 60%;">Detail</th>
            </tr>
          </thead>
          <tbody>
            <tr><td><span>Vendor Name :</span></td><td> <?= $modelVendor->vendorCompanyName;?> </td></tr>
            <tr><td><span>KRA Pin :</span></td><td><?= $modelVendor->vendorKraPin;?> </td></tr>
            <tr><td><span>Company Address :</span></td><td><?= $modelVendor->vendorCompanyAddress;?> </td></tr>
            <tr><td><span>Company Phone:</span></td><td><?= $modelVendor->vendorPhone;?> </td></tr>
            <tr><td><span>Company Email:</span></td><td><?= $modelVendor->vendoremail;?> </td></tr>
            <tr><td><span>Company Contact:</span></td><td><?= $modelVendor->vendorContactPersonFullName;?> </td></tr>
            <tr><td><span>Contact Phone:</span></td><td><?= $modelVendor->contactMobilePhone;?> </td></tr>
            <tr><td><span>Contact Emal:</span></td><td><?= $modelVendor->contactEmail;?> </td></tr>
            <tr><td colspan="2"><div class="col-md-2">
              <div class="form-actions">
                <a href="<?=Url::to(['/finance/expensepayment/bill-payment/default/closesearch']) ?>" class="btn btn-warning btn-sm pull-left text-center" >Close Search</a>
              </div>
            </div></td></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- Payments on Hold -->
  <div class="col-md-8">
    <?php
    // This is to get all the Invoices from Client whose Payments that are on hold
    $modelsFinanceBillsPaymentsLines1 = new FinanceBillsPayments();
    $output= $modelsFinanceBillsPaymentsLines1->getbillpaymentItems($model->id,$model->paymentstatus);

    //Calling the forms with the list of payments on hold plus passing the variables for form
    echo $this->render('_holdbillpaymentsinesUpdate', [
      // This is th model for the Bill Payments
      'model' => $model,
      // This is the result of the clients hold payments that are not yet assigned
      'output' => $output,
      ])
    ?>

  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Bill Payment Receipt</h3>
          <div class="box-tools pull-right">
          </div>
        </div>
        <?php
         // To get only payment from the client
         $searchModel = new FinanceBillsPaymentsSearch();
         $searchModel->vendorId = $modelVendor->vendorCompanyName;
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        ?>
           <div class="box-body finance-bills-payments-index">
             <?php Pjax::begin(); ?>
             <?php // echo $this->render('_search', ['model' => $searchModel]);

             ?>
             <?= GridView::widget([
                 'dataProvider' => $dataProvider,
                 'filterModel' => $searchModel,
                 'rowOptions'=> function($model)
                 {
                   // code...
                   if ($model->paymentstatus ==1) {
                     // code...
                     return['class'=>'success'];
                   } elseif ($model->paymentstatus==2) {
                     // code...

                     return['class'=>'warning'];
                   } else {
                     // code...
                     return['class'=>'danger'];
                   }

                 },
                 'columns' => [
                     ['class' => 'kartik\grid\ExpandRowColumn',
                       'value'=>function ($model,$key,$index,$column)
                       {
                         // code...
                         return GridView::ROW_COLLAPSED;
                       },
                       'detail'=>function ($model,$key,$index,$column)
                       {
                         // code...
                         $searchModel = new FinanceBillsPaymentsLinesSearch();
                         $searchModel->billId = $model->id;
                         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

                         return Yii::$app->controller->renderPartial('_paymentlines',[
                           'searchModel' => $searchModel,
                           'dataProvider' => $dataProvider,

                         ]);
                       },
                   ],
                     // [
                     //   'attribute'=>'vendorId',
                     //   'value'=>'vendor.vendorCompanyName',
                     // ],
                     'vendor.vendorCompanyName',
                     'billPaymentBillRef',
                     // 'id',
                     'billPaymentPayType',
                     [
                       'attribute'=>'billPaymentaccount',
                       'value'=>'billPaymentaccount0.fullyQualifiedName',
                     ],
                     'billPaymentTotalAmt',
                     'unallocatedAmount',
                     //'vendorId',
                     //'billPaymentPrivateNote:ntext',
                     //'billPaymentCreatedAt',
                     //'billPaymentUpdatedAt',
                     //'billPaymentDeletedAt',
                     //'userId',
                     //'paymentstatus',
                     // ['class' => 'yii\grid\ActionColumn'],
                     [
                         'class' => \microinginer\dropDownActionColumn\DropDownActionColumn::className(),
                         'items' => [
                             [
                                 'label' => 'View',
                                 'url'   => ['view'],
                             ],
                             [
                                 'label' => 'Update',
                                 'url'   => ['update'],
                                 'linkOptions' => [
                                     'data-method' => 'post'
                                 ],
                                 // 'options'=>['class'=>'update-modal-click grid-action'],
                                 // 'update'=>function($url,$model,$key){
                                 //       $btn = Html::button("<span class='glyphicon fa fa-pencil'></span>",[
                                 //           'value'=>Url::to((['update','id'=>$key])), //<---- here is where you define the action that handles the ajax request
                                 //           'class'=>'update-modal-click grid-action',
                                 //           'data-toggle'=>'tooltip',
                                 //           'data-placement'=>'bottom',
                                 //           'title'=>'Update'
                                 //       ]);
                                 //       return $btn;
                                 // },
                                 // 'option' 'class'=>'update-modal-click grid-action',
                                 // 'data-toggle'=>'tooltip',
                                 // 'data-placement'=>'bottom',
                             ],
                             [
                                 'label'   => 'Disable',
                                 'url'     => ['delete'],
                                 'linkOptions' => [
                                     'data-method' => 'post'
                                 ],
                             ],
                         ]
                     ],
                 ],
             ]); ?>
             <?php Pjax::end(); ?>
           </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
  <?php

} else {
  // code...
  ?>
  <div class="box-body">

    <div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h4><i class="icon fa fa-warning"></i> Alert!</h4>
      Please Select the Vendor you wish to add data for
    </div>
    <!-- <p class="pull-right"> -->
      <!-- <?= Html::button('Select Vendor', ['value'=>Url::to((['/finance/expensepayment/bill-payment/default/search'])),'class' => 'btn btn-sm btn-success','id'=>'modalButton']) ?> -->

      <?php
      // Modal::begin([
      //   'header'=>'<h4>Search Supplier</h4>',
      //   'class'=>'modal-dialog',
      //   'id'=>'modal',
      //   'size'=>'modal-lg',
      //   'options' => [
      //       'tabindex' => false // important for Select2 to work properly
      //   ],
      // ]);
      // echo "<div class='modal-content' id='modalContent'></div>";
      // Modal::end();
      ?>

        <?php
         // Html::a('Record Invoice Payments', ['create'], ['class' => 'btn btn-success']) ?>
    <!-- </p> -->

  </div>
  <?php
}
?>
