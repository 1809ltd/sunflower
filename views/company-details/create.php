<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompanyDetails */

$this->title = 'Create Company Details';
$this->params['breadcrumbs'][] = ['label' => 'Company Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-details-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
