<?php

namespace app\modules\finance\productsetup\financeeventactivity;

/**
 * financeeventactivity module definition class
 */
class financeeventactivity extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\productsetup\financeeventactivity\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
