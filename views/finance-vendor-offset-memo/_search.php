<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\finance\offset\outhouse\models\FinanceVendorOffsetMemoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-vendor-offset-memo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'vendorOffsetMemoAccount') ?>

    <?= $form->field($model, 'refernumber') ?>

    <?= $form->field($model, 'vendorOffsetMemoTxnDate') ?>

    <?= $form->field($model, 'vendorOffsetMemoNote') ?>

    <?php // echo $form->field($model, 'vendorId') ?>

    <?php // echo $form->field($model, 'offsetAmount') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoStatus') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoCreatedAt') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoUpdatedAt') ?>

    <?php // echo $form->field($model, 'vendorOffsetMemoDeletedAt') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
