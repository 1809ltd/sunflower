<?php

namespace app\modules\finance\expensepayment\officeexpensepayment\controllers;

use Yii;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLines;
use app\modules\finance\expensepayment\officeexpensepayment\models\FinanceOfficeRequisitionPaymentsLinesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FinanceOfficeRequisitionPaymentsLinesController implements the CRUD actions for FinanceOfficeRequisitionPaymentsLines model.
 */
class FinanceOfficeRequisitionPaymentsLinesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FinanceOfficeRequisitionPaymentsLines models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FinanceOfficeRequisitionPaymentsLinesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FinanceOfficeRequisitionPaymentsLines model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FinanceOfficeRequisitionPaymentsLines model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
      $model = new FinanceOfficeRequisitionPaymentsLines();

      //Geting the time created Timestamp
      $model->created_at= new \yii\db\Expression('NOW()');
      //Getting user Log in details
      $model->userId = Yii::$app->user->identity->id;

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
        // return $this->redirect(['view', 'id' => $model->id]);

        if (!empty($model->officeReqpaymentId)) {
          // code...
          return $this->redirect(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/update', 'id' => $model->officeReqpaymentId]);

        } else {
          // code...
          return $this->redirect(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/create']);
        }
      }
      return $this->renderAjax('create', [
        'model' => $model,
      ]);
    }

    /**
     * Updates an existing FinanceOfficeRequisitionPaymentsLines model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'id' => $model->id]);

            if (!empty($model->officeReqpaymentId)) {
              // code...
              return $this->redirect(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/update', 'id' => $model->officeReqpaymentId]);

            } else {
              // code...
              return $this->redirect(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/create']);

            }

            // return $this->redirect(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/create']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FinanceOfficeRequisitionPaymentsLines model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->layout = '@app/views/layouts/addformdatatablelayout';
        // $this->findModel($id)->delete();

        // Update all the data based on the payment made and status
        $this->findModel($id)->updateAttributes(['status'=> 0,'deletedAt' => new \yii\db\Expression('NOW()')]);

        return $this->redirect(['/finance/expensepayment/officeexpensepayment/finance-office-requisition-payments/create']);


        // return $this->redirect(['index']);
    }

    /**
     * Finds the FinanceOfficeRequisitionPaymentsLines model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FinanceOfficeRequisitionPaymentsLines the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FinanceOfficeRequisitionPaymentsLines::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
