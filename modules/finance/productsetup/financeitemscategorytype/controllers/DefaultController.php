<?php

namespace app\modules\finance\productsetup\financeitemscategorytype\controllers;

use yii\web\Controller;

/**
 * Default controller for the `financeitemscategorytype` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
