<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AssetRegistration */

$this->title = 'Create Asset Registration';
$this->params['breadcrumbs'][] = ['label' => 'Asset Registrations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asset-registration-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
