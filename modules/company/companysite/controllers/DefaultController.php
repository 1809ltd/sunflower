<?php

namespace app\modules\company\companysite\controllers;

use yii\web\Controller;

/**
 * Default controller for the `companysite` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
