<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

use yii\helpers\ArrayHelper;
// Getting th Various Roles

use app\modules\user\userroletypes\models\UserRoleTypes;

/* @var $this yii\web\View */
/* @var $model app\modules\user\userdetails\models\UserDetails */


$this->title = $model->userFName;
$this->params['breadcrumbs'][] = ['label' => 'User Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<style>.summary{display: none;}</style>
<div class="box box-primary">

  <div class="box-body chart-responsive">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

  </div>

</div>

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="<?=Url::to('@web/'.$model->userImage); ?>" alt="User profile picture">

          <h3 class="profile-username text-center"><?= $model->userFName." ".$model->userLName ?></h3>

          <p class="text-muted text-center">Staff Number: <?= $model->userStaffId ?></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Upcoming Event</b> <a class="pull-right">1,322</a>
            </li>
            <li class="list-group-item">
              <b>Following</b> <a class="pull-right">543</a>
            </li>
            <li class="list-group-item">
              <b>Friends</b> <a class="pull-right">13,287</a>
            </li>
          </ul>

          <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

      <!-- About Me Box -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">About Me</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> UserName</strong>

          <p class="text-muted">
            <?= $model->username ?>
          </p>

          <hr>

          <strong><i class="fa fa-map-marker margin-r-5"></i> Email</strong>

          <p class="text-muted"><?= $model->userEmail ?></p>

          <hr>

          <strong><i class="fa fa-pencil margin-r-5"></i> Permissions</strong>

          <p>
            <span class="label label-danger">UI Design</span>
            <span class="label label-success">Coding</span>
            <span class="label label-info">Javascript</span>
            <span class="label label-warning">PHP</span>
            <span class="label label-primary">Node.js</span>
          </p>

          <hr>

          <strong><i class="fa fa-file-text-o margin-r-5"></i> Last Log in</strong>

          <p> <?= Yii::$app->formatter->asDateTime($model->userLastLogin, 'long');?></p>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
          <li><a href="#timeline" data-toggle="tab">Timeline</a></li>
          <li><a href="#settings" data-toggle="tab">Assign Permissions</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="activity">


          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="timeline">

          </div>
          <!-- /.tab-pane -->

          <div class="tab-pane" id="settings">
            <?php $form = ActiveForm::begin([
              'action' => ['user-details/ajaxrights'],
              'class' => 'form-horizontal',
              'options' => [
                'class' => 'rights-form'
              ]
            ]); ?>
            <?= $form->field($modelAssignRights, 'userId')->hiddenInput(['value'=> $model->id])->label(false);?>
            <?= $form->field($modelAssignRights, 'status')->hiddenInput(['value'=> "1"])->label(false);?>
            <div class="form-group">
              <?= $form->field($modelAssignRights, 'roleId')->widget(Select2::classname(), [
                  'data' => ArrayHelper::map(UserRoleTypes::find()->where('`status` = 1')->asArray()->all(),'id','roleName'),
                  'language' => 'en',
                  'options' => ['id'=> 'roleId', 'placeholder' => 'Select a Role ...'],
                  'pluginOptions' => [
                      'allowClear' => true
                  ],
              ]); ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            <?php
              if (!empty($modelsUSerslistPErmision)) {
                // list is empty.
                ?>
                <?php Pjax::begin(); ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'id',
                        // 'roleId',
                        [
                          'attribute'=>'roleId',
                          'value'=>'role.roleName',
                        ],
                        // 'role.roleName',
                        // 'userId',
                        [
                          'attribute'=>'createdBy',
                          'value'=>'createdBy0.username',
                        ],
                        [
                          'attribute'=>'assigner',
                          'value'=>'assigner0.username',
                        ],
                        'createdAt',
                        //'updatedAt',
                        //'deletedAt',
                        //'assigner',
                        //'status',

                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
                <?php Pjax::end(); ?>
                <?php

                // print_r($modelsUSerslistPErmision);

              }else {
                // code...

                ?>
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                      No Permission granted
                </div>
                <?php
              }
            ?>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<?php
// $script = <<<EOD
// jQuery(document).ready(function($) {
//    $(".rights-form").submit(function(event) {
//         event.preventDefault(); // stopping submitting
//         var data = $(this).serializeArray();
//         var url = $(this).attr('action');
//         $.ajax({
//             url: url,
//             type: 'post',
//             dataType: 'json',
//             data: data
//         })
//         .done(function(response) {
//             if (response.data.success == true) {
//                 alert("Wow you Added Rights");
//             }
//         })
//         .fail(function() {
//             console.log("error");
//         });
//
//     });
// });
//
// EOD;
// $this->registerJs($script);
/*end getting the totalamount */
 ?>
<?php
use yii\web\View;
$this->registerJs("
   $('li.treeview').removeClass('active open');
   $('#Roles').addClass('active open');
   $('#role').addClass('active open');
", View::POS_READY);
?>
