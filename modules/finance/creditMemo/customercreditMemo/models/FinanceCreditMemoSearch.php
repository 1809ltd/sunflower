<?php

namespace app\modules\finance\creditMemo\customercreditMemo\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\finance\creditMemo\customercreditMemo\models\FinanceCreditMemo;

/**
 * FinanceCreditMemoSearch represents the model behind the search form of `app\modules\finance\creditMemo\customercreditMemo\models\FinanceCreditMemo`.
 */
class FinanceCreditMemoSearch extends FinanceCreditMemo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'creditMemoAccount', 'creditMemoStatus', 'userId'], 'integer'],
            [['refernumber', 'creditMemoTxnDate','customerId', 'creditMemoNote', 'creditMemoCreatedAt', 'creditMemoUpdatedAt', 'creditMemoDeletedAt'], 'safe'],
            [['creditAmount', 'totalTax'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FinanceCreditMemo::find();
        $query->joinWith(['customer']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'creditMemoAccount' => $this->creditMemoAccount,
            'creditMemoTxnDate' => $this->creditMemoTxnDate,
            // 'customerId' => $this->customerId,
            'creditAmount' => $this->creditAmount,
            'totalTax' => $this->totalTax,
            'creditMemoStatus' => $this->creditMemoStatus,
            'creditMemoCreatedAt' => $this->creditMemoCreatedAt,
            'creditMemoUpdatedAt' => $this->creditMemoUpdatedAt,
            'creditMemoDeletedAt' => $this->creditMemoDeletedAt,
            'userId' => $this->userId,
        ]);

        $query->andFilterWhere(['like', 'refernumber', $this->refernumber])
        ->andFilterWhere(['like', 'customerDisplayName', $this->customerId])
            ->andFilterWhere(['like', 'creditMemoNote', $this->creditMemoNote]);

        return $dataProvider;
    }
}
