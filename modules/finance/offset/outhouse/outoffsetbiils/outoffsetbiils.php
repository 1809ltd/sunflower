<?php

namespace app\modules\finance\offset\outhouse\outoffsetbiils;

/**
 * outoffsetbiils module definition class
 */
class outoffsetbiils extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\finance\offset\outhouse\outoffsetbiils\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
