<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FinanceBillsLines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="finance-bills-lines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'biilsId')->textInput() ?>

    <?= $form->field($model, 'biilsLinesVendorsRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biilsLinesDescription')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'biilsLinespacks')->textInput() ?>

    <?= $form->field($model, 'biilsLinesQty')->textInput() ?>

    <?= $form->field($model, 'billlinePriceperunit')->textInput() ?>

    <?= $form->field($model, 'biilsLinesAmount')->textInput() ?>

    <?= $form->field($model, 'billLinesTaxCodeRef')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biilsLinesTaxAmount')->textInput() ?>

    <?= $form->field($model, 'accountId')->textInput() ?>

    <?= $form->field($model, 'billsLinesBillableStatus')->textInput() ?>

    <?= $form->field($model, 'billsLinesCreatedAt')->textInput() ?>

    <?= $form->field($model, 'billsLinesDeleteAt')->textInput() ?>

    <?= $form->field($model, 'billsLinesUpdatedAt')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
