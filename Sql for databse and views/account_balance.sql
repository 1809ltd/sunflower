-- ----------------------------
-- View structure for `account_balance`
-- ----------------------------
CREATE OR REPLACE  VIEW `account_balance` AS SELECT
`all_transactions`.`accountParentId` AS `accountParentId`,
`all_transactions`.`accountsclassfication` AS `accountsclassfication`,
`all_transactions`.`accountId` AS `accountId`,
`all_transactions`.`accountName` AS `accountName`,
sum( `all_transactions`.`dr_amount` ) AS `dr_amount`,
sum( `all_transactions`.`cr_amount` ) AS `cr_amount`,
( sum( `all_transactions`.`dr_amount` ) - sum( `all_transactions`.`cr_amount` ) ) AS `balance`
FROM
	`all_transactions`
WHERE
	(
	( `all_transactions`.`accountParentId` = 6 )
	AND ( cast( `all_transactions`.`transactionDate` AS date ) <= cast( curdate( ) AS date ) )
	)
GROUP BY
	`all_transactions`.`accountId`
ORDER BY
	`all_transactions`.`accountId`;
